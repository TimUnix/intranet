$(document).ready(function(){

	let skuval;
    let barval;
    let brandval;
    let ffs;

	const baseUrl = $('#drivesFieldEditScript').data('baseUrl');

    function addEditableFieldsku(cell, value, className) {
        const input = $(`<input autofocus type="text" onfocus="this.selectionStart = this.selectionEnd = this.value.length;" class="form-control ${className}-input" value="${value}">`);
        skuval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    function addEditableFieldbar(cell, value, className) {
        const input = $(`<input autofocus type="text" onfocus="this.selectionStart = this.selectionEnd = this.value.length;" class="form-control ${className}-input" value="${value}">`);
        barval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }

     function addEditableField(cell, value, className) {
        const input = $(`<input autofocus type="text" onfocus="this.selectionStart = this.selectionEnd = this.value.length;" class="form-control ${className}-input" value="${value}">`);
        value

        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    function addDropDownff(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input w-auto">` +
                        `<option value=""></option>` +
                        `<option value="1.8in">1.8in</option>` +
                        `<option value="2.5in">2.5in</option>` +
                        `<option value="3.5in">3.5in</option>` +
                        `<option value="U.2">U.2</option>` +
                        `<option value="M.2">M.2</option>` +
                        `<option value="PCIe">PCIe</option>` +
                        `</select>`);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html('<i class="fa fa-check" aria-hidden="true"></i>');

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }
    function addDropDowntype(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input w-auto">` +
                        `<option value="SAS">SAS</option>` +
                        `<option value="SATA">SATA</option>` +
                        `<option value="SSD">SSD</option>` +
                        `</select>`);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html('<i class="fa fa-check" aria-hidden="true"></i>');

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }
    function addDropDowninterface(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input w-auto">` +
                        `<option value=""></option>` +
                        `<option value="SSD">SSD</option>` +
                        `</select>`);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html('<i class="fa fa-check" aria-hidden="true"></i>');

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }
    function addDropDowncondition(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input w-auto">` +
                        `<option value=""></option>` +
                        `<option value="NEW">NEW</option>` +
                        `<option value="NP3">NP3</option>` +
                        `<option value="NP6">NP6</option>` +
                        `<option value="GB">Grade B</option>` +
                        `</select>`);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html('<i class="fa fa-check" aria-hidden="true"></i>');

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }

	//SKU
   $('#product_table_body').click('.sku', function(event) {
        if(event.target.className == "sku"){
            const notes = $(event.target).closest('.sku');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel skubtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
        }else if(event.target.className == "fa fa-edit skupenbtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
            $("input:text").focus();
        }
    });
      

     $('#product_table_body').click('.sku-editable .sku-cancel', function(event) {
        const btnCancel = $(event.target).closest('.sku-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.sku-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('sku-editable');
        notesCell.addClass('sku');
    });

   $('#product_table_body').click('.sku-editable .sku-apply', function(event) {
        const btnApply = $(event.target).closest('.sku-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.sku-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.sku-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('sku-editable');
        notes.addClass('sku');
    
        

        var datas = {
            'data': {
                'sku': skuval, 
                'bar': bar.text().trim(),
                'data' :textField.val(), 
                'type' : "sku"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //SKU end

   // Barcode start
  $('#product_table_body').click('.bar', function(event) {
        if(event.target.className == "bar"){
            const notes = $(event.target).closest('.bar');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel barbtn"){
            const notes = $(event.target).closest('.bar');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbar(notes, value, 'bar');
        }else if(event.target.className == "fa fa-edit barpenbtn"){
            const notes = $(event.target).closest('.bar');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbar(notes, value, 'bar');
            $("input:text").focus();
        }
    });
      

     $('#product_table_body').click('.bar-editable .bar-cancel', function(event) {
        const btnCancel = $(event.target).closest('.bar-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.bar-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit barpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('bar-editable');
        notesCell.addClass('bar');
    });

   $('#product_table_body').click('.bar-editable .bar-apply', function(event) {
        const btnApply = $(event.target).closest('.bar-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.bar-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        const brand = btnApply.closest('tr').find('.brand');
        
    
        const textField = notes.find('.bar-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel barbtn"><i class="fa fa-edit barpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('bar-editable');
        notes.addClass('bar');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': barval,
                'data' :textField.val(), 
                'type' : "bar"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                
            }
        });
        
    });
   //barcode end

   //upc start
   $('#product_table_body').click('.upc', function(event) {
    if(event.target.className == "upc"){
        const notes = $(event.target).closest('.upc');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel upcbtn"){
        const notes = $(event.target).closest('.upc');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'upc');

    }else if(event.target.className == "fa fa-edit upcpenbtn"){
        const notes = $(event.target).closest('.upc');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'upc');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.upc-editable .upc-cancel', function(event) {
        const btnCancel = $(event.target).closest('.upc-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.upc-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel upcdbtn"><i class="fa fa-edit upcpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('upc-editable');
        notesCell.addClass('upc');
    });
    $('#product_table_body').click('.upc-editable .upc-apply', function(event) {
        const btnApply = $(event.target).closest('.upc-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.upc-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        

        const textField = notes.find('.upc-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel upcbtn"><i class="fa fa-edit upcpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('upc-editable');
        notes.addClass('upc');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "upc"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                // $("#load").html(msg);
            
            }
        });
    });
   //upc end

   //brand start
    $('#product_table_body').click('.brand', function(event) {
        if(event.target.className == "brand"){
            const notes = $(event.target).closest('.brand');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel brandbtn"){
            const notes = $(event.target).closest('.brand');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'brand');
        }else if(event.target.className == "fa fa-edit brandpenbtn"){
            const notes = $(event.target).closest('.brand');

            if (!notes) return;
    
            const value = notes.data('value');
           
            addEditableField(notes, value, 'brand');
            $("input:text").focus();
            
        }
    });
   
    $('#product_table_body').click('.brand-editable .brand-cancel', function(event) {
        const btnCancel = $(event.target).closest('.brand-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.brand-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('brand-editable');
        notesCell.addClass('brand');
    });
    $('#product_table_body').click('.brand-editable .brand-apply', function(event) {
        const btnApply = $(event.target).closest('.brand-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.brand-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.brand-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('brand-editable');
        notes.addClass('brand');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "brand"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                // $("#load").html(msg);
               
            }
        });
    });
   //brand end

   //form factor (ff) start
   $('#product_table_body').click('.ff', function(event) {
        if(event.target.className == "ff"){
            const notes = $(event.target).closest('.ff');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel ffbtn"){
            const notes = $(event.target).closest('.ff');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDownff(notes, value, 'ff');
        }else if(event.target.className == "fa fa-edit ffpenbtn"){
            const notes = $(event.target).closest('.ff');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDownff(notes, value, 'ff');
        }
    });
   
    $('#product_table_body').click('.ff-editable .ff-cancel', function(event) {
        const btnCancel = $(event.target).closest('.ff-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.ff-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel ffbtn"><i class="fa fa-edit ffpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('ff-editable');
        notesCell.addClass('ff');
    });
    $('#product_table_body').click('.ff-editable .ff-apply', function(event) {
        const btnApply = $(event.target).closest('.ff-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.ff-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.ff-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel ffbtn"><i class="fa fa-edit ffpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('ff-editable');
        notes.addClass('ff');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "ff"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            
            }
        });
    });
   //form factor (ff) end

   //size start
    $('#product_table_body').click('.size', function(event) {
        if(event.target.className == "size"){
            const notes = $(event.target).closest('.size');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel sizebtn"){
            const notes = $(event.target).closest('.size');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'size');
        }else if(event.target.className == "fa fa-edit sizepenbtn"){
            const notes = $(event.target).closest('.size');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'size');
            $("input:text").focus();
        }
    });
   
    $('#product_table_body').click('.size-editable .size-cancel', function(event) {
        const btnCancel = $(event.target).closest('.size-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.size-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel sizebtn"><i class="fa fa-edit sizepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('size-editable');
        notesCell.addClass('size');
    });
    $('#product_table_body').click('.size-editable .size-apply', function(event) {
        const btnApply = $(event.target).closest('.size-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.size-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.size-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel sizebtn"><i class="fa fa-edit sizepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('size-editable');
        notes.addClass('size');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "size"}
        };
       
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
               
            }
        });
    });
   //size end

   //type start
   $('#product_table_body').click('.type', function(event) {
        if(event.target.className == "type"){
            const notes = $(event.target).closest('.type');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel typebtn"){
            const notes = $(event.target).closest('.type');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDowntype(notes, value, 'type');
        }else if(event.target.className == "fa fa-edit typepenbtn"){
            const notes = $(event.target).closest('.type');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDowntype(notes, value, 'type');
        }
    });
   
    $('#product_table_body').click('.type-editable .type-cancel', function(event) {
        const btnCancel = $(event.target).closest('.type-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.type-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel typebtn"><i class="fa fa-edit typepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('type-editable');
        notesCell.addClass('type');
    });
    $('#product_table_body').click('.type-editable .type-apply', function(event) {
        const btnApply = $(event.target).closest('.type-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.type-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.type-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel typebtn"><i class="fa fa-edit typepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('type-editable');
        notes.addClass('type');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "type"}
        };
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //type end

   //rpm start
   $('#product_table_body').click('.rpm', function(event) {
        if(event.target.className == "rpm"){
            const notes = $(event.target).closest('.rpm');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel rpmbtn"){
            const notes = $(event.target).closest('.rpm');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'rpm');
        }else if(event.target.className == "fa fa-edit rpmpenbtn"){
            const notes = $(event.target).closest('.rpm');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'rpm');
            $("input:text").focus();
        }
    });
   
    $('#product_table_body').click('.rpm-editable .rpm-cancel', function(event) {
        const btnCancel = $(event.target).closest('.rpm-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.rpm-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rpmbtn"><i class="fa fa-edit rpmpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('rpm-editable');
        notesCell.addClass('rpm');
    });
    $('#product_table_body').click('.rpm-editable .rpm-apply', function(event) {
        const btnApply = $(event.target).closest('.rpm-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.rpm-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.rpm-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rpmbtn"><i class="fa fa-edit rpmpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('rpm-editable');
        notes.addClass('rpm');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "rpm"}
        };
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //rpm end

   //speed start
    $('#product_table_body').click('.speed', function(event) {
        if(event.target.className == "speed"){
            const notes = $(event.target).closest('.speed');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel speedbtn"){
            const notes = $(event.target).closest('.speed');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'speed');
        }else if(event.target.className == "fa fa-edit speedpenbtn"){
            const notes = $(event.target).closest('.speed');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'speed');
            $("input:text").focus();
        }
    });
   
    $('#product_table_body').click('.speed-editable .speed-cancel', function(event) {
        const btnCancel = $(event.target).closest('.speed-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.speed-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel speedbtn"><i class="fa fa-edit speedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('speed-editable');
        notesCell.addClass('speed');
    });
    $('#product_table_body').click('.speed-editable .speed-apply', function(event) {
        const btnApply = $(event.target).closest('.speed-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.speed-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.speed-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel speedbtn"><i class="fa fa-edit speedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('speed-editable');
        notes.addClass('speed');
    
        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "speed"}
        };
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //speed end

   //interface start
    $('#product_table_body').click('.interface', function(event) {
        if(event.target.className == "interface"){
            const notes = $(event.target).closest('.interface');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel interfacebtn"){
            const notes = $(event.target).closest('.interface');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDowninterface(notes, value, 'interface');
        }else if(event.target.className == "fa fa-edit interfacepenbtn"){
            const notes = $(event.target).closest('.interface');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'interface');
            $("input:text").focus();
        }
    });
   
    $('#product_table_body').click('.interface-editable .interface-cancel', function(event) {
        const btnCancel = $(event.target).closest('.interface-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.interface-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel interfacebtn"><i class="fa fa-edit interfacepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('interface-editable');
        notesCell.addClass('interface');
    });
    $('#product_table_body').click('.interface-editable .interface-apply', function(event) {
        const btnApply = $(event.target).closest('.interface-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.interface-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.interface-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel interfacebtn"><i class="fa fa-edit interfacepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('interface-editable');
        notes.addClass('interface');
    
        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "interface"}
        };
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //interface end

   //series start
    $('#product_table_body').click('.series', function(event) {
        if(event.target.className == "series"){
            const notes = $(event.target).closest('.series');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel seriesbtn"){
            const notes = $(event.target).closest('.series');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'series');
        }else if(event.target.className == "fa fa-edit seriespenbtn"){
            const notes = $(event.target).closest('.series');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'series');
            $("input:text").focus();
        }
    });
   
    $('#product_table_body').click('.series-editable .series-cancel', function(event) {
        const btnCancel = $(event.target).closest('.series-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.series-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel seriesbtn"><i class="fa fa-edit seriespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('series-editable');
        notesCell.addClass('series');
    });
    $('#product_table_body').click('.series-editable .series-apply', function(event) {
        const btnApply = $(event.target).closest('.series-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.series-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.series-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel seriesbtn"><i class="fa fa-edit seriespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('series-editable');
        notes.addClass('series');
    
        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "series"}
        };
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //series end

   //con start
    $('#product_table_body').click('.con', function(event) {
        if(event.target.className == "con"){
            const notes = $(event.target).closest('.con');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel conbtn"){
            const notes = $(event.target).closest('.con');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDowncondition(notes, value, 'con');
        }else if(event.target.className == "fa fa-edit conpenbtn"){
            const notes = $(event.target).closest('.con');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDowncondition(notes, value, 'con');
        }
    });
   
    $('#product_table_body').click('.con-editable .con-cancel', function(event) {
        const btnCancel = $(event.target).closest('.con-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.con-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('con-editable');
        notesCell.addClass('con');
    });
    $('#product_table_body').click('.con-editable .con-apply', function(event) {
        const btnApply = $(event.target).closest('.con-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.con-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.bar');
        
    
        const textField = notes.find('.con-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('con-editable');
        notes.addClass('con');
    
        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'bar': bar.text().trim(), 
                'data' : textField.val(), 
                'type' : "con"
            }
        };
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
               
            }
        });
    });
   //con end

   $('#product_table_body').click('.templatepenbtn', function(){
    const button = $(event.target).closest('.templatepenbtn');
    if(!button) return;
    const capacity = button.closest('tr').find('.item-size').text().trim();
    const ff = button.closest('tr').find('.item-ff').text().trim();
    const model = button.closest('tr').find('.item-bar').text().trim();
    const interface = button.closest('tr').find('.item-type').text().trim();
    const con = button.closest('tr').find('.item-con').text().trim();
    const brand = button.closest('tr').find('.item-brand').text().trim();
    const capacity_length = capacity.length;
   
    if(capacity_length>2){
        var capacity_val = capacity.concat('MB');
    }else{
        var capacity_val = capacity.concat('TB');
    }

    $('#capacity_template').val(capacity_val);
    $('#form_factor_template').val(ff);
    $('#model_template').val(model);
    $('#interface_template').val(interface);
    $('#condition_template').val(con);
    $('#brand_template').val(brand);
    $('.html-box').hide();
    $('#generateTemplate').modal('show');
   });

});