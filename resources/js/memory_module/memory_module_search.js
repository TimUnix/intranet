const columnsToCopy = {
    partNumber:3,
    stepcode: 4,
    speed: 5,
    numbercores: 6,
    codename: 7,
    tdp : 8,
    cache : 9,
    socket_support: 10,
    grade: 11
    
};

let baseUrls;

$(document).ready(function() {
    baseUrls = $("#inventoryScript").data('baseUrl');
    $('.filters').select2();
    $(".btn-copy-textarea").click(function() {
        const text = $(".textarea-copy").val();

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = text;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();
    });

    const searchParam = new URLSearchParams(window.location.search);
    const search = searchParam.get('search');
    var categoryFilters = searchParam.get('category_filters');
    var conditionFilters = searchParam.get('condition_filters');
    var sortOrder = searchParam.get('sort_order');
    var orderBy = searchParam.get('order_by');

    const wasItemRequested = search !== null;

    if (wasItemRequested) {
        if (categoryFilters !== null)
        {
            $('#categories').select2().val(categoryFilters.split(','));
            $('#categories').trigger('change');
        }
        else{
            categoryFilters = '';
        }
        if (conditionFilters !== null)
        {
            $('#conditions').select2().val(conditionFilters.split(','));
            $('#conditions').trigger('change');
        }
        else{
            conditionFilters = '';
        }
        if (sortOrder !== null && orderBy !== null)
        {
            $('#orderBy').find(':selected').prop('selected', false);
            $('#sortOrder').find(':selected').prop('selected', false);   
            $(`#orderBy > option[value="${orderBy}"]`).prop('selected', true);
            $(`#sortOrder > option[value="${sortOrder}"]`).prop('selected', true);
        }
        else{
            sortOrder='';
            orderBy='';
        }

        $("#total-items").html(spinnerLight);
        $("#total-price").html(spinnerLight);
        $('#itemSearch').val(search);

        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        
        const dataItems = {
            'search': search,
            'category_filters': categoryFilters,
            'condition_filters': conditionFilters,
            'sort_order': sortOrder,
            'order_by': orderBy
        };

        $.get(`${baseUrls}index.php/inventory/request/load_summary`, dataItems, function(response){
            $("#total-items").html(response.data.total);
            $("#total-price").html(`$${response.data.price}`);
        });

        $.get(`${baseUrls}index.php/inventory/request/load_items`, dataItems, function(response){
            $("#product_table_body").html(response);
            $('#exportCSV').prop('disabled', false);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
        });

        saveHistory(search);
    }

    $('#btnApplyFilters').click(function(event) {
        event.preventDefault();
        
        searcher();
        $('#filterModal').modal('hide');
    });

    $('#product_table_body').click('.ebay_unixpluscom', function(event) {
        const data = $(event.target).closest('.ebay_unixpluscom');

        if (!data) return;
        
        const image = data.find('img').attr('src');

        if (!image) return;

        const par = $(data).closest('tr').find('.par').text().trim();
        openInEbay(encodeURI(par), 'UNIXPlusCom');
    });

    $('#product_table_body').click('.ebay_unixsurplusnet', function(event) {
        const data = $(event.target).closest('.ebay_unixsurplusnet');

        if (!data) return;
        
        const image = data.find('img').attr('src');

        if (!image) return;

        const par = data.closest('tr').find('.par').text().trim();
        openInEbay(encodeURI(par), 'UNIXSurplusNet', a);
    });

    $('#sortOrder').change(function() {
        searcher();
    });

    $('#orderBy').change(function() {
        searcher();
    });

    $('#searchHistory').change(function(event) {
        const selected = $(event.target).find(':selected');

        $('#itemSearch').val(selected.val());
        searcher();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();
        searcher();
    });

    function searcher() {
        try {
            const searchItem = $('#itemSearch').val();
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();
        const categories = $('#categories').select2('data')
              .map(subcat => subcat.id)
              .join(',');
        const conditions = $('#conditions').select2('data')
              .map(subcategory => subcategory.id)
              .join(',');


        const route = `${baseUrls}index.php/inventory?search=${searchItem}&order_by=${orderBy}&sort_order=`
              + `${sortOrder}&category_filters=${categories}&condition_filters=${conditions}`;

        window.location.href = route;
        } catch (error) {
            
        }
        
    }

    function saveHistory(search) {
        $.post(`${baseUrls}index.php/inventory/request/save_search_history`, { 'search': search });
    }

    function openInEbay(par, store) {
        const newTab = window.open();
        
        $.get(`${baseUrls}index.php/inventory/find_item_number`, { 'par': par, 'store': store }, function(response) {
            if (response === 'Could not find item number.'){
                $(document).Toasts('create', {
                    title: 'Redirect Error',
                    body: response,
                    icon: 'fas fa-exclamation',
                    autohide: true,
                    delay: 4000
                });
            } else {
                const url = `https://www.ebay.com/itm/${response}`;
                newTab.opener = null;
                newTab.referrer = null;
                newTab.location = url;
            }
        });
    }
});
 
