$(document).ready(function() {
    $("#product-table").on('click', '.check-all-toggle', function() {
        const checkAllToggle = $('.check-all-toggle');
        $("td .checkbox-row")
            .each(function() {
                $(this).prop('checked', checkAllToggle.prop('checked'));
            });
    });

    $("#product-table").on('click', '.btn-batch-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const colPartNumber = columnsToCopy.partNumber;
        const colStepcode = columnsToCopy.stepcode;
        const colSpeed = columnsToCopy.speed;
        const colNumbercores = columnsToCopy.numbercores;
        const colCodename = columnsToCopy.codename;
        const colTdp = columnsToCopy.tdp;
        const colCache = columnsToCopy.cache;
        const colSocket_support = columnsToCopy.socket_support;
        const colGrade = columnsToCopy.grade;
        
        
        
        
        const isPartNumberBlank = rowData[colPartNumber] === undefined || rowData[colPartNumber].trim() === '';
        const isStepcodeBlank = rowData[colStepcode] === undefined ||
              rowData[colStepcode].trim() === '';
              const isSpeedBlank = rowData[colSpeed] === undefined || rowData[colSpeed].trim() === '';
              const isNumbercoresBlank = rowData[colNumbercores] === undefined || rowData[colNumbercores].trim() === '';
              const isCodenameBlank = rowData[colCodename] === undefined || rowData[colCodename].trim() === '';
              const isTdpBlank = rowData[colTdp] === undefined || rowData[colTdp].trim() === '';
              const isCacheBlank = rowData[colCache] === undefined || rowData[colCache].trim() === '';
              const isSocket_supportBlank = rowData[colSocket_support] === undefined || rowData[colSocket_support].trim() === '';
              const isGradeBlank = rowData[colGrade] === undefined || rowData[colGrade].trim() === '';
        

        
        
        const partNumber = isPartNumberBlank ? '' : `   ${rowData[colPartNumber].trim()}`;
        const Stepcode = isStepcodeBlank ? '' : `   ${rowData[colStepcode].trim()}`;
        const Speed = isSpeedBlank ? '' : `   ${rowData[colSpeed].trim()}`;
        const Numbercores = isNumbercoresBlank ? '' : `   ${rowData[colNumbercores].trim()}`;
        const Codename = isCodenameBlank ? '' : ` - ${rowData[colCodename].trim()}`;
        const Tdp = isTdpBlank ? '' : `   ${rowData[colTdp].trim()}`;
        const Cache = isCacheBlank ? '' : `   ${rowData[colCache].trim()}`;
        const Socket_support = isSocket_supportBlank ? '' : `   ${rowData[colSocket_support].trim()}`;
        const Grade = isGradeBlank ? '' : `  ${rowData[colGrade].trim()}`;

        const copiedData = `${partNumber}${Stepcode}` +
              `${Speed}${Numbercores}-${Codename}${Tdp}${Cache}${Socket_support}${Grade}`;

            rows.push(copiedData);
        });

        const text = rows.join('\n');
        $('.textarea-copy').val('');
        $('.textarea-copy').val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    $("#product-table").on('click', '.btn-par-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.partNumber);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    $("#product-table").on('click', '.btn-stepcode-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.stepcode);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    $("#product-table").on('click', '.btn-speed-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.speed);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    $("#product-table").on('click', '.btn-number_of_cores-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.numbercores);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    $("#product-table").on('click', '.btn-codename-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.codename);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    $("#product-table").on('click', '.btn-thermal_design_power-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.tdp);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    $("#product-table").on('click', '.btn-cpu_cache-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.cache);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    $("#product-table").on('click', '.btn-sockets_supported-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.socket_support);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    $("#product-table").on('click', '.btn-grade-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.grade);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    function getCopyText(index) {console.log()
        let rows = [];
        
        $("td .checkbox-row:checked").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const isCellBlank = rowData[index] !== undefined && rowData[index].trim().length === 0;

            if (!isCellBlank) rows.push(rowData[index].trim());
        });

        return rows.join('\n');
    }
});
