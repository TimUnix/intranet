const errorSkuEmpty = 0;
const errorPriceEmpty = 1;

let baseUrl;

$(document).ready(function(){
    let bannervalue;
    baseUrl = $('#listingScript').data('baseUrl');
    
    $('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);

    $.get(`${baseUrl}index.php/task/load_filters`, function(response) {
        $('.filters-container').html($(response).find('.filters-modal-group'));
        $('.filters').select2();
    }).then(function() { loadListing(); });

	$("#submit").click(function(event){
        if($("#sku").val() == ""){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku').focus();
        } else if (($('#request').val() == 1 || $('#request').val() == 3) && $('#price').val() === '') {
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);
            $('#price').focus();
        } else {
            event.preventDefault();
            
            if($("#bannerone").is(":checked")){
                bannervalue = "YES";

            }else if($("#bannertwo").is(":checked")){
                bannervalue = "NO";
            }

            $('#items').html(`<tr class="odd"><td valign="top" colspan="100%" class="datatables_empty">${spinner}</td></tr>`);
            var channelval = $("#channel").val();
            var channelvalebay = $("#newchannel").val();
            if(typeof(channelval) != "undefined" && channelval !== null) {
                channelvalue = channelval;
            }else if(typeof(channelvalebay) != "undefined" && channelvalebay !== null){
                channelvalue = channelvalebay;
            }

            var datas = {
                'sku' : $("#sku").val(),
                'channel': channelvalue,
                'shipping': $("#shipping").val(),
                'weight': $("#weight").val(),
                'price': $("#price").val(),
                'lot': $('#lot').val(),
                'moved': $('#moved').val(),
                'banner' : bannervalue,
                'notes' : $("#notes").val(),
                'request': $("#request").val(),
                ajax : '1'
            };
            $.ajax({
                url : `${baseUrl}index.php/task/request/create_listing`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == errorSkuEmpty) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else if (response == errorPriceEmpty) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);

                        $('#price').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else {
                        const refreshUrl = window.location.href.split('?')[0];
                        window.location.href = refreshUrl;
                    }
                }
            });
        }
    });

	$('#doneSwitch').change(function(){
        loadListing();
    });

    $('#listingsearch').click(function(){
		loadListing();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();
        //loadListing();
    });

    $('#searchColumn').change(function() {
        if ($(this).val() !== 'sku') {
            $('#searchSuggestions').html('');
        }
    });

    $('#sku').on('input', function(event) {
        const search = $(this).val();
        
        if (search.trim() !== '') {
            const data = { 'search': search };
            $.get(`${baseUrl}index.php/task/search_sku_suggestions`, data, function(response) {
                if (response.data.status === 200) {
                    $('#skuSuggestions').html('');
                    response.data.suggestions.forEach(function(suggestion) {
                        const sku = suggestion.par.replaceAll('-', '');
                        $('#skuSuggestions').append(`<option>${sku}</option>`);
                    });
                }
            });
        }
    });

    $('#itemsearch').on('input', function(event) {
        const search = $(this).val();
        const searchColumn = $('#searchColumn').val();
        
        if (searchColumn  === 'sku' && search.trim() !== '') {
            const data = { 'search': search };
            $.get(`${baseUrl}index.php/task/search_sku_suggestions`, data, function(response) {
                if (response.data.status === 200) {
                    $('#searchSuggestions').html('');
                    response.data.suggestions.forEach(function(suggestion) {
                        const sku = suggestion.par.replaceAll('-', '');
                        $('#searchSuggestions').append(`<option>${sku}</option>`);
                    });
                }
            });
        }
    });

    $('#exportCSV').click(function() {
        const orderBy = $('#orderByitem').val();
        const sortOrder = $('#sortOrderitem').val();
        
        
        
        var data = {
            'itemsearch': $("#skusearch").val(),
            'historysearch': $("#searchHistory").val(),
            'order_by': orderBy,
            'sort_order': sortOrder
        };

        const url = `${baseUrl}index.php/memory/export_csv`;

        $.post(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                var filename = `memory_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
            }
        });
    });

    $('#onlyMeSwitch').change(function() {
        if ($(this).prop('checked')) {
            $('#filterUsers').val('').trigger('change');
            $('#filterUsers').prop('disabled', true);
            loadListing();
        } else {
            $('#filterUsers').prop('disabled', false);
            loadListing();
        }
    });

    $('#filterUsers').on('change', function() { loadListing(); });
    
    $('#clearFilters').click(function() {
        $('.filters-modal').val('').trigger('change');
        loadListing();
    });
    
    if (channels === null || channels === undefined) {
        $.get(`${baseUrl}index.php/task/get_channels`, function(response) {
            channels = response;
        });
    }

    if (request_actions === null || request_actions === undefined) {
        $.get(`${baseUrl}index.php/task/get_actions`, function(response) {
            request_actions = response;
        });
    }

    $('#orderBy').change(function() {
        loadListing();
    });

    $('#sortOrder').change(function() {
        loadListing();
    });

    $('#applyFilters').click(function() {
        loadListing();
    });

    $('#request').change(function() {
        if ($(this).val() == 4) {
            $('#moved-form').removeClass('d-none');
        } else {
            $('#moved-form').addClass('d-none');
        }
    });
    
    const params = new URLSearchParams(window.location.search);
    const referred = params.get('referred');

    const isNewListingRequested = referred !== null && referred == 1;

    if (isNewListingRequested) {
        $('#myModal').modal('show');
    }

    $('#myModal').on('hidden.bs.modal', function() {
        clearModal();
    });

    function clearModal() {
        $('#success').html('');
        $('#sku').val('');
        $('#shipping').val('Calculated');
        $('#channel').find(':selected').prop('selected', false);
        $('#channel').find('[value="5"]').prop('selected', true);
        $('#price').val('');
        $('#notes').val('');
        $('#lot').val('1');
    }
});

/*function loadListing() {
    const orderBy = $('#orderBy').val();
    const sortOrder = $('#sortOrder').val();
    const searchColumn = $('#searchColumn').val();
    const filterChannels = $('#filterChannels').select2('data').map(function(channel) {
        return channel.element.value;
    }).join(',');
    const filterActions = $('#filterActions').select2('data').map(function(action) {
        return action.element.value;
    }).join(',');
    const filterBanner = $('#filterBanner').val();
    const filterUsers = $('#filterUsers').select2('data').map(function(user) {
        return user.element.value;
    }).join(',');

    var data = {
        'itemsearch' : $("#itemsearch").val(),
        'order_by': orderBy,
        'sort_order': sortOrder,
        'channels': filterChannels,
        'actions': filterActions,
        'banner': filterBanner,
        'users': filterUsers,
        'search_column': searchColumn,
        'is_done_shown': $('#doneSwitch').prop('checked'),
        'is_only_me_enabled': $('#onlyMeSwitch').prop('checked')
    };

    const url = `${baseUrl}index.php/task/load_listing`;
    const countUrl = `${baseUrl}index.php/task/load_listing_count`;

    $.post(url, data, function(response) {
        $('#items').html(response);
        paginate();
    });

    $.post(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
        }
    });
}*/

