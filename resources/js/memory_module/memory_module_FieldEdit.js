let channels;
let request_actions;

$(document).ready(function() {
    let currentCellNewPrice;
    let currentCellNotes;
    let currentPar;
    let parval;
    let stepval;
    
    const baseUrl = $('#listingFieldEditScript').data('baseUrl');

    $('#items').click('.new-price', function(event) {
        const newPrice = $(event.target).closest('.new-price');

        if (!newPrice) return;

        const value = newPrice.data('value');

        addEditableField(newPrice, value, 'new-price');
    });

    $('#items').click('.notes', function(event) {
        const notes = $(event.target).closest('.notes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableFieldnote(notes, value, 'notes');
    });


    ///
    $('#items').click('.datetimes', function(event) {
        const notes = $(event.target).closest('.datetimes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'datetimes');
    });
    /////////////partnumber
    $('#load_memory').click('.sku', function(event) {
        if(event.target.className == "sku"){
            const notes = $(event.target).closest('.sku');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel skubtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
        }else if(event.target.className == "fa fa-edit skupenbtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
        }
    });
    function addEditableFieldsku(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.sku-editable .sku-cancel', function(event) {
        const btnCancel = $(event.target).closest('.sku-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.sku-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('sku-editable');
        notesCell.addClass('sku');
    });
    $('#load_memory').click('.sku-editable .sku-apply', function(event) {
        const btnApply = $(event.target).closest('.sku-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.sku-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.sku-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('sku-editable');
        notes.addClass('sku');
    
        

        var datas = {
            'data': {'sku': parval, 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "sku"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////partnumber


    /////////////stepcode
    $('#load_memory').click('.barcode', function(event) {
        if(event.target.className == "barcode"){
            const notes = $(event.target).closest('.barcode');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel barcodetn"){
            const notes = $(event.target).closest('.barcode');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbarcode(notes, value, 'barcode');
        }else if(event.target.className == "fa fa-edit barcodepenbtn"){
            const notes = $(event.target).closest('.barcode');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbarcode(notes, value, 'barcode');
        }
    });
    function addEditableFieldbarcode(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        stepval = value;
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.barcode-editable .barcode-cancel', function(event) {
        const btnCancel = $(event.target).closest('.barcode-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.barcode-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel barcodebtn"><i class="fa fa-edit barcodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('barcode-editable');
        notesCell.addClass('barcode');
    });
    $('#load_memory').click('.barcode-editable .barcode-apply', function(event) {
        const btnApply = $(event.target).closest('.barcode-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.barcode-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.barcode-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel barcodebtn"><i class="fa fa-edit barcodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('barcode-editable');
        notes.addClass('barcode');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': stepval, 'data' :textField.val(), 'type' : "barcode"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////stepcode


    /////////////speed
    $('#load_memory').click('.brand', function(event) {
        if(event.target.className == "brand"){
            const notes = $(event.target).closest('.brand');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel brandbtn"){
            const notes = $(event.target).closest('.brand');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbrand(notes, value, 'brand');
        }else if(event.target.className == "fa fa-edit brandpenbtn"){
            const notes = $(event.target).closest('.brand');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbrand(notes, value, 'brand');
        }
    });
    function addEditableFieldbrand(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.brand-editable .brand-cancel', function(event) {
        const btnCancel = $(event.target).closest('.brand-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.brand-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('brand-editable');
        notesCell.addClass('brand');
    });
    $('#load_memory').click('.brand-editable .brand-apply', function(event) {
        const btnApply = $(event.target).closest('.brand-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.brand-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.brand-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('brand-editable');
        notes.addClass('brand');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "brand"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////speed


    /////////////number_of_cores
    $('#load_memory').click('.memory_size', function(event) {
        if(event.target.className == "memory_size"){
            const notes = $(event.target).closest('.memory_size');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel memory_sizebtn"){
            const notes = $(event.target).closest('.memory_size');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmemory_size(notes, value, 'memory_size');
        }else if(event.target.className == "fa fa-edit memory_sizepenbtn"){
            const notes = $(event.target).closest('.memory_size');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmemory_size(notes, value, 'memory_size');
        }
    });
    function addEditableFieldmemory_size(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.memory_size-editable .memory_size-cancel', function(event) {
        const btnCancel = $(event.target).closest('.memory_size-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.memory_size-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memory_sizebtn"><i class="fa fa-edit memory_sizepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('memory_size-editable');
        notesCell.addClass('memory_size');
    });
    $('#load_memory').click('.memory_size-editable .memory_size-apply', function(event) {
        const btnApply = $(event.target).closest('.memory_size-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.memory_size-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.memory_size-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memory_sizebtn"><i class="fa fa-edit memory_sizepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('memory_size-editable');
        notes.addClass('memory_size');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "memory_size"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////number_of_cores


    /////////////codename
    $('#load_memory').click('.memory_family', function(event) {
        if(event.target.className == "memory_family"){
            const notes = $(event.target).closest('.memory_family');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel memory_familybtn"){
            const notes = $(event.target).closest('.memory_family');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmemory_family(notes, value, 'memory_family');
        }else if(event.target.className == "fa fa-edit memory_familypenbtn"){
            const notes = $(event.target).closest('.memory_family');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmemory_family(notes, value, 'memory_family');
        }
    });
    function addEditableFieldmemory_family(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.memory_family-editable .memory_family-cancel', function(event) {
        const btnCancel = $(event.target).closest('.memory_family-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.memory_family-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memory_familybtn"><i class="fa fa-edit memory_familypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('memory_family-editable');
        notesCell.addClass('memory_family');
    });
    $('#load_memory').click('.memory_family-editable .memory_family-apply', function(event) {
        const btnApply = $(event.target).closest('.memory_family-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.memory_family-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.memory_family-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memory_familybtn"><i class="fa fa-edit memory_familypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('memory_family-editable');
        notes.addClass('memory_family');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "memory_family"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////codename


    /////////////thermal_design_power
    $('#load_memory').click('.memory_type', function(event) {
        if(event.target.className == "memory_type"){
            const notes = $(event.target).closest('.memory_type');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel memory_typebtn"){
            const notes = $(event.target).closest('.memory_type');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmemory_type(notes, value, 'memory_type');
        }else if(event.target.className == "fa fa-edit memory_typepenbtn"){
            const notes = $(event.target).closest('.memory_type');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmemory_type(notes, value, 'memory_type');
        }
    });
    function addEditableFieldmemory_type(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.memory_type-editable .memory_type-cancel', function(event) {
        const btnCancel = $(event.target).closest('.memory_type-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.memory_type-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memory_typebtn"><i class="fa fa-edit memory_typepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('memory_type-editable');
        notesCell.addClass('memory_type');
    });
    $('#load_memory').click('.memory_type-editable .memory_type-apply', function(event) {
        const btnApply = $(event.target).closest('.memory_type-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.memory_type-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.memory_type-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memory_typebtn"><i class="fa fa-edit memory_typepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('memory_type-editable');
        notes.addClass('memory_type');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "memory_type"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////thermal_design_power


    /////////////cpu_cache
    $('#load_memory').click('.speed', function(event) {
        if(event.target.className == "speed"){
            const notes = $(event.target).closest('.speed');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel speedbtn"){
            const notes = $(event.target).closest('.speed');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldspeed(notes, value, 'speed');
        }else if(event.target.className == "fa fa-edit speedpenbtn"){
            const notes = $(event.target).closest('.speed');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldspeed(notes, value, 'speed');
        }
    });
    function addEditableFieldspeed(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.speed-editable .speed-cancel', function(event) {
        const btnCancel = $(event.target).closest('.speed-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.speed-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel speedbtn"><i class="fa fa-edit speedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('speed-editable');
        notesCell.addClass('speed');
    });
    $('#load_memory').click('.speed-editable .speed-apply', function(event) {
        const btnApply = $(event.target).closest('.speed-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.speed-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.speed-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel speedbtn"><i class="fa fa-edit speedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('speed-editable');
        notes.addClass('speed');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "speed"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////cpu_cache


    /////////////sockets_supported
    $('#load_memory').click('.registry', function(event) {
        if(event.target.className == "registry"){
            const notes = $(event.target).closest('.registry');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel registrybtn"){
            const notes = $(event.target).closest('.registry');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldregistry(notes, value, 'registry');
        }else if(event.target.className == "fa fa-edit registrypenbtn"){
            const notes = $(event.target).closest('.registry');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldregistry(notes, value, 'registry');
        }
    });
    function addEditableFieldregistry(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.registry-editable .registry-cancel', function(event) {
        const btnCancel = $(event.target).closest('.registry-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.registry-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel registrybtn"><i class="fa fa-edit registrypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('registry-editable');
        notesCell.addClass('registry');
    });
    $('#load_memory').click('.registry-editable .registry-apply', function(event) {
        const btnApply = $(event.target).closest('.registry-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.registry-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.registry-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel registrybtn"><i class="fa fa-edit registrypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('registry-editable');
        notes.addClass('registry');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "registry"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////sockets_supported


    /////////////grade
    $('#load_memory').click('.cycles', function(event) {
        if(event.target.className == "cycles"){
            const notes = $(event.target).closest('.cycles');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel cyclesbtn"){
            const notes = $(event.target).closest('.grade');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcycles(notes, value, 'cycles');
        }else if(event.target.className == "fa fa-edit cyclespenbtn"){
            const notes = $(event.target).closest('.cycles');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcycles(notes, value, 'cycles');
        }
    });
    function addEditableFieldcycles(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.cycles-editable .cycles-cancel', function(event) {
        const btnCancel = $(event.target).closest('.cycles-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.cycles-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel cyclesbtn"><i class="fa fa-edit cyclespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('cycles-editable');
        notesCell.addClass('cycles');
    });
    $('#load_memory').click('.cycles-editable .cycles-apply', function(event) {
        const btnApply = $(event.target).closest('.cycles-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.cycles-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.cycles-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel cyclesbtn"><i class="fa fa-edit cyclespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('cycles-editable');
        notes.addClass('cycles');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "cycles"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////grade


    /////////////grade
    $('#load_memory').click('.con', function(event) {
        if(event.target.className == "con"){
            const notes = $(event.target).closest('.con');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel conbtn"){
            const notes = $(event.target).closest('.con');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcon(notes, value, 'con');
        }else if(event.target.className == "fa fa-edit conpenbtn"){
            const notes = $(event.target).closest('.con');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcon(notes, value, 'con');
        }
    });
    function addEditableFieldcon(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.con-editable .con-cancel', function(event) {
        const btnCancel = $(event.target).closest('.con-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.con-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('con-editable');
        notesCell.addClass('con');
    });
    $('#load_memory').click('.con-editable .con-apply', function(event) {
        const btnApply = $(event.target).closest('.con-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.con-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.con-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('con-editable');
        notes.addClass('con');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "con"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////grade


    /////////////grade
    $('#load_memory').click('.rank', function(event) {
        if(event.target.className == "rank"){
            const notes = $(event.target).closest('.rank');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel rankbtn"){
            const notes = $(event.target).closest('.rank');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldrank(notes, value, 'rank');
        }else if(event.target.className == "fa fa-edit rankpenbtn"){
            const notes = $(event.target).closest('.rank');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldrank(notes, value, 'rank');
        }
    });
    function addEditableFieldrank(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.rank-editable .rank-cancel', function(event) {
        const btnCancel = $(event.target).closest('.rank-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.rank-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rankbtn"><i class="fa fa-edit rankpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('rank-editable');
        notesCell.addClass('rank');
    });
    $('#load_memory').click('.rank-editable .rank-apply', function(event) {
        const btnApply = $(event.target).closest('.rank-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.rank-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.rank-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rankbtn"><i class="fa fa-edit rankpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('rank-editable');
        notes.addClass('rank');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "rank"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////grade


    /////////////grade
    $('#load_memory').click('.voltage', function(event) {
        if(event.target.className == "voltage"){
            const notes = $(event.target).closest('.voltage');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel voltagebtn"){
            const notes = $(event.target).closest('.voltage');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldvoltage(notes, value, 'voltage');
        }else if(event.target.className == "fa fa-edit voltagepenbtn"){
            const notes = $(event.target).closest('.voltage');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldvoltage(notes, value, 'voltage');
        }
    });
    function addEditableFieldvoltage(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.rank-editable .rank-cancel', function(event) {
        const btnCancel = $(event.target).closest('.voltage-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.voltage-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel voltagebtn"><i class="fa fa-edit voltagepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('voltage-editable');
        notesCell.addClass('voltage');
    });
    $('#load_memory').click('.voltage-editable .voltage-apply', function(event) {
        const btnApply = $(event.target).closest('.voltage-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.voltage-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.voltage-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel voltagebtn"><i class="fa fa-edit voltagepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('voltage-editable');
        notes.addClass('voltage');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "voltage"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////grade


    /////////////grade
    $('#load_memory').click('.notes', function(event) {
        if(event.target.className == "notes"){
            const notes = $(event.target).closest('.notes');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel notesbtn"){
            const notes = $(event.target).closest('.notes');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldnotes(notes, value, 'notes');
        }else if(event.target.className == "fa fa-edit notespenbtn"){
            const notes = $(event.target).closest('.notes');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldnotes(notes, value, 'notes');
        }
    });
    function addEditableFieldnotes(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_memory').click('.notes-editable .notes-cancel', function(event) {
        const btnCancel = $(event.target).closest('.notes-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.notes-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel notesbtn"><i class="fa fa-edit notespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('notes-editable');
        notesCell.addClass('notes');
    });
    $('#load_memory').click('.notes-editable .notes-apply', function(event) {
        const btnApply = $(event.target).closest('.notes-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.notes-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const barcode = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.notes-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel notesbtn"><i class="fa fa-edit notespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('notes-editable');
        notes.addClass('notes');
    
        

        var datas = {
            'data': {'sku': sku.text().trim(), 'barcode': barcode.text().trim(), 'data' :textField.val(), 'type' : "notes"}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;*/
    });
    ////////////grade


    ////////////delete
    $('#load_memory').click('.delete', function(event) {
        if(event.target.className == "delete"){
            const notes = $(event.target).closest('.delete');
    
            if (!notes) return;
    
            const par = notes.closest('tr').find('.sku');
            const stepcode = notes.closest('tr').find('.barcode');
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel deletebtn"){
            const notes = $(event.target).closest('.delete');
            

            if (!notes) return;
    
            const value = notes.data('value');
            const par = notes.closest('tr').find('.sku');
            const stepcode = notes.closest('tr').find('.barcode');
    
            addEditableFielddelete(notes, stepcode.text().trim(), par.text().trim(), 'delete');
        }else if(event.target.className == "fa fa-trash deletepenbtn"){
            const notes = $(event.target).closest('.delete');
            

            if (!notes) return;
    
            const value = notes.data('value');
            const par = notes.closest('tr').find('.sku');
            const stepcode = notes.closest('tr').find('.barcode');
    
            addEditableFielddelete(notes, stepcode.text().trim(), par.text().trim(), 'delete');
        }
    });
    function addEditableFielddelete(cell, stepcode, par, className) {
        
        ///console.log(stepcode)
        //console.log(par)


        const orderby = $("#orderByitem").val();
        const sortorder = $("#sortOrderitem").val();
        const skutext = $("#skusearch").val();


        
        var datas = {
            'data': {'sku': par, 'barcode': stepcode, 'orderby' : orderby, 'sortorder' : sortorder, 'skutext' : skutext}
        };
        $.ajax({
            url : `${baseUrl}index.php/memory/request/delete_memory`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load_memory").html(msg) 
                
                ///console.log(msg);
            }
        });
        /*parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);*/
        
    }
    $('#load_cpu').click('.par-editable .par-cancel', function(event) {
        const btnCancel = $(event.target).closest('.par-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.par-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-danger draftid-cancel parbtn"><i class="fa fa-pen parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('par-editable');
        notesCell.addClass('par');
    });
    $('#load_cpu').click('.par-editable .par-apply', function(event) {
        const btnApply = $(event.target).closest('.par-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.par-editable');
        const sku = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.par-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-danger draftid-cancel parbtn"><i class="fa fa-pen parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('par-editable');
        notes.addClass('par');
    
        

        var datas = {
            'data': {'partnumber': parval, 'stepcode': stepcode.text().trim(), 'data' :textField.val(), 'type' : "part"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////partnumber

});
