$(document).ready(function() {
    $("#product-table").on('click', '.btn-copy-row', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const rowDOM = copyButton.closest('tr');
        const rowData = [...rowDOM[0].cells].map(function(cell) {
            return cell.textContent;
        });

        const colPartNumber = columnsToCopy.partNumber;
        const colStepcode = columnsToCopy.stepcode;
        const colSpeed = columnsToCopy.speed;
        const colNumbercores = columnsToCopy.numbercores;
        const colCodename = columnsToCopy.codename;
        const colTdp = columnsToCopy.tdp;
        const colCache = columnsToCopy.cache;
        const colSocket_support = columnsToCopy.socket_support;
        const colGrade = columnsToCopy.grade;
        
        
        
        
        const isPartNumberBlank = rowData[colPartNumber] === undefined || rowData[colPartNumber].trim() === '';
        const isStepcodeBlank = rowData[colStepcode] === undefined ||
              rowData[colStepcode].trim() === '';
              const isSpeedBlank = rowData[colSpeed] === undefined || rowData[colSpeed].trim() === '';
              const isNumbercoresBlank = rowData[colNumbercores] === undefined || rowData[colNumbercores].trim() === '';
              const isCodenameBlank = rowData[colCodename] === undefined || rowData[colCodename].trim() === '';
              const isTdpBlank = rowData[colTdp] === undefined || rowData[colTdp].trim() === '';
              const isCacheBlank = rowData[colCache] === undefined || rowData[colCache].trim() === '';
              const isSocket_supportBlank = rowData[colSocket_support] === undefined || rowData[colSocket_support].trim() === '';
              const isGradeBlank = rowData[colGrade] === undefined || rowData[colGrade].trim() === '';
        

        
        
        const partNumber = isPartNumberBlank ? '' : `   ${rowData[colPartNumber].trim()}`;
        const Stepcode = isStepcodeBlank ? '' : `   ${rowData[colStepcode].trim()}`;
        const Speed = isSpeedBlank ? '' : `   ${rowData[colSpeed].trim()}`;
        const Numbercores = isNumbercoresBlank ? '' : `   ${rowData[colNumbercores].trim()}`;
        const Codename = isCodenameBlank ? '' : ` - ${rowData[colCodename].trim()}`;
        const Tdp = isTdpBlank ? '' : `   ${rowData[colTdp].trim()}`;
        const Cache = isCacheBlank ? '' : `   ${rowData[colCache].trim()}`;
        const Socket_support = isSocket_supportBlank ? '' : `   ${rowData[colSocket_support].trim()}`;
        const Grade = isGradeBlank ? '' : `  ${rowData[colGrade].trim()}`;

        const copiedData = `${partNumber}${Stepcode}` +
              `${Speed}${Numbercores}-${Codename}${Tdp}${Cache}${Socket_support}${Grade}`;

        $('.textarea-copy').val('');
        $('.textarea-copy').val(copiedData);
        
        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = copiedData;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();
    });
});
