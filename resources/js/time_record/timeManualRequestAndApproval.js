$(document).ready(function() {
    const baseUrl = $('.time-manual-script').data('baseUrl');

    let miniTimeInterval;

    $('#applyManualModal').on('show.bs.modal', function() {
        miniTimeInterval = setInterval(function() {
            const date = new Date();
            const options = {
                day: 'numeric',
                month: 'long',
                weekday: 'long',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric',
                timeZone: 'America/Los_Angeles',
                timeZoneName: 'short',
            };
            const dateString = date.toLocaleString('en-US', options);
            const displayTime = dateString.split(', ')[1];
            $('.apply-manual-timer').html(`${dateString}`);
        }, 1000);
    });

    $('#applyManualModal').on('hidden.bs.modal', function() {
        if (miniTimeInterval !== null || miniTimeInterval !== undefined)
            clearInterval(miniTimeInterval);
    });

    $('#manualRequestForm').on('submit', function(event) {
        event.preventDefault();

        const requestType = $('#manualRequestType').val();
        const manager = $('#manualRequestManager').val();

        const data = { 'type': requestType, 'manager': manager };

        $.post(`${baseUrl}index.php/time_record/manual_request`, data, function(response) {
            $('#applyManualModal').modal('hide');
            $.get(`${baseUrl}index.php/time_record/load_request`, function(response) {
                        $('#request_table_body').html(response);
                    
            });
            if (response.is_error) {
                $(document).Toasts('create', {
                    title: requestType === 'time_in' ? 'Time In Failed' : 'Time Out Failed',
                    body: response.error,
                    fade: true,
                    delay: 15000,
                    autohide: true,
                    autoremove: true
                });

                return;
            }

            if (requestType === 'time_in') {
                $('#insert_success').modal('show');
                $('#okay_modal').on('click', function() { $('#insert_success').modal('hide'); });
            } else {
                $('#insert_success_time_out').modal('show');
                $('#okay_modal_time_out').on('click', function() { $('#insert_success_time_out').modal('hide'); });
            }
        });
    });

    $('#manualRequestType').on('change', function() {
        const requestType = $(this).val();
        $('#manualRequestManager').prop('disabled', requestType === 'time_out');
    });

    $('.btn-manual-approval').on('click', function() {
        $('#approver_table, #request_table, #schedule_table, #time_table, #own_time_table, #eod_table').hide();

        const manualApprovalContainer = $('.manual-approval-container');

        if (manualApprovalContainer.hasClass('d-none')) {
            manualApprovalContainer.removeClass('d-none');
            manualApprovalContainer.show();
        } else {
            manualApprovalContainer.show();
        }

        loadRequests();
    });

    $('.manual-request-employee-filter').on('change', loadRequests);
    $('.manual-request-date-filter').on('change', loadRequests);

    let rowInReview = null;
    let manualApprovaData = {};

    $('.table-manual-approval').on('click', '.btn-deny-manual', function() {
        rowInReview = $(this).closest('tr');
        manualApprovalData = getManualRowData(this);
        manualApprovalData['time_request'] = 'Denied';
    });

    $('.table-manual-approval').on('click', '.btn-approve-manual', function() {
        rowInReview = $(this).closest('tr');
        manualApprovalData = getManualRowData(this);
        manualApprovalData['time_request'] = 'Approved';
    });

    $('.btn-approve-manual-submit').on('click', function() {
        manualApprovalData['notes'] = $('.approve-manual-notes').val();

        $.post(`${baseUrl}index.php/time_record/record_manual_approval`, manualApprovalData, function(response) {
            if (response.is_error) {
                $(document).Toasts('create', {
                    title: 'Approval Error',
                    body: response.error,
                    fade: true,
                    autohide: true,
                    autoremove: true,
                    delay: 15000
                });
            }
            
            $('.approve-manual-notes').val('');

            rowInReview.remove();
            rowInReview = null;
            manualApprovalData = {};
        });
    });

    $('.btn-clear-manual-approval-filters').on('click', function() {
        $('.manual-request-employee-filter').val('');
        $('.manual-request-date-filter').val('');
        
        loadRequests();
    });

    function getManualRowData(element) {
        const button = $(element);
        const row = button.closest('tr');

        const cellDateIn = row.find('.manual-date-in');
        const cellDateOut = row.find('.manual-date-out');

        const employee = row.find('.manual-employee').data('email');
        const dateIn = cellDateIn.data('dateIn');
        const timeIn = cellDateIn.data('timeIn');
        const dateOut = cellDateOut.data('dateOut');
        const timeOut = cellDateOut.data('timeOut');

        return {
            'employee': employee,
            'date_in': dateIn,
            'time_in': timeIn,
            'date_out': dateOut,
            'time_out': timeOut
        };
    }

    function loadRequests() {
        const employeeFilter = $('.manual-request-employee-filter').val();
        const dateFilter = $('.manual-request-date-filter').val();
        
        const data = {
            'employee': employeeFilter,
            'date': dateFilter
        };
        
        $.post(`${baseUrl}index.php/time_record/manual_requests`, data, function(response) {
            $('.table-manual-approval-body').html(response);
        });
    }
});
