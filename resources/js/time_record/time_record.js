let baseUrl;



$(document).ready(function() {
	let itemsOffset = 0;
	baseUrl = $('#timeRecordScript').data('baseUrl');
	syncStickyHeader();

	const parameters = new URLSearchParams(window.location.search);
	const filter_employee = parameters.get('filter_employee');
	const filter_date = parameters.get('filter_date');
	
	var time_in_display = document.getElementById("date_now");
	var day_in_display = document.getElementById("day_now");
	
	function refreshTime() {
	  var option = {year: 'numeric', month: 'long', day: 'numeric'};
	  var dateString = new Date().toLocaleString("en-US", {timeZone: "America/Los_Angeles", year: 'numeric', month: 'long', 
	  	timeZoneName: 'short', day: 'numeric', weekday: 'long', hour: 'numeric', minute: 'numeric', second: 'numeric' });
	 
	  var formattedString = dateString.replace(", ", " ");
	  var get_hours = formattedString.split(" ");

	  var weekday = get_hours[0];
	  var month = get_hours[1];
	  var days = get_hours[2];
	  var year = get_hours[3];
	  var hours = get_hours[4];
	  var meridiem = get_hours[5];
	  var time_zone = get_hours[6];
	  var joined_hours_meridiem = hours.concat(' ', meridiem);
	  var remove_comma = year.replace(",", " ");
	  var day = weekday + ', ' + month + ' ' + days + ' ' + remove_comma + ' ' + time_zone; 
	  time_in_display.innerHTML = joined_hours_meridiem;
	  day_in_display.innerHTML = day;
	}
	
	setInterval(refreshTime, 1000);

	$(`#schedule_table, #time_table, #request_table, #approver_table, #eod_table, .manual-approval-container, #fullname`).hide();

	var colspan = $('#own-time-table > thead > tr').children().length;
	$('#time_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
	$.get(`${baseUrl}index.php/time_record/load_item`, function(response) {
			$('#own_filter_date').val('');
            $('#own_time_table_body').html(response);
			$('#owntable').show();
			const tableWidth = $('#own_time_table_body').width();
                $('.scroller-content').width(tableWidth); 
                $('.sticky-table').width(tableWidth);
                syncStickyHeader(); 
                    
    });

	$('#time_in').click(function(){
		var dateString = new Date().toLocaleString("en-US", {timeZone: "America/Los_Angeles"});
	    var time_in = dateString.replace(", ", " ");
		var status = 'Time In'
		var data = {
			//'time_in'	: time_in,
			'status'	: status,
			ajax		: '1'
		};

		$.ajax({
			url : `${baseUrl}index.php/time_record/insert_timeIn`,
            type : "post",
            data : data,
            success : function(response){

            	if(response.no_exist_sched){
            		$('#sched_no_exist').modal('show');
            		$('#close_validate_sched_exist').click(function(){$('#sched_no_exist').modal('hide');});     	
            	}
            	else if(response.leave_whole_day_approved){
            		$('#leave_whole_day_approved').modal('show');
            		$('#close_validate_leave_whole_day').click(function(){$('#leave_whole_day_approved').modal('hide');});
            	}
            	else if(response.is_error){
            		$('#already_time_in').modal('show');
            		$('#close_validate_already_time_in').click(function(){$('#already_time_in').modal('hide');});
					
					
            	}else if(response.tito_approved){
            		$('#tito_approved').modal('show');
            		$('#close_validate_tito_approved').click(function(){$('#tito_approved').modal('hide');});
            	}else{
            		$('#insert_success').modal('show');
            		$('#okay_modal').click(function(){location.reload();});
            	}
            }         
		});
	});

	$('#admin_button').click(function(){

		
		$(`#schedule_table, #own_time_table, #remove_in_admin, #request_table, #approver_table, ` +
          `.manual-approval-container,#eod_table`).hide();
		$('#time_table, #for_admin, #clearFilters').show();
		
		$('#filter_employee').val(null).trigger('change');
		$('#filter_date').val('');
		
		var colspan = $('#time-table > thead > tr').children().length;
		$('#time_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
		$.get(`${baseUrl}index.php/time_record/load_employees`, function(response) {
	            $('#time_table_body').html(response);
				const tableWidth = $('#time_table_body').width();
                $('.scroller-content').width(tableWidth); 
                $('.sticky-table').width(tableWidth);
				
               
	    });
		
		$('#filter_employee').change(function(){
			$('#pdfFilter').val( $('#filter_employee').val());
			$('#pdfFilterdate').val( $('#filter_date').val());
			var data = {
				'filter_employee' : $('#filter_employee').val(),
				'filter_date' : $('#filter_date').val()
			};
			if($('#filter_employee').val() !== '' || $('#filter_date').val() !== ''){

				$.get(`${baseUrl}index.php/time_record/filter_employee`, data, function(response) {
				    $('#time_table_body').html(response);
				});
			}else{
				var colspan = $('#time-table > thead > tr').children().length;
				$('#time_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
				$.get(`${baseUrl}index.php/time_record/load_employees`, function(response) {
			            $('#time_table_body').html(response);
			    });
			}
		});

	});

	$('#filter_date').change(function(){
		$('#pdfFilter').val( $('#filter_employee').val());
		$('#pdfFilterdate').val( $('#filter_date').val());
	});

	$('#filter_employee, #filter_employee-approvalRequest').select2({
		width: 330,
		tags: true
	});
	$('#log_employee').select2({
		width: 360,
		tags: true
	});
	$('#filter_employee_eod').select2({
		width: 360,
		tags: true
	});

	$('#date_range').click(function(){
		$('#pdfFilter').val( $('#filter_employee').val());
		$('#pdfFilterdate').val( $('#filter_date').val());
		var data = {
			'filter_employee' : $('#filter_employee').val(),
			'filter_date' : $('#filter_date').val()
		};

		$.get(`${baseUrl}index.php/time_record/filter_employee`, data, function(response) {
	        $('#time_table_body').html(response);
	    });

	});

	$('#own_date_range').click(function(){
		if ($('#own_filter_date').val() == ''){
			return false;
		}else{
		var data = {
			'filter_date' : $('#own_filter_date').val()
		};

		$.get(`${baseUrl}index.php/time_record/filter_own_record`, data, function(response) {
	        $('#own_time_table_body').html(response);
	    });
		}
	});

	$('#my_record').click(function(){
		$(`#day_now, #date_now, #time_in, #time_out`).css('visibility', 'visible');
        

		$('#remove_in_admin, #own_time_table').show();

		$('#for_admin, #schedule_table, #time_table, #request_table, #approver_table, .manual-approval-container, #eod_table')
            .hide();


		var colspan = $('#own-time-table > thead > tr').children().length;
		$('#own_time_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
		$.get(`${baseUrl}index.php/time_record/load_item`, function(response) {
	            $('#own_time_table_body').html(response);        
	    });
	});

	$('#exportCSV').click(function() {
		var data = {
			'filter_employee' : $('#filter_employee').val(),
			ajax : '1'
		};

        const  url = `${baseUrl}index.php/time_record/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `timeRecord_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

	$('#PDFform').submit(function() {
		if (($('#filter_employee').val() == '') || ($('#filter_date').val() == '')){
		$('#fillup').modal('show');
        $('#close_fillup').click(function(){$('#fillup').modal('hide');});  
        return false;}
	});

	$('#ownPDFform').submit(function() {
		if ($('#own_filter_date').val() == ''){
		$('#fillupown').modal('show');
        $('#close_fillupown').click(function(){$('#fillupown').modal('hide');});  
        return false;}
	});


    $('#own_exportPDF').click(function(){
    	var data = {
			'filter_date' : $('#own_filter_date').val()
		};
	
		$.post(`${baseUrl}index.php/time_record/own_generate_pdf`, data, function(response) {
        	const printWindow = window.open("", "");
	        printWindow.document.write(response);
	        printWindow.document.close();
	        printWindow.focus();
	        
	        setTimeout(function() {
	            printWindow.print();
	            printWindow.close();
	        }, 500);
	    });
    });


	$('#filter_date, #filter_date-approvalRequest,#filter_date_eod, #own_filter_date').daterangepicker({

		"autoApply": true,
      locale: {
        format: 'YYYY-MM-DD'
      }
    });

    $('#time_logs').datetimepicker({
  		format: 'h:mm:ss A',
  		step: 60
    	
    });

    $('#date_logs, #date_to_modify, #date_to_request, #edit_start_leave_date, #edit_end_leave_date').datetimepicker({
    	format: 'YYYY-MM-DD'
    	
    });

    $('#time_out').click(function(){
    	$('#eod').modal('show');
    	$('#validate_eod').html('');

    });

    $('#logs_for_employees').click(function(){
    	var time = 'time_in';
    	$('#validate_logs').html('');
    	$('#log_employee').val(' ');
    	$('#log_status').val(time);
    	$('#time_logs input').val('');
    	$('#date_logs input').val('');
    	$('#date_to_modify input').val('');
    	$('#div_date_modify_logs').hide();
    	$('#finished_task_logs').val(' ');
    	$('#challenges_logs').val(' ');
    	$('#goals_logs').val(' ');
    	$('#div_date_logs').hide();
    	$('#div_task_logs').hide();
    	$('#div_challenges_logs').hide();
    	$('#div_goals_logs').hide();
    	$('#log_form').modal('show');
    	
    });

    $('#eod_submit').click(function(){
    	if($('#finished_task').val() == "" && $('#challenges').val()== "" && $('#tom_goal').val() == ""){
    		 $("#validate_eod").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input All Fields</h5>
                    </div>`);
    	}else{
    		var dateString = new Date().toLocaleString("en-US", {timeZone: "America/Los_Angeles"});
		    var time_out = dateString.replace(", ", " ");
	    	var status = 'Time Out';
	    	var data = {
	    		'finished_task' : $('#finished_task').val(),
	    		'challenges'	: $('#challenges').val(),
	    		'tom_goal'		: $('#tom_goal').val(),
	    		'status'		: status,
	    		'time_out'		: time_out

	    	};

	    	$.post(`${baseUrl}index.php/time_record/insert_eod`, data, function(response) {

	    		if(response.is_error){
	    			$("#validate_eod").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>You have already timed out</h5>
                    </div>`);
				}else if (response.is_restricted){
					$('#eod').modal('hide');
					$('#restricted').modal('show');
            		$('#close_restricted').click(function(){$('#restricted').modal('hide');});
				
	    		}else{
	    			$('#time_table_body').html(response);
		            $('#eod').modal('hide');
		            $('#insert_success_time_out').modal('show');
		            $('#okay_modal_time_out').click(function(){
		            	location.reload();
		            }); 
	    		}
		                   
		    });
    	}
    	
    });

    $('#log_status').change(function(){
    	
    	if($('#log_status').find(':selected').val() == 'time_out'){
    		$('#div_date_logs').show();
    		$('#div_date_modify_logs').show();
    		$('#div_task_logs').show();
    		$('#div_challenges_logs').show();
    		$('#div_goals_logs').show();
    		$('#div_date_modify_logs').show();
    	}

    	if($('#log_status').find(':selected').val() == 'time_in'){
    		$('#time_logs input').val('');
	    	$('#date_logs input').val('');
	    	$('#date_to_modify input').val('');
	    	$('#div_date_logs').hide();
    		$('#div_date_modify_logs').hide();
    		$('#div_task_logs').hide();
    		$('#div_challenges_logs').hide();
    		$('#div_goals_logs').hide();
    		$('#div_date_modify_logs').hide();
    	}
    });

    $('#logs_submit').click(function(){

	    var data = {
	    	'log_employee' : $('#log_employee').find(':selected').text(),
	    	'log_email'	   : $('#log_employee').find(':selected').val(),
	    	'log_status'	: $('#log_status').find(':selected').text(),
	    	'date_in_logs'	: $('#date_to_modify input').val(),
	    	'time_logs'		: $('#time_logs input').val(),
	    	'date_logs'		: $('#date_logs input').val(),
	    	'finished_task_logs' : $('#finished_task_logs').val(),
	    	'challenges_logs'	: $('#challenges_logs').val(),
	    	'goals_logs'		: $('#goals_logs').val()
	    };

	    $.get(`${baseUrl}index.php/time_record/insert_logs`, data, function(response) {

	    	if(response.is_error){
	    		$("#validate_logs").html(`<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>Please Input All Fields</h5></div>`);
    		 		
	    	}else if(response.no_exist_sched){
	    		$("#validate_logs").html(`<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>No time schedule for this employee</h5></div>`);
	    	}else if(response.validate_timeIn){
	    		$("#validate_logs").html(`<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>Time In already existed</h5></div>`);
	    	}else if(response.validate_timeOut){
	    		$("#validate_logs").html(`<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>Time Out already existed/not existed</h5></div>`);
	    	}else{
	    		$('#log_form').modal('hide');
		            
			    $.get(`${baseUrl}index.php/time_record/load_employees`, function(response) {
			        $('#time_table_body').html(response);
			    });
	    	}
		  			 
		});
    	
    });

	$('#clearFilterspdf').click(function(){
		$('#own_filter_date').val('');
		$.get(`${baseUrl}index.php/time_record/load_item`, function(response) {
            $('#own_time_table_body').html(response);
        });
	});

    $('#clearFilters, #clearFilters-schedule, #clearFilters-approvalRequest,#clearFilterseod').click(function(){
    	$('#filter_date').val('');
        $('#filter_employee').val(null).trigger('change');
        $('#filter_employee-schedule').val(null).trigger('change');
        $('#filter_date-approvalRequest').val('');
        $('#filter_employee-approvalRequest').val(null).trigger('change');

        $.get(`${baseUrl}index.php/time_record/load_schedule`, function(response) {
            $('#schedule_table_body').html(response);
        });
    });
	
});
