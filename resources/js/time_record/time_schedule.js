

$(document).ready(function() {

	baseUrl = $('#timeScheduleScript').data('baseUrl');

	$(`#schedule_in, #schedule_out, #edit_schedule_in, #edit_schedule_in_record, 
        #edit_schedule_out_record`).datetimepicker({
  		format: 'h:mm:ss A',
  		step: 60
    	
    });

    $('#edit_schedule_out').datetimepicker({
        format: 'h:mm:ss A',
        step: 60
        
    });
    

    $('#time_sched_button').click(function(){

        var data = {
                'filter_employee' : $('#filter_employee-schedule').val()
        };

       var colspan = $('#schedule-table > thead > tr').children().length;
        $('#schedule_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/time_record/load_schedule`, data, function(response) {
                $('#schedule_table_body').html(response);
                
        });

        $('#schedule_table').show();

        $(`#time_table, #own_time_table, #approver_table, #request_table, .manual-approval-container,#eod_table`).hide();

        $('#filter_date').val('');

        $('#filter_employee-schedule').val(null).trigger('change');
    
	});

    $('#filter_employee-schedule').change(function(){
            var data = {
                'filter_employee' : $('#filter_employee-schedule').val()
            };

            if($('#filter_employee-schedule').val() !== ''){
                $.get(`${baseUrl}index.php/time_record/load_schedule`, data, function(response) {
                    $('#schedule_table_body').html(response);
        
                });
            }else{
                var colspan = $('#schedule-table > thead > tr').children().length;
                $('#schedule_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
                $.get(`${baseUrl}index.php/time_record/load_schedule`, function(response) {
                        $('#schedule_table_body').html(response);
                        
                });
            }
        });

    $('#edit_schedule_employees').click(function(){

    	$('#edit_schedule_form').modal('show');
    });

    $('#schedule_table_body').click('.schedule_in', function(event){

    	if(event.target.className == "fa fa-edit schedinpenbtn"){
            var email = '';
            const notes = $(event.target).closest('.schedule_in');
            
            if (!notes) return;
    
            const value = notes.data('value');
            $('#validate_edit_schedulein').html('')
            $('#edit_schedulein_form').modal('show');
            if(value == 'Flexible'){
                $('#flexible_in').prop('checked', true);
                $('#edit_schedule_in input').prop('disabled', true);
            }else{
                $('#flexible_in').prop('checked', false);
                $('#edit_schedule_in input').prop('disabled', false);
            }

            var email = notes.closest('tr').find('.item-email').text().trim();
            $('.id-current-in').text(email);
            $('#edit_schedule_in input').val(value);
        }
    });

    $('#edit_schedulein_submit').click(function(){
        
        var data = {
            'email' : $('.id-current-in').text(),
            'schedule_in' : $('#edit_schedule_in input').val(),
            'flexible' : $('#flexible_in').prop('checked'),
            'filter_employee' : $('#filter_employee-schedule').val()
        };
 
        $.get(`${baseUrl}index.php/time_record/update_schedule_in`, data, function(response) {

            if(response.sched_not_exist){
                $("#validate_edit_schedulein").html(`<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i>Please Input Time In</h5></div>`);
            }else{
                $.get(`${baseUrl}index.php/time_record/load_schedule`, data, function(response) {
                    $('#schedule_table_body').html(response);
            
                });
                $('#edit_schedulein_form').modal('hide');
            }
                   
        });

    });

    $('#schedule_table_body').click('.schedule_out', function(event){
        if(event.target.className == "fa fa-edit schedoutpenbtn"){
            const notes = $(event.target).closest('.schedule_out');

            if (!notes) return;
            $('#validate_edit_scheduleout').html('');
            const value = notes.data('value');
            const email = notes.closest('tr').find('.item-email').text().trim();
              if(value == 'Flexible'){
                $('#flexible_out').prop('checked', true);
                $('#edit_schedule_out input').prop('disabled', true);
            }else{
                $('#flexible_out').prop('checked', false);
                $('#edit_schedule_out input').prop('disabled', false);
            }
            $('.id-current-out').text(email);
            $('#edit_scheduleout_form').modal('show');
            $('#edit_schedule_out input').val(value);
        }
    });    
    
    $('#edit_scheduleout_submit').click(function(){
        
                var data = {
                    'email' : $('.id-current-out').text(),
                    'schedule_out' : $('#edit_schedule_out input').val(),
                    'flexible' : $('#flexible_out').prop('checked')
                };
 
                $.get(`${baseUrl}index.php/time_record/update_schedule_out`, data, function(response) {
                    if(response.sched_not_exist){
                        $("#validate_edit_scheduleout").html(`<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input Time Out</h5>
                        </div>`);
                    }else{
                        $.get(`${baseUrl}index.php/time_record/load_schedule`, function(response) {
                            $('#schedule_table_body').html(response);
                           
                        });
                        $('#edit_scheduleout_form').modal('hide');
                    }
                });

    });

    $('#time_table_body').click('.schedule_in', function(event){

        if(event.target.className == "fa fa-edit schedinpenbtn"){
        
            const notes = $(event.target).closest('.schedule_in');
            
            if (!notes) return;
    
            const value = notes.data('value');
            
            $('#edit_schedulein_form_record').modal('show');

            var id = notes.closest('tr').find('.item-id').text().trim();
            $('.id-current-in-record').text(id);
            $('#edit_schedule_in_record input').val(value);
          
        }
    });

    $('#edit_schedulein_submit_record').click(function(){
        var status = 'Schedule In';
        var data = {
            'id' : $('.id-current-in-record').text(),
            'schedule_in' : $('#edit_schedule_in_record input').val(),
            'status' : status,
            'filter_employee' : $('#filter_employee').val(),
            'filter_date' : $('#filter_date').val()
        };

        $.get(`${baseUrl}index.php/time_record/update_schedule_record`, data, function(response) {
            $('#edit_schedulein_form_record').modal('hide');
            $.get(`${baseUrl}index.php/time_record/load_employees`, data, function(response) {
                    $('#time_table_body').html(response);
                    
            });

        });
    });

    $('#time_table_body').click('.schedule_out', function(event){

        if(event.target.className == "fa fa-edit schedoutpenbtn"){
        
            const notes = $(event.target).closest('.schedule_out');
            
            if (!notes) return;
    
            const value = notes.data('value');
            
            $('#edit_scheduleout_form_record').modal('show');

            var id = notes.closest('tr').find('.item-id').text().trim();
            $('.id-current-out-record').text(id);
            $('#edit_schedule_out_record input').val(value);
          
        }
    });

    $('#edit_scheduleout_submit_record').click(function(){
        var status = 'Schedule Out';
        var data = {
            'id' : $('.id-current-out-record').text(),
            'schedule_out' : $('#edit_schedule_out_record input').val(),
            'status' : status,
            'filter_employee' : $('#filter_employee').val(),
            'filter_date' : $('#filter_date').val()
        };
         
        $.get(`${baseUrl}index.php/time_record/update_schedule_record`, data, function(response) {
            $('#edit_schedulein_form_record').modal('hide');
            $.get(`${baseUrl}index.php/time_record/load_employees`, data, function(response) {
                    $('#time_table_body').html(response);
                    
            });

        });
    });

    $('#flexible_in, #flexible_out').change(function(){

        if ($('#flexible_in, #flexible_out').is(':checked')){
            $('#edit_schedule_in input, #edit_schedule_out input').val('Flexible');
            $('#edit_schedule_in input, #edit_schedule_out input').prop('disabled', true);
        }else{
            $('#edit_schedule_in input, #edit_schedule_out input').val('');
             $('#edit_schedule_in input, #edit_schedule_out input').prop('disabled', false);
        }
    });

});
