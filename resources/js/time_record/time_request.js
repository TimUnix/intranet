$(document).ready(function() {

	baseUrl = $('#timeRequestScript').data('baseUrl');

	$('#add_time_request').click(function(){
		$('#validate_add_request').html('');
		$('#chosen_manager').val('');
		$('#date_to_request input').val('');
		$('#add_request_form').modal('show');
	});

	
	//my time request tab
	$('#my_request').click(function(){


		$('#own_time_table, #time_table, #schedule_table, #approver_table, .manual-approval-container,#eod_table').hide();

	
		$('#request_table').show();

		var colspan = $('#request-table > thead > tr').children().length;
	        $('#request_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
		        $.get(`${baseUrl}index.php/time_record/load_request`, function(response) {
		                $('#request_table_body').html(response);
	                
	        });

	});
	//approval request tab
	$('#approval_request').click(function(){

		$('#own_time_table, #time_table, #schedule_table, #request_table, .manual-approval-container,#eod_table').hide();

		$('#approver_table').show();

		$('#filter_employee-approvalRequest').val(null).trigger('change');
		$('#filter_approval_status-approvalRequest').val('Pending').trigger('change');
		$('#filter_date-approvalRequest').val('');

	
		$.get(`${baseUrl}index.php/time_record/load_approver`, function(response) {
                $('#approver_table_body').html(response);
                
        	});
	});

	$('#filter_employee-approvalRequest').change(function(){
		var data = {
	        'filter_employee' : $('#filter_employee-approvalRequest').val(),
	        'filter_date' : $('#filter_date-approvalRequest').val(),
	        'approval_status' : $('#filter_approval_status-approvalRequest').val()
	    };

	    $.get(`${baseUrl}index.php/time_record/filter_employee_approver`, data, function(response) {$('#approver_table_body').html(response);});
	});

	$('#filter_approval_status-approvalRequest').change(function(){
		var data = {
	        'filter_employee' : $('#filter_employee-approvalRequest').val(),
	        'filter_date' : $('#filter_date-approvalRequest').val(),
	        'approval_status' : $('#filter_approval_status-approvalRequest').val()
	    };

	    $.get(`${baseUrl}index.php/time_record/filter_employee_approver`, data, function(response) {$('#approver_table_body').html(response);});
	});

	$('#date_range-approvalRequest').click(function(){
		var data = {
			'filter_employee' : $('#filter_employee-approvalRequest').val(),
			'filter_date' : $('#filter_date-approvalRequest').val(),
			'approval_status' : $('#filter_approval_status-approvalRequest').val()
		};

		$.get(`${baseUrl}index.php/time_record/filter_employee_approver`, data, function(response) {$('#approver_table_body').html(response);});

	});
	//insert TITO to time records request
	$('#request_submit').click(function(){

		var data = {
			'manager' : $('#chosen_manager').val(),
			'request_date' : $('#date_to_request input').val(),
			'status' : 'TITO'
		};

		$.get(`${baseUrl}index.php/time_record/add_request`, data, function(response) {

			if(response.validate_fill_form){
				$("#validate_add_request").html(`<div class="alert alert-danger alert-dismissible">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                        <h5><i class="icon fas fa-check"> Please Input All Fields</i></h5>
	                        </div>`);
			}
			else if(response.validate_request_exist){
				$("#validate_add_request").html(`<div class="alert alert-danger alert-dismissible">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                        <h5><i class="icon fas fa-check"> Request already exist</i></h5>
	                        </div>`);
			}else{
				$.get(`${baseUrl}index.php/time_record/load_request`, function(response) {
			                $('#request_table_body').html(response);
			                
			        });
				$('#add_request_form').modal('hide');

			}				      
        	});
	});
//approve button
	$('#approver_table_body').click('.employee', function(){
		if(event.target.className == "fa fa-check approvedbtn"){
		    const notes = $(event.target).closest('.approved_denied');
    
	            if (!notes) return;
	    		$('#approver_note_approved').val('');
	            const value = notes.data('value');
	            var email = notes.closest('tr').find('.item-email').text().trim();
	            var id = notes.closest('tr').find('.item-id').text().trim();
	            var start_date = notes.closest('tr').find('.item-start_date').text().trim();
	     		var end_date = notes.closest('tr').find('.item-end_date').text().trim();

	     		$('#id-requester-approved').val(id);
	     		
	            var data = {
	            	'email' : email,
	            	'start_date' : start_date,
	            	'end_date' : end_date
	            };
	
	            $.get(`${baseUrl}index.php/time_record/check_approved_denied_exist`, data, function(response) {
			$('#status_editor').modal('hide');
			if(response.already_approved_denied){
				$('#already_approved_denied').modal('show');
			}else{
				$('#status_editor_approved').modal('show');
			}		                
		    });
			//denied button
		}else if(event.target.className == "fa fa-times deniedbtn"){
		    const notes = $(event.target).closest('.approved_denied');
    
	            if (!notes) return;
	    		$('#approver_note_denied').val('');
	            const value = notes.data('value');
	            var id = notes.closest('tr').find('.item-id').text().trim();
	            $('#id-requester-denied').val(id);
	            var data = {
	            	'id' : id
	            };
	            
	            $.get(`${baseUrl}index.php/time_record/check_approved_denied_exist`, data, function(response) {
			$('#status_editor').modal('hide');

			if(response.already_approved_denied){
				$('#already_approved_denied').modal('show');
			}else{
				$('#status_editor_denied').modal('show');
			}
					                
		    });
		}
	});
	//approved
	$('#status_approve_submit').click(function(){

		var status_time_request = 'Approved';

		var data = {
			'id' : $('#id-requester-approved').val(),
			'status_time_request' : status_time_request,
			'notes' : $('#approver_note_approved').val()
		};
		
		$.get(`${baseUrl}index.php/time_record/update_request_status`, data, function(response) {
			$('#status_editor_approved').modal('hide');
			$('#approved_success').modal('show');
			$.get(`${baseUrl}index.php/time_record/load_approver`, function(response) {
		                $('#approver_table_body').html(response);
		                
		        });		                
		});
	});

	$('#status_denied_submit').click(function(){
		var status = 'TITO';
		var status_time_request = 'Denied';
		var data = {
			'id' : $('#id-requester-denied').val(),
			'status_time_request' : status_time_request,
			'notes' : $('#approver_note_denied').val(),
			'status' : status
		};

		$.get(`${baseUrl}index.php/time_record/update_request_status`, data, function(response) {
			$('#status_editor_denied').modal('hide');
			$('#denied_success').modal('show');
			$.get(`${baseUrl}index.php/time_record/load_approver`, function(response) {
		                $('#approver_table_body').html(response);
		                
		        	});		                
		});
	});

	$('#request_table_body').click('.employee', function(){
		
		if(event.target.className == "fa fa-edit statuseditpbtn"){
		    const notes = $(event.target).closest('.edit_show');
		    if (!notes) return;
	            const value = notes.data('value');

	            var id = notes.closest('tr').find('.item-id').text().trim();
	            var manager = notes.closest('tr').find('.item-manager').text().trim();
	            var date = notes.closest('tr').find('.item-request_date').text().trim();
	            var status = notes.closest('tr').find('.item-status-type').text().trim();
	            var leave_day_type = notes.closest('tr').find('.item-leave_day_type').text().trim();
	            var edit_start_leave_date = notes.closest('tr').find('.item-start_leave_date').text().trim();
	            var edit_end_leave_date =notes.closest('tr').find('.item-end_leave_date').text().trim();
	            var edit_leave_type = notes.closest('tr').find('.item-leave_type').text().trim();
	            var edit_leave_reason = notes.closest('tr').find('.item-leave_reason').text().trim();
	        	var edit_time_of_day = notes.closest('tr').find('.item-time_of_day').text().trim();

	            if(status == 'TITO'){
	            	$('#my_status_editor').modal('show');
	            	$("#my_approver_change").val(manager);
		            $('#date_to_request_change input').val(date);
		            $('#id-my-requester').val(id);
	            }else if(status == 'Leave' && leave_day_type == 'Whole Day'){
	            	$('#edit_request_leave').modal('show');
	            	$("#edit_chosen_manager_leave").val(manager);
	            	$('#edit_start_leave_date input').val(edit_start_leave_date);
	            	$('#edit_end_leave_date input').val(edit_end_leave_date);
	            	$('#edit_leave_day_type').val(leave_day_type);
	            	$('#div_edit_time_of_day').hide();
	            	$('#div_edit_leave_day_type_h').hide();
	            	$('#div_edit_leave_day_type_w').show();
	            	$('#edit_leave_type').val(edit_leave_type);
	            	$('#edit_leave_reason').val(edit_leave_reason);
	            	$('#edit_time_of_day :selected').text('');
	            	$('#id-edit-leave').val(id);
	            }else if(status == 'Leave' && leave_day_type == 'Half Day'){
	            	$('#edit_request_leave').modal('show');
	            	$('#id-edit-leave').val(id);
	            	$('#div_edit_leave_day_type_h').show();
	            	$('#div_edit_leave_day_type_w').hide();
	            	$("#edit_chosen_manager_leave").val(manager);
	            	$('#edit_start_leave_date input').val(edit_start_leave_date);
	            	$('#div_edit_end_leave_date').hide();
	            	$('#edit_leave_day_type').val(leave_day_type);
	            	$('#edit_time_of_day').val(edit_time_of_day);
	            	$('#edit_leave_reason').val(edit_leave_reason);
	            	$('#edit_leave_type').val(edit_leave_type);
	            	$('#edit_end_leave_date input').val('');
	            
	            }        
	            
		}
	});

	$('#my_approve_submit').click(function(){
		var data = {
			'id' : $('#id-my-requester').val(),
			'manager' : $('#my_approver_change').val(),
			'date' : $('#date_to_request_change input').val()
		};

		$.get(`${baseUrl}index.php/time_record/update_my_request_status`, data, function(response) {
			$('#my_status_editor').modal('hide');
			$.get(`${baseUrl}index.php/time_record/load_request`, function(response) {
		                $('#request_table_body').html(response);
		                
		        	});		                
		});		
	});

	$('#add_leave').click(function(){
		$('#validate_add_request_leave').html('');
		$('#chosen_manager_leave').val('');
		$('#leave_day_type').val('Whole Day');
		$('#start_leave_date input').val('');
		$('#end_leave_date input').val('');
		$('#div_end_leave_date').show();
		$('#leave_type').val('');
		$('#leave_reason').val('');
		$('#add_request_leave').modal('show');
		$('#div_time_of_day').hide();
	});

    $('#leave_submit').click(function(){
    	$('#validate_add_request_leave').html('');
    	var data = {
    		'chosen_manager_leave'	: $('#chosen_manager_leave').val(),
    		'leave_day_type'		: $('#leave_day_type').val(),
    		'start_leave_date'		: $('#start_leave_date input').val(),
    		'end_leave_date'		: $('#end_leave_date input').val(),
    		'time_of_day'			: $('#time_of_day').val(),
    		'leave_type'			: $('#leave_type').val(),
    		'leave_reason'			: $('#leave_reason').val(), 
    	};

    	$.get(`${baseUrl}index.php/time_record/add_leave`, data, function(response){
    		if(response.need_input_all){
    			$("#validate_add_request_leave").html(`<div class="alert alert-danger alert-dismissible">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                        <h5><i class="icon fas fa-check"> Please Input All Fields</i></h5>
	                        </div>`);
    		}else if(response.validate_request_exist){
				$("#validate_add_request_leave").html(`<div class="alert alert-danger alert-dismissible">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                        <h5><i class="icon fas fa-check"> Request already exist</i></h5>
	                        </div>`);
			}else{
    			$.get(`${baseUrl}index.php/time_record/load_request`, function(response) {
    					$('#add_request_leave').modal('hide');
		                $('#request_table_body').html(response);
		                
		        	});		  
    		}
    	});
    });

    $('#edit_leave_submit').click(function(){
    	if($('#edit_chosen_manager_leave').val() == '' || $('#edit_leave_day_type').val() == '' || $('#edit_start_leave_date input').val() == '' || $('#edit_time_of_day').val() == ''
    		|| $('#edit_leave_type').val() == '' || $('#edit_leave_reason').val() == ''){
    		$("#validate_edit_request_leave").html(`<div class="alert alert-danger alert-dismissible">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                        <h5><i class="icon fas fa-check"> Please Input All Fields</i></h5>
	                        </div>`);
    }else{
    	var data = {
    		'id'					: $('#id-edit-leave').val(),
    		'chosen_manager_leave'	: $('#edit_chosen_manager_leave').val(),
    		'leave_day_type'		: $('#edit_leave_day_type').val(),
    		'start_leave_date'		: $('#edit_start_leave_date input').val(),
    		'end_leave_date'		: $('#edit_end_leave_date input').val(),
    		'time_of_day'			: $('#edit_time_of_day').val(),
    		'leave_type'			: $('#edit_leave_type').val(),
    		'leave_reason'			: $('#edit_leave_reason').val(), 
    	};

    	$.get(`${baseUrl}index.php/time_record/edit_leave`, data, function(response){	
    		$.get(`${baseUrl}index.php/time_record/load_request`, function(response) {$('#request_table_body').html(response);});
    			$('#edit_request_leave').modal('hide');
    	   });
    }
    	

    });

    $('#start_leave_date, #end_leave_date, #date_to_request_change').datetimepicker({
    			format: 'YYYY-MM-DD'	
    });

    $('#leave_day_type').change(function(){
    	if($('#leave_day_type').find(':selected').val() == 'Whole Day'){
    		$('#div_time_of_day').hide();
    		$('#div_end_leave_date').show();
    	}else if($('#leave_day_type').find(':selected').val() == 'Half Day'){
    		$('#div_end_leave_date').hide();
    		$('#div_time_of_day').show();
    	}

    });

     $('#edit_leave_day_type').change(function(){
    	if($('#edit_leave_day_type').find(':selected').val() == 'Whole Day'){
    		$('#div_edit_time_of_day').hide();
    		$('#div_edit_end_leave_date').show();
    	}else if($('#edit_leave_day_type').find(':selected').val() == 'Half Day'){
    		$('#div_edit_end_leave_date').hide();
    		$('#div_edit_time_of_day').show();
    	}

    });
});
