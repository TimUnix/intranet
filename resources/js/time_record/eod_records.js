$(document).ready(function() {
    let itemsOffset = 0;
    baseUrl = $('#eodRecordsScript').data('baseUrl');

    $('#eod_records_button').click(function(){

		$(`#schedule_table, #own_time_table, #remove_in_admin, #request_table, #approver_table, #time_table, .manual-approval-container`).hide();
		$('#eod_table, #for_admin, #clearFilters').show();


		$('#filter_employee_eod').val(null).trigger('change');
		$('#filter_date_eod').val('');

		let itemsOffset = 0;
		let itemsLimit = 100;
		let itemsRequested = false;

		var colspan = $('#eod-table > thead > tr').children().length;
		$('#eod_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td>`);
		$.get(`${baseUrl}index.php/time_record/load_eod`, function(response) {
	            $('#eod_table_body').html(response);
	    });
		

		$(document).scroll(function() {

			const rowCount = $('#eod_table_body > tr').length;
			
			
			if (rowCount < itemsLimit) return;
			const position = $(this).scrollTop();
			let maxHeight = 0;
	
			// $('#eod_table_body > tr').each(function() {
			// 	maxHeight += $(this).height();
			// });

			maxHeight += $('#eod_table_body').height();
			

			if (position > (maxHeight * 9 / 10) && !itemsRequested) {
				itemsRequested = true;
				itemsOffset += itemsLimit;
	
				const data = {
					'items_offset': itemsOffset,
				};
	
				$.post(`${baseUrl}index.php/time_record/load_eod`, data, function(response) {
					$('#eod_table_body').append(response);
					itemsRequested = false;
					
				});
			}
		});

        $('#filter_employee_eod').on('change',function(){
			
            var data = {
                'filter_employee' : $('#filter_employee_eod').val(),
                'filter_date' : $('#filter_date_eod').val(),
                'items_offset': itemsOffset
            };

            if($('#filter_employee_eod').val() !== '' || $('#filter_date_eod').val() !== ''){
        
                $.get(`${baseUrl}index.php/time_record/filter_employee_eod`, data, function(response) {
                    $('#eod_table_body').html(response);
                    
                });
            }else{
                var colspan = $('#eod-table > thead > tr').children().length;
                $('#eod_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
                $.get(`${baseUrl}index.php/time_record/load_eod`, function(response) {
                        $('#eod_table_body').html(response);
                });
            }
        });
    
        $('#date_range_eod').on('click',function(){
            var data = {
                'filter_employee' : $('#filter_employee_eod').val(),
                'filter_date' : $('#filter_date_eod').val(),
                'items_offset': itemsOffset
            };
     
            if($('#filter_employee_eod').val() !== '' || $('#filter_date_eod').val() !== ''){
                $.get(`${baseUrl}index.php/time_record/filter_employee_eod`, data, function(response) {
                    $('#eod_table_body').html(response);
                });
            }else{
                var colspan = $('#eod-table > thead > tr').children().length;
                $('#eod_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
                $.get(`${baseUrl}index.php/time_record/load_eod`, function(response) {
                        $('#eod_table_body').html(response);
                });
            }
        });
	});

    $('#clearFilterseod').click(function(){
    	$('#filter_date_eod').val('');
        $('#filter_employee_eod').val(null).trigger('change');

        $.get(`${baseUrl}index.php/time_record/load_eod`, function(response) {
            $('#eod_table_body').html(response);
        });
    });

    $('#exportCSVeod').click(function() {
		var data = {
			'filter_employee' : $('#filter_employee_eod').val(),
            'filter_date' : $('#filter_date_eod').val(),
			ajax : '1'
		};

        const  url = `${baseUrl}index.php/time_record/generate_csv_eod`;

        if($('#filter_date_eod').val() == ''){
            $('#eod_filter_fill').modal('show');
        }else{
            $.get(url, data, function(response, _, xhr) {
                if (xhr.status === 200) {
                    const filename = `timeRecord_${new Date().toLocaleDateString()}.csv`;
    
                    const link = $('<a>');
                    link.addClass('d-none');
                    link.addClass('downloadCSVLink');
                    link.prop('target', '_blank');
                    link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                    link.prop('download', filename);
    
                    $('body').append(link);
                    link[0].click();
                    link.remove();
                    }
                });
        }
    });

    $('#close_eod_filter_fill').click(function(){
        $('#eod_filter_fill').modal('hide');
    });
});