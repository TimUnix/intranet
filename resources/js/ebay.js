let baseUrl;
let itemsOffset = 0;
let itemsLimit = 100;

const columnsToCopy = {
    description: 5,
    condition: 10,
    price: 18,
    total: 18,
    bar: 5,


    title: 5,
    itemnumber: 3,
    seller: 6,
    price: 7,
    bar: 5
};

$(document).ready(function() {
    $('.filters').select2();

    baseUrl = $('#ebayScript').data('baseUrl');

    const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const sellingState = parameters.get('sellingState');

    const isAnItemRequested = searchItem !== null;

    if (isAnItemRequested) {
        $('#exportCSV').prop('disabled', true);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);

        loadItems();

        saveHistory(searchItem);
    }

    $('#activeswitch').prop('checked', sellingState === 'Active');

    $('#seller').change(function(event) {
        search();
    });

    $('#orderBy').change(function(event) {
        search();
    });

    $('#sortOrder').change(function(event) {
        search();
    });

    $('#btnApplyFilters').click(function(event) {
        search();
        $('#filterModal').modal('hide');
    });

    $('#searchHistory').change(function(event) {
        const selected = $(event.target).find(':selected');

        $('#itemSearch').val(selected.val());

        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        search();
    });

    $('.btn-copy-textarea').click(function() {
        const text = $('.textarea-copy').val();

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = text;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();

        $(document).Toasts('create', {
            title: 'Copied to Clipboard',
            body: 'Text copied',
            icon: 'fas fa-clipboard',
            autohide: true,
            delay: 4000
        });

    });

    $("#activeswitch").on('change', function() {
        search();
    });

    let itemsRequested = false;

    $(document).scroll(function() {
        if (itemsOffset === 0) {
            const rowCount = $('tbody > tr').length;

            if (rowCount < 100) {
                return;
            }
        }

        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('tbody > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 /10) && !itemsRequested) {
            itemsRequested = true;
            loadItems();
        }
    });

    function search() {
        const searchItem = $('#itemSearch').val();
        
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();
        const seller = $('#seller').find(':selected').val();
        const sellingState = $('#activeswitch').prop('checked') ? 'Active' : '';
        const conditions = $('#conditions').select2('data')
              .map(condition => condition.id)
              .join(',');
        const categories = $('#categories').select2('data')
              .map(category => category.id)
              .join(',');

        const route = `${baseUrl}index.php/ebay_competitors?search=${searchItem}&seller=${seller}`
              + `&orderBy=${orderBy}&sortOrder=${sortOrder}&conditions=${conditions}&categories=${categories}`
              + `&sellingState=${sellingState}`;

        window.location.href = route;
    }

    function saveHistory(search) {
        $.post(`${baseUrl}index.php/ebay_competitors/save_search_history`, { 'search': search });
    }

    function loadItems() {
        const parameters = new URLSearchParams(window.location.search);
        const searchItem = parameters.get('search');
        const parameterSeller = parameters.get('seller');
        const parameterOrderBy = parameters.get('orderBy');
        const parameterSortOrder = parameters.get('sortOrder');
        const parameterConditions = parameters.get('conditions');
        const parameterCategories = parameters.get('categories');
        const parameterSellingState = parameters.get('sellingState');

        if (parameterOrderBy !== '' && parameterSortOrder !== '') {
            $('#orderBy').find(':selected').prop('selected', false);
            $('#sortOrder').find(':selected').prop('selected', false);            
            $(`#orderBy > option[value="${parameterOrderBy}"]`).prop('selected', true);
            $(`#sortOrder > option[value="${parameterSortOrder}"]`).prop('selected', true);
        }

        if (parameterSeller !== '') {
            $('#seller').find(':selected').prop('selected', false);
            $(`#seller > option[value="${parameterSeller}"]`).prop('selected', true);
        }

        if (parameterConditions !== '') {
            $('#conditions').select2().val(parameterConditions.split(',')).trigger('change');
        }

        if (parameterCategories !== '') {
            $('#categories').select2().val(parameterCategories.split(',')).trigger('change');
        }
        
        const data = {
            'searchItem': searchItem,
            'seller' : parameterSeller,
            'orderBy': parameterOrderBy,
            'sortOrder': parameterSortOrder,
            'conditions': parameterConditions,
            'categories': parameterCategories,
            'sellingState': parameterSellingState,
            'currentOffset': itemsOffset
        };

        if (itemsOffset === 0) {
            $.post(`${baseUrl}index.php/ebay_competitors/summary`, data, function(response) {
                $('.summary').html(response);
            });
        }

        $.post(`${baseUrl}index.php/ebay_competitors/search_item`, data, function(response) {
            if (itemsOffset === 0) {
                $('#product_table_body').html(response);
            } else {
                $('#product_table_body').append(response);
            }
        }).then(function() {
            itemsOffset += itemsLimit;
            itemsRequested = false;
            $('#exportCSV').prop('disabled', false);

            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
        });
    }
});
