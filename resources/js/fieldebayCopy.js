$(document).ready(function() {
    $('table').on('click', '.copy-field', function() {
        let copyButton = $(this);
        let copyButtonContent = copyButton.html();

        let copiedData = $.trim(this.parentNode.textContent);

        $(document).Toasts('create', {
            title: 'Copied to Clipboard',
            body: copiedData,
            icon: 'fas fa-clipboard',
            autohide: true,
            delay: 4000
        });

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');


        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = copiedData;
        $temp.select();

        var status = document.execCommand("copy");

        $temp.remove();
    });
});
