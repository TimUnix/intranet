$(document).ready(function(){
    let itemsOffset = 0;
    let itemsLimit = 100;

    var accessToken = localStorage.getItem('accessToken');

    const searchParam = new URLSearchParams(window.location.search);
    const search = searchParam.get('search');
    var categoryFilters = searchParam.get('category_filters');
    var conditionFilters = searchParam.get('condition_filters');
    const listingFilter = searchParam.get('listing_filter');
    var sortOrder = searchParam.get('sort_order');
    var orderBy = searchParam.get('order_by');
    
    var urlParams = new URLSearchParams(window.location.search);
    var code = urlParams.get('code');

    var redirect_uri = window.location.origin + '/index.php/inventory';
    var client_id = "566600176515-v8err916j41a3j5bvjsb220684jouc1d.apps.googleusercontent.com";
    var client_secret = "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"; // replace with your client secret
    var scope = "https://www.googleapis.com/auth/drive";

    if(code === null){
        var url = "";
        localStorage.setItem('currentUri', window.location.href);

        $("#exportToGdrive").click(function(){
            signIn(client_id,redirect_uri,scope,url);
        });
    }
    else{
        var access_token= "";
        
        $.ajax({
            type: 'POST',
            url: "https://oauth2.googleapis.com/token",
            data: {
                code:code,
                redirect_uri:redirect_uri,
                client_secret:client_secret,
                client_id:client_id, 
                grant_type: 'authorization_code',
                scope:scope,
            },
            dataType: "json",
            success: function(resultData) {
                localStorage.setItem("accessToken",resultData.access_token);
                localStorage.setItem("expires_in",resultData.expires_in);
                
                window.location = localStorage.getItem('currentUri');
            }
        });
    }
    
    function signIn(client_id,redirect_uri,scope,url){
        if(accessToken === null){
            url = "https://accounts.google.com/o/oauth2/v2/auth?redirect_uri="+redirect_uri
            +"&prompt=consent&response_type=code&client_id="+client_id+"&scope="+scope
            +"&access_type=offline";

            window.location = url;
        }else{
            var data = {
                'search': search,
                'category_filters': categoryFilters,
                'condition_filters': conditionFilters,
                'listing_filter': listingFilter,
                'sort_order': sortOrder,
                'order_by': orderBy,
                'items_offset': itemsOffset,
                'accessToken': accessToken
            };
       
            const  url_csv = `${baseUrl}index.php/inventory/generate_csv_gdrive`;
    
            $.post(url_csv, data, function(response, _, xhr) {
                if (xhr.status === 200) {
                    console.log("success");
                }
            });
        }
    }
});