//let baseUrl;

$(document).ready(function() {
    // check the session every 2 seconds
    baseUrl = $('#sessionCheckScript').data('baseUrl');

    setInterval(checkSession, 2000);

    function checkSession() {
        $.get(`${baseUrl}index.php/user/verify_session`, function(response) {
            if (!response.isUserSessionActive) {
                window.location.href = `${baseUrl}index.php/login`;
            }
        });
    }
});
