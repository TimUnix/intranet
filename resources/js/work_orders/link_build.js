$(document).ready(function() {
    $("#build_id").change(function(){


        $('#front_caddies').empty();
        $('#rear_caddies').empty();

        $('#front_caddies').append($('<option>', {value: '3.5 To Caddy Converter', text: '3.5 To Caddy Converter'}));
        $('#front_caddies').append($('<option>', {value: '3.5 Caddy', text: '3.5 Caddy'}));
        $('#front_caddies').append($('<option>', {value: '3.5 Filler', text: '3.5 Filler'}));
        $('#front_caddies').append($('<option>', {value: '2.5 Filler', text: '2.5 Filler'}));
        $('#front_caddies').append($('<option>', {value: '2.5 Caddy', text: '2.5 Caddy'}));


        $('#rear_caddies').append($('<option>', {value: '3.5 To Caddy Converter', text: '3.5 To Caddy Converter'}));
        $('#rear_caddies').append($('<option>', {value: '3.5 Caddy', text: '3.5 Caddy'}));
        $('#rear_caddies').append($('<option>', {value: '3.5 Filler', text: '3.5 Filler'}));
        $('#rear_caddies').append($('<option>', {value: '2.5 Filler', text: '2.5 Filler'}));
        $('#rear_caddies').append($('<option>', {value: '2.5 Caddy', text: '2.5 Caddy'}));

        $("#motherboard").empty();
        $("#cpu").empty();
        $("#memory").empty();
        var datas = {
            build_id : $("#build_id").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/request/get_build`,
            type : "post",
            data : datas,
            success : function(msg){
                
                
                if(msg.front_caddies == "2.5 Filler" || msg.front_caddies == "2.5 Caddy"){
                    $("#front_caddies option[value='3.5 To Caddy Converter']").remove();
                    $("#front_caddies option[value='3.5 Caddy']").remove();
                    $("#front_caddies option[value='3.5 Filler']").remove();
                    $("#front_caddies option[value='1']").remove();
                }
                else if(msg.front_caddies == "3.5 To Caddy Converter" || msg.front_caddies == "3.5 Caddy" || msg.front_caddies == "3.5 Filler"){
                    $("#front_caddies option[value='2.5 Filler']").remove();
                    $("#front_caddies option[value='2.5 Caddy']").remove();
                }


                if(msg.rear_caddies == "2.5 Filler" || msg.rear_caddies == "2.5 Caddy"){
                    $("#rear_caddies option[value='3.5 To Caddy Converter']").remove();
                    $("#rear_caddies option[value='3.5 Caddy']").remove();
                    $("#rear_caddies option[value='3.5 Filler']").remove();
                }
                else if(msg.rear_caddies == "3.5 To Caddy Converter" || msg.rear_caddies == "3.5 Caddy" || msg.rear_caddies == "3.5 Filler"){
                    $("#rear_caddies option[value='2.5 Filler']").remove();
                    $("#rear_caddies option[value='2.5 Caddy']").remove();
                }

                if(msg.rear_caddies == null || msg.rear_caddies == "null" || msg.rear_caddies == ""){
                    $("#rear_caddies option[value='3.5 To Caddy Converter']").remove();
                    $("#rear_caddies option[value='3.5 Caddy']").remove();
                    $("#rear_caddies option[value='3.5 Filler']").remove();
                    $("#rear_caddies option[value='2.5 Filler']").remove();
                    $("#rear_caddies option[value='2.5 Caddy']").remove();
                }
                if(msg.front_caddies == null || msg.front_caddies == "null" || msg.front_caddies == ""){
                    $("#front_caddies option[value='3.5 To Caddy Converter']").remove();
                    $("#front_caddies option[value='3.5 Caddy']").remove();
                    $("#front_caddies option[value='3.5 Filler']").remove();
                    $("#front_caddies option[value='2.5 Filler']").remove();
                    $("#front_caddies option[value='2.5 Caddy']").remove();
                }

                $(`#chasis option:contains(${msg.chassis})`).prop('selected',true);
                $("#front_caddies").val(msg.front_caddies)


                if(msg.fc_qty == 0 || msg.fc_qty == "" || msg.fc_qty == null || msg.fc_qty == "null" || msg.fc_qty == undefined || msg.fc_qty === null || msg.fc_qty === "null" || msg.fc_qty === undefined){
                    $("#qty_front_caddies").val("")
                }else{
                    $("#qty_front_caddies").val(msg.fc_qty)
                }
                
                $("#rear_caddies").val(msg.rear_caddies)


                if(msg.rc_qty == 0 || msg.rc_qty == "" || msg.rc_qty == null || msg.rc_qty == "null" || msg.rc_qty == undefined || msg.rc_qty === null || msg.rc_qty === "null" || msg.rc_qty === undefined){
                    $("#qty_rear_caddies").val("")
                }else{
                    $("#qty_rear_caddies").val(msg.rc_qty)
                }
                
               
                $(`#item_harddrive_one option:contains(${msg.drives})`).prop('selected',true);


                if(msg.drives_qty == 0 || msg.drives_qty == "" || msg.drives_qty == null || msg.drives_qty == "null" || msg.drives_qty == undefined || msg.drives_qty === null || msg.drives_qty === "null" || msg.drives_qty === undefined){
                    $("#qty_hard_drive_one").val("")
                }else{
                    $("#qty_hard_drive_one").val(msg.drives_qty)
                }
                
                if(msg.drives2 == null || msg.drives2 == "null"){
                    $("#item_harddrive_two").val($("#item_harddrive_two option:first").val());
                }else{
                    $(`#item_harddrive_two option:contains(${msg.drives2})`).prop('selected',true);
                }
                


                if(msg.drives_qty2 == 0 || msg.drives_qty2 == "" || msg.drives_qty2 == null || msg.drives_qty2 == "null" || msg.drives_qty2 == undefined || msg.drives_qty2 === null || msg.drives_qty2 === "null" || msg.drives_qty2 === undefined){
                    $("#qty_hard_drive_two").val("")
                }else{
                    $("#qty_hard_drive_two").val(msg.drives_qty2)
                }
                
                if(msg.drives3 == null || msg.drives3 == "null"){
                    $("#item_harddrive_three").val($("#item_harddrive_three option:first").val());
                }else{
                    $(`#item_harddrive_three option:contains(${msg.drives3})`).prop('selected',true);
                }
                


                if(msg.drives_qty3 == 0 || msg.drives_qty3 == "" || msg.drives_qty3 == null || msg.drives_qty3 == "null" || msg.drives_qty3 == undefined || msg.drives_qty3 === null || msg.drives_qty3 === "null" || msg.drives_qty3 === undefined){
                    $("#qty_hard_drive_three").val("")
                }else{
                    $("#qty_hard_drive_three").val(msg.drives_qty3)
                }
                

                
                  
                
                 



                if(msg.motherboard_qty == 0 || msg.motherboard_qty == "" || msg.motherboard_qty == null || msg.motherboard_qty == "null" || msg.motherboard_qty == undefined || msg.motherboard_qty === null || msg.motherboard_qty === "null" || msg.motherboard_qty === undefined){
                    $("#qty_motherboard").val("")
                }else{
                    $("#qty_motherboard").val(msg.motherboard_qty)
                }
                

                




                


                    if(msg.chassis == "" || msg.chassis == null || msg.chassis == "null"){
                        if(msg.motherboard == null || msg.motherboard == "" || msg.motherboard == "null" || msg.motherboard == undefined || msg.motherboard === null || msg.motherboard === "null" || msg.motherboard === undefined){
                            var put_mb = ""
                        }else{
                            var put_mb = msg.motherboard
                        }
                        var $mounting_ = $("<option selected='selected'></option>").val(put_mb).text(put_mb)
 
                        $("#motherboard").append($mounting_).trigger('change');


                        var $cpu_insert = $("<option selected='selected'></option>").val(msg.cpu).text(msg.cpu)
 
                        $("#cpu").append($cpu_insert).trigger('change'); 


                        var $memory_insert = $("<option selected='selected'></option>").val(msg.memory).text(msg.memory)
 
                        $("#memory").append($memory_insert).trigger('change');
                    }else{
                        var datas = {
                            part_number : msg.chassis,
                            cpu_for_edit : "",
                            motherboard_id : msg.motherboard,
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/request/motherboard`,
                            type : "post",
                            data : datas,
                            success : function(motherboard_chassis){
                                    $('#motherboard_chassis').html(motherboard_chassis);       
                                  }
                            });


                            var datas = {
                                part_number : msg.chassis,
                                cpu_for_edit : "",
                                cpu_id : msg.cpu,
                                ajax : '1'   
                            };
                            $.ajax({
                                url : `${baseUrl}index.php/work_orders/request/cpu`,
                                type : "post",
                                data : datas,
                                success : function(cpu){
                                        $('#motherboard_cpu').html(cpu);       
                                      }
                                });


                                var datas = {
                                    part_number : msg.chassis,
                                    cpu_for_edit : "",
                                    memory_id : msg.memory,
                                    ajax : '1'   
                                };
                                $.ajax({
                                    url : `${baseUrl}index.php/work_orders/request/memory`,
                                    type : "post",
                                    data : datas,
                                    success : function(motherboard_memory){
                                            $('#motherboard_memory').html(motherboard_memory);       
                                          }
                                    }).then(function(){
                                        var datas = {
                                            motherboard_ : $("#motherboard").val(),
                                            cpu_ : $("#cpu").val(),
                                            memory_ : $("#memory").val(),
                                            ajax : '1'   
                                        };
                                        $.ajax({
                                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                                            type : "post",
                                            data : datas,
                                            success : function(response_ob_nic_pci_slot){
                                                var datas = {
                                                    motherboard_ : $("#motherboard").val(),
                                                    cpu_ : $("#cpu").val(),
                                                    memory_ : $("#memory").val(),
                                                    ajax : '1'   
                                                };
                                                $.ajax({
                                                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                                                    type : "post",
                                                    data : datas,
                                                    success : function(response_ob_nic_pci_slot){
                                                        $("#ob_nic").text(response_ob_nic_pci_slot.ob_nic);
                                                        $("#pcie_slot").text(response_ob_nic_pci_slot.pci_slot);
                                                            ///$('#motherboard_chassis').html(motherboard);       
                                                        }
                                                    });
                                                    ///$('#motherboard_chassis').html(motherboard);       
                                                }
                                            });
                                    });
                    }
                


                    
                

                



                if(msg.cpu_qty == 0 || msg.cpu_qty == "" || msg.cpu_qty == null || msg.cpu_qty == "null" || msg.cpu_qty == undefined || msg.cpu_qty === null || msg.cpu_qty === "null" || msg.cpu_qty === undefined){
                    $("#qty_cpu").val("")
                }else{
                    $("#qty_cpu").val(msg.cpu_qty)
                }
                

                


                



                if(msg.memory_qty == 0 || msg.memory_qty == "" || msg.memory_qty == null || msg.memory_qty == "null" || msg.memory_qty == undefined || msg.memory_qty === null || msg.memory_qty === "null" || msg.memory_qty === undefined){
                    $("#qty_memory").val("")
                }else{
                    $("#qty_memory").val(msg.memory_qty)
                }
                

                $("#cntrl").val(msg.slot)


                if(msg.slot_qty == 0 || msg.slot_qty == "" || msg.slot_qty == null || msg.slot_qty == "null" || msg.slot_qty == undefined || msg.slot_qty === null || msg.slot_qty === "null" || msg.slot_qty === undefined){
                    $("#qty_cntrlr").val("")
                }else{
                    $("#qty_cntrlr").val(msg.slot_qty)
                }
                

                $("#cntrltwo").val(msg.slot2)


                if(msg.slot_qty2 == 0 || msg.slot_qty2 == "" || msg.slot_qty2 == null || msg.slot_qty2 == "null" || msg.slot_qty2 == undefined || msg.slot_qty2 === null || msg.slot_qty2 === "null" || msg.slot_qty2 === undefined){
                    $("#qty_cntrlrtwo").val("")
                }else{
                    $("#qty_cntrlrtwo").val(msg.slot_qty2)
                }
                

                $("#cntrlthree").val(msg.slot3)
                if(msg.slot_qty3 == 0 || msg.slot_qty3 == "" || msg.slot_qty3 == null || msg.slot_qty3 == "null" || msg.slot_qty3 == undefined || msg.slot_qty3 === null || msg.slot_qty3 === "null" || msg.slot_qty3 === undefined){
                    $("#qty_cntrlrthree").val("")
                }else{
                    $("#qty_cntrlrthree").val(msg.slot_qty3)
                }
                

                $("#psu").val(msg.psu_watts)


                if(msg.psu_qty == 0 || msg.psu_qty == "" || msg.psu_qty == null || msg.psu_qty == "null" || msg.psu_qty == undefined || msg.psu_qty === null || msg.psu_qty === "null" || msg.psu_qty === undefined){
                    $("#qty_psu").val("")
                }else{
                    $("#qty_psu").val(msg.psu_qty)
                }
                

                $(`#mounting option:contains(${msg.mounting})`).prop('selected',true);
                
                
                if(msg.psu_qty == 0 || msg.psu_qty == "" || msg.psu_qty == null || msg.psu_qty == "null" || msg.psu_qty == undefined || msg.psu_qty === null || msg.psu_qty === "null" || msg.psu_qty === undefined){
                    $("#qty_mounting").val("")
                }else{
                    $("#qty_mounting").val(msg.mounting_qty)
                }
                

                

                $("#backplane").text(msg.backplane)
                $("#drivebays").text(msg.drivebays)
                $("#rearbays").text(msg.rearbays)
                $("#nodes").text(msg.node)
                $("#psuchassis").text(msg.psu)
                $("#bin").text(msg.bin)
                           
            }
        }); 
    })




    $("#build_id_edit").change(function(){


        $('#front_caddies_edit').empty();
        $('#rear_caddies_edit').empty();

        $('#front_caddies_edit').append($('<option>', {value: '3.5 To Caddy Converter', text: '3.5 To Caddy Converter'}));
        $('#front_caddies_edit').append($('<option>', {value: '3.5 Caddy', text: '3.5 Caddy'}));
        $('#front_caddies_edit').append($('<option>', {value: '3.5 Filler', text: '3.5 Filler'}));
        $('#front_caddies_edit').append($('<option>', {value: '2.5 Filler', text: '2.5 Filler'}));
        $('#front_caddies_edit').append($('<option>', {value: '2.5 Caddy', text: '2.5 Caddy'}));


        $('#rear_caddies_edit').append($('<option>', {value: '3.5 To Caddy Converter', text: '3.5 To Caddy Converter'}));
        $('#rear_caddies_edit').append($('<option>', {value: '3.5 Caddy', text: '3.5 Caddy'}));
        $('#rear_caddies_edit').append($('<option>', {value: '3.5 Filler', text: '3.5 Filler'}));
        $('#rear_caddies_edit').append($('<option>', {value: '2.5 Filler', text: '2.5 Filler'}));
        $('#rear_caddies_edit').append($('<option>', {value: '2.5 Caddy', text: '2.5 Caddy'}));

        $("#motherboard_edit").empty();
        $("#cpu_edit").empty();
        $("#memory_edit").empty();
        var datas = {
            build_id : $("#build_id_edit").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/request/get_build`,
            type : "post",
            data : datas,
            success : function(msg){
                
                
                if(msg.front_caddies == "2.5 Filler" || msg.front_caddies == "2.5 Caddy"){
                    $("#front_caddies_edit option[value='3.5 To Caddy Converter']").remove();
                    $("#front_caddies_edit option[value='3.5 Caddy']").remove();
                    $("#front_caddies_edit option[value='3.5 Filler']").remove();
                    $("#front_caddies_edit option[value='1']").remove();
                }
                else if(msg.front_caddies == "3.5 To Caddy Converter" || msg.front_caddies == "3.5 Caddy" || msg.front_caddies == "3.5 Filler"){
                    $("#front_caddies_edit option[value='2.5 Filler']").remove();
                    $("#front_caddies_edit option[value='2.5 Caddy']").remove();
                }


                if(msg.rear_caddies == "2.5 Filler" || msg.rear_caddies == "2.5 Caddy"){
                    $("#rear_caddies_edit option[value='3.5 To Caddy Converter']").remove();
                    $("#rear_caddies_edit option[value='3.5 Caddy']").remove();
                    $("#rear_caddies_edit option[value='3.5 Filler']").remove();
                }
                else if(msg.rear_caddies == "3.5 To Caddy Converter" || msg.rear_caddies == "3.5 Caddy" || msg.rear_caddies == "3.5 Filler"){
                    $("#rear_caddies_edit option[value='2.5 Filler']").remove();
                    $("#rear_caddies_edit option[value='2.5 Caddy']").remove();
                }

                if(msg.rear_caddies == null || msg.rear_caddies == "null" || msg.rear_caddies == ""){
                    $("#rear_caddies_edit option[value='3.5 To Caddy Converter']").remove();
                    $("#rear_caddies_edit option[value='3.5 Caddy']").remove();
                    $("#rear_caddies_edit option[value='3.5 Filler']").remove();
                    $("#rear_caddies_edit option[value='2.5 Filler']").remove();
                    $("#rear_caddies_edit option[value='2.5 Caddy']").remove();
                }
                if(msg.front_caddies == null || msg.front_caddies == "null" || msg.front_caddies == ""){
                    $("#front_caddies_edit option[value='3.5 To Caddy Converter']").remove();
                    $("#front_caddies_edit option[value='3.5 Caddy']").remove();
                    $("#front_caddies_edit option[value='3.5 Filler']").remove();
                    $("#front_caddies_edit option[value='2.5 Filler']").remove();
                    $("#front_caddies_edit option[value='2.5 Caddy']").remove();
                }

                $(`#chassis_edit option:contains(${msg.chassis})`).prop('selected',true);
                $("#front_caddies_edit").val(msg.front_caddies)


                if(msg.fc_qty == 0 || msg.fc_qty == "" || msg.fc_qty == null || msg.fc_qty == "null" || msg.fc_qty == undefined || msg.fc_qty === null || msg.fc_qty === "null" || msg.fc_qty === undefined){
                    $("#qty_front_caddies_edit").val("")
                }else{
                    $("#qty_front_caddies_edit").val(msg.fc_qty)
                }
                
                $("#rear_caddies_edit").val(msg.rear_caddies)


                if(msg.rc_qty == 0 || msg.rc_qty == "" || msg.rc_qty == null || msg.rc_qty == "null" || msg.rc_qty == undefined || msg.rc_qty === null || msg.rc_qty === "null" || msg.rc_qty === undefined){
                    $("#qty_rear_caddies_edit").val("")
                }else{
                    $("#qty_rear_caddies_edit").val(msg.rc_qty)
                }
                
               
                


                if(msg.drives == null || msg.drives == "null"){
                    $("#item_harddrive_one_edit").val($("#item_harddrive_two_edit option:first").val());
                }else{
                    $(`#item_harddrive_one_edit option:contains(${msg.drives})`).prop('selected',true);
                }


                if(msg.drives_qty == 0 || msg.drives_qty == "" || msg.drives_qty == null || msg.drives_qty == "null" || msg.drives_qty == undefined || msg.drives_qty === null || msg.drives_qty === "null" || msg.drives_qty === undefined){
                    $("#qty_hard_drive_one_edit").val("")
                }else{
                    $("#qty_hard_drive_one_edit").val(msg.drives_qty)
                }
                
                if(msg.drives2 == null || msg.drives2 == "null"){
                    $("#item_harddrive_two_edit").val($("#item_harddrive_two_edit option:first").val());
                }else{
                    $(`#item_harddrive_two_edit option:contains(${msg.drives2})`).prop('selected',true);
                }
                


                if(msg.drives_qty2 == 0 || msg.drives_qty2 == "" || msg.drives_qty2 == null || msg.drives_qty2 == "null" || msg.drives_qty2 == undefined || msg.drives_qty2 === null || msg.drives_qty2 === "null" || msg.drives_qty2 === undefined){
                    $("#qty_hard_drive_two_edit").val("")
                }else{
                    $("#qty_hard_drive_two_edit").val(msg.drives_qty2)
                }
                


                if(msg.drives3 == null || msg.drives3 == "null"){
                    $("#item_harddrive_three_edit").val($("#item_harddrive_three_edit option:first").val());
                }else{
                    $(`#item_harddrive_three_edit option:contains(${msg.drives3})`).prop('selected',true);
                }
                


                if(msg.drives_qty3 == 0 || msg.drives_qty3 == "" || msg.drives_qty3 == null || msg.drives_qty3 == "null" || msg.drives_qty3 == undefined || msg.drives_qty3 === null || msg.drives_qty3 === "null" || msg.drives_qty3 === undefined){
                    $("#qty_hard_drive_three_edit").val("")
                }else{
                    $("#qty_hard_drive_three_edit").val(msg.drives_qty3)
                }
                

                
                 



                if(msg.motherboard_qty == 0 || msg.motherboard_qty == "" || msg.motherboard_qty == null || msg.motherboard_qty == "null" || msg.motherboard_qty == undefined || msg.motherboard_qty === null || msg.motherboard_qty === "null" || msg.motherboard_qty === undefined){
                    $("#qty_motherboard_edit").val("")
                }else{
                    $("#qty_motherboard_edit").val(msg.motherboard_qty)
                }
                

               




                if(msg.chassis == "" || msg.chassis == null || msg.chassis == "null"){
                    if(msg.motherboard == null || msg.motherboard == "" || msg.motherboard == "null" || msg.motherboard == undefined || msg.motherboard === null || msg.motherboard === "null" || msg.motherboard === undefined){
                        var put_mb = ""
                    }else{
                        var put_mb = msg.motherboard
                    }
                    var $mounting_ = $("<option selected='selected'></option>").val(put_mb).text(put_mb)
     
                    $("#motherboard_edit").append($mounting_).trigger('change');


                    var $cpu_insert = $("<option selected='selected'></option>").val(msg.cpu).text(msg.cpu)
 
                    $("#cpu_edit").append($cpu_insert).trigger('change');


                    var $memory_insert = $("<option selected='selected'></option>").val(msg.memory).text(msg.memory)
 
                    $("#memory_edit").append($memory_insert).trigger('change');
                }else{
                    var datas = {
                        part_number : msg.chassis,
                        cpu_for_edit : "",
                        cpu_id : msg.cpu,
                        ajax : '1'   
                    };
                    $.ajax({
                        url : `${baseUrl}index.php/work_orders/cpu_for_motherboard_edit`,
                        type : "post",
                        data : datas,
                        success : function(cpu){
                                $('#motherboard_cpu_edit').html(cpu);       
                              }
                        });
    
    
                    var datas = {
                        part_number : msg.chassis,
                        cpu_for_edit : "",
                        motherboard_id : msg.motherboard,
                        ajax : '1'   
                    };
                    $.ajax({
                        url : `${baseUrl}index.php/work_orders/motherboard_for_chassis_edit`,
                        type : "post",
                        data : datas,
                        success : function(motherboard_chassis){
                                $('#motherboard_chassis_edit').html(motherboard_chassis);       
                              }
                        });
    
    
                        var datas = {
                            part_number : msg.chassis,
                            cpu_for_edit : "",
                            memory_id : msg.memory,
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/memory_for_motherboard_edit`,
                            type : "post",
                            data : datas,
                            success : function(motherboard_memory){
                                    $('#motherboard_memory_edit').html(motherboard_memory);       
                                  }
                            }).then(function(){
                                var datas = {
                                    motherboard_ : $("#motherboard_edit").val(),
                                    cpu_ : $("#cpu_edit").val(),
                                    memory_ : $("#memory_edit").val(),
                                    ajax : '1'   
                                };
                                $.ajax({
                                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                                    type : "post",
                                    data : datas,
                                    success : function(response_ob_nic_pci_slot){
                                        var datas = {
                                            motherboard_ : $("#motherboard_edit").val(),
                                            cpu_ : $("#cpu_edit").val(),
                                            memory_ : $("#memory_edit").val(),
                                            ajax : '1'   
                                        };
                                        $.ajax({
                                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                                            type : "post",
                                            data : datas,
                                            success : function(response_ob_nic_pci_slot){
                                                $("#ob_nic_edit").text(response_ob_nic_pci_slot.ob_nic);
                                                $("#pcie_slot_edit").text(response_ob_nic_pci_slot.pci_slot);
                                                    ///$('#motherboard_chassis').html(motherboard);       
                                                }
                                            });
                                            ///$('#motherboard_chassis').html(motherboard);       
                                        }
                                    });
                            });
                }
                
                

                 



                if(msg.cpu_qty == 0 || msg.cpu_qty == "" || msg.cpu_qty == null || msg.cpu_qty == "null" || msg.cpu_qty == undefined || msg.cpu_qty === null || msg.cpu_qty === "null" || msg.cpu_qty === undefined){
                    $("#qty_cpu_edit").val("")
                }else{
                    $("#qty_cpu_edit").val(msg.cpu_qty)
                }
                

                


                



                if(msg.memory_qty == 0 || msg.memory_qty == "" || msg.memory_qty == null || msg.memory_qty == "null" || msg.memory_qty == undefined || msg.memory_qty === null || msg.memory_qty === "null" || msg.memory_qty === undefined){
                    $("#qty_memory_edit").val("")
                }else{
                    $("#qty_memory_edit").val(msg.memory_qty)
                }
                

                $("#cntrl_edit").val(msg.slot)


                if(msg.slot_qty == 0 || msg.slot_qty == "" || msg.slot_qty == null || msg.slot_qty == "null" || msg.slot_qty == undefined || msg.slot_qty === null || msg.slot_qty === "null" || msg.slot_qty === undefined){
                    $("#qty_cntrlr_edit").val("")
                }else{
                    $("#qty_cntrlr_edit").val(msg.slot_qty)
                }
                

                $("#cntrltwo_edit").val(msg.slot2)


                if(msg.slot_qty2 == 0 || msg.slot_qty2 == "" || msg.slot_qty2 == null || msg.slot_qty2 == "null" || msg.slot_qty2 == undefined || msg.slot_qty2 === null || msg.slot_qty2 === "null" || msg.slot_qty2 === undefined){
                    $("#qty_cntrlrtwo_edit").val("")
                }else{
                    $("#qty_cntrlrtwo_edit").val(msg.slot_qty2)
                }
                

                $("#cntrlthree_edit").val(msg.slot3)
                if(msg.slot_qty3 == 0 || msg.slot_qty3 == "" || msg.slot_qty3 == null || msg.slot_qty3 == "null" || msg.slot_qty3 == undefined || msg.slot_qty3 === null || msg.slot_qty3 === "null" || msg.slot_qty3 === undefined){
                    $("#qty_cntrlrthree").val("")
                }else{
                    $("#qty_cntrlrthree").val(msg.slot_qty3)
                }
                

                $("#psu_edit").val(msg.psu_watts)


                if(msg.psu_qty == 0 || msg.psu_qty == "" || msg.psu_qty == null || msg.psu_qty == "null" || msg.psu_qty == undefined || msg.psu_qty === null || msg.psu_qty === "null" || msg.psu_qty === undefined){
                    $("#qty_psu_edit").val("")
                }else{
                    $("#qty_psu_edit").val(msg.psu_qty)
                }
                

                $(`#mounting_edit option:contains(${msg.mounting})`).prop('selected',true);
                
                
                if(msg.psu_qty == 0 || msg.psu_qty == "" || msg.psu_qty == null || msg.psu_qty == "null" || msg.psu_qty == undefined || msg.psu_qty === null || msg.psu_qty === "null" || msg.psu_qty === undefined){
                    $("#qty_mounting_edit").val("")
                }else{
                    $("#qty_mounting_edit").val(msg.mounting_qty)
                }
                

                

                $("#backplane_edit").text(msg.backplane)
                $("#drivebays_edit").text(msg.drivebays)
                $("#rearbays_edit").text(msg.rearbays)
                $("#nodes_edit").text(msg.node)
                $("#psuchassis_edit").text(msg.psu)
                $("#bin_edit").text(msg.bin)

            }
        }) 
    })
});


$(document).ready(function() {
    $("#build_id_mb").change(function(){


        $("#cpu_mb").empty();
        $("#memory_mb").empty();

        var datas = {
            build_id : $("#build_id_mb").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/request/get_build`,
            type : "post",
            data : datas,
            success : function(msg){
                
                

                $(`#motherboard_mb option:contains(${msg.motherboard})`).prop('selected',true);


                if(msg.motherboard_qty == 0 || msg.motherboard_qty == "" || msg.motherboard_qty == null || msg.motherboard_qty == "null" || msg.motherboard_qty == undefined || msg.motherboard_qty === null || msg.motherboard_qty === "null" || msg.motherboard_qty === undefined){
                    $("#qty_motherboard_mb").val("")
                }else{
                    $("#qty_motherboard_mb").val(msg.motherboard_qty)
                }
                

                var $cpu_insert = $("<option selected='selected'></option>").val(msg.cpu).text(msg.cpu)
 
                $("#cpu_mb").append($cpu_insert).trigger('change'); 


                if(msg.cpu_qty == 0 || msg.cpu_qty == "" || msg.cpu_qty == null || msg.cpu_qty == "null" || msg.cpu_qty == undefined || msg.cpu_qty === null || msg.cpu_qty === "null" || msg.cpu_qty === undefined){
                    $("#qty_cpu_mb").val("")
                }else{
                    $("#qty_cpu_mb").val(msg.cpu_qty)
                }

                

                

                var $memory_insert = $("<option selected='selected'></option>").val(msg.memory).text(msg.memory)
 
                $("#memory_mb").append($memory_insert).trigger('change');


                if(msg.memory_qty == 0 || msg.memory_qty == "" || msg.memory_qty == null || msg.memory_qty == "null" || msg.memory_qty == undefined || msg.memory_qty === null || msg.memory_qty === "null" || msg.memory_qty === undefined){
                    $("#qty_memory_mb").val("")
                }else{
                    $("#qty_memory_mb").val(msg.memory_qty)
                }

                

                $("#cntrl_mb").val(msg.slot)


                if(msg.slot_qty == 0 || msg.slot_qty == "" || msg.slot_qty == null || msg.slot_qty == "null" || msg.slot_qty == undefined || msg.slot_qty === null || msg.slot_qty === "null" || msg.slot_qty === undefined){
                    $("#qty_cntrlr_mb").val("")
                }else{
                    $("#qty_cntrlr_mb").val(msg.slot_qty)
                }
                

                $("#cntrltwo_mb").val(msg.slot2)


                if(msg.slot_qty2 == 0 || msg.slot_qty2 == "" || msg.slot_qty2 == null || msg.slot_qty2 == "null" || msg.slot_qty2 == undefined || msg.slot_qty2 === null || msg.slot_qty2 === "null" || msg.slot_qty2 === undefined){
                    $("#qty_cntrlrtwo_mb").val("")
                }else{
                    $("#qty_cntrlrtwo_mb").val(msg.slot_qty2)
                }
                

                $("#cntrlthree_mb").val(msg.slot3)


                if(msg.slot_qty3 == 0 || msg.slot_qty3 == "" || msg.slot_qty3 == null || msg.slot_qty3 == "null" || msg.slot_qty3 == undefined || msg.slot_qty3 === null || msg.slot_qty3 === "null" || msg.slot_qty3 === undefined){
                    $("#qty_cntrlrthree_mb").val("")
                }else{
                    $("#qty_cntrlrthree_mb").val(msg.slot_qty3)
                }
                

                $("#psu_mb").val(msg.psu_watts)


                if(msg.psu_qty == 0 || msg.psu_qty == "" || msg.psu_qty == null || msg.psu_qty == "null" || msg.psu_qty == undefined || msg.psu_qty === null || msg.psu_qty === "null" || msg.psu_qty === undefined){
                    $("#qty_psu_mb").val("")
                }else{
                    $("#qty_psu_mb").val(msg.psu_qty)
                }


                


                    if(msg.motherboard == null || msg.motherboard == "null" || msg.motherboard == ""){
                        $("#cpu_mb").empty();
                        $("#memory_mb").empty();
                    }else{
                        var datas = {
                            motherboard : $("#motherboard_mb").val(),
                            motherboard_id : "",
                            cpu_id : msg.cpu,
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/cpu_for_motherboard2`,
                            type : "post",
                            data : datas,
                            success : function(cpu){
                                    $('#cpu_motherboard2').html(cpu);       
                                  }
                            });

                            
                        var datas = {
                            motherboard : $("#motherboard_mb").val(),
                            cpu_for_edit : "",
                            memory_id : msg.memory,
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/memory_for_motherboard2`,
                            type : "post",
                            data : datas,
                            success : function(motherboard_memory){
                                    $('#memory_motherboard2').html(motherboard_memory);       
                                  }
                            });
                    }
                    
               

                $(`#mounting_mb option:contains(${msg.mounting})`).prop('selected',true); 
                $("#qty_mounting_mb").val(msg.mounting_qty)

                $(`#warranty_ch_mb option:contains(${msg.warranty})`).prop('selected',true); 

                
                

                
                console.log(msg.motherboard)
                console.log(msg.motherboard_qty)
                console.log(msg.cpu)
                console.log(msg.cpu_qty)
                console.log(msg.memory)
                console.log(msg.memory_qty)
                console.log(msg.slot)
                console.log(msg.slot_qty)
                console.log(msg.slot2)
                console.log(msg.slot_qty2)
                console.log(msg.slot3)
                console.log(msg.slot_qty3)
                console.log(msg.psu_watts)
                console.log(msg.psu_qty)
                console.log(msg.mounting)
                console.log(msg.mounting_qty)
                console.log(msg.warranty)
                
                
            }
        }).then(function(){
            var datas = {
                motherboard_ : $("#motherboard_mb").val(),
                cpu_ : $("#cpu_mb").val(),
                memory_ : $("#memory_mb").val(),
                ajax : '1'   
            };
            $.ajax({
                url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                type : "post",
                data : datas,
                success : function(response_ob_nic_pci_slot){
                    var datas = {
                        motherboard_ : $("#motherboard_mb").val(),
                        cpu_ : $("#cpu_mb").val(),
                        memory_ : $("#memory_mb").val(),
                        ajax : '1'   
                    };
                    $.ajax({
                        url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                        type : "post",
                        data : datas,
                        success : function(response_ob_nic_pci_slot){
                            $("#ob_nic_mb").text(response_ob_nic_pci_slot.ob_nic);
                            $("#pcie_slot_mb").text(response_ob_nic_pci_slot.pci_slot);
                                ///$('#motherboard_chassis').html(motherboard);       
                            }
                        });
                        ///$('#motherboard_chassis').html(motherboard);       
                    }
                });
        }); 
    })


    $("#build_id_mb_edit").change(function(){


        $("#cpu_mb_edit").empty();
        $("#memory_mb_edit").empty();

        var datas = {
            build_id : $("#build_id_mb_edit").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/request/get_build`,
            type : "post",
            data : datas,
            success : function(msg){
                
                

                $(`#motherboard_mb_edit option:contains(${msg.motherboard})`).prop('selected',true);


                if(msg.motherboard_qty == 0 || msg.motherboard_qty == "" || msg.motherboard_qty == null || msg.motherboard_qty == "null" || msg.motherboard_qty == undefined || msg.motherboard_qty === null || msg.motherboard_qty === "null" || msg.motherboard_qty === undefined){
                    $("#qty_motherboard_mb_edit").val("")
                }else{
                    $("#qty_motherboard_mb_edit").val(msg.motherboard_qty)
                }
                

                var $cpu_insert = $("<option selected='selected'></option>").val(msg.cpu).text(msg.cpu)
 
                $("#cpu_m_editb").append($cpu_insert).trigger('change'); 


                if(msg.cpu_qty == 0 || msg.cpu_qty == "" || msg.cpu_qty == null || msg.cpu_qty == "null" || msg.cpu_qty == undefined || msg.cpu_qty === null || msg.cpu_qty === "null" || msg.cpu_qty === undefined){
                    $("#qty_cpu_mb_edit").val("")
                }else{
                    $("#qty_cpu_mb_edit").val(msg.cpu_qty)
                }

                

                 


                var $memory_insert = $("<option selected='selected'></option>").val(msg.memory).text(msg.memory)
 
                $("#memory_mb_edit").append($memory_insert).trigger('change');


                if(msg.memory_qty == 0 || msg.memory_qty == "" || msg.memory_qty == null || msg.memory_qty == "null" || msg.memory_qty == undefined || msg.memory_qty === null || msg.memory_qty === "null" || msg.memory_qty === undefined){
                    $("#qty_memory_mb_edit").val("")
                }else{
                    $("#qty_memory_mb_edit").val(msg.memory_qty)
                }

                

                $("#cntrl_mb_edit").val(msg.slot)


                if(msg.slot_qty == 0 || msg.slot_qty == "" || msg.slot_qty == null || msg.slot_qty == "null" || msg.slot_qty == undefined || msg.slot_qty === null || msg.slot_qty === "null" || msg.slot_qty === undefined){
                    $("#qty_cntrlr_mb_edit").val("")
                }else{
                    $("#qty_cntrlr_mb_edit").val(msg.slot_qty)
                }
                

                $("#cntrltwo_mb_edit").val(msg.slot2)


                if(msg.slot_qty2 == 0 || msg.slot_qty2 == "" || msg.slot_qty2 == null || msg.slot_qty2 == "null" || msg.slot_qty2 == undefined || msg.slot_qty2 === null || msg.slot_qty2 === "null" || msg.slot_qty2 === undefined){
                    $("#qty_cntrlrtwo_mb_edit").val("")
                }else{
                    $("#qty_cntrlrtwo_mb_edit").val(msg.slot_qty2)
                }
                

                $("#cntrlthree_mb_edit").val(msg.slot3)


                if(msg.slot_qty3 == 0 || msg.slot_qty3 == "" || msg.slot_qty3 == null || msg.slot_qty3 == "null" || msg.slot_qty3 == undefined || msg.slot_qty3 === null || msg.slot_qty3 === "null" || msg.slot_qty3 === undefined){
                    $("#qty_cntrlrthree_mb_edit").val("")
                }else{
                    $("#qty_cntrlrthree_mb_edit").val(msg.slot_qty3)
                }
                

                $("#psu_mb_edit").val(msg.psu_watts)


                if(msg.psu_qty == 0 || msg.psu_qty == "" || msg.psu_qty == null || msg.psu_qty == "null" || msg.psu_qty == undefined || msg.psu_qty === null || msg.psu_qty === "null" || msg.psu_qty === undefined){
                    $("#qty_psu_mb_edit").val("")
                }else{
                    $("#qty_psu_mb_edit").val(msg.psu_qty)
                }


                


                    if(msg.motherboard == null || msg.motherboard == "null" || msg.motherboard == ""){
                        $("#cpu_mb_edit").empty();
                        $("#memory_mb_edit").empty();
                    }else{
                        var datas = {
                            motherboard : $("#motherboard_mb_edit").val(),
                            motherboard_id : "",
                            cpu_id : msg.cpu,
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_cpu_mb_edit`,
                            type : "post",
                            data : datas,
                            success : function(cpu){
                                    $('#cpu_motherboard_mb_edit').html(cpu);       
                                  }
                            });

                            
                        var datas = {
                            motherboard : $("#motherboard_mb_edit").val(),
                            cpu_for_edit : "",
                            memory_id : msg.memory,
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_memory_mb_edit`,
                            type : "post",
                            data : datas,
                            success : function(motherboard_memory){
                                    $('#memory_motherboard_mb_edit').html(motherboard_memory);       
                                  }
                            }).then(function(){
                                var datas = {
                                    motherboard_ : $("#motherboard_mb_edit").val(),
                                    cpu_ : $("#cpu_mb_edit").val(),
                                    memory_ : $("#memory_mb_edit").val(),
                                    ajax : '1'   
                                };
                                $.ajax({
                                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                                    type : "post",
                                    data : datas,
                                    success : function(response_ob_nic_pci_slot){
                                        var datas = {
                                            motherboard_ : $("#motherboard_mb_edit").val(),
                                            cpu_ : $("#cpu_mb_edit").val(),
                                            memory_ : $("#memory_mb_edit").val(),
                                            ajax : '1'   
                                        };
                                        $.ajax({
                                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                                            type : "post",
                                            data : datas,
                                            success : function(response_ob_nic_pci_slot){
                                                $("#ob_nic_mb_edit").text(response_ob_nic_pci_slot.ob_nic);
                                                $("#pcie_slot_mb_edit").text(response_ob_nic_pci_slot.pci_slot);
                                                    ///$('#motherboard_chassis').html(motherboard);       
                                                }
                                            });
                                            ///$('#motherboard_chassis').html(motherboard);       
                                        }
                                    });
                            });
                    }
                    
               

                $(`#mounting_mb_edit option:contains(${msg.mounting})`).prop('selected',true); 
                $("#qty_mounting_mb_edit").val(msg.mounting_qty)

                $(`#warranty_ch_mb_edit option:contains(${msg.warranty})`).prop('selected',true); 

                
                

                
                console.log(msg.motherboard)
                console.log(msg.motherboard_qty)
                console.log(msg.cpu)
                console.log(msg.cpu_qty)
                console.log(msg.memory)
                console.log(msg.memory_qty)
                console.log(msg.slot)
                console.log(msg.slot_qty)
                console.log(msg.slot2)
                console.log(msg.slot_qty2)
                console.log(msg.slot3)
                console.log(msg.slot_qty3)
                console.log(msg.psu_watts)
                console.log(msg.psu_qty)
                console.log(msg.mounting)
                console.log(msg.mounting_qty)
                console.log(msg.warranty)
                
                
            }
        }); 
    })
});