$(document).ready(function() {

    $("#add_order_table_body").on('click', '.print_order_row', function(event){
        
        var chassis = $(this).closest("tr").find(".chassis").text();


        var motherboard = $(this).closest("tr").find(".item-motherboard").text();
        
        
        var work_order_id = $(this).closest("tr").find(".item-work_order_id").text();
        
        printWorkOrderDetail(motherboard, chassis, work_order_id);
          
      });
      $("#add_order_table_body_edit").on('click', '.print_order_row', function(event){
        
        var chassis = $(this).closest("tr").find(".chassis").text();


        var motherboard = $(this).closest("tr").find(".item-motherboard").text();
        
        
        var work_order_id = $(this).closest("tr").find(".item-work_order_id").text();
        
        
        printWorkOrderDetail_edit(motherboard, chassis, work_order_id);
          
      });

    $("#product_table_body").on('click', '.print', function(event){
        
        var barcode = $(this).closest("tr").find(".barcode").text();
        
        printWorkOrderOverview_on_edit(barcode.trim());
          
      });
    $("#btnPrintOrder").click(function() {
        printWorkOrderOverview();
    });
    $("#print_order_on_edit").click(function() {
        printWorkOrderOverview_on_edit();
    });

    
});



function printWorkOrderOverview_on_edit(barcode) {
    const data = {
        
        ///"barcode": $("#barcode_edit").text(),
        "barcode": barcode,
        
    };

    $.post(`${baseUrl}index.php/work_orders/print_overview_on_edit`, data, function(response) {
        const printWindow = window.open("", "");
        printWindow.document.write(response);
        printWindow.document.close();
        printWindow.focus();
        
        setTimeout(function() {
            printWindow.print();
            printWindow.close();
        }, 500);
    });
}

function printWorkOrderOverview() {
    const data = {
        "barcode_svg": $("<div></div>").append($("#barcode").clone()).html(),
        "barcode": $("#txt_input").val(),
        "created": $("#created").text(),
        "sales_rep": $("#sales_rep").text(),
        "ship_date": $(".datetimepicker-input").val(),
        "ship_method": $("#ship_method").val(),
        "client_name": $("#client_name").val(),
        "client_contact": $("#contact_no").val(),
        "client_email": $("#email").val(),
        "client_address": $("#address").val()
    };

    $.post(`${baseUrl}index.php/work_orders/print_overview`, data, function(response) {
        const printWindow = window.open("", "");
        printWindow.document.write(response);
        printWindow.document.close();
        printWindow.focus();
        
        setTimeout(function() {
            printWindow.print();
            printWindow.close();
        }, 500);
    });
}

function printWorkOrderDetail(motherboard, sku, work_order_id) {
    const data = {
        "sku": sku,
        "barcode_svg": $("<div></div>").append($("#barcode").clone()).html(),
        "barcode": $("#txt_input").val(),
        "created": $("#created").text(),
        "sales_rep": $("#sales_rep").text(),
        "ship_date": $(".datetimepicker-input").val(),
        "ship_method": $("#ship_method").val(),
        "client_name": $("#client_name").val(),
        "client_contact": $("#contact_no").val(),
        "client_email": $("#email").val(),
        "client_address": $("#address").val(),
        'motherboard' : motherboard,
        'work_order_id' : work_order_id
    };

    $.post(`${baseUrl}index.php/work_orders/print_detail`, data, function(response) {
        const printWindow = window.open("", "");
        printWindow.document.write(response);
        printWindow.document.close();
        printWindow.focus();
        
        setTimeout(function() {
            printWindow.print();
            printWindow.close();
        }, 500);
    });
}


function printWorkOrderDetail_edit(motherboard, sku, work_order_id) {
    const data = {
        "sku": sku,
        "barcode_svg": $("<div></div>").append($("#barcode_edit").clone()).html(),
        "barcode": $("#txt_input_edit").val(),
        "created": $("#created_edit").text(),
        "sales_rep": $("#sales_rep_edit").text(),
        "ship_date": $(".datetimepicker-input_edit").val(),
        "ship_method": $("#ship_method_edit").val(),
        "client_name": $("#client_name_edit").val(),
        "client_contact": $("#contact_no_edit").val(),
        "client_email": $("#email_edit").val(),
        "client_address": $("#address_edit").val(),
        'database' : 'work_orders',
        'motherboard' : motherboard,
        'work_order_id' : work_order_id
    };


    $.post(`${baseUrl}index.php/work_orders/print_detail`, data, function(response) {
        const printWindow = window.open("", "");
        printWindow.document.write(response);
        printWindow.document.close();
        printWindow.focus();
        
        setTimeout(function() {
            printWindow.print();
            printWindow.close();
        }, 500);
    });
}
