
$(document).ready(function() {

	baseUrl = $('#workOrderScript').data('baseUrl');
    var timer;
    var dateToday = new Date();
    var confirmDel;

    const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterSearchSelected = parameters.get('searchSelected');
    const parameterOrderBy = parameters.get('orderBy');
    const parameterSortOrder = parameters.get('sortOrder');

    $('#searchSelected').find(':selected').prop('selected', false);
    $(`#searchSelected > option[value="${parameterSearchSelected}"]`).prop('selected', true);

        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'orderBy' : parameterOrderBy,
            'sortOrder': parameterSortOrder
        };

        $('#itemSearch').val(searchItem);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/work_orders/search_item`, data, function(response) {
            $('#product_table_body').html(response);
             $('#exportCSV').prop('disabled', false);
            paginate();
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
        });

        const countUrl = `${baseUrl}index.php/work_orders/load_listing_count`;

        $.get(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
        }
    });

    document.getElementById("txt_input").addEventListener('keyup', function(){

        var text = document.getElementById("txt_input").value;
        JsBarcode("#barcode", text, {

            lineColor: "#000000",
            height: 140,
            marginTop: 40,
            marginBottom: 20
        });
//put condition for existing barcode
        var datas = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/check_barcode_exist`,
            type : "post",
            data : datas,
            success : function(response){
                    if(response.is_error){
                        $('#myModal').scrollTop(0);
                        $("#getcustomerdata").attr("disabled", true);
                        $('#barcode_error').show();
                        timer =setTimeout(function () {$('#barcode_error').hide(500);}, 3000);
                    }else{
                        $("#getcustomerdata").attr("disabled", false);
                    }
                }

        });

         var datas = {
            'barcode' : $('#txt_input').val()
        };
        $.post(`${baseUrl}index.php/work_orders/get_barcode`, datas, function(response) {
            $('#add_order_table_body').html(response);
            paginate();
            });
    });


    $('#exportCSV').click(function() {
        const searchItem = $('#itemSearch').val();   
        const searchSelected = $('#searchSelected').val();   
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();

        const data = {
            'searchItem': searchItem,
            'orderBy': parameterOrderBy,
            'sortOrder': parameterSortOrder,
            'searchSelected' : parameterSearchSelected

        };

        const  url = `${baseUrl}index.php/work_orders/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `work_orders_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

    $('.modal').css('overflow-y', 'auto');

    $('#searchColumn').click(function(event){
        event.preventDefault();
        var searchSelected = $('#searchSelected').val();
        const searchItem = $('#itemSearch').val();
        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        search();
    });

    $('#orderBy').change(function(event) {
        search();
    });

    $('#sortOrder').change(function(event) {
        search();
    });

    $('#success').click(function(){
        $(this).hide(500);
        clearTimeout(timer);
    });

    $('#add_drives').click(function(){
        $('.drives_list_copy').show();
        $('#add_drives').prop('disabled', true);
        $('#add_drives2').prop('disabled', false);
           
        
    });

    $('#add_drives2').click(function(){
        $('.drives_list_copy2').show();
        $('#add_drives2').prop('disabled', true);

    });

    $('#add_drives3').click(function(){
       $('#modalChassis').scrollTop(0);
            $("#error_maxfield").show();
            timer =setTimeout(function () {$('#error_maxfield').hide(500);}, 3000);
    });

     $('#add_controller').click(function(){
        $('.slot_list_copy').show();
        $('#add_controller').prop('disabled', true);
        $('#add_controller2').prop('disabled', false);
           
        
    });

    $('#add_controller2').click(function(){
        $('.slot_list_copy2').show();
        $('#add_controller2').prop('disabled', true);

    });
    
    $('#add_controller3').click(function(){
       $('#modalChassis').scrollTop(0);
            $("#error_maxfield").show();
            timer =setTimeout(function () {$('#error_maxfield').hide(500);}, 3000);
    });

    $('#add_controller_mb').click(function(){
        $('.slot_list_copy_mb').show();
        $('#add_controller_mb').prop('disabled', true);
        $('#add_controller2_mb').prop('disabled', false);
           
        
    });

    $('#add_controller2_mb').click(function(){
        $('.slot_list_copy2_mb').show();
        $('#add_controller2_mb').prop('disabled', true);

    });
    
    $('#add_controller3_mb').click(function(){
       $('#modalMotherboard').scrollTop(0);
            $("#error_maxfield_mb").show();
            timer =setTimeout(function () {$('#error_maxfield_mb').hide(500);}, 3000);
    });

   

    $('.remove_slot2_mb').click(function(){
        $('.slot_list_copy_mb').hide();
        $('.slot_list_copy2_mb').hide();
        $('#add_controller_mb').prop('disabled', false);
    });

    $('.remove_slot3_mb').click(function(){
        $('.slot_list_copy2_mb').hide();
        $('#add_controller2_mb').prop('disabled', false);
    });

   

    $('.remove_slot2').click(function(){
        $('.slot_list_copy').hide();
        $('.slot_list_copy2').hide();
        $('#add_controller').prop('disabled', false);
    });

    $('.remove_slot3').click(function(){
        $('.slot_list_copy2').hide();
        $('#add_controller2').prop('disabled', false);
    });

    $('.remove_drives2').click(function(){
        $('.drives_list_copy').hide();
        $('.drives_list_copy2').hide();
        $('#add_drives').prop('disabled', false);
    });

    $('.remove_drives3').click(function(){
        $('.drives_list_copy2').hide();
        $('#add_drives2').prop('disabled', false);
    });

     $('#remove_chassis').click(function(event){
        var none = '';

        $('#modalChassis').modal('hide');
        $('#modalMotherboard').modal('show');
        $('#motherboard_mb').val(none);
        $('#qty_motherboard_mb').val(none);
        $('#cpu_mb').val(none);
        $('#qty_cpu_mb').val(none);
        $('#memory_mb').val(none);
        $('#qty_memory_mb').val(none);
        $('#cntrl_mb').val(none);
        $('#qty_cntrlr_mb').val(none);
        $('#cntrltwo_mb').val(none);
        $('#qty_cntrlrtwo_mb').val(none);
        $('#cntrlthree_mb').val(none);
        $('#qty_cntrlrthree_mb').val(none);
        $('#psu_mb').val(none);
        $('#qty_psu_mb').val(none);
        $('#qty_mounting').val(none);
        $('#os_mb').val(none);

    });

    $('#include_chassis').click(function(event){
        var none = '';

        $('#modalMotherboard').modal('hide');
        $('#modalChassis').modal('show');
        $('#chasis').val(none);
        $('#qty_chassis').val(none);
        $('#backplane').text(none);
        $('#nodes').text(none);
        $('#drivebays').text(none);
        $('#psuchassis').text(none);
        $('#rearbays').text(none);
        $('#bin').text(none);
        $('#chasis').val(none);
        $('#qty_chassis').val(none);
        $('#front_caddies').val(none);
        $('#qty_front_caddies').val(none);
        $('#rear_caddies').val(none);
        $('#qty_rear_caddies').val(none);
        $('#item_harddrive_one').val('null');
        $('#qty_hard_drive_one').val(none);
        $('#item_harddrive_two').val('null');
        $('#qty_hard_drive_two').val(none);
        $('#item_harddrive_three').val('null');
        $('#qty_hard_drive_three').val(none);
        $('#motherboard').val(none);
        $('#qty_motherboard').val(none);
        $('#cpu').val(none);
        $('#qty_cpu').val(none);
        $('#memory').val(none);
        $('#qty_memory').val(none);
        $('#cntrl').val(none);
        $('#qty_cntrlr').val(none);
        $('#cntrltwo').val(none);
        $('#qty_cntrlrtwo').val(none);
        $('#cntrlthree').val(none);
        $('#qty_cntrlrthree').val(none);
        $('#psu').val(none);
        $('#qty_psu').val(none);
        $('#qty_mounting').val(none);
        $('#os').val(none);

    });

	$("#barcodesearch").click(function(){
    	$('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);
    	var datas = {
            barcode : $("#barcodeitem").val(),
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/search_barcode`,
            type : "post",
            data : datas,
            success : function(orders){
                    $("#display_modal_table").html(orders);       
                }
        });
  	});


  	$("#orderBy").change(function(){
    	var datas = {
            orderby : $("#orderBy").val(),
            search : $("#barcodeitem").val(),
            sortOrder : $("#sortOrder").val(),
            ajax : '1'   
        };
        $.ajax({
            url : "<?php echo base_url();   ?>index.php/work_orders/orderby_work_orders",
            type : "post",
            data : datas,
            success : function(orders){
                    $("#display_modal_table").html(orders);       
                }
        });
  	});


  	$("#sortOrder").change(function(){
    	var datas = {
            orderby : $("#orderBy").val(),
            search : $("#barcodeitem").val(),
            sortOrder : $("#sortOrder").val(),
            ajax : '1'   
        };
        $.ajax({
            url : "<?php echo base_url();   ?>index.php/work_orders/orderby_work_orders",
            type : "post",
            data : datas,
            success : function(orders){
                    $("#display_modal_table").html(orders);       
                }
         });
  	});

    $("#ship_date").datetimepicker({
        format : 'YYYY-MM-DD',
        minDate : dateToday
    });

    $("#product_table_body").on('click', '.deleterow', function(event){
      const btnDelete = $(this).closest('.deleterow');
      if (!btnDelete) return;

       confirmDel = btnDelete.closest("tr").find(".item-barcode").text();
        
    });

     $("#delete_row").on('click', function(event){
        var data = {
            'barcode' : confirmDel,
            ajax : '1'   
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/soft_delete_all`,
            type : "post",
            data : data,
            success : function(orders){
                        location.reload();    
                                                
                }
            });
    });

     //wai labot ni
    $("#add_order_table_body").on('click', '.removerow', function(event){
      const btnDelete = $(this).closest('.removerow');
      if (!btnDelete) return;

        confirmDel = btnDelete.closest("tr").find(".item-work_order_id").text();
        console.log(confirmDel);
    });

     $("#remove_row").on('click', function(event){
        var data_add = {
            'work_order_id' : confirmDel,
            ajax : '1'   
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/delete_row_add_order`,
            type : "post",
            data : data_add,
            success : function(orders){      
                      $('#confirmRemove').modal('hide');                         
                }
            });

         var datas_add = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };

        $.post(`${baseUrl}index.php/work_orders/get_barcode`, datas_add, function(response) {
            $('#add_order_table_body').html(response);
            paginate();
            });
    });

     $("#add_order_table_body_edit").on('click', '.removerow', function(event){
      const btnDelete = $(this).closest('.removerow');
      if (!btnDelete) return;

        softDel = btnDelete.closest("tr").find(".item-work_order_id").text();
        console.log(softDel);
    });

     $("#remove_row").on('click', function(event){
        var data_edit = {
            'work_order_id' : softDel,
            ajax : '1'   
        };
        console.log(data_edit); 
        $.ajax({
            url : `${baseUrl}index.php/work_orders/soft_delete_each`,
            type : "post",
            data : data_edit,
            success : function(orders){      
                      $('#confirmRemove').modal('hide');                         
                }
            });

         var data_edit = {
            'barcode' : $('#txt_input_edit').val(),
            ajax : '1'
        };

        $.post(`${baseUrl}index.php/work_orders/get_barcode_from_wo`, data_edit, function(response) {
            $('#add_order_table_body_edit').html(response);
            paginate();
            });
    });

     $("#close_del").click(function(event){

         var data = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'   
        };

        $.ajax({
            url : `${baseUrl}index.php/work_orders/delete_all_order`,
            type : "post",
            data : data,
            success : function(orders){
                        location.reload();    
                                                
                }
            });
     });


     $("#close_mod").click(function(event){

       location.reload();
     });

     $("#qty_front_caddies").change(function(){

        var data_add_front = {
            'qty_front_caddies' : $('#qty_front_caddies').val(),
            'drivebays' : $('#drivebays').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/front_caddies_qty_checker`,
            type : "post",
            data: data_add_front,
            success: function(response){
                var front_caddies_total = $('#qty_front_caddies').val();
                if(response.is_error){
                    
                    $('#modalChassis').scrollTop(0);
                    $('#qty_front_caddies').css('background-color', '#FF0000');
                    $("#add_order_chassis").prop("disabled", true);
                    $('#front_caddies_total').text(front_caddies_total);
                }else{
                    $("#add_order_chassis").prop("disabled", false);
                    $('#qty_front_caddies').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#front_caddies_total').text(front_caddies_total);
                }
            }
        });
     });

     $("#qty_front_caddies_edit").change(function(){

        var data_add_front = {
            'qty_front_caddies' : $('#qty_front_caddies_edit').val(),
            'drivebays' : $('#drivebays_edit').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/front_caddies_qty_checker`,
            type : "post",
            data: data_add_front,
            success: function(response){
                var qty_front_caddies_edit_total = $('#qty_front_caddies_edit').val();
                if(response.is_error){
                    
                    $('#modalChassis_edit').scrollTop(0);
                    $('#qty_front_caddies_edit').css('background-color', '#FF0000');
                    $("#edit_order_chassis").prop("disabled", true);
                    $('#qty_front_caddies_edit_total').text(qty_front_caddies_edit_total);
                }else{
                    $("#add_order_chassis").prop("disabled", false);
                    $('#qty_front_caddies_edit').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#qty_front_caddies_edit_total').text(qty_front_caddies_edit_total);
                }
            }
        });
     });

     $("#qty_rear_caddies").change(function(){
        var data_edit_rear = {
            'qty_rear_caddies' : $('#qty_rear_caddies').val(),
            'rearbays' : $('#rearbays').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/rear_caddies_qty_checker`,
            type : "post",
            data: data_edit_rear,
            success: function(response){
                var rear_caddies_total = $('#qty_rear_caddies').val();
                if(response.is_error){
                    $('#modalChassis').scrollTop(0);
                    timer =setTimeout(function () {$('#caddies_error').hide(500);}, 3000);
                    $("#add_order_chassis").prop("disabled", true);
                    $('#qty_rear_caddies').css('background-color', '#FF0000');
                    $('#rear_caddies_total').text(rear_caddies_total);

                }else{
                    $("#add_order_chassis").prop("disabled", false);
                    $('#qty_rear_caddies').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#rear_caddies_total').text(rear_caddies_total);
                }
            }
        });
     });

     $("#qty_rear_caddies_edit").change(function(){
        var data_add_rear = {
            'qty_rear_caddies' : $('#qty_rear_caddies_edit').val(),
            'rearbays' : $('#rearbays_edit').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/rear_caddies_qty_checker`,
            type : "post",
            data: data_add_rear,
            success: function(response){
                var rear_caddies_edit_total = $('#qty_rear_caddies_edit').val();
                if(response.is_error){
                    $('#modalChassis_edit').scrollTop(0);
                    timer = setTimeout(function () {$('#caddies_error_edit').hide(500);}, 3000);
                    $("#edit_order_chassis").prop("disabled", true);
                    $('#qty_rear_caddies_edit').css('background-color', '#FF0000');
                    $('#rear_caddies_edit_total').text(rear_caddies_edit_total);
                }else{
                    $("#edit_order_chassis").prop("disabled", false);
                    $('#qty_rear_caddies_edit').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#rear_caddies_edit_total').text(rear_caddies_edit_total);
                }
            }
        });
     });

     $("#qty_motherboard").change(function(){
        $('#motherboard_total').text();
        $('#cpu_total').text();
        $('#memory_total').text();
        var default_mb_qty = {
            'nodes' : $('#nodes').text(),
            'qty_motherboard' : $('#qty_motherboard').val(),
            'qty_chassis' : $('#qty_chassis').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/motherboard_qty_checker`,
            type : "post",
            data: default_mb_qty,
            success: function(response){
                var motherboard_total = $('#qty_motherboard').val();

                if(response.is_error){
                    $("#add_order_chassis").prop("disabled", true);
                    $('#qty_motherboard').css('background-color', '#FF0000');
                   $('#motherboard_total').text(' ');
                }else{
                    $("#add_order_chassis").prop("disabled", false);
                    $('#qty_motherboard').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#motherboard_total').text(motherboard_total);
                }

            }
        });

        var automate_motherboard_qty = {
            'nodes' : $('#nodes').text(),
            'qty_motherboard' : $('#qty_motherboard').val(),
            'qty_chassis' : $('#qty_chassis').val(),
            'cpu_total' : $('#qty_cpu').val(),
            'memory_total' : $('#qty_memory').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_motherboard`,
            type : "post",
            data: automate_motherboard_qty,
            success : function(response){
                var motherboard_total = $('#qty_motherboard').val();

                if(motherboard_total == '0' || motherboard_total == '' || motherboard_total == null){
                    $('#cpu_total').text();
                    $('#memory_total').text();
                }
                else if(response.chassis_cpu == '0'){
                    $('#cpu_total').text();
                    $('#memory_total').text(response.chassis_memory);
                }
                else if(response.chassis_memory == '0'){
                    $('#memory_total').text();
                    $('#cpu_total').text(response.chassis_cpu);
                }else if(response.chassis_cpu == '0' && response.chassis_memory == '0'){
                    $('#memory_total').text();
                    $('#cpu_total').text();
                }else{
                    $('#cpu_total').text(response.chassis_cpu);
                    $('#memory_total').text(response.chassis_memory);
                }
            }
        });
     });

     $("#qty_motherboard_edit").change(function(){
        $('#qty_motherboard_edit_total').text();
        $('#qty_cpu_edit_total').text();
        $('#qty_memory_edit_total').text();
        var default_mb_qty = {
            'nodes' : $('#nodes_edit').text(),
            'qty_motherboard' : $('#qty_motherboard_edit').val(),
            'qty_chassis' : $('#qty_chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/motherboard_qty_checker`,
            type : "post",
            data: default_mb_qty,
            success: function(response){
                var motherboard_total = $('#qty_motherboard_edit').val();

                if(response.is_error){
                    $("#edit_order_chassis").prop("disabled", true);
                    $('#qty_motherboard_edit').css('background-color', '#FF0000');
                   $('#qty_motherboard_edit_total').text(' ');
                }else{
                    $("#edit_order_chassis").prop("disabled", false);
                    $('#qty_motherboard_edit').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#qty_motherboard_edit_total').text(motherboard_total);
                }

            }
        });

        var automate_motherboard_qty = {
            'nodes' : $('#nodes_edit').text(),
            'qty_motherboard' : $('#qty_motherboard_edit').val(),
            'qty_chassis' : $('#qty_chassis_edit').val(),
            'cpu_total' : $('#qty_cpu_edit').val(),
            'memory_total' : $('#qty_memory_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_motherboard`,
            type : "post",
            data: automate_motherboard_qty,
            success : function(response){
                var motherboard_total = $('#qty_motherboard_edit').val();

                if(motherboard_total == '0' || motherboard_total == '' || motherboard_total == null){
                    $('#qty_cpu_edit_total').text();
                    $('#qty_memory_edit_total').text();
                }
                else if(response.chassis_cpu == '0'){
                    $('#qty_cpu_edit_total').text();
                    $('#qty_memory_edit_total').text(response.chassis_memory);
                }
                else if(response.chassis_memory == '0'){
                    $('#qty_memory_edit_total').text();
                    $('#qty_cpu_edit_total').text(response.chassis_cpu);
                }else if(response.chassis_cpu == '0' && response.chassis_memory == '0'){
                    $('#qty_memory_edit_total').text();
                    $('#qty_cpu_edit_total').text();
                }else{
                    $('#qty_cpu_edit_total').text(response.chassis_cpu);
                    $('#qty_memory_edit_total').text(response.chassis_memory);
                }
            }
        });
     });

     $('#qty_motherboard_mb, #qty_motherboard_mb_edit').change(function(){
        var qty_motherboard_mb = $('#qty_motherboard_mb').val();
        $('#motherboard_mb_total').text(qty_motherboard_mb);

        var qty_motherboard_mb_edit = $('#qty_motherboard_mb_edit').val();
        $('#qty_motherboard_mb_edit_total').text(qty_motherboard_mb_edit);
     });

     $('#qty_cpu').change(function(){
        var automate_cpu_qty = {
            'qty_cpu' : $('#qty_cpu').val(),
            'motherboard_total' : $('#qty_motherboard').val(),
            'qty_chassis' : $('#qty_chassis').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_cpu_qty`,
            type : "post",
            data: automate_cpu_qty,
            success: function(response){
               
                    $('#cpu_total').text(response.automate_cpu);    
            }
        });
     });

     $('#qty_cpu_edit').change(function(){
        var automate_cpu_qty = {
            'qty_cpu' : $('#qty_cpu_edit').val(),
            'motherboard_total' : $('#qty_motherboard_edit').val(),
            'qty_chassis' : $('#qty_chassis_edit').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_cpu_qty`,
            type : "post",
            data: automate_cpu_qty,
            success: function(response){
               
                    $('#qty_cpu_edit_total').text(response.automate_cpu);    
            }
        });
     });

     $('#qty_cpu_mb').change(function(){
        var automate_cpu_qty = {
            'qty_cpu' : $('#qty_cpu_mb').val(),
            'motherboard' : $('#qty_motherboard_mb').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_cpu_qty_mb`,
            type : "post",
            data: automate_cpu_qty,
            success: function(response){
               
                    $('#cpu_mb_total').text(response.automate_cpu_mb);    
            }
        });
     });

     $('#qty_cpu_mb_edit').change(function(){
        var automate_cpu_qty = {
            'qty_cpu' : $('#qty_cpu_mb_edit').val(),
            'motherboard' : $('#qty_motherboard_mb_edit').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_cpu_qty_mb`,
            type : "post",
            data: automate_cpu_qty,
            success: function(response){
               
                    $('#qty_cpu_mb_edit_total').text(response.automate_cpu_mb);    
            }
        });
     });


     $('#qty_memory').change(function(){
        var automate_memory_qty = {
            'qty_memory' : $('#qty_memory').val(),
            'motherboard_total' : $('#qty_motherboard').val(),
            'qty_chassis' : $('#qty_chassis').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_memory_qty`,
            type : "post",
            data: automate_memory_qty,
            success: function(response){
                
                    $('#memory_total').text(response.automate_memory);
            
            }
        });

     });

     $('#qty_memory_edit').change(function(){
        var automate_memory_qty = {
            'qty_memory' : $('#qty_memory_edit').val(),
            'motherboard_total' : $('#qty_motherboard_edit').val(),
            'qty_chassis' : $('#qty_chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_memory_qty`,
            type : "post",
            data: automate_memory_qty,
            success: function(response){
                
                    $('#qty_memory_edit_total').text(response.automate_memory);
            
            }
        });

     });

     $('#qty_memory_mb').change(function(){
        var automate_memory_qty = {
            'qty_memory' : $('#qty_memory_mb').val(),
            'motherboard' : $('#qty_motherboard_mb').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_memory_qty_mb`,
            type : "post",
            data: automate_memory_qty,
            success: function(response){
                
                    $('#memory_mb_total').text(response.automate_memory_mb);
            
            }
        });
     });

     $('#qty_memory_mb_edit').change(function(){
        var automate_memory_qty = {
            'qty_memory' : $('#qty_memory_mb_edit').val(),
            'motherboard' : $('#qty_motherboard_mb_edit').val(),
            ajax : '1'
        };
 
        $.ajax({
            url: `${baseUrl}index.php/work_orders/automate_memory_qty_mb`,
            type : "post",
            data: automate_memory_qty,
            success: function(response){
                
                    $('#qty_memory_mb_edit_total').text(response.automate_memory_mb);
            
            }
        });
     });

     $('#qty_psu').change(function(){
        var psu_checker = {
            'psuchassis' : $('#psuchassis').text(),
            'qty_psu': $('#qty_psu').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/psu_checker`,
            type : "post",
            data: psu_checker,
            success : function(response){
                const psu_total = $("#qty_psu").val();
                if(response.is_error){
                    $("#add_order_chassis").prop("disabled", true);
                    $('#qty_psu').css('background-color', '#FF0000');
                    $('#psu_total').text(psu_total);
                }else{
                    $("#add_order_chassis").prop("disabled", false);
                    $('#qty_psu').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#psu_total').text(psu_total);
                }
            }
        });
     });

     $('#qty_psu_edit').change(function(){
        var psu_checker = {
            'psuchassis' : $('#psuchassis_edit').text(),
            'qty_psu': $('#qty_psu_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/psu_checker`,
            type : "post",
            data: psu_checker,
            success : function(response){
                const qty_psu_edit_total = $("#qty_psu_edit").val();
                if(response.is_error){
                    $("#edit_order_chassis").prop("disabled", true);
                    $('#qty_psu_edit').css('background-color', '#FF0000');
                    $('#qty_psu_edit_total').text(qty_psu_edit_total);
                }else{
                    $("#edit_order_chassis").prop("disabled", false);
                    $('#qty_psu_edit').css('background-color', ' rgba(0,0,0,0.0)');
                    $('#qty_psu_edit_total').text(qty_psu_edit_total);
                }
            }
        });
     });

     $('#qty_psu_mb, #qty_psu_mb_edit').change(function(){
        var psu_mb_total = $('#qty_psu_mb').val()
        if(psu_mb_total == '' || psu_mb_total == '0'){
            $('#qty_psu_mb_total').text(' ');
        }else{
            $('#qty_psu_mb_total').text(psu_mb_total);
        }

        var qty_psu_mb_edit = $('#qty_psu_mb_edit').val()
        if(psu_mb_total == '' || psu_mb_total == '0'){
            $('#qty_psu_mb_edit_total').text(' ');
        }else{
            $('#qty_psu_mb_edit_total').text(qty_psu_mb_edit);
        }
     });

     $('#qty_chassis').change(function(){

        $('#motherboard_total').text();
        $('#cpu_total').text();
        $('#memory_total').text();
        var qty_chassis_checker = {
            'motherboard_total' : $('#qty_motherboard').val(),
            'cpu_total' : $('#qty_cpu').val(),
            'memory_total' : $('#qty_memory').val(),
            'qty_chassis' : $('#qty_chassis').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/qty_chassis_checker`,
            type : "post",
            data: qty_chassis_checker,
            success : function(response){

                $('#motherboard_total').text(response.chassis_mb);
                $('#cpu_total').text(response.chassis_cpu);
                $('#memory_total').text(response.chassis_memory);
                
               
            }
        });

     });

     $('#qty_chassis_edit').change(function(){

        $('#qty_motherboard_edit_total').text();
        $('#qty_cpu_edit_total').text();
        $('#qty_memory_edit_total').text();
        var qty_chassis_checker = {
            'motherboard_total' : $('#qty_motherboard_edit').val(),
            'cpu_total' : $('#qty_cpu_edit').val(),
            'memory_total' : $('#qty_memory_edit').val(),
            'qty_chassis' : $('#qty_chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/qty_chassis_checker`,
            type : "post",
            data: qty_chassis_checker,
            success : function(response){

                $('#qty_motherboard_edit_total').text(response.chassis_mb);
                $('#qty_cpu_edit_total').text(response.chassis_cpu);
                $('#qty_memory_edit_total').text(response.chassis_memory);
                
               
            }
        });

     });

     $('#qty_mounting').change(function(){
        var mounting_qty_checker = {
            'mounting_qty' : $('#qty_mounting').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/qty_mounting_checker`,
            type : "post",
            data: mounting_qty_checker,
            success : function(response){
                $('#mounting_total').text(response.mounting_qty);       
            }
        });
     });

     $('#qty_mounting_edit').change(function(){
        var mounting_qty_checker = {
            'mounting_qty' : $('#qty_mounting_edit').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/qty_mounting_checker`,
            type : "post",
            data: mounting_qty_checker,
            success : function(response){
                $('#qty_mounting_edit_total').text(response.mounting_qty);       
            }
        });
     });

     $('#qty_mounting_mb, #qty_mounting_mb_edit').change(function(){
        var qty_mounting_mb_total = $('#qty_mounting_mb').val();
        if(qty_mounting_mb_total == ' ' || qty_mounting_mb_total == '0'){
            $('#qty_mounting_mb_total').text('');
        }else{
            $('#qty_mounting_mb_total').text(qty_mounting_mb_total);
        }

        var qty_mounting_mb_edit = $('#qty_mounting_mb_edit').val();
        if(qty_mounting_mb_edit == ' ' || qty_mounting_mb_edit == '0'){
            $('#qty_mounting_mb_edit_total').text('');
        }else{
            $('#qty_mounting_mb_edit_total').text(qty_mounting_mb_edit);
        }
     });

     $('#qty_hard_drive_one, #qty_hard_drive_one_edit').change(function(){
        var drives1_total = $('#qty_hard_drive_one').val();
        if(drives1_total == ' ' || drives1_total == '0'){
            $('#drives1_total').text('');
        }else{
            $('#drives1_total').text(drives1_total);
        }

        var drives1_total_edit = $('#qty_hard_drive_one_edit').val();
        if(drives1_total_edit == ' ' || drives1_total_edit == '0'){
            $('#qty_hard_drive_one_edit_total').text('');
        }else{
            $('#qty_hard_drive_one_edit_total').text(drives1_total_edit);
        }
     });

     $('#qty_hard_drive_two, #qty_hard_drive_two_edit').change(function(){
        var drives2_total = $('#qty_hard_drive_two').val();
        if(drives2_total == ' ' || drives2_total == '0'){
            $('#drives2_total').text('');
        }else{
            $('#drives2_total').text(drives2_total);
        }

        var drives2_total_edit = $('#qty_hard_drive_two_edit').val();
        if(drives2_total_edit == ' ' || drives2_total_edit == '0'){
            $('#qty_hard_drive_two_edit_total').text('');
        }else{
            $('#qty_hard_drive_two_edit_total').text(drives2_total_edit);
        }
     });

     $('#qty_hard_drive_three, #qty_hard_drive_three_edit').change(function(){
        var drives3_total = $('#qty_hard_drive_three').val();
        if(drives3_total == ' ' || drives3_total == '0'){
            $('#drives3_total').text('');
        }else{
            $('#drives3_total').text(drives3_total);
        }

        var drives3_edit_total = $('#qty_hard_drive_three_edit').val();
        if(drives3_edit_total == ' ' || drives3_edit_total == '0'){
            $('#qty_hard_drive_three_edit_total').text('');
        }else{
            $('#qty_hard_drive_three_edit_total').text(drives3_edit_total);
        }
     });

     $('#qty_cntrlr, #qty_cntrlr_mb, #qty_cntrlr_edit, #qty_cntrlr_mb_edit').change(function(){
        var slot1_total = $('#qty_cntrlr').val();

        if(slot1_total == ' ' || slot1_total == '0'){
            $('#slot1_total').text('');
        }else{
            $('#slot1_total').text(slot1_total);
        }

        var slot1_mb_total = $('#qty_cntrlr_mb').val();
        if(slot1_mb_total == ' ' || slot1_mb_total == '0'){
            $('#qty_cntrlr_mb_total').text('');
        }else{
            $('#qty_cntrlr_mb_total').text(slot1_mb_total);
        }

         var slot1_edit_total = $('#qty_cntrlr_edit').val();

        if(slot1_edit_total == ' ' || slot1_edit_total == '0'){
            $('#qty_cntrlr_edit_total').text('');
        }else{
            $('#qty_cntrlr_edit_total').text(slot1_edit_total);
        }

        var slot1_mb_edit_total = $('#qty_cntrlr_mb_edit').val();
        if(slot1_mb_edit_total == ' ' || slot1_mb_edit_total == '0'){
            $('#qty_cntrlr_mb_edit_total').text('');
        }else{
            $('#qty_cntrlr_mb_edit_total').text(slot1_mb_edit_total);
        }
     });

     $('#qty_cntrlrtwo, #qty_cntrlrtwo_mb, #qty_cntrlrtwo_edit, #qty_cntrlrtwo_mb_edit').change(function(){
        var slot2_total = $('#qty_cntrlrtwo').val();
        if(slot2_total == ' ' || slot2_total == '0'){
            $('#slot2_total').text('');
        }else{
            $('#slot2_total').text(slot2_total);
        }

        var slot2_mb_total = $('#qty_cntrlrtwo_mb').val();
        if(slot2_mb_total == ' ' || slot2_mb_total == '0'){
            $('#qty_cntrlrtwo_mb_total').text('');
        }else{
            $('#qty_cntrlrtwo_mb_total').text(slot2_mb_total);
        }

        var slot2_edit_total = $('#qty_cntrlrtwo_edit').val();
        if(slot2_edit_total == ' ' || slot2_edit_total == '0'){
            $('#qty_cntrlrtwo_edit_total').text('');
        }else{
            $('#qty_cntrlrtwo_edit_total').text(slot2_edit_total);
        }

         var slot2_mb_edit_total = $('#qty_cntrlrtwo_mb_edit').val();
        if(slot2_mb_edit_total == ' ' || slot2_mb_edit_total == '0'){
            $('#qty_cntrlrtwo_mb_edit_two_total').text('');
        }else{
            $('#qty_cntrlrtwo_mb_edit_two_total').text(slot2_mb_edit_total);
        }
     });

     $('#qty_cntrlrthree, #qty_cntrlrthree_mb, #qty_cntrlrthree_edit, #qty_cntrlrthree_mb_edit').change(function(){
        var slot3_total = $('#qty_cntrlrthree').val();
        if(slot3_total == ' ' || slot3_total == '0'){
            $('#slot3_total').text('');
        }else{
            $('#slot3_total').text(slot3_total);
        }

        var slot3_mb_total = $('#qty_cntrlrthree_mb').val();
        if(slot3_mb_total == ' ' || slot3_mb_total == '0'){
            $('#qty_cntrlrthree_mb_total').text('');
        }else{
            $('#qty_cntrlrthree_mb_total').text(slot3_mb_total);
        }

        var slot3_edit_total = $('#qty_cntrlrthree_edit').val();
        if(slot3_edit_total == ' ' || slot3_edit_total == '0'){
            $('#qty_cntrlrthree_edit_total').text('');
        }else{
            $('#qty_cntrlrthree_edit_total').text(slot3_edit_total);
        }

        var slot3_mb_edit_total = $('#qty_cntrlrthree_mb_edit').val();
        if(slot3_mb_edit_total == ' ' || slot3_mb_edit_total == '0'){
            $('#qty_cntrlrthree_mb_edit_total').text('');
        }else{
            $('#qty_cntrlrthree_mb_edit_total').text(slot3_mb_edit_total);
        }
     });

     $('#chasis, #chassis_edit').select2({
        width: 440,
        tags: true
     });

     $(`#front_caddies, #rear_caddies, #item_harddrive_one, #item_harddrive_two, #item_harddrive_three,
        #motherboard, #cpu, #memory, #mounting, #motherboard_mb, #cpu_mb, #memory_mb, #mounting_mb, #front_caddies_edit
        #rear_caddies_edit, #item_harddrive_one_edit, #item_harddrive_two_edit, #item_harddrive_three_edit,
        #motherboard_edit, #cpu_edit, #memory_edit, #mounting_edit, #motherboard_mb_edit, #cpu_mb_edit, 
        #memory_mb_edit, #mounting_mb_edit`).select2({
        width: 315,
        tags: true
     });

     $('#warranty_ch, #warranty_mb, #warranty_ch_edit, #warranty_mb_edit').select2({
        width: 640,
        tags: true
     });

     $('#build_id, #build_id_mb, #build_id_edit, #build_id_mb_edit').select2({
        width: 250,
        tags: true
     });

    $('#insertOrder').click(function(){
        $('#myModal').modal({backdrop: 'static', keyboard: false} );
    });
    $('#submit_all_orders').prop('disabled', true);
    $('#submit_all_orders').click(function(){
        localStorage.removeItem('qty_motherboard_mb');
        localStorage.removeItem('qty_cpu_mb');
        localStorage.removeItem('qty_memory_mb');
        localStorage.removeItem('qty_cntrlr_mb');
        localStorage.removeItem('qty_cntrlrtwo_mb');
        localStorage.removeItem('qty_cntrlrthree_mb');
        localStorage.removeItem('cntrl_mb');
        localStorage.removeItem('cntrltwo_mb');
        localStorage.removeItem('cntrlthree_mb');
        localStorage.removeItem('qty_psu_mb');
        localStorage.removeItem('qty_mounting_mb');
        localStorage.removeItem('os_mb');
        localStorage.removeItem('barcode');
        localStorage.removeItem('ship_date');
        localStorage.removeItem('details_all');
        localStorage.removeItem('qty_chassis');
        localStorage.removeItem('qty_front_caddies');
        localStorage.removeItem('qty_rear_caddies');
        localStorage.removeItem('qty_hard_drive_one');
        localStorage.removeItem('qty_hard_drive_two');
        localStorage.removeItem('qty_hard_drive_three');
        localStorage.removeItem('qty_cpu');
        localStorage.removeItem('qty_memory');
        localStorage.removeItem('qty_cntrlr');
        localStorage.removeItem('qty_cntrlrtwo');
        localStorage.removeItem('qty_cntrlrthree');
        localStorage.removeItem('cntrl');
        localStorage.removeItem('cntrltwo');
        localStorage.removeItem('cntrlthree');
        localStorage.removeItem('qty_psu');
        localStorage.removeItem('psu');
        localStorage.removeItem('os');
        localStorage.removeItem('chasis');
        localStorage.removeItem('front_caddies');
        localStorage.removeItem('rear_caddies');
        localStorage.removeItem('item_harddrive_one');
        localStorage.removeItem('item_harddrive_two');
        localStorage.removeItem('item_harddrive_three');
        localStorage.removeItem('build_id');
        localStorage.removeItem('build_id_mb');
        localStorage.removeItem('motherboard_mb');
        localStorage.removeItem('qty_mounting');
        localStorage.removeItem('modalshow');
        

       localStorage.clear();


        let clearStorageExcept = function(exceptions) {
            let keys = [];
            exceptions = [].concat(exceptions); // prevent undefined
          
            // get storage keys
            $.each(localStorage, (key) => {
              keys.push(key);
            });
          
            // loop through keys
            for (let i = 0; i < keys.length; i++) {
              let key = keys[i];
              let deleteItem = true;
          
              // check if key excluded
              for (let j = 0; j < exceptions.length; j++) {
                let exception = exceptions[j];
                if (key == exception) {
                  deleteItem = false;
                }
              }
          
              // delete key
              if (deleteItem) {
                localStorage.removeItem(key);
              }
            }
          };
        var mailformat = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])/;
        if($('#txt_input').val() == '' ||  $('#ship_date input').val() == '' || $('#ship_method').val() == '' || 
            $('#details_all').val() == ''){
            $('#myModal').scrollTop(0); 
            $('#error_customerdata').show();
           timer =setTimeout(function () {$('#error_customerdata').hide(500);}, 3000);
        }else {
            event.preventDefault();
        var data = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };

        $.ajax({
             url : `${baseUrl}index.php/work_orders/insert_all_order`,
            type : "post",
            data : data,
            success : function(orders){


                $(`#chasis, #qty_chassis,
            
            #rear_caddies,
            #item_harddrive_one,
            #item_harddrive_two,
            #item_harddrive_three,
            #motherboard,
            #cpu,
            #memory,
            #cntrl,
            #cntrltwo,
            #cntrlthree,
            #psu,
            #os,
            #build_id,
            #qty_chassis,
            #qty_front_caddies,
            #qty_rear_caddies,
            #qty_hard_drive_one,
            #qty_hard_drive_two,
            #qty_hard_drive_three,
            #qty_motherboard,
            #qty_cpu,
            #qty_memory,
            #ob_nic,
            #pcie,
            #qty_cntrlr,
            #qty_cntrlrtwo,
            #qty_cntrlrthree,
            #qty_psu,
            #qty_mounting,
            .datetimepicker-input,
            #ship_method,
            #details_all,
            #txt_input`).val("");


            $(`#motherboard_mb,
            #cpu_mb,
            #memory_mb,
            #cntrl_mb,
            #cntrltwo_mb,
            #cntrlthree_mb,
            #psu_mb,
            #psu_mb,
            #build_id_mb,
            #qty_motherboard_mb,
            #qty_cpu_mb,
            #qty_memory_mb,
            #qty_cntrlr_mb,
            #qty_cntrlrtwo_mb,
            #qty_cntrlrthree_mb,
            #qty_psu_mb,
            #qty_mounting_mb`).val("");


            $(`#chasis option:contains(${''})`).prop('selected',true);
                var $chassis_ = $("<option selected='selected'></option>").val('').text('')
                $("#chasis").append($chassis_).trigger('change');


                $(`#front_caddies option:contains(${''})`).prop('selected',true);
                var $front_caddies_ = $("<option selected='selected'></option>").val('').text('')
                $("#front_caddies").append($front_caddies_).trigger('change');


                $(`#rear_caddies option:contains(${''})`).prop('selected',true);
                var $rear_caddies_ = $("<option selected='selected'></option>").val('').text('')
                $("#rear_caddies").append($rear_caddies_).trigger('change');


                $(`#item_harddrive_one option:contains(${''})`).prop('selected',true);
                var $item_harddrive_one_ = $("<option selected='selected'></option>").val('').text('')
                $("#item_harddrive_one").append($item_harddrive_one_).trigger('change');


                $(`#item_harddrive_two option:contains(${''})`).prop('selected',true);
                var $item_harddrive_two_ = $("<option selected='selected'></option>").val('').text('')
                $("#item_harddrive_two").append($item_harddrive_two_).trigger('change');


                $(`#item_harddrive_three option:contains(${''})`).prop('selected',true);
                var $item_harddrive_three_ = $("<option selected='selected'></option>").val('').text('')
                $("#item_harddrive_three").append($item_harddrive_three_).trigger('change');


                $(`#motherboard option:contains(${''})`).prop('selected',true);
                var $motherboard_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard").append($motherboard_).trigger('change');


                $(`#cpu option:contains(${''})`).prop('selected',true);
                var $cpu_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu").append($cpu_).trigger('change');


                $(`#memory option:contains(${''})`).prop('selected',true);
                var $memory_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory").append($memory_).trigger('change');


                $(`#build_id option:contains(${''})`).prop('selected',true);
                var $build_id_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id").append($build_id_).trigger('change');


                $(`#motherboard_mb option:contains(${''})`).prop('selected',true);
                var $motherboard_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard_mb").append($motherboard_mb_).trigger('change');


                $(`#cpu_mb option:contains(${''})`).prop('selected',true);
                var $cpu_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu_mb").append($cpu_mb_).trigger('change');


                $(`#memory_mb option:contains(${''})`).prop('selected',true);
                var $memory_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory_mb").append($memory_mb_).trigger('change');


                $(`#build_id_mb option:contains(${''})`).prop('selected',true);
                var $build_id_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id_mb").append($build_id_mb_).trigger('change');


                /////////////////////////////////////////////////////////////


            $(`#motherboard_mb option:contains(${''})`).prop('selected',true);
                var $motherboard_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard_mb").append($motherboard_mb_).trigger('change');


                $(`#cpu_mb option:contains(${''})`).prop('selected',true);
                var $cpu_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu_mb").append($cpu_mb_).trigger('change');


                $(`#memory_mb option:contains(${''})`).prop('selected',true);
                var $memory_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory_mb").append($memory_mb_).trigger('change');


                $(`#build_id_mb option:contains(${''})`).prop('selected',true);
                var $build_id_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id_mb").append($build_id_mb_).trigger('change');

            $(`#ship_method option:contains(${''})`).prop('selected',true);
                var $ship_method_ = $("<option selected='selected'></option>").val('').text('')
                $("#ship_method").append($ship_method_).trigger('change');
                       location.reload();   
                                                
                }
        });


        localStorage.removeItem('qty_motherboard_mb');
        localStorage.removeItem('qty_cpu_mb');
        localStorage.removeItem('qty_memory_mb');
        localStorage.removeItem('qty_cntrlr_mb');
        localStorage.removeItem('qty_cntrlrtwo_mb');
        localStorage.removeItem('qty_cntrlrthree_mb');
        localStorage.removeItem('cntrl_mb');
        localStorage.removeItem('cntrltwo_mb');
        localStorage.removeItem('cntrlthree_mb');
        localStorage.removeItem('qty_psu_mb');
        localStorage.removeItem('qty_mounting_mb');
        localStorage.removeItem('os_mb');
        localStorage.removeItem('barcode');
        localStorage.removeItem('ship_date');
        localStorage.removeItem('details_all');
        localStorage.removeItem('qty_chassis');
        localStorage.removeItem('qty_front_caddies');
        localStorage.removeItem('qty_rear_caddies');
        localStorage.removeItem('qty_hard_drive_one');
        localStorage.removeItem('qty_hard_drive_two');
        localStorage.removeItem('qty_hard_drive_three');
        localStorage.removeItem('qty_cpu');
        localStorage.removeItem('qty_memory');
        localStorage.removeItem('qty_cntrlr');
        localStorage.removeItem('qty_cntrlrtwo');
        localStorage.removeItem('qty_cntrlrthree');
        localStorage.removeItem('cntrl');
        localStorage.removeItem('cntrltwo');
        localStorage.removeItem('cntrlthree');
        localStorage.removeItem('qty_psu');
        localStorage.removeItem('psu');
        localStorage.removeItem('os');
        localStorage.removeItem('chasis');
        localStorage.removeItem('front_caddies');
        localStorage.removeItem('rear_caddies');
        localStorage.removeItem('item_harddrive_one');
        localStorage.removeItem('item_harddrive_two');
        localStorage.removeItem('item_harddrive_three');
        localStorage.removeItem('build_id');
        localStorage.removeItem('build_id_mb');
        localStorage.removeItem('motherboard_mb');
        localStorage.removeItem('qty_mounting');
        localStorage.removeItem('modalshow');

        localStorage.clear();


     
return
         var data = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
         };

         $.ajax({
             url : `${baseUrl}index.php/work_orders/delete_all_order`,
            type : "post",
            data : data,
            success : function(orders){
                       location.reload();   
                                                
                }
        });
     }


     localStorage.removeItem('qty_motherboard_mb');
        localStorage.removeItem('qty_cpu_mb');
        localStorage.removeItem('qty_memory_mb');
        localStorage.removeItem('qty_cntrlr_mb');
        localStorage.removeItem('qty_cntrlrtwo_mb');
        localStorage.removeItem('qty_cntrlrthree_mb');
        localStorage.removeItem('cntrl_mb');
        localStorage.removeItem('cntrltwo_mb');
        localStorage.removeItem('cntrlthree_mb');
        localStorage.removeItem('qty_psu_mb');
        localStorage.removeItem('qty_mounting_mb');
        localStorage.removeItem('os_mb');
        localStorage.removeItem('barcode');
        localStorage.removeItem('ship_date');
        localStorage.removeItem('details_all');
        localStorage.removeItem('qty_chassis');
        localStorage.removeItem('qty_front_caddies');
        localStorage.removeItem('qty_rear_caddies');
        localStorage.removeItem('qty_hard_drive_one');
        localStorage.removeItem('qty_hard_drive_two');
        localStorage.removeItem('qty_hard_drive_three');
        localStorage.removeItem('qty_cpu');
        localStorage.removeItem('qty_memory');
        localStorage.removeItem('qty_cntrlr');
        localStorage.removeItem('qty_cntrlrtwo');
        localStorage.removeItem('qty_cntrlrthree');
        localStorage.removeItem('cntrl');
        localStorage.removeItem('cntrltwo');
        localStorage.removeItem('cntrlthree');
        localStorage.removeItem('qty_psu');
        localStorage.removeItem('psu');
        localStorage.removeItem('os');
        localStorage.removeItem('chasis');
        localStorage.removeItem('front_caddies');
        localStorage.removeItem('rear_caddies');
        localStorage.removeItem('item_harddrive_one');
        localStorage.removeItem('item_harddrive_two');
        localStorage.removeItem('item_harddrive_three');
        localStorage.removeItem('build_id');
        localStorage.removeItem('build_id_mb');
        localStorage.removeItem('motherboard_mb');
        localStorage.removeItem('qty_mounting');
        localStorage.removeItem('modalshow');

        localStorage.clear();

    });

	var json = "";
    var hd = false;
    var addhd = 0;
  $("#getcustomerdata").click(function(){
    
    var datas = {
        created : $("#created").text(),
        ship_date : $("#ship_date input").val(),
        ship_method : $("#ship_method").val(),
        sales_rep : $("#sales_rep").text(),
        client_name : $("#client_name").val(),
        contact_no : $("#contact_no").val(),
        email : $("#email").val(),
        address : $("#address").val(),
        input_barcode : $("#txt_input").val(),
        details_all : $('#details_all').val().split("\n")
       };
      json = datas;
  });

    $('#txt_input').change(function(){
         var datas = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/check_barcode_exist`,
            type : "post",
            data : datas,
            success : function(response){
                    if(response.is_error){
                        $('#myModal').scrollTop(0);
                        $("#getcustomerdata").attr("disabled", true);
                        $('#barcode_error').show();
                        timer =setTimeout(function () {$('#barcode_error').hide(500);}, 3000);  
                    }else{
                        $("#getcustomerdata").attr("disabled", false);
                    }
                }

        });
    });

	$('#getcustomerdata').click(function(){
        var none = '';
        var mailformat = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])/;
        if($('#txt_input').val() == '' ||  $('#ship_date input').val() == '' || $('#ship_method').val() == '' || 
            $('#details_all').val() == '' ){
           $('#myModal').scrollTop(0); 
           $('#error_customerdata').show();
           timer =setTimeout(function () {$('#error_customerdata').hide(500);}, 3000);
        } else {
            $('#modalChassis').modal('show');
            $('#chasis').val(none);
            $('#qty_chassis').val(none);
            $('#backplane').text(none);
            $('#nodes').text(none);
            $('#drivebays').text(none);
            $('#psuchassis').text(none);
            $('#rearbays').text(none);
            $('#bin').text(none);
            $('#chasis').val(none);
            $('#qty_chassis').val(none);
            $('#front_caddies').val(none);
            $('#qty_front_caddies').val(none);
            $('#rear_caddies').val(none);
            $('#qty_rear_caddies').val(none);
            $('#item_harddrive_one').val('null');
            $('#qty_hard_drive_one').val(none);
            $('#item_harddrive_two').val('null');
            $('#qty_hard_drive_two').val(none);
            $('#item_harddrive_three').val('null');
            $('#qty_hard_drive_three').val(none);
            $('#motherboard').val(none);
            $('#qty_motherboard').val(none);
            $('#cpu').val(none);
            $('#qty_cpu').val(none);
            $('#memory').val(none);
            $('#qty_memory').val(none);
            $('#cntrl').val(none);
            $('#qty_cntrlr').val(none);
            $('#cntrltwo').val(none);
            $('#qty_cntrlrtwo').val(none);
            $('#cntrlthree').val(none);
            $('#qty_cntrlrthree').val(none);
            $('#psu').val(none);
            $('#qty_psu').val(none);
            $('#qty_mounting').val(none);
            $('#os').val(none);
        }

    });

  	$("#add_order_chassis").click(function(){
      /*  if($('#chasis').val() != null && $('#qty_chassis').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Chassis quantity </h5>
                    </div>`);

        }else if($('#chasis').val() == null && $('#qty_chassis').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Chassis Quantity. No selected option </h5>
                    </div>`);

        }else if( $('#cpu').val() != null && $('#qty_cpu').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu').val() == null && $('#qty_cpu').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory').val() == null && $('#qty_memory').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory').val() != null && $('#qty_memory').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);

        }else if($('#item_harddrive_one').val() === 'null' && $('#qty_hard_drive_one').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_one').val() != 'null' && $('#qty_hard_drive_one').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#item_harddrive_two').val() === 'null' && $('#qty_hard_drive_two').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_two').val() != 'null' && $('#qty_hard_drive_two').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#item_harddrive_three').val() === 'null' && $('#qty_hard_drive_three').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_three').val() != 'null' && $('#qty_hard_drive_three').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#cntrl').val() == '' && $('#qty_cntrlr').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrl').val() != '' && $('#qty_cntrlr').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#cntrltwo').val() == '' && $('#qty_cntrlrtwo').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrltwo').val() != '' && $('#qty_cntrlrtwo').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#cntrlthree').val() == '' && $('#qty_cntrlrthree').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrlthree').val() != '' && $('#qty_cntrlrthree').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu').val() == '' && $('#qty_psu').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu').val() != '' && $('#qty_psu').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#front_caddies').val() == 'null' && $('#qty_front_caddies').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Front Caddies Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#rear_caddies').val() == 'null' && $('#qty_rear_caddies').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Rear Caddies Quantity. No selected optoin</h5>
                    </div>`);
            
        }else if($('#motherboard').val() == null && $('#qty_motherboard').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard').val() != null && $('#qty_motherboard').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting').val() == '' && $('#qty_mounting').val() != ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting').val() != '' && $('#qty_mounting').val() == ''){
            $('#modalChassis').scrollTop(0);
            $("#error_input").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{*/
            
        $('#submit_all_orders').prop('disabled', false); 
		var created = json.created;
    	var ship_date = json.ship_date;
    	var ship_method = json.ship_method;
    	var sales_rep = json.sales_rep;
    	var client_name = json.client_name;
    	var contact_no = json.contact_no;
    	var email = json.email;
    	var address = json.address;
    	var barcode = json.input_barcode;
        var details_all = json.details_all;
        var name_detail = details_all[0];
        var address_one_detail = details_all[1];
        var address_two_detail = details_all[2];
        var number_detail = details_all[3];

    	var datas = {
            'chassis' : $("#chasis").val(),
            'qty_chassis' : $("#qty_chassis").val(),
            'backplane'     : $('#backplane').text(),
            'drivebays'     : $('#drivebays').text(),
            'rearbays'      : $('#rearbays').text(),
            'nodes'          : $('#nodes').text(),
            'psu_slots'           : $('#psuchassis').text(),
            'bin'           : $('#bin').text(),
            'front_caddies' : $("#front_caddies").val(),
            'qty_front_caddies' : $("#front_caddies_total").text(),
            'rear_caddies' : $("#rear_caddies").val(),
            'qty_rear_caddies' : $("#qty_rear_caddies").val(),
            'item_harddrive_one' : $("#item_harddrive_one").val(),
            'item_harddrive_two' : $("#item_harddrive_two").val(),
            'item_harddrive_three' : $("#item_harddrive_three").val(),
            'qty_hard_drive_one' : $("#qty_hard_drive_one").val(),
            'qty_hard_drive_two' : $("#qty_hard_drive_two").val(),
            'qty_hard_drive_three' : $("#qty_hard_drive_three").val(),
            'motherboard' : $("#motherboard").val(),
            'qty_motherboard' : $("#motherboard_total").text(),
            'cpu' : $("#cpu").val(),
            'qty_cpu' : $("#qty_cpu").val(),
            'memory' : $("#memory").val(),
            'qty_memory' : $("#qty_memory").val(),
            'ob_nic' : $("#ob_nic").text(),
            'pcie' : $("#pcie_slot").text(),
            'cntrl' : $("#cntrl").val(),
            'qty_cntrlr' : $("#qty_cntrlr").val(),
            'cntrltwo' : $("#cntrltwo").val(),
            'qty_cntrlrtwo' : $("#qty_cntrlrtwo").val(),
            'cntrlthree' : $("#cntrlthree").val(),
            'qty_cntrlrthree' : $("#qty_cntrlrthree").val(),
            'psu' : $("#psu").val(),
            'qty_psu' : $("#psu_total").text(),
            'mounting' : $("#mounting").val(),
            'qty_mounting' : $("#mounting_total").text(),
            'warranty' : $("#warranty_ch").val(),
            'os' : $("#os").val(),

            created : created,
            ship_date : ship_date,
            ship_method : ship_method,
            sales_rep : sales_rep,
            client_name : name_detail,
            contact_no : number_detail,
            email : email,
            address1 : address_one_detail,
            address2 : address_two_detail,
            barcode : barcode,
            details_all : details_all,
            ajax : '1'   
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/request/add_order`,
            type : "post",
            data : datas,
            success : function(response){
                $('#modalChassis').modal('hide');
                $('#add_order_table_body').html(response)

                }
            });

        var data = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };

        $.post(`${baseUrl}index.php/work_orders/get_barcode`, data, function(response) {

            $('#add_order_table_body').html(response);
            paginate();
            });



            ///
            $(`#motherboard_mb,
        #cpu_mb,
        #memory_mb,
        #cntrl_mb,
        #cntrltwo_mb,
        #cntrlthree_mb,
        #psu_mb,
        #psu_mb,
        #build_id_mb,
        #qty_motherboard_mb,
        #qty_cpu_mb,
        #qty_memory_mb,
        #qty_cntrlr_mb,
        #qty_cntrlrtwo_mb,
        #qty_cntrlrthree_mb,
        #qty_psu_mb,
        #qty_mounting_mb`).val("");


        $(`#motherboard_mb option:contains(${''})`).prop('selected',true);
                var $motherboard_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard_mb").append($motherboard_mb_).trigger('change');


                $(`#cpu_mb option:contains(${''})`).prop('selected',true);
                var $cpu_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu_mb").append($cpu_mb_).trigger('change');


                $(`#memory_mb option:contains(${''})`).prop('selected',true);
                var $memory_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory_mb").append($memory_mb_).trigger('change');


                $(`#build_id_mb option:contains(${''})`).prop('selected',true);
                var $build_id_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id_mb").append($build_id_mb_).trigger('change');

                
            ///

            $(`#chasis, #qty_chassis,
            #front_caddies,
            #rear_caddies,
            #item_harddrive_one,
            #item_harddrive_two,
            #item_harddrive_three,
            #motherboard,
            #cpu,
            #memory,
            #cntrl,
            #cntrltwo,
            #cntrlthree,
            #psu,
            #os,
            #build_id,
            #qty_chassis,
            #qty_front_caddies,
            #qty_rear_caddies,
            #qty_hard_drive_one,
            #qty_hard_drive_two,
            #qty_hard_drive_three,
            #qty_motherboard,
            #qty_cpu,
            #qty_memory,
            #qty_cntrlr,
            #qty_cntrlrtwo,
            #qty_cntrlrthree,
            #qty_psu,
            #qty_mounting`).val("");


                $(`#chasis option:contains(${'null'})`).prop('selected',true);
                var $chassis_ = $("<option selected='selected'></option>").val('null').text('')
                $("#chasis").append($chassis_).trigger('change');


                $(`#front_caddies option:contains(${''})`).prop('selected',true);
                var $front_caddies_ = $("<option selected='selected'></option>").val('').text('')
                $("#front_caddies").append($front_caddies_).trigger('change');


                $(`#rear_caddies option:contains(${''})`).prop('selected',true);
                var $rear_caddies_ = $("<option selected='selected'></option>").val('').text('')
                $("#rear_caddies").append($rear_caddies_).trigger('change');


                $(`#item_harddrive_one option:contains(${'null'})`).prop('selected',true);
                var $item_harddrive_one_ = $("<option selected='selected'></option>").val('null').text('')
                $("#item_harddrive_one").append($item_harddrive_one_).trigger('change');


                $(`#item_harddrive_two option:contains(${'null'})`).prop('selected',true);
                var $item_harddrive_two_ = $("<option selected='selected'></option>").val('null').text('')
                $("#item_harddrive_two").append($item_harddrive_two_).trigger('change');


                $(`#item_harddrive_three option:contains(${'null'})`).prop('selected',true);
                var $item_harddrive_three_ = $("<option selected='selected'></option>").val('null').text('')
                $("#item_harddrive_three").append($item_harddrive_three_).trigger('change');


                $(`#motherboard option:contains(${''})`).prop('selected',true);
                var $motherboard_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard").append($motherboard_).trigger('change');


                $(`#cpu option:contains(${''})`).prop('selected',true);
                var $cpu_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu").append($cpu_).trigger('change');


                $(`#memory option:contains(${''})`).prop('selected',true);
                var $memory_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory").append($memory_).trigger('change');


                $(`#build_id option:contains(${'null'})`).prop('selected',true);
                var $build_id_ = $("<option selected='selected'></option>").val('null').text('')
                $("#build_id").append($build_id_).trigger('change');


                $(`#motherboard_mb option:contains(${''})`).prop('selected',true);
                var $motherboard_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard_mb").append($motherboard_mb_).trigger('change');


                $(`#cpu_mb option:contains(${''})`).prop('selected',true);
                var $cpu_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu_mb").append($cpu_mb_).trigger('change');


                $(`#memory_mb option:contains(${''})`).prop('selected',true);
                var $memory_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory_mb").append($memory_mb_).trigger('change');


                $(`#build_id_mb option:contains(${''})`).prop('selected',true);
                var $build_id_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id_mb").append($build_id_mb_).trigger('change');

        localStorage.removeItem('qty_motherboard_mb');
        localStorage.removeItem('qty_cpu_mb');
        localStorage.removeItem('qty_memory_mb');
        localStorage.removeItem('qty_cntrlr_mb');
        localStorage.removeItem('qty_cntrlrtwo_mb');
        localStorage.removeItem('qty_cntrlrthree_mb');
        localStorage.removeItem('cntrl_mb');
        localStorage.removeItem('cntrltwo_mb');
        localStorage.removeItem('cntrlthree_mb');
        localStorage.removeItem('qty_psu_mb');
        localStorage.removeItem('qty_mounting_mb');
        localStorage.removeItem('os_mb');
        ///localStorage.removeItem('barcode');
        ///localStorage.removeItem('ship_date');
        //localStorage.removeItem('details_all');
        localStorage.removeItem('qty_chassis');
        localStorage.removeItem('qty_front_caddies');
        localStorage.removeItem('qty_rear_caddies');
        localStorage.removeItem('qty_hard_drive_one');
        localStorage.removeItem('qty_hard_drive_two');
        localStorage.removeItem('qty_hard_drive_three');
        localStorage.removeItem('qty_cpu');
        localStorage.removeItem('qty_memory');
        localStorage.removeItem('qty_cntrlr');
        localStorage.removeItem('qty_cntrlrtwo');
        localStorage.removeItem('qty_cntrlrthree');
        localStorage.removeItem('cntrl');
        localStorage.removeItem('cntrltwo');
        localStorage.removeItem('cntrlthree');
        localStorage.removeItem('qty_psu');
        localStorage.removeItem('psu');
        localStorage.removeItem('os');
        localStorage.removeItem('chasis');
        localStorage.removeItem('front_caddies');
        localStorage.removeItem('rear_caddies');
        localStorage.removeItem('item_harddrive_one');
        localStorage.removeItem('item_harddrive_two');
        localStorage.removeItem('item_harddrive_three');
        localStorage.removeItem('build_id');
        localStorage.removeItem('build_id_mb');
        localStorage.removeItem('motherboard_mb');
        localStorage.removeItem('qty_mounting');

     ///localStorage.clear();
  
        
    });

    $("#add_order_motherboard").click(function(){
      /*  if( $('#cpu_mb').val() != null && $('#qty_cpu_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu_mb').val() == null && $('#qty_cpu_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory_mb').val() == null && $('#qty_memory_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory_mb').val() != null && $('#qty_memory_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);
    
        }else if($('#cntrl_mb').val() == '' && $('#qty_cntrlr_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrl_mb').val() != '' && $('#qty_cntrlr_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#cntrltwo_mb').val() == '' && $('#qty_cntrlrtwo_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrltwo_mb').val() != '' && $('#qty_cntrlrtwo_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#cntrlthree_mb').val() == '' && $('#qty_cntrlrthree_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrlthree_mb').val() != '' && $('#qty_cntrlrthree_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu_mb').val() == '' && $('#qty_psu_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_mb').val() != '' && $('#qty_psu_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#motherboard_mb').val() == null && $('#qty_motherboard_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard_mb').val() != null && $('#qty_motherboard_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting_mb').val() == '' && $('#qty_mounting_mb').val() != ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting_mb').val() != '' && $('#qty_mounting_mb').val() == ''){
            $('#modalMotherboard').scrollTop(0);
            $("#error_input_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{*/
        $('#submit_all_orders').prop('disabled', false);  
        var created = json.created;
        var ship_date = json.ship_date;
        var ship_method = json.ship_method;
        var sales_rep = json.sales_rep;
        var client_name = json.client_name;
        var contact_no = json.contact_no;
        var email = json.email;
        var address = json.address;
        var barcode = json.input_barcode;
        var temp = "";
        var num = 0;
        var temp_null = null;
        var temp_null2 = 'null';
        

        var datas = {
            'motherboard' : $("#motherboard_mb").val(),
            'qty_motherboard' : $("#qty_motherboard_mb").val(),
            'cpu' : $("#cpu_mb").val(),
            'qty_cpu' : $("#qty_cpu_mb").val(),
            'memory' : $("#memory_mb").val(),
            'qty_memory' : $("#qty_memory_mb").val(),
            'ob_nic' : $("#ob_nic_mb").text(),
            'pcie' : $("#pcie_slot_mb").text(),
            'cntrl' : $("#cntrl_mb").val(),
            'qty_cntrlr' : $("#qty_cntrlr_mb").val(),
            'cntrltwo' : $("#cntrltwo_mb").val(),
            'qty_cntrlrtwo' : $("#qty_cntrlrtwo_mb").val(),
            'cntrlthree' : $("#cntrlthree_mb").val(),
            'qty_cntrlrthree' : $("#qty_cntrlrthree_mb").val(),
            'psu' : $("#psu_mb").val(),
            'qty_psu' : $("#qty_psu_mb").val(),
            'mounting' : $("#mounting_mb").val(),
            'qty_mounting' : $("#qty_mounting_mb").val(),
            'warranty' : $("#warranty_mb").val(),
            'os' : $("#os_mb").val(),

            created : created,
            ship_date : ship_date,
            ship_method : ship_method,
            sales_rep : sales_rep,
            client_name : client_name,
            contact_no : contact_no,
            email : email,
            address : address,
            barcode : barcode,
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/request/add_order`,
            type : "post",
            data : datas,
            success : function(display){ 
                        $('#modalChassis').modal('hide');
                        $('#modalMotherboard').modal('hide');  
                    }
            });

        var datas = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };
        $.post(`${baseUrl}index.php/work_orders/get_barcode`, datas, function(response) {
            $('#add_order_table_body').html(response);
            paginate();
            });
        


            ///
            $(`#chasis, #qty_chassis,
            #front_caddies,
            #rear_caddies,
            #item_harddrive_one,
            #item_harddrive_two,
            #item_harddrive_three,
            #motherboard,
            #cpu,
            #memory,
            #cntrl,
            #cntrltwo,
            #cntrlthree,
            #psu,
            #os,
            #build_id,
            #qty_chassis,
            #qty_front_caddies,
            #qty_rear_caddies,
            #qty_hard_drive_one,
            #qty_hard_drive_two,
            #qty_hard_drive_three,
            #qty_motherboard,
            #qty_cpu,
            #qty_memory,
            #qty_cntrlr,
            #qty_cntrlrtwo,
            #qty_cntrlrthree,
            #qty_psu,
            #qty_mounting`).val("");


                $(`#chasis option:contains(${'null'})`).prop('selected',true);
                var $chassis_ = $("<option selected='selected'></option>").val('null').text('')
                $("#chasis").append($chassis_).trigger('change');


                $(`#front_caddies option:contains(${''})`).prop('selected',true);
                var $front_caddies_ = $("<option selected='selected'></option>").val('').text('')
                $("#front_caddies").append($front_caddies_).trigger('change');


                $(`#rear_caddies option:contains(${''})`).prop('selected',true);
                var $rear_caddies_ = $("<option selected='selected'></option>").val('').text('')
                $("#rear_caddies").append($rear_caddies_).trigger('change');


                $(`#item_harddrive_one option:contains(${'null'})`).prop('selected',true);
                var $item_harddrive_one_ = $("<option selected='selected'></option>").val('null').text('')
                $("#item_harddrive_one").append($item_harddrive_one_).trigger('change');


                $(`#item_harddrive_two option:contains(${'null'})`).prop('selected',true);
                var $item_harddrive_two_ = $("<option selected='selected'></option>").val('null').text('')
                $("#item_harddrive_two").append($item_harddrive_two_).trigger('change');


                $(`#item_harddrive_three option:contains(${'null'})`).prop('selected',true);
                var $item_harddrive_three_ = $("<option selected='selected'></option>").val('null').text('')
                $("#item_harddrive_three").append($item_harddrive_three_).trigger('change');


                $(`#motherboard option:contains(${''})`).prop('selected',true);
                var $motherboard_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard").append($motherboard_).trigger('change');


                $(`#cpu option:contains(${''})`).prop('selected',true);
                var $cpu_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu").append($cpu_).trigger('change');


                $(`#memory option:contains(${''})`).prop('selected',true);
                var $memory_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory").append($memory_).trigger('change');


                $(`#build_id option:contains(${'null'})`).prop('selected',true);
                var $build_id_ = $("<option selected='selected'></option>").val('null').text('')
                $("#build_id").append($build_id_).trigger('change');


                $(`#motherboard_mb option:contains(${''})`).prop('selected',true);
                var $motherboard_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard_mb").append($motherboard_mb_).trigger('change');


                $(`#cpu_mb option:contains(${''})`).prop('selected',true);
                var $cpu_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu_mb").append($cpu_mb_).trigger('change');


                $(`#memory_mb option:contains(${''})`).prop('selected',true);
                var $memory_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory_mb").append($memory_mb_).trigger('change');


                $(`#build_id_mb option:contains(${''})`).prop('selected',true);
                var $build_id_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id_mb").append($build_id_mb_).trigger('change');
            ///

        $(`#motherboard_mb,
        #cpu_mb,
        #memory_mb,
        #cntrl_mb,
        #cntrltwo_mb,
        #cntrlthree_mb,
        #psu_mb,
        #psu_mb,
        #build_id_mb,
        #qty_motherboard_mb,
        #qty_cpu_mb,
        #qty_memory_mb,
        #qty_cntrlr_mb,
        #qty_cntrlrtwo_mb,
        #qty_cntrlrthree_mb,
        #qty_psu_mb,
        #qty_mounting_mb`).val("");


        $(`#motherboard_mb option:contains(${''})`).prop('selected',true);
                var $motherboard_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#motherboard_mb").append($motherboard_mb_).trigger('change');


                $(`#cpu_mb option:contains(${''})`).prop('selected',true);
                var $cpu_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#cpu_mb").append($cpu_mb_).trigger('change');


                $(`#memory_mb option:contains(${''})`).prop('selected',true);
                var $memory_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#memory_mb").append($memory_mb_).trigger('change');


                $(`#build_id_mb option:contains(${''})`).prop('selected',true);
                var $build_id_mb_ = $("<option selected='selected'></option>").val('').text('')
                $("#build_id_mb").append($build_id_mb_).trigger('change');




        localStorage.removeItem('qty_motherboard_mb');
        localStorage.removeItem('qty_cpu_mb');
        localStorage.removeItem('qty_memory_mb');
        localStorage.removeItem('qty_cntrlr_mb');
        localStorage.removeItem('qty_cntrlrtwo_mb');
        localStorage.removeItem('qty_cntrlrthree_mb');
        localStorage.removeItem('cntrl_mb');
        localStorage.removeItem('cntrltwo_mb');
        localStorage.removeItem('cntrlthree_mb');
        localStorage.removeItem('qty_psu_mb');
        localStorage.removeItem('qty_mounting_mb');
        localStorage.removeItem('os_mb');
        ///localStorage.removeItem('barcode');
        ///localStorage.removeItem('ship_date');
        //localStorage.removeItem('details_all');
        localStorage.removeItem('qty_chassis');
        localStorage.removeItem('qty_front_caddies');
        localStorage.removeItem('qty_rear_caddies');
        localStorage.removeItem('qty_hard_drive_one');
        localStorage.removeItem('qty_hard_drive_two');
        localStorage.removeItem('qty_hard_drive_three');
        localStorage.removeItem('qty_cpu');
        localStorage.removeItem('qty_memory');
        localStorage.removeItem('qty_cntrlr');
        localStorage.removeItem('qty_cntrlrtwo');
        localStorage.removeItem('qty_cntrlrthree');
        localStorage.removeItem('cntrl');
        localStorage.removeItem('cntrltwo');
        localStorage.removeItem('cntrlthree');
        localStorage.removeItem('qty_psu');
        localStorage.removeItem('psu');
        localStorage.removeItem('os');
        localStorage.removeItem('chasis');
        localStorage.removeItem('front_caddies');
        localStorage.removeItem('rear_caddies');
        localStorage.removeItem('item_harddrive_one');
        localStorage.removeItem('item_harddrive_two');
        localStorage.removeItem('item_harddrive_three');
        localStorage.removeItem('build_id');
        localStorage.removeItem('build_id_mb');
        localStorage.removeItem('motherboard_mb');
        localStorage.removeItem('qty_mounting');
        
    
    });

	$("#chasis").change(function(){
        $('#qty_chassis').val('1');
        $('#qty_motherboard ').val('1');
        $('#qty_mounting').val('1');
		$('#front_caddies').find('option:not(:first)').remove();
		$('#rear_caddies').find('option:not(:first)').remove();

        var datas = {
            part_number : $("#chasis").val(),
            'qty_chassis': $("#qty_chassis").val(),
            'motherboard_total' : $('#motherboard_total').text(),
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/request/search_chassis`,
            type : "post",
            data : datas,
            success : function(msg){

                $('#backplane').text(msg.backplane);
                $('#drivebays').text(msg.drivebays);
                $('#rearbays').text(msg.rearbays);
                $('#qty_front_caddies').val(msg.numeric_dbays);
                $('#front_caddies_total').text(msg.numeric_dbays);
                $('#qty_rear_caddies').val(msg.numeric_rbays);
                $('#rear_caddies_total').text(msg.numeric_rbays);
                //$('#psuchassis').text(msg.psu);
                $('#bin').text(msg.bin);
                $('#motherboard_total').text('1');
                $('#qty_psu').val(msg.psu_default);
                $('#psu_total').text(msg.psu_default);
                $('#mounting_total').text('1');

                if(msg.node == '1'){
                    $('#nodes').text('Single Node');
                }
                if(msg.node == '2'){
                    $('#nodes').text('Dual Node');
                }
                if(msg.node == '4'){
                    $('#nodes').text('Quad Node');
                }
                if(msg.node == '4'){
                    $('#nodes').text('Quad Node');
                }
                if(msg.node == '8'){
                    $('#nodes').text('Octal Node');
                }
                if(msg.node == '9'){
                    $('#nodes').text('Nonad Node');
                }
                if(msg.node == '12'){
                    $('#nodes').text('Duodecad Node');
                }
                if(msg.psu == '1'){
                    $('#psuchassis').text('Single Slot');
                }
                if(msg.psu == '2'){
                    $('#psuchassis').text('Dual Slot');
                }
                if(msg.psu == '4'){
                    $('#psuchassis').text('Quad Slot');
                }
                if(msg.dbays == "3.5"){
                    var data = {                    
                    "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                    },
                    select = $('select[name=front_caddies]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }else if(msg.dbays == "2.5"){
                        var data = {                    
                        	"dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=front_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                        }

					if(msg.rbays == "3.5"){
                        var data = {                    
                            "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                        },
                        select = $('select[name=rear_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                    }else if(msg.rbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=rear_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                        }                                                                      
                }
            });

        var datas = {
            part_number : $("#chasis").val(),
            ajax : '1'   
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/sss`,
            type : "post",
            data : datas,
            success : function(msgs){
                    $('#select_fc').html(msgs);       
                }
            });




            

        var datas = {
            part_number : $("#chasis").val(),
            motherboard_for_edit : "",
            motherboard_id : "",
            ajax : '1'   
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/request/motherboard`,
            type : "post",
            data : datas,
            success : function(motherboard){
                    $('#motherboard_chassis').html(motherboard);       
                }
            });

        var datas = {
            part_number : $("#chasis").val(),
            cpu_for_edit : "",
            cpu_id : "",
            ajax : '1'   
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/request/cpu`,
            type : "post",
            data : datas,
            success : function(cpu){
                    $('#motherboard_cpu').html(cpu);       
  				}
            });


        var datas = {
            part_number : $("#chasis").val(),
            memory_for_edit : "",
            memory_id : "",
            ajax : '1'   
        };
        $.ajax({
            url :  `${baseUrl}index.php/work_orders/request/memory`,
            type : "post",
            data : datas,
            success : function(cpu){
                    $('#motherboard_memory').html(cpu);       
                }
            }).then(function(){
                var datas = {
                    motherboard_ : $("#motherboard").val(),
                    cpu_ : $("#cpu").val(),
                    memory_ : $("#memory").val(),
                    ajax : '1'   
                };
                $.ajax({
                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                    type : "post",
                    data : datas,
                    success : function(response_ob_nic_pci_slot){
                        var datas = {
                            motherboard_ : $("#motherboard").val(),
                            cpu_ : $("#cpu").val(),
                            memory_ : $("#memory").val(),
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                            type : "post",
                            data : datas,
                            success : function(response_ob_nic_pci_slot){
                                $("#ob_nic").text(response_ob_nic_pci_slot.ob_nic);
                                $("#pcie_slot").text(response_ob_nic_pci_slot.pci_slot);
                                    ///$('#motherboard_chassis').html(motherboard);       
                                }
                            });
                            ///$('#motherboard_chassis').html(motherboard);       
                        }
                    });
            });
    });

    $('#motherboard_mb').change(function(event){

        $('#qty_motherboard_mb').val('1');
        $('#motherboard_mb_total').text('1');

        var datas = {
            'motherboard' : $('#motherboard_mb').val(),
            ajax : 1

        };

        $.ajax({

            url : `${baseUrl}index.php/work_orders/cpu_for_motherboard2`,
            type: "post",
            data : datas,
            success : function(cpu2){

                    $('#cpu_motherboard2').html(cpu2);
                }
            });
        var data = {
            'motherboard' :$("#motherboard_mb").val(),
             ajax : '1'
        };

            $.ajax({

            url : `${baseUrl}index.php/work_orders/memory_for_motherboard2`,
            type: "post",
            data : data,
            success : function(memory2){

                    $('#memory_motherboard2').html(memory2);

                }
            }).then(function(){
                var datas = {
                    motherboard_ : $("#motherboard_mb").val(),
                    cpu_ : $("#cpu_mb").val(),
                    memory_ : $("#memory_mb").val(),
                    ajax : '1'   
                };
                $.ajax({
                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                    type : "post",
                    data : datas,
                    success : function(response_ob_nic_pci_slot){
                        var datas = {
                            motherboard_ : $("#motherboard_mb").val(),
                            cpu_ : $("#cpu_mb").val(),
                            memory_ : $("#memory_mb").val(),
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                            type : "post",
                            data : datas,
                            success : function(response_ob_nic_pci_slot){
                                $("#ob_nic_mb").text(response_ob_nic_pci_slot.ob_nic);
                                $("#pcie_slot_mb").text(response_ob_nic_pci_slot.pci_slot);
                                    ///$('#motherboard_chassis').html(motherboard);       
                                }
                            });
                            ///$('#motherboard_chassis').html(motherboard);       
                        }
                    });
            });
    });    

	$('#chasis_').on('change', function () {
        
        var isDirty = !this.options[this.selectedIndex].defaultSelected;
    
        if (isDirty) {
            alert("has changed");
        } else {
            alert("default value");
        }
    });

    /////////////////////////open
    $("#chasis_").change(function(){

    	$('#front_caddies_').find('option:not(:first)').remove();
		$('#rear_caddies_').find('option:not(:first)').remove();

                
	var datas = {
        part_number : $("#chasis_").val(),
        ajax : '1'   
    };
    $.ajax({
        url : "<?php echo base_url();   ?>index.php/work_orders/request/search_chassis",
        type : "post",
        data : datas,
        success : function(msg){
                $('#backplane_').text(msg.backplane);
                $('#drivebays_').text(msg.drivebays);
                $('#rearbays_').text(msg.rearbays);
                $('#nodes_').text(msg.node);
                $('#psuchassis_').text(msg.psu);
                $('#bin_').text(msg.bin);
                    if(msg.dbays == "3.5"){
                        var data = {                    
                        "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                        },
                        select = $('select[name=front_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }else if(msg.dbays == "2.5"){
                        var data = {                    
                        "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=front_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }


                     if(msg.rbays == "3.5"){
                        var data = {                    
                        "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                        },
                        select = $('select[name=rear_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                     }else if(msg.rbays == "2.5"){
                        var data = {                    
                        "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=rear_caddies]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                     }                                    
            }
        });


    var datas = {
        part_number : $("#chasis_").val(),
        ajax : '1'   
    };
    $.ajax({
        url : "<?php echo base_url();   ?>index.php/work_orders/sss",
        type : "post",
        data : datas,
        success : function(msgs){
                $('#select_fc_').html(msgs);       
                                            
            }
        });


    var datas = {
        part_number : $("#chasis_").val(),
        motherboard_for_edit : "_for_edit",
        motherboard_id : "_",
        ajax : '1'   
        };
    $.ajax({
        url : "<?php echo base_url();   ?>index.php/work_orders/request/motherboard",
        type : "post",
        data : datas,
            success : function(motherboard){
                    $('#motherboard_chassis_').html(motherboard);       
                }
        });

    var datas = {
        part_number : $("#chasis_").val(),
        cpu_for_edit : "_for_edit",
        cpu_id : "_",
        ajax : '1'   
    };
    $.ajax({
        url : "<?php echo base_url();   ?>index.php/work_orders/request/cpu",
        type : "post",
        data : datas,
        success : function(cpu){
                     $('#motherboard_cpu_').html(cpu);       
            }
    });

	var datas = {
        part_number : $("#chasis_").val(),
        memory_for_edit : "_for_edit",
        memory_id : "_",
        ajax : '1'   
    };
    $.ajax({
        url : "<?php echo base_url();   ?>index.php/work_orders/request/memory",
        type : "post",
        data : datas,
        success : function(cpu){
                $('#motherboard_memory_').html(cpu);       
            }
        });
                
    });
    /////////////////////////end


    function search() {
        const searchItem = $('#itemSearch').val();
        const searchSelected = $('#searchSelected').val();
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();


        const route = `${baseUrl}index.php/work_orders?search=${searchItem}`+`&searchSelected=${searchSelected}&orderBy=${orderBy}` +`&sortOrder=${sortOrder}`;
        window.location.href = route;

       
    }



});
