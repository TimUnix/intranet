$(document).ready(function() {

	baseUrl = $('#workOrderScript').data('baseUrl');
    var timer;
    var dateToday = new Date();
    var confirmDel;

    $('#product_table_body').on('click', '.edit-item', function(event){
        const button = $(event.target).closest('.edit-item');
        if(!button) return;
        const barcode = button.closest('tr').find('.item-barcode');

        if(!barcode) return;
        $('.current-item-barcode').val(barcode.data('value'));
        var test = $('.current-item-barcode').val(barcode.data('value'));;

    });

    $('#add_order_table_body_edit').on('click', '.edit-item', function(event){
        const button = $(event.target).closest('.edit-item');
        if(!button) return;
        const chassis = button.closest('tr').find('.item-chassis').text().trim();
        const qty_chassis = button.closest('tr').find('.item-qty_chassis').text();
        const backplane = button.closest('tr').find('.item-backplane').text();
        const nodes = button.closest('tr').find('.item-nodes').text();
        const rearbays = button.closest('tr').find('.item-rearbays').text();
        const drivebays = button.closest('tr').find('.item-drivebays').text();
        const ff = button.closest('tr').find('.item-ff').text().trim();
        const rff = button.closest('tr').find('.item-rff').text().trim();
        const psu_slots = button.closest('tr').find('.item-psu_slots').text();
        const bin = button.closest('tr').find('.item-bin').text();
        const front_caddies = button.closest('tr').find('.item-front_caddies').text();
        const qty_front_caddies = button.closest('tr').find('.item-qty_front_caddies').text();
        const rear_caddies = button.closest('tr').find('.item-rear_caddies').text();
        const qty_rear_caddies = button.closest('tr').find('.item-qty_qty_rear_caddies').text();
        const item_harddrive_one = button.closest('tr').find('.item-item_harddrive_one').text();
        const item_harddrive_two = button.closest('tr').find('.item-item_harddrive_two').text();
        const item_harddrive_three = button.closest('tr').find('.item-item_harddrive_three').text();
        const qty_hard_drive_one = button.closest('tr').find('.item-qty_hard_drive_one').text();
        const qty_hard_drive_two = button.closest('tr').find('.item-qty_hard_drive_two').text();
        const qty_hard_drive_three = button.closest('tr').find('.item-qty_hard_drive_three').text();
        const motherboard = button.closest('tr').find('.item-motherboard').text();
        const qty_motherboard = button.closest('tr').find('.item-qty_motherboard').text();
        const cpu = button.closest('tr').find('.item-cpu').text();
        const cpu_qty = button.closest('tr').find('.item-qty_cpu').text();
        const memory = button.closest('tr').find('.item-memory').text();
        const qty_memory = button.closest('tr').find('.item-qty_memory').text();
        const cntrl = button.closest('tr').find('.item-cntrl').text();
        const controllertwo = button.closest('tr').find('.item-controllertwo').text();
        const controllerthree = button.closest('tr').find('.item-controllerthree').text();
        const qty_cntrlr = button.closest('tr').find('.item-qty_cntrlr').text();
        const qty_controllertwo = button.closest('tr').find('.item-qty_controllertwo').text();
        const qty_controllerthree = button.closest('tr').find('.item-qty_controllerthree').text();
        const psu = button.closest('tr').find('.item-psu').text();
        const qty_psu = button.closest('tr').find('.item-qty_psu').text();
        const mounting = button.closest('tr').find('.item-mounting').text();
        const qty_mounting = button.closest('tr').find('.item-qty_mounting').text();
        const warranty = button.closest('tr').find('.item-warranty').text();
        const os = button.closest('tr').find('.item-os').text();
        const work_order_id = button.closest('tr').find('.item-work_order_id').text();

        const drive_ff = drivebays + " " + ff;
        const rear_rff = rearbays + " " + rff;

        var parse_chassis = parseInt(chassis);
        var parse_mb = parseInt(qty_motherboard);
        var parse_cpu_qty = parseInt(cpu_qty);
        var parse_memory_qty = parseInt(qty_memory);

        var get_motherboard = parse_mb/parse_chassis;
        var get_motherboard = parseInt(get_motherboard);

        var get_cpu = parse_cpu_qty/get_motherboard;
        var get_memory = parse_memory_qty/get_motherboard;
        console.log(memory);
        $('#chassis_edit').val(chassis);
        $('#chassis_edit').trigger('change');
        $('#qty_chassis_edit').val(qty_chassis);
        $('#backplane_edit').text(backplane);
        $('#nodes_edit').text(nodes);
        $('#rearbays_edit').text(rear_rff);
        $('#drivebays_edit').text(drive_ff);
        $('#psuchassis_edit').text(psu_slots);
        $('#bin_edit').text(bin);
        $('#item_harddrive_one_edit').val(item_harddrive_one);
        $('#item_harddrive_one_edit').trigger('change');
        $('#item_harddrive_two_edit').val(item_harddrive_two);
        $('#item_harddrive_two_edit').trigger('change');
        $('#item_harddrive_three_edit').val(item_harddrive_three);
        $('#item_harddrive_three_edit').trigger('change');
        $('#motherboard_edit').val(motherboard);
        $('.motherboard_edit').trigger('change');        
        $('.cpu_edit').html(cpu);
        $('.cpu_edit').trigger('change');        
        $('.memory_edit').html(memory);
        $('.memory_edit').trigger('change');         
        $('#cntrl_edit').val(cntrl);
        $('#cntrltwo_edit').val(controllertwo);
        $('#cntrlthree_edit').val(controllerthree);
        $('#psu_edit').val(psu);       
        $('#mounting_edit').val(mounting);
        $('#mounting_edit').trigger('change');        
        $('#warranty_ch_edit').val(warranty);
        $('#warranty_ch_edit').trigger('change');
        $('#os_edit').val(os);
        $('#work_order_id').text(work_order_id);

        if(front_caddies == 'null'){
            $('.front_caddies_display').html();
        }else{
            $('.front_caddies_display').html(front_caddies);
        }
        if(rear_caddies == 'null'){
            $('.rear_caddies_display').html();
        }else{
            $('.rear_caddies_display').html(rear_caddies);
        }
        if(qty_front_caddies == 0){
            $('#qty_front_caddies_edit').val("");
        }else{
            $('#qty_front_caddies_edit').val(qty_front_caddies);
            $('#qty_front_caddies_edit_total').text(qty_front_caddies);
        }
        if(qty_rear_caddies == 0){
            $('#qty_rear_caddies_edit').val("");
        }else{
            $('#qty_rear_caddies_edit').val(qty_rear_caddies);
            $('#qty_rear_caddies_edit_total').text(qty_rear_caddies);
        }
        if(qty_hard_drive_one == 0){
            $('#qty_hard_drive_one_edit').val("");
        }else{
            $('#qty_hard_drive_one_edit').val(qty_hard_drive_one);
            $('#qty_hard_drive_one_edit_total').text(qty_hard_drive_one);
        }
        if(qty_hard_drive_two == 0){
            $('#qty_hard_drive_two_edit').val("");
        }else{
            $('#qty_hard_drive_two_edit').val(qty_hard_drive_two);
            $('#qty_hard_drive_two_edit_total').text(qty_hard_drive_two);
        }
        if(qty_hard_drive_three == 0){
            $('#qty_hard_drive_three_edit').val("");
        }else{
            $('#qty_hard_drive_three_edit').val(qty_hard_drive_three);
            $('#qty_hard_drive_three_edit_total').text(qty_hard_drive_three);
        }
        if(qty_motherboard == 0){
            $('#qty_motherboard_edit').val("");
        }else{

            $('#qty_motherboard_edit').val(get_motherboard);
            $('#qty_motherboard_edit_total').text(qty_motherboard);
        }
        if(cpu_qty == 0){
            $('#qty_cpu_edit').val("");
        }else{
            $('#qty_cpu_edit_total').text(cpu_qty);
            $('#qty_cpu_edit').val(cpu_qty);
        }
        if(qty_memory == 0){
            $('#qty_memory_edit').val("");
        }else{
            $('#qty_memory_edit_total').text(qty_memory);
            $('#qty_memory_edit').val(qty_memory);
        }
        if(qty_cntrlr == 0){
            $('#qty_cntrlr_edit').val("");
        }else{
            $('#qty_cntrlr_edit').val(qty_cntrlr);
            $('#qty_cntrlr_edit_total').text(qty_cntrlr);
        }
        if(qty_controllertwo == 0){
            $('#qty_cntrlrtwo_edit').val("");
        }else{
            $('#qty_cntrlrtwo_edit').val(qty_controllertwo);
            $('#qty_cntrlrtwo_edit_total').text(qty_controllertwo);
        }
        if(qty_controllerthree == 0){
            $('#qty_cntrlrthree_edit').val("");
        }else{
            $('#qty_cntrlrthree_edit').val(qty_controllerthree);
            $('#qty_cntrlrthree_edit_total').text(qty_controllerthree);
        }
        if(qty_psu == 0){
            $('#qty_psu_edit').val("");
        }else{
            $('#qty_psu_edit').val(qty_psu);
            $('#qty_psu_edit_total').text(qty_psu);
            
        }
        if(qty_mounting == 0){
            $('#qty_mounting_edit').val("");
        }else{
            $('#qty_mounting_edit').val(qty_mounting);
            $('#qty_mounting_edit_total').text(qty_mounting);
        }
        $('#front_caddies_edit').find('option:not(:first)').remove();
        $('#rear_caddies_edit').find('option:not(:first)').remove();
        $('#modalChassis_edit').modal('show');

        var datas = {
            'part_number' : chassis,
            ajax : '1'
        };

        $.ajax({
            url: `${baseUrl}index.php/work_orders/request/search_chassis`,
            type : "post",
            data : datas,
            success : function(msg){
                if(msg.dbays == "3.5"){
                    var data = {                    
                    "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                    },
                    select = $('select[name=front_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }else if(msg.dbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=front_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                        }

                    if(msg.rbays == "3.5"){
                        var data = {                    
                            "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                        },
                        select = $('select[name=rear_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                    }else if(msg.rbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=rear_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                        }  
            }
        });
        
    });

    $('#motherboard_edit').click(function(){
        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/motherboard_for_chassis_edit`,
            type : "post",
            data : datas,
            success : function(motherboard){
                    $('#motherboard_chassis_edit').html(motherboard);
            }
        });
    });

    $('#cpu_edit').click(function(){
        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/cpu_for_motherboard_edit`,
            type : "post",
            data : datas,
            success : function(cpu){
                    $('#motherboard_cpu_edit').html(cpu);       
                }
        });
    });

    $('#memory_edit').click(function(){
        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url :  `${baseUrl}index.php/work_orders/memory_for_motherboard_edit`,
            type : "post",
            data : datas,
            success : function(memory){
                    $('#motherboard_memory_edit').html(memory);       
                }
            });
    });

    $('#chassis_edit').change(function(){
        $('.front_caddies_display').html('');
        $('.rear_caddies_display').html('');
        $('#front_caddies_edit').find('option:not(:first)').remove();
        $('#rear_caddies_edit').find('option:not(:first)').remove();

        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/request/search_chassis`,
            type : "post",
            data : datas,
            success : function(msg){
                $('#backplane_edit').text(msg.backplane);
                $('#drivebays_edit').text(msg.drivebays);
                $('#rearbays_edit').text(msg.rearbays);
                $('#nodes_edit').text(msg.node);
                $('#psuchassis_edit').text(msg.psu);
                $('#bin_edit').text(msg.bin);
                if(msg.dbays == "3.5"){
                    var data = {                    
                    "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                    },
                    select = $('select[name=front_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }else if(msg.dbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=front_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                        }

                    if(msg.rbays == "3.5"){
                        var data = {                    
                            "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                        },
                        select = $('select[name=rear_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                    }else if(msg.rbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=rear_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                    }                                                                       
                }
            });

        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/motherboard_for_chassis_edit`,
            type : "post",
            data : datas,
            success : function(motherboard_edit){
                    $('#motherboard_chassis_edit').html(motherboard_edit);
            }
        });

        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/cpu_for_motherboard_edit`,
            type : "post",
            data : datas,
            success : function(cpu_edit){
                    $('#motherboard_cpu_edit').html(cpu_edit);       
                }
        });

        var datas = {
            'part_number' : $('#chassis_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url :  `${baseUrl}index.php/work_orders/memory_for_motherboard_edit`,
            type : "post",
            data : datas,
            success : function(memory_edit){
                    $('#motherboard_memory_edit').html(memory_edit);       
                }
            }).then(function(){
                var datas = {
                    motherboard_ : $("#motherboard_edit").val(),
                    cpu_ : $("#cpu_edit").val(),
                    memory_ : $("#memory_edit").val(),
                    ajax : '1'   
                };
                $.ajax({
                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                    type : "post",
                    data : datas,
                    success : function(response_ob_nic_pci_slot){
                        var datas = {
                            motherboard_ : $("#motherboard_edit").val(),
                            cpu_ : $("#cpu_edit").val(),
                            memory_ : $("#memory_edit").val(),
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                            type : "post",
                            data : datas,
                            success : function(response_ob_nic_pci_slot){
                                $("#ob_nic_edit").text(response_ob_nic_pci_slot.ob_nic);
                                $("#pcie_slot_edit").text(response_ob_nic_pci_slot.pci_slot);
                                    ///$('#motherboard_chassis').html(motherboard);       
                                }
                            });
                            ///$('#motherboard_chassis').html(motherboard);       
                        }
                    });
            });;


    });

    $('#edit_order_chassis1').click(function(event){
        if($('#chassis_edit').val() != null && $('#qty_chassis_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Chassis quantity </h5>
                    </div>`);

        }else if($('#chassis_edit').val() == null && $('#qty_chassis_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Chassis Quantity. No selected option </h5>
                    </div>`);

        }else if( $('#cpu_edit').val() != null && $('#qty_cpu_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu_edit').val() == null && $('#qty_cpu_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory_edit').val() == null && $('#qty_memory_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory_edit').val() != null && $('#qty_memory_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);

        }else if($('#item_harddrive_one_edit').val() === 'null' && $('#qty_hard_drive_one_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_one_edit').val() != 'null' && $('#qty_hard_drive_one_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#item_harddrive_two_edit').val() === 'null' && $('#qty_hard_drive_two_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_two_edit').val() != 'null' && $('#qty_hard_drive_two_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#item_harddrive_three_edit').val() === 'null' && $('#qty_hard_drive_three_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_three_edit').val() != 'null' && $('#qty_hard_drive_three_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#cntrl_edit').val() == '' && $('#qty_cntrlr_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrl_edit').val() != '' && $('#qty_cntrlr_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#cntrltwo_edit').val() == '' && $('#qty_cntrlrtwo_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrltwo_edit').val() != '' && $('#qty_cntrlrtwo_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#cntrlthree_edit').val() == '' && $('#qty_cntrlrthree_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrlthree_edit').val() != '' && $('#qty_cntrlrthree_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu_edit').val() == '' && $('#qty_psu_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_edit').val() != '' && $('#qty_psu_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#front_caddies_edit').val() == '' && $('#qty_front_caddies_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Front Caddies Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#rear_caddies_edit').val() == '' && $('#qty_rear_caddies_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Rear Caddies Quantity. No selected optoin</h5>
                    </div>`);
            
        }else if($('#rear_caddies_edit').val() == null && $('#qty_rear_caddies_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Rear Caddies Quantity. No selected optoin</h5>
                    </div>`);
            
        }else if($('#motherboard_edit').val() == null && $('#qty_motherboard_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard_edit').val() != null && $('#qty_motherboard_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting_edit').val() == '' && $('#qty_mounting_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting_edit').val() != '' && $('#qty_mounting_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{

        var datas = {
            'work_order_id' : $("#work_order_id").text(),
            'chassis' : $("#chassis_edit").val(),
            'qty_chassis' : $("#qty_chassis_edit").val(),
            'backplane'     : $('#backplane_edit').text(),
            'drivebays'     : $('#drivebays_edit').text(),
            'rearbays'      : $('#rearbays_edit').text(),
            'nodes'          : $('#nodes_edit').text(),
            'psuchassis'     : $('#psuchassis_edit').text(),
            'bin'           : $('#bin_edit').text(),
            'front_caddies' : $("#front_caddies_edit").val(),
            'qty_front_caddies' : $("#qty_front_caddies_edit_total").text(),
            'rear_caddies' : $("#rear_caddies_edit").val(),
            'qty_rear_caddies' : $("#qty_rear_caddies_edit_total").text(),
            'item_harddrive_one' : $("#item_harddrive_one_edit").val(),
            'item_harddrive_two' : $("#item_harddrive_two_edit").val(),
            'item_harddrive_three' : $("#item_harddrive_three_edit").val(),
            'qty_hard_drive_one' : $("#qty_hard_drive_one_edit_total").text(),
            'qty_hard_drive_two' : $("#qty_hard_drive_two_edit_total").text(),
            'qty_hard_drive_three' : $("#qty_hard_drive_three_edit_total").text(),
            'motherboard' : $("#motherboard_edit").val(),
            'qty_motherboard' : $("#qty_motherboard_edit_total").text(),
            'cpu' : $("#cpu_edit").val(),
            'qty_cpu' : $("#qty_cpu_edit_total").text(),
            'memory' : $("#memory_edit").val(),
            'qty_memory' : $("#qty_memory_edit_total").text(),
            'cntrl' : $("#cntrl_edit").val(),
            'qty_cntrlr' : $("#qty_cntrlr_edit_total").text(),
            'cntrltwo' : $("#cntrltwo_edit").val(),
            'qty_cntrlrtwo' : $("#qty_cntrlrtwo_edit_total").text(),
            'cntrlthree' : $("#cntrlthree_edit").val(),
            'qty_cntrlrthree' : $("#qty_cntrlrthree_edit_total").text(),
            'psu' : $("#psu_edit").val(),
            'qty_psu' : $("#qty_psu_edit_total").text(),
            'mounting' : $("#mounting_edit").val(),
            'qty_mounting' : $("#qty_mounting_edit_total").text(),
            'warranty' : $("#warranty_ch_edit").val(),
            'os' : $("#os_edit").val(),
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/update_each_order`,
            type : "post",
            data : datas,
            success : function(response){
                $('#modalChassis_edit').modal('hide');

                }
            });
        }

         var data = {
            'barcode' : $('#txt_input_edit').val(),
            ajax : '1'
        };
        $.post(`${baseUrl}index.php/work_orders/get_barcode_from_wo`, data, function(response) {
            $('#add_order_table_body_edit').html(response);
            paginate();
            });  
    });

    $('#update_all_orders_edit').click(function(){
         var mailformat = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])/;
         var mailformat = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])/;
        if($('#ship_date_edit input').val() == '' || $('#ship_method_edit').val() == '' || 
            $('#details_all_edit').val() == ''){
            $('#editorder').scrollTop(0); 
            $('#error_customerdata_edit').show();
           timer =setTimeout(function () {$('#error_customerdata_edit').hide(500);}, 3000);
        }else {
            event.preventDefault();
            var details_split = $('#details_all_edit').val().split("\n");
            console.log(details_split);
        var data = {
            'barcode' : $('#txt_input_edit').val(),
            'ship_date' : $('#ship_date_edit input').val(),
            'ship_method' : $('#ship_method_edit').val(),
            'client_name' : details_split[0],
            'contact_no' : details_split[3],
            'email' : $('#email_edit').val(),
            'address1' : details_split[1],
            'address2' : details_split[2],
            ajax : '1'
        }
        $.ajax({
             url : `${baseUrl}index.php/work_orders/update_customer_detail`,
            type : "post",
            data : data,
            success : function(orders){
                       location.reload();   
                                                
                }
        });
        }
    });

    $('#add_order_table_body_edit').on('click', '.edit-mb', function(event){
        const buttonmb = $(event.target).closest('.edit-mb');
        if(!buttonmb) return;
        const motherboard = buttonmb.closest('tr').find('.item-motherboard').text();
        const qty_motherboard = buttonmb.closest('tr').find('.item-qty_motherboard').text();
        const cpu = buttonmb.closest('tr').find('.item-cpu').text();
        const cpu_qty = buttonmb.closest('tr').find('.item-qty_cpu').text();
        const memory = buttonmb.closest('tr').find('.item-memory').text();
        const qty_memory = buttonmb.closest('tr').find('.item-qty_memory').text();
        const cntrl = buttonmb.closest('tr').find('.item-cntrl').text();
        const controllertwo = buttonmb.closest('tr').find('.item-controllertwo').text();
        const controllerthree = buttonmb.closest('tr').find('.item-controllerthree').text();
        const qty_cntrlr = buttonmb.closest('tr').find('.item-qty_cntrlr').text();
        const qty_controllertwo = buttonmb.closest('tr').find('.item-qty_controllertwo').text();
        const qty_controllerthree = buttonmb.closest('tr').find('.item-qty_controllerthree').text();
        const psu = buttonmb.closest('tr').find('.item-psu').text();
        const qty_psu = buttonmb.closest('tr').find('.item-qty_psu').text();
        const mounting = buttonmb.closest('tr').find('.item-mounting').text();
        const qty_mounting = buttonmb.closest('tr').find('.item-qty_mounting').text();
        const warranty = buttonmb.closest('tr').find('.item-warranty').text();
        const os = buttonmb.closest('tr').find('.item-os').text();
        const work_order_id = buttonmb.closest('tr').find('.item-work_order_id').text();

        var parse_mb = parseInt(qty_motherboard);
        var parse_cpu_qty = parseInt(cpu_qty);
        var parse_memory_qty = parseInt(qty_memory);

        var get_cpu = parse_cpu_qty/parse_mb;
        var get_memory = parse_memory_qty/parse_mb;

        $('#motherboard_mb_edit').val(motherboard);
        $('#motherboard_mb_edit').trigger('change');

        $('.cpu_mb_edit').html(cpu); 
        $('.cpu_mb_edit').trigger('change');     
        $('.memory_mb_edit').html(memory);
        $('.memory_mb_edit').trigger('change');        
        $('#cntrl_mb_edit').val(cntrl);
        $('#cntrltwo_mb_edit').val(controllertwo);
        $('#cntrlthree_mb_edit').val(controllerthree);
        $('#psu_mb_edit').val(psu);       
        $('#mounting_mb_edit').val(mounting);
        $('#mounting_mb_edit').trigger('change');       
        $('#warranty_mb_edit').val(warranty);
        $('#warranty_mb_edit').trigger('change');
        $('#os_mb_edit').val(os);
        $('#work_order_id_mb').text(work_order_id);

        $('#modalMotherboard_edit').modal('show');

        if(qty_motherboard == 0){
            $('#qty_motherboard_mb_edit').val("");
        }else{
            $('#qty_motherboard_mb_edit').val(qty_motherboard);
            $('#qty_motherboard_mb_edit_total').text(qty_motherboard);
        }
        if(cpu_qty == 0){
            $('#qty_cpu_mb_edit').val("");
        }else{
            $('#qty_cpu_mb_edit').val(cpu_qty);
            $('#qty_motherboard_mb_edit_total').text(cpu_qty);
        }
        if(qty_memory == 0){
            $('#qty_memory_mb_edit').val("");
        }else{
            $('#qty_memory_mb_edit_total').text(qty_memory);
            $('#qty_memory_mb_edit').val(qty_memory);
        }
        if(qty_cntrlr == 0){
            $('#qty_cntrlr_mb_edit').val("");
        }else{
            $('#qty_cntrlr_mb_edit').val(qty_cntrlr);
            $('#qty_cntrlr_mb_edit_total').text(qty_cntrlr);
        }
        if(qty_controllertwo == 0){
            $('#qty_cntrlrtwo_mb_edit').val("");
        }else{
            $('#qty_cntrlrtwo_mb_edit').val(qty_controllertwo);
            $('#qty_cntrlrtwo_mb_edit_two_total').text(qty_controllertwo);
        }
        if(qty_controllerthree == 0){
            $('#qty_cntrlrthree_mb_edit').val("");
        }else{
            $('#qty_cntrlrthree_mb_edit').val(qty_controllerthree);
            $('#qty_cntrlrthree_mb_edit_total').text(qty_controllerthree);
        }
        if(qty_psu == 0){
            $('#qty_psu_mb_edit').val("");
        }else{
            $('#qty_psu_mb_edit').val(qty_psu);
            $('#qty_psu_mb_edit_total').text(qty_psu);
        }
        if(qty_mounting == 0){
            $('#qty_mounting_mb_edit').val("");
        }else{
            $('#qty_mounting_mb_edit').val(qty_mounting);
            $('#qty_mounting_mb_edit_total').text(qty_mounting);
        }    

    });

    $('#cpu_mb_edit').click(function(){
        var datas = {
            'motherboard' : $('#motherboard_mb_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/get_cpu_mb_edit`,
            type : "post",
            data : datas,
            success : function(cpu2){
                    $('#cpu_motherboard_mb_edit').html(cpu2);       
                }
        });
    });

    $('#memory_mb_edit').click(function(){
        var datas = {
            'motherboard' : $('#motherboard_mb_edit').val(),
            ajax : '1'
        };
        console.log(datas);
        $.ajax({
            url :  `${baseUrl}index.php/work_orders/get_memory_mb_edit`,
            type : "post",
            data : datas,
            success : function(memory2){
                    $('#memory_motherboard_mb_edit').html(memory2);       
                }
            });
    });

    $('#motherboard_mb_edit').change(function(){
        var datas = {
            'motherboard' : $('#motherboard_mb_edit').val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/work_orders/get_cpu_mb_edit`,
            type : "post",
            data : datas,
            success : function(cpu2){
                    $('#cpu_motherboard_mb_edit').html(cpu2);       
                }
        });

         var datas = {
            'motherboard' : $('#motherboard_mb_edit').val(),
            ajax : '1'
        };
        console.log(datas);
        $.ajax({
            url :  `${baseUrl}index.php/work_orders/get_memory_mb_edit`,
            type : "post",
            data : datas,
            success : function(memory2){
                    $('#memory_motherboard_mb_edit').html(memory2);       
                }
            }).then(function(){
                var datas = {
                    motherboard_ : $("#motherboard_mb_edit").val(),
                    cpu_ : $("#cpu_mb_edit").val(),
                    memory_ : $("#memory_mb_edit").val(),
                    ajax : '1'   
                };
                $.ajax({
                    url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                    type : "post",
                    data : datas,
                    success : function(response_ob_nic_pci_slot){
                        var datas = {
                            motherboard_ : $("#motherboard_mb_edit").val(),
                            cpu_ : $("#cpu_mb_edit").val(),
                            memory_ : $("#memory_mb_edit").val(),
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                            type : "post",
                            data : datas,
                            success : function(response_ob_nic_pci_slot){
                                $("#ob_nic_mb_edit").text(response_ob_nic_pci_slot.ob_nic);
                                $("#pcie_slot_mb_edit").text(response_ob_nic_pci_slot.pci_slot);
                                    ///$('#motherboard_chassis').html(motherboard);       
                                }
                            });
                            ///$('#motherboard_chassis').html(motherboard);       
                        }
                    });
            });;;
    });
    // for edit
    $('#update_order_motherboard').click(function(){
        if( $('#cpu_mb_edit').val() != null && $('#qty_cpu_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu_mb_edit').val() == null && $('#qty_cpu_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory_mb_edit').val() == null && $('#qty_memory_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory_mb_edit').val() != null && $('#qty_memory_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);
    
        }else if($('#cntrl_mb_edit').val() == '' && $('#qty_cntrlr_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrl_mb_edit').val() != '' && $('#qty_cntrlr_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#cntrltwo_mb_edit').val() == '' && $('#qty_cntrlrtwo_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrltwo_mb_edit').val() != '' && $('#qty_cntrlrtwo_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#cntrlthree_mb_edit').val() == '' && $('#qty_cntrlrthree_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrlthree_mb_edit').val() != '' && $('#qty_cntrlrthree_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu_mb_edit').val() == '' && $('#qty_psu_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_mb_edit').val() != '' && $('#qty_psu_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#motherboard_mb_edit').val() == null && $('#qty_motherboard_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard_mb_edit').val() != null && $('#qty_motherboard_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting_mb_edit').val() == '' && $('#qty_mounting_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting_mb_edit').val() != '' && $('#qty_mounting_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{

        var datas = {
            'work_order_id' : $("#work_order_id_mb").text(),
            'motherboard' : $("#motherboard_mb_edit").val(),
            'qty_motherboard' : $("#qty_motherboard_mb_edit").val(),
            'cpu' : $("#cpu_mb_edit").val(),
            'qty_cpu' : $("#qty_cpu_mb_edit").val(),
            'memory' : $("#memory_mb_edit").val(),
            'qty_memory' : $("#qty_memory_mb_edit").val(),
            'ob_nic' : $("#ob_nic_mb_edit").text(),
            'pcie' : $("#pcie_slot_mb_edit").text(),
            'cntrl' : $("#cntrl_mb_edit").val(),
            'qty_cntrlr' : $("#qty_cntrlr_mb_edit").val(),
            'cntrltwo' : $("#cntrltwo_mb_edit").val(),
            'qty_cntrlrtwo' : $("#qty_cntrlrtwo_mb_edit").val(),
            'cntrlthree' : $("#cntrlthree_mb_edit").val(),
            'qty_cntrlrthree' : $("#qty_cntrlrthree_mb_edit").val(),
            'psu' : $("#psu_mb_edit").val(),
            'qty_psu' : $("#qty_psu_mb_edit").val(),
            'mounting' : $("#mounting_mb_edit").val(),
            'qty_mounting' : $("#qty_mounting_mb_edit").val(),
            'warranty' : $("#warranty_mb_edit").val(),
            'os' : $("#os_mb_edit").val(),
            'barcode' : $("#txt_input").val(),
            'barcode_id' : $("#barcode_edit").text(),
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/update_each_order`,
            type : "post",
            data : datas,
            success : function(response){
                $('#modalMotherboard_edit').modal('hide');
                $("#error_input_mb_edit").html("<p></p>")
                

                

                if($("#barcode_edit").text() == ""){
                    $('#add_order_table_body').html(response);
                }else{
                    $('#add_order_table_body_edit').html(response);
                }
                paginate();
                }
            });

            return
        var data_edit = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };
        console.log(data_edit);
        $.post(`${baseUrl}index.php/work_orders/get_barcode`, data_edit, function(response) {
            $('#add_order_table_body_edit').html(response);
            paginate();
            });
        }

    });

    // for add


    document.getElementById("product_table_body").addEventListener('click', function(){

        var text = document.getElementById("txt_input_edit").value;
        JsBarcode("#barcode_edit", text, {

            lineColor: "#000000",
            height: 140,
            marginTop: 40,
            marginBottom: 20
        });

        var datas = {
            'barcode' : $('#txt_input_edit').val()
        };
        $.post(`${baseUrl}index.php/work_orders/get_barcode_edit`, datas, function(response) {
            $('#add_order_table_body_edit').html(response);
            paginate();
            });

        var datas = {
            'barcode' : $('#txt_input_edit').val()
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/get_customer_detail_edit`,
            type : "post",
            data : datas,
            success : function(response){
                $('#details_all_edit').val(response.details_all);
                $('#ship_date_edit input').val(response.ship_date);
                $('#ship_method_edit').val(response.ship_method);
            }
        });

       
    });

    $('#add_order_table_body').on('click', '.edit-item', function(){
        const button = $(event.target).closest('.edit-item');
        if(!button) return;
        var chassis = button.closest('tr').find('.item-chassis').text();
        const qty_chassis = button.closest('tr').find('.item-qty_chassis').text();
        const backplane = button.closest('tr').find('.item-backplane').text();
        const nodes = button.closest('tr').find('.item-nodes').text();
        const rearbays = button.closest('tr').find('.item-rearbays').text();
        const drivebays = button.closest('tr').find('.item-drivebays').text();
        const ff = button.closest('tr').find('.item-ff').text().trim();
        const rff = button.closest('tr').find('.item-rff').text().trim();
        const psu_slots = button.closest('tr').find('.item-psu_slots').text();
        const bin = button.closest('tr').find('.item-bin').text();
        const front_caddies = button.closest('tr').find('.item-front_caddies').text();
        const qty_front_caddies = button.closest('tr').find('.item-qty_front_caddies').text();
        const rear_caddies = button.closest('tr').find('.item-rear_caddies').text();
        const qty_rear_caddies = button.closest('tr').find('.item-qty_rear_caddies').text();
        const item_harddrive_one = button.closest('tr').find('.item-item_harddrive_one').text();
        const item_harddrive_two = button.closest('tr').find('.item-item_harddrive_two').text();
        const item_harddrive_three = button.closest('tr').find('.item-item_harddrive_three').text();
        const qty_hard_drive_one = button.closest('tr').find('.item-qty_hard_drive_one').text();
        const qty_hard_drive_two = button.closest('tr').find('.item-qty_hard_drive_two').text();
        const qty_hard_drive_three = button.closest('tr').find('.item-qty_hard_drive_three').text();
        const motherboard = button.closest('tr').find('.item-motherboard').text();
        const qty_motherboard = button.closest('tr').find('.item-qty_motherboard').text();
        const cpu = button.closest('tr').find('.item-cpu').text();
        const cpu_qty = button.closest('tr').find('.item-qty_cpu').text();
        const memory = button.closest('tr').find('.item-memory').text();
        const qty_memory = button.closest('tr').find('.item-qty_memory').text();
        const cntrl = button.closest('tr').find('.item-cntrl').text();
        const controllertwo = button.closest('tr').find('.item-controllertwo').text();
        const controllerthree = button.closest('tr').find('.item-controllerthree').text();
        const qty_cntrlr = button.closest('tr').find('.item-qty_cntrlr').text();
        const qty_controllertwo = button.closest('tr').find('.item-qty_controllertwo').text();
        const qty_controllerthree = button.closest('tr').find('.item-qty_controllerthree').text();
        const psu = button.closest('tr').find('.item-psu').text();
        const qty_psu = button.closest('tr').find('.item-qty_psu').text();
        const mounting = button.closest('tr').find('.item-mounting').text();
        const qty_mounting = button.closest('tr').find('.item-qty_mounting').text();
        const warranty = button.closest('tr').find('.item-warranty').text();
        const os = button.closest('tr').find('.item-os').text();
        const work_order_id = button.closest('tr').find('.item-work_order_id').text();

        const drive_ff = drivebays + " " + ff;
        const rear_rff = rearbays + " " + rff;

        $(`#chassis_edit option:contains(${chassis})`).prop('selected',true);
        $chassis_edit_ = $("<option selected='selected'></option>").val(chassis).text(chassis)
        $("#chassis_edit").append($chassis_edit_).trigger('change');
        
        //$('#chassis_edit').val(chassis);
        $('#qty_chassis_edit').val(qty_chassis);
        $('#backplane_edit').text(backplane);
        $('#nodes_edit').text(nodes);
        $('#rearbays_edit').text(rear_rff);
        $('#drivebays_edit').text(drive_ff);
        $('#psuchassis_edit').text(psu_slots);
        $('#bin_edit').text(bin);
        ///$('#item_harddrive_one_edit').val(item_harddrive_one);


        var item_harddrive_one_edit_remove = {};
        $('#item_harddrive_one_edit option').each(function () {
            if (item_harddrive_one_edit_remove[this.value]) {
                $(this).remove()
            }
            item_harddrive_one_edit_remove[this.value] = true;
        })


        var item_harddrive_two_edit_remove = {};
        $('#item_harddrive_two_edit option').each(function () {
            if (item_harddrive_two_edit_remove[this.value]) {
                $(this).remove()
            }
            item_harddrive_two_edit_remove[this.value] = true;
        })


        var item_harddrive_three_edit_remove = {};
        $('#item_harddrive_three_edit option').each(function () {
            if (item_harddrive_three_edit_remove[this.value]) {
                $(this).remove()
            }
            item_harddrive_three_edit_remove[this.value] = true;
        })

        if(item_harddrive_one == "" || item_harddrive_one == null || item_harddrive_one == "null"){
            if(item_harddrive_one == null || item_harddrive_one == "" || item_harddrive_one == "null" || item_harddrive_one == undefined || item_harddrive_one === null || item_harddrive_one === "null" || item_harddrive_one === undefined){
                var put_item_harddrive_one = "null"
                $(`#item_harddrive_one_edit option:contains(${put_item_harddrive_one})`).prop('selected',true);
                var $item_harddrive_one_ = $("<option selected='selected'></option>").val(put_item_harddrive_one).text('')
                $("#item_harddrive_one_edit").append($item_harddrive_one_).trigger('change');
            }else{
                var put_item_harddrive_one = item_harddrive_one
                $(`#item_harddrive_one_edit option:contains(${put_item_harddrive_one})`).prop('selected',true);
                var $item_harddrive_one_ = $("<option selected='selected'></option>").val(put_item_harddrive_one).text(put_item_harddrive_one)
                $("#item_harddrive_one_edit").append($item_harddrive_one_).trigger('change');
            }
        }else{
            $(`#item_harddrive_one_edit option:contains(${item_harddrive_one})`).prop('selected',true);
            var $item_harddrive_one_ = $("<option selected='selected'></option>").val(item_harddrive_one).text(item_harddrive_one)
            $("#item_harddrive_one_edit").append($item_harddrive_one_).trigger('change');
        }

        ///$('#item_harddrive_two_edit').val(item_harddrive_two);
        if(item_harddrive_two == "" || item_harddrive_two == null || item_harddrive_two == "null"){
            if(item_harddrive_two == null || item_harddrive_two == "" || item_harddrive_two == "null" || item_harddrive_two == undefined || item_harddrive_two === null || item_harddrive_two === "null" || item_harddrive_two === undefined){
                var put_item_harddrive_two = "null"
                $(`#item_harddrive_two_edit option:contains(${put_item_harddrive_two})`).prop('selected',true);
                var $item_harddrive_two_ = $("<option selected='selected'></option>").val(put_item_harddrive_two).text('')
                $("#item_harddrive_two_edit").append($item_harddrive_two_).trigger('change');
            }else{
                var put_item_harddrive_two = item_harddrive_two
                $(`#item_harddrive_two_edit option:contains(${put_item_harddrive_two})`).prop('selected',true);
                $item_harddrive_two_ = $("<option selected='selected'></option>").val(put_item_harddrive_two).text(put_item_harddrive_two)
                $("#item_harddrive_two_edit").append($item_harddrive_two_edit_).trigger('change');
            }
        }else{
            $(`#item_harddrive_two_edit option:contains(${item_harddrive_two})`).prop('selected',true);
            $item_harddrive_two_edit_ = $("<option selected='selected'></option>").val(item_harddrive_two).text(item_harddrive_two)
            $("#item_harddrive_two_edit").append($item_harddrive_two_edit_).trigger('change');
        }
        ///$('#item_harddrive_three_edit').val(item_harddrive_three);


        if(item_harddrive_three == "" || item_harddrive_three == null || item_harddrive_three == "null"){
            if(item_harddrive_three == null || item_harddrive_three == "" || item_harddrive_three == "null" || item_harddrive_three == undefined || item_harddrive_three === null || item_harddrive_three === "null" || item_harddrive_three === undefined){
                var put_item_harddrive_three = "null"
                $(`#item_harddrive_three_edit option:contains(${put_item_harddrive_three})`).prop('selected',true);
                var $item_harddrive_three_edit_ = $("<option selected='selected'></option>").val(put_item_harddrive_three).text('')
                $("#item_harddrive_three_edit").append($item_harddrive_three_edit_).trigger('change');
            }else{
                var put_item_harddrive_three = item_harddrive_three
                $(`#item_harddrive_three_edit option:contains(${put_item_harddrive_three})`).prop('selected',true);
                $item_harddrive_three_edit_ = $("<option selected='selected'></option>").val(put_item_harddrive_three).text(put_item_harddrive_two)
                $("#item_harddrive_three_edit").append($item_harddrive_three_edit_).trigger('change');
            }
        }else{
            $(`#item_harddrive_three_edit option:contains(${item_harddrive_three})`).prop('selected',true);
            $item_harddrive_three_edit_ = $("<option selected='selected'></option>").val(item_harddrive_three).text(item_harddrive_three)
            $("#item_harddrive_three_edit").append($item_harddrive_three_edit_).trigger('change');
        }

        
        

        $('#motherboard_edit').val(motherboard);        
        $('#cpu_edit').val(cpu);       
        $('#memory_edit').val(memory);        
        $('#cntrl_edit').val(cntrl);
        $('#cntrltwo_edit').val(controllertwo);
        $('#cntrlthree_edit').val(controllerthree);
        $('#psu_edit').val(psu);       
        $('#mounting_edit').val(mounting);        
        $('#warranty_ch_edit').val(warranty);
        $('#os_edit').val(os);
        $('#work_order_id').text(work_order_id);

        if(front_caddies == 'null'){
            $('.front_caddies_display').html();
        }else{
            $('.front_caddies_display').html(front_caddies);
        }
        if(rear_caddies == 'null'){
            $('.rear_caddies_display').html();
        }else{
            $('.rear_caddies_display').html(rear_caddies);
        }
        if(qty_front_caddies == 0){
            $('#qty_front_caddies_edit').val("");
        }else{
            $('#qty_front_caddies_edit').val(qty_front_caddies);
        }
        if(qty_rear_caddies == 0){
            $('#qty_rear_caddies_edit').val("");
        }else{
            $('#qty_rear_caddies_edit').val(qty_rear_caddies);
        }
        if(qty_hard_drive_one == 0){
            $('#qty_hard_drive_one_edit').val("");
        }else{
            $('#qty_hard_drive_one_edit').val(qty_hard_drive_one);
        }
        if(qty_hard_drive_two == 0){
            $('#qty_hard_drive_two_edit').val("");
        }else{
            $('#qty_hard_drive_two_edit').val(qty_hard_drive_two);
        }
        if(qty_hard_drive_three == 0){
            $('#qty_hard_drive_three_edit').val("");
        }else{
            $('#qty_hard_drive_three_edit').val(qty_hard_drive_three);
        }
        if(qty_motherboard == 0){
            $('#qty_motherboard_edit').val("");
        }else{
            $('#qty_motherboard_edit').val(qty_motherboard);
        }
        if(cpu_qty == 0){
            $('#qty_cpu_edit').val("");
        }else{
            $('#qty_cpu_edit').val(cpu_qty);
        }
        if(qty_memory == 0){
            $('#qty_memory_edit').val("");
        }else{
            $('#qty_memory_edit').val(qty_memory);
        }
        if(qty_cntrlr == 0){
            $('#qty_cntrlr_edit').val("");
        }else{
            $('#qty_cntrlr_edit').val(qty_cntrlr);
        }
        if(qty_controllertwo == 0){
            $('#qty_cntrlrtwo_edit').val("");
        }else{
            $('#qty_cntrlrtwo_edit').val(qty_controllertwo);
        }
        if(qty_controllerthree == 0){
            $('#qty_cntrlrthree_edit').val("");
        }else{
            $('#qty_cntrlrthree_edit').val(qty_controllerthree);
        }
        if(qty_psu == 0){
            $('#qty_psu_edit').val("");
        }else{
            $('#qty_psu_edit').val(qty_psu);
        }
        if(qty_mounting == 0){
            $('#qty_mounting_edit').val("");
        }else{
            $('#qty_mounting_edit').val(qty_mounting);
        }

        $('#front_caddies_edit').find('option:not(:first)').remove();
        $('#rear_caddies_edit').find('option:not(:first)').remove();
        $('#modalChassis_edit').modal('show');
    });
    
    $('#edit_order_chassis').click(function(event){
        if($('#chassis_edit').val() != null && $('#qty_chassis_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Chassis quantity </h5>
                    </div>`);

        }else if($('#chassis_edit').val() == null && $('#qty_chassis_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Chassis Quantity. No selected option </h5>
                    </div>`);

        }else if( $('#cpu_edit').val() != null && $('#qty_cpu_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu_edit').val() == null && $('#qty_cpu_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory_edit').val() == null && $('#qty_memory_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory_edit').val() != null && $('#qty_memory_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);

        }else if($('#item_harddrive_one_edit').val() === 'null' && $('#qty_hard_drive_one_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_one_edit').val() != 'null' && $('#qty_hard_drive_one_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#item_harddrive_two_edit').val() === 'null' && $('#qty_hard_drive_two_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_two_edit').val() != 'null' && $('#qty_hard_drive_two_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#item_harddrive_three_edit').val() === 'null' && $('#qty_hard_drive_three_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#item_harddrive_three_edit').val() != 'null' && $('#qty_hard_drive_three_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#cntrl_edit').val() == '' && $('#qty_cntrlr_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrl_edit').val() != '' && $('#qty_cntrlr_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#cntrltwo_edit').val() == '' && $('#qty_cntrlrtwo_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrltwo_edit').val() != '' && $('#qty_cntrlrtwo_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#cntrlthree_edit').val() == '' && $('#qty_cntrlrthree_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrlthree_edit').val() != '' && $('#qty_cntrlrthree_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu_edit').val() == '' && $('#qty_psu_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_edit').val() != '' && $('#qty_psu_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#front_caddies_edit').val() == '' && $('#qty_front_caddies_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Front Caddies Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#rear_caddies_edit').val() == '' && $('#qty_rear_caddies_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Rear Caddies Quantity. No selected optoin</h5>
                    </div>`);
            
        }else if($('#motherboard_edit').val() == null && $('#qty_motherboard_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard_edit').val() != null && $('#qty_motherboard_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting_edit').val() == '' && $('#qty_mounting_edit').val() != ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting_edit').val() != '' && $('#qty_mounting_edit').val() == ''){
            $('#modalChassis_edit').scrollTop(0);
            $("#error_input_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{

        var datas = {
            'work_order_id' : $("#work_order_id").text(),
            'chassis' : $("#chassis_edit").val(),
            'qty_chassis' : $("#qty_chassis_edit").val(),
            'backplane'     : $('#backplane_edit').text(),
            'drivebays'     : $('#drivebays_edit').text(),
            'rearbays'      : $('#rearbays_edit').text(),
            'nodes'          : $('#nodes_edit').text(),
            'psuchassis'     : $('#psuchassis_edit').text(),
            'bin'           : $('#bin_edit').text(),
            'front_caddies' : $("#front_caddies_edit").val(),
            'qty_front_caddies' : $("#qty_front_caddies_edit").val(),
            'rear_caddies' : $("#rear_caddies_edit").val(),
            'qty_rear_caddies' : $("#qty_rear_caddies_edit").val(),
            'item_harddrive_one' : $("#item_harddrive_one_edit").val(),
            'item_harddrive_two' : $("#item_harddrive_two_edit").val(),
            'item_harddrive_three' : $("#item_harddrive_three_edit").val(),
            'qty_hard_drive_one' : $("#qty_hard_drive_one_edit").val(),
            'qty_hard_drive_two' : $("#qty_hard_drive_two_edit").val(),
            'qty_hard_drive_three' : $("#qty_hard_drive_three_edit").val(),
            'motherboard' : $("#motherboard_edit").val(),
            'qty_motherboard' : $("#qty_motherboard_edit").val(),
            'cpu' : $("#cpu_edit").val(),
            'qty_cpu' : $("#qty_cpu_edit").val(),
            'memory' : $("#memory_edit").val(),
            'qty_memory' : $("#qty_memory_edit").val(),


            'ob_nic' : $("#ob_nic_edit").text(),
            'pcie' : $("#pcie_slot_edit").text(),

            'cntrl' : $("#cntrl_edit").val(),
            'qty_cntrlr' : $("#qty_cntrlr_edit").val(),
            'cntrltwo' : $("#cntrltwo_edit").val(),
            'qty_cntrlrtwo' : $("#qty_cntrlrtwo_edit").val(),
            'cntrlthree' : $("#cntrlthree_edit").val(),
            'qty_cntrlrthree' : $("#qty_cntrlrthree_edit").val(),
            'psu' : $("#psu_edit").val(),
            'qty_psu' : $("#qty_psu_edit").val(),
            'mounting' : $("#mounting_edit").val(),
            'qty_mounting' : $("#qty_mounting_edit").val(),
            'warranty' : $("#warranty_ch_edit").val(),
            'os' : $("#os_edit").val(),
            'barcode' : $('#txt_input').val(),
            'barcode_id' : $('#barcode_edit').text(),
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/update_each_order_for_addOrder`,
            type : "post",
            data : datas,
            success : function(response){
                $('#modalChassis_edit').modal('hide');
                $("#error_input_edit").html("<p></p>")
                
                if($('#barcode_edit').text() == ""){
                    $('#add_order_table_body').html(response);
                }else{
                    $('#add_order_table_body_edit').html(response);
                }
                paginate();
                
                }
            });
         /*var data = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };

        $.post(`${baseUrl}index.php/work_orders/get_barcode`, data, function(response) {
            $('#add_order_table_body').html(response);
            paginate();
            });*/
        }  
        
    });

    $('#add_order_table_body').on('click', '.edit-mb', function(event){
        const buttonmb = $(event.target).closest('.edit-mb');
        if(!buttonmb) return;
        const motherboard = buttonmb.closest('tr').find('.item-motherboard').text();
        const qty_motherboard = buttonmb.closest('tr').find('.item-qty_motherboard').text();
        const cpu = buttonmb.closest('tr').find('.item-cpu').text();
        const cpu_qty = buttonmb.closest('tr').find('.item-qty_cpu').text();
        const memory = buttonmb.closest('tr').find('.item-memory').text();
        const qty_memory = buttonmb.closest('tr').find('.item-qty_memory').text();
        const cntrl = buttonmb.closest('tr').find('.item-cntrl').text();
        const controllertwo = buttonmb.closest('tr').find('.item-controllertwo').text();
        const controllerthree = buttonmb.closest('tr').find('.item-controllerthree').text();
        const qty_cntrlr = buttonmb.closest('tr').find('.item-qty_cntrlr').text();
        const qty_controllertwo = buttonmb.closest('tr').find('.item-qty_controllertwo').text();
        const qty_controllerthree = buttonmb.closest('tr').find('.item-qty_controllerthree').text();
        const psu = buttonmb.closest('tr').find('.item-psu').text();
        const qty_psu = buttonmb.closest('tr').find('.item-qty_psu').text();
        const mounting = buttonmb.closest('tr').find('.item-mounting').text();
        const qty_mounting = buttonmb.closest('tr').find('.item-qty_mounting').text();
        const warranty = buttonmb.closest('tr').find('.item-warranty').text();
        const os = buttonmb.closest('tr').find('.item-os').text();
        const work_order_id = buttonmb.closest('tr').find('.item-work_order_id').text();

        $('#motherboard_mb_edit').val(motherboard);

        var motherboard_mb_edit_remove = {};
        $('#motherboard_mb_edit option').each(function () {
            if (motherboard_mb_edit_remove[this.value]) {
                $(this).remove()
            }
            motherboard_mb_edit_remove[this.value] = true;
        })

        $(`#motherboard_mb_edit option:contains(${motherboard})`).prop('selected',true);
        $motherboard_mb_edit_ = $("<option selected='selected'></option>").val(motherboard).text(motherboard)
        $("#motherboard_mb_edit").append($motherboard_mb_edit_).trigger('change');

        $('#cpu_mb_edit').html(cpu);       
        $('#memory_mb_edit').html(memory);        
        $('#cntrl_mb_edit').val(cntrl);
        $('#cntrltwo_mb_edit').val(controllertwo);
        $('#cntrlthree_mb_edit').val(controllerthree);
        $('#psu_mb_edit').val(psu);       
        $('#mounting_mb_edit').val(mounting);        
        $('#warranty_mb_edit').val(warranty);
        $('#os_mb_edit').val(os);
        $('#work_order_id_mb').text(work_order_id);

        $('#modalMotherboard_edit').modal('show');

        if(qty_motherboard == 0){
            $('#qty_motherboard_mb_edit').val();
        }else{
            $('#qty_motherboard_mb_edit').val(qty_motherboard);
        }
        if(cpu_qty == 0){
            $('#qty_cpu_mb_edit').val();
        }else{
            $('#qty_cpu_mb_edit').val(cpu_qty);
        }
        if(qty_memory == 0){
            $('#qty_memory_mb_edit').val("");
        }else{
            $('#qty_memory_mb_edit').val(qty_memory);
        }
        if(qty_cntrlr == 0){
            $('#qty_cntrlr_mb_edit').val();
        }else{
            $('#qty_cntrlr_mb_edit').val(qty_cntrlr);
        }
        if(qty_controllertwo == 0){
            $('#qty_cntrlrtwo_mb_edit').val();
        }else{
            $('#qty_cntrlrtwo_mb_edit').val(qty_controllertwo);
        }
        if(qty_controllerthree == 0){
            $('#qty_cntrlrthree_mb_edit').val();
        }else{
            $('#qty_cntrlrthree_mb_edit').val(qty_controllerthree);
        }
        if(qty_psu == 0){
            $('#qty_psu_mb_edit').val();
        }else{
            $('#qty_psu_mb_edit').val(qty_psu);
        }
        if(qty_mounting == 0){
            $('#qty_mounting_mb_edit').val();
        }else{
            $('#qty_mounting_mb_edit').val(qty_mounting);
        }    

    });

  /*  $('#update_order_motherboard').click(function(){
        if( $('#cpu_mb_edit').val() != null && $('#qty_cpu_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu_mb_edit').val() == null && $('#qty_cpu_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory_mb_edit').val() == null && $('#qty_memory_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory_mb_edit').val() != null && $('#qty_memory_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);
    
        }else if($('#cntrl_mb_edit').val() == '' && $('#qty_cntrlr_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrl_mb_edit').val() != '' && $('#qty_cntrlr_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#cntrltwo_mb_edit').val() == '' && $('#qty_cntrlrtwo_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrltwo_mb_edit').val() != '' && $('#qty_cntrlrtwo_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#cntrlthree_mb_edit').val() == '' && $('#qty_cntrlrthree_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#cntrlthree_mb_edit').val() != '' && $('#qty_cntrlrthree_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu_mb_edit').val() == '' && $('#qty_psu_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_mb_edit').val() != '' && $('#qty_psu_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#motherboard_mb_edit').val() == null && $('#qty_motherboard_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard_mb_edit').val() != null && $('#qty_motherboard_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting_mb_edit').val() == '' && $('#qty_mounting_mb_edit').val() != ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting_mb_edit').val() != '' && $('#qty_mounting_mb_edit').val() == ''){
            $('#modalMotherboard_edit').scrollTop(0);
            $("#error_input_mb_edit").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{

        var datas = {
            'work_order_id' : $("#work_order_id_mb").text(),
            'motherboard' : $("#motherboard_mb_edit").val(),
            'qty_motherboard' : $("#qty_motherboard_mb_edit").val(),
            'cpu' : $("#cpu_mb_edit").val(),
            'qty_cpu' : $("#qty_cpu_mb_edit").val(),
            'memory' : $("#memory_mb_edit").val(),
            'qty_memory' : $("#qty_memory_mb_edit").val(),
            'cntrl' : $("#cntrl_mb_edit").val(),
            'qty_cntrlr' : $("#qty_cntrlr_mb_edit").val(),
            'cntrltwo' : $("#cntrltwo_mb_edit").val(),
            'qty_cntrlrtwo' : $("#qty_cntrlrtwo_mb_edit").val(),
            'cntrlthree' : $("#cntrlthree_mb_edit").val(),
            'qty_cntrlrthree' : $("#qty_cntrlrthree_mb_edit").val(),
            'psu' : $("#psu_mb_edit").val(),
            'qty_psu' : $("#qty_psu_mb_edit").val(),
            'mounting' : $("#mounting_mb_edit").val(),
            'qty_mounting' : $("#qty_mounting_mb_edit").val(),
            'warranty' : $("#warranty_mb_edit").val(),
            'os' : $("#os_mb_edit").val(),
            ajax : '1'   
        };
        $.ajax({
            url: `${baseUrl}index.php/work_orders/update_each_order_for_addOrder`,
            type : "post",
            data : datas,
            success : function(response){
                $('#modalMotherboard_edit').modal('hide');

                }
            });

        var data = {
            'barcode' : $('#txt_input').val(),
            ajax : '1'
        };
        $.post(`${baseUrl}index.php/work_orders/get_barcode`, data, function(response) {
            $('#add_order_table_body').html(response);
            paginate();
            });
  
        }

    }); */

    $("#ship_date_edit").datetimepicker({
        format : 'YYYY-MM-DD',
        minDate : dateToday
    });

    

});