$(document).ready(function(){
  /////////////////// for clients //////////////////////
  let barcode_input = "";
  let client_name_input = "";
  let contact_no_input = "";
  let email_input = "";
  let ship_date_input = "";
  let ship_method_input = "";
  let address_input = "";
  /////////////////// for chassis /////////////////////
  let build_id_input = "";
  let chasis_input = "";
  let chassis_change = "";
  let qty_chassis_input = "";
  let front_caddies_input = "";
  let front_caddies_change = "";
  let qty_front_caddies_input = "";
  let qty_front_caddies_change = "";
  let rear_caddies_input = "";
  let rear_caddies_change = "";
  let qty_rear_caddies_input = "";
  let qty_rear_caddies_change = "";
  let item_harddrive_one_input = "";
  let item_harddrive_one_change = "";
  let qty_hard_drive_one_input = "";
  let qty_item_harddrive_one_change = "";
  let item_harddrive_two_input = "";
  let item_harddrive_two_change = "";
  let qty_hard_drive_two_input = "";
  let qty_item_harddrive_two_change = "";
  let item_harddrive_three_input = "";
  let item_harddrive_three_change = "";
  let qty_hard_drive_three_input = "";
  let qty_item_harddrive_three_change = ""; 
  let motherboard_input = "";
  let motherboard_change = "";
  let cpu_input = "";
  let cpu_change = "";
  let memory_input = "";
  let memory_change = "";
  let cntrl_input = "";
  let cntrl_change = "";
  let cntrltwo_input = "";
  let cntrltwo_change = "";
  let cntrlthree_input = "";
  let cntrlthree_change = "";
  let psu_input = "";
  let psu_change = "";
  let mounting_input = "";
  let mounting_change = "";
  let warranty_ch_input = "";
  let warranty_ch_change = "";
  let os_input = "";
  let os_change = "";
  let qty_motherboard_input = "";
  let qty_motherboard_change = "";
  let qty_cpu_input = "";
  let qty_cpu_change = "";
  let qty_memory_input = "";
  let qty_memory_change = "";
  let qty_cntrlr_input = "";
  let qty_cntrlr_change = "";
  let qty_psu_input = "";
  let qty_psu_change = "";
  let qty_mounting_input = "";
  let qty_mounting_change = "";
  let qty_cntrlrtwo_input = "";
  let qty_cntrlrtwo_change = "";
  let qty_cntrlrthree_input = "";
  let qty_cntrlrthree_change = "";
  ////////////////////// for motherboard only /////////////////////////
  let motherboard_mb_input = "";
  let motherboard_mb_change = "";
  let cpu_mb_input = "";
  let cpu_mb_change = "";
  let memory_mb_input = "";
  let memory_mb_change = "";
  let cntrl_mb_input = "";
  let cntrl_mb_change = "";
  let cntrltwo_mb_input = "";
  let cntrltwo_mb_change = "";
  let cntrlthree_mb_input = "";
  let cntrlthree_mb_change = "";
  let psu_mb_input = "";
  let psu_mb_change = "";
  let mounting_mb_input = "";
  let mounting_mb_change = "";
  let warranty_mb_input = "";
  let warranty_mb_change = "";
  let os_mb_input = "";
  let os_mb_change = "";

  let qty_motherboard_mb_input = "";
  let qty_motherboard_mb_change = "";
  let qty_cpu_mb_input = "";
  let qty_cpu_mb_change = "";
  let qty_memory_mb_input = "";
  let qty_memory_mb_change = "";
  let qty_cntrlr_mb_input = "";
  let qty_cntrlr_mb_change = "";
  let qty_cntrlrtwo_mb_input = "";
  let qty_cntrlrtwo_mb_change = "";
  let qty_cntrlrthree_mb_input = "";
  let qty_cntrlrthree_mb_change = "";
  let qty_psu_mb_input = "";
  let qty_psu_mb_change = "";
  let qty_mounting_mb_input = "";
  let qty_mounting_mb_change = "";
  let build_id_mb_input = "";
  let build_id_mb_change = "";
  
  let backplane_input = "";
  let backplane_change = "";
  let drivebays_input = "";
  let drivebays_change = "";
  let rearbays_input = "";
  let rearbays_change = "";
  let nodes_input = "";
  let nodes_change = "";
  let psuchassis_input = "";
  let psuchassis_change = "";
  let bin_input = "";
  let bin_change = "";
  
  
  let details_all_input = "";
  let details_all_change = "";


    
    let getmodal = "";
    
    if(localStorage['qty_motherboard_mb'] !== "" || localStorage['qty_cpu_mb'] !== "" || localStorage['qty_memory_mb'] !== "" || localStorage['qty_cntrlr_mb'] !== "" || localStorage['qty_cntrlrtwo_mb'] !== "" ||
    localStorage['qty_cntrlrthree_mb'] !== "" || localStorage['cntrl_mb'] !== "" || localStorage['cntrltwo_mb'] !== "" || localStorage['cntrlthree_mb'] !== "" || localStorage['qty_psu_mb'] !== "" || 
    localStorage['qty_mounting_mb'] !== "" || localStorage['os_mb']){
      if(localStorage['barcode'] === "" || localStorage['barcode'] === undefined || localStorage['barcode'] === "undefined" && localStorage['ship_date'] === "" && localStorage['details_all'] === ""){
        console.log("no modal pop up")
      }else{
        $('#myModal').modal('show');
        $('#modalMotherboard').modal('show');
      }
    }else {
      if((localStorage['qty_chassis'] === "undefined" || localStorage['qty_chassis'] === undefined || localStorage['qty_chassis'] === "") && localStorage['qty_front_caddies'] === "" && localStorage['qty_rear_caddies'] === "" &&
      localStorage['qty_hard_drive_one'] === "" && localStorage['qty_hard_drive_two'] === "" && localStorage['qty_hard_drive_three'] === "" && localStorage['qty_motherboard'] === "" && localStorage['qty_cpu'] === "" &&
      localStorage['qty_memory'] === "" && localStorage['qty_cntrlr'] === "" && localStorage['qty_cntrlrtwo'] === "" && localStorage['qty_cntrlrthree'] === "" && localStorage['cntrl'] === "" && localStorage['cntrltwo'] === "" &&
      localStorage['cntrlthree'] === "" && localStorage['qty_psu'] === "" && localStorage['psu'] === "" && localStorage['qty_mounting'] === "" && localStorage['os'] === "" && (localStorage['chasis'] === null || localStorage['chasis'] === 'null') &&
      (localStorage['front_caddies'] === null || localStorage['front_caddies'] === "null") && (localStorage['rear_caddies'] === null || localStorage['rear_caddies'] === 'null') && (localStorage['item_harddrive_one'] === null || 
       localStorage['item_harddrive_one'] === 'null') &&
      (localStorage['item_harddrive_two'] === null || localStorage['item_harddrive_two'] === 'null') && (localStorage['item_harddrive_three'] === null || localStorage['item_harddrive_three'] === 'null')){
        if((localStorage['barcode'] === "" || localStorage['barcode'] === undefined || localStorage['barcode'] === "undefined") && localStorage['ship_date'] === "" && localStorage['details_all'] === ""){
          console.log("no modal pop up")
        }else{
          $('#myModal').modal('show');
        }
        
      }else{
        if(localStorage['front_caddies'] == "3.5 To Caddy Converter" || localStorage['front_caddies'] == "null"){
          console.log(localStorage['front_caddies'])
                }else{
                  $('#myModal').modal('show');
                  $('#modalChassis').modal('show');
                  ///alert(localStorage['front_caddies'])
                }
      }
    }
    console.log(localStorage['motherboard_mb'])

  $("input").keydown(function(){
    
     
  });
  $("input").keyup(function(){
      localStorage.clear()


      ////////////////////// modal change three
      $("#qty_motherboard_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_cpu_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_memory_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_cntrlr_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_cntrlrtwo_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_cntrlrthree_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_psu_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#qty_mounting_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      $("#os_mb").keyup(function(){
        modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })
      /////////////////////// end modal change three


      /////////////////////// modal change two


      /*$("#qty_chassis").keyup(function(){
        modalshow_input = 'modaltwo'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
      })*/


      //////////////////////// end modal change two
      /// barcode
      barcode_input = $("#txt_input").val()
      var barcode = localStorage['barcode'];
      if (barcode) mybarcode = barcode;
      else mybarcode = barcode_input;
      localStorage['barcode'] = mybarcode;
      /// end barcode


      /// client name
      client_name_input = $("#client_name").val()
      var client_name = localStorage['client_name'];
      if (client_name) myclient_name = client_name;
      else myclient_name = client_name_input;
      localStorage['client_name'] = myclient_name;
      /// end client name

      /// contact_no
      contact_no_input = $("#contact_no").val()
      var contact_no = localStorage['contact_no'];
      if (contact_no) mycontact_no = contact_no;
      else mycontact_no = contact_no_input;
      localStorage['contact_no'] = mycontact_no;
      /// end contact_no


      /// email
      email_input = $("#email").val()
      var email = localStorage['email'];
      if (email) myemail = email;
      else myemail = email_input;
      localStorage['email'] = myemail;
      /// end email


      /// ship date
      ship_date_input = $(".datetimepicker-input").val()
      var ship_date = localStorage['ship_date'];
      if (ship_date) myship_date = ship_date;
      else myship_date = ship_date_input;
      localStorage['ship_date'] = myship_date;
      /// end ship date


      /// ship method on change
      localStorage.removeItem('ship_method')
      ship_method_input = $("#ship_method").val()
      var ship_method = localStorage['ship_method'];
      if (ship_method) myship_method = ship_method;
      else myship_method = ship_method_input;
      localStorage['ship_method'] = myship_method;
      /// end ship method on change


      /// address
      address_input = $("#address").val()
      var address = localStorage['address'];
      if (address) myaddress = address;
      else myaddress = address_input;
      localStorage['address'] = myaddress;
      /// end address


      //////////////////////// for with chassis ///////////////////

      /// build id
      build_id_input = $("#build_id").val()
      var build_id = localStorage['build_id'];
      if (build_id) mybuild_id = build_id;
      else mybuild_id = build_id_input;
      localStorage['build_id'] = mybuild_id;
      /// end build id


      /// chasis
       chasis_input = $("#chasis").val()
       var chasis = localStorage['chasis'];
       if (chasis) mychasis = chasis;
       else mychasis = chasis_input;
       localStorage['chasis'] = mychasis;
      /// end chasis

      /// qty chassis
      qty_chassis_input = $("#qty_chassis").val()
      var qty_chassis = localStorage['qty_chassis'];
      if (qty_chassis) myqty_chassis = qty_chassis;
      else myqty_chassis = qty_chassis_input;
      localStorage['qty_chassis'] = myqty_chassis;
      /// end qty chassis


      /// front caddies
      front_caddies_input = $("#front_caddies").val()
      var front_caddies = localStorage['front_caddies'];
      if (front_caddies) myfront_caddies = front_caddies;
      else myfront_caddies = front_caddies_input;
      localStorage['front_caddies'] = myfront_caddies;
      /// end front caddies
      
      /// qty qty front caddies
      qty_front_caddies_input = $("#qty_front_caddies").val()
      var qty_front_caddies = localStorage['qty_front_caddies'];
      if (qty_front_caddies) myqty_front_caddies = qty_front_caddies;
      else myqty_front_caddies = qty_front_caddies_input;
      localStorage['qty_front_caddies'] = myqty_front_caddies;
      /// end qty front caddies


      /// rear caddies
      localStorage.removeItem('rear_caddies')
      rear_caddies_input = $("#rear_caddies").val()
       var rear_caddies = localStorage['rear_caddies'];
       if (rear_caddies) myrear_caddies = rear_caddies;
       else myrear_caddies = rear_caddies_input;
       localStorage['rear_caddies'] = myrear_caddies;
       /// end rear caddies


       /// qty qty rear caddies
      
      qty_rear_caddies_input = $("#qty_rear_caddies").val()
      var qty_rear_caddies = localStorage['qty_rear_caddies'];
      if (qty_rear_caddies) myqty_rear_caddies = qty_rear_caddies;
      else myqty_rear_caddies = qty_rear_caddies_input;
      localStorage['qty_rear_caddies'] = myqty_rear_caddies;
      /// end qty rear caddies


      /// qty hard drive one
      qty_hard_drive_one_input = $("#qty_hard_drive_one").val()
      var qty_hard_drive_one = localStorage['qty_hard_drive_one'];
      if (qty_hard_drive_one) myqty_hard_drive_one = qty_hard_drive_one;
      else myqty_hard_drive_one = qty_hard_drive_one_input;
      localStorage['qty_hard_drive_one'] = myqty_hard_drive_one;
      /// end qty hard drive one


      /// item harddrive one
      
      item_harddrive_one_input = $("#item_harddrive_one").val()
      var item_harddrive_one = localStorage['item_harddrive_one'];
      if (item_harddrive_one) myitem_harddrive_one = item_harddrive_one;
      else myitem_harddrive_one = item_harddrive_one_input;
      localStorage['item_harddrive_one'] = myitem_harddrive_one;
       
      /// end item harddrive one


      /// item harddrive two
      item_harddrive_two_input = $("#item_harddrive_two").val()
      var item_harddrive_two = localStorage['item_harddrive_two'];
      if (item_harddrive_two) myitem_harddrive_two = item_harddrive_two;
      else myitem_harddrive_two = item_harddrive_two_input;
      localStorage['item_harddrive_two'] = myitem_harddrive_two;
       
      /// end item harddrive two


      /// qty qty hard drive two
      qty_hard_drive_two_input = $("#qty_hard_drive_two").val()
      var qty_hard_drive_two = localStorage['qty_hard_drive_two'];
      if (qty_hard_drive_two) myqty_hard_drive_two = qty_hard_drive_two;
      else myqty_hard_drive_two = qty_hard_drive_two_input;
      localStorage['qty_hard_drive_two'] = myqty_hard_drive_two;
      /// end qty hard drive two


      /// item harddrive three
      item_harddrive_three_input = $("#item_harddrive_three").val()
      var item_harddrive_three = localStorage['item_harddrive_three'];
      if (item_harddrive_three) myitem_harddrive_three = item_harddrive_three;
      else myitem_harddrive_three = item_harddrive_three_input;
      localStorage['item_harddrive_three'] = myitem_harddrive_three;
      /// end item harddrive three


      /// qty qty hard drive three
      qty_hard_drive_three_input = $("#qty_hard_drive_three").val()
      var qty_hard_drive_three = localStorage['qty_hard_drive_three'];
      if (qty_hard_drive_three) myqty_hard_drive_three = qty_hard_drive_three;
      else myqty_hard_drive_three = qty_hard_drive_three_input;
      localStorage['qty_hard_drive_three'] = myqty_hard_drive_three;
      /// end qty hard drive three


      ///cntrl
      cntrl_input = $("#cntrl").val()
      var cntrl = localStorage['cntrl'];
      if (cntrl) mycntrl = cntrl;
      else mycntrl = cntrl_input;
      localStorage['cntrl'] = mycntrl;
      /// end cntrl

      ///cntrl2
      cntrltwo_input = $("#cntrltwo").val()
      var cntrltwo = localStorage['cntrltwo'];
      if (cntrltwo) mycntrltwo = cntrltwo;
      else mycntrltwo = cntrltwo_input;
      localStorage['cntrltwo'] = mycntrltwo;
      /// end cntrl2


      ///cntrl3
      cntrlthree_input = $("#cntrlthree").val()
      var cntrlthree = localStorage['cntrlthree'];
      if (cntrlthree) mycntrlthree = cntrlthree;
      else mycntrlthree = cntrlthree_input;
      localStorage['cntrlthree'] = mycntrlthree;
      /// end cntrl3

      ///psu
      psu_input = $("#psu").val()
      var psu = localStorage['psu'];
      if (psu) mypsu = psu;
      else mypsu = psu_input;
      localStorage['psu'] = mypsu;
      /// end psu

      /// mounting
      mounting_input = $("#mounting").val()
      var mounting = localStorage['mounting'];
      if (mounting) mymounting = mounting;
      else mymounting = mounting_input;
      localStorage['mounting'] = mymounting;
      /// end mounting

      /// warranty
      warranty_ch_input = $("#warranty_ch").val()
      var warranty_ch = localStorage['warranty_ch'];
      if (warranty_ch) mywarranty_ch = warranty_ch;
      else mywarranty_ch = warranty_ch_input;
      localStorage['warranty_ch'] = mywarranty_ch;
      /// end warranty

      /// os
      os_input = $("#os").val()
      var os = localStorage['os'];
      if (os) myos = os;
      else myos = os_input;
      localStorage['os'] = myos;
      /// end os

      /// qty motherboard
      qty_motherboard_input = $("#qty_motherboard").val()
      var qty_motherboard = localStorage['qty_motherboard'];
      if (qty_motherboard) myqty_motherboard = qty_motherboard;
      else myqty_motherboard = qty_motherboard_input;
      localStorage['qty_motherboard'] = myqty_motherboard;
      /// end qty motherboard

      /// qty qty cpu
      qty_cpu_input = $("#qty_cpu").val()
      var qty_cpu = localStorage['qty_cpu'];
      if (qty_cpu) myqty_cpu = qty_cpu;
      else myqty_cpu = qty_cpu_input;
      localStorage['qty_cpu'] = myqty_cpu;
      /// end qty qty cpu

      /// qty memory
      qty_memory_input = $("#qty_memory").val()
      var qty_memory = localStorage['qty_memory'];
      if (qty_memory) myqty_memory = qty_memory;
      else myqty_memory = qty_memory_input;
      localStorage['qty_memory'] = myqty_memory;
      /// end qty memory

      /// qty slot
      qty_cntrlr_input = $("#qty_cntrlr").val()
      var qty_cntrlr = localStorage['qty_cntrlr'];
      if (qty_cntrlr) myqty_cntrlr = qty_cntrlr;
      else myqty_cntrlr = qty_cntrlr_input;
      localStorage['qty_cntrlr'] = myqty_cntrlr;
      /// end qty slot

      /// qty slot 2
      qty_cntrlrtwo_input = $("#qty_cntrlrtwo").val()
      var qty_cntrlrtwo = localStorage['qty_cntrlrtwo'];
      if (qty_cntrlrtwo) myqty_cntrlrtwo = qty_cntrlrtwo;
      else myqty_cntrlrtwo = qty_cntrlrtwo_input;
      localStorage['qty_cntrlrtwo'] = myqty_cntrlrtwo;
      /// end qty slot 2

      /// qty slot 3
      qty_cntrlrthree_input = $("#qty_cntrlrthree").val()
      var qty_cntrlrthree = localStorage['qty_cntrlrthree'];
      if (qty_cntrlrthree) myqty_cntrlrthree = qty_cntrlrthree;
      else myqty_cntrlrthree = qty_cntrlrthree_input;
      localStorage['qty_cntrlrthree'] = myqty_cntrlrthree;
      /// end qty slot 3

      /// qty psu
      qty_psu_input = $("#qty_psu").val()
      var qty_psu = localStorage['qty_psu'];
      if (qty_psu) myqty_psu = qty_psu;
      else myqty_psu = qty_psu_input;
      localStorage['qty_psu'] = myqty_psu;
      /// end qty psu

      /// qty mounting
      qty_mounting_input = $("#qty_mounting").val()
      var qty_mounting = localStorage['qty_mounting'];
      if (qty_mounting) myqty_mounting = qty_mounting;
      else myqty_mounting = qty_mounting_input;
      localStorage['qty_mounting'] = myqty_mounting;
      /// end qty mounting


      /////////////// for motherboard only /////////////


      /// motherboard_mb
  $("#motherboard_mb").change(function(){
    motherboard_mb_change = $("#motherboard_mb").val();
    localStorage.removeItem('motherboard_mb')
    motherboard_mb_input = $("#motherboard_mb").val()
     var motherboard_mb = localStorage['motherboard_mb'];
     if (motherboard_mb) mymotherboard_mb = motherboard_mb;
     else mymotherboard_mb = motherboard_mb_input;
     localStorage['motherboard_mb'] = mymotherboard_mb;


     localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = 'modalthree';
        localStorage['modalshow'] = mymodalshow;
  })
  /// end motherboard_mb

  /// cpu_mb
$("#cpu_mb").change(function(){
    cpu_mb_change = $("#cpu_mb").val();
    localStorage.removeItem('cpu_mb')
    cpu_mb_input = $("#cpu_mb").val()
     var cpu_mb = localStorage['cpu_mb'];
     if (cpu_mb) mycpu_mb = cpu_mb;
     else mycpu_mb = cpu_mb_input;
     localStorage['cpu_mb'] = mycpu_mb;


     modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;
  })
  /// end cpu_mb

  /// cpu_mb
  $("#memory_mb").change(function(){
    memory_mb_change = $("#memory_mb").val();
    localStorage.removeItem('memory_mb')
    memory_mb_input = $("#memory_mb").val()
     var memory_mb = localStorage['memory_mb'];
     if (memory_mb) mymemory_mb = memory_mb;
     else mymemory_mb = memory_mb_input;
     localStorage['memory_mb'] = mymemory_mb;


     modalshow_input = 'modalthree'
        localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = modalshow_input;
        localStorage['modalshow'] = mymodalshow;  
    })
    /// end cpu_mb


    /// mounting_mb
  $("#mounting_mb").change(function(){
    mounting_mb_change = $("#mounting_mb").val();
    localStorage.removeItem('mounting_mb')
    mounting_mb_input = $("#mounting_mb").val()
     var mounting_mb = localStorage['mounting_mb'];
     if (mounting_mb) mymounting_mb = mounting_mb;
     else mymounting_mb = mounting_mb_input;
     localStorage['mounting_mb'] = mymounting_mb;
    })
    /// end mounting_mb


    /// warrant_mb
  $("#warranty_mb").change(function(){
    warranty_mb_change = $("#warranty_mb").val();
    localStorage.removeItem('warranty_mb')
    warranty_mb_input = $("#warranty_mb").val()
     var warranty_mb = localStorage['warranty_mb'];
     if (warranty_mb) mywarranty_mb = warranty_mb;
     else mywarranty_mb = warranty_mb_input;
     localStorage['warranty_mb'] = mywarranty_mb;
    })
    /// end warranty_mb
    
    
    /// qty cntrl_mb
      cntrl_mb_input = $("#cntrl_mb").val()
      var cntrl_mb = localStorage['cntrl_mb'];
      if (cntrl_mb) mycntrl_mb = cntrl_mb;
      else mycntrl_mb = cntrl_mb_input;
      localStorage['cntrl_mb'] = mycntrl_mb;
      /// end qty cntrl_mb


      /// qty cntrltwo_mb
      cntrltwo_mb_input = $("#cntrltwo_mb").val()
      var cntrltwo_mb = localStorage['cntrltwo_mb'];
      if (cntrltwo_mb) mycntrltwo_mb = cntrltwo_mb;
      else mycntrltwo_mb = cntrltwo_mb_input;
      localStorage['cntrltwo_mb'] = mycntrltwo_mb;
      /// end qty cntrltwo_mb


      /// qty cntrlthree_mb
      cntrlthree_mb_input = $("#cntrlthree_mb").val()
      var cntrlthree_mb = localStorage['cntrlthree_mb'];
      if (cntrlthree_mb) mycntrlthree_mb = cntrlthree_mb;
      else mycntrlthree_mb = cntrlthree_mb_input;
      localStorage['cntrlthree_mb'] = mycntrlthree_mb;
      /// end qty cntrlthree_mb


      /// qty psu_mb
      psu_mb_input = $("#psu_mb").val()
      var psu_mb = localStorage['psu_mb'];
      if (psu_mb) mypsu_mb = psu_mb;
      else mypsu_mb = psu_mb_input;
      localStorage['psu_mb'] = mypsu_mb;
      /// end qty psu_mb


      /// qty os_mb
      os_mb_input = $("#os_mb").val()
      var os_mb = localStorage['os_mb'];
      if (os_mb) myos_mb = os_mb;
      else myos_mb = os_mb_input;
      localStorage['os_mb'] = myos_mb;
      /// end qty os_mb


      /// qty qty_motherboard_mb
      qty_motherboard_mb_input = $("#qty_motherboard_mb").val()
      var qty_motherboard_mb = localStorage['qty_motherboard_mb'];
      if (qty_motherboard_mb) myqty_motherboard_mb = qty_motherboard_mb;
      else myqty_motherboard_mb = qty_motherboard_mb_input;
      localStorage['qty_motherboard_mb'] = myqty_motherboard_mb;
      /// end qty_motherboard_mb


      ///  qty_cpu_mb
      qty_cpu_mb_input = $("#qty_cpu_mb").val()
      var qty_cpu_mb = localStorage['qty_cpu_mb'];
      if (qty_cpu_mb) myqty_cpu_mb = qty_cpu_mb;
      else myqty_cpu_mb = qty_cpu_mb_input;
      localStorage['qty_cpu_mb'] = myqty_cpu_mb;
      /// end qty_motherboard_mb


      ///  qty_memory_mb
      qty_memory_mb_input = $("#qty_memory_mb").val()
      var qty_memory_mb = localStorage['qty_memory_mb'];
      if (qty_memory_mb) myqty_memory_mb = qty_memory_mb;
      else myqty_memory_mb = qty_memory_mb_input;
      localStorage['qty_memory_mb'] = myqty_memory_mb;
      /// end qty_memory_mb


      /// qty_cntrlr_mb
      qty_cntrlr_mb_input = $("#qty_cntrlr_mb").val()
      var qty_cntrlr_mb = localStorage['qty_cntrlr_mb'];
      if (qty_cntrlr_mb) myqty_cntrlr_mb = qty_cntrlr_mb;
      else myqty_cntrlr_mb = qty_cntrlr_mb_input;
      localStorage['qty_cntrlr_mb'] = myqty_cntrlr_mb;
      /// end qty_cntrlr_mb


      /// qty_cntrlrtwo_mb
      qty_cntrlrtwo_mb_input = $("#qty_cntrlrtwo_mb").val()
      var qty_cntrlrtwo_mb = localStorage['qty_cntrlrtwo_mb'];
      if (qty_cntrlrtwo_mb) myqty_cntrlrtwo_mb = qty_cntrlrtwo_mb;
      else myqty_cntrlrtwo_mb = qty_cntrlrtwo_mb_input;
      localStorage['qty_cntrlrtwo_mb'] = myqty_cntrlrtwo_mb;
      /// end qty_cntrlrtwo_mb


      /// qty_cntrlrthree_mb
      qty_cntrlrthree_mb_input = $("#qty_cntrlrthree_mb").val()
      var qty_cntrlrthree_mb = localStorage['qty_cntrlrthree_mb'];
      if (qty_cntrlrthree_mb) myqty_cntrlrthree_mb = qty_cntrlrthree_mb;
      else myqty_cntrlrthree_mb = qty_cntrlrthree_mb_input;
      localStorage['qty_cntrlrthree_mb'] = myqty_cntrlrthree_mb;
      /// end qty_cntrlrthree_mb


      /// qty_psu_mb
      qty_psu_mb_input = $("#qty_psu_mb").val()
      var qty_psu_mb = localStorage['qty_psu_mb'];
      if (qty_psu_mb) myqty_psu_mb = qty_psu_mb;
      else myqty_psu_mb = qty_psu_mb_input;
      localStorage['qty_psu_mb'] = myqty_psu_mb;
      /// end qty_psu_mb


      /// qty_mounting_mb
      qty_mounting_mb_input = $("#qty_mounting_mb").val()
      var qty_mounting_mb = localStorage['qty_mounting_mb'];
      if (qty_mounting_mb) myqty_mounting_mb = qty_mounting_mb;
      else myqty_mounting_mb = qty_mounting_mb_input;
      localStorage['qty_mounting_mb'] = myqty_mounting_mb;
      /// end qty_mounting_mb


      /////// for backplane drivebays rearbays nodes psuchassis bin ///////

      /// backplane
      backplane_input = $("#backplane").text()
      var backplane = localStorage['backplane'];
      if (backplane) mybackplane = backplane;
      else mybackplane = backplane_input;
      localStorage['backplane'] = mybackplane;
      /// end backplane


      /// drivebays
      drivebays_input = $("#drivebays").text()
      var drivebays = localStorage['drivebays'];
      if (drivebays) mydrivebays = drivebays;
      else mydrivebays = drivebays_input;
      localStorage['drivebays'] = mydrivebays;
      /// end backplane


      /// rearbays
      rearbays_input = $("#rearbays").text()
      var rearbays = localStorage['rearbays'];
      if (rearbays) myrearbays = rearbays;
      else myrearbays = rearbays_input;
      localStorage['rearbays'] = myrearbays;
      /// end backplane


      /// nodes
      nodes_input = $("#nodes").text()
      var nodes = localStorage['nodes'];
      if (nodes) mynodes = nodes;
      else mynodes = nodes_input;
      localStorage['nodes'] = mynodes;
      /// end backplane


      /// psuchassis
      psuchassis_input = $("#psuchassis").text()
      var psuchassis = localStorage['psuchassis'];
      if (psuchassis) mypsuchassis = psuchassis;
      else mypsuchassis = psuchassis_input;
      localStorage['psuchassis'] = mypsuchassis;
      /// end backplane


      /// bin
      bin_input = $("#bin").text()
      var bin = localStorage['bin'];
      if (bin) mybin = bin;
      else mybin = bin_input;
      localStorage['bin'] = mybin;
      /// end backplane


      /// details_all
    localStorage.removeItem('details_all')
    details_all_input = $("#details_all").val()
    var details_all = localStorage['details_all'];
    if (details_all) mydetails_all = details_all;
    else mydetails_all = details_all_input;
    localStorage['details_all'] = mydetails_all;
    /// end details_all

      
      

  });
  
  $("#txt_input").val(localStorage['barcode'])
  $("#client_name").val(localStorage['client_name'])
  $("#contact_no").val(localStorage['contact_no'])
  $("#email").val(localStorage['email'])
  $(".datetimepicker-input").val(localStorage['ship_date'])
  $("#ship_method").val(localStorage['ship_method'])
  $("#address").val(localStorage['address'])
  
  //////////////////////////////// for chassis //////////////////////////////////////

  $("#build_id").val(localStorage['build_id'])
  chassis_change = localStorage['chasis'];
  front_caddies_change = localStorage['front_caddies'];
  rear_caddies_change = localStorage['rear_caddies'];
  item_harddrive_one_change = localStorage['item_harddrive_one'];
  qty_item_harddrive_one_change = localStorage['qty_item_harddrive_one'];
  item_harddrive_two_change = localStorage['item_harddrive_two'];
  qty_item_harddrive_two_change = localStorage['qty_hard_drive_two'];
  item_harddrive_three_change = localStorage['item_harddrive_three'];
  qty_item_harddrive_three_change = localStorage['qty_hard_drive_three'];
  motherboard_change = localStorage['motherboard'];
  cpu_change = localStorage['cpu'];
  memory_change = localStorage['memory'];
  cntrl_change = localStorage['cntrl'];
  cntrltwo_change = localStorage['cntrltwo'];
  cntrlthree_change = localStorage['cntrlthree'];
  psu_change = localStorage['psu'];
  mounting_change = localStorage['mounting'];
  warranty_ch_change = localStorage['warranty_ch'];
  os_change = localStorage['os'];
  qty_motherboard_change = localStorage['qty_motherboard'];
  qty_cpu_change = localStorage['qty_cpu'];
  qty_memory_change = localStorage['qty_memory'];
  qty_cntrlr_change = localStorage['qty_cntrlr'];
  qty_psu_change = localStorage['qty_psu'];
  qty_mounting_change = localStorage['qty_mounting'];
  qty_cntrlrthree_change = localStorage['qty_cntrlrthree'];
  qty_cntrlrtwo_change = localStorage['qty_cntrlrtwo'];
  qty_front_caddies_change = localStorage['qty_front_caddies'];
  qty_rear_caddies_change = localStorage['qty_rear_caddies'];

  
  if(motherboard_change != "" && motherboard_change != "null"){
      var $motherboard_change_insert = $("<option selected='selected'></option>").val(motherboard_change).text(motherboard_change)
      $("#motherboard").append($motherboard_change_insert).trigger('change');
  }else{
      var $motherboard_change_insert = $("<option selected='selected'></option>").val("").text("")
      $("#motherboard").append($motherboard_change_insert).trigger('change');
  }
    
  if(cpu_change != "" && cpu_change != "null"){
      var $cpu_change_insert = $("<option selected='selected'></option>").val(cpu_change).text(cpu_change)
      $("#cpu").append($cpu_change_insert).trigger('change');
  }else{
      var $cpu_change_insert = $("<option selected='selected'></option>").val("").text("")
      $("#cpu").append($cpu_change_insert).trigger('change');
  }

  if(memory_change != "" && memory_change != "null"){
      var $memory_change_insert = $("<option selected='selected'></option>").val(memory_change).text(memory_change)
      $("#memory").append($memory_change_insert).trigger('change');
  }else{
      var $memory_change_insert = $("<option selected='selected'></option>").val("").text("")
      $("#memory").append($memory_change_insert).trigger('change');
  }


  ////////////////////////////// for motherboard only //////////////////////////////////////////
  motherboard_mb_change = localStorage['motherboard_mb'];
  cpu_mb_change = localStorage['cpu_mb'];
  memory_mb_change = localStorage['memory_mb'];
  cntrl_mb_change = localStorage['cntrl_mb'];
  cntrltwo_mb_change = localStorage['cntrltwo_mb'];
  cntrlthree_mb_change = localStorage['cntrlthree_mb'];
  psu_mb_change = localStorage['psu_mb'];
  mounting_mb_change = localStorage['mounting_mb'];
  warranty_mb_change = localStorage['warranty_mb'];
  os_mb_change = localStorage['os_mb'];


  qty_motherboard_mb_change = localStorage['qty_motherboard_mb'];
  qty_cpu_mb_change = localStorage['qty_cpu_mb'];
  qty_memory_mb_change = localStorage['qty_memory_mb'];
  qty_cntrlr_mb_change = localStorage['qty_cntrlr_mb'];
  qty_cntrlrtwo_mb_change = localStorage['qty_cntrlrtwo_mb'];
  qty_cntrlrthree_mb_change = localStorage['qty_cntrlrthree_mb'];
  qty_psu_mb_change = localStorage['qty_psu_mb'];
  qty_mounting_mb_change = localStorage['qty_mounting_mb'];
  build_id_mb_change = localStorage['build_id_mb'];
  
  
  details_all_change = localStorage['details_all'];


  
  /// backplane
  /*backplane_input = $("#backplane").text() ///////// use this if error has occur
  var backplane = localStorage['backplane'];
  if (backplane) mybackplane = backplane;
  else mybackplane = backplane_input;
  localStorage['backplane'] = mybackplane;*/
  /// end backplane

  /////// for backplane drivebays rearbays nodes psuchassis bin ///////

  backplane_change = localStorage['backplane'];
  drivebays_change = localStorage['drivebays'];
  rearbays_change = localStorage['rearbays'];
  nodes_change = localStorage['nodes'];
  psuchassis_change = localStorage['psuchassis'];
  bin_change = localStorage['bin'];

  if(cpu_mb_change != "" && cpu_mb_change != "null"){
      var $cpu_mb_change_insert = $("<option selected='selected'></option>").val(cpu_mb_change).text(cpu_mb_change)
      $("#cpu_mb").append($cpu_mb_change_insert).trigger('change');
  }else{
      var $cpu_mb_change_insert = $("<option selected='selected'></option>").val("").text("")
      $("#cpu_mb").append($cpu_mb_change_insert).trigger('change');
  }

  if(memory_mb_change != "" && memory_mb_change != "null"){
      var $memory_mb_change_insert = $("<option selected='selected'></option>").val(memory_mb_change).text(memory_mb_change)
      $("#memory_mb").append($memory_mb_change_insert).trigger('change');
  }else{
      var $memory_mb_change_insert = $("<option selected='selected'></option>").val("").text("")
      $("#memory_mb").append($memory_mb_change_insert).trigger('change');
  }

  
  setInterval(function(){ 
       /// ship date on background
      localStorage.removeItem('ship_date')
      ship_date_input = $(".datetimepicker-input").val()
      var ship_date = localStorage['ship_date'];
      if (ship_date) myship_date = ship_date;
      else myship_date = ship_date_input;
      localStorage['ship_date'] = myship_date;
      /// end ship date on background

      /// chasis
      localStorage.removeItem('chasis')
       chasis_input = $("#chasis").val()
       var chasis = localStorage['chasis'];
       if (chasis) mychasis = chasis;
       else mychasis = chasis_input;
       localStorage['chasis'] = mychasis;
    /// end chasis
    if(chassis_change != ""){
      $(`#chasis option:contains(${chassis_change})`).prop('selected',true);
    }
    $("#qty_chassis").val(localStorage['qty_chassis'])
    if(front_caddies_change != ""){$(`#front_caddies option:contains(${front_caddies_change})`).prop('selected',true);}

    if(qty_front_caddies_change != ""){$("#qty_front_caddies").val(localStorage['qty_front_caddies'])}
    qty_front_caddies_input = $("#qty_front_caddies").val();
    localStorage['qty_front_caddies'] = qty_front_caddies_input;
    
    if(qty_rear_caddies_change != ""){$("#qty_rear_caddies").val(localStorage['qty_rear_caddies'])}
    qty_rear_caddies_input = $("#qty_rear_caddies").val();
    localStorage['qty_rear_caddies'] = qty_rear_caddies_input;

    if(rear_caddies_change != ""){$(`#rear_caddies option:contains(${rear_caddies_change})`).prop('selected',true);}
    
    front_caddies_input = $("#front_caddies").val();
    localStorage['front_caddies'] = front_caddies_input;

    rear_caddies_input = $("#rear_caddies").val();
    localStorage['rear_caddies'] = rear_caddies_input;

    chassis_input = $("#chasis").val();
    localStorage['chasis'] = chasis_input;

    if(item_harddrive_one_change != ""){$(`#item_harddrive_one option:contains(${item_harddrive_one_change})`).prop('selected',true);}
    item_harddrive_one_input = $("#item_harddrive_one").val();
    localStorage['item_harddrive_one'] = item_harddrive_one_input;

    if(qty_item_harddrive_one_change != ""){$("#qty_hard_drive_one").val(localStorage['qty_hard_drive_one'])}
    qty_hard_drive_one_input = $("#qty_hard_drive_one").val();
    localStorage['qty_hard_drive_one'] = qty_hard_drive_one_input;


    if(item_harddrive_two_change != ""){$(`#item_harddrive_two option:contains(${item_harddrive_two_change})`).prop('selected',true);}
    item_harddrive_two_input = $("#item_harddrive_two").val();
    localStorage['item_harddrive_two'] = item_harddrive_two_input;

    if(qty_item_harddrive_two_change != ""){$("#qty_hard_drive_two").val(localStorage['qty_hard_drive_two'])}
    qty_hard_drive_two_input = $("#qty_hard_drive_two").val();
    localStorage['qty_hard_drive_two'] = qty_hard_drive_two_input;

    if(item_harddrive_three_change != ""){$(`#item_harddrive_three option:contains(${item_harddrive_three_change})`).prop('selected',true);}
    item_harddrive_three_input = $("#item_harddrive_three").val();
    localStorage['item_harddrive_three'] = item_harddrive_three_input;

    motherboard_input = $("#motherboard").val();
    localStorage['motherboard'] = motherboard_input;
    if($("#motherboard").val() != ""){$("#motherboard").val(motherboard_input);}

    cpu_input = $("#cpu").val();
    localStorage['cpu'] = cpu_input;
    if($("#cpu").val() != ""){$("#cpu").val(cpu_input);}

    memory_input = $("#memory").val();
    localStorage['memory'] = memory_input;
    if($("#memory").val() != ""){$("#memory").val(memory_input);}

    if(cntrl_change != ""){$("#cntrl").val(localStorage['cntrl'])}
    cntrl_input = $("#cntrl").val();
    localStorage['cntrl'] = cntrl_input;

    if(cntrltwo_change != ""){$("#cntrltwo").val(localStorage['cntrltwo'])}
    cntrltwo_input = $("#cntrltwo").val();
    localStorage['cntrltwo'] = cntrltwo_input;

    if(cntrlthree_change != ""){$("#cntrlthree").val(localStorage['cntrlthree'])}
    cntrlthree_input = $("#cntrlthree").val();
    localStorage['cntrlthree'] = cntrlthree_input;

    if(psu_change != ""){$("#psu").val(localStorage['psu'])}
    psu_input = $("#psu").val();
    localStorage['psu'] = psu_input;

    if(mounting_change != ""){$(`#mounting option:contains(${mounting_change})`).prop('selected',true);}
    mounting_input = $("#mounting").val();
    localStorage['mounting'] = mounting_input;

    if(warranty_ch_change != ""){$(`#warranty_ch option:contains(${warranty_ch_change})`).prop('selected',true);}
    warranty_ch_input = $("#warranty_ch").val();
    localStorage['warranty_ch'] = warranty_ch_input;

    if(os_change != ""){$("#os").val(localStorage['os'])}
    os_input = $("#os").val();
    localStorage['os'] = os_input;
    
    if(qty_motherboard_change != ""){$("#qty_motherboard").val(localStorage['qty_motherboard'])}
    qty_motherboard_input = $("#qty_motherboard").val();
    localStorage['qty_motherboard'] = qty_motherboard_input;

    if(qty_cpu_change != ""){$("#qty_cpu").val(localStorage['qty_cpu'])}
    qty_cpu_input = $("#qty_cpu").val();
    localStorage['qty_cpu'] = qty_cpu_input;

    if(qty_memory_change != ""){$("#qty_memory").val(localStorage['qty_memory'])}
    qty_memory_input = $("#qty_memory").val();
    localStorage['qty_memory'] = qty_memory_input;

    if(qty_cntrlr_change != ""){$("#qty_cntrlr").val(localStorage['qty_cntrlr'])}
    qty_cntrlr_input = $("#qty_cntrlr").val();
    localStorage['qty_cntrlr'] = qty_cntrlr_input;

    if(qty_cntrlrtwo_change != ""){$("#qty_cntrlrtwo").val(localStorage['qty_cntrlrtwo'])}
    qty_cntrlrtwo_input = $("#qty_cntrlrtwo").val();
    localStorage['qty_cntrlrtwo'] = qty_cntrlrtwo_input;

    if(qty_cntrlrthree_change != ""){$("#qty_cntrlrthree").val(localStorage['qty_cntrlrthree'])}
    qty_cntrlrthree_input = $("#qty_cntrlrthree").val();
    localStorage['qty_cntrlrthree'] = qty_cntrlrthree_input;

    if(qty_psu_change != ""){$("#qty_psu").val(localStorage['qty_psu'])}
    qty_psu_input = $("#qty_psu").val();
    localStorage['qty_psu'] = qty_psu_input;

    if(qty_mounting_change != ""){$("#qty_mounting").val(localStorage['qty_mounting'])}
    qty_mounting_input = $("#qty_mounting").val();
    localStorage['qty_mounting'] = qty_mounting_input;

    if(qty_item_harddrive_three_change != ""){$("#qty_hard_drive_three").val(localStorage['qty_hard_drive_three'])}
    qty_hard_drive_three_input = $("#qty_hard_drive_three").val();
    localStorage['qty_hard_drive_three'] = qty_hard_drive_three_input;



    if(motherboard_change != ""){$(`#motherboard option:contains(${motherboard_change})`).prop('selected',true);}
    motherboard_input = $("#motherboard").val();
    localStorage['motherboard'] = motherboard_input;


    if(cpu_change != ""){$(`#cpu option:contains(${cpu_change})`).prop('selected',true);}
    cpu_input = $("#cpu").val();
    localStorage['cpu'] = cpu_input;


    if(memory_change != ""){$(`#memory option:contains(${memory_change})`).prop('selected',true);}
    memory_input = $("#memory").val();
    localStorage['memory'] = memory_input;

    //////////////////////////////////////// for motherboard only ////////////////////////////////////////////

    if(motherboard_mb_change != ""){$(`#motherboard_mb option:contains(${motherboard_mb_change})`).prop('selected',true);}
    motherboard_mb_input = $("#motherboard_mb").val();
    localStorage['motherboard_mb'] = motherboard_mb_input;


    if(cpu_mb_change != ""){$(`#cpu_mb option:contains(${cpu_mb_change})`).prop('selected',true);}
    cpu_mb_input = $("#cpu_mb").val();
    localStorage['cpu_mb'] = cpu_mb_input;


    if(memory_mb_change != ""){$(`#memory_mb option:contains(${memory_mb_change})`).prop('selected',true);}
    memory_mb_input = $("#memory_mb").val();
    localStorage['memory_mb'] = memory_mb_input;
    
    
    if(cntrl_mb_change != ""){$("#cntrl_mb").val(localStorage['cntrl_mb'])}
    cntrl_mb_input = $("#cntrl_mb").val();
    localStorage['cntrl_mb'] = cntrl_mb_input;
    
    
    if(cntrltwo_mb_change != ""){$("#cntrltwo_mb").val(localStorage['cntrltwo_mb'])}
    cntrltwo_mb_input = $("#cntrltwo_mb").val();
    localStorage['cntrltwo_mb'] = cntrltwo_mb_input;
    
    
    if(cntrlthree_mb_change != ""){$("#cntrlthree_mb").val(localStorage['cntrlthree_mb'])}
    cntrlthree_mb_input = $("#cntrlthree_mb").val();
    localStorage['cntrlthree_mb'] = cntrlthree_mb_input;
    
    
    if(psu_mb_change != ""){$("#psu_mb").val(localStorage['psu_mb'])}
    psu_mb_input = $("#psu_mb").val();
    localStorage['psu_mb'] = psu_mb_input;


    if(mounting_mb_change != ""){$(`#mounting_mb option:contains(${mounting_mb_change})`).prop('selected',true);}
    mounting_mb_input = $("#mounting_mb").val();
    localStorage['mounting_mb'] = mounting_mb_input;


    if(warranty_mb_change != ""){$(`#warranty_mb option:contains(${warranty_mb_change})`).prop('selected',true);}
    warranty_mb_input = $("#warranty_mb").val();
    localStorage['warranty_mb'] = warranty_mb_input;


    if(os_mb_change != ""){$("#os_mb").val(localStorage['os_mb'])}
    os_mb_input = $("#os_mb").val();
    localStorage['os_mb'] = os_mb_input;details_all


    /////////////////////////////////////////////////////////////////////////////
    if(qty_motherboard_mb_change != ""){$("#qty_motherboard_mb").val(localStorage['qty_motherboard_mb'])}
    qty_motherboard_mb_input = $("#qty_motherboard_mb").val();
    localStorage['qty_motherboard_mb'] = qty_motherboard_mb_input;


    if(qty_cpu_mb_change != ""){$("#qty_cpu_mb").val(localStorage['qty_cpu_mb'])}
    qty_cpu_mb_input = $("#qty_cpu_mb").val();
    localStorage['qty_cpu_mb'] = qty_cpu_mb_input;


    if(qty_memory_mb_change != ""){$("#qty_memory_mb").val(localStorage['qty_memory_mb'])}
    qty_memory_mb_input = $("#qty_memory_mb").val();
    localStorage['qty_memory_mb'] = qty_memory_mb_input;


    if(qty_cntrlr_mb_change != ""){$("#qty_cntrlr_mb").val(localStorage['qty_cntrlr_mb'])}
    qty_cntrlr_mb_input = $("#qty_cntrlr_mb").val();
    localStorage['qty_cntrlr_mb'] = qty_cntrlr_mb_input;


    if(qty_cntrlrtwo_mb_change != ""){$("#qty_cntrlrtwo_mb").val(localStorage['qty_cntrlrtwo_mb'])}
    qty_cntrlrtwo_mb_input = $("#qty_cntrlrtwo_mb").val();
    localStorage['qty_cntrlrtwo_mb'] = qty_cntrlrtwo_mb_input;


    if(qty_cntrlrthree_mb_change != ""){$("#qty_cntrlrthree_mb").val(localStorage['qty_cntrlrthree_mb'])}
    qty_cntrlrthree_mb_input = $("#qty_cntrlrthree_mb").val();
    localStorage['qty_cntrlrthree_mb'] = qty_cntrlrthree_mb_input;


    if(qty_psu_mb_change != ""){$("#qty_psu_mb").val(localStorage['qty_psu_mb'])}
    qty_psu_mb_input = $("#qty_psu_mb").val();
    localStorage['qty_psu_mb'] = qty_psu_mb_input;


    if(qty_mounting_mb_change != ""){$("#qty_mounting_mb").val(localStorage['qty_mounting_mb'])}
    qty_mounting_mb_input = $("#qty_mounting_mb").val();
    localStorage['qty_mounting_mb'] = qty_mounting_mb_input;


    if(build_id_mb_change != ""){$(`#build_id_mb option:contains(${build_id_mb_change})`).prop('selected',true);}
    build_id_mb_input = $("#build_id_mb").val();
    localStorage['build_id_mb'] = build_id_mb_input;


    ////////////////////////////////////////////////////////////////////////////////

    /////// for backplane drivebays rearbays nodes psuchassis bin ///////

    if(backplane_change != ""){$("#backplane").text(localStorage['backplane'])}
    backplane_input = $("#backplane").text();
    localStorage['backplane'] = backplane_input;


    if(rearbays_change != ""){$("#rearbays").text(localStorage['rearbays'])}
    rearbays_input = $("#rearbays").text();
    localStorage['rearbays'] = rearbays_input;


    if(nodes_change != ""){$("#nodes").text(localStorage['nodes'])}
    nodes_input = $("#nodes").text();
    localStorage['nodes'] = nodes_input;


    if(drivebays_change != ""){$("#drivebays").text(localStorage['drivebays'])}
    drivebays_input = $("#drivebays").text();
    localStorage['drivebays'] = drivebays_input;


    if(psuchassis_change != ""){$("#psuchassis").text(localStorage['psuchassis'])}
    psuchassis_input = $("#psuchassis").text();
    localStorage['psuchassis'] = psuchassis_input;


    if(bin_change != ""){$("#bin").text(localStorage['bin'])}
    bin_input = $("#bin").text();
    localStorage['bin'] = bin_input;


    if(details_all_change != ""){$("#details_all").val(localStorage['details_all'])}
    details_all_input = $("#details_all").val();
    localStorage['details_all'] = details_all_input;


    
   }, 1000);

   $('#os').keyup(function(){
      /// os
      localStorage.removeItem('os')
      os_input = $("#os").val()
      var os = localStorage['os'];
      if (os) myos = os;
      else myos = os_input;
      localStorage['os'] = myos;
      /// end os
  });


  $('#details_all').keyup(function(){
    /// details_all
    localStorage.removeItem('details_all')
    details_all_input = $("#details_all").val()
    var details_all = localStorage['details_all'];
    if (details_all) mydetails_all = details_all;
    else mydetails_all = details_all_input;
    localStorage['details_all'] = mydetails_all;
    /// end details_all
});


   $("#ship_method").change(function(){
       /// ship method on change
      localStorage.removeItem('ship_method')
      ship_method_input = $("#ship_method").val()
      var ship_method = localStorage['ship_method'];
      if (ship_method) myship_method = ship_method;
      else myship_method = ship_method_input;
      localStorage['ship_method'] = myship_method;
      /// end ship method on change
   })

   $('#address').keyup(function(){
      /// address
      localStorage.removeItem('address')
      address_input = $("#address").val()
      var address = localStorage['address'];
      if (address) myaddress = address;
      else myaddress = address_input;
      localStorage['address'] = myaddress;
      /// end address
  });

  /// build id
  $("#build_id").change(function(){
      chassis_change = "";
      item_harddrive_one_change = "";
      item_harddrive_two_change = "";
      item_harddrive_three_change = "";
      mounting_change = "";
      warranty_ch_change = "";


      motherboard_change = "";
      localStorage.removeItem('motherboard')
      
      
      cpu_change = "";
      localStorage.removeItem('cpu')
      
      
      memory_change = "";
      localStorage.removeItem('memory')
      
      localStorage.removeItem('qty_chassis')
      
      localStorage.removeItem('item_harddrive_one')
      $("#qty_hard_drive_one").val("")
      localStorage.removeItem('qty_hard_drive_one')
      qty_item_harddrive_one_change = $("#qty_hard_drive_one").val();

      localStorage.removeItem('item_harddrive_two')
      $("#qty_hard_drive_two").val("")
      localStorage.removeItem('qty_hard_drive_two')
      qty_item_harddrive_two_change = $("#qty_hard_drive_two").val();

      localStorage.removeItem('item_harddrive_three')
      $("#qty_hard_drive_three").val("")
      localStorage.removeItem('qty_hard_drive_three')
      qty_item_harddrive_three_change = $("#qty_hard_drive_three").val();

      $("#cntrl").val("")
      localStorage.removeItem('cntrl')
      cntrl_change = $("#cntrl").val();

      $("#cntrltwo").val("")
      localStorage.removeItem('cntrltwo');
      cntrltwo_change = $("#cntrltwo").val();

      $("#cntrlthree").val("")
      localStorage.removeItem('cntrlthree');
      cntrlthree_change = $("#cntrlthree").val();

      $("#psu").val("")
      localStorage.removeItem('psu');
      psu_change = $("#psu").val();

      localStorage.removeItem('mounting')
      
      localStorage.removeItem('warranty_ch')

      $("#qty_motherboard").val("")
      localStorage.removeItem('qty_motherboard')
      qty_motherboard_change = $("#qty_motherboard").val();

      $("#qty_cpu").val("")
      localStorage.removeItem('qty_cpu')
      qty_cpu_change = $("#qty_cpu").val();

      $("#qty_memory").val("")
      localStorage.removeItem('qty_memory')
      qty_memory_change = $("#qty_memory").val();

      $("#qty_cntrlr").val("")
      localStorage.removeItem('qty_cntrlr')
      qty_cntrlr_change = $("#qty_cntrlr").val();

      $("#qty_cntrlrtwo").val("")
      localStorage.removeItem('qty_cntrlrtwo')
      qty_cntrlrtwo_change = $("#qty_cntrlrtwo").val();

      $("#qty_cntrlrthree").val("")
      localStorage.removeItem('qty_cntrlrthree')
      qty_cntrlrthree_change = $("#qty_cntrlrthree").val();

      $("#qty_psu").val("")
      localStorage.removeItem('qty_psu')
      qty_psu_change = $("#qty_psu").val();

      $("#qty_mounting").val("")
      localStorage.removeItem('qty_mounting')
      qty_mounting_change = $("#qty_mounting").val();

      $("#qty_front_caddies").val("")
      localStorage.removeItem('qty_front_caddies')
      qty_front_caddies_change = $("#qty_front_caddies").val();


      $("#qty_rear_caddies").val("")
      localStorage.removeItem('qty_rear_caddies')
      qty_mounting_change = $("#qty_rear_caddies").val();


      /////////////////////////////////////////////////

      /////// for backplane drivebays rearbays nodes psuchassis bin ///////

      backplane_change = $("#backplane").text();
      localStorage.removeItem('backplane')


      drivebays_change = $("#drivebays").text();
      localStorage.removeItem('drivebays')


      rearbays_change = $("#rearbays").text();
      localStorage.removeItem('rearbays')


      nodes_change = $("#nodes").text();
      localStorage.removeItem('nodes')


      psuchassis_change = $("#psuchassis").text();
      localStorage.removeItem('psuchassis')


      bin_change = $("#bin").text();
      localStorage.removeItem('bin')

      /////// for backplane drivebays rearbays nodes psuchassis bin ///////
      
      ////////////////////////////////////////////////////
    
       localStorage.removeItem('build_id')
       build_id_input = $("#build_id").val()

       var build_id = localStorage['build_id'];
       if (build_id) mybuild_id = build_id;
       
       else mybuild_id = build_id_input;
       localStorage['build_id'] = mybuild_id;
       
    })
    /// end build id

    /// front caddies
  $("#front_caddies").change(function(){
      front_caddies_change = $("#front_caddies").val();
      localStorage.removeItem('front_caddies')
      front_caddies_input = $("#front_caddies").val()
      var front_caddies = localStorage['front_caddies'];
      if (front_caddies) myfront_caddies = front_caddies;
      else myfront_caddies = front_caddies_input;
      localStorage['front_caddies'] = myfront_caddies;
    })
    /// end front caddies

    /// chassis change
  $("#chasis").change(function(){
      chassis_change = $("#chasis").val();
      localStorage.removeItem('qty_front_caddies')
      item_harddrive_two_input = $("#item_harddrive_two").val();
      localStorage['item_harddrive_two'] = item_harddrive_two_input;
      front_caddies_input = $("#front_caddies").val();
      rear_caddies_input = $("#rear_caddies").val();
      localStorage['front_caddies'] = front_caddies_input;
      localStorage['rear_caddies'] = rear_caddies_input;


      backplane_change = '';
      backplane_input = $("#backplane").text()
      localStorage.removeItem('backplane')
      console.log(backplane_input)


     

      drivebays_change = '';
      drivebays_input = $("#drivebays").text();
      localStorage.removeItem('drivebays')


      rearbays_change = '';
      rearbays_input = $("#rearbays").text();
      localStorage.removeItem('rearbays')


      nodes_change = '';
      nodes_input = $("#nodes").text()
      localStorage.removeItem('nodes')


      psuchassis_change = '';
      psuchassis_input = $("#psuchassis").text()
      localStorage.removeItem('psuchassis')


      bin_change = '';
      bin_input = $("#bin").text()
      localStorage.removeItem('bin')


      memory_change = "";
      localStorage.removeItem('memory')


      cpu_change = "";
      localStorage.removeItem('cpu')


      motherboard_change = "";
      localStorage.removeItem('motherboard')
    })
    /// end chassis change

    /// rear caddies
  $("#rear_caddies").change(function(){
      rear_caddies_change = $("#rear_caddies").val();
      localStorage.removeItem('rear_caddies')
      rear_caddies_input = $("#rear_caddies").val()
       var rear_caddies = localStorage['rear_caddies'];
       if (rear_caddies) myrear_caddies = rear_caddies;
       else myrear_caddies = rear_caddies_input;
       localStorage['rear_caddies'] = myrear_caddies;
    })
    /// end rear caddies

    /// item harddrive one
  $("#item_harddrive_one").change(function(){
      item_harddrive_one_change = $("#item_harddrive_one").val();
      localStorage.removeItem('item_harddrive_one')
      item_harddrive_one_input = $("#item_harddrive_one").val()
       var item_harddrive_one = localStorage['item_harddrive_one'];
       if (item_harddrive_one) myitem_harddrive_one = item_harddrive_one;
       else myitem_harddrive_one = item_harddrive_one_input;
       localStorage['item_harddrive_one'] = myitem_harddrive_one;
    })
    /// end item harddrive one

    /// item harddrive two
  $("#item_harddrive_two").change(function(){
      item_harddrive_two_change = $("#item_harddrive_two").val();
      localStorage.removeItem('item_harddrive_two')
      item_harddrive_two_input = $("#item_harddrive_two").val()
       var item_harddrive_two = localStorage['item_harddrive_two'];
       if (item_harddrive_two) myitem_harddrive_two = item_harddrive_two;
       else myitem_harddrive_two = item_harddrive_two_input;
       localStorage['item_harddrive_two'] = myitem_harddrive_two;
    })
    /// end item harddrive two

    /// item harddrive three
  $("#item_harddrive_three").change(function(){
      item_harddrive_three_change = $("#item_harddrive_three").val();
      localStorage.removeItem('item_harddrive_three')
      item_harddrive_three_input = $("#item_harddrive_three").val()
       var item_harddrive_three = localStorage['item_harddrive_three'];
       if (item_harddrive_three) myitem_harddrive_three = item_harddrive_three;
       else myitem_harddrive_three = item_harddrive_three_input;
       localStorage['item_harddrive_three'] = myitem_harddrive_three;
    })
    /// end item harddrive three

    /// mounting
  $("#mounting").change(function(){
      mounting_change = $("#mounting").val();
      localStorage.removeItem('mounting')
      mounting_input = $("#mounting").val()
       var mounting = localStorage['mounting'];
       if (mounting) mymounting = mounting;
       else mymounting = mounting_input;
       localStorage['mounting'] = mymounting;
    })
    /// end mounting

    /// warranty
  $("#warranty_ch").change(function(){
      warranty_ch_change = $("#warranty_ch").val();
      localStorage.removeItem('warranty_ch')
      warranty_ch_input = $("#warranty_ch").val()
       var warranty_ch = localStorage['warranty_ch'];
       if (warranty_ch) mywarranty_ch = warranty_ch;
       else mywarranty_ch = warranty_ch_input;
       localStorage['warranty_ch'] = mywarranty_ch;
    })
    /// end warranty


    /// motherboard_mb
  $("#motherboard_mb").change(function(){
      motherboard_mb_change = $("#motherboard_mb").val();
      localStorage.removeItem('motherboard_mb')
      motherboard_mb_input = $("#motherboard_mb").val()
       var motherboard_mb = localStorage['motherboard_mb'];
       if (motherboard_mb) mymotherboard_mb = motherboard_mb;
       else mymotherboard_mb = motherboard_mb_input;
       localStorage['motherboard_mb'] = mymotherboard_mb;


       localStorage.removeItem('modalshow')
        var modal = localStorage['modalshow'];
        if (modal) mymodalshow = modal;
        else mymodalshow = 'modalthree';
        localStorage['modalshow'] = mymodalshow;
    })
    /// end motherboard_mb

    /// cpu_mb
  /*$("#cpu_mb").change(function(){
      cpu_mb_change = $("#cpu_mb").val();
      localStorage.removeItem('cpu_mb')
      cpu_mb_input = $("#cpu_mb").val()
       var cpu_mb = localStorage['cpu_mb'];
       if (cpu_mb) mycpu_mb = cpu_mb;
       else mycpu_mb = cpu_mb_input;
       localStorage['cpu_mb'] = mycpu_mb;
    })*/
    /// end cpu_mb

    /// cpu_mb
    /*$("#memory_mb").change(function(){
      memory_mb_change = $("#memory_mb").val();
      localStorage.removeItem('memory_mb')
      memory_mb_input = $("#memory_mb").val()
       var memory_mb = localStorage['memory_mb'];
       if (memory_mb) mymemory_mb = memory_mb;
       else mymemory_mb = memory_mb_input;
       localStorage['memory_mb'] = mymemory_mb;
      })*/
      /// end cpu_mb


      /// mounting_mb
    $("#mounting_mb").change(function(){
      mounting_mb_change = $("#mounting_mb").val();
      localStorage.removeItem('mounting_mb')
      mounting_mb_input = $("#mounting_mb").val()
       var mounting_mb = localStorage['mounting_mb'];
       if (mounting_mb) mymounting_mb = mounting_mb;
       else mymounting_mb = mounting_mb_input;
       localStorage['mounting_mb'] = mymounting_mb;
      })
      /// end mounting_mb


      /// warrant_mb
    $("#warranty_mb").change(function(){
      warranty_mb_change = $("#warranty_mb").val();
      localStorage.removeItem('warranty_mb')
      warranty_mb_input = $("#warranty_mb").val()
       var warranty_mb = localStorage['warranty_mb'];
       if (warranty_mb) mywarranty_mb = warranty_mb;
       else mywarranty_mb = warranty_mb_input;
       localStorage['warranty_mb'] = mywarranty_mb;
      })
      /// end warranty_mb

      /// os_mb
      $('#os_mb').keyup(function(){
        /// os
        localStorage.removeItem('os_mb')
        os_mb_input = $("#os_mb").val()
        var os_mb = localStorage['os_mb'];
        if (os_mb) myos_mb = os_mb;
        else myos_mb = os_mb_input;
        localStorage['os_mb'] = myos_mb;
        /// end os
    });

    /// build id mb
    $("#build_id_mb").change(function(){
      
      motherboard_mb_input = "";
      motherboard_mb_change = "";
      localStorage.removeItem('motherboard_mb')
      cpu_mb_change = "";
      localStorage.removeItem('cpu_mb')
      
      
      memory_mb_change = "";
      localStorage.removeItem('memory_mb')
      
      
      cpu_mb_change = "";
      localStorage.removeItem('cpu_mb')


      mounting_mb_change = "";
      localStorage.removeItem('mounting_mb')


      warranty_mb_change = "";
      localStorage.removeItem('warranty_mb')


      build_id_mb_change = "";
      localStorage.removeItem('build_id_mb')

      $("#cntrl_mb").val("")
      localStorage.removeItem('cntrl_mb')
      cntrl_mb_change = $("#cntrl_mb").val();
      
      $("#cntrltwo_mb").val("")
      localStorage.removeItem('cntrltwo_mb')
      cntrltwo_mb_change = $("#cntrltwo_mb").val();
      
      $("#cntrlthree_mb").val("")
      localStorage.removeItem('cntrlthree_mb')
      cntrlthree_mb_change = $("#cntrlthree_mb").val();
      
      $("#psu_mb").val("")
      localStorage.removeItem('psu_mb')
      psu_mb_change = $("#psu_mb").val();


      localStorage.removeItem('os_mb')
      os_mb_change = $("#os_mb").val();


      ///////////////////////////////////////////////
      $("#qty_motherboard_mb").val("")
      localStorage.removeItem('qty_motherboard_mb')
      qty_motherboard_mb_change = $("#qty_motherboard_mb").val();


      $("#qty_cpu_mb").val("")
      localStorage.removeItem('qty_cpu_mb')
      qty_cpu_mb_change = $("#qty_cpu_mb").val();

      $("#qty_memory_mb").val("")
      localStorage.removeItem('qty_memory_mb')
      qty_memory_mb_change = $("#qty_memory_mb").val();


      $("#qty_cntrlr_mb").val("")
      localStorage.removeItem('qty_cntrlr_mb')
      qty_cntrlr_mb_change = $("#qty_cntrlr_mb").val();


      $("#qty_cntrlrtwo_mb").val("")
      localStorage.removeItem('qty_cntrlrtwo_mb')
      qty_cntrlrtwo_mb_change = $("#qty_cntrlrtwo_mb").val();


      $("#qty_cntrlrthree_mb").val("")
      localStorage.removeItem('qty_cntrlrthree_mb')
      qty_cntrlrthree_mb_change = $("#qty_cntrlrthree_mb").val();


      $("#qty_psu_mb").val("")
      localStorage.removeItem('qty_psu_mb')
      qty_psu_mb_change = $("#qty_psu_mb").val();


      $("#qty_mounting_mb").val("")
      localStorage.removeItem('qty_mounting_mb')
      qty_mounting_mb_change = $("#qty_mounting_mb").val();
      

      ///localStorage.removeItem('qty_front_caddies_mb')
      ///qty_front_caddies_change = $("#qty_front_caddies_mb").val();
    
       localStorage.removeItem('build_id_mb')
       build_id_mb_input = $("#build_id_mb").val()

       var build_id_mb = localStorage['build_id_mb'];
       if (build_id_mb) mybuild_id_mb = build_id_mb;
       
       else mybuild_id_mb = build_id_mb_input;
       localStorage['build_id_mb'] = mybuild_id_mb;
       
    })
    /// end build id mb


    //////////////// use this if error show in otherboard cpu and memory /////////////
    /// memory
    /*$("#memory").change(function(){
      $("#memory").val("")
      localStorage.removeItem('memory')
      memory_change = $("#memory").val();
      localStorage.removeItem('memory')
      memory_input = $("#memory").val()
       var memory = localStorage['memory'];
       if (memory) mymemory = memory;
       else mymemory = memory_input;
       localStorage['memory'] = mymemory;
      })
      /// end memory


      /// memory
    $("#memory").change(function(){
      $("#memory").val("")
      localStorage.removeItem('memory')
      memory_change = $("#memory").val();
      localStorage.removeItem('memory')
      memory_input = $("#memory").val()
       var memory = localStorage['memory'];
       if (memory) mymemory = memory;
       else mymemory = memory_input;
       localStorage['memory'] = mymemory;
      })
      /// end memory


      /// memory
    $("#memory").change(function(){
      $("#memory").val("")
      localStorage.removeItem('memory')
      memory_change = $("#memory").val();
      localStorage.removeItem('memory')
      memory_input = $("#memory").val()
       var memory = localStorage['memory'];
       if (memory) mymemory = memory;
       else mymemory = memory_input;
       localStorage['memory'] = mymemory;
      })
      /// end memory
      */

 
});