$(document).ready(function() {

baseUrl = $('#addOrderScript').data('baseUrl');

var checked = false;
var unchecked = false;
  $("#filter_item").click(function(){
      $("#display_error_for_add").empty();
    if($("#isAgeSelected").is(':checked') == true){
        checked = true;
        if(checked == true && unchecked == false){
            $("#harddrive_remove").remove();
          $("#motherboard_remove").remove();
        }
        
    }else{
        if(checked == true && unchecked == false){ ///add og harddisk og motherboard
            $("#harddrive_item").append(`<div id="harddrive_remove"><div class="text-center" style="font-size: x-large;">
            	Hard Drves</div></br>
				<div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="front_caddies" class="col-sm-2 col-form-label">Front Caddies</label>
                                <div class="col-sm-10">
                                    <select class="custom-select front_caddies_select" id="front_caddies" name="front_caddies">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="qty_front_caddies" class="col-sm-2 col-form-label">Qty</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qty_front_caddies" placeholder="Qty">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="rear_caddies" class="col-sm-2 col-form-label">Rear Caddies</label>
                                <div class="col-sm-10">
                                    <select class="custom-select rear_caddies_select" id="rear_caddies" name="rear_caddies">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="qty_rear_caddies" class="col-sm-2 col-form-label">Qty</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qty_rear_caddies" placeholder="Qty">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container">
                    <div class="row"  id="itemcontainer">
	                    <div class="col-md-6">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-1">
                                    <!--<button type="button" class="btn btn-link" id="add_item" style="margin-left: -15px;">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i></button>-->
                                    <!--<button type="button" class="btn btn-link" id="remove_item" style="margin-left: -15px;">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i></button>-->
                                    <!--<button type="button" class="btn btn-link" id="remove_item" style="margin-left: -15px;">
                                        <i class="fa fa-minus" aria-hidden="true"></i></button>-->
                                    </div>
                                    <div class="col-md-11">
                                        <div class="form-group row">
                                        <label for="item_harddrive_one" class="col-sm-2 col-form-label">HD</label>
                                            <div class="col-sm-10">
                                                <select class="custom-select item_harddrive_one_select" id="item_harddrive_one">
                                                    <option value="0">Select Harddisk</option>
                                                    <?php foreach($drives as $drive):   ?>
                                                    <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                                    <?php endforeach;   ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="qty_hard_drive_two" class="col-sm-2 col-form-label">Qty</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qty_hard_drive_one" placeholder="qty">
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="container">
                        <div class="row" id="itemcontainer">
                            <div class="col-md-6">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <!--<button type="button" class="btn btn-link" id="add_item" style="margin-left: -15px;">
                                                <i class="fa fa-plus-square" aria-hidden="true"></i></button>-->
                                            <!--<button type="button" class="btn btn-link" id="remove_item" style="margin-left: -15px;">
                                                <i class="fa fa-minus" aria-hidden="true"></i></button>-->
                                        </div>
                                        <div class="col-md-11">
                                            <div class="form-group row">
                                            <label for="item_harddrive_two" class="col-sm-2 col-form-label">HD</label>
                                                <div class="col-sm-10">
                                                    <select class="custom-select item_harddrive_two_select" id="item_harddrive_two">
                                                        <option>Select Harddisk</option>
                                                        <?php foreach($drives as $drive):   ?>
                                                        <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                                        <?php endforeach;   ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                <label for="qty_hard_drive_two" class="col-sm-2 col-form-label">Qty</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="qty_hard_drive_two" placeholder="Qty">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="container">
                        <div class="row" id="itemcontainer">
                            <div class="col-md-6">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <!--<button type="button" class="btn btn-link" id="add_item" style="margin-left: -15px;">
                                                <i class="fa fa-plus-square" aria-hidden="true"></i></button>-->
                                                <!--<button type="button" class="btn btn-link" id="remove_item" style="margin-left: -15px;">
                                                    <i class="fa fa-minus" aria-hidden="true"></i></button>-->
                                        </div>
                                        <div class="col-md-11">
                                            <div class="form-group row">
                                            <label for="item_harddrive_three" class="col-sm-2 col-form-label">HD</label>
                                                <div class="col-sm-10">
                                                    <select class="custom-select item_harddrive_three_select" id="item_harddrive_three">
                                                        <option>Select Harddisk</option>
                                                        <?php foreach($drives as $drive):   ?>
                                                        <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                                        <?php endforeach;   ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                <label for="qty_hard_drive_three" class="col-sm-2 col-form-label">Qty</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="qty_hard_drive_three" placeholder="Qty">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>`);
        	$('.front_caddies_select').select2({ width: '100%' });
  			$('.rear_caddies_select').select2({ width: '100%' });
  			$('.item_harddrive_one_select').select2({ width: '100%' });

  			$('.item_harddrive_two_select').select2({ width: '100%' });
  			$('.item_harddrive_three_select').select2({ width: '100%' });
                    
  		$("#add_item").click(function(){
            if(add == 0){
                $("#itemcontainer").append(`    
                <div class="col-md-6" id="hdoneremovenew">
                    <div class="container">
                        <div class="row">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <div class="col-md-11">
                                <div class="form-group row">
                                <label for="item_harddrive_two" class="col-sm-2 col-form-label">HD </label>
                                <div class="col-sm-10">
                                    <select class="custom-select item_harddrive_two_select" id="item_harddrive_two" name="rear_caddies">
                                        <option value="0">Select Harddisk</option>
                                        <?php foreach($drives as $drive):   ?>
                                        <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                        <?php endforeach;   ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="hdoneremovetextnew">
                <div class="form-group row">
                <label for="qty_hard_drive" class="col-sm-2 col-form-label">Qty</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="qty_hard_drive_two" placeholder="Qty">
                    </div>
                </div>
            </div>`);
        	$('.item_harddrive_two_select').select2({ width: '100%' });
                add = add + 1
                return
            }
            if(add == 1){
                $("#itemcontainer").append(`    
                <div class="col-md-6" id="hdtworemovenew">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div>
                            <div class="col-md-11">
                                <div class="form-group row">
                                <label for="item_harddrive_three" class="col-sm-2 col-form-label">HD</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select item_harddrive_three_select" id="item_harddrive_three" name="rear_caddies">
                                        	<option value="0">Select Harddisk</option>
                                        	<?php foreach($drives as $drive):   ?>
                                        	<option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                        	<?php endforeach;   ?>
                                    	</select>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" id="hdtworemovetextnew">
                    <div class="form-group row">
                     <label for="qty_hard_drive" class="col-sm-2 col-form-label">Qty</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="qty_hard_drive_three" placeholder="Qty">
                        </div>
                    </div>
                </div>`);
                $('.item_harddrive_three_select').select2({ width: '100%' });
                add = add + 1
                return
            }
        });

  		$("#remove_item").click(function(){
            
            if(add == 1){
                
                $("#hdoneremovenew").remove();
                $("#hdoneremovetextnew").remove();
                add = add - 1
            }
            if(add == 2){
                //alert("add 2 is appearing and will remove it")
                $("#hdtworemovenew").remove();
                $("#hdtworemovetextnew").remove();
                add = add - 1
            }
        });
      /* $("#motherboard_item").append(`<div id="motherboard_remove"><div class="text-center" style="font-size: x-large;">
       			MotherBoard</div></br>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="motherboard" class="col-sm-2 col-form-label">MotherBoard</label>
                            <div class="col-sm-10" id="motherboard_chassis">
                                <select class="custom-select motherboard_select" id="motherboard"></select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label for="qty_motherboard" class="col-sm-2 col-form-label">Qty</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="qty_motherboard" placeholder="Qty">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label for="cpu" class="col-sm-2 col-form-label">CPU</label>
                            <div class="col-sm-10" id="motherboard_cpu">
                                <select class="custom-select cpu_select" id="cpu"></select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label for="qty_cpu" class="col-sm-2 col-form-label">Qty</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="qty_cpu" placeholder="Qty">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label for="memory" class="col-sm-2 col-form-label">Memory</label>
                            <div class="col-sm-10" id="motherboard_memory">
                                <select class="custom-select memory_select" id="memory">
                                    <option>option 1</option></select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label for="qty_memory" class="col-sm-2 col-form-label">Qty</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="qty_memory" placeholder="Qty">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);checked = false;
            }
        }
  	}); */

	/*///////////////////////////// open walay gamit second mudal ne*/
  /*  $("#filter_item_view_modal").click(function(){
    	if($("#view_modal_order").is(':checked') == true){
        	checked = true;
        if(checked == true && unchecked == false){
            $("#harddrive_remove_").remove();
          	$("#motherboard_remove_").remove();
        }
        
    	}else{
        	if(checked == true && unchecked == false){
            	$("#harddrive_item_").append(`<div id="harddrive_remove_"><div class="text-center" style="font-size: x-large;">
            		Hard Drves</div></br>
					<div class="container">
                        <div class="row">
	                        <div class="col-md-6">
                                <div class="form-group row">
                                <label for="front_caddies_" class="col-sm-2 col-form-label">Front Caddies</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select front_caddies_select" id="front_caddies_" name="front_caddies">
                                            <option></option></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                <label for="qty_front_caddies_" class="col-sm-2 col-form-label">Qty</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="qty_front_caddies_" placeholder="Qty">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                <label for="rear_caddies_" class="col-sm-2 col-form-label">Rear Caddies</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select rear_caddies_select" id="rear_caddies_" name="rear_caddies">
                                            <option></option> </select>
                                    </div>
                                </div>
                            </div>
                          	<div class="col-md-6">
                                <div class="form-group row">
                                <label for="qty_rear_caddies_" class="col-sm-2 col-form-label">Qty</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="qty_rear_caddies_" placeholder="Qty">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="container">
                        <div class="row"  id="itemcontainer">
                            <div class="col-md-6">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <button type="button" class="btn btn-link" id="add_item" style="margin-left: -15px;">
                                                <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="col-md-11">
                                            <div class="form-group row">
                                            <label for="item_harddrive_one_" class="col-sm-2 col-form-label">HD</label>
                                                <div class="col-sm-10">
                                                    <select class="custom-select item_harddrive_one_select" id="item_harddrive_one_" name="rear_caddies">
                                                        <?php foreach($drives as $drive):   ?>
                                                        <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                                        <?php endforeach;   ?></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                <label for="qty_hard_drive_one_" class="col-sm-2 col-form-label">Qty</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="qty_hard_drive_one_" placeholder="qty">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`);
            $('.front_caddies_select').select2({ width: '100%' });
  			$('.rear_caddies_select').select2({ width: '100%' });
  			$('.item_harddrive_one_select').select2({ width: '100%' });
            add_id = 0;
            $("#add_item").on('click', function(){
                if(add_id == 0){
                $("#itemcontainer").append(`    
                <div class="col-md-6">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-11">
                                <div class="form-group row">
                                <label for="item_harddrive_two_" class="col-sm-2 col-form-label">HD</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select item_harddrive_two_select" id="item_harddrive_two_" name="rear_caddies">
                                            <?php foreach($drives as $drive):   ?>
                                            <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                            <?php endforeach;   ?></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                    <label for="qty_hard_drive" class="col-sm-2 col-form-label">Qty</label>
                    	<div class="col-sm-10">
                            <input type="text" class="form-control" id="qty_hard_drive_two" placeholder="Qty">
                        </div>
                    </div>
                </div>`);
            	}else if(add_id == 1){
                $("#itemcontainer").append(`    
                <div class="col-md-6">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1"></div>
                                <div class="col-md-11">
                                    <div class="form-group row">
                                    <label for="item_harddrive_three_" class="col-sm-2 col-form-label">HD</label>
                                        <div class="col-sm-10">
                                            <select class="custom-select item_harddrive_three_select" id="item_harddrive_three_" name="rear_caddies">
                                                <?php foreach($drives as $drive):   ?>
                                                <option value=<?= $drive->sku ?>><?= $drive->bar ?><?= $drive->ff ?><?= $drive->brand ?><?= $drive->type ?></option>
                                                <?php endforeach;   ?></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label for="qty_hard_drive" class="col-sm-2 col-form-label">Qty</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="qty_hard_drive_three" placeholder="Qty">
                            </div>
                        </div>
                    </div>`);
            }
            $('.item_harddrive_two_select').select2({ width: '100%' });
            $('.item_harddrive_three_select').select2({ width: '100%' });
            
            add_id = add_id + 1;
        });
           $("#motherboard_item_").append(`<div id="motherboard_remove_"><div class="text-center">
           		MotherBoard</div></br>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="motherboard_" class="col-sm-2 col-form-label">MotherBoard</label>
                                <div class="col-sm-10" id="motherboard_chassis_">
                                    <select class="custom-select motherboard_select" id="motherboard_"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="qty_motherboard_" class="col-sm-2 col-form-label">Qty</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qty_motherboard_" placeholder="Qty">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="cpu_" class="col-sm-2 col-form-label">CPU</label>
                                <div class="col-sm-10" id="motherboard_cpu">
                                    <select class="custom-select cpu_select" id="cpu_"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="qty_cpu_" class="col-sm-2 col-form-label">Qty</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qty_cpu_" placeholder="Qty">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="memory_" class="col-sm-2 col-form-label">Memory</label>
                                <div class="col-sm-10" id="motherboard_memory">
                                    <select class="custom-select memory_select" id="memory_">
                                        <option>option 1</option></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                            <label for="qty_memory_" class="col-sm-2 col-form-label">Qty</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qty_memory_" placeholder="Qty">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`);checked = false;
         	}
        }
  	}); */
  	/*END*/

  	$('.js-example-basic-single').select2({ width: '100%' });
  	$('.front_caddies_select').select2({ width: '100%' });
  	$('.rear_caddies_select').select2({ width: '100%' });
  	$('.item_harddrive_one_select').select2({ width: '100%' });
  	$('.cpu_select').select2({ width: '100%' });
  	$('.memory_select').select2({ width: '100%' });
  	$('.motherboard_select').select2({ width: '100%' });
  	$('.item_harddrive_two_select').select2({ width: '100%' });
  	$('.item_harddrive_three_select').select2({ width: '100%' });
   
   	$("#close_modal_new").click(function(){//alert()
    	$("#hdoneremovenew_").remove();
    	$("#hdoneremovetextnew_").remove();
    	$("#hdtworemovenew_").remove();
    	$("#hdtworemovetextnew_").remove();
    	return
  	});

  	

});