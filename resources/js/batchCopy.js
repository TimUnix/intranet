$(document).ready(function() {
    $("#product-table").on('click', '.check-all-toggle', function() {
        const checkAllToggle = $('.check-all-toggle');
        $("td .checkbox-row")
            .each(function() {
                $(this).prop('checked', checkAllToggle.prop('checked'));
            });
    });


    /// new sticky header check all toggle
    $(".sticky-table").on('click', '.check-all-toggle_sticky_header', function() {
        const checkAllToggle = $('.check-all-toggle_sticky_header');
        $("td .checkbox-row")
            .each(function() {
                $(this).prop('checked', checkAllToggle.prop('checked'));
            });
    });
    /// end new sticky header check all toggle

    $("#product-table").on('click', '.btn-batch-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {
            const copyButton = $(this);
            
            const colPartNumber = copyButton.closest('tr').find('.par');
            const colDescription = copyButton.closest('tr').find('.des');;
            const colCondition = copyButton.closest('tr').find('.con');
            const colQuantity = copyButton.closest('tr').find('.qty');
            const colPrice = copyButton.closest('tr').find('.price');
    
            
            const condition = colCondition.data('value');
            const partNumber = colPartNumber.data('value');
            const description = colDescription.data('value');
            const quantity = colQuantity.data('value');
            const price = colPrice.data('value');
    
    
            const copiedData =  "$"+ price + "  " + condition + "  " + partNumber + "  " + description +" - " + quantity + " qty " ;
            console.log(copiedData);
            rows.push(copiedData);
        });

        const text = rows.join('\n');
        $('.textarea-copy').val('');
        $('.textarea-copy').val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });


    /// new sticky header batch copy
    $(".sticky-table").on('click', '.btn-batch-copy_sticky_header', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const colPartNumber = columnsToCopy.partNumber;
            const colDescription = columnsToCopy.description;
            const colCondition = columnsToCopy.condition;
            const colQuantity = columnsToCopy.quantity;
            const colTotal = columnsToCopy.total;
            const colPrice = columnsToCopy.price;
            
            const isTotalBlank = rowData[colTotal] === undefined || rowData[colTotal].trim() === '';
            const isConditionBlank = rowData[colCondition] === undefined || rowData[colCondition].trim() === '';
            const isPartNumberBlank = rowData[colPartNumber] === undefined || rowData[colPartNumber].trim() === '';
            const isDescriptionBlank = rowData[colDescription] === undefined ||
                  rowData[colDescription].trim() === '';
            const isQuantityBlank = rowData[colQuantity] === undefined || rowData[colQuantity].trim() === '';
            const isPriceBlank = rowData[colPrice] === undefined || rowData[colPrice].trim() === '';

            const total = isTotalBlank ? '' : `${rowData[colTotal].trim()}`;
            const condition = isConditionBlank ? '' : `   ${rowData[colCondition].trim()}`;
            const partNumber = isPartNumberBlank ? '' : `   ${rowData[colPartNumber].trim()}`;
            const description = isDescriptionBlank ? '' : `   ${rowData[colDescription].trim()}`;
            const quantity = isQuantityBlank ? '' : ` - ${rowData[colQuantity].trim()} qty`;
            const price = isPriceBlank ? '' : ` ${rowData[colPrice].trim()}`;

            const copiedData = `${price}${condition}${partNumber}` +
                  `${description}${quantity}`;

            rows.push(copiedData);
        });

        const text = rows.join('\n');
        $('.textarea-copy').val('');
        $('.textarea-copy').val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    /// end new sticky header batch copy

    $("#product-table").on('click', '.btn-par-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {

            const copyButton = $(this);
            const colPartNumber = copyButton.closest('tr').find('.par');
            const partNumber = colPartNumber.data('value');
        const copiedData = partNumber;
        rows.push(copiedData);
        });

        const text = rows.join('\n');
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });


    /// sticky header batch copy
    $(".sticky-table").on('click', '.btn-par-copy_sticky_header', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.partNumber);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    /// end sticky header batch copy

    $("#product-table").on('click', '.btn-bar-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {

        const copyButton = $(this);
        const colbar = copyButton.closest('tr').find('.bar');
        const barNumber = colbar.data('value');
        const copiedData = barNumber;
        rows.push(copiedData);
        });

        const text = rows.join('\n');
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });


    /// new bar copy sticky header
    $(".sticky-table").on('click', '.btn-bar-copy_sticky_header', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.bar);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    /// end bar copy sticky header

    $("#product-table").on('click', '.btn-des-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {

        const copyButton = $(this);
        const colDes = copyButton.closest('tr').find('.des');
        const Des = colDes.data('value');
        const copiedData = Des;
        rows.push(copiedData);
        });

        const text = rows.join('\n');
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });


    /// new des copy sticky header
    $(".sticky-table").on('click', '.btn-des-copy_sticky_header', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.description);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });
    /// end des copy sticky header


    $("#product-table").on('click', '.item-rwidgets', function(event) {
        const button = $(event.target).closest('.item-rwidgets');

            if (!button) return;

            const itemNumber = button.closest('tr').find('.item-item-number');
            ///alert(itemNumber.text().trim())
            $('#view').html(
                `<div>${spinner}</div>`
            );

            var datas = {
                'item_number' : itemNumber.text().trim(),
                ajax : '1'
            };
            $.ajax({
                url : `${baseUrl}index.php/ebay_competitors/get_ebay_item_json`,
                type : "post",
                data : datas,
                success : function(response){
                   $('#view').html(response)
                }
            });
    });

    function getCopyText(index) {
        let rows = [];
        
        $("td .checkbox-row:checked").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const isCellBlank = rowData[index] !== undefined && rowData[index].trim().length === 0;

            if (!isCellBlank) rows.push(rowData[index].trim());
        });

        return rows.join('\n');
    }
});
