

$(document).ready(function() {
	baseUrl = $('#usersAccessScript').data('baseUrl');

	syncStickyHeader();

	$.get(`${baseUrl}index.php/users_access/get_users`, function(response) {
        $('#users_access_table').html(response);
	});

	$.get(`${baseUrl}index.php/users_access/get_modules`, function(response) {
        $('#modules_access_table').html(response);
	});

	$('#users_searchColumn').click(function(event){
        var data = {
            'searchItem' : $('#users_itemSearch').val().trim()}
			
            $.get(`${baseUrl}index.php/users_access/search_users`,data,  function(response) {
                $('#users_access_table').html(response);    
            });
    });

    $("#users_itemSearch").keypress(function(event) {
        if (event.keyCode == 13) {
            $("#users_searchColumn").click();
        }
    });

	$('#modules_searchColumn').click(function(event){
        var data = {
            'searchItem' : $('#modules_itemSearch').val().trim()}
			
            $.get(`${baseUrl}index.php/users_access/search_modules`,data,  function(response) {
                $('#modules_access_table').html(response);    
            });
    });

    $("#modules_itemSearch").keypress(function(event) {
        if (event.keyCode == 13) {
            $("#modules_searchColumn").click();
        }
    });

	
	$('#users_access_table').on('click', function(event) { 
		var get_userid = $(event.target).closest('.item-users');
		var user_checkbox = get_userid.closest('tr').find('.users-access');
		var checkbox = user_checkbox.closest('tr').find('input:checkbox').get(0);
        var user_id =  user_checkbox.val();

		if ($('.users-access').filter(':checked').length >= 1) {
			$('.users-access').not(this).prop('checked', false);
			checkbox.checked = !checkbox.checked;
		} else if ((event.target != checkbox)) {
			checkbox.checked = !checkbox.checked;
		}

		$('.users-access').change(function(event) { 
			if ($('.users-access').filter(':checked').length >= 1) {
				$('.users-access').not(this).prop('checked', false);
			} else if ($('.users-access').filter(':checked').length == 0) {
				$('#modules_access_table').hide();
			}
		});
		
		$('#user-module_id').text(user_id);

		var data = {
			'user_id' : user_id
		}

		$.get(`${baseUrl}index.php/users_access/show_modules_checker`,data,  function(response) {
			if(response.menu_checker) {
				var datas = {
					'user_id' : $('#user-module_id').text()
				}

				$.get(`${baseUrl}index.php/users_access/insert_modules`,datas);
			}
		});
	
		$.get(`${baseUrl}index.php/users_access/check_superadmin`,data, function(response) {
			if(response.admin == 0) {
				$('#superadmin').prop('checked', false);
				$('#modules_access_table').show();
			} else if (response.admin == 1) {
				$('#superadmin').prop('checked', true);
				$('#modules_access_table').hide();
			}
		});
		$.get(`${baseUrl}index.php/users_access/show_modules`,data,  function(response) {
			$('#modules_access_table').html(response);
		});
	});

	$('#modules_access_table').on('click', function(event) {
		var get_menuid = $(event.target).closest('.item-modules, .item-menu');
		var menu_checkbox = get_menuid.closest('tr').find('.module-access, .modules-access, .menu-access');
		var checkbox = menu_checkbox.closest('tr').find('input:checkbox').get(0);
		var menu_id = menu_checkbox.val();

		if ((event.target != checkbox)) {
			checkbox.checked = !checkbox.checked;
		}

		if ($('.modules-access, .menu-access').is(":checked") === true) {
			var access = 1;
		} else if ($('.modules-access, .menu-access').is(":checked") === false) {
			var access = 0;
		}

		var data = {
			'access' : access,
			'menu_id' : menu_id,
			'user_id' : $('#user-module_id').text()
		}

		$.get(`${baseUrl}index.php/users_access/update_menu_access`,data, function(response) {
			$.get(`${baseUrl}index.php/users_access/show_modules`,data,  function(response) {
				$('#modules_access_table').html(response);
			});
		});
	});

	$('#superadmin').click(function(event) {
		if ($('#superadmin').is(":checked") === true) {
			$('#modules_access_table').hide();
			
			var data = {
				'access' : 1,
				'user_id' : $('#user-module_id').text()
			}
		} else {
			$('#modules_access_table').show();

			var data = {
				'access' : 0,
				'user_id' : $('#user-module_id').text()
			}
		}

		$.get(`${baseUrl}index.php/users_access/superadmin`,data,  function(response) {		
			$.get(`${baseUrl}index.php/users_access/show_modules`,data,  function(response) {
				$('#modules_access_table').html(response);
			});
		});

	});
});
