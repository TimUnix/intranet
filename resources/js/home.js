$(document).ready(function() {
    const baseUrl = $('#homeScript').data('baseUrl');

    $(".chart-container").css('height', '500px');
    $(".chart-container").css('width', '500px');

    $("#total-items").html(spinnerLight);
    $("#total-price").html(spinnerLight);
    $("#brand-one-items").html(spinnerLight);
    $("#brand-one-price").html(spinnerLight);
    $("#brand-two-items").html(spinnerLight);
    $("#brand-two-price").html(spinnerLight);

    const colors = [
        '#389dd6', '#614647', '#db356b', '#8a604d',
        '#806e4d', '#367bd9', '#6343bf', '#41544e',
        '#58e82c', '#a9b547', '#87734d', '#4c9c6e',
        '#99964d', '#968a4d', '#b34774', '#3f3c4a',
        '#4d826f', '#2e2f33', '#475f63', '#6b6a49',
        '#41544c', '#4e6b49', '#de33b2', '#8a4d5e',
        '#6cb547', '#58f522', '#3f4238', '#c4b941',
        '#5ca64b', '#2aeba1', '#595e45', '#91e82c',
        '#47664d', '#4c7d64', '#5c5b44', '#313838',
        '#424d57', '#424154', '#445742', '#d63857',
        '#41544b', '#41c48c', '#a1834c', '#49616b',
        '#c7406c', '#db356b', '#7a8a4d', '#4d857e',
        '#62784c', '#704a69', '#b54768', '#3541db',
        '#4d7485', '#f02687', '#383331', '#40c7b7',
        '#c24d42', '#a2d638', '#3d353a', '#f7e120'
    ];


    $.get(`${baseUrl}index.php/search/request/load_summary`,
          function(response) {
              $("#total-items").html(response['amount']);
              $("#total-price").html(`$${response['price']}`);
          });

    $.get(`${baseUrl}index.php/search/request/brand_items_and_price`,
          function(response) {
              $(".chart-spinner").hide();

              const filteredItems = response.items.total_items.filter(
                  function(item) {
                      return item.man !== '';
                  }
              );

              const brandOneItems = filteredItems[0];
              const brandTwoItems = filteredItems[1];

              const filteredPrices = response.price.filter(function(item) {
                  return item.brand_name !== '';
              });

              const priceOne = filteredPrices[0];
              const priceTwo = filteredPrices[1];

              $("#brand-one-items-label").html(brandOneItems.man);
              $("#brand-two-items-label").html(brandTwoItems.man);

              $("#brand-one-items").html(brandOneItems.countman);
              $("#brand-two-items").html(brandTwoItems.countman);

              const brandOnePrice = Math.floor(priceOne.get_the_price);
              const brandTwoPrice = Math.floor(priceTwo.get_the_price);

              $("#brand-one-price").html(`$${brandOnePrice}`);
              $("#brand-two-price").html(`$${brandTwoPrice}`);

              $("#brand-one-price-label").html(priceOne.brand_name);
              $("#brand-two-price-label").html(priceTwo.brand_name);

              const brandItemsNames = response.items.total_items.map(
                  function(brand) {

                      const label = brand.man === '' ? 'Blank' : brand.man;

                      return `${label} (${brand.countman})`;
                  });
              
              const brandItemsCount = response.items.total_items.map(
                  function(brands) {
                      return brands.countman;
                  }
              );

              const brandItemsCtx = $("#brandItemsChart")[0]
                    .getContext("2d");
              const brandItemsChart = new Chart(brandItemsCtx, {
                  type: 'doughnut',
                  data: {
                      labels: brandItemsNames,
                      datasets: [{
                          label: "Items by Brand",
                          data: brandItemsCount,
                          backgroundColor: colors
                      }],
                  }
              });

              const brandPricesNames = response.price.map(
                  function(brand) {
                      const label = brand.brand_name === '' ? 'Blank'
                            : brand.brand_name;

                      const price = Math.floor(brand.get_the_price);

                      return `${label} ($${price})`;
                  }
              );
              
              const brandsTotalPrices = response.price.map(function(brands) {
                  return brands.get_the_price;
              });

              const brandPricesCtx = $("#brandPricesChart")[0]
                    .getContext("2d");
              const brandPricesChart = new Chart(brandPricesCtx, {
                  type: 'doughnut',
                  data: {
                      labels: brandPricesNames,
                      datasets: [{
                          label: "Item Prices by Brand",
                          data: brandsTotalPrices,
                          backgroundColor: colors
                      }]
                  }
              });
          }
         );

    $.get(`${baseUrl}index.php/search/request/category_and_condition_count`,
          function(response) {
              const categories = response.categories;
              const conditions = response.conditions;

              const categoryNames = categories.map(function(category) {
                  return `${category.cat} (${category.cat_count})`;
              });

              const categoriesChartCtx = $("#categoryItems")[0]
                    .getContext('2d');

              const categoriesChart = new Chart(categoriesChartCtx, {
                  type: 'doughnut',
                  data: {
                      labels: categoryNames,
                      datasets: [{
                          label: "Items by Category",
                          data: categories.map(function(category) {
                              return category.cat_count;
                          }),
                          backgroundColor: colors,
                      }],
                      options: {
                          legend: {
                              position: 'left'
                          }
                      }
                  }
              });

              const conditionNames = conditions.map(function(condition) {
                  return `${condition.con} (${condition.con_count})`;
              });

              const conditionChartCtx = $("#conditionItems")[0]
                    .getContext('2d');

              const conditionChart = new Chart(conditionChartCtx, {
                  type: 'doughnut',
                  data: {
                      labels: conditionNames,
                      datasets: [{
                          label: "Items by Condition",
                          data: conditions.map(function(condition) {
                              return condition.con_count;
                          }),
                          backgroundColor: colors
                      }]
                  }
              });
          }
         );
    $.get(`${baseUrl}index.php/search/request/year_count`,
          function(response) {
              const years = response.years;
              
              const yearNames = years.map(function(year) {
                  return `${year.y} (${year.y_count})`;
              });

              const yearChartCtx = $("#yearItems")[0]
                    .getContext('2d');

              const yearsChart = new Chart(yearChartCtx, {
                  type: 'doughnut',
                  data: {
                      labels: yearNames,
                      datasets: [{
                          label: "Items by Year",
                          data: years.map(function(year) {
                              return year.y_count;
                          }),
                          backgroundColor: colors,
                      }],
                      options: {
                          legend: {
                              position: 'left'
                          }
                      }
                  }
              });
              
         } /* function response*/
         );
    //himuan ug lain
    $.get(`${baseUrl}index.php/search/request/pie_area_count`,
          function(response) {
            const inv_count = response.inv_count;
            const pieInventories = response.pieInventories;
            const uni_count = response.uni_count;
            const itr_count = response.itr_count;

              //pie inventory
              const pieInvNames = pieInventories.map(function(pieInventory) {
                  return `${pieInventory.inv} (${pieInventory.inv_count})`;
              });

              const pieInventoryChartCtx =$("#pieInventory")[0]
                    .getContext('2d');
              const pieInventoryChart = new Chart(pieInventoryChartCtx, {
                  type: 'doughnut',
                  data: {
                      labels: pieInvNames,
                      datasets: [{
                          label: "test test",
                          data: pieInventories.map(function(pieInventory) {
                              return pieInventory.inv_count;
                          }),
                          backgroundColor: colors
                      }]
                  }
              });//end
              //area
            const yearInvNames = inv_count.map(function(yearInventory) {
                  return yearInventory.y;
              });
             const yearInventoryChartCtx =$("#yearInventory")[0].getContext('2d');
            // document.getElementById('yearInventory').getContext('2d')
              const yearInventoryChart = new Chart(yearInventoryChartCtx, {
                  type: 'line',
                  data: {
                      labels: yearInvNames,
                      datasets: [{
                          label: "Inv",
                          fill: true,
                          borderWidth: 1,
                          lineTension: 0.5,
                          borderWidth: 3,
                          backgroundColor: 'rgba(210, 214, 222, 0.4)',
                          borderColor: 'rgba(189, 195, 195, 1)',   
                          pointRadius: false,
                          pointColor: 'rgba(210, 214, 222, 1)',
                          pointStrokeColor: '#c1c7d1',
                          pointHighlightFill: '#fff',
                          pointHighlightStroke: 'rgba(220,220,220,1)',          
                          data: 
                          inv_count.map(function(invCount) {
                              return invCount.inv_count;
                          }),
                         
                      },
                    {
                        label: "Uni",                            
                          fill: true,  
                          borderWidth: 1, 
                          lineTension: 0.5,
                          backgroundColor: 'rgba(75, 192, 192, 0.4)',
                          borderColor: 'rgba(75, 192, 192, 1)',
                          borderWidth: 3,
                        //  backgroundColor: 'rgba(60,141,188,0.9)',
                         // borderColor: 'rgba(60,141,188,0.8)',                         
                          pointRadius: false,                  
                          data: 
                          uni_count.map(function(uniCount) {
                              return uniCount.uni_count;
                          }),
                         // backgroundColor: colors
                      },
                      {
                        label: "Itr",                            
                          fill: true,  
                          borderWidth: 1, 
                          lineTension: 0.5,
                          backgroundColor: 'rgba(255, 192, 192, 0.4)',
                          borderColor: 'rgba(255, 192, 192, 1)',
                          borderWidth: 3,                                              
                          pointRadius: true,                        
                          data: 
                           itr_count.map(function(itrCount) {
                              return itrCount.itr_count;
                      }),
                      }
                      ]
                      
                  },
                  //option
                      options: {
                        responsive: true, // Instruct chart js to respond nicely.   
                        propagate: true 
                                                          
                      } //end of option
              });//end
              
          } //function response
         );
    
});
