$(document).ready(function() {
    $("#product-table").on('click', '.btn-copy-row', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const rowDOM = copyButton.closest('tr');
        const rowData = [...rowDOM[0].cells].map(function(cell) {
            return cell.textContent;
        });

        const colSeller = columnsToCopy.seller;
        const colTitle = columnsToCopy.title;
        const colItemNumber = columnsToCopy.itemnumber;
        const colPrice = columnsToCopy.price;
        const colTotal = columnsToCopy.total;
        
        const isTotalBlank = rowData[colTotal] === undefined || rowData[colTotal].trim() === '';
        const isItemNumberBlank = rowData[colItemNumber] === undefined || rowData[colItemNumber].trim() === '';
        const isSellerBlank = rowData[colSeller] === undefined || rowData[colSeller].trim() === '';
        const isTitleBlank = rowData[colTitle] === undefined ||
              rowData[colTitle].trim() === '';
        const isPriceBlank = rowData[colPrice] === undefined || rowData[colPrice].trim() === '';

        const total = isTotalBlank ? '' : `${rowData[colTotal].trim()}`;
        const ItemNumber = isItemNumberBlank ? '' : `   ${rowData[colItemNumber].trim()}`;
        const Seller = isSellerBlank ? '' : `   ${rowData[colSeller].trim()}`;
        const Title = isTitleBlank ? '' : `   ${rowData[colTitle].trim()}`;
        const Price = isPriceBlank ? '' : ` - ${rowData[colPrice].trim()}`;

        const copiedDataToDisplay = `${ItemNumber}${Title}` +
              `${Seller}${Price}`;

        $('.textarea-copy').val('');
        $('.textarea-copy').val(copiedDataToDisplay);
        
        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = copiedDataToDisplay;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();
    });
});
