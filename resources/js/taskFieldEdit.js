let request_actions;

$(document).ready(function() {
    let currentCellNewPrice;
    let currentCellNotes;
    let currentPar;
    let archiveRow;
    let archiveId;
    
    const baseUrl = $('#listingFieldEditScript').data('baseUrl');

    $('#items').on('click', '.new-price', function(event) {
        const newPrice = $(event.target).closest('.new-price');

        if (!newPrice) return;

        const value = newPrice.data('value');

        addEditableField(newPrice, value, 'new-price');
    });

    $('#items').on('click', '.notes .notebtn', function(event) {
        if ($(event.target).hasClass('notebtn') || $(event.target).hasClass('notepenbtn')) {
            const notes = $(event.target).closest('.notes');

            if (!notes) return;

            const value = notes.data('value').toString();

            addEditableFieldnote(notes, value, 'notes');
        }
    });


    ///
    $('#items').on('click', '.datetimes', function(event) {
        const notes = $(event.target).closest('.datetimes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'datetimes');
    });
    /////////////sku
    $('#items').on('click', '.sku', function(event) {
        if ($(event.target).hasClass('skubtn')) {
            const cell = $(event.target).closest('.sku');
    
            if (!cell) return;
    
            const value = cell.data('value');

            addEditableField(cell, value, 'sku');
        }
    });
    
    $('#items').click('.sku-editable .sku-cancel', function(event) {
        if ($(event.target).hasClass('sku-cancel')) {
            const cell = $(event.target).closest('.sku-editable');

            if (!cell) return;
            
            const requester = cell.data('requester');
            const editor = cell.data('editor');

            const nameToShow = (editor !== null && editor !== '' && editor !== undefined) ? editor : requester;

            const dateRequested = cell.data('dateRequested');
            const lastEdited = cell.data('lastEdited');

            const dateToShow = (lastEdited !== null && lastEdited !== undefined && lastEdited !== '') ? lastEdited
                  : dateRequested;

            const firstname = nameToShow.trim().split(' ').slice(0, 1);
            const firstnameWithTooltip = showNameWithTooltip(firstname, nameToShow);

            cell.html(`<button type="button" class="btn btn-link text-primary skubtn"><i class="fa fa-pen skubtn" aria-hidden="true"></i></button>${cell.data('value')} ${firstnameWithTooltip}  ${dateToShow}`);
            cell.removeClass('sku-editable');
            cell.addClass('sku');
        }
    });
    $('#items').click('.sku-editable .sku-apply', function(event) {
        if ($(event.target).hasClass('sku-apply')) {
            const cell = $(event.target).closest('.sku-editable');
            const listing_id = cell.closest('tr').find('.listing_id');
            const par = cell.closest('tr').find('.par');
            const datetime = cell.closest('tr').find('.datetime');
            const request = cell.closest('tr').find('.request');
            const sku = cell.closest('tr').find('.sku');
            
            const textField = cell.find('.sku-input');
            if (!textField) return;
            if (!textField.val()) return;
            
            const requester = cell.data('requester');
            const editor = cell.data('editor');

            const nameToShow = (editor !== null && editor !== undefined && editor !== '') ? editor : requester;
            const nameWithTooltip = showNameWithTooltip(nameToShow.split(' ').slice(0, 1), nameToShow);

            const dateRequested = cell.data('dateRequested');
            const lastEdited = cell.data('lastEdited');

            const dateToShow = (lastEdited !== null && lastEdited !== undefined && lastEdited !== '') ? lastEdited
                  : dateRequested;

            cell.html('<button type="button" class="btn btn-link text-primary draftid-cancel skubtn">'
                      + `<i class="fa fa-pen skubtn" aria-hidden="true"></i></button>${textField.val()} `
                      + `${nameWithTooltip} ${dateToShow}`);
            cell.data('value', textField.val());
            cell.removeClass('sku-editable');
            cell.addClass('sku');
            
            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "sku"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
        }
    });
    ////////////sku

    $('#items').on('click', '.photo .photobtn', function(event) {
        const photoBtn = $(event.target).closest('.photobtn');
        
        if (!photoBtn) return;

        const photo = photoBtn.closest('.photo');

        if (!photo) return;
        
        const value = photo.data('value');

        const input = $('<select class="form-control photo-input w-auto">' +
                        '<option value="1">YES</option>' +
                        '<option value="0" selected>NO</option>' +
                        '</select>');
        
        const btnAccept = $('<button type="button" class="btn btn-link text-success photo-apply"></button>');
        btnAccept.html('<i class="fa fa-check photo-apply" aria-hidden="true"></i>');

        const btnCancel = $('<button type="button" class="btn btn-link text-danger photo-cancel"></button>');
        btnCancel.html('<i class="fa fa-times photo-cancel" aria-hidden="true"></i>');

        photo.html(input).append(btnCancel).append(btnAccept);
        photo.removeClass('photo');
        photo.addClass('photo-editable');
    });

    $('#items').click('.photo-editable .photo-cancel', function(event) {
        if ($(event.target).hasClass('photo-cancel')) {
            const btnCancel = $(event.target).closest('.photo-cancel');

            if (!btnCancel) return;

            const photo = btnCancel.closest('td');
            if (!photo) return;
            const lastEdited = photo.data('lastEdited');
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const editor = photo.data('editor');
            const nameWithTooltip = showNameWithTooltip(editor.split(' ').slice(0, 1), editor);

            photo.html(`<button type="button" class="btn btn-link text-primary draftid-cancel photobtn">
                   <i class="fa fa-pen" aria-hidden="true"></i>
                   </button>
                   ${nameWithTooltip} ${lastEditedText}
                   `);

            photo.removeClass('photo-editable');
            photo.addClass('photo');
        }
    });

    $('#items').click('.photo-editable .photo-apply', function(event) {
        if ($(event.target).hasClass('photo-apply')) {
            const btnApply = $(event.target).closest('.photo-apply');

            if (!btnApply) return;

            const photo = btnApply.closest('td');
            const photoInput = photo.find('.photo-input');
            const datetime = btnApply.closest('tr').find('.datetime');
            const sku = btnApply.closest('tr').find('.sku');
            const listingId = btnApply.closest('tr').find('.listing_id');

            if (!datetime || !photoInput || !sku || !listingId) return;

            const value = photoInput.val();

            photo.removeClass('photo-editable');
            photo.addClass('photo');
            
            const data = {'data': {
                'request': value,
                'datetime': datetime.text(),
                'sku': sku.text(),
                'listing_id': listingId.text(),
                'col': 'photo'
            }};

            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
    });

    $('#items').on('click', '.moved .movedbtn', function(event) {
        const movedBtn = $(event.target).closest('.movedbtn');
        
        if (!movedBtn) return;

        const moved = movedBtn.closest('.moved');

        if (!moved) return;
        
        const value = moved.data('value');

        addEditableField(moved, value, 'moved');
    });

    $('#items').click('.moved-editable .moved-cancel', function(event) {
        if ($(event.target).hasClass('moved-cancel')) {
            const btnCancel = $(event.target).closest('.moved-cancel');

            if (!btnCancel) return;

            const moved = btnCancel.closest('td');
            const value = moved.data('value');
            const lastEdited = moved.data('last-edited');
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const editor = moved.data('editor');
            const nameWithTooltip = showNameWithTooltip(editor.split(' ').slice(0, 1), editor);

            moved.html(`<button type="button" class="btn btn-link text-primary draftid-cancel movedbtn">
                   <i class="fa fa-pen" aria-hidden="true"></i>
                   </button>
                   ${value}
                   ${nameWithTooltip} ${lastEditedText}
                   `);

            moved.removeClass('moved-editable');
            moved.addClass('moved');
        }
    });

    $('#items').click('.moved-editable .moved-apply', function(event) {
        if ($(event.target).hasClass('moved-apply')) {
            const btnApply = $(event.target).closest('.moved-apply');

            if (!btnApply) return;

            const moved = btnApply.closest('td');
            const movedInput = moved.find('.moved-input');
            const datetime = btnApply.closest('tr').find('.datetime');
            const sku = btnApply.closest('tr').find('.sku');
            const listingId = btnApply.closest('tr').find('.listing_id');

            if (!datetime || !movedInput || !sku || !listingId) return;

            const value = movedInput.val();

            moved.removeClass('moved-editable');
            moved.addClass('moved');
            
            const data = {'data': {
                'request': value,
                'datetime': datetime.text(),
                'sku': sku.text(),
                'listing_id': listingId.text(),
                'col': 'moved'
            }};

            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
    });
    
    $('#items').on('click', '.lot .lotbtn', function(event) {
        const lotBtn = $(event.target).closest('.lotbtn');
        
        if (!lotBtn) return;

        const lot = lotBtn.closest('.lot');

        if (!lot) return;
        
        const value = lot.data('value');

        addEditableField(lot, value, 'lot');
    });

    $('#items').click('.lot-editable .lot-cancel', function(event) {
        if ($(event.target).hasClass('lot-cancel')) {
            const btnCancel = $(event.target).closest('.lot-cancel');

            if (!btnCancel) return;

            const lot = btnCancel.closest('td');
            const value = lot.data('value');
            const lastEdited = lot.data('last-edited');
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const editor = lot.data('editor');
            const nameWithTooltip = showNameWithTooltip(editor.split(' ').slice(0, 1), editor);

            lot.html(`<button type="button" class="btn btn-link text-primary draftid-cancel lotbtn">
                   <i class="fa fa-pen" aria-hidden="true"></i>
                   </button>
                   ${value}
                   ${nameWithTooltip} ${lastEditedText}
                   `);

            lot.removeClass('lot-editable');
            lot.addClass('lot');
        }
    });

    $('#items').click('.lot-editable .lot-apply', function(event) {
        if ($(event.target).hasClass('lot-apply')) {
            const btnApply = $(event.target).closest('.lot-apply');

            if (!btnApply) return;

            const lot = btnApply.closest('td');
            const lotInput = lot.find('.lot-input');
            const datetime = btnApply.closest('tr').find('.datetime');
            const sku = btnApply.closest('tr').find('.sku');
            const listingId = btnApply.closest('tr').find('.listing_id');

            if (!datetime || !lotInput || !sku || !listingId) return;

            const value = lotInput.val();

            lot.removeClass('moved-editable');
            lot.addClass('moved');
            
            const data = {'data': {
                'request': value,
                'datetime': datetime.text(),
                'sku': sku.text(),
                'listing_id': listingId.text(),
                'col': 'lot'
            }};

            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
    });
    
    /////request
    $('#items').on('click', '.request', function(event){
        if(event.target.className == "request"){
            const notes = $(event.target).closest('.request');

            if (!notes) return;

            const value = notes.data('value');

            //adddropdownrequest(notes, value, 'request');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel requestbtn"){
            const notes = $(event.target).closest('.request');

            if (!notes) return;

            const value = notes.data('value');

            adddropdownrequest(notes, value, 'request');
        }else if(event.target.className == "fa fa-pen requestpenbtn"){
            const notes = $(event.target).closest('.request');

            if (!notes) return;

            const value = notes.data('value');

            adddropdownrequest(notes, value, 'request');
        }
   });


   $('#items').click('.request-editable .request-cancel', function(event) {
       const btnCancel = $(event.target).closest('.request-cancel');
       if (!btnCancel) return;

       const notesCell = btnCancel.closest('.request-editable');
       let editor = notesCell.data('editor');
       if (!editor) editor = '';
       let lastEdited = notesCell.data('lastEdited');
       if (!lastEdited) lastEdited = '';
       const firstname = editor.split(' ').slice(0, 1);
       const nameWithTooltip = showNameWithTooltip(firstname, editor);
       const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
       
       notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel requestbtn"><i class="fa fa-pen requestpenbtn" aria-hidden="true"></i></button>${notesCell.data('value')}${nameWithTooltip} ${lastEditedText}`);
       notesCell.removeClass('request-editable');
       notesCell.addClass('request');
   });


    $('#items').click('.request-editable .request-apply', function(event) {
        const btnApply = $(event.target).closest('.request-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.request-editable');
        const listing_id = btnApply.closest('tr').find('.listing_id');
        const par = btnApply.closest('tr').find('.par');
        const datetime = btnApply.closest('tr').find('.datetime');
        const request = btnApply.closest('tr').find('.request');
        const sku = btnApply.closest('tr').find('.sku');
        
        const textField = notes.find('.request-input');
        if (!textField) return;
        if (!textField.val()) return;

        let editor = notes.data('editor');
        if (!editor) editor = '';
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);

        const selectedAction = textField.find(':selected').text().trim();
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel requestbtn"><i class="fa fa-pen requestpenbtn" aria-hidden="true"></i></button>${nameWithTooltip} ${selectedAction}`);
        notes.data('value', selectedAction);
        notes.removeClass('request-editable');
        notes.addClass('request');

        if ((textField.val() && textField.val().length > 0)) {
            const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listing_id.text(), 'col': "request"} };

            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
        //window.location.href = `${baseUrl}index.php/task`;
    });
    /////request
    ///////price
    $('#items').on('click', '.price', function(event) {
        if(event.target.className == "price"){
            const notes = $(event.target).closest('.price');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel pricebtn"){
            const notes = $(event.target).closest('.price');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldNumber(notes, value, 'price');
        }else if(event.target.className == "fa fa-pen pricepenbtn"){
            const notes = $(event.target).closest('.price');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldNumber(notes, value, 'price');
        }
    });
    function addEditableFieldNumber(cell, value, className) {
        const input = $(`<input type="number" class="form-control ${className}-input w-auto" value="${value}">
                          step="any"`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#items').click('.price-editable .price-cancel', function(event) {
        const btnCancel = $(event.target).closest('.price-cancel');
        if (!btnCancel) return;

        const newPriceCell = btnCancel.closest('.price-editable');
        let editor = newPriceCell.data('editor');
        if (!editor) editor = '';
        let lastEdited = newPriceCell.data('lastEdited');
        if (!lastEdited) lastEdited = '';
        if (newPriceCell.data('value') == 0) {
            lastEdited = '';
            editor = '';
        }
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);
        const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;

        const formattedPrice = formatPrice(newPriceCell.data('value'));
        newPriceCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel pricebtn"><i class="fa fa-pen pricepenbtn" aria-hidden="true"></i></button>${formattedPrice}${nameWithTooltip} ${lastEditedText}`);
        newPriceCell.removeClass('price-editable');
        newPriceCell.addClass('price');
    });
    $('#items').click('.price-editable .price-apply', function(event) {
        const btnApply = $(event.target).closest('.price-apply');
        if (!btnApply) return;
    
        const newPrice = btnApply.closest('.price-editable');
        const textField = newPrice.find('.price-input');
        if (!textField) return;
        if (!textField.val()) return;

        const listing_id = btnApply.closest('tr').find('.listing_id');
        const par = btnApply.closest('tr').find('.par');
        const datetime = btnApply.closest('tr').find('.datetime');
        const request = btnApply.closest('tr').find('.request');
        const sku = btnApply.closest('tr').find('.sku');

        if (textField.val() && textField.val().match(/[^$,.\d]/)) return;

        let editor = newPrice.data('editor');
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);

        const formattedPrice = formatPrice(textField.val());
        newPrice.html(`<button type="button" class="btn btn-link text-primary draftid-cancel pricebtn"><i class="fa fa-pen pricepenbtn" aria-hidden="true"></i></button>${nameWithTooltip} ${formattedPrice}`);
        newPrice.data('value', textField.val());
        newPrice.removeClass('price-editable');
        newPrice.addClass('price');
    
        if ((textField.val() && textField.val().length > 0)) {
            const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "price"} };
    
            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ///////price

    // promotion
    $('#items').on('click', '.promotion', function(event) {
        if ($(event.target).hasClass('promotionbtn')) {
            const promotion = $(event.target).closest('.promotion');

            if (!promotion) return;

            const value = promotion.data('value');

            addEditableFieldPercentage(promotion, value, 'promotion');
        }
    });

    $('#items').click('.promotion-editable .promotion-cancel', function(event) {
        if ($(event.target).hasClass('promotion-cancel')) {
            const promotionEditable = $(event.target).closest('.promotion-editable');

            if (!promotionEditable) return;

            let editor = promotionEditable.data('editor');
            if (!editor) editor = '';
            let lastEdited = promotionEditable.data('lastEdited');
            if (!lastEdited) lastEdited = '';

            if (promotionEditable.data('value') == '') {
                lastEdited = '';
                editor = '';
            }

            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const value = `${promotionEditable.data('value')}`;
            const valueDisplay = !isNaN(value)|| value.trim() != '' ? `${value}%` : value;

            promotionEditable.html(`<button type="button" class="btn btn-link text-primary promotionbtn"><i class="fa fa-pen promotionbtn" aria-hidden="true"></i></button>${valueDisplay}${nameWithTooltip} ${lastEditedText}`);
            promotionEditable.removeClass('promotion-editable');
            promotionEditable.addClass('promotion');
        }
    });

    $('#items').click('.promotion-editable .promotion-apply', function(event) {
        if ($(event.target).hasClass('promotion-apply')) {
            const promotionEditable = $(event.target).closest('.promotion-editable');

            if (!promotionEditable) return;

            const textField = promotionEditable.find('.promotion-input');
            if (!textField) return;
            if (!textField.val()) return;

            const listing_id = promotionEditable.closest('tr').find('.listing_id');
            const par = promotionEditable.closest('tr').find('.par');
            const datetime = promotionEditable.closest('tr').find('.datetime');
            const request = promotionEditable.closest('tr').find('.request');
            const sku = promotionEditable.closest('tr').find('.sku');

            const editor = promotionEditable.data('editor');
            const firstname = editor.split(' ').slice(0, 1);
            const lastEdited = promotionEditable.data('lastEdited');
            const nameWithTooltip = showNameWithTooltip(firstname, editor);

            const value = `${textField.val()}`;
            const valueDisplay = !isNaN(value) ? `${value}%` : value;
            promotionEditable.html(`<button type="button" class="btn btn-link text-primary promotionbtn"><i class="fa fa-pen promotionbtn" aria-hidden="true"></i></button>${valueDisplay}${nameWithTooltip} ${lastEdited}`);
            promotionEditable.data('value', textField.val());
            promotionEditable.removeClass('promotion-editable');
            promotionEditable.addClass('promotion');
            
            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "promotion"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
        }
    }); // end promotion

    // volume discount
    $('#items').on('click', '.volume-discount-2x', function(event) {
        if ($(event.target).hasClass('volumediscount2xbtn')) {
            const volumeDiscount2x = $(event.target).closest('.volume-discount-2x');

            if (!volumeDiscount2x) return;

            const value = volumeDiscount2x.data('value');

            addEditableFieldPercentage(volumeDiscount2x, value, 'volume-discount-2x', 'number');
        }
    });

    $('#items').click('.volume-discount-2x-editable .volume-discount-2x-cancel', function(event) {
        if ($(event.target).hasClass('volume-discount-2x-cancel')) {
            const volumeDiscountEditable = $(event.target).closest('.volume-discount-2x-editable');

            if (!volumeDiscountEditable) return;

            let editor = volumeDiscountEditable.data('editor');
            if (!editor) editor = '';
            let lastEdited = volumeDiscountEditable.data('lastEdited');
            if (!lastEdited) lastEdited = '';

            if (volumeDiscountEditable.data('value') == '') {
                lastEdited = '';
                editor = '';
            }

            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const value = `${volumeDiscountEditable.data('value')}`;
            const valueDisplay = !isNaN(value) && value.trim() != '' ? `${value}%` : 'None';

            volumeDiscountEditable.html(`<button type="button" class="btn btn-link text-primary volumediscount2xbtn"><i class="fa fa-pen volumediscount2xbtn" aria-hidden="true"></i></button>${valueDisplay}${nameWithTooltip} ${lastEditedText}`);
            volumeDiscountEditable.removeClass('volume-discount-2x-editable');
            volumeDiscountEditable.addClass('volume-discount-2x');
        }
    });

    $('#items').click('.volume-discount-2x-editable .volume-discount-2x-apply', function(event) {
        if ($(event.target).hasClass('volume-discount-2x-apply')) {
            const volumeDiscountEditable = $(event.target).closest('.volume-discount-2x-editable');

            if (!volumeDiscountEditable) return;

            const textField = volumeDiscountEditable.find('.volume-discount-2x-input');
            if (!textField) return;
            if (!textField.val()) return;

            const listing_id = volumeDiscountEditable.closest('tr').find('.listing_id');
            const par = volumeDiscountEditable.closest('tr').find('.par');
            const datetime = volumeDiscountEditable.closest('tr').find('.datetime');
            const request = volumeDiscountEditable.closest('tr').find('.request');
            const sku = volumeDiscountEditable.closest('tr').find('.sku');

            const editor = volumeDiscountEditable.data('editor');
            const firstname = editor.split(' ').slice(0, 1);
            const lastEdited = volumeDiscountEditable.data('lastEdited');
            const nameWithTooltip = showNameWithTooltip(firstname, editor);

            const value = `${textField.val()}`;
            const valueDisplay = !isNaN(value) ? `${value}%` : value;
            volumeDiscountEditable.html('<button type="button" class="btn btn-link text-primary volumediscount2xbtn">'
                                        + '<i class="fa fa-pen volumediscount2xbtn" aria-hidden="true"></i></button>'
                                        + `${valueDisplay}${nameWithTooltip} ${lastEdited}`);
            volumeDiscountEditable.data('value', textField.val());
            volumeDiscountEditable.removeClass('volume-discount-2x-editable');
            volumeDiscountEditable.addClass('volume-discount-2x');
            
            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "volume_discount_2x"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
        }
    });
    
    $('#items').on('click', '.volume-discount-4x', function(event) {
        if ($(event.target).hasClass('volumediscount4xbtn')) {
            const volumeDiscount4x = $(event.target).closest('.volume-discount-4x');

            if (!volumeDiscount4x) return;

            const value = volumeDiscount4x.data('value');

            addEditableFieldPercentage(volumeDiscount4x, value, 'volume-discount-4x', 'number');
        }
    });

    $('#items').click('.volume-discount-4x-editable .volume-discount-4x-cancel', function(event) {
        if ($(event.target).hasClass('volume-discount-4x-cancel')) {
            const volumeDiscountEditable = $(event.target).closest('.volume-discount-4x-editable');

            if (!volumeDiscountEditable) return;

            let editor = volumeDiscountEditable.data('editor');
            if (!editor) editor = '';
            let lastEdited = volumeDiscountEditable.data('lastEdited');
            if (!lastEdited) lastEdited = '';

            if (volumeDiscountEditable.data('value') == '') {
                lastEdited = '';
                editor = '';
            }

            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const value = `${volumeDiscountEditable.data('value')}` ;
            const valueDisplay = !isNaN(value) && value.trim() != '' ? `${value}%` : 'None';

            volumeDiscountEditable.html(`<button type="button" class="btn btn-link text-primary volumediscount4xbtn"><i class="fa fa-pen volumediscount4xbtn" aria-hidden="true"></i></button>${valueDisplay}${nameWithTooltip} ${lastEditedText}`);
            volumeDiscountEditable.removeClass('volume-discount-4x-editable');
            volumeDiscountEditable.addClass('volume-discount-4x');
        }
    });

    $('#items').click('.volume-discount-4x-editable .volume-discount-4x-apply', function(event) {
        if ($(event.target).hasClass('volume-discount-4x-apply')) {
            const volumeDiscountEditable = $(event.target).closest('.volume-discount-4x-editable');

            if (!volumeDiscountEditable) return;

            const textField = volumeDiscountEditable.find('.volume-discount-4x-input');
            if (!textField) return;
            if (!textField.val()) return;

            const listing_id = volumeDiscountEditable.closest('tr').find('.listing_id');
            const par = volumeDiscountEditable.closest('tr').find('.par');
            const datetime = volumeDiscountEditable.closest('tr').find('.datetime');
            const request = volumeDiscountEditable.closest('tr').find('.request');
            const sku = volumeDiscountEditable.closest('tr').find('.sku');

            const editor = volumeDiscountEditable.data('editor');
            const firstname = editor.split(' ').slice(0, 1);
            const lastEdited = volumeDiscountEditable.data('lastEdited');
            const nameWithTooltip = showNameWithTooltip(firstname, editor);

            const value = `${textField.val()}`;
            const valueDisplay = !isNaN(value) ? `${value}%` : value;
            volumeDiscountEditable.html('<button type="button" class="btn btn-link text-primary volumediscount4xbtn">'
                                        + '<i class="fa fa-pen volumediscount4xbtn" aria-hidden="true"></i></button>'
                                        + `${valueDisplay}${nameWithTooltip} ${lastEdited}`);
            volumeDiscountEditable.data('value', textField.val());
            volumeDiscountEditable.removeClass('volume-discount-4x-editable');
            volumeDiscountEditable.addClass('volume-discount-4x');
            
            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "volume_discount_4x"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
        }
    });

    $('#items').on('click', '.volume-discount-3x', function(event) {
        if ($(event.target).hasClass('volumediscount3xbtn')) {
            const volumeDiscount3x = $(event.target).closest('.volume-discount-3x');

            if (!volumeDiscount3x) return;

            const value = volumeDiscount3x.data('value');

            addEditableFieldPercentage(volumeDiscount3x, value, 'volume-discount-3x', 'number');
        }
    });

    $('#items').click('.volume-discount-3x-editable .volume-discount-3x-cancel', function(event) {
        if ($(event.target).hasClass('volume-discount-3x-cancel')) {
            const volumeDiscountEditable = $(event.target).closest('.volume-discount-3x-editable');

            if (!volumeDiscountEditable) return;

            let editor = volumeDiscountEditable.data('editor');
            if (!editor) editor = '';
            let lastEdited = volumeDiscountEditable.data('lastEdited');
            if (!lastEdited) lastEdited = '';

            if (volumeDiscountEditable.data('value') == '') {
                lastEdited = '';
                editor = '';
            }

            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            const value = `${volumeDiscountEditable.data('value')}` ;
            const valueDisplay = !isNaN(value) && value.trim() != '' ? `${value}%` : 'None';

            volumeDiscountEditable.html(`<button type="button" class="btn btn-link text-primary volumediscount3xbtn"><i class="fa fa-pen volumediscount3xbtn" aria-hidden="true"></i></button>${valueDisplay}${nameWithTooltip} ${lastEditedText}`);
            volumeDiscountEditable.removeClass('volume-discount-3x-editable');
            volumeDiscountEditable.addClass('volume-discount-3x');
        }
    });

    $('#items').click('.volume-discount-3x-editable .volume-discount-3x-apply', function(event) {
        if ($(event.target).hasClass('volume-discount-3x-apply')) {
            const volumeDiscountEditable = $(event.target).closest('.volume-discount-3x-editable');

            if (!volumeDiscountEditable) return;

            const textField = volumeDiscountEditable.find('.volume-discount-3x-input');
            if (!textField) return;
            if (!textField.val()) return;

            const listing_id = volumeDiscountEditable.closest('tr').find('.listing_id');
            const par = volumeDiscountEditable.closest('tr').find('.par');
            const datetime = volumeDiscountEditable.closest('tr').find('.datetime');
            const request = volumeDiscountEditable.closest('tr').find('.request');
            const sku = volumeDiscountEditable.closest('tr').find('.sku');

            const editor = volumeDiscountEditable.data('editor');
            const firstname = editor.split(' ').slice(0, 1);
            const lastEdited = volumeDiscountEditable.data('lastEdited');
            const nameWithTooltip = showNameWithTooltip(firstname, editor);

            const value = `${textField.val()}`;
            const valueDisplay = !isNaN(value) ? `${value}%` : value;
            volumeDiscountEditable.html('<button type="button" class="btn btn-link text-primary volumediscount3xbtn">'
                                        + '<i class="fa fa-pen volumediscount3xbtn" aria-hidden="true"></i></button>'
                                        + `${valueDisplay}${nameWithTooltip} ${lastEdited}`);
            volumeDiscountEditable.data('value', textField.val());
            volumeDiscountEditable.removeClass('volume-discount-3x-editable');
            volumeDiscountEditable.addClass('volume-discount-3x');
            
            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "volume_discount_3x"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
        }
    }); // end volume discount

    /////////shipping
     $('#items').on('click', '.shipping', function(event) {
        if(event.target.className == "shipping"){
            const notes = $(event.target).closest('.shipping');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel shippingbtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldshipping(notes, value, 'shipping');
        }else if(event.target.className == "fa fa-pen shippingpenbtn"){
            const notes = $(event.target).closest('.shipping');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldshipping(notes, value, 'shipping');
        }
    });

    function addEditableFieldshipping(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }

    $('#items').click('.shipping-editable .shipping-cancel', function(event) {
        const btnCancel = $(event.target).closest('.shipping-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.shipping-editable');
        let editor = notesCell.data('editor');
        if (!editor) editor = '';
        let lastEdited = notesCell.data('lastEdited');
        if (!lastEdited) lastEdited = '';
        if (notesCell.data('value') == 0) {
            lastEdited = '';
            editor = '';
        }
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);
        const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
    
        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel notebtn"><i class="fa fa-pen shippingpenbtn" aria-hidden="true"></i></button>${notesCell.data('value')}${nameWithTooltip} ${lastEditedText}`);
        notesCell.removeClass('shipping-editable');
        notesCell.addClass('shipping');
    });
    $('#items').click('.shipping-editable .shipping-apply', function(event) {
        const btnApply = $(event.target).closest('.shipping-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.shipping-editable');
        const listing_id = btnApply.closest('tr').find('.listing_id');
        const par = btnApply.closest('tr').find('.par');
        const datetime = btnApply.closest('tr').find('.datetime');
        const request = btnApply.closest('tr').find('.request');
        const sku = btnApply.closest('tr').find('.sku');
        
        let editor = notes.data('editor');
        if (!editor) editor = '';
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);
    
        const textField = notes.find('.shipping-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel shippingbtn"><i class="fa fa-pen shippingpenbtn" aria-hidden="true"></i></button>${nameWithTooltip} ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('shipping-editable');
        notes.addClass('shipping');
    
        if ((textField.val() && textField.val().length > 0)) {
            const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "shipping"} };

            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
        //window.location.href = `${baseUrl}index.php/task`;
    });
    /////shipping


    /////////////
    //////////////note
    $('#items').on('click', '.weight', function(event) {
            const weight = $(event.target).closest('.weight');
    
            if (!weight) return;
    
            const value = weight.data('value');
    
            addEditableFieldNumber(weight, value, 'weight');
    });

    $('#items').click('.weight-editable .weight-cancel', function(event) {
        const btnCancel = $(event.target).closest('.weight-cancel');
        if (!btnCancel) return;

        const weight = btnCancel.closest('tr').find('.weight-editable');
        if (!weight) return;
        
        let editor = weight.data('editor');
        if (!editor) editor = '';

        let lastEdited = weight.data('lastEdited');
        if (!lastEdited) lastEdited = '';

        if (weight.data('value') == 0) {
            lastEdited = '';
            editor = '';
        }

        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);
        const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
        const weightDisplay = weight.data('value') == 0 ? '' : weight.data('value');

        weight.html('<button type="button" class="btn btn-link text-primary draftid-cancel penbtn">' +
                    '<i class="fa fa-pen penbtn" aria-hidden="true"></i>' +
                    '</button>' +
                    `${weightDisplay} lbs${nameWithTooltip} ${lastEditedText}`);
        weight.removeClass('weight-editable');
        weight.addClass('weight');
    });

    $('#items').click('.weight-editable .weight-apply', function(event) {
        const btnApply = $(event.target).closest('.weight-apply');
        if (!btnApply) return;

        const weight = btnApply.closest('.weight-editable');
        const sku = btnApply.closest('tr').find('.sku');
        let editor = weight.data('editor');
        if (!editor) editor = '';
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);

        const textField = weight.find('.weight-input');
        if (!textField) return;
        if (!textField.val()) return;

        if (textField.val() && textField.val().match(/[^$,.\d]/)) return;

        const inputWeight = textField.val();
        weight.html('<button type="button" class="btn btn-link text-primary draftid-cancel penbtn">' +
                    '<i class="fa fa-pen penbtn" aria-hidden="true"></i>' +
                    '</button>' +
                    `${inputWeight} KG${nameWithTooltip}`);
        weight.data('value', textField.val());
        weight.removeClass('weight-editable');
        weight.addClass('weight');

        const datetime = weight.closest('tr').find('.datetime');
        const listing_id = weight.closest('tr').find('.listing_id');

        if ((textField.val() && textField.val().length > 0)) {
            const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.val(),
                                   'listing_id': listing_id.text(), 'col': "weight"} };
    
            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
    });

    $('#items').on('click', '.price', function(event) {
        if(event.target.className == "price"){
            const notes = $(event.target).closest('.price');
            
            if (!notes) return;
            
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel pricebtn"){
            const notes = $(event.target).closest('.price');

            if (!notes) return;
            
            const value = notes.data('value');
            
            addEditableFieldNumber(notes, value, 'price');
        }else if(event.target.className == "fa fa-pen pricepenbtn"){
            const notes = $(event.target).closest('.price');

            if (!notes) return;
            
            const value = notes.data('value');
            
            addEditableFieldNumber(notes, value, 'price');
        }
    });

    function addEditableFieldnote(cell, value, className) {
        const input = $(`<textarea type="text" class="form-control ${className}-input" rows="5" cols="200">` +
                        `${value.replaceAll('<br>', '&#10;')}</textarea>`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(`<i class="fa fa-check ${className}-apply" aria-hidden="true"></i>`);

        const btnCancel = $(`<button type="button" class="btn btn-link text-primary ${className}-cancel"></button>`);
        btnCancel.html(`<i class="fa fa-times ${className}-cancel" aria-hidden="true"></i>`);

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#items').click('.notes-editable .notes-cancel', function(event) {
        if ($(event.target).hasClass('notes-cancel')) {
            const btnCancel = $(event.target).closest('.notes-cancel');
            if (!btnCancel) return;
            
            const notesCell = btnCancel.closest('.notes-editable');
            let editor = notesCell.data('editor');
            if (!editor) editor = '';
            let lastEdited = notesCell.data('lastEdited');
            if (!lastEdited) lastEdited = '';
            if (notesCell.data('value') == 0) {
                lastEdited = '';
                editor = '';
            }
            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
            
            notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel notebtn"><i class="fa fa-pen notepenbtn" aria-hidden="true"></i></button>${notesCell.data('value')}${nameWithTooltip} ${lastEditedText}`);
            notesCell.removeClass('notes-editable');
            notesCell.addClass('notes');
        }
    });

    
    $('#items').click('.notes-editable .notes-apply', function(event) {
        if ($(event.target).hasClass('notes-apply')) { 
            const btnApply = $(event.target).closest('.notes-apply');
            if (!btnApply) return;
            
            const notes = btnApply.closest('.notes-editable');
            const listing_id = btnApply.closest('tr').find('.listing_id');
            const par = btnApply.closest('tr').find('.par');
            const datetime = btnApply.closest('tr').find('.datetime');
            const request = btnApply.closest('tr').find('.request');
            const sku = btnApply.closest('tr').find('.sku');
            let editor = notes.data('editor');
            if (!editor) editor = '';
            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            
            const textField = notes.find('.notes-input');
            if (!textField) return;
            if (!textField.val()) return;

            notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel notebtn"><i class="fa fa-pen notepenbtn" aria-hidden="true"></i></button>${textField.val().replaceAll('\n', '<br>')}<br>${nameWithTooltip} `);
            notes.data('value', textField.val());
            notes.removeClass('notes-editable');
            notes.addClass('notes');
            
            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "notes"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
            //window.location.href = `${baseUrl}index.php/task`;
        }
    });
    /////////////note

/////////////banner
    $('#items').on('click', '.banner', function(event) {
        if(event.target.className == "banner"){
            const notes = $(event.target).closest('.banner');

        if (!notes) return;

        const value = notes.data('value');

        //adddropdownrequest(notes, value, 'request');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel bannerbtn"){
            const notes = $(event.target).closest('.banner');

        if (!notes) return;

        const value = notes.data('value');

        adddropdownbanner(notes, value, 'banner');
        }else if(event.target.className == "fa fa-pen bannerpenbtn"){
            const notes = $(event.target).closest('.banner');

        if (!notes) return;

        const value = notes.data('value');

        adddropdownbanner(notes, value, 'banner');
        }
   });


    function adddropdownbanner(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input w-auto">
                     <option value="YES">YES</option>
                     <option value="NO">NO</option>
                     </select>`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }


   $('#items').click('.banner-editable .banner-cancel', function(event) {
       const btnCancel = $(event.target).closest('.banner-cancel');
       if (!btnCancel) return;

       const notesCell = btnCancel.closest('.banner-editable');
       let editor = notesCell.data('editor');
       if (!editor) editor = '';
       let lastEdited = notesCell.data('lastEdited');
       if (!lastEdited) lastEdited = '';
       const firstname = editor.split(' ').slice(0, 1);
       const nameWithTooltip = showNameWithTooltip(firstname, editor);
       const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;

       notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel bannertbtn"><i class="fa fa-pen bannerpenbtn" aria-hidden="true"></i></button>${notesCell.data('value')}${nameWithTooltip} ${lastEditedText}`);
       notesCell.removeClass('banner-editable');
       notesCell.addClass('banner');
   });


    $('#items').click('.banner-editable .banner-apply', function(event) {
        const btnApply = $(event.target).closest('.banner-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.banner-editable');
        const listing_id = btnApply.closest('tr').find('.listing_id');
        const par = btnApply.closest('tr').find('.par');
        const datetime = btnApply.closest('tr').find('.datetime');
        const request = btnApply.closest('tr').find('.request');
        const sku = btnApply.closest('tr').find('.sku');
        let editor = notes.data('editor');
        if (!editor) editor = '';
        const firstname = editor.split(' ').slice(0, 1);
        const nameWithTooltip = showNameWithTooltip(firstname, editor);

        const textField = notes.find('.banner-input');
        if (!textField) return;
        if (!textField.val()) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel bannerbtn"><i class="fa fa-pen bannerpenbtn" aria-hidden="true"></i></button>${nameWithTooltip} ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('banner-editable');
        notes.addClass('banner');

        if ((textField.val() && textField.val().length > 0)) {
            const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listing_id.text(), 'col': "banner"} };

            $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                loadListing();
            });
        }
        //window.location.href = `${baseUrl}index.php/task`;
    });


    $('#items').on('click', '.qty3', function(event) {
        if(event.target.className == "qty3"){
            const notes = $(event.target).closest('.qty3');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel qty3btn"){
            const notes = $(event.target).closest('.qty3');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldqty3(notes, value, 'qty3');
        }else if(event.target.className == "fa fa-pen qty3btn"){
            const notes = $(event.target).closest('.qty3');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldqty3(notes, value, 'qty3');
        }
    });


    function addEditableFieldqty3(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-primary ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }


    $('#items').click('.qty3-editable .qty3-cancel', function(event) {
        const btnCancel = $(event.target).closest('.qty3-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.qty3-editable');


        const cell = btnCancel.closest('.qty3-editable');
            let editor = cell.data('editor');
            if (!editor) editor = '';
            let lastEdited = cell.data('lastEdited');
            if (!lastEdited) lastEdited = '';
            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qty3btn"><i class="fa fa-pen qty3penbtn" aria-hidden="true"></i></button>
        ${cell.data('value')}${nameWithTooltip} ${lastEditedText}`);
        notesCell.removeClass('qty3-editable');
        notesCell.addClass('qty3');
    });


    $('#items').click('.qty3-editable .qty3-apply', function(event) {
        const btnApply = $(event.target).closest('.qty3-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.qty3-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const listingId = btnApply.closest('tr').find('.listing_id');
        const datetime = btnApply.closest('tr').find('.datetime');
        
    
        const textField = notes.find('.qty3-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qty3btn"><i class="fa fa-pen qty3penbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('qty3-editable');
        notes.addClass('qty3');
    
       
        const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listingId.text(), 'col': "qty_3"} };
                

        $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
            loadListing();
        });
        
    });

    


/////////banner
    /////////////
    $('#items').on('click', '.draftid', function(event) {
        if(event.target.className == "draftid"){
            const notes = $(event.target).closest('.draftid');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel draft"){
            const notes = $(event.target).closest('.draftid');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldraftid(notes, value, 'draftid');
        }else if(event.target.className == "fa fa-pen draftpen"){
            const notes = $(event.target).closest('.draftid');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddraftid(notes, value, 'draftid');
        }
    });


    function addEditableFielddraftid(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-primary ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }


    $('#items').click('.draftid-editable .draftid-cancel', function(event) {
        const btnCancel = $(event.target).closest('.draftid-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.draftid-editable');


        const cell = btnCancel.closest('.draftid-editable');
            let editor = cell.data('editor');
            if (!editor) editor = '';
            let lastEdited = cell.data('lastEdited');
            if (!lastEdited) lastEdited = '';
            const firstname = editor.split(' ').slice(0, 1);
            const nameWithTooltip = showNameWithTooltip(firstname, editor);
            const lastEditedText = lastEdited === '' ? '' : `${lastEdited}`;
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel draftbtn"><i class="fa fa-pen draftpen" aria-hidden="true"></i></button>
        ${cell.data('value')}${nameWithTooltip} ${lastEditedText}`);
        notesCell.removeClass('draftid-editable');
        notesCell.addClass('draftid');
    });


    $('#items').click('.draftid-editable .draftid-apply', function(event) {
        const btnApply = $(event.target).closest('.draftid-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.draftid-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const listingId = btnApply.closest('tr').find('.listing_id');
        const datetime = btnApply.closest('tr').find('.datetime');
        
    
        const textField = notes.find('.draftid-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel draftbtn"><i class="fa fa-pen draftpen" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('draftid-editable');
        notes.addClass('draftid');
    
       
        const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listingId.text(), 'col': "draft_id"} };
                

        $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
            loadListing();
        });
        
    });

    
    $('#items').on('click', '.listingitemid', function(event) {
        
        if(event.target.className == "listingitemid"){
            const notes = $(event.target).closest('.listingitemid');

            if (!notes) return;
    
            const value = notes.data('value');
    
            //addEditableField(notes, value, 'listingitemid');
        }else if(event.target.className == "fa fa-pen listingpen"){
            const notes = $(event.target).closest('.listingitemid');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'listingitemid');
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel listing"){
            const notes = $(event.target).closest('.listingitemid');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'listingitemid');
        }
    });
    $('#items').click('.listingitemid-editable .listingitemid-apply', function(event) {
        if ($(event.target).hasClass('listingitemid-apply')) {
            const btnApply = $(event.target).closest('.listingitemid-apply');
            if (!btnApply) return;

            const notes = btnApply.closest('.listingitemid-editable');
            const listing_id = btnApply.closest('tr').find('.listing_id');
            const par = btnApply.closest('tr').find('.par');
            const datetime = btnApply.closest('tr').find('.datetime');
            const request = btnApply.closest('tr').find('.request');
            const sku = btnApply.closest('tr').find('.sku');
            
            const listingby = btnApply.closest('tr').find('.listingby').data('value');
            const firstname = showNameWithTooltip(listingby.split(' ').slice(0, 1), listingby);
            const listingdate = btnApply.closest('tr').find('.listingdate').data('value');

            const textField = notes.find('.listingitemid-input');
            if (!textField) return;
            //if (!textField.val()) return;

            notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel listing"><i class="fa fa-pen listingpen" aria-hidden="true"></i></button>
        <a href='https://www.ebay.com/itm/${textField.val()}' target='_blank'>${textField.val()}</a></br>${firstname} ${listingdate}`);
            notes.data('value', textField.val());
            notes.removeClass('listingitemid-editable');
            notes.addClass('listingitemid');

            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listing_id.text(), 'col': "listing_item_id"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }else{
                const data = {'data': {'request': '0', 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listing_id.text(), 'col': "listing_item_id"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
            // window.location.href = `${baseUrl}index.php/task`;
        }
    });
    $('#items').click('.listingitemid-editable .listingitemid-cancel', function(event) {
        if ($(event.target).hasClass('listingitemid-cancel')) {
            const btnCancel = $(event.target).closest('.listingitemid-cancel');
            if (!btnCancel) return;

            const notesCell = btnCancel.closest('.listingitemid-editable');
            const listingby = btnCancel.closest('tr').find('.listingby').data('value');
            const firstname = showNameWithTooltip(listingby.split(' ').slice(0, 1));
            const listingdate = btnCancel.closest('tr').find('.listingdate').data('value');;

            notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel listing"><i class="fa fa-pen listingpen" aria-hidden="true"></i></button><a href='https://www.ebay.com/itm/${notesCell.data('value')}' target='_blank'>${notesCell.data('value')}</a><br>${firstname} ${listingdate}`);
            notesCell.removeClass('listingitemid-editable');
            notesCell.addClass('listingitemid');
        }
    });
    $('#items').click('.draftid-editable .draftid-apply', function(event) {
        if ($(event.target).hasClass('draftid-apply')) {
            const btnApply = $(event.target).closest('.draftid-apply');
            if (!btnApply) return;

            const notes = btnApply.closest('.draftid-editable');

            const listing_id = btnApply.closest('tr').find('.listing_id');
            const par = btnApply.closest('tr').find('.par');
            const datetime = btnApply.closest('tr').find('.datetime');
            const request = btnApply.closest('tr').find('.request');
            const sku = btnApply.closest('tr').find('.sku');
            
            const draftdate = btnApply.closest('tr').find('.draftdate');
            const draftby = btnApply.closest('tr').find('.draftby');
            

            const textField = notes.find('.draftid-input');
            if (!textField) return;
            //if (!textField.val()) return;

            notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel draft"><i class="fa fa-pen draftpen" aria-hidden="true"></i></button>
        <a href='https://bulksell.ebay.com/ws/eBayISAPI.dll?SingleList&sellingMode=SellLikeItem&draftId=${textField.val()}' target='_blank'>${textField.val()}</a> </br> ${draftby.text() +"</br>"+ draftdate.text()}`);
            notes.data('value', textField.val());
            notes.removeClass('draftid-editable');
            notes.addClass('draftid');

            if ((textField.val() && textField.val().length > 0)) {
                const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listing_id.text(), 'col': "draft_id"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }else{
                const data = {'data': {'request': '0', 'datetime': datetime.text(), 'sku': sku.text(), 'listing_id': listing_id.text(), 'col': "draft_id"} };
                
                $.post(`${baseUrl}index.php/task/request/update_columns`, data, function() {
                    loadListing();
                });
            }
            // window.location.href = `${baseUrl}index.php/task`;
        }
    });

    function addEditableFielddraftid(cell, value, className) {
        /*let str = `${value}`;
const myArr = str.split("/");
var itemdata = myArr[0];
var datedata = myArr[1];*/
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html(`<i class="fa fa-times ${className}-cancel" aria-hidden="true"></i>`);

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }

    $('#items').click('.draftid-editable .draftid-cancel', function(event) {
        if ($(event.target).hasClass('draftid-cancel')) {
            const btnCancel = $(event.target).closest('.draftid-cancel');
            if (!btnCancel) return;

            const notesCell = btnCancel.closest('.draftid-editable');
            const draftdate = btnCancel.closest('tr').find('.draftdate');
            const date = draftdate.data('value');
            const draftby = btnCancel.closest('tr').find('.draftby').data('value');
            const firstname = showNameWithTooltip(draftby?.split(' ').slice(0, 1), draftby);

            notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel draft"><i class="fa fa-pen draftpen" aria-hidden="true"></i></button><a href='https://bulksell.ebay.com/ws/eBayISAPI.dll?SingleList&sellingMode=SellLikeItem&draftId=${notesCell.data('value')}' target='_blank'>${notesCell.data('value')}</a> ${firstname} ${date}`);
            notesCell.removeClass('draftid-editable');
            notesCell.addClass('draftid');
        }
    });
    
    $('#items').click('.datetimes-editable .datetimes-apply', function(event) {
        const btnApply = $(event.target).closest('.datetimes-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.datetimes-editable');
        const par = btnApply.closest('tr').find('.par');
        const datetime = btnApply.closest('tr').find('.datetimes');

        const textField = notes.find('.datetimes-input');
        if (!textField) return;
        if (!textField.val()) return;

        notes.html(textField.val());
        notes.data('value', textField.val());
        notes.removeClass('datetimes-editable');
        notes.addClass('datetimes');

        if ((textField.val() && textField.val().length > 0)) {
            const data = { 'par': par.text(), 'notes': textField.val(), 'datetimes': datetime.text() };

            //$.post(`${baseUrl}index.php/task/request/update_listings`, data);
        }
    });
    $('#items').click('.datetimes-editable .datetimes-cancel', function(event) {
        const btnCancel = $(event.target).closest('.datetimes-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.datetimes-editable');

        notesCell.html(notesCell.data('value'));
        notesCell.removeClass('datetimes-editable');
        notesCell.addClass('datetimes');
    });

    $('#items').on('click', '.btn-archive', function() {
        const btn = $(this).closest('.btn-archive');

        if (!btn) return;
        
        archiveRow = $(this).closest('tr');
        archiveId = archiveRow.find('.listing_id').data('value');

        const archiveSku = archiveRow.find('.item-sku').data('value');
        const archiveNotes = archiveRow.find('.item-notes').data('value');

        $('#confirmArchiveModalLabel').html(archiveSku);
        $('.confirm-archive-notes').html(archiveNotes);
    });

    $('.confirm-archive').on('click', function() {
        const notes = $('.confirm-archive-notes').val();
        
        const data = {
            'id': archiveId,
            'notes': notes
        };

        $.post(`${baseUrl}index.php/task/archive`, data, function(response) {
            if (response.is_success) {
                $('#confirmArchiveModal').modal('hide');
                const taskCount = $('.task-count');
                const count = Number(taskCount.html());
                taskCount.html(count - 1);
                archiveRow.remove();
            }
        });
    });

    function adddropdownrequest(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input">${request_actions}</select>`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }
    
    ///

    function formatPrice(price) {
        const numberFormatOptions = {                                                 
            style: 'currency', currency: 'USD', minimumFractionDigits: 2, maximumFractionDigits: 2
        };
        
        const numberFormat = new Intl.NumberFormat('en-US', numberFormatOptions);

        return numberFormat.format(price);
    }

    function addEditableFieldPercentage(cell, value, className) {
        const input = $(
            '<div class="input-group">'
                + `<input type="number" step="any" class="form-control ${className}-input w-auto" value="${value}">`
                + '<div class="input-group-append"><span class="input-group-text">&#37;</span></div>'
                + '</div>'
        );
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(`<i class="fa fa-check ${className}-apply" aria-hidden="true"></i>`);

        const btnCancel = $(`<button type="button" class="btn btn-link text-primary ${className}-cancel"></button>`);
        btnCancel.html(`<i class="fa fa-times ${className}-cancel" aria-hidden="true"></i>`);

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }

    function addEditableField(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input w-auto" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(`<i class="fa fa-check ${className}-apply" aria-hidden="true"></i>`);

        const btnCancel = $(`<button type="button" class="btn btn-link text-primary ${className}-cancel"></button>`);
        btnCancel.html(`<i class="fa fa-times ${className}-cancel" aria-hidden="true"></i>`);

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }

    function showNameWithTooltip(firstname, fullname) {
        return `<br><div data-toggle="tooltip" data-placement="top" title="${fullname}">${firstname}</div>`;
    }

    function showSpinner() {
        $('#items').html(
            `<tr class="odd"><td valign="top" colspan="100%" class="datatables_empty">${spinner}</td></tr>`
        );
    }
});
