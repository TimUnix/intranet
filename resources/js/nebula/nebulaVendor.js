let baseUrl;

$(document).ready(function() {
	baseUrl = $('#nebulaVendorScript').data('baseUrl');

	$('#addVendorPhoto').on('change', function(event) {
		const filename = event.target.files[0].name;

		$(this).next('.custom-file-label').html(filename);
	});

	$('.add-vendor-form').on('submit', function(event) {
		event.preventDefault();

		const data = new FormData($('.add-vendor-form')[0]);

		$.post({
			url: `${baseUrl}index.php/thanos/vendor/add_vendor`,
			enctype: 'multipart/form-data',
			data: data,
			processData: false,
			contentType: false,
			cache: false
		}, function(response) {
			if (response !== undefined && response.hasOwnProperty('error')) {
				$(document).Toasts('create', {
					title: response.error.title,
					body: response.error.message,
					autohide: true,
					autoremove: true,
					fade: true,
					delay: 7000
				});

				return;
			}

			location.reload();
		});
	});

    $('.card-vendor').on('click', '.btn-edit-name', function(event) {
        const btnEdit = $(event.target).closest('.btn-edit-name');
        const vendor = btnEdit.closest('.card-body').find('.vendor');

        if (!vendor.hasClass('d-none')) {
            vendor.addClass('d-none');
        }

        const vendorEditable = btnEdit.closest('.card-body').find('.vendor-editable');

        vendorEditable.removeClass('d-none');
    });

    $('.card-vendor').on('click', '.btn-vendor-cancel-edit', function(event) {
        const btnCancelEdit = $(event.target).closest('.btn-vendor-cancel-edit');
        const vendorEditable = btnCancelEdit.closest('.card-body').find('.vendor-editable');

        if (!vendorEditable.hasClass('d-none')) {
            vendorEditable.addClass('d-none');
        }

        const vendor = btnCancelEdit.closest('.card-body').find('.vendor');

        vendor.removeClass('d-none');
    });

    $('.card-vendor').on('click', '.btn-vendor-save-change', function(event) {
        const btnSaveChange = $(event.target).closest('.btn-vendor-save-change');

        const vendorNewName = btnSaveChange.closest('.card-body').find('.vendor-input').val();
        const vendorOldName = btnSaveChange.closest('.card-body').find('.vendor .vendor-name').data('name');

        const data = {
            'old_name': vendorOldName,
            'new_name': vendorNewName
        };

        $.post(`${baseUrl}index.php/thanos/vendor/change_name`, data, function(response) {
            if (response !== undefined && response.hasOwnProperty('error')) {
                $(document).Toasts('create', {
					title: response.error.title,
					body: response.error.message,
					autohide: true,
					autoremove: true,
					fade: true,
					delay: 7000,
                    class: 'bg-danger',
                    icon: 'fa fa-exclamation-triangle'
                });

                return;
            } 

            $(document).Toasts('create', {
                title: 'Change Name Success',
                body: 'Successfully changed vendor name',
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 7000,
                class: 'bg-success',
                icon: 'fa fa-check'
            });

            btnSaveChange.closest('.card-body').find('.vendor .vendor-name').data('name', vendorNewName);
            btnSaveChange.closest('.card-body').find('.vendor .vendor-name').html(vendorNewName);

            if (!btnSaveChange.closest('.card-body').find('.vendor-editable').hasClass('d-none')) {
                btnSaveChange.closest('.card-body').find('.vendor-editable').addClass('d-none');
            }

            btnSaveChange.closest('.card-body').find('.vendor').removeClass('d-none');
        });
    });

    $('.file-vendor-upload').on('change', function(event) {
        const fileInput = event.target.closest('.file-vendor-upload');
        const image = fileInput.files[0];
        const vendorName = $(fileInput).closest('.card').find('.vendor-name').data('name');

        const formData = new FormData();
        formData.append('vendor_logo', image);
        formData.append('vendor_name', vendorName);

		$.post({
			url: `${baseUrl}index.php/thanos/vendor/update_logo`,
			enctype: 'multipart/form-data',
			data: formData,
			processData: false,
			contentType: false,
			cache: false
		}, function(response) {
			if (response !== undefined && response.hasOwnProperty('error')) {
				$(document).Toasts('create', {
					title: response.error.title,
					body: response.error.message,
					autohide: true,
					autoremove: true,
					fade: true,
					delay: 7000
				});

				return;
			}

            location.reload();
        });
        
    });
});
