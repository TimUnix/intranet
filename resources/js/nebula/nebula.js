$(document).ready(function () {
    baseUrl = $('#nebulaScript').data('baseUrl');

    const itemsLimit = 100;
    let currentOffset = 0;
    let moreItemsRequested = false;
    let verifyMode = $('#switchVerifyMode').is(':checked');

    let orderBy = 'created';
    let sortOrder = 'ASC';

    const referenceWidth = $('.card').width();
    let colspan = 0;
    let colWidthSum = 0;

    $('#searchQuery').focus();

    $('#reportsTable > thead > tr > th').each(function () {
        colWidthSum += $(this).width();

        if (colWidthSum < referenceWidth) {
            colspan++;
        }
    });

    const searchParameters = new URLSearchParams(window.location.search);
    const searchParameterCount = Array.from(searchParameters).length;

    if (searchParameterCount > 0) {
        const searchQuery = searchParameters.get('searchQuery');
        const searchColumn = searchParameters.get('searchColumn');
        const directories = searchParameters.get('directories');
        verifyMode = searchParameters.get('verifyMode') === 'true';

        $('#searchQuery').val(searchQuery);
        $('#searchColumn').val(searchColumn);
        $('#switchVerifyMode').prop('checked', verifyMode);

        directories.split(',').forEach(function (directory) {
            $(`.directory-filter[value="${directory}"]`).prop('checked', true);
        });

        updateCertificateUrl();
        loadReports();
        loadLots();
    }

    let lastHeaderClicked = '';
    let currentHeaderClicked = '';

    $('.header-sortable').on('click', function () {
        currentOffset = 0;
        moreItemsRequested = false;
        currentHeaderClicked = $(this).data('column');
        orderBy = currentHeaderClicked;

        const table = $(this).closest('table');
        const isMainTable = table.hasClass('main-table');

        if (!isMainTable && !table.hasClass('sticky-table')) return;

        const otherTable = $(isMainTable ? '.sticky-table' : '.main-table');

        if (currentHeaderClicked === lastHeaderClicked) {
            $(this).find('.fa-sort-up').toggleClass('d-none');
            $(this).find('.fa-sort-down').toggleClass('d-none');

            otherTable.find(`th[data-column="${currentHeaderClicked}"]`).find('.fa-sort-up').toggleClass('d-none');
            otherTable.find(`th[data-column="${currentHeaderClicked}"]`).find('.fa-sort-down').toggleClass('d-none');

            sortOrder = toggleSorting(sortOrder);
        } else {
            $('.fa-sort-up').removeClass('d-none');
            $('.fa-sort-up').addClass('d-none');
            $('.fa-sort-down').removeClass('d-none');
            $('.fa-sort-down').addClass('d-none');

            $(this).find('.fa-sort-up').removeClass('d-none');
            otherTable.find(`th[data-column="${currentHeaderClicked}"]`).find('.fa-sort-up').removeClass('d-none');
            sortOrder = 'ASC';
        }

        const url = new URL(window.location.href);
        url.searchParams.set('orderBy', orderBy);
        url.searchParams.set('sortOrder', sortOrder);

        const state = {
            'orderBy': orderBy,
            'sortOrder': sortOrder
        };

        window.history.pushState(state, 'Sorting', url);

        syncStickyHeader();
        updateCertificateUrl();
        loadReports();

        lastHeaderClicked = currentHeaderClicked;
    });

    let timer;

    $('#searchQuery').on('input paste', function () {
        clearTimeout(timer);
        const searchBar = $(this);

        timer = setTimeout(function () {
            currentOffset = 0;
            moreItemsRequested = false;

            if (searchBar.val().length === 0) {
                return;
            }

            const url = new URL(window.location.href);
            url.searchParams.set('searchQuery', searchBar.val());
            window.history.pushState({ 'searchQuery': searchBar.val() }, 'Search Query', url);

            updateCertificateUrl();
            loadReports();

            $('#searchQuery').focus();
        }, 1000);
    });

    $('#searchQuery').on('focus', function () {
        if ($(this).val().length > 0) {
            $(this).select();
        }
    });

    $('#reportsTable').on('click', '.btn-report-info', function () {
        const button = $(this).closest('.btn-report-info');
        const row = button.closest('tr');

        if (!button || !row) return;

        const serialNumber = row.find('.report-serial-number').data('value');

        $.post(`${baseUrl}index.php/thanos/nebula/load_full_report`, { 'serial_number': serialNumber },
            function (response) {
                $('#fullReport').html(response);
            });
    });

    $('#exportCSV').click(function () {
        const searchQuery = $('#searchQuery').val();
        const searchColumn = $('#searchColumn').val();
        const directories = $('.directory-filter:checked').toArray()
            .map(function (checkbox) { return $(checkbox).val(); });

        const data = {
            'searchQuery': searchQuery,
            'orderBy': orderBy,
            'sortOrder': sortOrder,
            'searchColumn': searchColumn,
            'directories': directories
        };

        $.post(`${baseUrl}index.php/thanos/nebula/exportCSV`, data, function (response) {
            var filename = `nebula_${new Date().toLocaleDateString()}.csv`;
            const link = $('<a>');
            link.addClass('d-none');
            link.addClass('downloadCSVLink');
            link.prop('target', '_blank');
            link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
            link.prop('download', filename);

            $('body').append(link);
            link[0].click();
            link.remove();
        });
    });

    $('.btnPrintCertificate').on('click', function () {
        const printWindow = window.open(`${baseUrl}index.php/thanos/nebula/certificate`);
        printWindow.document.close();
        printWindow.focus();

        printWindow.print();
    });

    $(document).on('scroll', function () {
        const rowCount = $('#reports > tr').length;

        // if the rows initially returned are less than 100, there are no more rows to load further
        if (rowCount < itemsLimit) return;

        const position = $(this).scrollTop();
        let maxHeight = $(this).height();

        if (position >= (maxHeight * 4 / 5) && !moreItemsRequested) {
            moreItemsRequested = true;
            loadReports();
        }
    });

    $('#searchForm').on('submit', function (event) {
        event.preventDefault();

        search();
    });

    $('#reportsTable').on('click', '.btn-copy', function () {
        const button = $(this).closest('.btn-copy');
        const icon = button.find('.btn-copy-icon');
        const cell = button.closest('td');
        const value = cell.data('value');

        const temp = document.createElement('input');
        document.body.append(temp);
        temp.value = value;
        temp.select();

        document.execCommand('copy');
        temp.remove();

        icon.toggleClass('fa-copy');
        icon.toggleClass('fa-check');
        button.toggleClass('text-success');

        setTimeout(function () {
            icon.toggleClass('fa-check');
            icon.toggleClass('fa-copy');
            button.toggleClass('text-success');
        }, 1000);

        $(document).Toasts('create', {
            title: 'Copied',
            body: `Copied ${value}`,
            autohide: true,
            autoremove: true,
            fade: true,
            delay: 3000
        });
    });

    $('#reportsTable').on('click', '.header-selector', function () {
        const checkbox = $(this).closest('.header-selector');
        const isChecked = checkbox.prop('checked');

        $('.report-selector').filter(function () {
            const row = $(this).closest('tr');
            const displayState = $(row).css('display');
            const isRowVisible = displayState != 'none';
            return isRowVisible;
        })
            .find('input[type="checkbox"]')
            .prop('checked', isChecked);
    });

    $('.btn-batch-assign-cost-sell').on('click', function () {
        const assignmentOption = $('#batchAssignCostSellOptions').val();
        const value = $('#batchAssignCostSellValue').val();

        const selectedReports = $('#reports').children().toArray()
            .filter(function (row) {
                const rowValue = $(row).find('.report-selector').find('').is(':checked');
                const isRowSelected = $(row).find('.report-selector').find('input[type="checkbox"]').is(':checked');

                const isValueSame = rowValue === value;

                return !isValueSame && isRowSelected;
            });


        if (selectedReports.length === 0) {
            $(document).Toasts('create', {
                title: 'Selected records up to date',
                body: 'No records to update',
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 7000
            });

            return;
        }

        const serialNumbers = selectedReports.map(function (row) {
            return $(row).find('.report-serial-number').data('value');
        });

        const data = { 'serial_numbers': serialNumbers, 'assignment_option': assignmentOption, 'value': value };

        $.post(`${baseUrl}index.php/thanos/nebula/batch_assign_cost_sell`, data, function (response) {
            $(document).Toasts('create', {
                title: response.title,
                body: response.message,
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 7000
            });

            if (!response.is_error) {
                $('#batchAssignCostSellModal').modal('hide');
                $('#batchAssignCostSellValue').val('');

                selectedReports.forEach(function (row) {
                    $(row).find(`.report-${assignmentOption}`).data('value', value);
                    $(row).find(`.report-${assignmentOption}`).find('.report-editable-value').html(`\$${value}`);
                });
            }

        });
    });

    $('.btn-disposition').on('click', function () {
        const button = $(this).closest('.btn-disposition');
        const disposition = button.data('disposition');

        const selectedReports = $('#reports').children().toArray()
            .filter(function (row) {
                const rowDisposition = $(row).find('.report-disposition').data('value');
                const isRowSelected = $(row).find('.report-selector').find('input[type="checkbox"]').is(':checked');

                const isDispositionSame = rowDisposition === disposition;
                return disposition === 'Clear' ? isRowSelected && rowDisposition.length > 1
                    : isRowSelected && !isDispositionSame;
            });

        if (selectedReports.length === 0) {
            $(document).Toasts('create', {
                title: 'Selected records up to date',
                body: 'No records to update',
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 7000
            });

            return;
        }

        const serialNumbers = selectedReports.map(function (row) {
            return $(row).find('.report-serial-number').data('value');
        });

        const data = { 'serial_numbers': serialNumbers, 'disposition': disposition };

        $.post(`${baseUrl}index.php/thanos/nebula/update_disposition`, data, function (response) {
            const title = response.is_error ? 'Update Failed' : 'Update Success';

            $(document).Toasts('create', {
                title: title,
                body: response.message,
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 7000
            });

            if (!response.is_error) {
                selectedReports.forEach(function (row) {
                    $(row).find('.report-disposition').data('value', disposition === 'Clear' ? '' : disposition);
                    $(row).find('.report-disposition').html(disposition === 'Clear' ? '' : disposition);
                });
            }
        });
    });

    $('#reportsTable').on('click', '.btn-report-edit', function () {
        const button = $(this).closest('.btn-report-edit');
        const cell = button.closest('.report-editable');
        const editContainer = cell.find('.report-editable-field-container');
        const valueField = cell.find('.report-editable-value');

        valueField.toggleClass('d-none');
        button.toggleClass('d-none');
        editContainer.toggleClass('d-none');
    });

    $('#reportsTable').on('click', '.btn-report-edit-cancel', function () {
        const button = $(this).closest('.btn-report-edit-cancel');
        const cell = button.closest('.report-editable');
        const editContainer = cell.find('.report-editable-field-container');
        const editableField = editContainer.find('.report-editable-field');

        editableField.val(cell.data('value'));

        cell.find('.btn-report-edit').toggleClass('d-none');
        cell.find('.report-editable-value').toggleClass('d-none');
        editContainer.toggleClass('d-none');
    });

    $('#reportsTable').on('click', '.btn-report-edit-apply', function () {
        const button = $(this).closest('.btn-report-edit-apply');
        const cell = button.closest('.report-editable');
        const row = cell.closest('tr');
        const editContainer = cell.find('.report-editable-field-container');
        const editableField = editContainer.find('.report-editable-field');
        const cellSerialNumber = row.find('.report-serial-number');
        const serialNumber = cellSerialNumber.data('value');
        const valueField = cell.find('.report-editable-value');

        const field = cell.data('field');
        const value = editableField.val();

        const data = { 'serial_number': serialNumber, 'field': field, 'value': value };

        $.post(`${baseUrl}index.php/thanos/nebula/apply_report_edit`, data, function (response) {
            const title = response.is_error ? 'Update Error' : 'Update Success';

            $(document).Toasts('create', {
                title: title,
                body: response.message,
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 5000
            });

            if (response.is_error) return;

            cell.find('.btn-report-edit').toggleClass('d-none');
            valueField.toggleClass('d-none');
            editContainer.toggleClass('d-none');
            cell.data('value', value);
            editableField.val(value);
            valueField.text(`\$${value}`);
        });
    });

    $('#reportsTable').on('click', '.btn-report-defective', function () {
        const button = $(this).closest('.btn-report-defective');
        const cell = button.closest('.report-editable');
        const row = cell.closest('tr');
        const cellSerialNumber = row.find('.report-serial-number');
        const serialNumber = cellSerialNumber.data('value');
        const value = 'Defective';

        const field = cell.data('field');

        const data = { 'serial_number': serialNumber, 'field': field, 'value': value };

        $.post(`${baseUrl}index.php/thanos/nebula/apply_report_edit`, data, function (response) {
            const title = response.is_error ? 'Update Error' : 'Update Success';

            $(document).Toasts('create', {
                title: title,
                body: 'Successfully marked serial defective',
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 5000
            });

            if (response.is_error) return;

            const btnClearStatus = $('<button type="button" class="btn btn-link btn-clear-status">');
            btnClearStatus.html('<i class="fa fa-times" aria-hidden="true"></i> Clear');

            row.removeClass('result-erased');
            row.removeClass('result-other');
            row.removeClass('result-defective');
            row.addClass('result-defective');

            cell.data('value', value);
            cell.html(value);
            cell.append(btnClearStatus);
        });
    });

    $('#createPurchaseOrderForm').on('submit', function (event) {
        event.preventDefault();
        const spinner = $('.create-purchase-order-spinner');

        if (spinner.hasClass('d-none')) {
            spinner.removeClass('d-none');
        }

        const warningBanner = $('.create-purchase-order-banner');

        const location = $('#createPurchaseOrderLocation').val();
        const name = $('#createPurchaseOrderName').val();
        const vendor = $('#createPurchaseOrderVendor').val();

        if (location === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Location cannot be left blank.');

            return;
        }

        if (name === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Name cannot be left blank.');

            return;
        }

        const data = { 'location': location, 'name': name, 'vendor': vendor };

        $.post(`${baseUrl}index.php/thanos/nebula/create_purchase_order`, data, function (response) {
            $('#createPurchaseOrderModal').modal('hide');

            if (!spinner.hasClass('d-none')) {
                spinner.addClass('d-none');
            }

            if (!response.is_error) {
                const displayName = name.toLowerCase().includes('po') ? name : `PO${name}`;
                const formCheck = $('<div>').addClass('form-check form-check-inline ml-1 mb-1'
                    + ' form-check-directory-filter');
                const inputGroup = $('<div>').addClass('input-group-text directory-filters-container');
                const checkbox = $('<input type="checkbox" class="form-check-input directory-filter" '
                    + `id="directory-${displayName}">`).val(`${displayName}`);
                const label = $(`<label for="directory-${displayName}" class="form-check-label">`).
                    html(`${displayName}`);

                inputGroup.append(checkbox).append(label);
                formCheck.append(inputGroup);

                $('#renamePurchaseOrderOption').append($('<option>', {
                    value: displayName,
                    text: displayName
                }));

                $('#createLotDirectory').append($('<option>', {
                    value: displayName,
                    text: displayName
                }));

                $('.form-check-directory-filter').last().after(formCheck);
                $('#createPurchaseOrderName').val('');
            }

            $(document).Toasts('create', {
                title: response.title,
                body: response.message,
                autohide: true,
                autoremove: true,
                delay: 7000
            });
        });
    });

    $('#renamePurchaseOrderForm').on('submit', function (event) {
        event.preventDefault();
        const spinner = $('.rename-purchase-order-spinner');

        if (spinner.hasClass('d-none')) {
            spinner.removeClass('d-none');
        }

        const warningBanner = $('.rename-purchase-order-banner');

        const oldName = $('#renamePurchaseOrderOption').val();
        const newName = $('#renamePurchaseOrderName').val();

        if (oldName === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Purchase order cannot be left blank.');

            return;
        }

        if (newName === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Name cannot be left blank.');

            return;
        }

        const data = { 'old_name': oldName, 'new_name': newName };

        $.post(`${baseUrl}index.php/thanos/nebula/rename_purchase_order`, data, function (response) {
            $('#renamePurchaseOrderModal').modal('hide');

            if (!spinner.hasClass('d-none')) {
                spinner.addClass('d-none');
            }

            if (!response.is_error) {
                const displayName = newName.toLowerCase().includes('po') ? newName : `PO${newName}`;
                const checkbox = $(`.directory-filter[value="${oldName}"]`);
                checkbox.val(displayName);
                const checkboxId = checkbox.prop('id');
                checkbox.prop('id', `directory-${displayName}`);
                $(`label[for="${checkboxId}"]`).html(`${displayName}`);
                loadLots();

                const renamePurchaseOrderOption = $('#renamePurchaseOrderOption').find(`option[value="${oldName}"]`);
                renamePurchaseOrderOption.val(displayName);
                renamePurchaseOrderOption.html(displayName);

                const createLotDirectoryOption = $('#createLotDirectory').find(`option[value="${oldName}"]`);
                createLotDirectoryOption.val(displayName);
                createLotDirectoryOption.html(displayName);

                $('#renamePurchaseOrderOption').val('');
                $('#renamePurchaseOrderName').val('');
            }

            $(document).Toasts('create', {
                title: response.title,
                body: response.message,
                autohide: true,
                autoremove: true,
                delay: 7000
            });
        });
    });

    $('#createLotForm').on('submit', function (event) {
        event.preventDefault();
        const spinner = $('.create-lot-spinner');

        if (spinner.hasClass('d-none')) {
            spinner.removeClass('d-none');
        }

        const warningBanner = $('.create-lot-banner');

        const directory = $('#createLotDirectory').val();
        const customerCode = $('#createLotCustomerCode').val().trim();

        if (directory === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Purchase Order cannot be left blank.');

            return;
        }

        if (customerCode === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Customer Code cannot be left blank.');

            return;
        }

        const data = {
            'directory': directory,
            'customer_code': customerCode
        };

        $.post(`${baseUrl}index.php/thanos/nebula/create_lot`, data, function (response) {
            if (response.is_error) {
                if (response.error_type === 'lot_exists') {
                    const warningBanner = $('.create-lot-banner');

                    if (warningBanner.hasClass('d-none')) {
                        warningBanner.removeClass('d-none');
                    }

                    warningBanner.html('Lot already exists.');

                    return;
                }
            }

            $('#createLotModal').modal('hide');
            $('#createLotDirectory').val('');
            $('#createLotCustomerCode').val('');

            if (!spinner.hasClass('d-none')) {
                spinner.addClass('d-none');
            }

            $('#renameLotOptions').append($('<option>', {
                value: `${directory} ${customerCode}`,
                text: `${directory} ${customerCode}`
            }));

            loadLots();

            $(document).Toasts('create', {
                title: response.title,
                body: response.message,
                autohide: true,
                autoremove: true,
                delay: 7000
            });
        });
    });

    $('#renameLotForm').on('submit', function (event) {
        event.preventDefault();
        const spinner = $('.rename-lot-spinner');

        if (spinner.hasClass('d-none')) {
            spinner.removeClass('d-none');
        }

        const warningBanner = $('.rename-lot-banner');

        const lot = $('#renameLotOptions').val();
        const customerCode = $('#renameLotCustomerCode').val().trim();

        if (lot === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Lot cannot be left blank.');

            return;
        }

        if (customerCode === '') {
            if (warningBanner.hasClass('d-none')) {
                warningBanner.removeClass('d-none');
            }

            warningBanner.html('Customer Code cannot be left blank.');

            return;
        }

        const data = {
            'lot': lot,
            'customer_code': customerCode
        };

        $.post(`${baseUrl}index.php/thanos/nebula/rename_lot`, data, function (response) {
            if (response.is_error) {
                if (response.error_type === 'lot_exists') {
                    const warningBanner = $('.rename-lot-banner');

                    if (warningBanner.hasClass('d-none')) {
                        warningBanner.removeClass('d-none');
                    }

                    warningBanner.html('Lot already exists.');

                    return;
                }
            }

            $('#renameLotModal').modal('hide');
            $('#renameLotOptions').val('');
            $('#renameLotCustomerCode').val('');

            if (!spinner.hasClass('d-none')) {
                spinner.addClass('d-none');
            }

            const lotOption = $('#renameLotOptions').find(`option[value="${lot}"]`);
            const lotOptionSplit = lotOption.text().trim().split(' ');
            const purchaseOrder = lotOptionSplit[0];

            lotOption.val(`${purchaseOrder} ${customerCode}`);
            lotOption.html(`${purchaseOrder} ${customerCode}`);

            loadLots();

            $(document).Toasts('create', {
                title: response.title,
                body: response.message,
                autohide: true,
                autoremove: true,
                delay: 7000
            });
        });
    });

    $('#createLotModal').on('show.bs.modal', function () {
        const warningBanner = $('.create-lot-banner');
        const spinner = $('.create-lot-spinner');

        if (!warningBanner.hasClass('d-none')) {
            warningBanner.addClass('d-none');
        }

        if (!spinner.hasClass('d-none')) {
            spinner.addClass('d-none');
        }
    });

    $('#renameLotModal').on('show.bs.modal', function () {
        const warningBanner = $('.rename-lot-banner');
        const spinner = $('.rename-lot-spinner');

        if (!warningBanner.hasClass('d-none')) {
            warningBanner.addClass('d-none');
        }

        if (!spinner.hasClass('d-none')) {
            spinner.addClass('d-none');
        }
    });

    $('#createPurchaseOrderModal').on('show.bs.modal', function () {
        const warningBanner = $('.create-purchase-order-banner');
        const spinner = $('.create-purchase-order-spinner');

        if (!warningBanner.hasClass('d-none')) {
            warningBanner.addClass('d-none');
        }

        if (!spinner.hasClass('d-none')) {
            spinner.addClass('d-none');
        }
    });

    $('#renamePurchaseOrderModal').on('show.bs.modal', function () {
        const warningBanner = $('.rename-purchase-order-banner');
        const spinner = $('.rename-purchase-order-spinner');

        if (!warningBanner.hasClass('d-none')) {
            warningBanner.addClass('d-none');
        }

        if (!spinner.hasClass('d-none')) {
            spinner.addClass('d-none');
        }
    });

    $('.batch-selector-options').on('click', '.batch-selector-item', function () {
        $('.header-selector').prop('checked', false);
        $('.report-selector').find('input[type="checkbox"]').prop('checked', false);

        const selector = $(this).text().trim();

        $('#reports').children().toArray().forEach(function (row) {
            const partNumber = $(row).find('.report-product-name').data('value');

            if (selector === 'Clear Filter') {
                $(row).show();
            } else if (partNumber !== selector) {
                $(row).hide();
            } else {
                $(row).show();
            }
        });
    });

    $('.btn-assign-lot').on('click', function () {
        const lots = $('.purchase-order-lot:checked').toArray()
            .map(function (lot) {
                return $(lot).val();
            });

        if (lots.length > 1) {
            $(document).Toasts('create', {
                title: 'Too many Lots Selected',
                body: 'Only one lot can be applied at a time.',
                autohide: true,
                autoremove: true,
                delay: 7000
            });

            return;
        }

        if (lots.length === 0) {
            $(document).Toasts('create', {
                title: 'No Lot Selected',
                body: 'No lot has been selected. Please select a lot.',
                autohide: true,
                autoremove: true,
                delay: 7000
            });

            return;
        }

        const lot = lots[0];

        const selectedReports = $('#reports').children().toArray()
            .filter(function (row) {
                return $(row).find('.report-selector').find('input[type="checkbox"]').prop('checked');
            });

        if (selectedReports.length === 0) {
            $(document).Toasts('create', {
                title: 'No Report Selected',
                body: 'No report has been selected. Please select a report.',
                autohide: true,
                autoremove: true,
                delay: 7000
            });

            return;
        }

        const filteredReports = selectedReports.filter(function (row) {
            const rowLot = $(row).find('.report-lot').data('value');

            return rowLot !== lot;
        });

        if (filteredReports.length === 0) {
            $(document).Toasts('create', {
                title: 'Reports up-to-date.',
                body: 'The selected reports are already up-to-date.',
                autohide: true,
                autoremove: true,
                delay: 7000
            });

            return;
        }

        const reports = filteredReports.map(function (row) {
            return $(row).find('.report-serial-number').data('value');
        });

        const data = {
            'lot': lot,
            'reports': reports
        };

        $.post(`${baseUrl}index.php/thanos/nebula/assign_lot`, data, function (response) {
            $(document).Toasts('create', {
                title: response.title,
                body: response.message,
                autohide: true,
                autoremove: true,
                delay: 7000
            });

            filteredReports.forEach(function (row) {
                const cellLot = $(row).find('.report-lot');
                cellLot.html(lot);
                cellLot.data('value', lot);
            });
        });
    });

    $('.directory-filter').on('click', function () {
        loadLots();
    });

    $('#viewCertificateVendorOptions').on('change', function () {
        updateCertificateUrl();
    });

    $('#reportsTable').on('click', '.btn-clear-status', function () {
        const button = $(this).closest('.btn-clear-status');
        const cell = button.closest('td');
        const row = button.closest('tr');

        const serialNumber = row.find('.report-serial-number').data('value');

        const data = { 'serial_number': serialNumber, 'field': 'status', 'value': '' };

        $.post(`${baseUrl}index.php/thanos/nebula/apply_report_edit`, data, function (response) {
            const title = response.is_error ? 'Update Error' : 'Update Success';

            $(document).Toasts('create', {
                title: title,
                body: 'Successfully cleared serial status',
                autohide: true,
                autoremove: true,
                fade: true,
                delay: 5000
            });

            row.removeClass('result-defective');
            row.removeClass('result-erased');
            row.removeClass('result-other');

            const result = row.find('.report-result').data('value');

            if (result === 'Erased') {
                if (!row.hasClass('result-erased')) row.addClass('result-erased');
            } else {
                if (!row.hasClass('result-other')) row.addClass('result-other');
            }

            if (response.is_error) return;

            const btnMarkDefective = $('<button class="btn btn-link btn-report-defective" type="button">');
            btnMarkDefective.html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>');
            btnMarkDefective.append(' Mark Defective');

            cell.data('value', '');
            cell.html(btnMarkDefective);
        });
    });

    $('.btn-add-device-submit').on('click', function () {
        if ($('#switchmultiMode').is(':checked')) {
				scanmode();		
        } else {
			
			if ($('#addDeviceResultList').val() == "Choose..."){
				$(document).Toasts('create', {
					title: "Empty Result!",
					body: "Please input Result",
					icon: 'fa fa-warning',
					autohide: true,
					autoremove: true,
					fade: true,
					delay: 3000,
					class: 'bg-danger mw-100 m-1 text-md'
				});
                $('#addDeviceSerial').val('');
                $('#addDeviceResultList').focus();
            }else if ($('#addDevicePO').val() == "No matched PO" || $('#addDevicePO').val() == "Select a Vendor first" ){
                $(document).Toasts('create', {
                    title: "PO invalid",
                    body: "Invalid Purchase Order",
                    icon: 'fa fa-warning',
                    autohide: true,
                    autoremove: true,
                    fade: true,
                    delay: 3000,
                    class: 'bg-danger mw-100 m-1 text-md'
                });
                $('#addDeviceSerial').val('');
                $('#addDevicePO').focus();
			}else{
            const capacity = $('#sizeselect').val();
            const data = {
                'serial_number': $('#addDeviceSerial').val(),
                'part_number': $('#addDevicePartNumber').val(),
                'vendor': $('#addDeviceVendor').val(),
                'purchase_order': $('#addDevicePO').val(),
                'size': $('#addDeviceSize').val() + " " + capacity,
                'result': $('#addDeviceResultList').val(),
                'status': $('#addDeviceStatus').val()
            };

            $.post(`${baseUrl}index.php/thanos/nebula/create`, data, function (response) {
                const title = response.is_error ? 'Add Serial Failed' : 'Add Serial Success';
                const icon = response.is_error ? 'fa fa-warning' : 'fa fa-check';

                if (title == "Add Serial Success") {
                    $(document).Toasts('create', {
                        title: title,
                        body: response.message,
                        icon: icon,
                        autohide: true,
                        autoremove: true,
                        fade: true,
                        delay: 3000,
                        class: 'bg-success mw-100 m-1 text-md'
                    });
                } else {
                    $(document).Toasts('create', {
                        title: title,
                        body: response.message,
                        icon: icon,
                        autohide: true,
                        autoremove: true,
                        fade: true,
                        delay: 3000,
                        class: 'bg-danger mw-100 m-1 text-md'
                    });
                }

                if (!response.is_error) {
                    $('#addDeviceModal').modal('hide');
                    $('#addDeviceSerial').val('');
                    $('#addDevicePartNumber').val('');
                    $('#addDeviceDirectory').val('');
                    $('#addDeviceSize').val('');
                    $('#addDeviceResultList').val('');
                    $('#addDeviceStatus').val('');
                }
            });
		}
        }
    });

    $('#addDeviceVendor').on('change',function(){
        const data = {
            'vendor': $('#addDeviceVendor').val()};
        $.post(`${baseUrl}index.php/thanos/nebula/getvendor`, data, function (response) {
            const select = document.getElementById('addDevicePO');
            if (response == "No matched PO"){
                select.innerHTML = "";
                select.options.add(new Option(response,response));
            }else{
                select.innerHTML = "";
                for(let i = 0; i < response.purchase_order.length; i++){ 
                    select.options.add(new Option(response.purchase_order[i].name, response.purchase_order[i].name));
                }
            }
            });    
    });

    $('#switchmultiMode').on('change', function () {
        if ($(this).is(':checked')) {
            $('#addDeviceResultList').val("Scanned In");
            $('#addDeviceStatusList').val("Scanned");
            $('#addDeviceSerial').focus();
            $('#modalsize').last().addClass("modal-lg");
            $('#listserial').collapse("show");
        } else {
            $('#addDeviceResultList').prop('selectedIndex', 0);
            $('#addDeviceStatusList').prop('selectedIndex', 0);
            $(".btn-add-device-submit").prop('disabled', false);
            $('#modalsize').last().removeClass("modal-lg");
            $('#serialList').empty();
            $('#listserial').collapse("hide");
        }

    });

    $("#addDeviceSerial").keypress(function (event) {
		if ($('#switchmultiMode').is(':checked')){
			if (event.keyCode == 13) {
				scanmode();
			}
		}
    });


    $("#editlistbtn").on('click',function (event) {
        $('#removelistbtn').prop('disabled', false);
        $('.seriallistcheck').prop('disabled', false);
    });

    $("#removelistbtn").on('click',function (event) {
        var removeditems =  $('.seriallistcheck:checked').length;
        count = count - removeditems;
        $('#serialcount').html(''+ count +'');
        const seriallistnumber2 = $('.seriallistcheck:checked').toArray()
            .map(function (checkbox) { return $(checkbox).val(); });
            const data = {
                'serial_number': seriallistnumber2
            };
            $.post(`${baseUrl}index.php/thanos/nebula/removeitems`, data, function (response) {
                const title = response.is_error ? 'Remove Serial Failed' : 'Remove Serial Success';
                const icon = response.is_error ? 'fa fa-warning' : 'fa fa-check';
    
                if (!response.is_error) {
                    $(document).Toasts('create', {
                        title: title,
                        body: response.message,
                        icon: icon,
                        autohide: true,
                        autoremove: true,
                        fade: true,
                        delay: 3000,
                        class: 'bg-success mw-100 m-1 text-md'
                    });
                } else {
                    $(document).Toasts('create', {
                        title: title,
                        body: response.message,
                        icon: icon,
                        autohide: true,
                        autoremove: true,
                        fade: true,
                        delay: 3000,
                        class: 'bg-danger mw-100 m-1 text-md'
                    });
                }
    
                if (!response.is_error) {
                    $('#removelistbtn').prop('disabled', true);
                    $(".seriallistcheck").each(function() {
                        $('.seriallistcheck').prop('disabled', true);
                        if ($(this).is(":checked")) {
                            $(this).parent().remove();
                        }
                    });
                    $('#addDeviceSerial').focus();
                    $('#addDeviceSerial').val('');
                }else{
                    $('#addDeviceSerial').val('');
                    $('#addDeviceSerial').focus();
                }
            });
        
    });
    let count = 0;
    function scanmode() {
		if ($('#addDeviceResultList').val() == "Choose..."){
			$(document).Toasts('create', {
				title: "Empty Result!",
				body: "Please input Result",
				icon: 'fa fa-warning',
				autohide: true,
				autoremove: true,
				fade: true,
				delay: 3000,
				class: 'bg-danger mw-100 m-1 text-md'
			});
            $('#addDeviceSerial').val('');
            $('#addDeviceSerial').focus();
		}else if ($('#addDevicePO').val() == "No matched PO" || $('#addDevicePO').val() == "Select a Vendor first" ){
                $(document).Toasts('create', {
                    title: "PO invalid",
                    body: "Invalid Purchase Order",
                    icon: 'fa fa-warning',
                    autohide: true,
                    autoremove: true,
                    fade: true,
                    delay: 3000,
                    class: 'bg-danger mw-100 m-1 text-md'
                });
                $('#addDeviceSerial').val('');
                $('#addDeviceSerial').focus();
        }else{
        const capacity = $('#sizeselect').val();
        const data = {
            'serial_number': $('#addDeviceSerial').val(),
            'part_number': $('#addDevicePartNumber').val(),
            'vendor': $('#addDeviceVendor').val(),
            'purchase_order': $('#addDevicePO').val(),
            'size': $('#addDeviceSize').val() + " " + capacity,
            'result': $('#addDeviceResultList').val(),
            'status': $('#addDeviceStatus').val()
        };
        $.post(`${baseUrl}index.php/thanos/nebula/create`, data, function (response) {
            const title = response.is_error ? 'Add Serial Failed' : 'Add Serial Success';
            const icon = response.is_error ? 'fa fa-warning' : 'fa fa-check';

            if (!response.is_error) {
                $(document).Toasts('create', {
                    title: title,
                    body: response.message,
                    icon: icon,
                    autohide: true,
                    autoremove: true,
                    fade: true,
                    delay: 3000,
                    class: 'bg-success mw-100 m-1 text-md'
                });
            } else {
                $(document).Toasts('create', {
                    title: title,
                    body: response.message,
                    icon: icon,
                    autohide: true,
                    autoremove: true,
                    fade: true,
                    delay: 3000,
                    class: 'bg-danger mw-100 m-1 text-md'
                });
            }

            if (!response.is_error) {
                var serial = $('#addDeviceSerial').val();
                $('#serialList').append('<li><input type="checkbox" class="seriallistcheck" id="'  + serial + '" name="'  + serial + '" value="'  + serial + '" disabled > ' + serial + '</li>');
                count = count + 1;
                $('#serialcount').html(''+ count +'');
                console.log(count);
                $('#addDeviceSerial').focus();
                $('#addDeviceSerial').val('');
            }else{
				$('#addDeviceSerial').val('');
				$('#addDeviceSerial').focus();
			}
        });
	}
    }

    $("#addDeviceModal").on('hide.bs.modal', function (event) {
        count = 0;
        $('#switchmultiMode').prop('checked', false);
        $('#addDeviceSerial').val('');
        $('#addDevicePartNumber').val('');
        $('#addDeviceDirectory').val('');
        $('#addDeviceSize').val('');
        $('#addDeviceResult').val('');
        $('#addDeviceStatus').val('');
        $('#serialList').empty();
        $('#listserial').collapse("hide");
        $('#modalsize').last().removeClass("modal-lg");
        $('#removelistbtn').prop('disabled', true);
    });


    $('#switchVerifyMode').on('change', function () {
        verifyMode = $(this).is(':checked');
        search();
    });

    function loadLots() {
        const directories = $('.directory-filter:checked').toArray().map(function (row) {
            return $(row).val();
        });

        if (directories.length === 0) {
            $('.purchase-order-lots').html('');

            return;
        }

        const data = {
            'directories': directories
        };

        $.post(`${baseUrl}index.php/thanos/nebula/load_lots`, data, function (response) {
            $('.purchase-order-lots').html(response);
        });
    }

    function loadReports() {
        const spinningRow = $(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);

        if (!moreItemsRequested) {
            $('#reports').html(spinningRow);
            $('.totals-container').html(spinner);
            $('#exportCSV').prop('disabled', true);
        } else {
            $('#reports').append(spinningRow);
        }

        const searchQuery = $('#searchQuery').val();
        const searchColumn = $('#searchColumn').val();
        const directories = $('.directory-filter:checked').toArray()
            .map(function (checkbox) { return $(checkbox).val(); });

        const data = {
            'searchQuery': searchQuery,
            'orderBy': orderBy,
            'sortOrder': sortOrder,
            'searchColumn': searchColumn,
            'directories': directories,
            'currentOffset': currentOffset,
            'verifyMode': verifyMode
        };

        if (!moreItemsRequested) {
            $.post(`${baseUrl}index.php/thanos/nebula/summary`, data, function (response, _, xhr) {
                if (xhr.status === 200) {
                    $('.totals-container').html(response);
                }
            });
        }

        $.post(`${baseUrl}index.php/thanos/nebula/load`, data, function (response) {
            if (moreItemsRequested) {
                $('#reports').append(response.view);
                spinningRow.remove();
            } else {
                $('#reports').html(response.view);
            }

            currentOffset = $('#reports > tr').length;

            const batchSelectors = $('#reports').children().toArray()
                .map(function (row) {
                    return $(row).find('.report-product-name').data('value');
                })
                .filter(function (value, index, names) {
                    return names.indexOf(value) === index;
                });

            $('.batch-selector-options').html('');

            $('.batch-selector-options').append('<button type="button" class="dropdown-item batch-selector-item">'
                + 'Clear Filter'
                + '</button>'
                + '<div class="dropdown-divider"></div>');

            for (const selector of batchSelectors) {
                const dropdownItem = $('<button type="button" class="dropdown-item batch-selector-item">'
                    + selector
                    + '</button>');
                $('.batch-selector-options').append(dropdownItem);
            }

            if (verifyMode) {
                const reportsVerified = response.reports_verified;
                const title = reportsVerified > 1 || reportsVerified === 0 ? ` ${reportsVerified} Serials Scanned` :
                    ` ${reportsVerified} Serial Scanned`;

                $(document).Toasts('create', {
                    title: title,
                    icon: 'fa fa-info',
                    autohide: true,
                    autoremove: true,
                    fade: true,
                    delay: 3000,
                    class: 'toast-status bg-info'
                });
            }
        }).then(function () {
            $('#exportCSV').prop('disabled', false);
            syncStickyHeader();
            moreItemsRequested = false;

            $('#searchQuery').focus();
        });
    }

    function toggleSorting(sortOrder) {
        if (sortOrder === 'ASC') {
            return 'DESC';
        } else {
            return 'ASC';
        }
    }

    function updateCertificateUrl() {
        const searchQuery = encodeURIComponent($('#searchQuery').val());
        const searchColumn = $('#searchColumn').val();
        const directories = $('.directory-filter:checked').toArray()
            .map(function (checkbox) { return encodeURIComponent($(checkbox).val()); })
            .join(',');
        const vendor = $('#viewCertificateVendorOptions').val();

        const certificateUrl = `${baseUrl}index.php/thanos/nebula/certificate?searchQuery=${searchQuery}`
            + `&searchColumn=${searchColumn}&directories=${directories}&orderBy=${orderBy}`
            + `&sortOrder=${sortOrder}&vendor=${vendor}`;
        $('.btn-view-certificate').prop('href', certificateUrl);
    }

    function search() {
        const searchQuery = encodeURIComponent($('#searchQuery').val());
        const searchColumn = $('#searchColumn').val();
        const directories = $('.directory-filter:checked').toArray()
            .map(function (checkbox) { return encodeURIComponent($(checkbox).val()); })
            .join(',');

        const url = `${baseUrl}index.php/thanos/nebula?searchQuery=${searchQuery}&searchColumn=${searchColumn}`
            + `&directories=${directories}&orderBy=${orderBy}&sortOrder=${sortOrder}&verifyMode=${verifyMode}`;

        window.location.href = url;
    }
});
