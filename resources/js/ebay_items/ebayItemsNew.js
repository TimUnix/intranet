let baseUrl;
let categorySpecificsChangeDone = false;
let businessPoliciesChangeDone = false;

$(document).ready(function () {
	baseUrl = $('#ebayItemsNewScript').data('baseUrl');

	$('#requestSaveDraftEbayAd').on('click', function () { saveDraft(); });

	$('.btn-draft-delete').on('click', function () {
		const id = $(this).data('id');
		const button = this;

		const data = { 'id': id };

		$.post(`${baseUrl}index.php/ebay_items/archive_draft`, data, function () {
			$(button).closest('.card').remove();
		});
	});

	bindDraftRestoreClickListener();
});

function bindDraftRestoreClickListener() {
	$('.btn-draft-restore').on('click', draftRestoreClickHandler);
}

function draftRestoreClickHandler() {
	$('#newEbayAdDraftsModal').modal('hide');
	const data = { 'id': $(this).data('id') };

	$.post(`${baseUrl}index.php/ebay_items/draft_fields`, data, function (response) {
		const category = response.find(function (field) { return field.html_id === 'newEbayAdCategory'; });
		const store = response.find(function (field) { return field.html_id === 'newEbayAdStore'; });

		$(`#${category.html_id}`).val(category.value).trigger('change');
		$(`#${store.html_id}`).val(store.value).trigger('change');

		return response;
	}).then(function (response) {
		$(document).ajaxComplete(function (_, _, options) {
			$(document).off('.btn-draft-restore');

			if (options.url.includes('get_category_specifics')) {
				categorySpecificsChangeDone = true;
			}

			if (options.url.includes('load_business_policies')) {
				businessPoliciesChangeDone = true;
			}

			if (!categorySpecificsChangeDone && !businessPoliciesChangeDone) {
				return;
			}

			const specialCharacters = '!#$%^&*()-_+=\\|/?<>,.:;\'"[]{}';

			for (let field of response) {
				switch (field.html_id) {
					case "newEbayAdCategory":
						continue;
					case "img":
					$('.image-display').html('');
						const filetype = field.value.includes('.jpg') || field.value.includes('.jpeg')
							? 'image/jpeg'
							: field.value.includes('.tiff') ? 'image/tiff'
								: field.value.includes('.gif') ? 'image/gif'
									: 'image/png';

						$('.image-display').append(
							`<img src="${baseUrl}uploads/${field.value}" width="180" class="mx-2" `
							+ `data-filetype="${filetype}" data-filename="${field.value}">`

						);
						break;
					default:
						const idContainsSpecialCharacters = field.html_id.split('').some(function (substring) {
							return specialCharacters.indexOf(substring) >= 0;
						});

						const htmlId = idContainsSpecialCharacters ? '#' + $.escapeSelector(field.html_id)
							: `#${field.html_id}`;

						const htmlField = $(htmlId);

						if (htmlField.attr('type') === 'checkbox') {
							htmlField.prop('checked', true);
						} else {
							htmlField.val(field.value);
						}
				}
			}

			categorySpecificsChangeDone = false;
			businessPoliciesChangeDone = false;
			$('.btn-draft-restore').off();
			bindDraftRestoreClickListener();
		});
	});
}

function saveDraft() {
	const isSkuValid = $('#newEbayAdSku')[0].reportValidity();

	if (!isSkuValid) {
		return;
	}

	const fields = {};

	$('.image-display img').each(function (index) {
		fields[`img-${index}`] = {
			'id': 'img',
			'value': $(this).data('filename')
		};
	});

	$('.new-ebay-ad-main-field').each(function () {
		if ($(this).prop('nodeName').toLowerCase() === 'input' && $(this).attr('type') === 'checkbox') {
			if ($(this).is(':checked')) {
				fields[$(this).attr('name')] = {
					'id': $(this).attr('id'),
					'value': $(this).val()
				};
			}
		} else {
			if ($(this).val() !== undefined && $(this).val() !== null && $(this).val().trim() !== '') {
				fields[$(this).attr('name')] = {
					'id': $(this).attr('id'),
					'value': $(this).val().trim()
				};
			}
		}
	});

	$('.new-ebay-ad-category-specific').each(function () {
		if ($(this).attr('type') === 'checkbox') {
			if ($(this).is(':checked')) {
				fields[$(this).attr('name')] = {
					'id': $(this).attr('id'),
					'value': $(this).val().trim()
				};
			}
		} else {
			if ($(this).val() !== undefined && $(this).val() !== null && $(this).val().trim() !== '') {
				fields[$(this).attr('name')] = {
					'id': $(this).attr('id'),
					'value': $(this).val()
				};
			}
		}
	});

	const data = { 'fields': fields };

	$.post(`${baseUrl}index.php/ebay_items/save_ebay_ad_draft`, data, function (response) {
		$(document).Toasts('create', {
			title: response.title,
			body: response.message,
			autoremove: true,
			fade: true,
			delay: 7000
		});
	});
}
