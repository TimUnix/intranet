

$(document).ready(function() {
    const baseUrl = $('#ebayApiScript').data('baseUrl');

    $('.request-change-description').click(function(event) {
        event.preventDefault();

        if (!changeDescriptionSku) {
            $(document).Toasts('create', {
                title: 'Change description error',
                body: 'An unexpected error occurred while attempting to change the description',
                autohide: true,
                delay: 7000
            });
            
            return;
        }

        $('.change-description-spinner-container').toggleClass('d-none');

        const store = $('.current-item-store').html();
        const itemNumber = $('.current-item-number').html();
        const sku = $('.current-item-sku').text();
        const newDescription = $('.new-description').val();
        const id = $('.current-id').html();
        const previousDescription = $(".current-description").text();
        const adActionChangeDescription = 2;

        const cellItemNumber = $(`td[data-value="${itemNumber}"]`);
        if (!cellItemNumber) return;

        const cellDescription = cellItemNumber.closest("tr").find(".item-title");
        if (!cellDescription) return;

        const linkDescription = cellDescription.find("a");
        if (!linkDescription) return;

        const data = {
            'store': store,
            'item_number': itemNumber,
            'sku': changeDescriptionSku,
            'new_description': newDescription,
            'previous_description' : previousDescription
        };

        const data_desc = {
            'id': id,
            'description': newDescription
        };

        const data_log = {
            "item_number": itemNumber,
            "new_value": newDescription,
            "old_value": previousDescription,
            "action_type_ads": adActionChangeDescription
        };

        $.post(`${baseUrl}index.php/ebay_api/change_description_request`, data, function(response) {
            if (response.is_error) {
                response.errors.forEach(function(error) {
                    $(document).Toasts('create', {
                        title: 'Description Change Failed',
                        body: error.longMessage,
                        autohide: true,
                        delay: 4000
                    });
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Description Change Successful',
                body: `Successfully changed the description for the item with id ${itemNumber}`,
                autohide: true,
                delay: 4000
            });

            $.post(`${baseUrl}index.php/ebay_items/log_ebay_ad_change`, data_log);

            $.post(`${baseUrl}index.php/ebay_items/update_description`, data_desc, function(msg){
                cellDescription.data("value", newDescription);
                linkDescription.html(newDescription);
            });
        }).always(function() {
			$('.change-description-spinner-container').toggleClass('d-none');
			$('#editDescriptionModal').modal('hide');
			$('.new-description').val('');
        });
    });

    $('#newEbayAdStore').on('change', function() {
        const store = $(this).val();

        const data = { 'store': store };

        $.post(`${baseUrl}index.php/ebay_items/load_business_policies`, data, function(response) {
            $('.ebay-business-policies-container').html(response);
        });
    });

    $('#newEbayAdPromotedListings').on('change', function() {
        $('#newEbayAdPromotionRate').prop('disabled', $(this).val() === '');
    });

    $('#newEbayAdCategory').on('change', function() {
        const categoryId = $(this).val();

        $('.new-ebay-ad-category-specifics-container').html(spinner);
        
        if (categoryId === '') {
            $('.new-ebay-ad-category-specifics-container').html('Unable to load fields for this category');
            return;
        }
        
        const data = { 'category_id': categoryId };

        $.post(`${baseUrl}index.php/ebay_items/get_category_specifics`, data, function(response) {
            $('.new-ebay-ad-category-specifics-container').html(response);
        }).then(function() {
            $('#newEbayAdCondition').on('change', function() {
                const selectedCondition = $(this).find(':selected').val();
                const newConditions = ['NEW', 'LIKE_NEW', 'NEW_OTHER', 'NEW_WITH_DEFECTS'];
                const isItemNew = newConditions.includes(selectedCondition);

                if (isItemNew) {
                    $('.new-ebay-ad-condition-description-container').html('');
                } else {
                    $.post(`${baseUrl}index.php/ebay_items/show_condition_description_field`, function(response) {
                        $('.new-ebay-ad-condition-description-container').html(response);
                    });
                }
            });
        });
    });

    $('.btnEndItemContinue').click(function() {
        $('.end-item-spinner-container').toggleClass('d-none');

        const itemNumber = $('.endItemTitle').data('itemNumber');
        const itemRow = $('.endItemTitle').data('itemRow');
        const store = $('.endItemTitle').data('store');
        const sku = $('.endItemTitle').data('sku');
        
        const data = { 'item_number': itemNumber, 'store': store, 'sku': encodeURIComponent(sku) };

        $.post(`${baseUrl}index.php/ebay_api/end_item_request`, data, function(response) {
            $('#endItemModal').modal('hide');
            $('.end-item-spinner-container').toggleClass('d-none');

            if (response.is_error) {

		const errorMessage = $('<ul></ul>').html(
		    response.errors.map(function(error) { return `<li>${error}</li>`; })
		);
		
                $(document).Toasts('create', {
                    title: 'End Item Failed',
                    body: errorMessage,
                    autohide: true,
		            autoremove: true,
                    fade: true,
                    delay: 15000
                });
            } else {
                const row = $('#product_table_body').children().eq(itemRow);
                const rowItemNumber = row.find('.item-item-number').data('itemNumber');
         
                if (rowItemNumber === itemNumber) {
                    row.remove();
                }
           
                $(document).Toasts('create', {
                    title: response.title,
                    body: response.message,
                    autohide: true,
                    autoremove: true,
                    fade: true,
                    delay: 15000
                });
            }
        });
    });

    $('.request-change-ad-price').click(function(event) {
        event.preventDefault();

        $('.change-ad-price-spinner-container').toggleClass('d-none');

        const store = $('.change-ad-price-current-item-store').html();
        const itemNumber = $('.change-ad-price-current-item-number').html();
        const sku = $('.change-ad-price-current-item-sku').html();
        const newPrice = $('.new-ad-price').val();
        const currentPrice = $('.change-ad-price-current-price').html();
        const id = $('.change-ad-price-current-id').html();
        const changePriceAction = 3;

        const cellItemNumber = $(`td[data-value="${itemNumber}"]`);
        if (!cellItemNumber) return;

        const cellPrice = cellItemNumber.closest("tr").find(".item-price");
        if (!cellPrice) return;

        const dataRemote = {
            'store': store,
            'item_number': itemNumber,
            'sku': sku,
            'new_value': newPrice,
            'action_type_ads': changePriceAction
        };

        const dataUpdate = {
            'id': id,
            'price': newPrice
        };

        const dataLog = {
            "item_number": itemNumber,
            "old_value": currentPrice,
            "new_value": newPrice,
            "action_type_ads": changePriceAction
        };


        $.post(`${baseUrl}index.php/ebay_api/change_ad_price_quantity_request`, dataRemote, function(response) {
            if (response.is_error) {
                response.errors.forEach(function(error) {
                    $(document).Toasts('create', {
                        title: 'Price Change Failed',
                        body: error.message,
                        autohide: true,
                        delay: 4000
                    });
                });
                
                return;
            }

            $(document).Toasts('create', {
                title: 'Price Change Successful',
                body: `Successfully changed the price for the item with id ${itemNumber}`,
                autohide: true,
                delay: 4000
            });

            $.post(`${baseUrl}index.php/ebay_items/log_ebay_ad_change`, dataLog);

            $.post(`${baseUrl}index.php/ebay_items/update_price`, dataUpdate, function() {
                const changePriceButton = cellPrice.find('.edit-price-ebay-ads').clone(true);
                cellPrice.data("value", newPrice);
                cellPrice.html(`\$${newPrice}`);
                changePriceButton.appendTo(cellPrice);
            });
        }).always(function() {
            $('.change-ad-price-spinner-container').toggleClass('d-none');
            $('#changeAdPriceModal').modal('hide');
            $('.new-ad-price').val('');
        });

        location.reload();
    });

    $('.request-change-ebay-quantity').click(function() {
        $('.change-quantity-spinner-container').toggleClass('d-none');

        const store = $('.change-quantity-current-item-store').html();
        const itemNumber = $('.change-quantity-current-item-number').html();
        const sku = $('.change-quantity-current-item-sku').html();
        const newQuantity = $('.new-quantity').val();
        const currentQuantity = $('.change-quantity-current-quantity').html();
        const id = $('.change-quantity-current-id').html();
        const changeQuantityAction = 6;

        const cellItemNumber = $(`td[data-value="${itemNumber}"]`);
        if (!cellItemNumber) return;

        const cellQuantity = cellItemNumber.closest("tr").find(".item-quantity");
        if (!cellQuantity) return;

        const dataRemote = {
            'store': store,
            'item_number': itemNumber,
            'sku': sku,
            'new_value': newQuantity,
            'action_type_ads': changeQuantityAction
        };

        const dataUpdate = {
            'id': id,
            'quantity': newQuantity
        };

        const dataLog = {
            "item_number": itemNumber,
            "old_value": currentQuantity,
            "new_value": newQuantity,
            "action_type_ads": changeQuantityAction
        };

        $.post(`${baseUrl}index.php/ebay_api/change_ad_price_quantity_request`, dataRemote, function(response) {
            if (response.is_error) {
                response.errors.forEach(function(error) {
                    $(document).Toasts('create', {
                        title: 'Price Change Failed',
                        body: error.message,
                        autohide: true,
                        delay: 4000
                    });
                });
                
                return;
            }

            $(document).Toasts('create', {
                title: 'Price Change Successful',
                body: `Successfully changed the quantity for the item with id ${itemNumber}`,
                autohide: true,
                delay: 4000
            });

            $.post(`${baseUrl}index.php/ebay_items/log_ebay_ad_change`, dataLog);

            $.post(`${baseUrl}index.php/ebay_items/update_quantity`, dataUpdate, function() {
                const changeQuantityButton = cellQuantity.find('.btn-toggle-change-ebay-quantity').clone(true);
                cellQuantity.data("value", newQuantity);
                cellQuantity.html(newQuantity);
                changeQuantityButton.appendTo(cellQuantity);
            });
        }).always(function() {
            $('.change-quantity-spinner-container').toggleClass('d-none');
            $('#changeQuantityModal').modal('hide');
            $('.new-quantity').val('');
        });

        location.reload();
    });

    $('#newEbayAdBestOffer').on('change', function() {
        $('.best-offer').prop('disabled', !$(this).prop('checked'));
    });

    $('#newEbayAdLot').on('change', function() {
        $('#newEbayAdLotAmount').prop('disabled', !$(this).prop('checked'));
    });

    $("#newEbayAdPhoto").on('change', function() {
        const photoUploadForm = document.querySelector("#onPremisesUploadForm");

        $.post({
            url: `${baseUrl}index.php/ebay_items/upload_photo`,
            data: new FormData(photoUploadForm),
            processData: false,
            contentType: false,
            cache: false,
            success: function(response) {
                $(".image-display").html("");

                response.forEach(function(image) {
                    $('.image-display').append(
                        `<img src="${baseUrl}uploads/${image.filename}" width="180" class="mx-2" `
                            + `data-filetype="${image.filetype}" data-filename="${image.filename}">`
                    );
                });
            }
        });
    });

    $('#newEbayAdTitle').on('keyup change', function() {
        validateNewAdForm();
    });

    $('#newEbayAdForm').on('submit', function(event) {
        event.preventDefault();

        const sku = $('#newEbayAdSku').val();
        const title = $('#newEbayAdTitle').val();
        const description = $('#newEbayAdDescription').val();
        const store = $('#newEbayAdStore').val();
        const price = $('#newEbayAdPrice').val();
        const quantity = $('#newEbayAdQuantity').val();
        const weight = $('#newEbayAdWeight').val();
        const format = $('#newEbayAdFormat').val();
        const duration = $('#newEbayAdDuration').val();
        const category = $('#newEbayAdCategory').val();
        const condition = $('#newEbayAdCondition').val();
        const location = $('#newEbayAdItemLocation').val();
        const lot = $('#newEbayAdLotAmount').val();
        const lotEnabled = $('#newEbayAdLot').prop('checked');
        const bestOfferEnabled = $('#newEbayAdBestOffer').prop('checked');
        const bestOfferMin = $('#newEbayAdBestOfferMin').val();
        const bestOfferMax = $('#newEbayAdBestOfferMax').val();
        const upc = $('#newEbayAdUPC').val();
        const conditionDescription = $('#newEbayAdConditionDescription').val();
        const shippingPolicy = $('#newEbayAdShippingPolicy').val();
        const campaignId = $('#newEbayAdPromotedListings').val();
        const promotionRate = $('#newEbayAdPromotionRate').val();
        const volumePriceTwoOrMore = $('newEbayAdVolumePrice2OrMore').val();
        const volumePriceThreeOrMore = $('newEbayAdVolumePrice3OrMore').val();
        const volumePriceFourOrMore = $('newEbayAdVolumePrice4OrMore').val();
        const photos = [];

        if (title.length > 80) {
            $('.new-ebay-ad-error-message').html('Title is too long. The title can only be up to 80 characters long');

            if ($('.new-ebay-ad-error-message').hasClass('d-none')) {
                $('.new-ebay-ad-error-message').removeClass('d-none');
            }

            return;
        }

        $('.new-ebay-ad-spinner-container').removeClass('d-none');
        
        if (!$('.new-ebay-ad-error-message').hasClass('d-none')) {
            $('.new-ebay-ad-error-message').addClass('d-none');
        }

        $('.image-display').children().each(function() {
            const filepath = $(this).prop('src');
            const filetype = $(this).data('filetype');
            const filename = $(this).data('filename');
            
            photos.push({ 'filepath': filepath, 'filetype': filetype, 'filename': filename });
        });

        const photoData = { 'photos' : photos, 'store': store, 'title': title };

        $.post(`${baseUrl}index.php/ebay_api/upload_photos`, photoData, function(response) {
            const noPhotoError = 0;

            if (response.is_error) {
                if (response.error_type === noPhotoError) {
                    $('.new-ebay-ad-error-message').removeClass('d-none');
                    $('.new-ebay-ad-error-message').html('Please provide at least one photo.');
                    $('.new-ebay-ad-spinner-container').addClass('d-none');
                } else {
                    $(document).Toasts('create', {
                        title: response.error_title,
                        body: `${response.short_message} <br> ${response.long_message}`,
                        autohide: true,
                        delay: 15000
                    });
                }

                return;
            }

            const data = {
                'best_offer_enabled': bestOfferEnabled,
                'best_offer_max': bestOfferMax,
                'best_offer_min': bestOfferMin,
                'campaign_id': campaignId,
                'category': category,
                'condition': condition,
                'condition_description': conditionDescription,
                'description': description,
                'duration': duration,
                'format': format,
                'location': location,
                'lot_enabled': lotEnabled,
                'lot': lot,
                'photos': response.photo_urls,
				'local_photos': photos.map(function(photo) { return photo.filename; }),
                'price': price,
                'promotion_rate': promotionRate,
                'quantity': quantity,
                'shipping_policy': shippingPolicy,
                'sku': sku,
                'store': store,
                'title': title,
                'upc': upc,
                'volume_price_two_or_more': volumePriceTwoOrMore,
                'volume_price_three_or_more': volumePriceThreeOrMore,
                'volume_price_four_or_more': volumePriceFourOrMore,
                'weight': weight
            };

            $('.new-ebay-ad-category-specific-container').each(function(i, container) {
                const categorySpecificKey = $(container).find('.new-ebay-ad-category-specific-key').data('key')
                      .trim();
                const categorySpecific = $(container).find('.new-ebay-ad-category-specific');
                const nodeType = categorySpecific.prop('nodeName');

                if (nodeType === 'INPUT') {
                    const inputType = categorySpecific.prop('type');

                    if (inputType === 'checkbox') {
                        const specifics = [];

                        $(container).find('.new-ebay-ad-category-specific:checked').each(function(i, checkbox) {
                            specifics.push($(checkbox).val());
                        });
                        
                        data[categorySpecificKey] = specifics;
                    } else if (inputType === 'text') {
                        const categorySpecificValue = $(container).find('.new-ebay-ad-category-specific').val();

                        if (categorySpecificValue.trim() !== '') {
                            data[categorySpecificKey] = categorySpecificValue;
                        }
                    }
                } else if (nodeType === 'SELECT') {
                    data[categorySpecificKey] = categorySpecific.val();
                }
            });

            $.post(`${baseUrl}index.php/ebay_api/create_ebay_ad`, data, function(createAdResponse) {
                const missingFieldsError = 0;

                $('.new-ebay-ad-spinner-container').addClass('d-none');
                $('#createEbayAdModal').modal('toggle');
                $('#newEbayAdForm').trigger('reset');
                $('#onPremisesUploadForm').trigger('reset');
                $('.image-display').html('');

                if (createAdResponse.is_error) {
					saveDraft();
                    if (createAdResponse.error_type == missingFieldsError) {
                        $('.new-ebay-ad-error-message').html(createAdResponse.message);
                    } else {
                        createAdResponse.errors.forEach(function(error) {
                            $(document).Toasts('create', {
                                title: 'Create eBay Ad Error',
                                body: error.longMessage,
                                autohide: true,
                                delay: 15000
                            });
                        });
                    }
                } else {
                    $(document).Toasts('create', {
                        title: createAdResponse.title,
                        body: createAdResponse.message,
                        autohide: true,
                        delay: 15000
                    });
                }
            });
        });            
    });
});

function validateNewAdForm() {
    const titleInput = $('#newEbayAdTitle');
    const titleCounter = $('.new-ebay-ad-title-counter');
    const title = titleInput.val();
    const maxLength = 80;

    if (title.length > maxLength ) {
        if (!titleInput.hasClass('is-invalid')) {
            $('#newEbayAdTitle').addClass('is-invalid');
        }

        if (!titleCounter.hasClass('text-danger')) {
            titleCounter.addClass('text-danger');
        }
    } else {
        titleCounter.removeClass('text-danger');
        titleInput.removeClass('is-invalid');
    }

    $('.new-ebay-ad-title-counter').html(maxLength - title.length);
}
