let baseUrl;
let trendChart;
let changeDescriptionSku;
const errorSkuEmpty = 0;
const errorPriceEmpty = 0;

const columnsToCopy = {
    // description: 5,
    //  condition: 10,
    //  price: 18,
    // total: 18,
    // bar: 5,
    itemnumber: 1
};

// To be used in dynamicTableColumns.js

// Used in sorting
let currentHeader = '';
let lastHeader = '';
let isColumnSortingReversed = false;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {
    let bannervalue;
    let itemsLimit = 100;
    let itemsOffset = 0;
    let itemsRequested = false;
    const colors = [
        '#389dd6', '#614647', '#db356b', '#8a604d',
        '#806e4d', '#367bd9', '#6343bf', '#41544e',
        '#58e82c', '#a9b547', '#87734d', '#4c9c6e',
        '#99964d', '#968a4d', '#b34774', '#3f3c4a',
        '#4d826f', '#2e2f33', '#475f63', '#6b6a49',
        '#41544c', '#4e6b49', '#de33b2', '#8a4d5e',
        '#6cb547', '#58f522', '#3f4238', '#c4b941',
        '#5ca64b', '#2aeba1', '#595e45', '#91e82c',
        '#47664d', '#4c7d64', '#5c5b44', '#313838',
        '#424d57', '#424154', '#445742', '#d63857',
        '#41544b', '#41c48c', '#a1834c', '#49616b',
        '#c7406c', '#db356b', '#7a8a4d', '#4d857e',
        '#62784c', '#704a69', '#b54768', '#3541db',
        '#4d7485', '#f02687', '#383331', '#40c7b7',
        '#c24d42', '#a2d638', '#3d353a', '#f7e120'
    ];

    $('.filters').select2();

    baseUrl = $('#ebayScript').data('baseUrl');
    $('#NoneSoldSelected').prop('disabled', true);
    syncStickyHeader();

    const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterSearchSelected = parameters.get('searchSelected');
    const parameterFilterNoneSold = parameters.get('filterNoneSold');
    const parameterCategoryFilters = parameters.get('categories');
    const parameterChannelFilters = parameters.get('seller');
    const parameterSwitch = parameters.get('switches') == 1;
    const isAnItemRequested = searchItem !== null;

    if( parameterSwitch == 1) {
        $('#switchWeekNoneSold').prop('checked', parameterSwitch);
        if ($('#switchWeekNoneSold').prop('checked', parameterSwitch)){
            $('#NoneSoldSelected').prop('disabled', false);
            $('#NoneSoldSelected').val(parameterFilterNoneSold);
        } 
    }

    if (isAnItemRequested) {
        $('#searchSelected').val(parameterSearchSelected);    

        if (parameterCategoryFilters !== '' ) {
            const categoriesSelected = parameterCategoryFilters.split(',').map(function(number) {
                return Number(number);
            });

            $('.category-filter').each(function() {
                const position = Number($(this).data('position'));

                if (categoriesSelected.includes(position)) {
                    $(this).prop('checked', true);
                }
            });
        }

        const categories = $('.category-filter:checked').map(function() {
            return $(this).val();
        }).toArray();

        if (parameterChannelFilters !== '' ) {
            const channelsSelected = parameterChannelFilters.split(',').map(function(number) {
                return Number(number);
            });

            $('.channel-filter').each(function() {
                const position = Number($(this).data('position'));

                if (channelsSelected.includes(position)) {
                    $(this).prop('checked', true);
                }
            });
        } 

        const channels = $('.channel-filter:checked').map(function() {
            return $(this).val();
        }).toArray();
        
        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'filterNoneSold': parameterFilterNoneSold,
            'items_offset': itemsOffset,
            'categories' : categories,
            'channels' : channels
        };

        $('#itemSearch').val(searchItem);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#toggleExportCSVModal').prop('disabled', true);
        $('.category-filter:not(:checked)').prop('disabled', true);
        $('.channel-filter:not(:checked)').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.post(`${baseUrl}index.php/ebay_items/load_summary`, data, function(response){
    
            $("#total-items").html(response.total);
        });
        $.post(`${baseUrl}index.php/ebay_items/search_item`, data, function(response) {
          
            $('#product_table_body').html(response);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
            $('.sticky-table').width(tableWidth);
            $('#toggleExportCSVModal').prop('disabled', false);
            $('.category-filter:not(:checked)').prop('disabled', false);
            $('.channel-filter:not(:checked)').prop('disabled', false);
            syncStickyHeader();
        });

        saveHistory(searchItem);
    }

    $('#switchWeekNoneSold').change(function(){
        if ($('#switchWeekNoneSold').is(':checked')){
            $('#NoneSoldSelected').prop('disabled', false);
            search();
        } else {
            $('#NoneSoldSelected').prop('disabled', true);
            search();
        }
        
    });

    $

    $('#NoneSoldSelected').change(function(){
        if ($('#switchWeekNoneSold').is(':checked')){
            $('#NoneSoldSelected').prop('disabled', false);
            search();
        } else {
            $('#NoneSoldSelected').prop('disabled', true);
        }
    });

    $('.btnClearCategoryFilters').on('click', function() {
        $('input.category-filter').each(function() {
            $(this).prop('checked', false);
        });

        search();
    });

    $('.btnClearChannelFilters').on('click', function() {
        $('input.channel-filter').each(function() {
            $(this).prop('checked', false);
        });

        search();
    });
    
    $('#product-table').on('click', 'th.header-sortable', function() {
        triggerTableSort(this);
        syncStickyHeader();
    });

    $('.sticky-table').on('click', 'th.header-sortable', function() {
        triggerTableSort(this);
        syncStickyHeader();
    });

    $(document).scroll(function() {
        const rowCount = $('tbody > tr').length;

        // if the rows initially returned are less than 100, there are no more rows to load further
        if (rowCount < itemsLimit) return;
        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('tbody > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            itemsOffset += itemsLimit;

            const categories = $('.category-filter:checked').map(function() {
                return $(this).val();
            }).toArray();

            const channels = $('.channel-filter:checked').map(function() {
                return $(this).val();
            }).toArray();

            $('#NoneSoldSelected').change(function() {
                search();
            });

            const data = {
                'searchItem': searchItem,
                'searchSelected' : parameterSearchSelected,
                'orderBy': currentHeader,
                'sortOrder': isColumnSortingReversed ? 'desc' : 'asc',
                'filterNoneSold': parameterFilterNoneSold,
                'items_offset': itemsOffset,
                'categories' : categories,
                'channels' : channels
            };

            $.post(`${baseUrl}index.php/ebay_items/load_summary`, data, function(response){
 
                $("#total-items").text(response.total);
            });

            $.post(`${baseUrl}index.php/ebay_items/search_item`, data, function(response) {
  
                $('#product_table_body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    $('table#product-table').on("click", '.new-listing', function(event) {
        const button = $(event.target).closest('.new-listing');

        if (!button) return;

        const sku = button.closest('tr').find('.item-sku');
        const price = button.closest('tr').find('.item-price');
        const item_number = button.closest('tr').find('.item-item-number').text().trim();


        if (!sku || !price) return;
        $('.current-item-sku').val(sku.data('itemSku'));
        $('.current-item-price').val(price.data('value'));
        $('#current-item-number').text(item_number);
    });

    $("#submit").on("click", function(event){
        if($("#sku").val() == " "){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku').focus();

        } else if (($('#request').val() == 1 || $('#request').val() == 3) && $('#price').val() === '') {
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);
            $('#price').focus();
        } else {
            event.preventDefault();
            
            if($("#bannerone").is(":checked")){
                bannervalue = "YES";

            }else if($("#bannertwo").is(":checked")){
                bannervalue = "NO";
            }

            var datas = {
                'sku' : $("#sku").val(),
                'shipping': $("#shipping").val(),
                'weight': $("#weight").val(),
                'price': $("#price").val(),
                'lot': $('#lot').val(),
                'moved': $('#moved').val(),
                'banner' : bannervalue,
                'notes' : $("#notes").val(),
                'request': $("#request").val(),
                ajax : '1'
            };

            $.ajax({
                url : `${baseUrl}index.php/ebay_items/create_listing`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == errorSkuEmpty) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else if (response == errorPriceEmpty) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);

                        $('#price').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else {
                        search();
                    }
                }
            });
        }
    });

    $('table#product-table').on("click", '.btn-edit-price-listing', function(event) {
        const button = $(event.target).closest('.btn-edit-price-listing');

        if (!button) return;

        const sku = button.closest('tr').find('.item-sku');
        const price = button.closest('tr').find('.item-price');
        
        
        if (!sku || !price) return;
        
        $('#request').find(':selected').prop('selected', false);
        $('#request').find('option[value="3"]').prop('selected', true);
        $('.current-item-sku').val(sku.data('itemSku'));
        $('.current-item-price').val(price.data('value'));
    });

    $('table#product-table').on('click', '.btn-toggle-change-ebay-quantity', function() {
        const cellQuantity = $(this).closest('.item-quantity');
        
        if (!cellQuantity) return;
        
        const row = cellQuantity.closest('tr');
        const cellSku = row.find('.item-sku');
        const cellTitle = row.find('.item-title');
        const cellItemNumber = row.find('.item-item-number');
        const cellId = row.find('.item-id');
        const cellSeller = row.find('.item-seller');

        if (!row || !cellSku || !cellTitle || !cellItemNumber || !cellId || !cellSeller) return;

        const sku = cellSku.data('itemSku');
        const title = cellTitle.data('value');
        const itemNumber = cellItemNumber.data('itemNumber');
        const id = cellId.data('value');
        const seller = cellSeller.data('value');
        const quantity = cellQuantity.data('value');

        $('.change-quantity-current-id').html(id);
        $('.change-quantity-current-item-sku').html(sku);
        $('.change-quantity-current-quantity').html(quantity);
        $('.new-quantity').val(quantity);
        $('.change-quantity-current-description').html(title);
        $('.change-quantity-current-item-number').html(itemNumber);
        $('.change-quantity-current-item-store').html(seller);
    });

    $("#submitPrice").click(function(event){
        if($("#sku_price").val() == " " && !$("#sku_price").val()){
            $("#success_price").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku_price').focus();
        } else if (($('#request_price').val() == 1 || $('#request_price').val() == 6) && $('#price_price').val() === '') {
            $("#success_price").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);
            $('#price_price').focus();
        } else {
            event.preventDefault();
            
            if($("#bannerone").is(":checked")){
                bannervalue = "YES";

            }else if($("#bannertwo").is(":checked")){
                bannervalue = "NO";
            }

            var datas = {
                'sku' : $("#sku_price").val(),
                'shipping': $("#shipping_price").val(),
                'weight': $("#weight_price").val(),
                'price': $("#price_price").val(),
                'lot': $('#lot_price').val(),
                'moved': $('#moved_price').val(),
                'banner' : bannervalue,
                'notes' : $("#notes_price").val(),
                'request': $("#request_price").val(),
                ajax : '1'
            };
            $.ajax({
                url : `${baseUrl}index.php/ebay_items/create_listing`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == errorSkuEmpty) {
                        $("#success_price").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                        </div>`);

                        $('#sku_price').focus();
                        $("#submitPrice").html(`<button type="button" class="btn btn-primary" id="submitPrice">Submit</button>`);
                    } else if (response == errorPriceEmpty) {
                        $("#success_price").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                        </div>`);

                        $('#price_price').focus();
                        $("#submitPrice").html(`<button type="button" class="btn btn-primary" id="submitPrice">Submit</button>`);
                    } else {
                        search();
                    }
                }
            });
        }
    });

    $('table#product-table').on("click", '.btn-edit-description-listing', function(event) {
        const button = $(event.target).closest('.btn-edit-description-listing');

        if (!button) return;

        const sku = button.closest('tr').find('.item-sku');
        const price = button.closest('tr').find('.item-price');
        
        if (!sku || !price) return;

        $('#request').find(':selected').prop('selected', false);
        $('#request').find('option[value="7"]').prop('selected', true);
        $('.current-item-sku').val(sku.data('itemSku'));
        $('.current-item-price').val(price.data('value'));
    });

    $("#submitDesc").click(function(event){
        if($("#sku_desc").val() == " "){
            $("#success_desc").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku_desc').focus();
        } else {
            event.preventDefault();
            
            if($("#bannerone").is(":checked")){
                bannervalue = "YES";

            }else if($("#bannertwo").is(":checked")){
                bannervalue = "NO";
            }

            var datas = {
                'sku' : $("#sku_desc").val(),
                'shipping': $("#shipping_desc").val(),
                'weight': $("#weight_desc").val(),
                'price': $("#price_desc").val(),
                'lot': $('#lot_desc').val(),
                'moved': $('#moved_desc').val(),
                'banner' : bannervalue,
                'notes' : $("#notes_desc").val(),
                'request': $("#request_desc").val(),
                ajax : '1'
            };
            $.ajax({
                url : `${baseUrl}index.php/ebay_items/create_listing`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == errorSkuEmpty) {
                        $("#success_desc").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);

                        $('#sku_desc').focus();
                        $("#submitDesc").html(`<button type="button" class="btn btn-primary" id="submitDesc">Submit</button>`);
                    } else if (response == errorPriceEmpty) {
                        $("#success_desc").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);

                        $('#price_desc').focus();
                        $("#submitDesc").html(`<button type="button" class="btn btn-primary" id="submitDesc">Submit</button>`);
                    } else {
                        search();
                    }
                }
            });
        }
    });

    $('table#product-table').on('click', '.btnEndItem', function() {
        const btnEndItem = $(this).closest('.btnEndItem');

        if (!btnEndItem) return;

        const row = btnEndItem.closest('tr');
        const cellTitle = row.find('.item-title');
        const cellItemNumber = row.find('.item-item-number');
        const cellStore = row.find('.item-seller');
        const cellSku = row.find('.item-sku');

        if (!cellTitle || !cellItemNumber || !row || !cellStore) return;

        const title = cellTitle.data('value');
        const itemNumber = cellItemNumber.data('value');
        const itemRow = row.index();
        const store = cellStore.data('value');
        const sku = cellSku.data('value');

        $('.endItemTitle').text(title);
        $('.endItemTitle').data('itemNumber', itemNumber);
        $('.endItemTitle').data('itemRow', itemRow);
        $('.endItemTitle').data('store', store);
        $('.endItemTitle').data('sku', sku);
    });

    $('#endItemModal').on('hide.bs.modal', function() {
        $('.endItemTitle').text('');
        $('.endItemTitle').data('itemNumber', '');
        $('.endItemTitle').data('itemRow', '');
        $('.endItemTitle').data('store', '');
        $('.endItemTitle').data('sku', '');
    });

    $('#request').change(function() {
        if ($(this).val() == 2) {
            $('.current-item-price').val('');
        }
    });

    $('table#product-table').on("click", '.btn-edit-description-ebay-ads', function(event) {
        const button = $(event.target).closest('.btn-edit-description-ebay-ads');

        if (!button) return;

        const itemNumber = button.closest('tr').find('.item-item-number');
        const description = button.closest('tr').find('.item-title');
        const seller = button.closest('tr').find('.item-seller');
        const sku = button.closest('tr').find('.item-sku');
        const price = button.closest('tr').find('.item-price');
        const id = button.closest('tr').find('.item-id').text();
 
        if (!itemNumber || !description || !seller || !sku || !price) return;

        $('.current-item-number').html(itemNumber.data('value'));
        $('.current-item-sku').html(sku.data('itemSku'));
        changeDescriptionSku = sku.data('itemSku');
        $('.current-item-store').html(seller.data('value'));
        $('.current-description').html(description.data('value'));
        $('.new-description').val(description.data('value'));
        $('.current-item-price').html(price.data('value'));
        $('.current-id').html(id);

        $('#editDescriptionModal').modal('toggle');
    });

    $('table#product-table').on("click", '.btn-edit-price-ebay-ads', function(event) {
        const button = $(event.target).closest('.btn-edit-price-ebay-ads');

        if (!button) return;

        const itemNumber = button.closest('tr').find('.item-item-number');
        const description = button.closest('tr').find('.item-title');
        const seller = button.closest('tr').find('.item-seller');
        const sku = button.closest('tr').find('.item-sku');
        const price = button.closest('tr').find('.item-price');
        const id = button.closest('tr').find('.item-id');
    
        if (!itemNumber || !description || !seller || !sku || !price) return;

        $('.change-ad-price-current-item-number').html(itemNumber.data('value'));
        $('.change-ad-price-current-item-sku').html(sku.data('itemSku'));
        $('.change-ad-price-current-item-store').html(seller.data('value'));
        $('.change-ad-price-current-description').html(description.data('value'));
        $('.change-ad-price-current-price').html(`\$${price.data('value')}`);
        $('.new-ad-price').val(price.data('value'));
        $('.change-ad-price-current-id').html(id.data('value'));

        $('#changeAdPriceModal').modal('toggle');
    });

    $('table#product-table').on("click", '.btn-edit-price-low', function(event) {
        const button = $(event.target).closest('.btn-edit-price-low');

        if (!button) return;

        const itemNumber = button.closest('tr').find('.item-item-number');
        const description = button.closest('tr').find('.item-title');
        const seller = button.closest('tr').find('.item-seller');
        const sku = button.closest('tr').find('.item-sku');
        const priceLow = button.closest('tr').find('.item-price_research_market_low');
        const id = button.closest('tr').find('.item-id');
    
        if (!itemNumber || !description || !seller || !sku || !priceLow) return;

        $('.change-low-price-current-item-number').html(itemNumber.data('value'));
        $('.change-low-price-current-item-sku').html(sku.data('itemSku'));
        $('.change-low-price-current-item-store').html(seller.data('value'));
        $('.change-low-price-current-description').html(description.data('value'));
        $('.change-low-current-price').html(`\$${priceLow.data('value')}`);
        $('.newLowPrice').val(priceLow.data('value'));
        $('.change-low-price-current-id').html(id.data('value'));
        $('#changeLowModal').modal('toggle');
    });


    $('table#product-table').on("click", '.btn-edit-price-high', function(event) {
        const button = $(event.target).closest('.btn-edit-price-high');

        if (!button) return;

        const itemNumber = button.closest('tr').find('.item-item-number');
        const description = button.closest('tr').find('.item-title');
        const seller = button.closest('tr').find('.item-seller');
        const sku = button.closest('tr').find('.item-sku');
        const priceHigh = button.closest('tr').find('.item-price_research_market_high');
        const id = button.closest('tr').find('.item-id');
    
        if (!itemNumber || !description || !seller || !sku || !priceHigh) return;

        $('.change-high-price-current-item-number').html(itemNumber.data('value'));
        $('.change-high-price-current-item-sku').html(sku.data('itemSku'));
        $('.change-high-price-current-item-store').html(seller.data('value'));
        $('.change-high-price-current-description').html(description.data('value'));
        $('.change-high-current-price').html(`\$${priceHigh.data('value')}`);
        $('.newHighPrice').val(priceHigh.data('value'));
        $('.change-high-price-current-id').html(id.data('value'));

        $('#changeHighModal').modal('toggle');
    });

    $('table#product-table').on("click", 'button.item-logs', function(event) {
        const button = $(event.target).closest('.item-logs');
        const itemNumber = button.closest('tr').find('.item-item-number').data('value');

        const data = { 'item_number': itemNumber };

        $.post(`${baseUrl}index.php/ebay_items/get_logs`, data, function(response){
            $('#log').html(response);
        }).then(function() {
            $('#logsModal').modal('toggle');
        });
    });

    $('table#product-table').on("click", '.check', function(event) {
        const itemNumber = event.target.closest('.item-item-number');

        if (!itemNumber) return;

        const sku = $(event.target).closest('tr').find('.item-sku').data('itemSku');

        const data = { 'item_number': itemNumber.dataset.itemNumber };

        $.get(`${baseUrl}index.php/ebay_items/item_trend`, data, function(response) {
            if (trendChart) {
                $('.modal-graph-spinner').toggleClass('d-none');
                $('.trend-graph').toggleClass('d-none');
                
                trendChart.destroy();
            }
            
            $('.graph-description').html(`Recent trends for ${sku}.`);
            $('.modal-graph-spinner').toggleClass('d-none');
            $('.trend-graph').toggleClass('d-none');

            const label = response.trends.map(function(trend) {
                const dateOptions = { year: "numeric", month: "long", day: "numeric" };
                const date = new Date(trend.date).toLocaleDateString(undefined, dateOptions);
                return `${date} (\$${trend.current_price})`;
            });

            const trendChartCtx = $(".trend-graph")[0].getContext('2d');
            trendChart = new Chart(trendChartCtx, {
                type: 'line',
                data: {
                    labels: label,
                    datasets: [
                        {
                            label: 'Hits',
                            fill: false,
                            borderWidth: 1, 
                            lineTension: 0.5,
                            backgroundColor: colors[0],
                            borderColor: colors[0],
                            borderWidth: 3,                      
                            pointRadius: 2,                  
                            data: response.trends.map(function(trend) {
                                return trend.hit_count;
                            })
                        },
                        {
                            label: 'Sold',
                            fill: false,
                            borderWidth: 1, 
                            lineTension: 0.5,
                            backgroundColor: colors[31],
                            borderColor: colors[31],
                            borderWidth: 3,
                            pointRadius: 2,
                            data: response.trends.map(function(trend) {
                                return trend.quantity_sold;
                            })                            
                        },
                        {
                            label: 'Total Hits 7 Days',
                            fill: false,
                            borderWidth: 1, 
                            lineTension: 0.5,
                            backgroundColor: colors[52],
                            borderColor: colors[52],
                            borderWidth: 3,
                            pointRadius: 2,
                            data: response.trends.map(function(trend) {
                                return trend.total_hits_7_days === null ? 0 : trend.total_hits_7_days;
                            })
                        },
                        {
                            label: 'Total Sold 7 Days',
                            fill: false,
                            borderWidth: 1, 
                            lineTension: 0.5,
                            backgroundColor: colors[29],
                            borderColor: colors[29],
                            borderWidth: 3,
                            pointRadius: 2,
                            data: response.trends.map(function(trend) {
                                return trend.total_sold_7_days === null ? 0 : trend.total_sold_7_days;
                            })                            
                        },
                        {
                            label: 'Velocity',
                            fill: false,
                            borderWidth: 1, 
                            lineTension: 0.5,
                            backgroundColor: colors[2],
                            borderColor: colors[2],
                            borderWidth: 3,
                            pointRadius: 2,
                            data: response.trends.map(function(trend) {
                                return trend.velocity;
                            })                            
                        }
                    ]
                },
                options: {
                    responsive: true,
                    propagate: true
                }
            });
        });
    });

    $('#newListingModal').on('hidden.bs.modal', function() {
        $('#request').find(':selected').prop('selected', false);
        $('#request').find('option[value="1"]').prop('selected', true);
    });

    $('#searchColumn').click(function(event){
        event.preventDefault();
        var searchSelected = $('#searchSelected').val();

        const searchItem = $('#itemSearch').val();
        search();
    });

    $('#searchHistory').change(function(event) {
        const selected = $(event.target).find(':selected');

        $('#itemSearch').val(selected.val());

        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();
        $('#switchWeekNoneSold').is(':checked');

        search();
    });

    $('#exportCSV').click(function() {
        const spinner = $('.export-csv-spinner-container');

        if (spinner.hasClass('.d-none')) {
            spinner.removeClass('.d-none');
        }

        const columns = $('.export-column:checked').map(function() {
            return $(this).val();
        }).toArray();

        const categories = $('.category-filter:checked').map(function() {
            return $(this).val();
        }).toArray();

        const channels = $('.channel-filter:checked').map(function() {
            return $(this).val();
        }).toArray();

        const searchSelected = $('#searchSelected').val();
        const search = $('#itemSearch').val();

        const data = {
            'search': search,
            'searchSelected' : searchSelected,
            'filterNoneSold': parameterFilterNoneSold,
            'categories': categories,
            'channela' : channels,
            'columns': columns,
            'channels' : channels
        };

        $.post(`${baseUrl}index.php/ebay_items/generate_csv`, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `ebay_items_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();

                if (!spinner.hasClass('.d-none')) {
                    spinner.removeClass('.d-none');
                }

                $('#exportCsvModal').modal('hide');
            }
        });
    });

    $('#product_table_body').on("click", '.item-sku_qty', function(event) {
        const button_sku = $(event.target).closest('.item-sku_qty');
        if(!button_sku) return;
        const sku_qty = button_sku.closest('tr').find('.item-sku').data('itemSku');
        
        var datas = {
            'sku' : sku_qty,
            ajax  : '1'
        };

        $.post(`${baseUrl}index.php/ebay_items/get_total_quantity_sold`, datas, function(response){
            $('#sku_quantity_body').html(response);
        }).then(function() {
            $('#sku_quantity').modal('show');
        });

    });

    function triggerTableSort(clickedHeader) {
        const header = $(clickedHeader).closest('th.header-sortable');
        const column = header.data('column');
        currentHeader = column;
        itemsOffset = 0;

        const clickedTable = header.closest('table');
        const otherTable = clickedTable.hasClass('main-table') ? $('.sticky-table') : $('.main-table');
        const otherHeader = otherTable.find('tr').find(`th[data-column="${column}"`);

        if (currentHeader === lastHeader) {
            header.find('.fa-sort-up').toggleClass('d-none');
            header.find('.fa-sort-down').toggleClass('d-none');
            otherHeader.find('.fa-sort-up').toggleClass('d-none');
            otherHeader.find('.fa-sort-down').toggleClass('d-none');

            isColumnSortingReversed = !isColumnSortingReversed;
        } else {
            $('.fa-sort-up').removeClass('d-none');
            $('.fa-sort-down').removeClass('d-none');
            $('.fa-sort-up').addClass('d-none');
            $('.fa-sort-down').addClass('d-none');
            header.find('.fa-sort-up').toggleClass('d-none');
            otherHeader.find('.fa-sort-up').toggleClass('d-none');

            isColumnSortingReversed = false;
        }

        sortRows(column);
    }

    function sortRows(header, isTriggeredByLazyLoad) {
        const searchParams = new URLSearchParams(window.location.search);
        const searchItem = searchParams.get('search');
        const paramOrderBy = searchParams.get('orderBy');
        const parameterSearchSelected = searchParams.get('searchSelected');
        const parameterFilterNoneSold = parameters.get('filterNoneSold');

        const categories = $('.category-filter:checked').map(function() {
            return $(this).val();
        }).toArray();

        const channels = $('.channel-filter:checked').map(function() {
            return $(this).val();
        }).toArray();

        $('#NoneSoldSelected').change(function() {
            search();
        });

        const orderBy = header;
        const sortOrder = isColumnSortingReversed ? 'desc' : 'asc';
        const url = new URL(window.location.href);
        url.searchParams.set('orderBy', header);
        url.searchParams.set('sortOrder', sortOrder);

        const state = {
            'header': header,
            'sortOrer': sortOrder
        };

        window.history.pushState(state, document.title, url.href);

        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'orderBy': header,
            'sortOrder': sortOrder,
            'filterNoneSold': parameterFilterNoneSold,
            'items_offset': itemsOffset,
            'categories' : categories,
            'channels' : channels
        };

        $.post(`${baseUrl}index.php/ebay_items/load_summary`, data, function(response){
 
            $("#total-items").html(response.total);
        });

        $.post(`${baseUrl}index.php/ebay_items/search_item`, data, function(response) {
    
            $('#product_table_body').html(response);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
            $('.sticky-table').width(tableWidth);
            $('#toggleExportCSVModal').prop('disabled', false);
            $('.category-filter:not(:checked)').prop('disabled', false);
            syncStickyHeader();
            lastHeader = header;
        });
    }
});

function saveHistory(search) {
    $.post(`${baseUrl}index.php/ebay_items/save_search_history`, { 'search': search });
}

function search() {
    const searchItem = encodeURIComponent($('#itemSearch').val());
    const searchSelected = $('#searchSelected').val();
    const sortOrder = isColumnSortingReversed ? 'desc' : 'asc';

    const categoryFilters = $('.category-filter:checked').map(function() {
        return $(this).data('position');
    }).toArray().join(',');

    const channelFilters = $('.channel-filter:checked').map(function() {
        return $(this).data('position');
    }).toArray().join(',');
    
    const isFilterswitches = $('#switchWeekNoneSold').is(':checked') ? 1 : 0;
        if ($('#switchWeekNoneSold').is(':checked')){
            var Filternonesold = encodeURIComponent($('#NoneSoldSelected').val());
        }
    
    const route = `${baseUrl}index.php/ebay_items?search=${searchItem}&searchSelected=${searchSelected}`
          + `&categories=${categoryFilters}&seller=${channelFilters}&filterNoneSold=${Filternonesold}&switches=${isFilterswitches}`;

    window.location.href = route;
}

