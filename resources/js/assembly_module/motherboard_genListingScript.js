$(document).ready(function() {

baseUrl = $('#assemblyMBgenListingScript').data('baseUrl');

$('#generateMB_genListing').click(function(event){
        const qty = 'x';
        const none = 'None';
        const empty =' ';
        const node = 'U';
        const comma = ',';
        const sales_rep = $('#sales_rep').html();
        const date = new Date().toLocaleString();
        const author = sales_rep + empty + date;
       /* if($('#motherboard2').val() == null && $('#motherboard_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option </h5>
                    </div>`);
            return;
        }else if($('#motherboard2').val() != null && $('#motherboard_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`);
            return;
        }
        else if($('#cpu2').val() == null && $('#cpu_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);
            return;
        }else if($('#cpu2').val() != null && $('#cpu_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
            return;
        }
          else if($('#memory2').val() == null && $('#memory_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);
            return;
        }else if($('#memory2').val() != null && $('#memory_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);
            return;
        }else if($('#slot_mb').val() == '' && $('#slot_qty_mb').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot_mb').val() != '' && $('#slot_qty_mb').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot_mb2').val() == '' && $('#slot_qty_mb2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot_mb2').val() != '' && $('#slot_qty_mb2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot_mb3').val() == '' && $('#slot_qty_mb3').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot_mb3').val() != '' && $('#slot_qty_mb3').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#psu_watts2').val() == '' && $('#psu_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No selected PSU</h5>
                    </div>`);
            return;
        }else if($('#psu_watts2').val() != '' && $('#psu_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
            return;
        }else if($('#mounting2').val() == '' && $('#mounting_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            return;
        }else if($('#mounting2').val() != '' && $('#mounting_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
            return;
        }*/

        $('#build_id-genspec-mb').text($('#build_id2').html());
        $('#author_date-genspec-mb').text(author);
        $('#custom_label-genspec-mb').text($('#build_id2').html() + empty + $('#motherboard2').val());

        $('#ob_nic-genspec-mb').text($('#ob_nic_mb').html());
        $('#pcie_slot-genspec-mb').text($('#pcie_slot_mb').html());
        
        if($('#cpu2').val() == null && $('#cpu_qty2').val() == ''){
            $('#motherboard_genericListing').modal('show');
            $('#cpu-genspec-qty-mb').text(empty);
            $('#cpu-genspec-mb').text(none);
            $('#cpu-genqty-symbol-mb').text(qty).remove();

        }
        else if($('#cpu2').val() != null && $('#cpu_qty2').val() != ''){
            $('#motherboard_genericListing').modal('show')
            //$('#cpu-spec').text($('#cpu').find(':selected').text());
            $('#cpu-genspec-mb').text($('#cpu2').text());
            $('#cpu-genspec-qty-mb').text($('#cpu_qty2').val());
            $('#cpu-genqty-symbol-mb').text(qty);
        }

        
        if($('#memory2').val() == null && $('#memory_qty2').val() == ''){
            $('#motherboard_genericListing').modal('show');
            $('#memory-genspec-qty-mb').text(none);
            $('#memory-genspec-mb').text(empty);
            $('#memory-genqty-symbol-mb').text(qty).remove();

        }
       else if($('#memory2').val() != null && $('#memory_qty2').val() != ''){
            $('#motherboard_genericListing').modal('show')
            //$('#memory-spec').text($('#memory').text());
            $('#memory-genspec-mb').text($('#memory2').find(':selected').text());
            $('#memory-genspec-qty-mb').text($('#memory_qty2').val());
            $('#memory-genqty-symbol-mb').text(qty);
        }
        
        if($('#slot_mb').val() == '' && $('#slot_qty_mb').val() == ''){
            $('#motherboard_genericListing').modal('show');
            $('#slot-genspec-qty-mb').text(none);
            $('#slot-genspec-mb').text(empty);
            $('#slot-genqty-symbol-mb').text(qty).remove();

        }
        else if($('#slot_mb').val() != '' && $('#slot_qty_mb').val() != ''){
            $('#motherboard_genericListing').modal('show')
            $('#slot-genspec-mb').text($('#slot_mb').val());
            $('#slot-genspec-qty-mb').text($('#slot_qty_mb').val());
            $('#slot-genqty-symbol-mb').text(qty);
        }

        if($('#slot_mb2').val() != '' && $('#slot_qty_mb2').val() != ''){
            $('#motherboard_genericListing').modal('show');
            $('#slot-gencomma-mb2').text(comma);
            $('#slot-genspec-mb2').text($('#slot_mb2').val());
            $('#slot-genspec-qty-mb2').text($('#slot_qty_mb2').val());
            $('#slot-genqty-symbol-mb2').text(qty);
        }

        if($('#slot_mb3').val() != '' && $('#slot_qty_mb3').val() != ''){
            $('#motherboard_genericListing').modal('show');
            $('#slot-gencomma-mb3').text(comma);
            $('#slot-genspec-mb3').text($('#slot_mb2').val());
            $('#slot-genspec-qty-mb3').text($('#slot_qty_mb2').val());
            $('#slot-genqty-symbol-mb3').text(qty);
        }

        $('#railkit-genspec-mb').text($('#mounting2').val());
        $('#warranty-genspec-mb').text($('#warranty2').val());

        if($('#psu_watts2').val() == '' && $('#psu_qty2').val() == ''){
            $('#motherboard_genericListing').modal('show');
            $('#power-genspec-qty-mb').text(none);
            $('#power-genspec-mb').text(empty);
            $('#power-genqty-symbol-mb').text(qty).remove();

        }
        else if($('#slot2').val() != '' && $('#psu_qty2').val() != ''){
            $('#motherboard_genericListing').modal('show')
            $('#power-genspec-mb').text($('#psu_watts2').val());
            $('#power-genspec-qty-mb').text($('#psu_qty2').val());
            $('#power-genqty-symbol-mb').text(qty);
        }



    });

});