let baseUrl;

$(document).ready(function() {

	baseUrl = $('#assemblyScript').data('baseUrl');
    var confirmDel;
    var timer;
	const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterSearchSelected = parameters.get('searchSelected');
    const parameterOrderBy = parameters.get('orderBy');
    const parameterSortOrder = parameters.get('sortOrder');

    	$('#searchSelected').find(':selected').prop('selected', false);
        $(`#searchSelected > option[value="${parameterSearchSelected}"]`).prop('selected', true);

        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'orderBy' : parameterOrderBy,
            'sortOrder': parameterSortOrder
        };

        $('#itemSearch').val(searchItem);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/assembly/search_item`, data, function(response) {
            $('#product_table_body').html(response);
             $('#exportCSV').prop('disabled', false);
            paginate();
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
        });

        const countUrl = `${baseUrl}index.php/assembly/load_listing_count`;

        $.get(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
        }
    });

   $('#chassis, #warranty, #chassis_edit, #warranty_edit, #warranty2, #warranty2_edit').select2({
        width: 640,
        tags: true
    });

   $(`#front_caddies, #rear_caddies, #drives1, #drives2, #drives3, #motherboard, #cpu, #memory, 
    #mounting, #front_caddies_edit, #rear_caddies_edit, #drives1_edit, #drives2_edit, #drives3_edit, 
    #motherboard_edit, #cpu_edit, #memory_edit, #mounting_edit, #motherboard2, #cpu2, #memory2, #mounting2,
    #motherboard2_edit, #cpu2_edit, #memory2_edit`).select2({
        width: 440,
        tags: true
   });

    $(`#backplane, #node, #drivebays, #psu, #rearbays, #bin, #backplane_edit, #node_edit, #drivebays_edit, 
        #psu_edit, #rearbays_edit, #bin_edit`).bind('dblclick', function(){
        $(this).attr('contentEditable', true);
    });


   $('table#product-table').click('.edit-item', function(event) {
         if ($(event.target).hasClass('edit-item')) {
             const button = $(event.target).closest('.edit-item');
             
            const build_id = button.closest('tr').find('.item-build_id').text().trim();
            const chassis = button.closest('tr').find('.item-chassis').text().trim();
            const backplane = button.closest('tr').find('.item-backplane').text().trim();
            const drivebays = button.closest('tr').find('.item-drivebays').text().trim();
            const rearbays = button.closest('tr').find('.item-rearbays').text().trim();
            const ff = button.closest('tr').find('.item-ff').text().trim();
            const rff = button.closest('tr').find('.item-rff').text().trim();
            const node = button.closest('tr').find('.item-node').text().trim();
            const psu = button.closest('tr').find('.item-psu').text().trim();
            const bin = button.closest('tr').find('.item-bin').text().trim();
            const front_caddies = button.closest('tr').find('.item-front_caddies').text().trim();
            const fc_qty = button.closest('tr').find('.item-fc_qty').text().trim();
            const rear_caddies = button.closest('tr').find('.item-rear_caddies').text().trim();
            const rc_qty = button.closest('tr').find('.item-rc_qty').text().trim();
            const drives = button.closest('tr').find('.item-drives').text().trim();
            const drives_qty = button.closest('tr').find('.item-drives_qty').text().trim();
            const drives2 = button.closest('tr').find('.item-drives2').text().trim();
            const drives_qty2 = button.closest('tr').find('.item-drives_qty2').text().trim();
            const drives3 = button.closest('tr').find('.item-drives3').text().trim();
            const drives_qty3 = button.closest('tr').find('.item-drives_qty3').text().trim();
            const motherboard = button.closest('tr').find('.item-motherboard').text().trim();
            const motherboard_qty = button.closest('tr').find('.item-motherboard_qty').text().trim();
            const cpu = button.closest('tr').find('.item-cpu').text().trim();
            const cpu_qty = button.closest('tr').find('.item-cpu_qty').text().trim();
            const memory = button.closest('tr').find('.item-memory').text().trim();
            const memory_qty = button.closest('tr').find('.item-memory_qty').text().trim();
            const ob_nic = button.closest('tr').find('.item-ob_nic').text().trim();
            const pcie_slot = button.closest('tr').find('.item-pcie_slot').text().trim();
            const slot = button.closest('tr').find('.item-slot').text().trim();
            const slot_qty = button.closest('tr').find('.item-slot_qty').text().trim();
            const slot2 = button.closest('tr').find('.item-slot2').text().trim();
            const slot_qty2 = button.closest('tr').find('.item-slot_qty2').text().trim();
            const slot3 = button.closest('tr').find('.item-slot3').text().trim();
            const slot_qty3 = button.closest('tr').find('.item-slot_qty3').text().trim();
            const psu_watts = button.closest('tr').find('.item-psu_watts').text().trim();;
            const psu_qty = button.closest('tr').find('.item-psu_qty').text().trim();;
            const mounting = button.closest('tr').find('.item-mounting').text().trim();;
            const mounting_qty = button.closest('tr').find('.item-mounting_qty').text().trim();;
            const warranty = button.closest('tr').find('.item-warranty').text().trim();;

            const drive_ff = drivebays + " " + ff;
            const rear_rff = rearbays + " " + rff;

            $('#build_id_edit').text(build_id);
            $('#build_id2_edit').text(build_id);
            $('#chassis_edit').val(chassis);
            $('#chassis_edit').trigger('change');
            $('#backplane_edit').text(backplane);
            $('#drivebays_edit').text(drive_ff);
            $('#rearbays_edit').text(rear_rff);
            $('#node_edit').text(node);
            $('#psu_edit').text(psu);
            $('#bin_edit').text(bin);
            $('#ob_nic_edit').text(ob_nic);
            $('#pcie_slot_edit').text(pcie_slot);
            $('#ob_nic_mb_edit').text(ob_nic);
            $('#pcie_slot_mb_edit').text(pcie_slot);
            if(front_caddies == 'null'){
                $('.current-frontcaddies').html();
            }else{
               $('.current-frontcaddies').html(front_caddies); 
            }
            
            if(fc_qty == 0){
                $('#fc_qty_edit').val();
            }else{
                $('#fc_qty_edit').val(fc_qty);
            }

            if(rear_caddies == 'null'){
                $('.current-rearcaddies').html();
            }else{
                $('.current-rearcaddies').html(rear_caddies);
            }

            if(rc_qty == 0){
                $('#rc_qty_edit').val();
            }else{
                $('#rc_qty_edit').val(rc_qty);
            }

            $("#drives1_edit").val(drives);
            if(drives_qty == 0){
                $('#drives_qty_edit').val();
            }else{
                $('#drives_qty_edit').val(drives_qty);
            }
             
            $('#drives2_edit').val(drives2);
            if(drives_qty2 == 0){
                $('#drives_qty2_edit').val();
            }else{
                $('#drives_qty2_edit').val(drives_qty2);
            }

            $('#drives3_edit').val(drives3);
            if(drives_qty3 == 0){
                $('#drives_qty3_edit').val();
            }else{
                $('#drives_qty3_edit').val(drives_qty3);
            }
             
            if(motherboard == 'null'){
                $('.current-motherboard').html();
            }else{
                $('.current-motherboard').html(motherboard);
            }

            if(motherboard_qty == 0 ){
                $('#motherboard_qty_edit').val();
            }else{
                $('#motherboard_qty_edit').val(motherboard_qty);
            }

            $('#motherboard2_edit').val(motherboard);
            if(motherboard_qty == 0 ){
                $('#motherboard_qty2_edit').val();
            }else{
                $('#motherboard_qty2_edit').val(motherboard_qty);
            }

            $('.current-cpu').html(cpu);
            if(cpu_qty == 0){
                $('#cpu_qty_edit').val();
            }else{
                $('#cpu_qty_edit').val(cpu_qty);
            }

            if(cpu_qty == 0){
                $('#cpu_qty2_edit').val();
            }else{
                $('#cpu_qty2_edit').val(cpu_qty);
            }
             
            $('.current-memory').html(memory);
            if(memory_qty == 0){
                $('#memory_qty_edit').val();
            }else{
                $('#memory_qty_edit').val(memory_qty);
            }

            if(memory_qty == 0){
                $('#memory_qty2_edit').val();
            }else{
                $('#memory_qty2_edit').val(memory_qty);
            }
             
             $('#slot_edit').val(slot);
             if(slot_qty == 0){
                $('#slot_qty_edit').val();
             }else{
               $('#slot_qty_edit').val(slot_qty); 
             }

             $('#slot_mb_edit').val(slot);
             if(slot_qty == 0){
                $('#slot_qty_mb_edit').val();
             }else{
               $('#slot_qty_mb_edit').val(slot_qty); 
             }

             $('#slot2_edit').val(slot2);
             if(slot_qty2 == 0){
                $('#slot_qty2_edit').val();
             }else{
                $('#slot_qty2_edit').val(slot_qty2);
             }

             $('#slot_mb2_edit').val(slot2);
             if(slot_qty2 == 0){
                $('#slot_qty_mb2_edit').val();
             }else{
                $('#slot_qty_mb2_edit').val(slot_qty2);
             }
             
             $('#slot3_edit').val(slot3);
             if(slot_qty3 == 0){
                $('#slot_qty3_edit').val();
             }else{
                $('#slot_qty3_edit').val(slot_qty3);
             }

             $('#slot_mb3_edit').val(slot3);
             if(slot_qty3 == 0){
                $('#slot_qty_mb3_edit').val();
             }else{
                $('#slot_qty_mb3_edit').val(slot_qty3);
             }
             
             $('#psu_watts_edit').val(psu_watts);
             if(psu_qty == 0){
                $('#psu_qty_edit').val();
             }else{
                $('#psu_qty_edit').val(psu_qty);
             }

             $('#psu_watts2_edit').val(psu_watts);
             if(psu_qty == 0){
                $('#psu_qty2_edit').val();
             }else{
                $('#psu_qty2_edit').val(psu_qty);
             }
             
             $('#mounting2_edit').val(mounting);
             if(mounting_qty == 0){
                $('#mounting_qty2_edit').val();
             }else{
                $('#mounting_qty2_edit').val(mounting_qty);
             }

             $('#mounting_edit').val(mounting);
             if(mounting_qty == 0){
                $('#mounting_qty_edit').val();
             }else{
                $('#mounting_qty_edit').val(mounting_qty);
             }
             
             $('#warranty_edit').val(warranty);
             $('#warranty2_edit').val(warranty);

        $('#front_caddies_edit').find('option:not(:first)').remove();
        $('#rear_caddies_edit').find('option:not(:first)').remove();
            

        var data = {
            'chassis' :$("#chassis_edit").val(),
            ajax : '1'
        };
  
        $.ajax({
            url : `${baseUrl}index.php/assembly/chassis_detail`,
            type: "post",
            data : data,
            success : function(msg){
                if(msg.dbays == "3.5"){
                    var data = {                    
                        "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                    },
                    select = $('select[name=front_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                    });
                    }else if(msg.dbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=front_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }
                         
                    if(msg.rbays == "3.5"){
                        var data = {                    
                            "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                        },
                        select = $('select[name=rear_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }else if(msg.rbays == "2.5"){
                        var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                        },
                        select = $('select[name=rear_caddies_edit]');
                        $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                        });
                    }                    
                }

        });   
    }
    });

    $('#motherboard_edit').click(function(){
        var data = {
            'chassis' : $("#chassis_edit").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/assembly/motherboard_for_chassis_editCH`,
            type: "post",
            data : data,
            success : function(motherboard){
                $('#motherboard_chassis_edit').html(motherboard);
            }
        });
    });

    $('#cpu_edit').click(function(){
        var data = {
            'chassis' : $("#chassis_edit").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/assembly/cpu_for_motherboard_editCH`,
            type: "post",
            data : data,
            success : function(motherboard){
                $('#cpu_motherboard_edit').html(motherboard);
            }
        });
    });

    $('#memory_edit').click(function(){
        var data = {
            'chassis' : $("#chassis_edit").val(),
             ajax : '1'
            };
            $.ajax({
                url : `${baseUrl}index.php/assembly/memory_for_motherboard_editCH`,
                type: "post",
                data : data,
                success : function(memory){

                    $('#memory_motherboard_edit').html(memory);

                }
            });
    });

    $('#cpu2_edit').click(function(){
        var data = {
            'motherboard' :$("#motherboard2_edit").val(),
             ajax : '1'
        }
        $.ajax({

            url : `${baseUrl}index.php/assembly/cpu_for_motherboard_editMB`,
            type: "post",
            data : data,
            success : function(cpu2){

                    $('#cpu_motherboard_mb_edit').html(cpu2);
                }
            });
    });
    
    $('#memory2_edit').click(function(){
        var data = {
            'motherboard' :$("#motherboard2_edit").val(),
             ajax : '1'
            }
            $.ajax({

            url : `${baseUrl}index.php/assembly/memory_for_motherboard_editMB`,
            type: "post",
            data : data,
            success : function(memory2){

                    $('#memory_motherboard_mb_edit').html(memory2);

                }
            });
    });

    $('#chassis_edit').change(function(event){

        $('#front_caddies_edit').find('option:not(:first)').remove();
        $('#rear_caddies_edit').find('option:not(:first)').remove();
            var data = {
                'chassis' :$("#chassis_edit").val(),
                ajax : '1'
            };
  
        $.ajax({
            url : `${baseUrl}index.php/assembly/chassis_detail`,
            type: "post",
            data : data,
            success : function(msg){
                $('#backplane_edit').text(msg.backplane);
                $('#drivebays_edit').text(msg.drivebays);
                $('#rearbays_edit').text(msg.rearbays);
                $('#node_edit').text(msg.node);
                $('#psu_edit').text(msg.psu);
                $('#bin_edit').text(msg.bin);
                $('#mfr_edit').text(msg.mfr);

                if(msg.dbays == "3.5"){
                    var data = {                    
                        "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                    },
                    select = $('select[name=front_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                    });
                }else if(msg.dbays == "2.5"){
                    var data = {                    
                        "dbays": ["2.5 Caddy","2.5 Filler"]
                    },
                    select = $('select[name=front_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                    });
                }


                if(msg.rbays == "3.5"){
                    var data = {                    
                        "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                    },
                    select = $('select[name=rear_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                    });
                }else if(msg.rbays == "2.5"){
                    var data = {                    
                        "dbays": ["2.5 Caddy","2.5 Filler"]
                    },
                    select = $('select[name=rear_caddies_edit]');
                    $.each( data.dbays, function(i, dbays) {
                        select.append( $('<option/>',{val:dbays, text:dbays}) );
                    });
                }                    
            }

        });

        var data = {
            'chassis' : $("#chassis_edit").val(),
            ajax : '1'
        };
        $.ajax({
            url : `${baseUrl}index.php/assembly/motherboard_for_chassis_editCH`,
            type: "post",
            data : data,
            success : function(motherboard){
                $('#motherboard_chassis_edit').html(motherboard);
            }
        });

        var data = {
            'chassis' : $("#chassis_edit").val(),
             ajax : '1'
        }

        $.ajax({
            url : `${baseUrl}index.php/assembly/obnic_pcislot_motherboard`,
            type: "post",
            data : data,
            success:  function(response){

                $('#ob_nic_edit').text(response.ob_nic);
                $('#pcie_slot_edit').text(response.pcie_slot);
            }
        });

        var data = {
            'chassis' : $("#chassis_edit").val(),
             ajax : '1'
            };
            $.ajax({
                url : `${baseUrl}index.php/assembly/cpu_for_motherboard_editCH`,
                type: "post",
                data : data,
                success : function(motherboard){
                    $('#cpu_motherboard_edit').html(motherboard);
                }
        });

        var data = {
            'chassis' : $("#chassis_edit").val(),
             ajax : '1'
            };
             $.ajax({
                url : `${baseUrl}index.php/assembly/memory_for_motherboard_editCH`,
                type: "post",
                data : data,
                success : function(memory){

                    $('#memory_motherboard_edit').html(memory);

                }
            });

    });


    $('#updateChassis').click(function(){
    
        var data = {
            'build_id'  : $('#build_id_edit').text(),
            'chassis'   : $('#chassis_edit').val(),
            'backplane' : $('#backplane_edit').text(),
            'rearbays'  : $('#rearbays_edit').text(),
            'drivebays' : $('#drivebays_edit').text(),
            'node'      : $('#node_edit').text(),
            'psu'       : $('#psu_edit').text(),
            'bin'       : $('#bin_edit').text(),
            'front_caddies' : $('#front_caddies_edit').val(),
            'fc_qty'        : $('#fc_qty_edit').val(),
            'rear_caddies'  : $('#rear_caddies_edit').val(),
            'rc_qty'        : $('#rc_qty_edit').val(),
            'drives'        : $('#drives1_edit').val(),
            'drives_qty'    : $('#drives_qty_edit').val(),
            'drives2'        : $('#drives2_edit').val(),
            'drives_qty2'    : $('#drives_qty2_edit').val(),
            'drives3'        : $('#drives3_edit').val(),
            'drives_qty3'    : $('#drives_qty3_edit').val(),
            'motherboard'   : $('#motherboard_edit').val(),
            'motherboard_qty': $('#motherboard_qty_edit').val(),
            'cpu'           : $('#cpu_edit').val(),
            'cpu_qty'       : $('#cpu_qty_edit').val(),
            'memory'        : $('#memory_edit').val(),
            'memory_qty'    : $('#memory_qty_edit').val(),
            'ob_nic'        : $('#ob_nic_edit').text(),
            'pcie_slot'     : $('#pcie_slot_edit').text(),
            'slot'          : $('#slot_edit').val(),
            'slot_qty'      : $('#slot_qty_edit').val(),
            'slot2'          : $('#slot2_edit').val(),
            'slot_qty2'      : $('#slot_qty2_edit').val(),
            'slot3'          : $('#slot3_edit').val(),
            'slot_qty3'      : $('#slot_qty3_edit').val(),
            'psu_watts'     : $('#psu_watts_edit').val(),
            'psu_qty'       : $('#psu_qty_edit').val(),
            'mounting'      : $('#mounting_edit').val(),
            'mounting_qty'  : $('#mounting_qty_edit').val(),
            'warranty'      : $('#warranty_edit').val(),
            ajax : '1'
        }
        
        $.ajax({
            url : `${baseUrl}index.php/assembly/update_row`,
            type: "post",
            data : data,
            success : function(){
                const refreshUrl = window.location.href.split('?')[0];
                window.location.href = refreshUrl;
            }
        });
    
    });


    $('#updateMotherboard').click(function(){
       
        event.preventDefault();

        var data = {
            'build_id'      : $('#build_id2_edit').text(),
            'motherboard'   : $('#motherboard2_edit').val(),
            'motherboard_qty': $('#motherboard_qty2_edit').val(),
            'cpu'           : $('#cpu2_edit').val(),
            'cpu_qty'       : $('#cpu_qty_edit').val(),
            'memory'        : $('#memory2_edit').val(),
            'memory_qty'    : $('#memory_qty2_edit').val(),
            'ob_nic'        : $('#ob_nic_mb_edit').text(),
            'pcie_slot'     : $('#pcie_slot_mb_edit').text(),
            'slot'          : $('#slot_mb_edit').val(),
            'slot_qty'      : $('#slot_qty_mb_edit').val(),
            'slot2'          : $('#slot_mb2_edit').val(),
            'slot_qty2'      : $('#slot_qty_mb2_edit').val(),
            'slot3'          : $('#slot_mb3_edit').val(),
            'slot_qty3'      : $('#slot_qty_mb3_edit').val(),
            'psu_watts'     : $('#psu_watts2_edit').val(),
            'psu_qty'       : $('#psu_qty2_edit').val(),
            'mounting'      : $('#mounting2_edit').val(),
            'mounting_qty'  : $('#mounting_qty2_edit').val(),
            'warranty'      : $('#warranty2_edit').val(),
            'ajax'          : 1
        };
        console.log(data);
        $.ajax({

            url : `${baseUrl}index.php/assembly/update_row`,
            type : "post",
            data : data,
            success : function(response){
                const refreshUrl = window.location.href.split('?')[0];
                window.location.href = refreshUrl;
            }
        })
    
    

    });  

    $('#add_drives').click(function(){
        $('.drives_list_copy').show();
        $('#add_drives').prop('disabled', true);
        $('#add_drives2').prop('disabled', false);
           
        
    });

    $('#add_drives2').click(function(){
        $('.drives_list_copy2').show();
        $('#add_drives2').prop('disabled', true);

    });

    $('#add_drives3').click(function(){
       $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
                    </div>`);

    });

    $('.remove_drives2').click(function(){
        $('.drives_list_copy').hide();
        $('.drives_list_copy2').hide();
        $('#add_drives').prop('disabled', false);
    });

    $('.remove_drives3').click(function(){
        $('.drives_list_copy2').hide();
        $('#add_drives2').prop('disabled', false);
    });

    $('#add_controller').click(function(){
        $('.slot_list_copy').show();
        $('#add_controller').prop('disabled', true);
        $('#add_controller2').prop('disabled', false);
           
        
    });

    $('#add_controller2').click(function(){
        $('.slot_list_copy2').show();
        $('#add_controller2').prop('disabled', true);

    });

    $('#add_controller3').click(function(){
       $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
                    </div>`);

    });

    $('.remove_slot2').click(function(){
        $('.slot_list_copy').hide();
        $('.slot_list_copy2').hide();
        $('#add_controller').prop('disabled', false);
    });

    $('.remove_slot3').click(function(){
        $('.slot_list_copy2').hide();
        $('#add_controller2').prop('disabled', false);
    });

    $('#add_controller_mb').click(function(){
        $('.slot_list_mb2').show();
        $('#add_controller_mb').prop('disabled', true);
        $('#add_controller_mb2').prop('disabled', false);
           
        
    });

    $('#add_controller_mb2').click(function(){
        $('.slot_list_mb3').show();
        $('#add_controller_mb2').prop('disabled', true);

    });

    $('#add_controller_mb3').click(function(){
       $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
                    </div>`);

    });

    $('.remove_slot_mb2').click(function(){
       // $('.slot_list_mb').hide();
        $('.slot_list_mb2').hide();
        $('#add_controller_mb').prop('disabled', false);
    });

    $('.remove_slot_mb3').click(function(){
        $('.slot_list_mb3').hide();
        $('#add_controller_mb2').prop('disabled', false);
    });

    $('.modal').css('overflow-y', 'auto');

    $('#remove_chassis').click(function(event){
        var none = " ";
        $('#myModal').modal('hide');
        $('#motherboard_modal').modal('show');
        $('#motherboard2').val(none);
        $('#motherboard_qty2').val(none);
        $('#cpu2').val(none);
        $('#cpu_qty2').val(none);
        $('#memory2').val(none);
        $('#memory_qty2').val(none);
        $('#slot_mb').val(none);
        $('#slot_qty_mb').val(none);
        $('#slot_mb2').val(none);
        $('#slot_qty_mb2').val(none);
        $('#slot_mb3').val(none);
        $('#slot_qty_mb3').val(none);
        $('#psu_watts2').val(none);
        $('#psu_qty2').val(none);
        $('#mounting_qty2').val(none);

    });

    $('#include_chassis').click(function(event){
        var none = " ";
        $('#motherboard_modal').modal('hide');
        $('#myModal').modal('show');
        $('#chassis').val(none);
        $('#backplane').text(none);
        $('#node').text(none);
        $('#drivebays').text(none);
        $('#psu').text(none);
        $('#rearbays').text(none);
        $('#bin').text(none);
        $('#front_caddies').val(none);
        $('#fc_qty').val(none);
        $('#rear_caddies').val(none);
        $('#rc_qty').val(none);
        $('#drives1').val(none);
        $('#drives_qty').val(none);
        $('#drives2').val(none);
        $('#drives_qty2').val(none);
        $('#drives3').val(none);
        $('#drives_qty3').val(none);
        $('#motherboard').val(none);
        $('#motherboard_qty').val(none);
        $('#cpu').val(none);
        $('#cpu_qty').val(none);
        $('#memory').val(none);
        $('#memory_qty').val(none);
        $('#slot').val(none);
        $('#slot_qty').val(none);
        $('#slot2').val(none);
        $('#slot_qty2').val(none);
        $('#slot3').val(none);
        $('#slot_qty3').val(none);
        $('#psu_watts').val(none);
        $('#psu_qty').val(none);
        $('#mounting_qty').val(none);
    });

    $('#searchColumn').click(function(event){
        event.preventDefault();
        var searchSelected = $('#searchSelected').val();
        console.log(searchSelected);
        const searchItem = $('#itemSearch').val();
        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        search();
    });

    $('#orderBy').change(function(event) {
        search();
    });

    $('#sortOrder').change(function(event) {
        search();
    });

    $('#insertListing').click(function(event){

        
        //console.log(dates);
        //var num = parseInt($('#build_id').text());
        
     //   dates++;

    });

    $('#chassis').change(function(event){

        $('#front_caddies').find('option:not(:first)').remove();
        $('#rear_caddies').find('option:not(:first)').remove();
        var data = {
            'chassis' :$("#chassis").val(),
             ajax : '1'
        }
  
        $.ajax({
            url : `${baseUrl}index.php/assembly/chassis_detail`,
            type: "post",
            data : data,
            success : function(msg){
                $('#backplane').text(msg.backplane);
                $('#drivebays').text(msg.drivebays);
                $('#rearbays').text(msg.rearbays);
                $('#node').text(msg.node);
                $('#psu').text(msg.psu);
                $('#bin').text(msg.bin);
                $('#mfr').text(msg.mfr);


                 if(msg.dbays == "3.5"){
                            var data = {                    
                            "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                            },
                                select = $('select[name=front_caddies]');
                            $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                         }else if(msg.dbays == "2.5"){
                            var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                            },
                                select = $('select[name=front_caddies]');
                            $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                         }


                         if(msg.rbays == "3.5"){
                            var data = {                    
                            "dbays": ["3.5 To Caddy Converter","3.5 Caddy","3.5 Filler"]
                            },
                                select = $('select[name=rear_caddies]');
                            $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                         }else if(msg.rbays == "2.5"){
                            var data = {                    
                            "dbays": ["2.5 Caddy","2.5 Filler"]
                            },
                                select = $('select[name=rear_caddies]');
                            $.each( data.dbays, function(i, dbays) {
                            select.append( $('<option/>',{val:dbays, text:dbays}) );
                            });
                         }                    
            }

        });

        var data = {
            'chassis' : $("#chassis").val(),
             ajax : '1'
            }
            $.ajax({

            url : `${baseUrl}index.php/assembly/motherboard_for_chassis`,
            type: "post",
            data : data,
            success : function(motherboard){

                    $('#motherboard_chassis').html(motherboard);

                }
            });

        var data = {
            'chassis' : $("#chassis").val(),
             ajax : '1'
        }

        $.ajax({
            url : `${baseUrl}index.php/assembly/obnic_pcislot_motherboard`,
            type: "post",
            data : data,
            success:  function(response){

                $('#ob_nic').text(response.ob_nic);
                $('#pcie_slot').text(response.pcie_slot);
            }
        });


        var data = {
            'chassis' : $("#chassis").val(),
             ajax : '1'
            }
            $.ajax({

            url : `${baseUrl}index.php/assembly/cpu_for_motherboard`,
            type: "post",
            data : data,
            success : function(cpu){

                    $('#cpu_motherboard').html(cpu);

                }
            });
        var data = {
            'chassis' : $("#chassis").val(),
             ajax : '1'
            }
            $.ajax({

            url : `${baseUrl}index.php/assembly/memory_for_motherboard`,
            type: "post",
            data : data,
            success : function(memory){

                    $('#memory_motherboard').html(memory);

                }
            });



    });

    $('#motherboard2').change(function(event){

        var data = {
            'motherboard' :$("#motherboard2").val(),
             ajax : '1'
        };

        $.ajax({

            url : `${baseUrl}index.php/assembly/cpu_for_motherboard2`,
            type: "post",
            data : data,
            success : function(cpu2){

                    $('#cpu_motherboard2').html(cpu2);
                }
            });

        var data = {
            'motherboard' : $('#motherboard2').val(),
            ajax : '1'
        }

         $.ajax({
             url : `${baseUrl}index.php/assembly/get_obnic_pcieslot`,
            type: "post",
            data : data,
            success : function(event){

                    $('#ob_nic_mb').text(event.ob_nic);
                    $('#pcie_slot_mb').text(event.pcie_slot);
                }
        });

        var data = {
            'motherboard' :$("#motherboard2").val(),
             ajax : '1'
            }
            $.ajax({

            url : `${baseUrl}index.php/assembly/memory_for_motherboard2`,
            type: "post",
            data : data,
            success : function(memory2){

                    $('#memory_motherboard2').html(memory2);

                }
            });
       

    });


    $('#motherboard2_edit').change(function(event){

        var data = {
            'motherboard' :$("#motherboard2_edit").val(),
             ajax : '1'
        }
        $.ajax({

            url : `${baseUrl}index.php/assembly/cpu_for_motherboard_editMB`,
            type: "post",
            data : data,
            success : function(cpu2){

                    $('#cpu_motherboard_mb_edit').html(cpu2);
                }
            });

        var data = {
            'motherboard' : $('#motherboard2_edit').val(),
            ajax : '1'
        }

         $.ajax({
             url : `${baseUrl}index.php/assembly/get_obnic_pcieslot`,
            type: "post",
            data : data,
            success : function(event){

                    $('#ob_nic_mb_edit').text(event.ob_nic);
                    $('#pcie_slot_mb_edit').text(event.pcie_slot);
                }
        });


        var data = {
            'motherboard' :$("#motherboard2_edit").val(),
             ajax : '1'
            }
            $.ajax({

            url : `${baseUrl}index.php/assembly/memory_for_motherboard_editMB`,
            type: "post",
            data : data,
            success : function(memory2){

                    $('#memory_motherboard_mb_edit').html(memory2);

                }
            });
       

    });

    $('#submit').click(function(){

        if( $('#cpu').val() != null && $('#cpu_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu').val() == null && $('#cpu_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory').val() == null && $('#memory_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory').val() != null && $('#memory_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);

        }else if($('#drives1').val() === 'null' && $('#drives_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#drives1').val() != 'null' && $('#drives_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#drives2').val() === 'null' && $('#drives_qty2').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#drives2').val() != 'null' && $('#drives_qt2').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#drives3').val() === 'null' && $('#drives_qty3').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);

        }else if($('#drives3').val() != 'null' && $('#drives_qty3').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
    
        }else if($('#slot').val() == '' && $('#slot_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#slot').val() != '' && $('#slot_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#slot2').val() == '' && $('#slot_qty2').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#slot2').val() != '' && $('#slot_qty2').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#slot3').val() == '' && $('#slot_qty3').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#slot3').val() != '' && $('#slot_qty3').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#psu_watts').val() == '' && $('#psu_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_watts').val() != '' && $('#psu_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#front_caddies').val() == 'null' && $('#fc_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Front Caddies Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#front_caddies').val() != 'null' && $('#fc_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Front Caddies Quantity</h5>
                    </div>`); 
        }else if($('#rear_caddies').val() == 'null' && $('#rc_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Rear Caddies Quantity. No selected optoin</h5>
                    </div>`);
            
        }else if($('#rear_caddies').val() != 'null' && $('#rc_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Rear Caddies Quantity</h5>
                    </div>`); 
           
        }else if($('#motherboard').val() == 'null' && $('#motherboard_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard').val() != 'null' && $('#motherboard_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if($('#mounting').val() == '' && $('#mounting_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting').val() != '' && $('#mounting_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }
        else{

        event.preventDefault();

        var data = {

            'build_id'      : $('#build_id').text(),
            'sales_rep'     : $('#sales_rep').text(),
            'chassis'       : $('#chassis').val(),
            'backplane'     : $('#backplane').text(),
            'drivebays'     : $('#drivebays').text(),
            'rearbays'      : $('#rearbays').text(),
           'node'          : $('#node').text(),
            'psu'           : $('#psu').text(),
            'bin'           : $('#bin').text(),
            'front_caddies' : $('#front_caddies').val(),
            'fc_qty'        : $('#fc_qty').val(),
            'rear_caddies'  : $('#rear_caddies').val(),
            'rc_qty'        : $('#rc_qty').val(),
            'drives'        : $('#drives1').val(),
            'drives_qty'    : $('#drives_qty').val(),
            'drives2'        : $('#drives2').val(),
            'drives_qty2'    : $('#drives_qty2').val(),
            'drives3'        : $('#drives3').val(),
            'drives_qty3'    : $('#drives_qty3').val(),
            'motherboard'   : $('#motherboard').val(),
            'motherboard_qty': $('#motherboard_qty').val(),
            'cpu'           : $('#cpu').val(),
            'cpu_qty'       : $('#cpu_qty').val(),
            'memory'        : $('#memory').val(),
            'memory_qty'    : $('#memory_qty').val(),
            'ob_nic'        : $('#ob_nic').text(),
            'pcie_slot'     : $('#pcie_slot').text(),
            'slot'          : $('#slot').val(),
            'slot_qty'      : $('#slot_qty').val(),
            'slot2'          : $('#slot2').val(),
            'slot_qty2'      : $('#slot_qty2').val(),
            'slot3'          : $('#slot3').val(),
            'slot_qty3'      : $('#slot_qty3').val(),
            'psu_watts'     : $('#psu_watts').val(),
            'psu_qty'       : $('#psu_qty').val(),
            'mounting'      : $('#mounting').val(),
            'mounting_qty'  : $('#mounting_qty').val(),
            'warranty'      : $('#warranty').val(),
            'ajax'          : 1

        };

        $.ajax({

            url : `${baseUrl}index.php/assembly/create_listing`,
            type : "post",
            data : data,
            success : function(response){
                    
                const refreshUrl = window.location.href.split('?')[0];
                window.location.href = refreshUrl;
                
            }
        });
    }
        
    });

    $('#submit_motherboard').click(function(){

        if($('#motherboard2').val() == 'null' && $('#motherboard_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#motherboard2').val() != 'null' && $('#motherboard_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`); 
        }else if( $('#cpu2').val() != null && $('#cpu_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
             
        }
        else if($('#cpu2').val() == null && $('#cpu_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);

        }else if($('#memory2').val() == null && $('#memory_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);

        }else if($('#memory2').val() != null && $('#memory_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);

        }else if($('#slot_mb').val() == ' ' && $('#slot_qty_mb').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#slot_mb').val() != ' ' && $('#slot_qty_mb').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#slot_mb2').val() == ' ' && $('#slot_qty_mb2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
    
        }else if($('#slot_mb2').val() != ' ' && $('#slot_qty_mb2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }else if($('#slot_mb3').val() == ' ' && $('#slot_qty_mb3').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);

        }else if($('#slot_mb3').val() != ' ' && $('#slot_qty_mb3').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
         
        }
        else if($('#psu_watts2').val() == ' ' && $('#psu_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No PSU inputted</h5>
                    </div>`);
            
        }else if($('#psu_watts2').val() != '' && $('#psu_qty2').val() == ' '){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
           
        }else if($('#mounting2').val() == '' && $('#mounting_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            
        }else if($('#mounting2').val() != '' && $('#mounting_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
           
        }else{
        event.preventDefault();

        var data = {
             
            'build_id'      : $('#build_id2').html(),
            'sales_rep'     : $('#sales_rep2').html(),
            'motherboard'   : $('#motherboard2').val(),
            'motherboard_qty': $('#motherboard_qty2').val(),
            'cpu'           : $('#cpu2').val(),
            'cpu_qty'       : $('#cpu_qty').val(),
            'memory'        : $('#memory2').val(),
            'memory_qty'    : $('#memory_qty2').val(),
            'ob_nic'        : $('#ob_nic_mb').text(),
            'pcie_slot'     : $('#pcie_slot_mb').text(),
            'slot'          : $('#slot_mb').val(),
            'slot_qty'      : $('#slot_qty_mb').val(),
            'slot2'          : $('#slot_mb2').val(),
            'slot_qty2'      : $('#slot_qty_mb2').val(),
            'slot3'          : $('#slot_mb3').val(),
            'slot_qty3'      : $('#slot_qty_mb3').val(),
            'psu_watts'     : $('#psu_watts2').val(),
            'psu_qty'       : $('#psu_qty2').val(),
            'mounting'      : $('#mounting2').val(),
            'mounting_qty'  : $('#mounting_qty2').val(),
            'warranty'      : $('#warranty2').val(),
            'ajax'          : 1
        };

        $.ajax({

            url : `${baseUrl}index.php/assembly/create_listing`,
            type : "post",
            data : data,
            success : function(response){
                const refreshUrl = window.location.href.split('?')[0];
                window.location.href = refreshUrl;
            }
        });


    }

    });

    $("#product_table_body").on('click', '.deleteBtn', function(event){
      const btnDelete = $(this).closest('.deleteBtn');
      if (!btnDelete) return;

       confirmDel = btnDelete.closest("tr").find(".item-build_id").text();
        
    });

     $("#delete_row").on('click', function(event){
        var data = {
            'build_id' : confirmDel,
            ajax : '1'   
        };
        console.log(data); 
        $.ajax({
            url : `${baseUrl}index.php/assembly/delete_row`,
            type : "post",
            data : data,
            success : function(orders){
                        location.reload();    
                                                
                }
            });
    });  

     $('#exportCSV').click(function() {
        const searchItem = $('#itemSearch').val();   
        const searchSelected = $('#searchSelected').val();   
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();

        const data = {
            'searchItem': searchItem,
            'orderBy': parameterOrderBy,
            'sortOrder': parameterSortOrder,
            'searchSelected' : parameterSearchSelected

        };

        const  url = `${baseUrl}index.php/assembly/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `assembly_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

    $("#fc_qty").change(function(){
        const data_add_front = {
            'fc_qty' : $('#fc_qty').val(),
            'drivebays' : $('#drivebays').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/assembly/front_caddies_qty_checker`,
            type : "post",
            data: data_add_front,
            success: function(response){
                if(response.is_error){
                    $('#myModal').scrollTop(0);
                    $('#caddies_error').show();
                    timer =setTimeout(function () {$('#caddies_error').hide(500);}, 3000);
                    $("#submit").prop("disabled", true);
                }else{
                    $("#submit").prop("disabled", false);
                }
            }
        });
    });

    $("#rc_qty").change(function(){
        const data_edit_rear = {
            'rc_qty' : $('#rc_qty').val(),
            'rearbays' : $('#rearbays').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/assembly/rear_caddies_qty_checker`,
            type : "post",
            data: data_edit_rear,
            success: function(response){
                if(response.is_error){
                    $('#myModal').scrollTop(0);
                    
                    $('#caddies_error').show();
                    timer =setTimeout(function () {$('#caddies_error').hide(500);}, 3000);
                    $("#submit").prop("disabled", true);
                }else{
                    $("#submit").prop("disabled", false);
                }
            }
        });
    });

    $("#fc_qty_edit").change(function(){
        const data_edit_front = {
            'fc_qty' : $('#fc_qty_edit').val(),
            'drivebays' : $('#drivebays_edit').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/assembly/front_caddies_qty_checker`,
            type : "post",
            data: data_edit_front,
            success: function(response){
                if(response.is_error){
                    $('#editChassis').scrollTop(0);
                    $('#caddies_error_edit').show();
                    timer =setTimeout(function () {$('#caddies_error_edit').hide(500);}, 3000);
                    $("#updateChassis").prop("disabled", true);
                }else{
                    $("#updateChassis").prop("disabled", false);
                }
            }
        });
    });

    $("#rc_qty_edit").change(function(){
        const data_edit_rear = {
            'rc_qty' : $('#rc_qty_edit').val(),
            'rearbays' : $('#rearbays_edit').text(),
            ajax : '1'
        };
        $.ajax({
            url: `${baseUrl}index.php/assembly/rear_caddies_qty_checker`,
            type : "post",
            data: data_edit_rear,
            success: function(response){
                if(response.is_error){
                    $('#editChassis').scrollTop(0);
                    
                    $('#caddies_error_edit').show();
                    timer =setTimeout(function () {$('#caddies_error_edit').hide(500);}, 3000);
                    $("#updateChassis").prop("disabled", true);
                }else{
                    $("#updateChassis").prop("disabled", false);
                }
            }
        });
    });

    $("#edit_chassis_close").click(function(){

        $("#editChassis").modal('hide');
        location.reload();
    });

    $("#edit_mb_close").click(function(){
        $("#editMotherboard").modal('hide');
        location.reload();
    });

    $("#close_add_chassis").click(function(){
        $("#myModal").modal('hide');
        location.reload();
    });

    $('#insertListing').click(function(){
        $('#myModal').modal({backdrop: 'static', keyboard: false} );
    });


    function search() {
        const searchItem = $('#itemSearch').val();
        const searchSelected = $('#searchSelected').val()
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();


        const route = `${baseUrl}index.php/assembly?search=${searchItem}`+`&searchSelected=${searchSelected}&orderBy=${orderBy}` +`&sortOrder=${sortOrder}`;
        window.location.href = route;

       
    }

});
