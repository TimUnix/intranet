$(document).ready(function() {

baseUrl = $('#assemblyMBeBayListingScript').data('baseUrl');

$('#generateMB_ebayListing').click(function(event){
        const qty = 'x';
        const none = 'None';
        const empty =' ';
        const node = 'U';
        const comma = ',';
        const sales_rep = $('#sales_rep').html();
        const date = new Date().toLocaleString();
        const author = sales_rep + empty + date;
       /* if($('#motherboard2').val() == null && $('#motherboard_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Motherboard Quantity. No selected option </h5>
                    </div>`);
            return;
        }else if($('#motherboard2').val() != null && $('#motherboard_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Motherboard Quantity</h5>
                    </div>`);
            return;
        }
        else if($('#cpu2').val() == null && $('#cpu_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);
            return;
        }else if($('#cpu2').val() != null && $('#cpu_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
            return;
        }
          else if($('#memory2').val() == null && $('#memory_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);
            return;
        }else if($('#memory2').val() != null && $('#memory_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);
            return;
        }else if($('#slot_mb').val() == '' && $('#slot_qty_mb').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot_mb').val() != '' && $('#slot_qty_mb').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot_mb2').val() == '' && $('#slot_qty_mb2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot_mb2').val() != '' && $('#slot_qty_mb2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot_mb3').val() == '' && $('#slot_qty_mb3').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot_mb3').val() != '' && $('#slot_qty_mb3').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#psu_watts2').val() == '' && $('#psu_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No selected PSU</h5>
                    </div>`);
            return;
        }else if($('#psu_watts2').val() != '' && $('#psu_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
            return;
        }else if($('#mounting2').val() == '' && $('#mounting_qty2').val() != ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            return;
        }else if($('#mounting2').val() != '' && $('#mounting_qty2').val() == ''){
            $('#motherboard_modal').scrollTop(0);
            $("#success_mb").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
            return;
        }*/

        $('#build_id-spec-mb').text($('#build_id2').html());
        $('#author_date-spec-mb').text(author);
        $('#custom_label-spec-mb').text($('#build_id2').html() + empty + $('#motherboard2').val());

        $('#ob_nic-spec-mb').text($('#ob_nic_mb').html());
        $('#pcie_slot-spec-mb').text($('#pcie_slot_mb').html());
        
        
        if($('#cpu2').val() == null && $('#cpu_qty2').val() == ''){
            $('#motherboard_ebayListing').modal('show');
            $('#cpu-spec-qty-mb').text(empty);
            $('#cpu-spec-mb').text(none);
            $('#cpu-qty-symbol-mb').text(qty).remove();

        }
        else if($('#cpu2').val() != null && $('#cpu_qty2').val() != ''){
            $('#motherboard_ebayListing').modal('show')
            //$('#cpu-spec').text($('#cpu').find(':selected').text());
            $('#cpu-spec-mb').text($('#cpu2').text());
            $('#cpu-spec-qty-mb').text($('#cpu_qty2').val());
            $('#cpu-qty-symbol-mb').text(qty);
        }

        
        if($('#memory2').val() == null && $('#memory_qty2').val() == ''){
            $('#motherboard_ebayListing').modal('show');
            $('#memory-spec-qty-mb').text(none);
            $('#memory-spec-mb').text(empty);
            $('#memory-qty-symbol-mb').text(qty).remove();

        }
       else if($('#memory2').val() != null && $('#memory_qty2').val() != ''){
            $('#motherboard_ebayListing').modal('show')
            //$('#memory-spec').text($('#memory').text());
            $('#memory-spec-mb').text($('#memory2').find(':selected').text());
            $('#memory-spec-qty-mb').text($('#memory_qty2').val());
            $('#memory-qty-symbol-mb').text(qty);
        }
        
        if($('#slot_mb').val() == '' && $('#slot_qty_mb').val() == ''){
            $('#motherboard_ebayListing').modal('show');
            $('#slot-spec-qty-mb').text(none);
            $('#slot-spec-mb').text(empty);
            $('#slot-qty-symbol-mb').text(qty).remove();

        }
        else if($('#slot_mb').val() != '' && $('#slot_qty_mb').val() != ''){
            $('#motherboard_ebayListing').modal('show')
            $('#slot-spec-mb').text($('#slot_mb').val());
            $('#slot-spec-qty-mb').text($('#slot_qty_mb').val());
            $('#slot-qty-symbol-mb').text(qty);
        }

        if($('#slot_mb2').val() != '' && $('#slot_qty_mb2').val() != ''){
            $('#motherboard_ebayListing').modal('show');
            $('#slot-comma-mb2').text(comma);
            $('#slot-spec-mb2').text($('#slot_mb2').val());
            $('#slot-spec-qty-mb2').text($('#slot_qty_mb2').val());
            $('#slot-qty-symbol-mb2').text(qty);

        }

        if($('#slot_mb3').val() != '' && $('#slot_qty_mb3').val() != ''){
            $('#motherboard_ebayListing').modal('show');
            $('#slot-comma-mb3').text(comma);
            $('#slot-spec-mb3').text($('#slot_mb3').val());
            $('#slot-spec-qty-mb3').text($('#slot_qty_mb3').val());
            $('#slot-qty-symbol-mb3').text(qty);

        }

        $('#railkit-spec-mb').text($('#mounting2').val());
        $('#warranty-spec-mb').text($('#warranty2').val());

        if($('#psu_watts2').val() == '' && $('#psu_qty2').val() == ''){
            $('#motherboard_ebayListing').modal('show');
            $('#power-spec-qty-mb').text(none);
            $('#power-spec-mb').text(empty);
            $('#power-qty-symbol-mb').text(qty).remove();

        }
        else if($('#psu_watts2').val() != '' && $('#psu_qty2').val() != ''){
            $('#motherboard_ebayListing').modal('show')
            $('#power-spec-mb').text($('#psu_watts2').val());
            $('#power-spec-qty-mb').text($('#psu_qty2').val());
            $('#power-qty-symbol-mb').text(qty);
        }



    });

});