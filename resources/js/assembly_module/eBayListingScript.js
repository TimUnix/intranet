$(document).ready(function() {

baseUrl = $('#assemblyeBayListingScript').data('baseUrl');

$('#generate_ebayListing').click(function(event){
        const qty = 'x';
        const none = 'None';
        const empty =' ';
        const node = 'U'
        const comma = ','
        const sales_rep = $('#sales_rep').html();
        const date = new Date().toLocaleString();
        const author = sales_rep + empty + date;

     /*   if($('#cpu').val() == null && $('#cpu_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove CPU Quantity. No selected CPU </h5>
                    </div>`);
            return;
        }else if($('#cpu').val() != null && $('#cpu_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input CPU Quantity</h5>
                    </div>`);
            return;
        }else if($('#memory').val() == null && $('#memory_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Memory Quantity. No selected Memory </h5>
                    </div>`);
            return;
        }else if($('#memory').val() != null && $('#memory_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Memory Quantity</h5>
                    </div>`);
            return;
        }else if($('#drives1').val() === 'null' && $('#drives_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);
            return;
        }else if($('#drives1').val() != 'null' && $('#drives_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
            return;
        }else if($('#drives2').val() === 'null' && $('#drives_qty2').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);
            return;
        }else if($('#drives2').val() != 'null' && $('#drives_qty2').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
            return;
        }else if($('#drives3').val() === 'null' && $('#drives_qty3').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Drives Quantity. No selected Hard Drive </h5>
                    </div>`);
            return;
        }else if($('#drives3').val() != 'null' && $('#drives_qty3').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Drives Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot').val() == '' && $('#slot_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot').val() != '' && $('#slot_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot2').val() == '' && $('#slot_qty2').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot2').val() != '' && $('#slot_qty2').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#slot3').val() == '' && $('#slot_qty3').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Controller Quantity. No selected Controller</h5>
                    </div>`);
            return;
        }else if($('#slot3').val() != '' && $('#slot_qty3').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Controller Quantity</h5>
                    </div>`); 
            return;
        }else if($('#psu_watts').val() == '' && $('#psu_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove PSU Quantity. No selected PSU</h5>
                    </div>`);
            return;
        }else if($('#psu_watts').val() != '' && $('#psu_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input PSU Quantity</h5>
                    </div>`); 
            return;
        }else if($('#mounting').val() == '' && $('#mounting_qty').val() != ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please remove Mounting Quantity. No selected option</h5>
                    </div>`);
            return;
        }else if($('#mounting').val() != '' && $('#mounting_qty').val() == ''){
            $('#myModal').scrollTop(0);
            $("#success").html(`<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Mounting Quantity</h5>
                    </div>`); 
            return;
        }*/

        $('#build_id-spec').text($('#build_id').html());
        $('#author_date-spec').text(author);
        $('#custom_label-spec').text($('#build_id').html() + empty + $('#chassis').val());
        if($('#cpu').val() == null && $('#cpu_qty').val() == ''){
            $('#ebayListing').modal('show');
            $('#cpu-spec-qty').text(empty);
            $('#cpu-spec').text(none);
            $('#cpu-qty-symbol').text(qty).remove();

        }
        else if($('#cpu').val() != null && $('#cpu_qty').val() != ''){
            $('#ebayListing').modal('show')
            //$('#cpu-spec').text($('#cpu').find(':selected').text());
            $('#cpu-spec').text($('#cpu').text());
            $('#cpu-spec-qty').text($('#cpu_qty').val());
            $('#cpu-qty-symbol').text(qty);
        }

        
        if($('#memory').val() == null && $('#memory_qty').val() == ''){
            $('#ebayListing').modal('show');
            $('#memory-spec-qty').text(none);
            $('#memory-spec').text(empty);
            $('#memory-qty-symbol').text(qty).remove();

        }
       else if($('#memory').val() != null && $('#memory_qty').val() != ''){
            $('#ebayListing').modal('show')
            //$('#memory-spec').text($('#memory').text());
            $('#memory-spec').text($('#memory').find(':selected').text());
            $('#memory-spec-qty').text($('#memory_qty').val());
            $('#memory-qty-symbol').text(qty);
        }

        if($('#drives1').val() == 'null' && $('#drives_qty').val() == ''){
            $('#ebayListing').modal('show');
            $('#drives-spec-qty').text(none);
            $('#drives-spec').text(empty);
            $('#drives-qty-symbol').text(qty).remove();

        }
        else if($('#drives1').val() != null && $('#drives_qty').val() != ''){
            $('#ebayListing').modal('show');
            $('#drives-spec').text($('#drives1').find(':selected').text());
            $('#drives-spec-qty').text($('#drives_qty').val());
            $('#drives-qty-symbol').text(qty);
        }

        if($('#drives2').val() != 'null' && $('#drives_qty2').val() != ''){
                $('#ebayListing').modal('show');
                $('#drives-comma2').text(comma);
                $('#drives-spec2').text($('#drives2').find(':selected').text());
                $('#drives-spec-qty2').text($('#drives_qty2').val());
                $('#drives-qty-symbol2').text(qty);
        }

        if($('#drives3').val() != null && $('#drives_qty3').val() != ''){
            $('#ebayListing').modal('show');
            $('#drives-comma3').text(comma);
            $('#drives-spec3').text($('#drives3').find(':selected').text());
            $('#drives-spec-qty3').text($('#drives_qty3').val());
            $('#drives-qty-symbol3').text(qty);
        }
        
        if($('#slot').val() == '' && $('#slot_qty').val() == ''){
            $('#ebayListing').modal('show');
            $('#slot-spec-qty').text(none);
            $('#slot-spec').text(empty);
            $('#slot-qty-symbol').text(qty).remove();

        }
        else if($('#slot').val() != '' && $('#slot_qty').val() != ''){
            $('#ebayListing').modal('show')
            $('#slot-spec').text($('#slot').val());
            $('#slot-spec-qty').text($('#slot_qty').val());
            $('#slot-qty-symbol').text(qty);
        }

        if($('#slot2').val() != '' && $('#slot_qty2').val() != ''){
            $('#ebayListing').modal('show');
            $('#slot-comma2').text(comma);
            $('#slot-spec2').text($('#slot2').val());
            $('#slot-spec-qty2').text($('#slot_qty2').val());
            $('#slot-qty-symbol2').text(qty);

        }

        if($('#slot3').val() != '' && $('#slot_qty3').val() != ''){
           $('#ebayListing').modal('show');
           $('#slot-comma3').text(comma);
            $('#slot-spec3').text($('#slot3').val());
            $('#slot-spec-qty3').text($('#slot_qty3').val());
            $('#slot-qty-symbol3').text(qty);

        }

        var drivebays = $('#drivebays').text();
        var rearbays = $('#rearbays').text();


        if(drivebays === ''){
        	 $('#drivebays-spec').text();
        }else{
        	$('#drivebays-spec').text(drivebays);
    	}

    	if(rearbays === ''){
        	 $('#rearbays-spec').text();
        }else{
        	$('#rearbays-spec').text(rearbays);
    	}

    	if(drivebays === ''){
        	 $('#drivebays-spec-2').text();
        }else{
        	$('#drivebays-spec-2').text(drivebays);
    	}

    	if(rearbays === ''){
        	 $('#rearbays-spec-2').text();
        }else{
        	$('#rearbays-spec-2').text(rearbays);
    	}

        $('#u-spec').text($('#node').html());
        $('#u-symbol-spec').text(node);
        $('#motherboard-spec').text($('#mfr').html());
        $('#backplane-spec').text($('#backplane').html());
        $('#ob_nic-spec').text($('#ob_nic').html());
        $('#railkit-spec').text($('#mounting').val());
        $('#warranty-spec').text($('#warranty').val());
        $('#pcie_slot-spec').text($('#pcie_slot').html());
           

        if($('#psu').html() == 1){
        	$('#psu-spec').text('Single slot');
        }
        else if($('#psu').html() == 2){
        	$('#psu-spec').text('Dual slot');
        }

        if($('#psu_watts').val() == '' && $('#psu_qty').val() == ''){
            $('#ebayListing').modal('show');
            $('#power-spec-qty').text(none);
            $('#power-spec').text(empty);
            $('#power-qty-symbol').text(qty).remove();

        }
        else if($('#slot').val() != '' && $('#psu_qty').val() != ''){
            $('#ebayListing').modal('show')
            $('#power-spec').text($('#psu_watts').val());
            $('#power-spec-qty').text($('#psu_qty').val());
            $('#power-qty-symbol').text(qty);
        }



    });

});