$(document).ready(function() {
    $("#product-table").on('click', '.check-all-toggle', function() {
        const checkAllToggle = $('.check-all-toggle');
        $("td .checkbox-row")
            .each(function() {
                $(this).prop('checked', checkAllToggle.prop('checked'));
            });
    });

    $("#product-table").on('click', '.btn-batch-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        let rows = [];

        $("td .checkbox-row:checked").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const colPartNumber = columnsToCopy.partNumber;
            const colDescription = columnsToCopy.description;
            const colCondition = columnsToCopy.condition;
            const colQuantity = columnsToCopy.qty;
            const colTotal = columnsToCopy.total;


            const colOem = columnsToCopy.oem;
            const colPrice = columnsToCopy.price;
            
            const isTotalBlank = rowData[colTotal] === undefined || rowData[colTotal].trim() === '';
            const isConditionBlank = rowData[colCondition] === undefined || rowData[colCondition].trim() === '';
            const isPartNumberBlank = rowData[colPartNumber] === undefined || rowData[colPartNumber].trim() === '';
            const isDescriptionBlank = rowData[colDescription] === undefined ||
                  rowData[colDescription].trim() === '';
            const isQuantityBlank = rowData[colQuantity] === undefined || rowData[colQuantity].trim() === '';


            const isOemBlank = rowData[colOem] === undefined || rowData[colOem].trim() === '';
              const isPriceBlank = rowData[colPrice] === undefined || rowData[colPrice].trim() === '';

            const total = isTotalBlank ? '' : `${rowData[colTotal].trim()}`;
            const condition = isConditionBlank ? '' : `   ${rowData[colCondition].trim()}`;
            const partNumber = isPartNumberBlank ? '' : `   ${rowData[colPartNumber].trim()}`;
            const description = isDescriptionBlank ? '' : `   ${rowData[colDescription].trim()}`;
            const quantity = isQuantityBlank ? '' : ` - ${rowData[colQuantity].trim()} qty`;


            const Oem = isOemBlank ? '' : `   ${rowData[colOem].trim()}`;
        const Price = isPriceBlank ? '' : `   ${rowData[colPrice].trim()}`;

            const copiedData = `${Price}${partNumber}${description}` +
                  `${Oem}${quantity}`;

            rows.push(copiedData);
        });

        const text = rows.join('\n');
        $('.textarea-copy').val('');
        $('.textarea-copy').val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    $("#product-table").on('click', '.btn-par-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.partNumber);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    $("#product-table").on('click', '.btn-bar-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.bar);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    $("#product-table").on('click', '.btn-des-copy', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const text = getCopyText(columnsToCopy.description);
        $(".textarea-copy").val('');
        $(".textarea-copy").val(text);

        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);
    });

    function getCopyText(index) {
        let rows = [];
        
        $("td .checkbox-row:checked").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const isCellBlank = rowData[index] !== undefined && rowData[index].trim().length === 0;

            if (!isCellBlank) rows.push(rowData[index].trim());
        });

        return rows.join('\n');
    }
});
