$(document).ready(function() {
    $("#product-table").on('click', '.btn-copy-row', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();

        const rowDOM = copyButton.closest('tr');
        const rowData = [...rowDOM[0].cells].map(function(cell) {
            return cell.textContent;
        });

        const colPartNumber = columnsToCopy.partNumber;
        const colDescription = columnsToCopy.description;
        const colOem = columnsToCopy.oem;
        const colPrice = columnsToCopy.price;
        const colQuantity = columnsToCopy.qty;
        
        
        
        
        const isPartNumberBlank = rowData[colPartNumber] === undefined || rowData[colPartNumber].trim() === '';
        const isDescriptionBlank = rowData[colDescription] === undefined ||
              rowData[colDescription].trim() === '';
              const isOemBlank = rowData[colOem] === undefined || rowData[colOem].trim() === '';
              const isPriceBlank = rowData[colPrice] === undefined || rowData[colPrice].trim() === '';
              const isQuantityBlank = rowData[colQuantity] === undefined || rowData[colQuantity].trim() === '';
        

        
        
        const partNumber = isPartNumberBlank ? '' : `   ${rowData[colPartNumber].trim()}`;
        const description = isDescriptionBlank ? '' : `   ${rowData[colDescription].trim()}`;
        const Oem = isOemBlank ? '' : `   ${rowData[colOem].trim()}`;
        const Price = isPriceBlank ? '' : `   ${rowData[colPrice].trim()}`;
        const Quantity = isQuantityBlank ? '' : ` - ${rowData[colQuantity].trim()} qty`;

        const copiedData = `${Price}${partNumber}` +
              `${description}${Oem}-${Quantity}`;

        $('.textarea-copy').val('');
        $('.textarea-copy').val(copiedData);
        
        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = copiedData;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();
    });
});
