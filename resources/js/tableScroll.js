let stuck = false;
const horizontalScrollerOffset = $('.horizontal-scroller').offset()?.top;
const headerHeight = $('.table-container thead').height();

$(document).ready(function() {
    $(function(){
        /**
         * Ensure scrolling for all elements are in sync
         **/
        $(".horizontal-scroller").scroll(function(){
            $(".table-responsive").scrollLeft($(".horizontal-scroller").scrollLeft());
            $('.sticky-headers').scrollLeft($('.horizontal-scroller').scrollLeft());
        });

        $(".table-responsive").scroll(function(){
            $(".horizontal-scroller").scrollLeft($(".table-responsive").scrollLeft());
            $('.sticky-headers').scrollLeft($('.sticky-headers').scrollLeft());
        });

        $('.sticky-headers').scroll(function() {
            $(".table-responsive").scrollLeft($(".sticky-headers").scrollLeft());
            $(".horizontal-scroller").scrollLeft($(".sticky-headers").scrollLeft());
        });
    });

    $(document).scroll(function(event) {
        const tableOffset = $('.table-container').offset().top;
        const scrollPosition = $(this).scrollTop();

        if (scrollPosition >= (tableOffset + headerHeight) && !stuck) {
            stuck = true;
            $('.sticky-headers').removeClass('d-none');
            $('.sticky-headers').css('overflow', 'visible');
            $('.table-container').css('overflow', 'visible');
        }

        if (scrollPosition < tableOffset + headerHeight && stuck) {
            if (!$('.sticky-headers').hasClass('d-none')) {
                $('.sticky-headers').addClass('d-none');
                $('.sticky-headers').css('overflow', 'auto');
                $('.table-container').css('overflow', 'auto');
            }

            stuck = false;
        }
    });
});

function syncStickyHeader() {
    const tableWidth = $('.main-table').width();
    $('.scroller-content').width(tableWidth);
    $('.sticky-table').width(tableWidth);
    
    $('.main-table thead tr').each(function(index) {
        const stickyHeaderRow = $('.sticky-table thead tr').eq(index);

        $(this).children().each(function(index) {
            stickyHeaderRow.children().eq(index).width($(this).width());
        });
    });
}
