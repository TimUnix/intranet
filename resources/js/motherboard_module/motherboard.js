const errorSkuEmpty = 0;
let baseUrl;

$(document).ready(function() {

	baseUrl = $('#motherboardScript').data('baseUrl');

	const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterSearchSelected = parameters.get('searchSelected');
    const parameterOrderBy = parameters.get('orderBy');
    const parameterSortOrder = parameters.get('sortOrder');


    	$('#searchSelected').find(':selected').prop('selected', false);
        $(`#searchSelected > option[value="${parameterSearchSelected}"]`).prop('selected', true);

        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'orderBy' : parameterOrderBy,
            'sortOrder': parameterSortOrder
        };

        $('#itemSearch').val(searchItem);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/motherboard/search_item`, data, function(response) {
            $('#product_table_body').html(response);
            $('#exportCSV').prop('disabled', false);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
            paginate();
        });

        const countUrl = `${baseUrl}index.php/motherboard/load_listing_count`;

        $.get(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
        }
    });

    $('#searchColumn').click(function(event){
        event.preventDefault();
        var searchSelected = $('#searchSelected').val();
        console.log(searchSelected);
        const searchItem = $('#itemSearch').val();
        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        search();
    });

    $('#submit').click(function(event){


        if($("#sku").val() == " " && !$("#sku").val()){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku').focus();
        }
        else{
            event.preventDefault();
            //$('#product_table_body').html(`<tr class="odd"><td valign="top" colspan="100%" class="datatables_empty">${spinner}</td></tr>`);

            var datas = {
                'sku' : $("#sku").val(),
                'barcode' : $("#barcode").val(),
                'brand': $("#brand").val(),
                'model': $("#model").val(),
                'rev_version': $("#rev_version").val(),
                'qty_cpu': $("#qty_cpu").val(),
                'memory': $("#memory").val(),
                'shield': $("#shield").val(),
                'heatsink': $("#heatsink").val(),
                'request': $("#request").val(),
                ajax : '1'
            };
            console.log(datas);
            $.ajax({
                url : `${baseUrl}index.php/motherboard/createListing`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else if(response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else{
                        const refreshUrl = window.location.href.split('?')[0];
                        window.location.href = refreshUrl;
                    }
                }
            });
        }

    });

    $('#orderBy').change(function(event) {
        search();
    });

    $('#sortOrder').change(function(event) {
        search();
    });

     $('#exportCSV').click(function() {
        const searchItem = $('#itemSearch').val();   
        const searchSelected = $('#searchSelected').val();   
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();

        const data = {
            'searchItem': searchItem,
            'orderBy': parameterOrderBy,
            'sortOrder': parameterSortOrder,
            'searchSelected' : parameterSearchSelected

        };

        const  url = `${baseUrl}index.php/motherboard/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `motherboard_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });


    function search() {
        const searchItem = $('#itemSearch').val();
        const searchSelected = $('#searchSelected').val()
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();


        const route = `${baseUrl}index.php/motherboard?search=${searchItem}`+`&searchSelected=${searchSelected}&orderBy=${orderBy}` +`&sortOrder=${sortOrder}`;
        window.location.href = route;

       
    }

});
