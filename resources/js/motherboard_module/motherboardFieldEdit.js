$(document).ready(function(){

	let skuval;
    let barcodeval;
    let brandval;
    let ffs;

	const baseUrl = $('#motherboardFieldEditScript').data('baseUrl');

    function addEditableFieldsku(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        skuval = value
        console.log(skuval);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    function addEditableFieldbar(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        barcodeval = value
        console.log(barval);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }

     function addEditableField(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    function addDropDownshield(cell, value, className) {
        const input = $(`<select class="form-control ${className}-input w-auto">` +
                        `<option value=""></option>` +
                        `<option value="I/O Shield">I/O Shield</option>` +
                        `</select>`);
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html('<i class="fa fa-check" aria-hidden="true"></i>');

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }

	//SKU
   $('#product_table_body').click('.sku', function(event) {
        if(event.target.className == "sku"){
            const notes = $(event.target).closest('.sku');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel skubtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
        }else if(event.target.className == "fa fa-edit skupenbtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
        }
    });
      

     $('#product_table_body').click('.sku-editable .sku-cancel', function(event) {
        const btnCancel = $(event.target).closest('.sku-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.sku-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('sku-editable');
        notesCell.addClass('sku');
    });

   $('#product_table_body').click('.sku-editable .sku-apply', function(event) {
        const btnApply = $(event.target).closest('.sku-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.sku-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.sku-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('sku-editable');
        notes.addClass('sku');
    
        

        var datas = {
            'data': {
                'sku': skuval, 
                'barcode': barcode.text().trim(),
                'data' :textField.val(), 
                'type' : "sku"}
        };
        console.log(datas);
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                console.log(datas);
            }
        });
    });
   //SKU end

   // Barcode start
  $('#product_table_body').click('.barcode', function(event) {
        if(event.target.className == "barcode"){
            const notes = $(event.target).closest('.barcode');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel barcodebtn"){
            const notes = $(event.target).closest('.barcode');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbar(notes, value, 'barcode');
        }else if(event.target.className == "fa fa-edit barcodepenbtn"){
            const notes = $(event.target).closest('.barcode');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbar(notes, value, 'barcode');
        }
    });
      

     $('#product_table_body').click('.barcode-editable .barcode-cancel', function(event) {
        const btnCancel = $(event.target).closest('.barcode-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.barcode-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit barcodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('barcode-editable');
        notesCell.addClass('barcode');
    });

   $('#product_table_body').click('.barcode-editable .barcode-apply', function(event) {
        const btnApply = $(event.target).closest('.barcode-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.barcode-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        const brand = btnApply.closest('tr').find('.brand');
        
    
        const textField = notes.find('.barcode-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel barcodebtn"><i class="fa fa-edit barcodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('barcode-editable');
        notes.addClass('barcode');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcodeval,
                'data' :textField.val(), 
                'type' : "barcode"}
        };
        console.log(datas);
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                console.log(datas);
            }
        });
        
    });
   //barcode end

   //brand start
    $('#product_table_body').click('.brand', function(event) {
        if(event.target.className == "brand"){
            const notes = $(event.target).closest('.brand');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel brandbtn"){
            const notes = $(event.target).closest('.brand');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'brand');
        }else if(event.target.className == "fa fa-edit brandpenbtn"){
            const notes = $(event.target).closest('.brand');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'brand');
        }
    });
   
    $('#product_table_body').click('.brand-editable .brand-cancel', function(event) {
        const btnCancel = $(event.target).closest('.brand-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.brand-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('brand-editable');
        notesCell.addClass('brand');
    });
    $('#product_table_body').click('.brand-editable .brand-apply', function(event) {
        const btnApply = $(event.target).closest('.brand-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.brand-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.brand-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('brand-editable');
        notes.addClass('brand');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "brand"}
        };
        console.log(datas);
        $.ajax({
            url : `${baseUrl}index.php/drives/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
                console.log(datas);
            }
        });
    });
   //brand end

   //model start
   $('#product_table_body').click('.model', function(event) {
        if(event.target.className == "model"){
            const notes = $(event.target).closest('.model');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel modelbtn"){
            const notes = $(event.target).closest('.model');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'model');
        }else if(event.target.className == "fa fa-edit modelpenbtn"){
            const notes = $(event.target).closest('.model');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'model');
        }
    });
   
    $('#product_table_body').click('.model-editable .model-cancel', function(event) {
        const btnCancel = $(event.target).closest('.model-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.model-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel modelbtn"><i class="fa fa-edit modelpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('model-editable');
        notesCell.addClass('model');
    });
    $('#product_table_body').click('.model-editable .model-apply', function(event) {
        const btnApply = $(event.target).closest('.model-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.model-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.model-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel modelbtn"><i class="fa fa-edit modelpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('model-editable');
        notes.addClass('model');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "model"}
        };
        console.log(datas);
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                console.log(datas);
            }
        });
    });
   //model end

   //rev version start
    $('#product_table_body').click('.rev_version', function(event) {
        if(event.target.className == "rev_version"){
            const notes = $(event.target).closest('.rev_version');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel rev_versionbtn"){
            const notes = $(event.target).closest('.rev_version');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'rev_version');
        }else if(event.target.className == "fa fa-edit rev_versionpenbtn"){
            const notes = $(event.target).closest('.rev_version');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'rev_version');
        }
    });
   
    $('#product_table_body').click('.rev_version-editable .rev_version-cancel', function(event) {
        const btnCancel = $(event.target).closest('.rev_version-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.rev_version-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rev_versionbtn"><i class="fa fa-edit rev_versionpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('rev_version-editable');
        notesCell.addClass('rev_version');
    });
    $('#product_table_body').click('.rev_version-editable .rev_version-apply', function(event) {
        const btnApply = $(event.target).closest('.rev_version-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.rev_version-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.rev_version-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rev_versionbtn"><i class="fa fa-edit rev_versionpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('rev_version-editable');
        notes.addClass('rev_version');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "rev_version"}
        };
        console.log(datas);
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                console.log(datas);
            }
        });
    });
   //Rev Version end

   //Qty CPU start
   $('#product_table_body').click('.qty_cpu', function(event) {
        if(event.target.className == "qty_cpu"){
            const notes = $(event.target).closest('.qty_cpu');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel qty_cpubtn"){
            const notes = $(event.target).closest('.qty_cpu');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'qty_cpu');
        }else if(event.target.className == "fa fa-edit qty_cpupenbtn"){
            const notes = $(event.target).closest('.qty_cpu');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'qty_cpu');
        }
    });
   
    $('#product_table_body').click('.qty_cpu-editable .qty_cpu-cancel', function(event) {
        const btnCancel = $(event.target).closest('.qty_cpu-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.qty_cpu-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qty_cpubtn"><i class="fa fa-edit qty_cpupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('qty_cpu-editable');
        notesCell.addClass('qty_cpu');
    });
    $('#product_table_body').click('.qty_cpu-editable .qty_cpu-apply', function(event) {
        const btnApply = $(event.target).closest('.qty_cpu-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.qty_cpu-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.qty_cpu-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qty_cpubtn"><i class="fa fa-edit qty_cpupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('qty_cpu-editable');
        notes.addClass('qty_cpu');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "qty_cpu"}
        };
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //Qty CPU end

   //memory start
   $('#product_table_body').click('.memory', function(event) {
        if(event.target.className == "memory"){
            const notes = $(event.target).closest('.memory');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel memorybtn"){
            const notes = $(event.target).closest('.memory');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'memory');
        }else if(event.target.className == "fa fa-edit memorypenbtn"){
            const notes = $(event.target).closest('.memory');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'memory');
        }
    });
   
    $('#product_table_body').click('.memory-editable .memory-cancel', function(event) {
        const btnCancel = $(event.target).closest('.memory-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.memory-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memorybtn"><i class="fa fa-edit memorypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('memory-editable');
        notesCell.addClass('memory');
    });
    $('#product_table_body').click('.memory-editable .memory-apply', function(event) {
        const btnApply = $(event.target).closest('.memory-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.memory-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.memory-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel memorybtn"><i class="fa fa-edit memorypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('memory-editable');
        notes.addClass('memory');
    
        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "memory"}
        };
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //memory end

   //shield start
    $('#product_table_body').click('.shield', function(event) {
        if(event.target.className == "shield"){
            const notes = $(event.target).closest('.shield');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel shieldbtn"){
            const notes = $(event.target).closest('.shield');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDownshield(notes, value, 'shield');
        }else if(event.target.className == "fa fa-edit shieldpenbtn"){
            const notes = $(event.target).closest('.shield');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDownshield(notes, value, 'shield');
        }
    });
   
    $('#product_table_body').click('.shield-editable .shield-cancel', function(event) {
        const btnCancel = $(event.target).closest('.shield-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.shield-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel shieldbtn"><i class="fa fa-edit shieldpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('shield-editable');
        notesCell.addClass('shield');
    });
    $('#product_table_body').click('.shield-editable .shield-apply', function(event) {
        const btnApply = $(event.target).closest('.shield-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.shield-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.shield-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel shieldbtn"><i class="fa fa-edit shieldpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('shield-editable');
        notes.addClass('shield');
    
        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "shield"}
        };
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //shield end

   //heatsink start
    $('#product_table_body').click('.heatsink', function(event) {
        if(event.target.className == "heatsink"){
            const notes = $(event.target).closest('.heatsink');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel heatsinkbtn"){
            const notes = $(event.target).closest('.heatsink');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addDropDowninterface(notes, value, 'heatsink');
        }else if(event.target.className == "fa fa-edit heatsinkpenbtn"){
            const notes = $(event.target).closest('.heatsink');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableField(notes, value, 'heatsink');
        }
    });
   
    $('#product_table_body').click('.heatsink-editable .heatsink-cancel', function(event) {
        const btnCancel = $(event.target).closest('.heatsink-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.heatsink-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel heatsinkbtn"><i class="fa fa-edit heatsinkpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('heatsink-editable');
        notesCell.addClass('heatsink');
    });
    $('#product_table_body').click('.heatsink-editable .heatsink-apply', function(event) {
        const btnApply = $(event.target).closest('.heatsink-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.heatsink-editable');
        const sku = btnApply.closest('tr').find('.sku');
        const bar = btnApply.closest('tr').find('.barcode');
        
    
        const textField = notes.find('.heatsink-input');
        if (!textField) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel heatsinkbtn"><i class="fa fa-edit heatsinkpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('heatsink-editable');
        notes.addClass('heatsink');
    
        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'barcode': barcode.text().trim(), 
                'data' : textField.val(), 
                'type' : "heatsink"}
        };
        $.ajax({
            url : `${baseUrl}index.php/motherboard/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
            }
        });
    });
   //heatsink end

});