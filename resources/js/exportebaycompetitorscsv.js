$(document).ready(function() {
    


    $(".ret").click(function() {
        
        let rows = [];
        let part = [];

        $("td .checkbox-row").each(function() {
            const row = $(this).closest("tr");
            const rowData = [...row[0].cells].map(function(cell) {
                return cell.textContent;
            });

            const colPartNumber = columnsToCopy.itemnumber;
            
            const isPartNumberBlank = rowData[colPartNumber] === undefined || rowData[colPartNumber].trim() === '';
            
            const partNumber = isPartNumberBlank ? '' : `   ${rowData[colPartNumber].trim()}`;
            
            part.push(partNumber);

        }); 
        console.log(part)
        data = {"part_number" : part};
        
        window.location = `${baseUrl}index.php/ebay_competitors/generate_csv/` + "?id=" + JSON.stringify(data);
    });

   
});
