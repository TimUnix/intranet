const errorSkuEmpty = 0;
let baseUrl;

let currentHeader = '';
let lastHeader = '';
let isColumnSortingReversed = false;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {

	let itemsLimit = 100;
    let itemsOffset = 0;
    let itemsRequested = false;

	baseUrl = $('#serverScript').data('baseUrl');

    syncStickyHeader();

    $('#warranty-').hide();
    $('#shipping-').hide();
	const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterSearchSelected = parameters.get('searchSelected');
    const parameterOrderBy = parameters.get('orderBy');
    const parameterSortOrder = parameters.get('sortOrder');

    	$('#searchSelected').find(':selected').prop('selected', false);
        $(`#searchSelected > option[value="${parameterSearchSelected}"]`).prop('selected', true);

        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'orderBy' : parameterOrderBy,
            'sortOrder': parameterSortOrder
        };

        $('#itemSearch').val(searchItem);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/server/search_item`, data, function(response) {
            $('#product_table_body').html(response);
             $('#exportCSV').prop('disabled', false);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
            $('.sticky-table').width(tableWidth);
            syncStickyHeader();
        });

        const countUrl = `${baseUrl}index.php/server/load_listing_count`;

        $.get(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
            syncStickyHeader();
        }
    });

	$('#searchColumn').click(function(event){
        event.preventDefault();
        var searchSelected = $('#searchSelected').val();
        const searchItem = $('#itemSearch').val();
        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        search();
    });

	$('#submit').click(function(event){


        if($("#sku").val() == " " && !$("#sku").val()){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku').focus();
        } else{
            event.preventDefault();
           
            var datas = {
                'sku' : $("#sku").val(),
                'con' : $("#con").val(),
				'units': $("#units").val(),
                'brand' : $("#brand").val(),
                'model': $("#model").val(),
                'nodes': $("#nodes").val(),
                'bays': $("#bays").val(),
                'drive_ff': $("#drive_ff").val(),
                'backplane': $("#backplane").val(),
                'socket_supported': $("#socket_supported").val(),
                'qty_of_power_supply': $("#qty_of_power_supply").val(),
                ajax : '1'
            };
           
            $.ajax({
                url : `${baseUrl}index.php/server/createServer`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else{
                        const refreshUrl = window.location.href.split('?')[0];
                        window.location.href = refreshUrl;
                    }
                }
            });
        }

    });

	$('#exportCSV').click(function() {
        const searchItem = $('#itemSearch').val();   
        const searchSelected = $('#searchSelected').val();   
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();

        const data = {
            'searchItem': searchItem,
            'orderBy': parameterOrderBy,
            'sortOrder': parameterSortOrder,
            'searchSelected' : parameterSearchSelected

        };

        const  url = `${baseUrl}index.php/server/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `server_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

	$('#orderBy').change(function() {
        search();
    });

    $('#sortOrder').change(function(event) {
        search();
    });

	$('#preview_template_submit').click(function(event){

        var title = $('#title_template').val();
		var units = $('#units_template').val();
        var brand = $('#brand_template').val();
        var model = $('#model_template').val();
        var ff = $('#ff_template').val();
		var interface = $('#interface_template').val();
        var condition = $('#condition_template').val();
        var warranty = $('#warranty_template').val();
        var description = $('#description_template').val();
       
        if(title == '' || interface == '' || brand == '' || model == '' || ff == '' 
            || condition == '' || warranty == '' || description == ''){
                $("#validate_template").html(`<h5 style="color:red">Please complete the data below</h5>`);
            }else{
                $('#previewTemplate').modal('show');
     
        if(interface == 'SATA' || interface == 'SATA SSD'){
            $('.application_preview').text('Enterprise Storage | Data Center Servers');
        }else if(interface == 'SAS' || interface == 'SAS SSD'){
            $('.application_preview').text('Enterprise Storage or Home PC');
        }else if(interface == 'NVME'){
            $('.application_preview').text('Enterprise Storage | Data Center Servers');      
        }else{
            $('.application_preview').text();
        }

        $('.title_preview').text(title);
		$('.units_preview').text(units);
        $('.brand_preview').text(brand);
        $('.model_preview').text(model);
        $('.ff_preview').text(ff);
        $('.interface_preview').text(interface);
        $('.condition_preview').text(condition);
        $('.warranty_preview').text(warranty);
        $('.description_preview').text(description);

        document.getElementById("image_preview").src =`${baseUrl}resources/img/server/${units}-${brand}-${model}.png`;

        }  
    });

	$('#generate_template_submit').click(function(){
        var title = $('#title_template').val();
		var units = $('#units_template').val();
        var brand = $('#brand_template').val();
        var model = $('#model_template').val();
        var ff = $('#form_factor_template').val();
		var interface = $('#interface_template').val();
        var condition = $('#condition_template').val();
        var warranty = $('#warranty_template').val();
        var description = $('#description_template').val();
        if(title == '' || interface == '' || brand == '' || model == '' || ff == '' 
            || condition == '' || warranty == '' || description == ''){
                $("#validate_template").html(`<h5 style="color:red">Please complete the data below</h5>`);
        }else{
            $('.html-box').show();
                
                
            if(interface == 'SATA' || interface == 'SATA SSD'){
                $('.application_preview').text('Enterprise Storage | Data Center Servers');
            }else if(interface == 'SAS' || interface == 'SAS SSD'){
                $('.application_preview').text('Enterprise Storage or Home PC');
            }else if(interface == 'NVME'){
                $('.application_preview').text('Enterprise Storage | Data Center Servers');      
            }else{
                 $('.application_preview').text();
            }
            $('.title_preview').text(title);
			$('.units_preview').text(units);
            $('.brand_preview').text(brand);
            $('.model_preview').text(model);
            $('.ff_preview').text(ff);
            $('.interface_preview').text(interface);
            $('.condition_preview').text(condition);
            $('.warranty_preview').text(warranty);
            $('.description_preview').text(description);
            document.getElementById("image_preview_html").src =`https://intranet.unixsurplus.com/resources/img/server/${units}-${brand}-${model}.png`;
        
            let clone = document.getElementById("whole_html").innerHTML;
        
            $('#copy_html_template').val(clone);
        }
       
    });

	 $('#button_copy').click(function(){
        $("#copy_html_template").select();
        document.execCommand('copy');
    });

	$('#about_us').click(function(){
        $('#about-us').show();
        $('#warranty-').hide();
        $('#shipping-').hide();
    });
    $('#warranty').click(function(){
        $('#about-us').hide();
        $('#shipping-').hide();
        $('#warranty-').show();
    });
    $('#shipping').click(function(){
        $('#about-us').hide();
        $('#warranty-').hide();
        $('#shipping-').show();
    });

	$(document).scroll(function() {
        const rowCount = $('#product_table_body > tr').length;

        // if the rows initially returned are less than 100, there are no more rows to load further
        if (rowCount < itemsLimit) return;
        const position = $(this).scrollTop();
        let maxHeight = 0;
       
        $('#product_table_body > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            itemsOffset += itemsLimit;

            const categories = $('.category-filter:checked').map(function() {
                return $(this).val();
            }).toArray();

            const data = {
                'searchItem': searchItem,
                'searchSelected' : parameterSearchSelected,
                'orderBy': currentHeader,
                'sortOrder': isColumnSortingReversed ? 'desc' : 'asc',
                'items_offset': itemsOffset,
                'categories' : categories
            };

            $.get(`${baseUrl}index.php/server/search_item`, data, function(response) {
                $('#product_table_body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    function search() {
		try {
        const searchItem = $('#itemSearch').val();
        const searchSelected = $('#searchSelected').val();
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();


        const route = `${baseUrl}index.php/server?search=${searchItem}`+`&searchSelected=${searchSelected}&orderBy=${orderBy}` +`&sortOrder=${sortOrder}`;
        window.location.href = route;
		} catch (error) {
            
        }
       
    }

});
