$(document).ready(function(){

	let skuval;

	const baseUrl = $('#serverFieldEditScript').data('baseUrl');

	function addEditableFieldsku(cell, value, className) {
        const input = $(`<input autofocus type="text" onfocus="this.selectionStart = this.selectionEnd = this.value.length;" class="form-control ${className}-input" value="${value}">`);
        skuval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }

     function addEditableField(cell, value, className) {
        const input = $(`<input autofocus type="text" onfocus="this.selectionStart = this.selectionEnd = this.value.length;" class="form-control ${className}-input" value="${value}">`);
        value

        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }

	//SKU
   $('#product_table_body').click('.sku', function(event) {
        if(event.target.className == "sku"){
            const notes = $(event.target).closest('.sku');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-primary draftid-cancel skubtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
        }else if(event.target.className == "fa fa-edit skupenbtn"){
            const notes = $(event.target).closest('.sku');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsku(notes, value, 'sku');
            $("input:text").focus();
        }
    });
      

     $('#product_table_body').click('.sku-editable .sku-cancel', function(event) {
        const btnCancel = $(event.target).closest('.sku-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.sku-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('sku-editable');
        notesCell.addClass('sku');
    });

   $('#product_table_body').click('.sku-editable .sku-apply', function(event) {
        const btnApply = $(event.target).closest('.sku-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.sku-editable');
        const sku = btnApply.closest('tr').find('.sku');
        
        const textField = notes.find('.sku-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('sku-editable');
        notes.addClass('sku');
    
        

        var datas = {
            'data': {
                'sku': skuval, 
                'data' :textField.val(), 
                'type' : "sku"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //SKU end

	$('#product_table_body').click('.sku-editable .sku-cancel', function(event) {
		const btnCancel = $(event.target).closest('.sku-cancel');
		if (!btnCancel) return;

		const notesCell = btnCancel.closest('.sku-editable');
		

		notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
		<!--<button type="button" class="btn btn-link copy-field">
			<i class="fa fa-copy" aria-hidden="true"></i>
		</button>--> ${notesCell.data('value')}`);
		notesCell.removeClass('sku-editable');
		notesCell.addClass('sku');
	});
	$('#product_table_body').click('.sku-editable .sku-apply', function(event) {
		const btnApply = $(event.target).closest('.sku-apply');
		if (!btnApply) return;

		const notes = btnApply.closest('.sku-editable');
		const sku = btnApply.closest('tr').find('.sku');

		const textField = notes.find('.sku-input');
		if (!textField) return;

		notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
		<!--<button type="button" class="btn btn-link copy-field">
			<i class="fa fa-copy" aria-hidden="true"></i>
		</button>--> 
		${textField.val()}`);
		notes.data('value', textField.val());
		notes.removeClass('sku-editable');
		notes.addClass('sku');

		

		var datas = {
			'data': {
				'sku': sku.text().trim(), 
				'data' : textField.val(), 
				'type' : "sku"}
		};
		
		$.ajax({
			url : `${baseUrl}index.php/server/update_columns`,
			type : "post",
			data : datas,
			success : function(msg){

			}
		});
	});
	//SKU end

   //CONDITION start
   $('#product_table_body').click('.con', function(event) {
    if(event.target.className == "con"){
        const notes = $(event.target).closest('.con');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel conbtn"){
        const notes = $(event.target).closest('.con');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'con');

    }else if(event.target.className == "fa fa-edit conpenbtn"){
        const notes = $(event.target).closest('.con');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'con');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.con-editable .con-cancel', function(event) {
        const btnCancel = $(event.target).closest('.con-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.con-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('con-editable');
        notesCell.addClass('con');
    });
    $('#product_table_body').click('.con-editable .con-apply', function(event) {
        const btnApply = $(event.target).closest('.con-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.con-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.con-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('con-editable');
        notes.addClass('sku');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "con"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //CONDITION end

   //UNITS start
   $('#product_table_body').click('.units', function(event) {
    if(event.target.className == "units"){
        const notes = $(event.target).closest('.units');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel unitsbtn"){
        const notes = $(event.target).closest('.units');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'units');

    }else if(event.target.className == "fa fa-edit unitspenbtn"){
        const notes = $(event.target).closest('.units');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'units');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.units-editable .units-cancel', function(event) {
        const btnCancel = $(event.target).closest('.units-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.units-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel unitsbtn"><i class="fa fa-edit unitspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('units-editable');
        notesCell.addClass('units');
    });
    $('#product_table_body').click('.units-editable .units-apply', function(event) {
        const btnApply = $(event.target).closest('.units-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.units-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.units-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel unitsbtn"><i class="fa fa-edit unitspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('units-editable');
        notes.addClass('units');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "units"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //UNITS end


   //BRAND start
   $('#product_table_body').click('.brand', function(event) {
    if(event.target.className == "brand"){
        const notes = $(event.target).closest('.brand');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel brandbtn"){
        const notes = $(event.target).closest('.brand');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'brand');

    }else if(event.target.className == "fa fa-edit brandpenbtn"){
        const notes = $(event.target).closest('.brand');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'brand');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.brand-editable .brand-cancel', function(event) {
        const btnCancel = $(event.target).closest('.brand-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.brand-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel branddbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('brand-editable');
        notesCell.addClass('brand');
    });
    $('#product_table_body').click('.brand-editable .brand-apply', function(event) {
        const btnApply = $(event.target).closest('.brand-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.brand-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.brand-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel brandbtn"><i class="fa fa-edit brandpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('brand-editable');
        notes.addClass('brand');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "brand"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //BRAND end

   //MODEL start
   $('#product_table_body').click('.model', function(event) {
    if(event.target.className == "model"){
        const notes = $(event.target).closest('.model');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel modelbtn"){
        const notes = $(event.target).closest('.model');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'model');

    }else if(event.target.className == "fa fa-edit modelpenbtn"){
        const notes = $(event.target).closest('.model');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'model');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.model-editable .model-cancel', function(event) {
        const btnCancel = $(event.target).closest('.model-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.model-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel modelbtn"><i class="fa fa-edit modelpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('model-editable');
        notesCell.addClass('model');
    });
    $('#product_table_body').click('.model-editable .model-apply', function(event) {
        const btnApply = $(event.target).closest('.model-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.model-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.model-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel modelbtn"><i class="fa fa-edit modelpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('model-editable');
        notes.addClass('model');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "model"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //MODEL end

   //NODES start
   $('#product_table_body').click('.nodes', function(event) {
    if(event.target.className == "nodes"){
        const notes = $(event.target).closest('.nodes');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel nodesbtn"){
        const notes = $(event.target).closest('.nodes');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'nodes');

    }else if(event.target.className == "fa fa-edit nodespenbtn"){
        const notes = $(event.target).closest('.nodes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'nodes');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.nodes-editable .nodes-cancel', function(event) {
        const btnCancel = $(event.target).closest('.nodes-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.nodes-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel nodesbtn"><i class="fa fa-edit nodespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('nodes-editable');
        notesCell.addClass('nodes');
    });
    $('#product_table_body').click('.nodes-editable .nodes-apply', function(event) {
        const btnApply = $(event.target).closest('.nodes-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.nodes-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.nodes-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel nodesbtn"><i class="fa fa-edit nodespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('nodes-editable');
        notes.addClass('nodes');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "nodes"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //NODES end

   //BAYS start
   $('#product_table_body').click('.bays', function(event) {
    if(event.target.className == "bays"){
        const notes = $(event.target).closest('.bays');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel baysbtn"){
        const notes = $(event.target).closest('.bays');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'bays');

    }else if(event.target.className == "fa fa-edit bayspenbtn"){
        const notes = $(event.target).closest('.bays');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'bays');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.bays-editable .bays-cancel', function(event) {
        const btnCancel = $(event.target).closest('.bays-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.bays-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel baysbtn"><i class="fa fa-edit bayspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('bays-editable');
        notesCell.addClass('bays');
    });
    $('#product_table_body').click('.bays-editable .bays-apply', function(event) {
        const btnApply = $(event.target).closest('.bays-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.bays-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.bays-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel baysbtn"><i class="fa fa-edit bayspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('bays-editable');
        notes.addClass('bays');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "bays"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //BAYS end

   //DRIVE_FF start
   $('#product_table_body').click('.drive_ff', function(event) {
    if(event.target.className == "drive_ff"){
        const notes = $(event.target).closest('.drive_ff');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel drive_ffbtn"){
        const notes = $(event.target).closest('.drive_ff');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'drive_ff');

    }else if(event.target.className == "fa fa-edit drive_ffpenbtn"){
        const notes = $(event.target).closest('.drive_ff');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'drive_ff');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.drive_ff-editable .drive_ff-cancel', function(event) {
        const btnCancel = $(event.target).closest('.drive_ff-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.drive_ff-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel drive_ffbtn"><i class="fa fa-edit drive_ffpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('drive_ff-editable');
        notesCell.addClass('drive_ff');
    });
    $('#product_table_body').click('.drive_ff-editable .drive_ff-apply', function(event) {
        const btnApply = $(event.target).closest('.drive_ff-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.drive_ff-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.drive_ff-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel drive_ffbtn"><i class="fa fa-edit drive_ffpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('drive_ff-editable');
        notes.addClass('drive_ff');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "drive_ff"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //DRIVE_FF end

   //BACKPLANE start
   $('#product_table_body').click('.backplane', function(event) {
    if(event.target.className == "backplane"){
        const notes = $(event.target).closest('.backplane');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel backplanebtn"){
        const notes = $(event.target).closest('.backplane');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'backplane');

    }else if(event.target.className == "fa fa-edit backplanepenbtn"){
        const notes = $(event.target).closest('.backplane');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'backplane');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.backplane-editable .backplane-cancel', function(event) {
        const btnCancel = $(event.target).closest('.backplane-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.backplane-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel backplanebtn"><i class="fa fa-edit backplanepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('backplane-editable');
        notesCell.addClass('backplane');
    });
    $('#product_table_body').click('.backplane-editable .backplane-apply', function(event) {
        const btnApply = $(event.target).closest('.backplane-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.backplane-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.backplane-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel backplanebtn"><i class="fa fa-edit backplanepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('backplane-editable');
        notes.addClass('backplane');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "backplane"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //BACKPLANE end

   //SOCKET start
   $('#product_table_body').click('.socket_supported', function(event) {
    if(event.target.className == "socket_supported"){
        const notes = $(event.target).closest('.socket_supported');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel socket_supportedbtn"){
        const notes = $(event.target).closest('.socket_supported');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'socket_supported');

    }else if(event.target.className == "fa fa-edit socket_supportedpenbtn"){
        const notes = $(event.target).closest('.socket_supported');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'socket_supported');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.socket_supported-editable .socket_supported-cancel', function(event) {
        const btnCancel = $(event.target).closest('.socket_supported-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.socket_supported-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel socket_supportedbtn"><i class="fa fa-edit socket_supportedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('socket_supported-editable');
        notesCell.addClass('socket_supported');
    });
    $('#product_table_body').click('.socket_supported-editable .socket_supported-apply', function(event) {
        const btnApply = $(event.target).closest('.socket_supported-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.socket_supported-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.socket_supported-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel socket_supportedbtn"><i class="fa fa-edit socket_supportedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('socket_supported-editable');
        notes.addClass('socket_supported');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "socket_supported"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //SOCKET end

   //POWER_SUPPLY start
   $('#product_table_body').click('.qty_of_power_supply', function(event) {
    if(event.target.className == "qty_of_power_supply"){
        const notes = $(event.target).closest('.qty_of_power_supply');

        if (!notes) return;

        const value = notes.data('value');
        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel qty_of_power_supplybtn"){
        const notes = $(event.target).closest('.qty_of_power_supply');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableField(notes, value, 'qty_of_power_supply');

    }else if(event.target.className == "fa fa-edit qty_of_power_supplypenbtn"){
        const notes = $(event.target).closest('.qty_of_power_supply');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'qty_of_power_supply');
        $("input:text").focus();

    }
    });

    $('#product_table_body').click('.qty_of_power_supply-editable .qty_of_power_supply-cancel', function(event) {
        const btnCancel = $(event.target).closest('.qty_of_power_supply-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.qty_of_power_supply-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qty_of_power_supplybtn"><i class="fa fa-edit qty_of_power_supplypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('qty_of_power_supply-editable');
        notesCell.addClass('qty_of_power_supply');
    });
    $('#product_table_body').click('.qty_of_power_supply-editable .qty_of_power_supply-apply', function(event) {
        const btnApply = $(event.target).closest('.qty_of_power_supply-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.qty_of_power_supply-editable');
        const sku = btnApply.closest('tr').find('.sku');

        const textField = notes.find('.qty_of_power_supply-input');
        if (!textField) return;

        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qty_of_power_supplybtn"><i class="fa fa-edit qty_of_power_supplypenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('qty_of_power_supply-editable');
        notes.addClass('qty_of_power_supply');

        

        var datas = {
            'data': {
                'sku': sku.text().trim(), 
                'data' : textField.val(), 
                'type' : "qty_of_power_supply"}
        };
        
        $.ajax({
            url : `${baseUrl}index.php/server/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){

            }
        });
    });
   //POWER_SUPPLY end

});



