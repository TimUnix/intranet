let baseUrl;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {
    
    let itemsOffset = 0;
    // let lastId = 0;
    let itemsLimit = 100;
    let itemsRequested = false;
  
    baseUrl = $('#switches_reportScript').data('baseUrl');
    syncStickyHeader();

    $.get(`${baseUrl}index.php/switch_report/load_report`,  function(response) {
        $('#switch_report_table_body').html(response);
                const tableWidth = $('#switch_report_table_body').width();
                $('.scroller-content').width(tableWidth); 
                $('.sticky-table').width(tableWidth);
                syncStickyHeader(); 
    });

    $(document).scroll(function() {
        const rowCount = $('#switch_report_table_body > tr').length;
        console.log(rowCount);
        // if the rows initially returned are less than 100, there are no more rows to load further
        if (rowCount < itemsLimit) return;
        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('#switch_report_table_body > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            itemsOffset += itemsLimit;

            const data = {
                'items_offset': itemsOffset,
            };

            $.post(`${baseUrl}index.php/switch_report/load_report`, data, function(response) {
                $('#switches_report_table_body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    
    $('#myModal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });


    $('#searchColumn').click(function(event){
        var data = {
        'searchItem' : $('#itemSearch').val().trim()}
        $.get(`${baseUrl}index.php/switch_report/search_item`,data,  function(response) {
            $('#switch_report_table_body').html(response);
                const tableWidth = $('#switch_report_table_body').width();
                $('.scroller-content').width(tableWidth); 
                $('.sticky-table').width(tableWidth);
                syncStickyHeader(); 
        });
        
    });

    $("#itemSearch").keypress(function(event) {
        if (event.keyCode == 13) {
            $("#searchColumn").click();
        }
    });


    $('#submitf').on('click',function(event){  
            if  (($("#serial").val() == "")){
                $("#serial").focus();
                alert("Serial # must be filled out");
                return false;
            }else{
                var data = {
                    'eqm_mpn' : $("#eqm_mpn").val(),
                    'eqm_des' : $("#eqm_des").val(),
                    'data_dest': $("#data_dest").val(),
                    'qty': $("#qty").val(),
                    'boxed': $("#boxed").val(),
                    'despostion': $("#despostion").val(),
                    'serial': $("#serial").val(),
                    'recieved': $("#recieved").val(),
                    'notes': $("#notes").val(),
                    ajax : '1'
                };
                console.log(data);
                $.ajax({
                    url : `${baseUrl}index.php/switch_report/createListing`,
                    type : "post",
                    data : data,
                    success : function(response){
                       
                            const refreshUrl = window.location.href.split('?')[0];
                            window.location.href = refreshUrl;
                        
                    }
                
                });
            }
        });
    });
