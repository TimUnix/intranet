
let newval
function addEditableField(cell, value, className) {
    
    const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
    newval = value;
    const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
    btnAccept.html(
        '<i class="fa fa-check" aria-hidden="true"></i>'
    );

    const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
    btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

    $(cell).html(input).append(btnCancel).append(btnAccept);
    $(cell).removeClass(className);
    $(cell).addClass(`${className}-editable`);
}

function addEditableselectyesno(cell, value, className) {

    const input = $(`<select class="form-control ${className}-input">`+
    `<option value="Yes">Yes</option>`+
    `<option value="No">No</option>`+
    `</select>`);     
    //newval = value;

    const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
    btnAccept.html(
        '<i class="fa fa-check" aria-hidden="true"></i>'
    );

    const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
    btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

    $(cell).html(input).append(btnCancel).append(btnAccept);
    $(cell).removeClass(className);
    $(cell).addClass(`${className}-editable`);
}


$('#switch_report_table_body').on('click','.serial', function(event) {
    
    if(event.target.className == "serial"){
        const notes = $(event.target).closest('.serial');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel serialbtn"){
        const notes = $(event.target).closest('.serial');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'serial');
    }else if(event.target.className == "fa fa-edit fa-sm serialpenbtn"){
        const notes = $(event.target).closest('.serial');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'serial');
    }
});

$('#switch_report_table_body').click('.serial-editable .serial-cancel', function(event) {
    const btnCancel = $(event.target).closest('.serial-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.serial-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('serial-editable');
    notesCell.addClass('serial');
});

$('#switch_report_table_body').click('.serial-editable .serial-apply', function(event) {
    
    const btnApply = $(event.target).closest('.serial-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.serial-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.serial-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit fa-sm serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('serial-editable');
    notes.addClass('serial');

    

    var datas = {
        'data': {
            'serial': newval, 
            'id': id,
            'data' :textField.val(), 
            'type' : "serial"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});


$('#switch_report_table_body').on('click','.eqm_mpnbtn', function(event) {
    if(event.target.className == "eqm_mpn"){
        const notes = $(event.target).closest('.eqm_mpn');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel eqm_mpnbtn"){
        const notes = $(event.target).closest('.eqm_mpn');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'eqm_mpn');
    }else if(event.target.className == "fa fa-edit fa-sm eqm_mpnpenbtn"){
        const notes = $(event.target).closest('.eqm_mpn');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'eqm_mpn');
    }
});

$('#switch_report_table_body').click('.eqm_mpn-editable .eqm_mpn-cancel', function(event) {
    const btnCancel = $(event.target).closest('.eqm_mpn-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.eqm_mpn-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel eqm_mpnbtn"><i class="fa fa-edit eqm_mpnbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('eqm_mpn-editable');
    notesCell.addClass('eqm_mpn');
});

$('#switch_report_table_body').click('.eqm_mpn-editable .eqm_mpn-apply', function(event) {
    
    const btnApply = $(event.target).closest('.eqm_mpn-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    

    const notes = btnApply.closest('.eqm_mpn-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.eqm_mpn-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel eqm_mpnbtn"><i class="fa fa-edit fa-sm eqm_mpnpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('eqm_mpn-editable');
    notes.addClass('eqm_mpn');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "eqm_mpn"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

//

//

$('#switch_report_table_body').on('click','.eqm_desbtn', function(event) {
    if(event.target.className == "eqm_des"){
        const notes = $(event.target).closest('.eqm_des');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel eqm_desbtn"){
        const notes = $(event.target).closest('.eqm_des');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'eqm_des');
    }else if(event.target.className == "fa fa-edit fa-sm eqm_despenbtn"){
        const notes = $(event.target).closest('.eqm_des');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'eqm_des');
    }
});

$('#switch_report_table_body').click('.eqm_des-editable .eqm_des-cancel', function(event) {
    const btnCancel = $(event.target).closest('.eqm_des-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.eqm_des-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('eqm_des-editable');
    notesCell.addClass('eqm_des');
});

$('#switch_report_table_body').click('.eqm_des-editable .eqm_des-apply', function(event) {
    
    const btnApply = $(event.target).closest('.eqm_des-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.eqm_des-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.eqm_des-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel eqm_desbtn"><i class="fa fa-edit fa-sm eqm_despenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('eqm_des-editable');
    notes.addClass('eqm_des');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "eqm_des"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//


//

$('#switch_report_table_body').on('click','.data_destbtn', function(event) {
    if(event.target.className == "data_dest"){
        const notes = $(event.target).closest('.data_dest');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel data_destbtn"){
        const notes = $(event.target).closest('.data_dest');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselectyesno(notes, value, 'data_dest');
    }else if(event.target.className == "fa fa-edit fa-sm data_destpenbtn"){
        const notes = $(event.target).closest('.data_dest');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselectyesno(notes, value, 'data_dest');
    }
});

$('#switch_report_table_body').click('.data_dest-editable .data_dest-cancel', function(event) {
    const btnCancel = $(event.target).closest('.data_dest-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.data_dest-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('data_dest-editable');
    notesCell.addClass('data_dest');
});

$('#switch_report_table_body').click('.data_dest-editable .data_dest-apply', function(event) {
    
    const btnApply = $(event.target).closest('.data_dest-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.data_dest-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.data_dest-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel data_destbtn"><i class="fa fa-edit fa-sm data_destpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('data_dest-editable');
    notes.addClass('data_dest');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "data_dest"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switch_report_table_body').on('click','.qtybtn', function(event) {
    if(event.target.className == "qty"){
        const notes = $(event.target).closest('.qty');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel qtybtn"){
        const notes = $(event.target).closest('.qty');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'qty');
    }else if(event.target.className == "fa fa-edit fa-sm qtypenbtn"){
        const notes = $(event.target).closest('.qty');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'qty');
    }
});

$('#switch_report_table_body').click('.qty-editable .qty-cancel', function(event) {
    const btnCancel = $(event.target).closest('.qty-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.qty-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('qty-editable');
    notesCell.addClass('qty');
});

$('#switch_report_table_body').click('.qty-editable .qty-apply', function(event) {
    
    const btnApply = $(event.target).closest('.qty-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.qty-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.qty-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qtybtn"><i class="fa fa-edit fa-sm qtypenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('qty-editable');
    notes.addClass('qty');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "qty"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switch_report_table_body').on('click','.boxedbtn', function(event) {
    if(event.target.className == "boxed"){
        const notes = $(event.target).closest('.boxed');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel boxedbtn"){
        const notes = $(event.target).closest('.boxed');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselectyesno(notes, value, 'boxed');
    }else if(event.target.className == "fa fa-edit fa-sm boxedpenbtn"){
        const notes = $(event.target).closest('.boxed');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselectyesno(notes, value, 'boxed');
    }
});

$('#switch_report_table_body').click('.boxed-editable .boxed-cancel', function(event) {
    const btnCancel = $(event.target).closest('.boxed-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.boxed-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('boxed-editable');
    notesCell.addClass('boxed');
});

$('#switch_report_table_body').click('.boxed-editable .boxed-apply', function(event) {
    
    const btnApply = $(event.target).closest('.boxed-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.boxed-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.boxed-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel boxedbtn"><i class="fa fa-edit fa-sm boxedpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('boxed-editable');
    notes.addClass('boxed');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "boxed"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switch_report_table_body').on('click','.despostionbtn', function(event) {
    if(event.target.className == "despostion"){
        const notes = $(event.target).closest('.despostion');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel despostionbtn"){
        const notes = $(event.target).closest('.despostion');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'despostion');
    }else if(event.target.className == "fa fa-edit fa-sm despostionpenbtn"){
        const notes = $(event.target).closest('.despostion');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'despostion');
    }
});

$('#switch_report_table_body').click('.despostion-editable .despostion-cancel', function(event) {
    const btnCancel = $(event.target).closest('.despostion-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.despostion-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('despostion-editable');
    notesCell.addClass('despostion');
});

$('#switch_report_table_body').click('.despostion-editable .despostion-apply', function(event) {
   
    const btnApply = $(event.target).closest('.despostion-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.despostion-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.despostion-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel despostionbtn"><i class="fa fa-edit fa-sm despostionpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('despostion-editable');
    notes.addClass('despostion');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "despostion"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switch_report_table_body').on('click','.notesbtn', function(event) {
    if(event.target.className == "notes"){
        const notes = $(event.target).closest('.notes');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel notesbtn"){
        const notes = $(event.target).closest('.notes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'notes');
    }else if(event.target.className == "fa fa-edit fa-sm notespenbtn"){
        const notes = $(event.target).closest('.notes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'notes');
    }
});

$('#switch_report_table_body').click('.notes-editable .notes-cancel', function(event) {
    const btnCancel = $(event.target).closest('.notes-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.notes-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel serialbtn"><i class="fa fa-edit serialpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('notes-editable');
    notesCell.addClass('notes');
});

$('#switch_report_table_body').click('.notes-editable .notes-apply', function(event) {
    
    const btnApply = $(event.target).closest('.notes-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.notes-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.notes-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel notesbtn"><i class="fa fa-edit fa-sm notespenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('notes-editable');
    notes.addClass('notes');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "notes"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });


});

//

$('#switch_report_table_body').on('click','.recievedbtn', function(event) {
    if(event.target.className == "recieved"){
        const notes = $(event.target).closest('.recieved');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel recievedbtn"){
        const notes = $(event.target).closest('.recieved');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'recieved');
    }else if(event.target.className == "fa fa-edit fa-sm recievedpenbtn"){
        const notes = $(event.target).closest('.recieved');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'recieved');
    }
});

$('#switch_report_table_body').click('.recieved-editable .recieved-cancel', function(event) {
    const btnCancel = $(event.target).closest('.recieved-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.recieved-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel recievedbtn"><i class="fa fa-edit recievedpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('recieved-editable');
    notesCell.addClass('recieved');
});

$('#switch_report_table_body').click('.recieved-editable .recieved-apply', function(event) {
   
    const btnApply = $(event.target).closest('.recieved-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.recieved-editable');
    const serial = btnApply.closest('tr').find('.serial');
    const id = btnApply.closest('tr').find('.idnum').data("id");
    

    const textField = notes.find('.recieved-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel recievedbtn"><i class="fa fa-edit fa-sm recievedpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('recieved-editable');
    notes.addClass('recieved');

    

    var datas = {
        'data': {
            'serial': serial.text().trim(), 
            'id': id,
            'data' :textField.val(), 
            'type' : "recieved"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switch_report/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});
