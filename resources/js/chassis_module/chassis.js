
$(document).ready(function() {
	baseUrl = $('#chassisScript').data('baseUrl');

	/*const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterOrderBy = parameters.get('orderBy');
    const parameterSortOrder = parameters.get('sortOrder');

  	const data = {
            'part_number': searchItem,
            'orderBy' : parameterOrderBy,
            'sortOrder': parameterSortOrder
    };

    $('#partsearch').val(searchItem);
    const colspan = $('#inventory-table > thead > tr').children().length;
    $('#exportCSV').prop('disabled', true);

    $('#load_chassis').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/chassis_module/get_chassis_specific`, data, function(response) {
            $('#load_chassis').html(response);
            $('#exportCSV').prop('disabled', false);
            paginate();
        });*/

     $('#load_chassis').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
    $("#load_chassis").load(`${baseUrl}chassis_module/getchassis`);

    $("#search_chassis").click(function(){
        $('#load_chassis').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
        var datas = {
         part_number : $("#partsearch").val(),
         sortOrder : $("#sortOrderitem").val(),
         orderBy : $("#orderByitem").val(),
         ajax : '1'
            };
         $.ajax({
         url : `${baseUrl}index.php/chassis_module/get_chassis_specific`,
         type : "post",
         data : datas,
            success : function(msg){                         
                $("#load_chassis").html(msg);
                                                
            }
        });
    });
});