let channels;
let request_actions;

$(document).ready(function() {
    let currentCellNewPrice;
    let currentCellNotes;
    let currentPar;
    let parval;
    let stepval;
    
    const baseUrl = $('#chassisFieldEditScript').data('baseUrl');

    $('#items').click('.new-price', function(event) {
        const newPrice = $(event.target).closest('.new-price');

        if (!newPrice) return;

        const value = newPrice.data('value');

        addEditableField(newPrice, value, 'new-price');
    });

    $('#items').click('.notes', function(event) {
        const notes = $(event.target).closest('.notes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableFieldnote(notes, value, 'notes');
    });


    ///
    $('#items').click('.datetimes', function(event) {
        const notes = $(event.target).closest('.datetimes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'datetimes');
    });
    /////////////partnumber
    $('#load_chassis').click('.par', function(event) {
        if(event.target.className == "par"){
            const notes = $(event.target).closest('.par');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel parbtn"){
            const notes = $(event.target).closest('.par');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldpar(notes, value, 'par');
        }else if(event.target.className == "fa fa-edit parpenbtn"){
            const notes = $(event.target).closest('.par');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldpar(notes, value, 'par');
        }
    });
    function addEditableFieldpar(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.par-editable .par-cancel', function(event) {
        const btnCancel = $(event.target).closest('.par-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.par-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel parbtn"><i class="fa fa-edit parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('par-editable');
        notesCell.addClass('par');
    });
    $('#load_chassis').click('.par-editable .par-apply', function(event) {
        const btnApply = $(event.target).closest('.par-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.par-editable');
        const sku = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.par-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel parbtn"><i class="fa fa-edit parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('par-editable');
        notes.addClass('par');
    
        

        var datas = {
            'data': {'part_number': parval, 'data' :textField.val(), 'type' : "part_number"}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////partnumber


    /////////////mfr
    $('#load_chassis').click('.mfr', function(event) {
        if(event.target.className == "mfr"){
            const notes = $(event.target).closest('.mfr');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel mfrbtn"){
            const notes = $(event.target).closest('.mfr');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmfr(notes, value, 'mfr');
        }else if(event.target.className == "fa fa-edit mfrpenbtn"){
            const notes = $(event.target).closest('.mfr');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmfr(notes, value, 'mfr');
        }
    });
    function addEditableFieldmfr(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        stepval = value;
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.mfr-editable .mfr-cancel', function(event) {
        const btnCancel = $(event.target).closest('.mfr-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.mfr-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel mfrbtn"><i class="fa fa-edit mfrpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('mfr-editable');
        notesCell.addClass('mfr');
    });
    $('#load_chassis').click('.mfr-editable .mfr-apply', function(event) {
        const btnApply = $(event.target).closest('.mfr-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.mfr-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.mfr-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel mfrbtn"><i class="fa fa-edit mfrpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('mfr-editable');
        notes.addClass('mfr');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' :textField.val(), 'type' : "mfr"}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////stepcode


    /////////////u
    $('#load_chassis').click('.u', function(event) {
        if(event.target.className == "u"){
            const notes = $(event.target).closest('.u');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel ubtn"){
            const notes = $(event.target).closest('.u');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldu(notes, value, 'u');
        }else if(event.target.className == "fa fa-edit upenbtn"){
            const notes = $(event.target).closest('.u');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldu(notes, value, 'u');
        }
    });
    function addEditableFieldu(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.u-editable .u-cancel', function(event) {
        const btnCancel = $(event.target).closest('.u-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.u-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel ubtn"><i class="fa fa-edit upenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('u-editable');
        notesCell.addClass('u');
    });
    $('#load_chassis').click('.u-editable .u-apply', function(event) {
        const btnApply = $(event.target).closest('.u-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.u-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.u-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel ubtn"><i class="fa fa-edit upenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('u-editable');
        notes.addClass('u');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : "u"}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                //$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////speed


    /////////////model
    $('#load_chassis').click('.model', function(event) {
        if(event.target.className == "model"){
            const notes = $(event.target).closest('.model');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel modelbtn"){
            const notes = $(event.target).closest('.model');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditablemodel(notes, value, 'model');
        }else if(event.target.className == "fa fa-edit modelpenbtn"){
            const notes = $(event.target).closest('.model');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditablemodel(notes, value, 'model');
        }
    });
    function addEditablemodel(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.model-editable .model-cancel', function(event) {
        const btnCancel = $(event.target).closest('.model-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.model-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel modelbtn"><i class="fa fa-edit modelpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('model-editable');
        notesCell.addClass('model');
    });
    $('#load_chassis').click('.model-editable .model-apply', function(event) {
        const btnApply = $(event.target).closest('.model-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.model-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.model-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel modelbtn"><i class="fa fa-edit modelpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('model-editable');
        notes.addClass('model');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : "model"}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                //$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////number_of_cores


    /////////////ff
    $('#load_chassis').click('.ff', function(event) {
        if(event.target.className == "ff"){
            const notes = $(event.target).closest('.ff');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel ffbtn"){
            const notes = $(event.target).closest('.ff');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldff(notes, value, 'ff');
        }else if(event.target.className == "fa fa-edit ffpenbtn"){
            const notes = $(event.target).closest('.ff');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldff(notes, value, 'ff');
        }
    });
    function addEditableFieldff(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.ff-editable .ff-cancel', function(event) {
        const btnCancel = $(event.target).closest('.ff-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.ff-editable');

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel ffbtn"><i class="fa fa-edit ffpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('ff-editable');
        notesCell.addClass('ff');
    });
    $('#load_chassis').click('.ff-editable .ff-apply', function(event) {
        const btnApply = $(event.target).closest('.ff-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.ff-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.ff-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel ffbtn"><i class="fa fa-edit ffpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('ff-editable');
        notes.addClass('ff');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : "ff"}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                //$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////codename


    /////////////psu
    $('#load_chassis').click('.psu', function(event) {
        if(event.target.className == "psu"){
            const notes = $(event.target).closest('.psu');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel psubtn"){
            const notes = $(event.target).closest('.psu');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldpsu(notes, value, 'psu');
        }else if(event.target.className == "fa fa-edit psupenbtn"){
            const notes = $(event.target).closest('.psu');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldpsu(notes, value, 'psu');
        }
    });
    function addEditableFieldpsu(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.psu-editable .psu-cancel', function(event) {
        const btnCancel = $(event.target).closest('.psu-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.psu-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel psubtn"><i class="fa fa-edit psupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('psu-editable');
        notesCell.addClass('psu');
    });
    $('#load_chassis').click('.psu-editable .psu-apply', function(event) {
        const btnApply = $(event.target).closest('.psu-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.psu-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.psu-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel psubtn"><i class="fa fa-edit psupenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('psu-editable');
        notes.addClass('psu');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' :  textField.val(), 'type' : 'psu'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                //$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////thermal_design_power


    /////////////motherboard
    $('#load_chassis').click('.motherboard', function(event) {
        if(event.target.className == "motherboard"){
            const notes = $(event.target).closest('.motherboard');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel motherboardbtn"){
            const notes = $(event.target).closest('.motherboard');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmotherboard(notes, value, 'motherboard');
        }else if(event.target.className == "fa fa-edit motherboardpenbtn"){
            const notes = $(event.target).closest('.motherboard');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldmotherboard(notes, value, 'motherboard');
        }
    });
    function addEditableFieldmotherboard(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.motherboard-editable .motherboard-cancel', function(event) {
        const btnCancel = $(event.target).closest('.motherboard-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.motherboard-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel motherboardbtn"><i class="fa fa-edit motherboardpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('motherboard-editable');
        notesCell.addClass('motherboard');
    });
    $('#load_chassis').click('.motherboard-editable .motherboard-apply', function(event) {
        const btnApply = $(event.target).closest('.motherboard-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.motherboard-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.motherboard-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel motherboardbtn"><i class="fa fa-edit motherboardpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('motherboard-editable');
        notes.addClass('motherboard');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : "motherboard"}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                //$("#load").html(msg);
                console.group(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////cpu_cache


    /////////////controller
    $('#load_chassis').click('.controller', function(event) {
        if(event.target.className == "controller"){
            const notes = $(event.target).closest('.controller');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel controllerbtn"){
            const notes = $(event.target).closest('.controller');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcontroller(notes, value, 'controller');
        }else if(event.target.className == "fa fa-edit controllerpenbtn"){
            const notes = $(event.target).closest('.controller');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcontroller(notes, value, 'controller');
        }
    });
    function addEditableFieldcontroller(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.controller-editable .controller-cancel', function(event) {
        const btnCancel = $(event.target).closest('.controller-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.controller-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel controllerbtn"><i class="fa fa-edit controllerpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('controller-editable');
        notesCell.addClass('controller');
    });
    $('#load_chassis').click('.controller-editable .controller-apply', function(event) {
        const btnApply = $(event.target).closest('.controller-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.controller-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.controller-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel controllerbtn"><i class="fa fa-edit controllerpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('controller-editable');
        notes.addClass('controller');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'controller'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                //$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////sockets_supported


    /////////////bin
    $('#load_chassis').click('.bin', function(event) {
        if(event.target.className == "bin"){
            const notes = $(event.target).closest('.bin');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel binbtn"){
            const notes = $(event.target).closest('.bin');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbin(notes, value, 'bin');
        }else if(event.target.className == "fa fa-edit binpenbtn"){
            const notes = $(event.target).closest('.bin');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbin(notes, value, 'bin');
        }
    });
    function addEditableFieldbin(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.bin-editable .bin-cancel', function(event) {
        const btnCancel = $(event.target).closest('.bin-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.bin-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel binbtn"><i class="fa fa-edit binpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('bin-editable');
        notesCell.addClass('bin');
    });
    $('#load_chassis').click('.bin-editable .bin-apply', function(event) {
        const btnApply = $(event.target).closest('.bin-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.bin-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.bin-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel binbtn"><i class="fa fa-edit binpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('bin-editable');
        notes.addClass('bin');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'bin'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    /////////////node
    $('#load_chassis').click('.node', function(event) {
        if(event.target.className == "node"){
            const notes = $(event.target).closest('.node');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel nodebtn"){
            const notes = $(event.target).closest('.node');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbin(notes, value, 'node');
        }else if(event.target.className == "fa fa-edit nodepenbtn"){
            const notes = $(event.target).closest('.node');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbin(notes, value, 'node');
        }
    });
    function addEditableFieldbin(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.node-editable .node-cancel', function(event) {
        const btnCancel = $(event.target).closest('.node-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.node-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel nodebtn"><i class="fa fa-edit nodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('node-editable');
        notesCell.addClass('node');
    });
    $('#load_chassis').click('.node-editable .node-apply', function(event) {
        const btnApply = $(event.target).closest('.node-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.node-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.node-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel nodebtn"><i class="fa fa-edit nodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('node-editable');
        notes.addClass('node');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'node'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    /////////////backplane
    $('#load_chassis').click('.backplane', function(event) {
        if(event.target.className == "backplane"){
            const notes = $(event.target).closest('.backplane');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel backplanebtn"){
            const notes = $(event.target).closest('.backplane');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbackplane(notes, value, 'backplane');
        }else if(event.target.className == "fa fa-edit backplanepenbtn"){
            const notes = $(event.target).closest('.backplane');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldbackplane(notes, value, 'backplane');
        }
    });
    function addEditableFieldbackplane(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.backplane-editable .backplane-cancel', function(event) {
        const btnCancel = $(event.target).closest('.backplane-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.backplane-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel backplanebtn"><i class="fa fa-edit backplanepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('backplane-editable');
        notesCell.addClass('backplane');
    });
    $('#load_chassis').click('.backplane-editable .backplane-apply', function(event) {
        const btnApply = $(event.target).closest('.backplane-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.backplane-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.backplane-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel backplanebtn"><i class="fa fa-edit backplanepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('backplane-editable');
        notes.addClass('backplane');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'backplane'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    /////////////drivebays
    $('#load_chassis').click('.drivebays', function(event) {
        if(event.target.className == "drivebays"){
            const notes = $(event.target).closest('.drivebays');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel drivebaysbtn"){
            const notes = $(event.target).closest('.drivebays');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddrivebays(notes, value, 'drivebays');
        }else if(event.target.className == "fa fa-edit drivebayspenbtn"){
            const notes = $(event.target).closest('.drivebays');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddrivebays(notes, value, 'drivebays');
        }
    });
    function addEditableFielddrivebays(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.drivebays-editable .drivebays-cancel', function(event) {
        const btnCancel = $(event.target).closest('.drivebays-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.drivebays-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel drivebaysbtn"><i class="fa fa-edit drivebayspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('drivebays-editable');
        notesCell.addClass('drivebays');
    });
    $('#load_chassis').click('.drivebays-editable .drivebays-apply', function(event) {
        const btnApply = $(event.target).closest('.drivebays-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.drivebays-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.drivebays-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel drivebaysbtn"><i class="fa fa-edit drivebayspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('drivebays-editable');
        notes.addClass('drivebays');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'drivebays'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    /////////////rearbays
    $('#load_chassis').click('.rearbays', function(event) {
        if(event.target.className == "rearbays"){
            const notes = $(event.target).closest('.rearbays');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel rearbaysbtn"){
            const notes = $(event.target).closest('.rearbays');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddrivebays(notes, value, 'rearbays');
        }else if(event.target.className == "fa fa-edit rearbayspenbtn"){
            const notes = $(event.target).closest('.rearbays');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddrivebays(notes, value, 'rearbays');
        }
    });
    function addEditableFielddrivebays(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.rearbays-editable .rearbays-cancel', function(event) {
        const btnCancel = $(event.target).closest('.rearbays-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.rearbays-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rearbaysbtn"><i class="fa fa-edit rearbayspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('rearbays-editable');
        notesCell.addClass('rearbays');
    });
    $('#load_chassis').click('.rearbays-editable .rearbays-apply', function(event) {
        const btnApply = $(event.target).closest('.rearbays-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.rearbays-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.rearbays-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel rearbaysbtn"><i class="fa fa-edit rearbayspenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('rearbays-editable');
        notes.addClass('rearbays');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'rearbays'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    /////////////con
    $('#load_chassis').click('.con', function(event) {
        if(event.target.className == "con"){
            const notes = $(event.target).closest('.con');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel conbtn"){
            const notes = $(event.target).closest('.con');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddrivebays(notes, value, 'con');
        }else if(event.target.className == "fa fa-edit conpenbtn"){
            const notes = $(event.target).closest('.con');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFielddrivebays(notes, value, 'con');
        }
    });
    function addEditableFielddrivebays(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_chassis').click('.con-editable .con-cancel', function(event) {
        const btnCancel = $(event.target).closest('.con-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.con-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('con-editable');
        notesCell.addClass('con');
    });
    $('#load_chassis').click('.con-editable .con-apply', function(event) {
        const btnApply = $(event.target).closest('.con-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.con-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.mfr');
        
    
        const textField = notes.find('.con-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit conpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('con-editable');
        notes.addClass('con');
    
        

        var datas = {
            'data': {'part_number': par.text().trim(), 'data' : textField.val(), 'type' : 'con'}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg);
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    ////////////delete
    $('#load_chassis').click('.delete', function(event) {
        if(event.target.className == "delete"){
            const notes = $(event.target).closest('.delete');
    
            if (!notes) return;
    
            const par = notes.closest('tr').find('.par');
            const stepcode = notes.closest('tr').find('.stepcode');
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel deletebtn"){
            const notes = $(event.target).closest('.delete');
            

            if (!notes) return;
    
            const value = notes.data('value');
            const par = notes.closest('tr').find('.par');
            const stepcode = notes.closest('tr').find('.stepcode');
    
            addEditableFielddelete(notes, stepcode.text().trim(), par.text().trim(), 'delete');
        }else if(event.target.className == "fa fa-trash deletepenbtn"){
            const notes = $(event.target).closest('.delete');
            

            if (!notes) return;
    
            const value = notes.data('value');
            const par = notes.closest('tr').find('.par');
            const stepcode = notes.closest('tr').find('.stepcode');
    
            addEditableFielddelete(notes, stepcode.text().trim(), par.text().trim(), 'delete');
        }
    });
    function addEditableFielddelete(cell, stepcode, par, className) {
        
        ///console.log(stepcode)
        //console.log(par)


        const orderby = $("#orderByitem").val();
        const sortorder = $("#sortOrderitem").val();
        const partnumbertext = $("#partsearch").val();


        
        var datas = {
            'data': {'part_number': par, 'orderby' : orderby, 'sortorder' : sortorder, 'partnumbertext' : partnumbertext}
        };
        $.ajax({
            url : `${baseUrl}index.php/chassis_module/request/delete_chassis`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load_chassis").html(msg)
                //console.log(msg); 
                
                ///console.log(msg);
            }
        });
        /*parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);*/
        
    }
    $('#load_cpu').click('.par-editable .par-cancel', function(event) {
        const btnCancel = $(event.target).closest('.par-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.par-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-danger draftid-cancel parbtn"><i class="fa fa-pen parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('par-editable');
        notesCell.addClass('par');
    });
    $('#load_cpu').click('.par-editable .par-apply', function(event) {
        const btnApply = $(event.target).closest('.par-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.par-editable');
        const sku = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.par-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-danger draftid-cancel parbtn"><i class="fa fa-pen parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('par-editable');
        notes.addClass('par');
    
        

        var datas = {
            'data': {'partnumber': parval, 'stepcode': stepcode.text().trim(), 'data' :textField.val(), 'type' : "part"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////partnumber

});
