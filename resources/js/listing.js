let baseUrl;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {
    baseUrl = $('#listingScript').data('baseUrl');

    let lastId = 0;

    const itemsLimit = 100;
    let itemsRequested = false;

    $(document).scroll(function() {
        const rowCount = $('tbody.product-listing-body > tr').length;
        // if the rows initially returned are less than 100, there are no more rows to load further
        if (lastId === 0 && rowCount < itemsLimit) return;
    
        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('tbody > tr').each(function() {
            maxHeight += $(this).height();
        });


        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            lastId = $('.product-listing-body tr:last').find('.sku').data('id');
            const currentMaxRow = $('.product-listing-body > tr').length;

            const searchData = $('.search-form').serialize();
            const data = `${searchData}&last_id=${lastId}&current_max_row=${currentMaxRow}`;
        
            $.post(`${baseUrl}index.php/listing/load_listing`, data, function(response) {
                $('.product-listing-body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    let timesHeaderClicked = 0;
    let lastHeaderClicked = '';
    let currentHeaderClicked = '';

    $('.listing-table').on('click', '.header-sortable', function() {
        const header = $(this).closest('th');
        const column = header.data('column');
        const table = header.closest('table');
        const otherTable = table.hasClass('main-table') ? $('.sticky-table') : $('.main-table');
        const otherHeader = otherTable.find(`th[data-column="${column}"]`);

        currentHeaderClicked = column;

        if (currentHeaderClicked === lastHeaderClicked) {
            header.find('.fa-sort-up').toggleClass('d-none');
            header.find('.fa-sort-down').toggleClass('d-none');
            otherHeader.find('.fa-sort-up').toggleClass('d-none');
            otherHeader.find('.fa-sort-down').toggleClass('d-none');

            timesHeaderClicked++;
        } else {
            $('.fa-sort-up').removeClass('d-none');
            $('.fa-sort-down').removeClass('d-none');
            $('.fa-sort-up').addClass('d-none');
            $('.fa-sort-down').addClass('d-none');
            header.find('.fa-sort-up').toggleClass('d-none');
            otherHeader.find('.fa-sort-up').toggleClass('d-none');

            timesHeaderClicked = 1;
        }

        const rows = $('.product-listing-body').children().toArray();

        if (currentHeaderClicked === 'index'
            || (currentHeaderClicked === lastHeaderClicked && timesHeaderClicked > 1)) {
            const rowsReversed = rows.reverse();
            $('.product-listing-body').html(rows);
        }

        if (timesHeaderClicked === 1) {
            const rowsSorted = rows.sort(function(firstRow, secondRow) {
                const firstCell = $(firstRow).find(`.${column.replaceAll('_', '-')}`);
                const firstData = firstCell.data('value');
                const secondCell = $(secondRow).find(`.${column.replaceAll('_', '-')}`);
                const secondData = secondCell.data('value');

                if ($.isNumeric(firstData) && $.isNumeric(secondData)) {
                    return Number(firstData) - Number(secondData);
                } else if ($.isNumeric(firstData) && secondData === undefined || secondData === null ||
                           secondData === '') {
                    return -1;
                } else if ($.isNumeric(secondData) && firstData === undefined || firstData === null ||
                           firstData === '') {
                    return 1;
                } else if (firstData === undefined || firstData === null || firstData === '' ||
                           secondData === undefined || secondData === null || secondData === '') {
                    if (firstData === undefined || firstData === null || firstData === '' && secondData.length > 0) {
                        return -1;
                    } else if (firstData.length > 0 && secondData === undefined || secondData === null ||
                        secondData === '') {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    if (firstData.toUpperCase() < secondData.toUpperCase()) {
                        return -1;
                    }

                    if (firstData.toUpperCase() > secondData.toUpperCase()) {
                        return 1;
                    }
                }
                
                return 0;
            });

            header.find('.fa-sort-up').removeClass('d-none');
            $('.product-listing-body').html(rowsSorted);
        }

        if (lastHeaderClicked !== 'index') {
            reindexRows();
        }

        lastHeaderClicked = currentHeaderClicked;
    });

    syncStickyHeader();
    
    $('.main-header-row').children().each(function(index, header) {
        $('.sticky-header-row').children().eq(index).width($(header).width());
    });

    $('.main-subheader-row').children().each(function(index, header) {
        $('.sticky-subheader-row').children().eq(index).width($(header).width());
    });

    $('.switch-restrict-assignee').change(function() { $('.search-form').submit(); });
    $('.user-filter').change(function() { $('.search-form').submit(); });

    $('.btn-clear-channel-filters').on('click', function() {
        $('.channel-filter:checked').prop('checked', false);
        $('.search-form').submit();
    });

    $('.btn-clear-category-filters').on('click', function() {
        $('.category-filter:checked').prop('checked', false);
        $('.search-form').submit();
    });

    $('.product-listing-body').on('click', '.btn-edit', function() {
        const cell = $(this).closest('td');
        
        if (!cell) return;
        
        cell.find('.cell-edit-container').toggleClass('d-none');
        cell.find('.cell-content-container').toggleClass('d-none');
        const rowedt = cell.closest('#roww');
        rowedt[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
        
        
    });

    $('.product-listing-body').on('click', '.btn-cancel', function() {
        const cell = $(this).closest('td');

        if (!cell) return;
        
        cell.find('.cell-edit-container').toggleClass('d-none');
        cell.find('.cell-content-container').toggleClass('d-none');
    });

    $('.product-listing-body').on('click', '.btn-save', function() {
        const btn = $(this).closest('.btn-save');

        if (!btn) {
            $(document).Toasts('create', {
                title: 'Save Error',
                body: 'An unexpected error occurred while attempting to save the data',
                autohide: true,
                delay: 7000
            });
                                
            return;
        }

        const cell = btn.closest('td');

        if (!cell) {
            $(document).Toasts('create', {
                title: 'Save Error',
                body: 'An unexpected error occurred while attempting to save the data',
                autohide: true,
                delay: 7000
            });
                                
            return;
        }

        const id = btn.closest('tr').find('.sku').data('id');
        const column = cell.prop('class').replaceAll('-', '_');
        const value = cell.find('.cell-edit').val();

        const data = {
            'id': id,
            'column': column,
            'value': value,
        };

    
        $.post(`${baseUrl}index.php/listing/save_column`, data, function(response) {

            
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Save failed',
                    body: `Failed to save ${column} ${value}`,
                    autohide: true,
                    delay: 7000
                });
                
                
                return;    
            }

            $(document).Toasts('create', {
                title: 'Save success',
                body: `Successfully saved ${column} ${value}`,
                autohide: true,
                delay: 7000
                
            });
            
            var datas = {
                'price_low_last_edited': response.price_low_last_edited,
                'price_low_last_edited_by' : response.price_low_last_edited_by
            };

            cell.data('value', value);

            if (column === 'link') {
                const link = cell.find('.cell-value').find('a');
                link.prop('href', value);
                link.html(`${value}`);
            } else if (column === 'price_research_market_low'){
                cell.find('.cell-value').html(`\$${value}`);

            } else if  (column === 'price_research_market_high'){
                cell.find('.cell-value').html(`\$${value}`);

            } else if  (column === 'note'){
                cell.find('.cell-value').html(`${value}`);

            } else if  (column === 'channel'){
                cell.find('.cell-value').html(`${value}`);
            } else if  (column === 'comp_link'){
                cell.find('.cell-value').html(`${value}`);
            } else {
                cell.find('.cell-value').html(value);
            }

            cell.find('.cell-edit-container').toggleClass('d-none');
            cell.find('.cell-content-container').toggleClass('d-none');

        const row = cell.closest('#roww');
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
        row.css('background-color', '#f9f9b1');
        
        });
        setTimeout(function() {
            loadListing();
        }, 3000);
        
    });

    let listingToMarkDone = -1;

    $('.product-listing-body').on('click', '.btn-done', function(event) {
        const button = $(this).closest('.btn-done');

        if (!button) {
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing done.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        rowToRemove = button.closest('tr');
        const sku = rowToRemove.find('.sku').data('value');
        $('.sku-done').html(sku);

        if (!rowToRemove) {
            event.preventDefault();
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing done.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        listingToMarkDone = rowToRemove.find('.sku').data('id');
    });

    $('.btn-confirm-done').on('click', function() {
        $('#markListingDoneModal').modal('hide');
        const data = { 'id': listingToMarkDone };

        $.post(`${baseUrl}index.php/listing/mark_done`, data, function(response) {
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Mark Done failed',
                    body: 'Failed to mark listing done.',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Mark Done success',
                body: 'Successfully marked listing done.',
                autohide: true,
                delay: 7000
            });
            loadListing();
        });
    });

    $('.product-listing-body').on('click', '.btn-undone', function(event) {
        const button = $(this).closest('.btn-undone');
    
        if (!button) {
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing undone.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        rowToRemove = button.closest('tr');
        const sku = rowToRemove.find('.sku').data('value');
    
        $('.sku-done').html(sku);

        if (!rowToRemove) {
            event.preventDefault();
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing undone.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        listingToMarkDone = rowToRemove.find('.sku').data('id');
    });

    $('.btn-confirm-undone').on('click', function() {
        $('#markListingUndoneModal').modal('hide');
        const data = { 'id': listingToMarkDone };
    
        $.post(`${baseUrl}index.php/listing/mark_undone`, data, function(response) {
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Mark Undone failed',
                    body: 'Failed to mark listing undone.',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Mark undone success',
                body: 'Successfully marked listing undone.',
                autohide: true,
                delay: 7000
            });
            loadListing();
        });
    });

    $('#createListingForm').submit(function(event) {
        event.preventDefault();

        const data = $(this).serialize();

        $.post(`${baseUrl}index.php/listing/create_listing`, data, function(response) {
            if (response === 'exists') {
                $(document).Toasts('create', {
                    title: 'SKU Exists',
                    autohide: true,
                    delay: 7000
                });

                return;
            }
            
            $('.create-listing-modal').modal('hide');

            if (response === '') {
                $(document).Toasts('create', {
                    title: 'Failed to create listing',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $('#createListingForm')[0].reset();
            $('.product-listing-body').prepend(response);

            reindexRows();

            $(document).Toasts('create', {
                title: 'Successfully created listing',
                autohide: true,
                delay: 7000
            });
        });
    });

    $('#exportCSV').click(function() {
		var data = {
			'filter_sku' : $('#filter_sku').val(),
			ajax : '1'
		};

        const  url = `${baseUrl}index.php/listing/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `ProductList_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

    function reindexRows() {
        $('tbody > tr').each(function(index, row) {
            $(row).find('th').html(index + 1);
        });
    }


    $('.filters').select2();
    $('#filterUsers').prop('enable', true);
    $('#filterUsers').on('change', function() { loadListing(); });
    

    $('#doneSwitch').on('change', function() { 
        if ($('#doneSwitch').prop('checked')){
            loadListing();
            // $('.search-form').submit();
        }else{
            loadListing();
        }        
    });
        

    function loadListing() {
        if ($('#doneSwitch').prop('checked')){
        const filterUsers = $('#filterUsers').val();
        var done_filter = $('#doneSwitch').val();
        var data = {
            'users': filterUsers, 
            'done_filter': done_filter, 
        };
    }else{
        const filterUsers = $('#filterUsers').val();
        var data = {
            'users': filterUsers, 
        };
    }
        $.post(`${baseUrl}index.php/listing/load_listing`, data, function(response) {
            $('.product-listing-body').html(response);
            itemsRequested = false;
            syncStickyHeader();
        });
    }
    
});

function syncStickyHeader() {
    const tableWidth = $('.main-table').width();
    $('.scroller-content').width(tableWidth);
    $('.sticky-table').width(tableWidth);
    
    $('.main-header-row').children().each(function(index, header) {
        $('.sticky-header-row').children().eq(index).width($(header).width());
    });

}








