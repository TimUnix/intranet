let baseUrl;

$(document).ready(function() {
    let currentCellNewPrice;
    let currentCellNotes;
    let currentPar;
    
    baseUrl = $('#priceUpdateScript').data('baseUrl');

    $('#filter-select').change(function() {
        showSpinner();

        $.post(`${baseUrl}index.php/price_update/request/apply_filter/`, { 'filter': $(this).val() }, drawTable);
    });

    $('#items').click('.new-price', function(event) {
        const newPrice = $(event.target).closest('.new-price');

        if (!newPrice) return;

        const value = newPrice.data('value');

        addEditableField(newPrice, value, 'new-price');
    });

    $('#items').click('.notes', function(event) {
        const notes = $(event.target).closest('.notes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'notes');
    });

    $('#items').click('.new-price-editable .new-price-apply', function(event) {
        const btnApply = $(event.target).closest('.new-price-apply');
        if (!btnApply) return;

        const newPrice = btnApply.closest('.new-price-editable');
        const par = btnApply.closest('tr').find('.par');

        const textField = newPrice.find('.new-price-input');
        if (!textField) return;
        if (!textField.val()) return;

        if (textField.val() && textField.val().match(/[^$,.\d]/)) return;

        const formattedPrice = formatPrice(textField.val());
        newPrice.html(formattedPrice);
        newPrice.data('value', textField.val());
        newPrice.removeClass('new-price-editable');
        newPrice.addClass('new-price');

        if ((par.text() && par.text().length > 0) && (textField.val() && textField.val().length > 0)) {
            const data = { 'par': par.text(), 'price': textField.val() };
           $.post(`${baseUrl}index.php/price_update/request/save_new_price`, data);
        }
    });

    $('#items').click('.new-price-editable .new-price-cancel', function(event) {
        const btnCancel = $(event.target).closest('.new-price-cancel');
        if (!btnCancel) return;

        const newPriceCell = btnCancel.closest('.new-price-editable');

        const formattedPrice = formatPrice(newPriceCell.data('value'));
        newPriceCell.html(formattedPrice);
        newPriceCell.removeClass('new-price-editable');
        newPriceCell.addClass('new-price');
    });

    $('#items').click('.notes-editable .notes-apply', function(event) {
        const btnApply = $(event.target).closest('.notes-apply');
        if (!btnApply) return;

        const notes = btnApply.closest('.notes-editable');
        const par = btnApply.closest('tr').find('.par');

        const textField = notes.find('.notes-input');
        if (!textField) return;
        if (!textField.val()) return;

        notes.html(textField.val());
        notes.data('value', textField.val());
        notes.removeClass('notes-editable');
        notes.addClass('notes');

        if ((par.text() && par.text().length > 0) && (textField.val() && textField.val().length > 0)) {
            const data = { 'par': par.text(), 'notes': textField.val() };

            $.post(`${baseUrl}index.php/price_update/request/save_new_price_notes`, data);
        }
    });

    $('#items').click('.notes-editable .notes-cancel', function(event) {
        const btnCancel = $(event.target).closest('.notes-cancel');
        if (!btnCancel) return;

        const notesCell = btnCancel.closest('.notes-editable');

        notesCell.html(notesCell.data('value'));
        notesCell.removeClass('notes-editable');
        notesCell.addClass('notes');
    });

    function formatPrice(price) {
        const numberFormatOptions = {                                                 
            style: 'currency', currency: 'USD', minimumFractionDigits: 2, maximumFractionDigits: 2
        };
        
        const numberFormat = new Intl.NumberFormat('en-US', numberFormatOptions);

        return numberFormat.format(price);
    }

    function addEditableField(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
    }

    function showSpinner() {
        $('#items').html(
            `<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`
        );
    }

    function drawTable(response) {
        $('#items').html(response);
    }
});
