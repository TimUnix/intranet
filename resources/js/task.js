const errorSkuEmpty = 0;
const errorPriceEmpty = 1;
const errorSkuExists = 2;
const exit_sku_exists_and_listing_id = "SKU already done and listed to ebay";
const sku_and_task_request_already_exist = "Sku and Task Request Already Exist";

$(document).ready(function(){
    let bannervalue;
    baseUrl = $('#listingScript').data('baseUrl');


    syncStickyHeader();
    
    $('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);

    $.get(`${baseUrl}index.php/task/load_filters`, function(response) {
        $('.filters-container').html(response);
    }).then(function() {
        $('.filters').select2();

        loadListing();
    });

	$("#submit").click(function(event) {
        if($("#request").val() == "9"){
            event.preventDefault();
            
            if($("#bannerone").is(":checked")){
                bannervalue = "YES";

            }else if($("#bannertwo").is(":checked")){
                bannervalue = "NO";
            }

            let promotion = '0';

            if ($('#promotion').val() != '') {
                promotion = $('#promotion').val();
            }

            let volumeDiscount2x = '0';
            let volumeDiscount3x = '0';
            let volumeDiscount4x = '0';

            if ($('#volumeDiscount2x').val().trim() != '') {
                volumeDiscount2x = $('#volumeDiscount2x').val();
            }

            if ($('#volumeDiscount3x').val().trim() != '') {
                volumeDiscount3x = $('#volumeDiscount3x').val();
            }

            if ($('#volumeDiscount4x').val().trim() != '') {
                volumeDiscount4x = $('#volumeDiscount4x').val();
            }
            var datas = {
                'sku' : $("#sku").val(),
                'shipping': $("#shipping").val(),
                'weight': $("#weight").val(),
                'price': $("#price").val(),
                'promotion': promotion,
                'volume_discount_2x': volumeDiscount2x,
                'volume_discount_3x': volumeDiscount3x,
                'volume_discount_4x': volumeDiscount4x,
                'lot': $('#lot').val(),
                'moved': $('#moved').val(),
                'banner' : bannervalue,
                'notes' : $("#notes").val(),
                'request': $("#request").val(),
                ajax : '1'
            };
            
            $.ajax({
                url : `${baseUrl}index.php/task/request/create_listing`,
                type : "post",
                data : datas,
                success : function(response){
                    
                    const refreshUrl = window.location.href.split('?')[0];
                    window.location.href = refreshUrl;
                }
            });
        }else{

            
            if($("#sku").val() == ""){
                $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
                $('#sku').focus();
            } else if (($('#request').val() == 1 || $('#request').val() == 3) && $('#price').val() === '') {
                $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);
                $('#price').focus();
            } else {
                event.preventDefault();
                
                if($("#bannerone").is(":checked")){
                    bannervalue = "YES";

                }else if($("#bannertwo").is(":checked")){
                    bannervalue = "NO";
                }

                let promotion = '0';

                if ($('#promotion').val() != '') {
                    promotion = $('#promotion').val();
                }

                let volumeDiscount2x = '0';
                let volumeDiscount3x = '0';
                let volumeDiscount4x = '0';

                if ($('#volumeDiscount2x').val().trim() != '') {
                    volumeDiscount2x = $('#volumeDiscount2x').val();
                }

                if ($('#volumeDiscount3x').val().trim() != '') {
                    volumeDiscount3x = $('#volumeDiscount3x').val();
                }

                if ($('#volumeDiscount4x').val().trim() != '') {
                    volumeDiscount4x = $('#volumeDiscount4x').val();
                }

                var datas = {
                    'sku' : $("#sku").val(),
                    'shipping': $("#shipping").val(),
                    'weight': $("#weight").val(),
                    'price': $("#price").val(),
                    'promotion': promotion,
                    'volume_discount_2x': volumeDiscount2x,
                    'volume_discount_3x': volumeDiscount3x,
                    'volume_discount_4x': volumeDiscount4x,
                    'lot': $('#lot').val(),
                    'moved': $('#moved').val(),
                    'banner' : bannervalue,
                    'notes' : $("#notes").val(),
                    'request': $("#request").val(),
                    ajax : '1'
                };
                $.ajax({
                    url : `${baseUrl}index.php/task/request/create_listing`,
                    type : "post",
                    data : datas,
                    success : function(response){

                        if (response == errorSkuEmpty) {
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Sku and Task Request Already Exist!</h5>
                    </div>`);

                            $('#sku').focus();
                            $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                        } else if (response == errorPriceEmpty) {
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);

                            $('#price').focus();
                            $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                        } else if (response == errorSkuExists) {
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>SKU and Task already exist</h5>
                    </div>`);
                        } else if (response == exit_sku_exists_and_listing_id) {
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>SKU already done and listed to ebay</h5>
                    </div>`);
                        } else if (response == sku_and_task_request_already_exist) {
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>SKU and Task already exist</h5>
                    </div>`);
                        } else {
                            const refreshUrl = window.location.href.split('?')[0];
                            window.location.href = refreshUrl;
                        }
                    }
                });
            }
        }
    });


    ///
    $(document).on('scroll', function() {
        const rowCount = $('tbody > tr').length;

        // if the rows initially returned are less than 100, there are no more rows to load further

        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('tbody > tr').each(function() {
            maxHeight += $(this).height();
        }, function(){
            const orderBy = $('#orderBy').val();
            const sortOrder = $('#sortOrder').val();
            const searchColumn = $('#searchColumn').val();
            const filterActions = $('#filterActions').select2('data').map(function(action) {
                return action.element.value;
            }).join(',');
            const filterBanner = $('#filterBanner').val();
            const filterPhoto = $('#filterPhoto').val();
            const filterUsers = $('#filterUsers').select2('data').map(function(user) {
                return user.element.value;
            }).join(',');
            const filterCategories = $('#filterCategories').select2('data').map(function(category) {
                return category.element.value;
            });

            var data = {
                'itemsearch' : $("#itemsearch").val(),
                'order_by': orderBy,
                'sort_order': sortOrder,
                'actions': filterActions,
                'banner': filterBanner,
                'photo': filterPhoto,
                'users': filterUsers,
                'categories': filterCategories,
                'search_column': searchColumn,
                'is_done_shown': $('#doneSwitch').prop('checked'),
                'is_archived_shown': $('#archiveSwitch').prop('checked'),
                'is_only_me_enabled': $('#onlyMeSwitch').prop('checked')
            };

            const url = `${baseUrl}index.php/task/load_listing`;
            const countUrl = `${baseUrl}index.php/task/load_listing_count`;

            $.post(url, data, function(response) {
                $('#items').html(response);
                const tableWidth = $('#items').width();
                $('.scroller-content').width(tableWidth);
                syncStickyHeader();
            }).then(function() {
                
                const tableWidth = $('#items').width();
                $('.scroller-content').width(tableWidth);
                syncStickyHeader();
            });

            $.post(countUrl, data, function(response, _, xhr) {
                if (xhr.status === 200) {
                    $('.task-count').html(response);
                }
            });
        });

        

            
        return
            

            
        
    });
    ///

	$('#doneSwitch').change(function(){
        $('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);
        
        loadListing();
    });

    $('#archiveSwitch').change(function() {
        $('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);

        loadListing();
    });

    $('#listingsearch').click(function(){
		loadListing();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();
        loadListing();
    });

    $('#searchColumn').change(function() {
        if ($(this).val() !== 'sku') {
            $('#searchSuggestions').html('');
        }
    });

    $('#sku').on('input', function(event) {
        const search = $(this).val();
        
        if (search.trim() !== '') {
            const data = { 'search': search };
            $.get(`${baseUrl}index.php/task/search_sku_suggestions`, data, function(response) {
                if (response.data.status === 200) {
                    $('#skuSuggestions').html('');
                    response.data.suggestions.forEach(function(suggestion) {
                        const sku = suggestion.par.replaceAll('-', '');
                        $('#skuSuggestions').append(`<option>${sku}</option>`);
                    });
                }
            });
        }
    });

    $('#itemsearch').on('input', function(event) {
        const search = $(this).val();
        const searchColumn = $('#searchColumn').val();
        
        if (searchColumn  === 'sku' && search.trim() !== '') {
            const data = { 'search': search };
            $.get(`${baseUrl}index.php/task/search_sku_suggestions`, data, function(response) {
                if (response.data.status === 200) {
                    $('#searchSuggestions').html('');
                    response.data.suggestions.forEach(function(suggestion) {
                        const sku = suggestion.par.replaceAll('-', '');
                        $('#searchSuggestions').append(`<option>${sku}</option>`);
                    });
                }
            });
        }
    });

    $('#exportCSV').click(function() {
        const orderBy = $('#orderBy').val();
        const sortOrder = $('#sortOrder').val();
        const filterActions = $('#filterActions').select2('data').map(function(action) {
            return action.element.value;
        }).join(',');
        const filterBanner = $('#filterBanner').find(':selected').val();

        var data = {
            'itemsearch': $("#itemsearch").val(),
            'order_by': orderBy,
            'sort_order': sortOrder,
            'actions': filterActions,
            'banner': filterBanner,
            'is_done_shown': $('#doneSwitch').prop('checked'),
            'is_only_me_enabled': $('#onlyMeSwitch').prop('checked')
        };

        const url = `${baseUrl}index.php/task/export_csv`;

        $.post(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                var filename = `listing_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
            }
        });
    });

    $('#onlyMeSwitch').change(function() {
        if ($(this).prop('checked')) {
            $('#filterUsers').val('').trigger('change');
            $('#filterUsers').prop('disabled', true);
            $('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);
            loadListing();
        } else {
            $('#filterUsers').prop('disabled', false);
            $('#items').html(`<tr class="odd"><td valign="top" colspan="100%">${spinner}</td></tr>`);
            loadListing();
        }
    });

    $('#filterUsers').on('change', function() { loadListing(); });
    
    $('#clearFilters').click(function() {
        $('.filters').select2().val('').trigger('change');
        $('#filterBanner').find(':selected').prop('selected', false);
        $('#filterPhoto').find(':selected').prop('selected', false);
        $('.option-none').prop('selected', true);

        loadListing();
    });
    
    if (request_actions === null || request_actions === undefined) {
        $.get(`${baseUrl}index.php/task/get_actions`, function(response) {
            request_actions = response;
        });
    }

    $('#orderBy').change(function() {
        loadListing();
    });

    $('#sortOrder').change(function() {
        loadListing();
    });

    $('#applyFilters').click(function() {
        loadListing();
    });

    $('#request').change(function() {
        if ($(this).find(':selected').text().trim().toLowerCase() == 'change ebay channel') {
            $('#moved-form').removeClass('d-none');
        } else {
            $('#moved-form').addClass('d-none');
        }
    });
    
    const params = new URLSearchParams(window.location.search);
    const referred = params.get('referred');

    const isNewListingRequested = referred !== null && referred == 1;

    if (isNewListingRequested) {
        $('#myModal').modal('show');
    }

    $('#myModal').on('hidden.bs.modal', function() {
        clearModal();
    });

    function clearModal() {
        $('#success').html('');
        $('#sku').val('');
        $('#shipping').val('Calculated');
        $('#price').val('');
        $('#notes').val('');
        $('#lot').val('1');
    }
});

function loadListing() {
    const orderBy = $('#orderBy').val();
    const sortOrder = $('#sortOrder').val();
    const searchColumn = $('#searchColumn').val();
    const filterActions = $('#filterActions').select2('data').map(function(action) {
        return action.element.value;
    }).join(',');
    const filterBanner = $('#filterBanner').val();
    const filterPhoto = $('#filterPhoto').val();
    const filterUsers = $('#filterUsers').select2('data').map(function(user) {
        return user.element.value;
    }).join(',');
    const filterCategories = $('#filterCategories').select2('data').map(function(category) {
        return category.element.value;
    });

    var data = {
        'itemsearch' : $("#itemsearch").val(),
        'order_by': orderBy,
        'sort_order': sortOrder,
        'actions': filterActions,
        'banner': filterBanner,
        'photo': filterPhoto,
        'users': filterUsers,
        'categories': filterCategories,
        'search_column': searchColumn,
        'is_done_shown': $('#doneSwitch').prop('checked'),
        'is_archived_shown': $('#archiveSwitch').prop('checked'),
        'is_only_me_enabled': $('#onlyMeSwitch').prop('checked')
    };

    const url = `${baseUrl}index.php/task/load_listing`;
    const countUrl = `${baseUrl}index.php/task/load_listing_count`;

    $.post(url, data, function(response) {
        $('#items').html(response);
        const tableWidth = $('#items').width();
        $('.scroller-content').width(tableWidth);
        syncStickyHeader();
    }).then(function() {
        
        const tableWidth = $('#items').width();
        $('.scroller-content').width(tableWidth);
        syncStickyHeader();
    });

    $.post(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
        }
    });
}


function syncStickyHeader() {
    const tableWidth = $('.main-table').width();
    $('.scroller-content').width(tableWidth);
    $('.sticky-table').width(tableWidth);
    
    $('.main-header-row').children().each(function(index, header) {
        $('.sticky-header-row').children().eq(index).width($(header).width());
    });

    
}

