const errorSkuEmpty = 0;
const errorPriceEmpty = 1;
const exit_sku_exists = 2;
const sku_success_register = 'SKU Successfully Registered';
const sku_already_register = 'SKU already done and listed to ebay';
const sku_and_task_request_already_exist = "Sku and Task Request Already Exist";

let baseUrls;

$(document).ready(function(){
    let bannervalue;
    baseUrls = $('#listingScript').data('baseUrl');
    
	$("#submit").click(function(event){
        if($("#sku").val() == ""){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku').focus();
        } else if (($('#request').val() == 1 || $('#request').val() == 3) && $('#price').val() === '') {
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);
            $('#price').focus();
        } else {
            event.preventDefault();
            
            if($("#bannerone").is(":checked")){
                bannervalue = "YES";

            }else if($("#bannertwo").is(":checked")){
                bannervalue = "NO";
            }

            $('#items').html(`<tr class="odd"><td valign="top" colspan="100%" class="datatables_empty">${spinner}</td></tr>`);

            var datas = {
                'sku' : $("#sku").val(),
                'shipping': $("#shipping").val(),
                'weight': $("#weight").val(),
                'price': $("#price").val(),
                'promotion': $('#promotion').val(),
                'volume_discount_2x': $('#volumeDiscount2x').val(),
                'volume_discount_3x': $('#volumeDiscount3x').val(),
                'volume_discount_4x': $('#volumeDiscount4x').val(),
                'lot': $('#lot').val(),
                'moved': $('#moved').val(),
                'banner' : bannervalue,
                'notes' : $("#notes").val(),
                'request': $("#request").val(),
                ajax : '1'
            };
            console.log(datas);
          
            $.ajax({
                url : `${baseUrls}index.php/task/request/create_listing`,
                type : "post",
                data : datas,
                success : function(response){

                    if (response == errorSkuEmpty) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Sku and Task Request Already Exist!</h5>
                    </div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else if (response == errorPriceEmpty) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input Price</h5>
                    </div>`);

                        $('#price').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else if (response == exit_sku_exists) {
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Sku and Task Request Already Exist!</h5>
                    </div>`);

                        $('#sku').focus();
                        $('#request').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else if(response == sku_success_register){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Sku and Task Request Already Exist!</h5>
                    </div>`);

                        $('#sku').focus();
                        $('#request').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else if(response == sku_already_register){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>SKU already done and listed to ebay</h5>
                    </div>`);

                        $('#sku').focus();
                        $('#request').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else if(response == sku_and_task_request_already_exist){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Sku and Task Request Already Exist</h5>
                    </div>`);

                        $('#sku').focus();
                        $('#request').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    } else {
                        $('#myModal').modal('hide');

                        const sku = $('#sku').val();
                        const row = document.querySelector(`td[data-value="${sku}"]`).closest('tr');
                        
                        row.scrollIntoView({
                            behavior: 'auto',
                            block: 'center',
                            inline: 'center'
                        });
                        const backgroundColor = row.style.backgroundColor;
   
                        setTimeout(function() {
                            row.style.backgroundColor = '#f9f9b1';
                        }, 1000);

                        setTimeout(function() {
                            row.style.backgroundColor = backgroundColor;
                        }, 900000);

                        $(document).Toasts('create', {
                            title: 'Task successfully saved',
                            body: `Task successfully created for ${sku}`,
                            autohide: true,
                            delay: 5000
                        });
                    }
                }
            });
        }
    });
});