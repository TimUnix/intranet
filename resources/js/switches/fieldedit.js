
let newval
function addEditableField(cell, value, className) {
    
    const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
    newval = value;
    const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
    btnAccept.html(
        '<i class="fa fa-check" aria-hidden="true"></i>'
    );

    const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
    btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

    $(cell).html(input).append(btnCancel).append(btnAccept);
    $(cell).removeClass(className);
    $(cell).addClass(`${className}-editable`);
}

function updatecol(){
    
}

$('#switches_table_body').on('click','.sku', function(event) {
    
    if(event.target.className == "sku"){
        const notes = $(event.target).closest('.sku');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel skubtn"){
        const notes = $(event.target).closest('.sku');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'sku');
    }else if(event.target.className == "fa fa-edit fa-sm skupenbtn"){
        const notes = $(event.target).closest('.sku');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'sku');
    }
});

$('#switches_table_body').click('.sku-editable .sku-cancel', function(event) {
    const btnCancel = $(event.target).closest('.sku-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.sku-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('sku-editable');
    notesCell.addClass('sku');
});

$('#switches_table_body').click('.sku-editable .sku-apply', function(event) {
    
    const btnApply = $(event.target).closest('.sku-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.sku-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.sku-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit fa-sm skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('sku-editable');
    notes.addClass('sku');

    

    var datas = {
        'data': {
            'sku': newval, 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "sku"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});


$('#switches_table_body').on('click','.qtybtn', function(event) {
    if(event.target.className == "qty"){
        const notes = $(event.target).closest('.qty');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel qtybtn"){
        const notes = $(event.target).closest('.qty');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'qty');
    }else if(event.target.className == "fa fa-edit fa-sm qtypenbtn"){
        const notes = $(event.target).closest('.qty');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'qty');
    }
});

$('#switches_table_body').click('.qty-editable .qty-cancel', function(event) {
    const btnCancel = $(event.target).closest('.qty-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.qty-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qtybtn"><i class="fa fa-edit qtybtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('qty-editable');
    notesCell.addClass('qty');
});

$('#switches_table_body').click('.qty-editable .qty-apply', function(event) {
    
    const btnApply = $(event.target).closest('.qty-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    

    const notes = btnApply.closest('.qty-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.qty-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel qtybtn"><i class="fa fa-edit fa-sm qtypenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('qty-editable');
    notes.addClass('qty');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "qty"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//
$('#switches_table_body').on('click','.switchmodelbtn', function(event) {
    if(event.target.className == "switchmodel"){
        const notes = $(event.target).closest('.switchmodel');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel switchmodelbtn"){
        const notes = $(event.target).closest('.switchmodel');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'switchmodel');
    }else if(event.target.className == "fa fa-edit fa-sm switchmodelpenbtn"){
        const notes = $(event.target).closest('.switchmodel');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'switchmodel');
    }
});

$('#switches_table_body').click('.switchmodel-editable .switchmodel-cancel', function(event) {
    const btnCancel = $(event.target).closest('.switchmodel-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.switchmodel-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel switchmodelbtn"><i class="fa fa-edit switchmodelpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('switchmodel-editable');
    notesCell.addClass('switchmodel');
});

$('#switches_table_body').click('.switchmodel-editable .switchmodel-apply', function(event) {

    const btnApply = $(event.target).closest('.switchmodel-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    
    const notes = btnApply.closest('.switchmodel-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.switchmodel-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel switchmodelbtn"><i class="fa fa-edit fa-sm switchmodelpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('switchmodel-editable');
    notes.addClass('switchmodel');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': newval,
            'data' :textField.val(), 
            'type' : "switchmodel"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.conbtn', function(event) {
    if(event.target.className == "con"){
        const notes = $(event.target).closest('.con');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel conbtn"){
        const notes = $(event.target).closest('.con');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'con');
    }else if(event.target.className == "fa fa-edit fa-sm conpenbtn"){
        const notes = $(event.target).closest('.con');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'con');
    }
});

$('#switches_table_body').click('.con-editable .con-cancel', function(event) {
    const btnCancel = $(event.target).closest('.con-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.con-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('con-editable');
    notesCell.addClass('con');
});

$('#switches_table_body').click('.con-editable .con-apply', function(event) {
    
    const btnApply = $(event.target).closest('.con-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }


    const notes = btnApply.closest('.con-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.con-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel conbtn"><i class="fa fa-edit fa-sm conpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('con-editable');
    notes.addClass('con');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "con"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.oemmanbtn', function(event) {
    if(event.target.className == "oemman"){
        const notes = $(event.target).closest('.oemman');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel oemmanbtn"){
        const notes = $(event.target).closest('.oemman');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'oemman');
    }else if(event.target.className == "fa fa-edit fa-sm oemmanpenbtn"){
        const notes = $(event.target).closest('.oemman');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'oemman');
    }
});

$('#switches_table_body').click('.oemman-editable .oemman-cancel', function(event) {
    const btnCancel = $(event.target).closest('.oemman-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.oemman-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('oemman-editable');
    notesCell.addClass('oemman');
});

$('#switches_table_body').click('.oemman-editable .oemman-apply', function(event) {
    
    const btnApply = $(event.target).closest('.oemman-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.oemman-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.oemman-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel oemmanbtn"><i class="fa fa-edit fa-sm oemmanpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('oemman-editable');
    notes.addClass('oemman');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "oemman"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.portbtn', function(event) {
    if(event.target.className == "port"){
        const notes = $(event.target).closest('.port');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel portbtn"){
        const notes = $(event.target).closest('.port');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'port');
    }else if(event.target.className == "fa fa-edit fa-sm portpenbtn"){
        const notes = $(event.target).closest('.port');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'port');
    }
});

$('#switches_table_body').click('.port-editable .port-cancel', function(event) {
    const btnCancel = $(event.target).closest('.port-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.port-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('port-editable');
    notesCell.addClass('port');
});

$('#switches_table_body').click('.port-editable .port-apply', function(event) {
   
    const btnApply = $(event.target).closest('.port-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.port-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.port-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel portbtn"><i class="fa fa-edit fa-sm portpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('port-editable');
    notes.addClass('port');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "port"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.licensebtn', function(event) {
    if(event.target.className == "license"){
        const notes = $(event.target).closest('.license');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel licensebtn"){
        const notes = $(event.target).closest('.license');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'license');
    }else if(event.target.className == "fa fa-edit fa-sm licensepenbtn"){
        const notes = $(event.target).closest('.license');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'license');
    }
});

$('#switches_table_body').click('.license-editable .license-cancel', function(event) {
    const btnCancel = $(event.target).closest('.license-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.license-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('license-editable');
    notesCell.addClass('license');
});

$('#switches_table_body').click('.license-editable .license-apply', function(event) {
    
    const btnApply = $(event.target).closest('.license-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.license-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.license-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel licensebtn"><i class="fa fa-edit fa-sm licensepenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('license-editable');
    notes.addClass('license');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "license"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.featuresbtn', function(event) {
    if(event.target.className == "features"){
        const notes = $(event.target).closest('.features');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel featuresbtn"){
        const notes = $(event.target).closest('.features');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'features');
    }else if(event.target.className == "fa fa-edit fa-sm featurespenbtn"){
        const notes = $(event.target).closest('.features');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'features');
    }
});

$('#switches_table_body').click('.features-editable .features-cancel', function(event) {
    const btnCancel = $(event.target).closest('.features-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.features-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('features-editable');
    notesCell.addClass('features');
});

$('#switches_table_body').click('.features-editable .features-apply', function(event) {
    
    const btnApply = $(event.target).closest('.features-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.features-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.features-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel featuresbtn"><i class="fa fa-edit fa-sm featurespenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('features-editable');
    notes.addClass('features');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "features"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.dualsinglebtn', function(event) {
    if(event.target.className == "dualsingle"){
        const notes = $(event.target).closest('.dualsingle');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel dualsinglebtn"){
        const notes = $(event.target).closest('.dualsingle');

        if (!notes) return;

        const value = notes.data('value');

        addEditableFieldsku(notes, value, 'dualsingle');
    }else if(event.target.className == "fa fa-edit fa-sm dualsinglepenbtn"){
        const notes = $(event.target).closest('.dualsingle');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'dualsingle');
    }
});

$('#switches_table_body').click('.dualsingle-editable .dualsingle-cancel', function(event) {
    const btnCancel = $(event.target).closest('.dualsingle-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.dualsingle-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('dualsingle-editable');
    notesCell.addClass('dualsingle');
});

$('#switches_table_body').click('.dualsingle-editable .dualsingle-apply', function(event) {
    
    const btnApply = $(event.target).closest('.dualsingle-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.dualsingle-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.dualsingle-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel dualsinglebtn"><i class="fa fa-edit fa-sm dualsinglepenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('dualsingle-editable');
    notes.addClass('dualsingle');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "dualsingle"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.airflowbtn', function(event) {
    if(event.target.className == "airflow"){
        const notes = $(event.target).closest('.airflow');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel airflowbtn"){
        const notes = $(event.target).closest('.airflow');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'airflow');
    }else if(event.target.className == "fa fa-edit fa-sm airflowpenbtn"){
        const notes = $(event.target).closest('.airflow');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'airflow');
    }
});

$('#switches_table_body').click('.airflow-editable .airflow-cancel', function(event) {
    const btnCancel = $(event.target).closest('.airflow-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.airflow-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('airflow-editable');
    notesCell.addClass('airflow');
});

$('#switches_table_body').click('.airflow-editable .airflow-apply', function(event) {
   
    const btnApply = $(event.target).closest('.airflow-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }
    const notes = btnApply.closest('.airflow-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.airflow-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel airflowbtn"><i class="fa fa-edit fa-sm airflowpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('airflow-editable');
    notes.addClass('airflow');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "airflow"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});

//

$('#switches_table_body').on('click','.noedrbtn', function(event) {
    if(event.target.className == "noedr"){
        const notes = $(event.target).closest('.noedr');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel noedrbtn"){
        const notes = $(event.target).closest('.noedr');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'noedr');
    }else if(event.target.className == "fa fa-edit fa-sm noedrpenbtn"){
        const notes = $(event.target).closest('.noedr');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'noedr');
    }
});

$('#switches_table_body').click('.noedr-editable .noedr-cancel', function(event) {
    const btnCancel = $(event.target).closest('.noedr-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.noedr-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('noedr-editable');
    notesCell.addClass('noedr');
});

$('#switches_table_body').click('.noedr-editable .noedr-apply', function(event) {
    
    const btnApply = $(event.target).closest('.noedr-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.noedr-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const switchmodel = btnApply.closest('tr').find('.switchmodel');
    

    const textField = notes.find('.noedr-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel noedrbtn"><i class="fa fa-edit fa-sm noedrpenbtn" aria-hidden="true"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('noedr-editable');
    notes.addClass('noedr');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(), 
            'switchmodel': switchmodel.text().trim(),
            'data' :textField.val(), 
            'type' : "noedr"}
    };
    console.log(datas);
    $.ajax({
        url : `${baseUrl}index.php/switches/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
            console.log(datas);
        }
    });
});