let baseUrl;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {
    
    let itemsOffset = 0;
    // let lastId = 0;
    let itemsLimit = 100;
    let itemsRequested = false;
  
    baseUrl = $('#switchesScript').data('baseUrl');
    syncStickyHeader();

    $.get(`${baseUrl}index.php/switches/load_galactus`,  function(response) {
        $('#switches_table_body').html(response);
                const tableWidth = $('#switches_table_body').width();
                $('.scroller-content').width(tableWidth); 
                $('.sticky-table').width(tableWidth);
                syncStickyHeader(); 
    });

    $(document).scroll(function() {
        const rowCount = $('#switches_table_body > tr').length;
        console.log(rowCount);
        // if the rows initially returned are less than 100, there are no more rows to load further
        if (rowCount < itemsLimit) return;
        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('#switches_table_body > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            itemsOffset += itemsLimit;

            const data = {
                'items_offset': itemsOffset,
            };

            $.post(`${baseUrl}index.php/switches/load_galactus`, data, function(response) {
                $('#switches_table_body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    $('#doneSwitch').on('change', function() { 
        if ($('#doneSwitch').prop('checked')){
            const data = {'done' : 1}
            $.post(`${baseUrl}index.php/switches/get_done`, data, function(response) {
                $('#switches_table_body').html(response);
            });
        } else {
                loadswitch();
            }        
    });

    
    $('#myModal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });

    let listingToMarkDone = -1;

    $('#switches_table_body').on('click', '.btn-done', function(event) {
        
        const button = $(this).closest('.btn-done');
        
        if (!button) {
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing done.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        rowToRemove = button.closest('tr');
        const sku = rowToRemove.find('#lwsku').data('value');
        
        $('.sku-done').html(sku);

        if (!rowToRemove) {
            event.preventDefault();
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing done.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        listingToMarkDone = rowToRemove.find('.sku').data('id');
        
    });

    $('.btn-confirm-done').on('click', function() {
        $('#markListingDoneModal').modal('hide');
        const data = { 'id': listingToMarkDone };
        
        $.post(`${baseUrl}index.php/switches/mark_done`, data, function(response) {
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Mark Done failed',
                    body: 'Failed to mark listing done.',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Mark Done success',
                body: 'Successfully marked listing done.',
                autohide: true,
                delay: 7000
            });

            loadswitch();
        });
    });

    $('#switches_table_body').on('click', '.btn-undone', function(event) {
        const button = $(this).closest('.btn-undone');
        
        if (!button) {
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing undone.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        rowToRemove = button.closest('tr');
        const sku = rowToRemove.find('#lwsku').data('value');
        
        $('.sku-done').html(sku);

        if (!rowToRemove) {
            event.preventDefault();
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing undone.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        listingToMarkDone = rowToRemove.find('.sku').data('id');
        console.log(listingToMarkDone);
    });

    $('.btn-confirm-undone').on('click', function() {
        $('#markListingUndoneModal').modal('hide');
        const data = { 'id': listingToMarkDone };
    
        $.post(`${baseUrl}index.php/switches/mark_undone`, data, function(response) {
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Mark Undone failed',
                    body: 'Failed to mark listing undone.',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Mark undone success',
                body: 'Successfully marked listing undone.',
                autohide: true,
                delay: 7000
            });
            loadswitch();
        });
    });

    $('#searchColumn').click(function(event){
        var data = {
        'searchItem' : $('#itemSearch').val().trim()}
        $.get(`${baseUrl}index.php/switches/search_item`,data,  function(response) {
            $('#switches_table_body').html(response);
                const tableWidth = $('#switches_table_body').width();
                $('.scroller-content').width(tableWidth); 
                $('.sticky-table').width(tableWidth);
                syncStickyHeader(); 
        });
        
    });

    function loadswitch(){
        
        $.get(`${baseUrl}index.php/switches/load_galactus`,function(response) {
            $('#switches_table_body').html(response);

            const refreshUrl = window.location.href.split('?')[0];
            window.location.href = refreshUrl;
            
        });
    }

        $('#submitf').on('click',function(event){  
            if  (($("#sku").val() == "")){
                $("#sku").focus();
                alert("SKU must be filled out");
                return false;
            }else{
                var data = {
                    'sku' : $("#sku").val(),
                    'qty' : $("#qty").val(),
                    'con': $("#con").val(),
                    'oem': $("#oem").val(),
                    'switch': $("#switch").val(),
                    'port': $("#port").val(),
                    'license': $("#license").val(),
                    'features': $("#features").val(),
                    'dual': $("#dual").val(),
                    'airflow': $("#airflow").val(), 
                    'noedr': $("#noedr").val(),
                    ajax : '1'
                };
                console.log(data);
                $.ajax({
                    url : `${baseUrl}index.php/switches/createListing`,
                    type : "post",
                    data : data,
                    success : function(response){
                        if (response == true){
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                            $('#sku').focus();
                            $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                        }else if(response == true){
                            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                            $('#speed').focus();
                            $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                        }else{
                            const refreshUrl = window.location.href.split('?')[0];
                            window.location.href = refreshUrl;
                        }
                    }
                
                });
            }
        });
    });
