const columnsToCopy = {
    partNumber: 8,
    description: 10,
    condition: 11,
    quantity: 12,
    total: 15,
    bar: 9,
    price : 13
};

let baseUrl;

$(document).ready(function() {
    let itemsOffset = 0;
    let itemsLimit = 100;


    syncStickyHeader();
    
    baseUrl = $("#inventoryScript").data('baseUrl');
    $(".btn-copy-textarea").click(function() {
        const text = $(".textarea-copy").val();

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = text;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();
    });

    const searchParam = new URLSearchParams(window.location.search);
    const search = searchParam.get('search');
    var categoryFilters = searchParam.get('category_filters');
    var conditionFilters = searchParam.get('condition_filters');
    const listingFilter = searchParam.get('listing_filter');
    var sortOrder = searchParam.get('sort_order');
    var orderBy = searchParam.get('order_by');

    const wasItemRequested = search !== null;
    
    if (wasItemRequested) {
        loadData();
    }

    $('table#product-table').on('click', '.update-linnworks-desc', function(response){

        if(event.target.className == "fa fa-edit update-linnworks-desc"){
            const button = $(event.target).closest('.update-linnworks-desc');
            if (!button) return;

            var sku = button.closest('tr').find('.par').text().trim();
            var description = button.closest('tr').find('.des').text().trim();
            var id = button.closest('tr').find('.item-id').text();
            var description_new = button.closest('tr').find('.des');
     

            $('#sku-linnworks').text(sku);
            $('#current-desc-linn').text(description);
            $('#new-desc-linn').val(description_new.data('value'));
        
            $('#updateDescriptionLinn').modal('toggle');
        }
        
        
    });

    $('#updateLinnworksDescSubmit').on('click', function(response){
        var data = {
            'sku'  :    $('#sku-linnworks').text(),
            'desc'  :   $('#new-desc-linn').val()
        };
 
        $.post(`${baseUrl}index.php/inventory/update_linn_desc`, data, function(response){
            $('#updateDescriptionLinn').modal('hide');
            searcher();
        });
    });

    $('table#product-table').on('click', '.update-linnworks-price', function(response){

        if(event.target.className == "fa fa-edit update-linnworks-price"){
            const button = $(event.target).closest('.update-linnworks-price');
            if (!button) return;

            var sku = button.closest('tr').find('.par').text().trim();
            var price = button.closest('tr').find('.price').text().trim();
            var id = button.closest('tr').find('.item-id').text();
            var price_new = button.closest('tr').find('.price');
     

            $('#sku-linnworks-price').text(sku);
            $('#current-price-linn').text(price);
            $('#new-price-linn').val(price_new.data('value'));
        
            $('#updatePriceLinn').modal('toggle');
        }
    });

    $('#updateLinnworksPriceSubmit').on('click', function(response){
        var data = {
            'sku'  :    $('#sku-linnworks-price').text(),
            'price'  :   $('#new-price-linn').val()
        };
 
        $.post(`${baseUrl}index.php/inventory/update_linn_price`, data, function(response){
            $('#updatePriceLinn').modal('hide');
            searcher();
        });
    });

    $('#product_table_body').on('click', '.btn-draft', function() {
        
        const btnDraft = $(this).closest('.btn-draft');
        

        if (!btnDraft) return;
    
        const cellPar = btnDraft.closest('tr').find('.par');
        const cellOempn = btnDraft.closest('tr').find('.bar');
        const cellCategory = btnDraft.closest('tr').find('.cat');
        const cellDescription = btnDraft.closest('tr').find('.des');
        const cellCondition = btnDraft.closest('tr').find('.con');
        const cellPrice = btnDraft.closest('tr').find('.price');
        const cellQuantity = btnDraft.closest('tr').find('.qty');
        const cellImages = btnDraft.closest('tr').find('img');
        
         
    
        if (!cellPar || !cellOempn || !cellCategory || !cellDescription || !cellPrice || !cellQuantity) return;
    
        const category = cellCategory.data('value');
        const sku = cellPar.data('value');
        const description = cellDescription.data('value');
        const condition = cellCondition.data('value');
        const price = cellPrice.data('value');
        const quantity = cellQuantity.data('value');

        
        const dataItems = {
            'par': sku
        };

        if(category =='SAS'||category =='SATA'||category =='SSD'){
            
            $.post(`${baseUrl}index.php/inventory/drives_title`, dataItems, function(response){
                if (response.is_not_existing) {
                    $('#errormessage').html(`<div class="modal-title alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><h6><i class="icon fas fa-check"></i>Sku does not exist in drives! Please add new listing task</h6></span>
                    </div>`);
                    $('#draftModalLabel').html('');
                    $('#draftDes').html('');
                    setTimeout(function() {
                    $(".alert").alert('close');
                    }, 5000);
                } else {
                    $('#draftModalLabel').html(response);
                    $('#draftDes').html(response);
                }
            });
        }else{
            $('#draftModalLabel').html(sku);
            $('#draftDes').html(description);
        }
        
        $('#draftSku').html(sku);
        $('#draftPrice').html(price);
        $('#draftQty').html(quantity);
        $('#draftCondition').html(condition);
        $('#draftCategory').html(category);
        
       
       const ar = $(this).closest('tr').find('.h').children();
       if ( $('.carousel-inner').children().length > 0 ) {
            $('.carousel-inner').empty();
        }

       for (let index = 0; index < ar.length; index++) {  
           const alink = ar[index].getAttribute("href");
            $('.carousel-inner').append("<div id=\"itm" +index+ "\" class=\"carousel-item\"><img id=\"slide\" class=\"d-block w-100\" src=" + alink +" alt=\"slide\"></div>");
          }
          
          $('#itm0').attr("class","carousel-item active");
    });
    

    $('#addtask').on('click', function() {
        $par = document.getElementById('draftSku').innerText;
        $cat = document.getElementById('draftCategory').innerText;
        $action = $('#action').val();
        $price = document.getElementById('draftPrice').innerText;
        $shipping =  $('#shipping').val();
        const dataItems = {
            'par': $par,
            'category': $cat,
            'action': $action,
            'shipping': $shipping,
            'price': $price
        };

    

        $.post(`${baseUrl}index.php/inventory/addtask`, dataItems, function(response){
            if (response.is_existing) {
                $('#draftModal').modal('hide');
                $("#existing").modal('show');
                $('#close_existing').click(function(){$('#existing').modal('hide');});
            } else {
                $('#draftModal').modal('hide');

                $(document).Toasts('create', {
                    title: 'Task successfully saved',
                    body: `Task successfully created for ${$par}`,
                    autohide: true,
                    delay: 5000
                });
            }
        });
    });
    

    $('.btn-create-ad-blank').on('click', function() {
        $('#newEbayAdForm').trigger('reset');
        $('#onPremisesUploadForm').trigger('reset');
        $('.image-display').html('');
    });

    $('#product_table_body').on('click', '.btn-create-ad', function() {
        const btnCreateAd = $(this).closest('.btn-create-ad');

        if (!btnCreateAd) return;

        const cellPar = btnCreateAd.closest('tr').find('.par');
        const cellOempn = btnCreateAd.closest('tr').find('.bar');
        const cellCategory = btnCreateAd.closest('tr').find('.cat');
        const cellDescription = btnCreateAd.closest('tr').find('.des');
        const cellCondition = btnCreateAd.closest('tr').find('.con');
        const cellPrice = btnCreateAd.closest('tr').find('.price');
        const cellQuantity = btnCreateAd.closest('tr').find('.qty');

        if (!cellPar || !cellOempn || !cellCategory || !cellDescription || !cellPrice || !cellQuantity) return;

        const category = cellCategory.data('value');
        const sku = cellPar.data('value');
        const mpn = cellOempn.data('value');
        const description = cellDescription.data('value');
        const condition = cellCondition.data('value');
        const price = cellPrice.data('value');
        const quantity = cellQuantity.data('value');

        const data = { 'sku': sku, 'category': category };

        $('#newEbayAdCategory').find(':selected').prop('selected', false);
        $('#newEbayAdCategory').children().each(function() {
            if ($(this).text() === category) {
                $(this).prop('selected', true);
            }
        });

        $('#newEbayAdCondition').find(':selected').prop('selected', false);
        $('#newEbayAdCondition').each(function() {
            if ($(this).text() === condition) {
                $(this).prop('selected', true);
            }
        });

        $('#newEbayAdCategory').trigger('change');
        $('#newEbayAdSku').val(sku);
        $('#newEbayAdTitle').val(description);
        $('#newEbayAdPrice').val(price);
        $('#newEbayAdQuantity').val(quantity);

        const mpnLowerCase = mpn.toLowerCase();

        
        if (mpnLowerCase === 'com' || mpnLowerCase === 'net' || mpnLowerCase === 'plus' || mpnLowerCase === 'itr') {
            $('#newEbayAdStore').val(mpnLowerCase);
        }

        $.post(`${baseUrl}index.php/inventory/load_item_properties`, data, function(response) {
            $('.new-ebay-ad-category-specifics-container').find('input[name="MPN"]').val(mpn);
            switch(category) {
            case 'CPU':
                autofillCpu(response);
                break;
            case 'SATA':
            case 'SAS':
            case 'NVME':
            case 'SSD':
                autofillDrive(response);
                break;
            case 'Memory':
                autofillMemory(response);
                break;
            case 'Motherboard':
                autofillMotherboard(response);
                break;
            default:
                break;
            }
        });

        validateNewAdForm();
    });

    let itemsRequested = false;

    $(document).scroll(function() {
        const rowCount = $('tbody > tr').length;

        // if the rows initially returned are less than 100, there are no more rows to load further
        if (itemsOffset === 0 && rowCount < itemsLimit) return;

        const position = $(this).scrollTop();
        let maxHeight = 0;

        $('tbody > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            itemsOffset += itemsLimit;

            if (categoryFilters !== null) {
                categoryFilters.split(',').forEach(function(category) {
                    $(`#category_${category.replace(' ', '_')}`).prop('checked', true);
                });
            } else {
                categoryFilters = '';
            }

            if (conditionFilters !== null) {
                conditionFilters.split(',').forEach(function(condition) {
                    $(`#condition_${condition.replace(' ', '_')}`).prop('checked', true);
                });
            } else {
                conditionFilters = '';
            }

            if (sortOrder !== null && orderBy !== null) {
                $('#orderBy').find(':selected').prop('selected', false);
                $('#sortOrder').find(':selected').prop('selected', false);   
                $(`#orderBy > option[value="${orderBy}"]`).prop('selected', true);
                $(`#sortOrder > option[value="${sortOrder}"]`).prop('selected', true);
            } else {
                sortOrder='';
                orderBy='';
            }

            $('#itemSearch').val(search);
            
            const data = {
                'search': search,
                'category_filters': categoryFilters,
                'condition_filters': conditionFilters,
                'listing_filter': listingFilter,
                'sort_order': sortOrder,
                'order_by': orderBy,
                'items_offset': itemsOffset
            };

            $.post(`${baseUrl}index.php/inventory/request/load_items`, data, function(response) {
                $('#product_table_body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    $('table#product-table').on('click', '.keyword-btn', function(event) {
            if (!$('.keyword-alert').hasClass('d-none')) {
                $('.keyword-alert').addClass('d-none');
            }

            const par = $(event.target).closest('tr').find('.par').data('value');
            const keyword = $(event.target).closest('tr').find('.key').data('value');

            $('#keywordPar').val(par);
            $('#keywordPrase').val(keyword);
            $('#keywordModal').modal('toggle');
    });

    $('#keywordForm').submit(function(event) {
        event.preventDefault();

        const data = {
            'part': $('#keywordPar').val(),
            'prase': $('#keywordPrase').val()
        };
        
        $.post(`${baseUrl}index.php/inventory/save_keyword`, data, function(response) {
            const isSaveSuccessful = response.data.is_save_successful;
            const par = response.data.par;

            if (!response.data.message) {
                $('#keywordModal').modal('hide');
            }

            if (isSaveSuccessful) {
                $(document).Toasts('create', {
                    title: 'Save Successful',
                    body: `Keyword for part number ${par} successfully saved.`,
                    icon: 'fas fa-check',
                    autohide: true,
                    delay: 4000
                });
                searcher();
            } else if (!isSaveSuccessful && response.data.message === 'duplicate_prase') {
                $('.keyword-alert').html('Duplicate keyword. Please try another keyword');
                $('.keyword-alert').removeClass('d-none');
            } else {
                $(document).Toasts('create', {
                    title: 'Save Error',
                    body: `An error occured while saving keyword for part number ${par}.`,
                    icon: 'fas fa-exclamation',
                    autohide: true,
                    delay: 4000
                });
            }
        });
    });

    $('#keywordModal').on('hidden.bs.modal', function() {
        if (!$('.keyword-alert').hasClass('d-none')) {
            $('.keyword-alert').addClass('d-none');
        }
        
        $('#keywordPar').val('');
        $('#keywordPrase').val('');
    });

    $('#sortOrder').change(function() {
        searcher();
    });

    $('#orderBy').change(function() {
        searcher();
    });

    $('#listingFilter').change(function() {
        searcher();
    });

    $('#searchHistory').change(function(event) {
        const selected = $(event.target).find(':selected');

        $('#itemSearch').val(selected.val());
        searcher();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();
        searcher();
    });

    function loadData() {
        if (categoryFilters !== null) {
            categoryFilters.split(',').forEach(function(category) {
                $(`#category_${category.replace(' ', '_')}`).prop('checked', true);
            });
        } else {
            categoryFilters = '';
        }

        if (conditionFilters !== null) {
            conditionFilters.split(',').forEach(function(condition) {
                $(`#condition_${condition.replace(' ', '_')}`).prop('checked', true);
            });
        } else {
            conditionFilters = '';
        }

        $('#listingFilter').val(listingFilter);

        if (sortOrder !== null && orderBy !== null) {
            $('#orderBy').find(':selected').prop('selected', false);
            $('#sortOrder').find(':selected').prop('selected', false);   
            $(`#orderBy > option[value="${orderBy}"]`).prop('selected', true);
            $(`#sortOrder > option[value="${sortOrder}"]`).prop('selected', true);
        } else {
            sortOrder='';
            orderBy='';
        }

        $('#itemSearch').val(search);

        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#exportToGdrive').prop('disabled', true);
         $('.con-cat-filter:not(:checked)').prop('disabled', true);
        
        $("#total-items").html(spinner);
        $("#total-price").html(spinner);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        
        const dataItems = {
            'search': search,
            'category_filters': categoryFilters,
            'condition_filters': conditionFilters,
            'listing_filter': listingFilter,
            'sort_order': sortOrder,
            'order_by': orderBy,
            'items_offset': itemsOffset
        };

        $.post(`${baseUrl}index.php/inventory/request/load_summary`, dataItems, function(response){
                $("#total-items").html(response.data.total);
                $("#total-price").html(`$${response.data.price}`);
        });

        $.post(`${baseUrl}index.php/inventory/request/load_items`, dataItems, function(response){
            $("#product_table_body").html(response);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
            $('#exportCSV').prop('disabled', false);
            $('#exportToGdrive').prop('disabled', false);
            $('.con-cat-filter:not(:checked)').prop('disabled', false);
            syncStickyHeader();
        });

        saveHistory(search);
    }

    function searcher() {
        const searchItem = $('#itemSearch').val();
        const listingFilter = $('#listingFilter').val();
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();
        let categories = [];

        $('.category-filters :checked').each(function() {
            categories.push($(this).val());
        });

        const categoriesText = categories.join(',');
        
        let conditions = [];

        $('.condition-filters :checked').each(function() {
            conditions.push($(this).val());
        });

        const conditionsText = conditions.join(',');

        let categoriesText_with_and = categoriesText;

        if (/&/.test(categoriesText) == true) {
            // alert('Your search string contains illegal characters.');
            categoriesText_with_and = categoriesText.replace("&", "%2D");
        }

        const route = `${baseUrl}index.php/inventory?search=${searchItem}&order_by=${orderBy}&sort_order=`
              + `${sortOrder}&category_filters=${categoriesText_with_and}&condition_filters=${conditionsText}`
              + `&listing_filter=${listingFilter}`;

        window.location.href = route;
    }

    function saveHistory(search) {
        $.post(`${baseUrl}index.php/inventory/request/save_search_history`, { 'search': search });
    }

    $('#product_table_body').on('click', '.task_list', function(event) {
        const notes = $(event.target).closest('.task_list');
        const sku = $(event.target).closest('.sku');
        const tasks = $(event.target).closest('.task_list').val();
    
        if (!notes) return;

        const value = notes.data('value');
        const listing_id = sku.closest('tr').find('.par');
        const a = listing_id.text().toString();


        const getsku = sku.closest('tr').find('.par');
        const loadsku = getsku.text().trim().toString();

        const getprice = sku.closest('tr').find('.price');
        const loadprice = getprice.text().trim().toString();


        $('#success').html('');
        $('#lot').val('');
        $('#notes').val('');
        $(".getsku").html(`<label for="weight">SKU</label>
            <input type="text" class="form-control" id="sku" step="any" value=${loadsku}>`);

        $('#request').val(tasks);

        $(".getprice").html(`<label for="price">Price</label>
              <input type="text" class="form-control" id="price" step="any"
              value="${loadprice}">`);                       

        //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
    });

    $('#product_table_body').on('click', '.link', function(event) {
            const notes = $(event.target).closest('.link');

            if (!notes) return;
            
            const value = notes.data('value');
            const listing_id = notes.closest('.bbb').find('.link');
            
            addEditableFieldsku(notes, value, 'sku');
    });

    $('#exportCSV').click(function(){
        var data = {
			'search': search,
            'category_filters': categoryFilters,
            'condition_filters': conditionFilters,
            'listing_filter': listingFilter,
            'sort_order': sortOrder,
            'order_by': orderBy,
            'items_offset': itemsOffset
		};
   
        const  url_csv = `${baseUrl}index.php/inventory/generate_csv`;

        $.post(url_csv, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `export_items_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

    function autofillCpu(response) {
        const cpuCache = response.cpu_cache.split(' ')
              .filter(function(word) { return word !== 'SmartCache' && word !== 'L2' && word !== 'L3'; })
              .join(' ');
        
        if (response.sockets_supported.includes('LGA')) {
            $('.new-ebay-ad-category-specifics-container').find('[name="Brand"]').val('Intel');
        }
        
        if (response.cpu_cache.includes('SmartCache') || response.cpu_cache.includes('L2')) {
            $('.new-ebay-ad-category-specifics-container').find('[name="L2 Cache"]').val(cpuCache);
        } else if (response.cpu_cache.includes('L3')) {
            $('.new-ebay-ad-category-specifics-container').find('[name="L3 Cache"]').val(cpuCache);
        }

        $('.new-ebay-ad-category-specifics-container').find('[name="Number of Cores"]').val(response.number_of_cores);
        $('.new-ebay-ad-category-specifics-container').find('[name="Clock Speed"]').val(response.speed);
        $('.new-ebay-ad-category-specifics-container').find('[name="Socket Type"]').val(response.sockets_supported);
    }

    function autofillDrive(response) {
        const categorySpecificsContainer = $('.new-ebay-ad-category-specifics-container');

        categorySpecificsContainer.find('[name="MPN"]').val(response.bar);
        categorySpecificsContainer.find('[name="Brand"]').val(response.brand);
        categorySpecificsContainer.find('[name="Form Factor"]').val(response.ff);
        categorySpecificsContainer.find(`[value="${response.interface}"]`).prop('checked', true);
        categorySpecificsContainer.find('[name="Storage Capacity"]').val(response.size);
        categorySpecificsContainer.find('[name="Rotation Speed"]').val(response.rpm);
        categorySpecificsContainer.find('[name="Read Speed"]').val(response.speed);
    }

    function autofillMemory(response) {
        const categorySpecificsContainer = $('.new-ebay-ad-category-specifics-container');

        categorySpecificsContainer.find('[name="MPN"]').val(response.barcode);
        categorySpecificsContainer.find('[name="Brand"]').val(response.brand);
        categorySpecificsContainer.find('[name="Total Capacity"]').val(response.memory_size);
        categorySpecificsContainer.find('[name="Memory Family"]').val(response.memory_family);
        categorySpecificsContainer.find('[name="Bus Speed"]').val(`${response.memory_type}-${response.speed}`);
    }

    function autofillMotherboard(response) {
        const categorySpecificsContainer = $('.new-ebay-ad-category-specifics-container');

        categorySpecificsContainer.find('[name="MPN"]').val(response.barcode);
        categorySpecificsContainer.find('[name="Brand"]').val(response.brand);
        categorySpecificsContainer.find('[name="Model"]').val(response.model);
        categorySpecificsContainer.find('[name="Memory Type"]').val(response.memory_family);
    }

    function addEditableFieldsku(cell, value, className) {}

    $('#items').on('click', '.sku-cancel', function(event) {
        const btnCancel = $(event.target).closest('.sku-cancel');
        if (!btnCancel) return;
        
        const notesCell = btnCancel.closest('.sku-editable');
        const requester = notesCell.data('requester');
        if (!requester) return;
        const firstname = requester.trim().split(' ').slice(0, 1);
        const firstnameWithTooltip = showNameWithTooltip(firstname, requester);

        notesCell.html(`<button type="button" class="btn btn-link text-danger draftid-cancel skubtn"><i class="fa fa-pen skupenbtn" aria-hidden="true"></i></button>${firstnameWithTooltip} ${notesCell.data('value')}`);
        notesCell.removeClass('sku-editable');
        notesCell.addClass('sku');
    });

    $('#items').on('click', '.sku-apply', function(event) {
        const btnApply = $(event.get).closest('.sku-apply');
        if (!btnApply) return;
        
        const notes = btnApply.closest('.sku-editable');
        const listing_id = btnApply.closest('tr').find('.listing_id');
        const par = btnApply.closest('tr').find('.par');
        const datetime = btnApply.closest('tr').find('.datetime');
        const request = btnApply.closest('tr').find('.request');
        const sku = btnApply.closest('tr').find('.sku');
        
        
        const textField = notes.find('.sku-input');
        if (!textField) return;
        if (!textField.val()) return;
        
        notes.html(`<button type="button" class="btn btn-link text-danger draftid-cancel skubtn"><i class="fa fa-pen skupenbtn" aria-hidden="true"></i></button>
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('sku-editable');
        notes.addClass('sku');
        
        if ((textField.val() && textField.val().length > 0)) {
            const data = {'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "sku"} };
            
            //$.post(`${baseUrl}index.php/task/request/update_columns`, data);
        }

        var datas = {
            'data': {'request': textField.val(), 'datetime': datetime.text(), 'sku': textField.val(), 'listing_id': listing_id.text(), 'col': "sku"}
        };
        $.ajax({
            url : `${baseUrl}index.php/task/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////sku
});
 
function syncStickyHeader() {
    const tableWidth = $('.main-table').width();
    $('.scroller-content').width(tableWidth);
    $('.sticky-table').width(tableWidth);
    
    $('.main-header-row').children().each(function(index, header) {
        $('.sticky-header-row').children().eq(index).width($(header).width());
    });

    
}