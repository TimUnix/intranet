let channels;
let request_actions;

$(document).ready(function() {
    let currentCellNewPrice;
    let currentCellNotes;
    let currentPar;
    let parval;
    let stepval;
    
    const baseUrl = $('#listingFieldEditScript').data('baseUrl');

    $('#items').click('.new-price', function(event) {
        const newPrice = $(event.target).closest('.new-price');

        if (!newPrice) return;

        const value = newPrice.data('value');

        addEditableField(newPrice, value, 'new-price');
    });

    $('#items').click('.notes', function(event) {
        const notes = $(event.target).closest('.notes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableFieldnote(notes, value, 'notes');
    });


    ///
    $('#items').click('.datetimes', function(event) {
        const notes = $(event.target).closest('.datetimes');

        if (!notes) return;

        const value = notes.data('value');

        addEditableField(notes, value, 'datetimes');
    });
    /////////////partnumber
    $('#load_cpu').click('.par', function(event) {
        if(event.target.className == "par"){
            const notes = $(event.target).closest('.par');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel parbtn"){
            const notes = $(event.target).closest('.par');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldpar(notes, value, 'par');
        }else if(event.target.className == "fa fa-edit parpenbtn"){
            const notes = $(event.target).closest('.par');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldpar(notes, value, 'par');
        }
    });
    function addEditableFieldpar(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.par-editable .par-cancel', function(event) {
        const btnCancel = $(event.target).closest('.par-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.par-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel parbtn"><i class="fa fa-edit parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('par-editable');
        notesCell.addClass('par');
    });
    $('#load_cpu').click('.par-editable .par-apply', function(event) {
        const btnApply = $(event.target).closest('.par-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.par-editable');
        const sku = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.par-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel parbtn"><i class="fa fa-edit parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('par-editable');
        notes.addClass('par');
    
        

        var datas = {
            'data': {'partnumber': parval, 'stepcode': stepcode.text().trim(), 'data' :textField.val(), 'type' : "part"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////partnumber


    /////////////stepcode
    $('#load_cpu').click('.stepcode', function(event) {
        if(event.target.className == "stepcode"){
            const notes = $(event.target).closest('.stepcode');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel stepcodebtn"){
            const notes = $(event.target).closest('.stepcode');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldstepcode(notes, value, 'stepcode');
        }else if(event.target.className == "fa fa-edit stepcodepenbtn"){
            const notes = $(event.target).closest('.stepcode');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldstepcode(notes, value, 'stepcode');
        }
    });
    function addEditableFieldstepcode(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        stepval = value;
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.stepcode-editable .stepcode-cancel', function(event) {
        const btnCancel = $(event.target).closest('.stepcode-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.stepcode-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel parbtn"><i class="fa fa-edit stepcodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('stepcode-editable');
        notesCell.addClass('stepcode');
    });
    $('#load_cpu').click('.stepcode-editable .stepcode-apply', function(event) {
        const btnApply = $(event.target).closest('.stepcode-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.stepcode-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.stepcode-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel stepcodebtn"><i class="fa fa-edit stepcodepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('stepcode-editable');
        notes.addClass('stepcode');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepval, 'data' : textField.val(), 'type' : "stepcode"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////stepcode


    /////////////speed
    $('#load_cpu').click('.speed', function(event) {
        if(event.target.className == "speed"){
            const notes = $(event.target).closest('.speed');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel speedbtn"){
            const notes = $(event.target).closest('.speed');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldspeed(notes, value, 'speed');
        }else if(event.target.className == "fa fa-edit speedpenbtn"){
            const notes = $(event.target).closest('.speed');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldspeed(notes, value, 'speed');
        }
    });
    function addEditableFieldspeed(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.speed-editable .speed-cancel', function(event) {
        const btnCancel = $(event.target).closest('.speed-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.speed-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel speedbtn"><i class="fa fa-edit speedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('speed-editable');
        notesCell.addClass('speed');
    });
    $('#load_cpu').click('.speed-editable .speed-apply', function(event) {
        const btnApply = $(event.target).closest('.speed-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.speed-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.speed-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel speedbtn"><i class="fa fa-edit speedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('speed-editable');
        notes.addClass('speed');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' : textField.val(), 'type' : "speed"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////speed


    /////////////number_of_cores
    $('#load_cpu').click('.number_of_cores', function(event) {
        if(event.target.className == "number_of_cores"){
            const notes = $(event.target).closest('.number_of_cores');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel number_of_coresbtn"){
            const notes = $(event.target).closest('.number_of_cores');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldnumber_of_cores(notes, value, 'number_of_cores');
        }else if(event.target.className == "fa fa-edit number_of_corespenbtn"){
            const notes = $(event.target).closest('.number_of_cores');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldnumber_of_cores(notes, value, 'number_of_cores');
        }
    });
    function addEditableFieldnumber_of_cores(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.number_of_cores-editable .number_of_cores-cancel', function(event) {
        const btnCancel = $(event.target).closest('.number_of_cores-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.number_of_cores-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel number_of_coresbtn"><i class="fa fa-edit number_of_corespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('number_of_cores-editable');
        notesCell.addClass('number_of_cores');
    });
    $('#load_cpu').click('.number_of_cores-editable .number_of_cores-apply', function(event) {
        const btnApply = $(event.target).closest('.number_of_cores-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.number_of_cores-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.number_of_cores-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel number_of_coresbtn"><i class="fa fa-edit number_of_corespenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('number_of_cores-editable');
        notes.addClass('number_of_cores');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' : textField.val(), 'type' : "number_of_cores"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////number_of_cores


    /////////////codename
    $('#load_cpu').click('.codename', function(event) {
        if(event.target.className == "codename"){
            const notes = $(event.target).closest('.codename');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel codenamebtn"){
            const notes = $(event.target).closest('.codename');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcodename(notes, value, 'codename');
        }else if(event.target.className == "fa fa-edit codenamepenbtn"){
            const notes = $(event.target).closest('.codename');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcodename(notes, value, 'codename');
        }
    });
    function addEditableFieldcodename(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.codename-editable .codename-cancel', function(event) {
        const btnCancel = $(event.target).closest('.codename-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.codename-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel codenamebtn"><i class="fa fa-edit codenamepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('codename-editable');
        notesCell.addClass('codename');
    });
    $('#load_cpu').click('.codename-editable .codename-apply', function(event) {
        const btnApply = $(event.target).closest('.codename-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.codename-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.codename-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel codenamebtn"><i class="fa fa-edit codenamepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('codename-editable');
        notes.addClass('codename');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' : textField.val(), 'type' : "codename"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////codename


    /////////////thermal_design_power
    $('#load_cpu').click('.thermal_design_power', function(event) {
        if(event.target.className == "thermal_design_power"){
            const notes = $(event.target).closest('.thermal_design_power');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel thermal_design_powerbtn"){
            const notes = $(event.target).closest('.thermal_design_power');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldthermal_design_power(notes, value, 'thermal_design_power');
        }else if(event.target.className == "fa fa-edit thermal_design_powerpenbtn"){
            const notes = $(event.target).closest('.thermal_design_power');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldthermal_design_power(notes, value, 'thermal_design_power');
        }
    });
    function addEditableFieldthermal_design_power(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.thermal_design_power-editable .thermal_design_power-cancel', function(event) {
        const btnCancel = $(event.target).closest('.thermal_design_power-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.thermal_design_power-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel thermal_design_powerbtn"><i class="fa fa-edit thermal_design_powerpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('thermal_design_power-editable');
        notesCell.addClass('thermal_design_power');
    });
    $('#load_cpu').click('.thermal_design_power-editable .thermal_design_power-apply', function(event) {
        const btnApply = $(event.target).closest('.thermal_design_power-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.thermal_design_power-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.thermal_design_power-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel thermal_design_powerbtn"><i class="fa fa-edit thermal_design_powerpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('thermal_design_power-editable');
        notes.addClass('thermal_design_power');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' :  textField.val(), 'type' : 'thermal_design_power'}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////thermal_design_power


    /////////////cpu_cache
    $('#load_cpu').click('.cpu_cache', function(event) {
        if(event.target.className == "cpu_cache"){
            const notes = $(event.target).closest('.cpu_cache');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel cpu_cachebtn"){
            const notes = $(event.target).closest('.cpu_cache');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcpu_cache(notes, value, 'cpu_cache');
        }else if(event.target.className == "fa fa-edit cpu_cachepenbtn"){
            const notes = $(event.target).closest('.cpu_cache');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldcpu_cache(notes, value, 'cpu_cache');
        }
    });
    function addEditableFieldcpu_cache(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.cpu_cache-editable .cpu_cache-cancel', function(event) {
        const btnCancel = $(event.target).closest('.cpu_cache-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.cpu_cache-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel cpu_cachebtn"><i class="fa fa-edit cpu_cachepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('cpu_cache-editable');
        notesCell.addClass('cpu_cache');
    });
    $('#load_cpu').click('.cpu_cache-editable .cpu_cache-apply', function(event) {
        const btnApply = $(event.target).closest('.cpu_cache-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.cpu_cache-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.cpu_cache-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel cpu_cachebtn"><i class="fa fa-edit cpu_cachepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('cpu_cache-editable');
        notes.addClass('cpu_cache');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' : textField.val(), 'type' : "cpu_cache"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////cpu_cache


    /////////////sockets_supported
    $('#load_cpu').click('.sockets_supported', function(event) {
        if(event.target.className == "sockets_supported"){
            const notes = $(event.target).closest('.sockets_supported');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel sockets_supportedbtn"){
            const notes = $(event.target).closest('.sockets_supported');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsockets_supported(notes, value, 'sockets_supported');
        }else if(event.target.className == "fa fa-edit sockets_supportedpenbtn"){
            const notes = $(event.target).closest('.sockets_supported');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldsockets_supported(notes, value, 'sockets_supported');
        }
    });
    function addEditableFieldsockets_supported(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.sockets_supported-editable .sockets_supported-cancel', function(event) {
        const btnCancel = $(event.target).closest('.sockets_supported-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.sockets_supported-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel sockets_supportedbtn"><i class="fa fa-edit sockets_supportedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('sockets_supported-editable');
        notesCell.addClass('sockets_supported');
    });
    $('#load_cpu').click('.sockets_supported-editable .sockets_supported-apply', function(event) {
        const btnApply = $(event.target).closest('.sockets_supported-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.sockets_supported-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.sockets_supported-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel sockets_supportedbtn"><i class="fa fa-edit sockets_supportedpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('sockets_supported-editable');
        notes.addClass('sockets_supported');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' : textField.val(), 'type' : 'sockets_supported'}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////sockets_supported


    /////////////grade
    $('#load_cpu').click('.grade', function(event) {
        if(event.target.className == "grade"){
            const notes = $(event.target).closest('.grade');
    
            if (!notes) return;
    
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel gradebtn"){
            const notes = $(event.target).closest('.grade');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldgrade(notes, value, 'grade');
        }else if(event.target.className == "fa fa-edit gradepenbtn"){
            const notes = $(event.target).closest('.grade');

            if (!notes) return;
    
            const value = notes.data('value');
    
            addEditableFieldgrade(notes, value, 'grade');
        }
    });
    function addEditableFieldgrade(cell, value, className) {
        const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
        
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);
        
    }
    $('#load_cpu').click('.grade-editable .grade-cancel', function(event) {
        const btnCancel = $(event.target).closest('.grade-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.grade-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel gradebtn"><i class="fa fa-edit gradepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('grade-editable');
        notesCell.addClass('grade');
    });
    $('#load_cpu').click('.grade-editable .grade-apply', function(event) {
        const btnApply = $(event.target).closest('.grade-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.grade-editable');
        const par = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.grade-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel gradebtn"><i class="fa fa-edit gradepenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('grade-editable');
        notes.addClass('grade');
    
        

        var datas = {
            'data': {'partnumber': par.text().trim(), 'stepcode': stepcode.text().trim(), 'data' : textField.val(), 'type' : 'grade'}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load").html(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////grade


    ////////////delete
    $('#load_cpu').click('.delete', function(event) {
        if(event.target.className == "delete"){
            const notes = $(event.target).closest('.delete');
    
            if (!notes) return;
    
            const par = notes.closest('tr').find('.par');
            const stepcode = notes.closest('tr').find('.stepcode');
            const value = notes.data('value');
            //window.open(`http://www.ebay.com/itm/${(notes.data('value'))}/`);
        }else if(event.target.className == "btn btn-link text-danger draftid-cancel deletebtn"){
            const notes = $(event.target).closest('.delete');
            

            if (!notes) return;
    
            const value = notes.data('value');
            const par = notes.closest('tr').find('.par');
            const stepcode = notes.closest('tr').find('.stepcode');
    
            addEditableFielddelete(notes, stepcode.text().trim(), par.text().trim(), 'delete');
        }else if(event.target.className == "fa fa-trash deletepenbtn"){
            const notes = $(event.target).closest('.delete');
            

            if (!notes) return;
    
            const value = notes.data('value');
            const par = notes.closest('tr').find('.par');
            const stepcode = notes.closest('tr').find('.stepcode');
    
            addEditableFielddelete(notes, stepcode.text().trim(), par.text().trim(), 'delete');
        }
    });
    function addEditableFielddelete(cell, stepcode, par, className) {
        
        ///console.log(stepcode)
        //console.log(par)


        const orderby = $("#orderByitem").val();
        const sortorder = $("#sortOrderitem").val();
        const partnumbertext = $("#partsearch").val();


        
        var datas = {
            'data': {'partnumber': par, 'stepcode': stepcode, 'orderby' : orderby, 'sortorder' : sortorder, 'partnumbertext' : partnumbertext}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/delete_cpu`,
            type : "post",
            data : datas,
            success : function(msg){
                $("#load_cpu").html(msg) 
                
                ///console.log(msg);
            }
        });
        /*parval = value
        const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
        btnAccept.html(
            '<i class="fa fa-check" aria-hidden="true"></i>'
        );

        const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
        btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

        $(cell).html(input).append(btnCancel).append(btnAccept);
        $(cell).removeClass(className);
        $(cell).addClass(`${className}-editable`);*/
        
    }
    $('#load_cpu').click('.par-editable .par-cancel', function(event) {
        const btnCancel = $(event.target).closest('.par-cancel');
        if (!btnCancel) return;
    
        const notesCell = btnCancel.closest('.par-editable');
        

        notesCell.html(`<button type="button" class="btn btn-link text-danger draftid-cancel parbtn"><i class="fa fa-pen parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> ${notesCell.data('value')}`);
        notesCell.removeClass('par-editable');
        notesCell.addClass('par');
    });
    $('#load_cpu').click('.par-editable .par-apply', function(event) {
        const btnApply = $(event.target).closest('.par-apply');
        if (!btnApply) return;
    
        const notes = btnApply.closest('.par-editable');
        const sku = btnApply.closest('tr').find('.par');
        const stepcode = btnApply.closest('tr').find('.stepcode');
        
    
        const textField = notes.find('.par-input');
        if (!textField) return;
        if (!textField.val()) return;
    
        notes.html(`<button type="button" class="btn btn-link text-danger draftid-cancel parbtn"><i class="fa fa-pen parpenbtn" aria-hidden="true"></i></button>
        <!--<button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>--> 
        ${textField.val()}`);
        notes.data('value', textField.val());
        notes.removeClass('par-editable');
        notes.addClass('par');
    
        

        var datas = {
            'data': {'partnumber': parval, 'stepcode': stepcode.text().trim(), 'data' :textField.val(), 'type' : "part"}
        };
        $.ajax({
            url : `${baseUrl}index.php/cpu_module/request/update_columns`,
            type : "post",
            data : datas,
            success : function(msg){
                ///$("#load").html(msg)
                console.log(msg);
            }
        });
        //window.location.href = `${baseUrl}index.php/task`;
    });
    ////////////partnumber

});
