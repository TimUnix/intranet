let baseUrl;

$(document).ready(function() {

    baseUrl = $('#photoScript').data('baseUrl');

    $.get(`${baseUrl}index.php/photo_request/load_request`,  function(response) {
        $('#photo_table_body').html(response);
		const tableWidth = $('#product_table_body').width();
		$('.scroller-content').width(tableWidth);
		$('.sticky-table').width(tableWidth);
		syncStickyHeader();
    });

    const dol = "$";
    $('#submitf').on('click',function(event){  
        console.log('data');
            var data = {
                'sku' : $("#formsku").val(),
                'price': dol.concat($("#price").val()),
                'angles': $("#angles").val(),
                'comments': $("#comments").val(),
                'location': $("#location").val(),
                ajax : '1'
            };
            console.log(data);
            $.ajax({
                url : `${baseUrl}index.php/photo_request/createrequest`,
                type : "post",
                data : data,
                success : function(response){
                    if (response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else if(response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $('#speed').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else{
                        const refreshUrl = window.location.href.split('?')[0];
                        window.location.href = refreshUrl;
                    }
                }
            
            });
    });

    $('#searchColumn').click(function(event){
        if ($('#archiveSwitch').prop('checked')){
            if($('#itemSearch').val()== ''){
                const data = { 'is_archive': 1 }
                $.post(`${baseUrl}index.php/photo_request/get_archive`, data, function(response) {
                    $('#photo_table_body').html(response);
                });
            }else{
            const data = {'searchItem' : $('#itemSearch').val().trim()}
            $.get(`${baseUrl}index.php/photo_request/search_archive`, data, function(response) {
                $('#photo_table_body').html(response);
            });}
        } else if ($('#doneSwitch').prop('checked')){
			if($('#itemSearch').val()== ''){
                const data = { 'done': 1 }
                $.post(`${baseUrl}index.php/photo_request/get_done`, data, function(response) {
                    $('#photo_table_body').html(response);
                });
            }else{
            const data = {'searchItem' : $('#itemSearch').val().trim()}
            $.get(`${baseUrl}index.php/photo_request/search_done`, data, function(response) {
                $('#photo_table_body').html(response);
            });}
		} else { 

        var data = {
        'searchItem' : $('#itemSearch').val().trim()}
       
        $.get(`${baseUrl}index.php/photo_request/search_item`,data,  function(response) {
            $('#photo_table_body').html(response);
            
        });
    }
    });

    $('#doneSwitch').on('change', function() { 
        if ($('#doneSwitch').prop('checked')){
            const data = { 'done': 1 }
            $.post(`${baseUrl}index.php/photo_request/get_done`, data, function(response) {
                $('#photo_table_body').html(response);
            });
        }else{
            load_request();
        }        
    });
   

    let listingToMarkDone = -1;

    $('#photo_table_body').on('click', '.btn-done', function(event) {
        const button = $(this).closest('.btn-done');
        
        if (!button) {
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing done.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        rowToRemove = button.closest('tr');
        const sku = rowToRemove.find('#sku').data('value');
        
        $('.sku-done').html(sku);

        if (!rowToRemove) {
            event.preventDefault();
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing done.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        listingToMarkDone = rowToRemove.find('.id').data('id');
        console.log(listingToMarkDone);
        
    });

    $('.btn-confirm-done').on('click', function() {
        $('#markListingDoneModal').modal('hide');
        const data = { 'id': listingToMarkDone };
        
        $.post(`${baseUrl}index.php/photo_request/mark_done`, data, function(response) {
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Mark Done failed',
                    body: 'Failed to mark listing done.',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Mark Done success',
                body: 'Successfully marked listing done.',
                autohide: true,
                delay: 7000
            });

            load_request();
        });
    });

    $('#photo_table_body').on('click', '.btn-undone', function(event) {
        const button = $(this).closest('.btn-undone');
        
        if (!button) {
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing undone.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        rowToRemove = button.closest('tr');
        const sku = rowToRemove.find('#sku').data('value');
        
        $('.sku-done').html(sku);

        if (!rowToRemove) {
            event.preventDefault();
            $(document).Toasts('create', {
                title: 'Done error',
                body: `An unexpected error occurred while attempting to mark listing undone.`,
                autohide: true,
                delay: 7000
            });

            return;
        }

        listingToMarkDone = rowToRemove.find('.id').data('id');
        console.log(listingToMarkDone);
    });

    $('.btn-confirm-undone').on('click', function() {
        $('#markListingUndoneModal').modal('hide');
        const data = { 'id': listingToMarkDone };
    
        $.post(`${baseUrl}index.php/photo_request/mark_undone`, data, function(response) {
            if (!response.is_success) {
                $(document).Toasts('create', {
                    title: 'Mark Undone failed',
                    body: 'Failed to mark listing undone.',
                    autohide: true,
                    delay: 7000
                });

                return;
            }

            $(document).Toasts('create', {
                title: 'Mark undone success',
                body: 'Successfully marked listing undone.',
                autohide: true,
                delay: 7000
            });
            load_request();
        });
    });

    function load_request(){
		if ($('#doneSwitch').prop('checked')){
            const data = { 'done': 1 }
            $.post(`${baseUrl}index.php/photo_request/get_done`, data, function(response) {
                $('#photo_table_body').html(response);
            });
        } else {
			$.get(`${baseUrl}index.php/photo_request/load_request`,  function(response) {
				$('#photo_table_body').html(response);
						const tableWidth = $('#photo_table_body').width();
						$('.scroller-content').width(tableWidth);   
			});
		}
	}


        let listingToArchive = -1;

        $('#photo_table_body').on('click', '.btn-archive', function(event) {
            
            const button = $(this).closest('.btn-archive');
            
            if (!button) {
                $(document).Toasts('create', {
                    title: 'Done error',
                    body: `An unexpected error occurred while attempting to mark listing done.`,
                    autohide: true,
                    delay: 7000
                });
    
                return;
            }
    
            rowToRemove = button.closest('tr');
            const sku = rowToRemove.find('#sku').data('value');
            
            $('.sku-archive').html(sku);
    
            if (!rowToRemove) {
                event.preventDefault();
                $(document).Toasts('create', {
                    title: 'Archive error',
                    body: `An unexpected error occurred while attempting to archive record.`,
                    autohide: true,
                    delay: 7000
                });
    
                return;
            }
    
            listingToArchive = rowToRemove.find('.id').data('id');
            console.log(listingToArchive);
            
        });
    
        $('.btn-confirm-archive').on('click', function() {
            $('#archiveModal').modal('hide');
            const data = { 'id': listingToArchive,
                        'notes': $("#notes").val() };
            
            $.post(`${baseUrl}index.php/photo_request/archive`, data, function(response) {
                if (!response.is_success) {
                    $(document).Toasts('create', {
                        title: 'Archive failed',
                        body: 'Failed to archive record.',
                        autohide: true,
                        delay: 7000
                    });
    
                    return;
                }
    
                $(document).Toasts('create', {
                    title: 'Archive success',
                    body: 'Successfully Archived.',
                    autohide: true,
                    delay: 7000
                });
    
                load_request();
            });
        });

        $('#archiveSwitch').on('change', function() { 
            if ($('#archiveSwitch').prop('checked')){
                if($('#itemSearch').val()== ''){
                    const data = { 'is_archive': 1 }
                    $.post(`${baseUrl}index.php/photo_request/get_archive`, data, function(response) {
                        $('#photo_table_body').html(response);
                    });
                }else{
                const data = {'searchItem' : $('#itemSearch').val().trim()}
                $.get(`${baseUrl}index.php/photo_request/search_archive`, data, function(response) {
                    $('#photo_table_body').html(response);
                });}
            }else{
                load_request();
            }        
    });
});
