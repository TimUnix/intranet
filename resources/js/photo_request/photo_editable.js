let newval
        function addEditableField(cell, value, className) {
            
            const input = $(`<input type="text" class="form-control ${className}-input" value="${value}">`);
            newval = value;
            const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
            btnAccept.html(
                '<i class="fa fa-check" aria-hidden="true"></i>'
            );

            const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
            btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

            $(cell).html(input).append(btnCancel).append(btnAccept);
            $(cell).removeClass(className);
            $(cell).addClass(`${className}-editable`);
        }

        function addEditable(cell, value, className) {
             
            const input = $(`<textarea class="form-control ${className}-input" value="${value}">${value}</textarea>`);
            newval = value;
            
            const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
            btnAccept.html(
                '<i class="fa fa-check" aria-hidden="true"></i>'
            );

            const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
            btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

            $(cell).html(input).append(btnCancel).append(btnAccept);
            $(cell).removeClass(className);
            $(cell).addClass(`${className}-editable`);
        }


        function addEditableselect(cell, value, className) {

            const input = $(`<input type="text" list="angleslist" class="form-control ${className}-input">
                                <datalist id="angleslist">
                                        <option>Front-Facing</option>
                                        <option>Sides</option>
                                        <option>Back</option>
                                        <option>Label Information</option>
                                        <option>System Specs on Monitor</option>
                                        <option>All of the Above</option>
                                        <option>Other</option>
                                </datalist>`);     
            newval = value;
            const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
            btnAccept.html(
                '<i class="fa fa-check" aria-hidden="true"></i>'
            );

            const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
            btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

            $(cell).html(input).append(btnCancel).append(btnAccept);
            $(cell).removeClass(className);
            $(cell).addClass(`${className}-editable`);
        }

        function addEditableselectyesno(cell, value, className) {

            const input = $(`<select class="form-control ${className}-input">`+
            `<option value="Yes">Yes</option>`+
            `<option value="No">No</option>`+
            `</select>`);     
            //newval = value;
   
            const btnAccept = $(`<button type="button" class="btn btn-link text-success ${className}-apply"></button>`);
            btnAccept.html(
                '<i class="fa fa-check" aria-hidden="true"></i>'
            );

            const btnCancel = $(`<button type="button" class="btn btn-link text-danger ${className}-cancel"></button>`);
            btnCancel.html('<i class="fa fa-times" aria-hidden="true"></i>');

            $(cell).html(input).append(btnCancel).append(btnAccept);
            $(cell).removeClass(className);
            $(cell).addClass(`${className}-editable`);
        }
//
        $('#photo_table_body').on('click','.sku', function(event) {
            
            if(event.target.className == "sku"){
                const notes = $(event.target).closest('.sku');

                if (!notes) return;


            }else if(event.target.className == "btn btn-link text-primary draftid-cancel skubtn"){
                const notes = $(event.target).closest('.sku');

                if (!notes) return;

                const value = notes.data('value');

                addEditableField(notes, value, 'sku');
            }else if(event.target.className == "fa fa-edit fa-sm skupenbtn"){
                const notes = $(event.target).closest('.sku');

                if (!notes) return;
A
                const value = notes.data('value');

                addEditableField(notes, value, 'sku');
            }
        });

        $('#photo_table_body').click('.sku-editable .sku-cancel', function(event) {
            const btnCancel = $(event.target).closest('.sku-cancel');
            if (!btnCancel) return;

            const notesCell = btnCancel.closest('.sku-editable');
            

            notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit skupenbtn" aria-hidden="true"></i></button>
            <!--<button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>--> ${notesCell.data('value')}`);
            notesCell.removeClass('sku-editable');
            notesCell.addClass('sku');
        });

        $('#photo_table_body').click('.sku-editable .sku-apply', function(event) {
            
            const btnApply = $(event.target).closest('.sku-apply');
            if (!btnApply) return;

            const row = btnApply.closest('.myrow');
            row.css('background-color', '#f5e48e');
            if (row.length){
                row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
            }

            const notes = btnApply.closest('.sku-editable');
            const modelnum = btnApply.closest('tr').find('.sku');
            const name = btnApply.closest('tr').find('.name');
            

            const textField = notes.find('.sku-input');
            if (!textField) return;
            if (!textField.val()) return;

            notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel skubtn"><i class="fa fa-edit fa-sm skupenbtn" aria-hidden="true"></i></button>
            <!--<button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>--> 
            ${textField.val()}`);
            notes.data('value', textField.val());
            notes.removeClass('sku-editable');
            notes.addClass('sku');
            

            var datas = {
                'data': {
                    'sku': newval, 
                    'name': name.text().trim(),
                    'data' :textField.val(), 
                    'type' : "sku"}
            };

            $.ajax({
                url : `${baseUrl}index.php/photo_request/update_columns`,
                type : "post",
                data : datas,
                success : function(msg){

                }
            });
        });

//
$('#photo_table_body').on('click','.addtolinn', function(event) {
    
    if(event.target.className == "addtolinn"){
        const notes = $(event.target).closest('.addtolinn');

        if (!notes) return;
 
  

    }else if(event.target.className == "btn btn-link text-primary draftid-cancel addtolinnbtn"){
        const notes = $(event.target).closest('.addtolinn');

        if (!notes) return;

        const value = notes.data('value');
        
        addEditableselectyesno(notes, value, 'addtolinn');
    }else if(event.target.className == "fa fa-edit fa-sm addtolinnpenbtn"){
        
        const notes = $(event.target).closest('.addtolinn');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselectyesno(notes, value, 'addtolinn');
    }
});

$('#photo_table_body').click('.addtolinn-editable .addtolinn-cancel', function(event) {
    const btnCancel = $(event.target).closest('.addtolinn-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.addtolinn-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel addtolinnbtn"><i class="fa fa-edit fa-sm addtolinnpenbtn"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('addtolinn-editable');
    notesCell.addClass('addtolinn');
});

$('#photo_table_body').click('.addtolinn-editable .addtolinn-apply', function(event) {
    
    const btnApply = $(event.target).closest('.addtolinn-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.addtolinn-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const name = btnApply.closest('tr').find('.name');
    

    const textField = notes.find('.addtolinn-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel addtolinnbtn"><i class="fa fa-edit fa-sm addtolinnpenbtn"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('addtolinn-editable');
    notes.addClass('addtolinn');

 
    var datas = {
        'data': {
            'sku': sku.text().trim(),
            'name': name.text().trim(),
            'data' :textField.val(), 
            'type' : "added_to_linnworks"}
    };

    $.ajax({
        url : `${baseUrl}index.php/photo_request/update_columns_addtolinn`,
        type : "post",
        data : datas,
        success : function(msg){

        }
    });
    setTimeout(function() {
    $.get(`${baseUrl}index.php/photo_request/load_request`,  function(response) {
        $('#photo_table_body').html(response);
                const tableWidth = $('#photo_table_body').width();
                $('.scroller-content').width(tableWidth);   
    });
    }, 2000);
});



$('#photo_table_body').on('click','.comments', function(event) {
            
    if(event.target.className == "comments"){
        const notes = $(event.target).closest('.comments');

        if (!notes) return;

        const value = notes.data('value');
    }else if(event.target.className == "btn btn-link text-primary draftid-cancel commentsbtn"){
        const notes = $(event.target).closest('.comments');

        if (!notes) return;

        const value = notes.data('value');

        addEditable(notes, value, 'comments');
    }else if(event.target.className == "fa fa-edit fa-sm commentspenbtn"){
        const notes = $(event.target).closest('.comments');

        if (!notes) return;

        const value = notes.data('value');

        addEditable(notes, value, 'comments');
    }
});

$('#photo_table_body').click('.comments-editable .comments-cancel', function(event) {
    const btnCancel = $(event.target).closest('.comments-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.comments-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel commentsbtn"><i class="fa fa-edit fa-sm commentspenbtn"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('comments-editable');
    notesCell.addClass('comments');
 
});

$('#photo_table_body').click('.comments-editable .comments-apply', function(event) {
    
    const btnApply = $(event.target).closest('.comments-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.comments-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const name = btnApply.closest('tr').find('.name');
    

    const textField = notes.find('.comments-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel commentsbtn"><i class="fa fa-edit fa-sm commentspenbtn"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('comments-editable');
    notes.addClass('comments');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(),
            'name': name.text().trim(),
            'data' :textField.val(), 
            'type' : "comments"}
    };

    $.ajax({
        url : `${baseUrl}index.php/photo_request/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
 
        }
    });
});

//


$('#photo_table_body').on('click','.angles', function(event) {
            
    if(event.target.className == "angles"){
        const notes = $(event.target).closest('.angles');

        if (!notes) return;


    }else if(event.target.className == "btn btn-link text-primary draftid-cancel anglesbtn"){
        const notes = $(event.target).closest('.angles');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselect(notes, value, 'angles');
    }else if(event.target.className == "fa fa-edit fa-sm anglespenbtn"){
        const notes = $(event.target).closest('.angles');

        if (!notes) return;

        const value = notes.data('value');

        addEditableselect(notes, value, 'angles');
    }
});

$('#photo_table_body').click('.angles-editable .angles-cancel', function(event) {
    const btnCancel = $(event.target).closest('.angles-cancel');
    if (!btnCancel) return;

    const notesCell = btnCancel.closest('.angles-editable');
    

    notesCell.html(`<button type="button" class="btn btn-link text-primary draftid-cancel anglesbtn"><i class="fa fa-edit fa-sm anglespenbtn"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> ${notesCell.data('value')}`);
    notesCell.removeClass('angles-editable');
    notesCell.addClass('angles');
});

$('#photo_table_body').click('.angles-editable .angles-apply', function(event) {
    
    const btnApply = $(event.target).closest('.angles-apply');
    if (!btnApply) return;

    const row = btnApply.closest('.myrow');
    row.css('background-color', '#f5e48e');
    if (row.length){
        row[0].scrollIntoView({behavior: 'auto',block: 'center',inline: 'center'});
    }

    const notes = btnApply.closest('.angles-editable');
    const sku = btnApply.closest('tr').find('.sku');
    const name = btnApply.closest('tr').find('.name');
    

    const textField = notes.find('.angles-input');
    if (!textField) return;
    if (!textField.val()) return;

    notes.html(`<button type="button" class="btn btn-link text-primary draftid-cancel anglesbtn"><i class="fa fa-edit fa-sm anglespenbtn"></i></button>
    <!--<button type="button" class="btn btn-link copy-field">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>--> 
    ${textField.val()}`);
    notes.data('value', textField.val());
    notes.removeClass('angles-editable');
    notes.addClass('angles');

    

    var datas = {
        'data': {
            'sku': sku.text().trim(),
            'name': name.text().trim(),
            'data' :textField.val(), 
            'type' : "angles"}
    };

    $.ajax({
        url : `${baseUrl}index.php/photo_request/update_columns`,
        type : "post",
        data : datas,
        success : function(msg){
    
        }
    });

    //

    
});