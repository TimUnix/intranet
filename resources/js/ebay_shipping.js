

$(document).ready(function() {
	const baseUrl = $('#ebayShippingScript').data('baseUrl');

	const colors = [
        '#70A1D7', '#f56954', '#F47C7C', '#F7F48B',
        '#C6F2FF', '#A1DE93', '#6343bf', '#41544e',
        '#58e82c', '#a9b547', '#87734d', '#4c9c6e',
        '#99964d', '#968a4d', '#b34774', '#3f3c4a',
        '#4d826f', '#2e2f33', '#475f63', '#6b6a49',
        '#41544c', '#4e6b49', '#de33b2', '#8a4d5e',
        '#6cb547', '#58f522', '#3f4238', '#c4b941',
        '#5ca64b', '#2aeba1', '#595e45', '#91e82c',
        '#47664d', '#4c7d64', '#5c5b44', '#313838',
        '#424d57', '#424154', '#445742', '#d63857',
        '#41544b', '#41c48c', '#a1834c', '#49616b',
        '#c7406c', '#db356b', '#7a8a4d', '#4d857e',
        '#62784c', '#704a69', '#b54768', '#3541db',
        '#4d7485', '#f02687', '#383331', '#40c7b7',
        '#c24d42', '#a2d638', '#3d353a', '#f7e120'
    ];
var canvas = document.getElementById("shipping");
var canvas_sold = document.getElementById("quantitySold");
var shipChart;
var quantitySoldChart;

	$.get(`${baseUrl}index.php/Ebay_shipping/get_shipping`,
		function(response){
      
      var today = response.shipped_today;
      var tomorrow = response.shipped_tomorrow;

			const shipChartCtx = canvas.getContext('2d');
       shipChart = new Chart(shipChartCtx, {
        type: 'doughnut',
        data: {
          labels: ['Today', 'Tomorrow'],
          datasets: [{data: [today.map(function(td){ return td.count_today_shipping}), tomorrow.map(function(tm){
              return tm.count_tomorrow_shipping})],
              backgroundColor: colors,
          }],
          options: {
            responsive: true,
            maintainAspectRatio : false,
          }
        }
    });
    canvas.onclick = function(evt) {
        var activePoints = shipChart.getElementsAtEventForMode(evt, 'nearest', {intersect: true}, true);
        if(activePoints.length){
          const firstPoint = activePoints[0];
          const label = shipChart.data.labels[firstPoint.index];
          console.log(label);
          if(label == 'Tomorrow'){
            $('#shipped_tomorrow').modal('show');
            $.get(`${baseUrl}index.php/ebay_shipping/display_shipped_tomorrow`, function(response) {
              $('#shipped_tomorrow_body').html(response);
            });
          }else if(label == 'Today'){
            $('#shipped_today').modal('show');
            $.get(`${baseUrl}index.php/ebay_shipping/display_shipped_today`, function(response) {
              $('#shipped_today_body').html(response);
            });
          }
        }
      }
  });

  $("#shipped_today_body_today").load(`${baseUrl}index.php/ebay_shipping/get_shipping_data`);
  
  $("#quantity_shipped_today_body_table").load(`${baseUrl}index.php/ebay_shipping/display_quantity_shiping`);
  
  $.get(`${baseUrl}index.php/Ebay_shipping/get_quantity`,
    function(response){
      var today = response.sold_today;
      var tomorrow = response.sold_tomorrow;

      const quantitySoldChartx = $("#quantitySold")[0].getContext('2d');

       quantitySoldChart = new Chart(quantitySoldChartx, {
        type: 'doughnut',
        data: {
          labels: ['Today', 'Tomorrow'],
          datasets: [{data: [today.map(function(td){ return td.sold_today}), tomorrow.map(function(tm){
            return tm.sold_tomorrow})],
            backgroundColor: colors,
          }],
          options: {
            responsive: true,
            maintainAspectRatio : false,

          }
        }
    }); //chart
    canvas_sold.onclick = function(evt) {
        var activePoints = quantitySoldChart.getElementsAtEventForMode(evt, 'nearest', {intersect: true}, true);
        if(activePoints.length){
          const firstPoint = activePoints[0];
          const label = quantitySoldChart.data.labels[firstPoint.index];
          console.log(label);
          if(label == 'Tomorrow'){
            $('#quantity_tomorrow').modal('show');
            $.get(`${baseUrl}index.php/ebay_shipping/display_quantity_tomorrow`, function(response) {
              $('#quantity_shipped_tomorrow_body').html(response);
           //   paginate();
            });
          }else if(label == 'Today'){
            $('#quantity_today').modal('show');
            $.get(`${baseUrl}index.php/ebay_shipping/display_quantity_today`, function(response) {
              $('#quantity_shipped_today_body').html(response);
              paginate();
            });
          }
        }
      }
  }); //function
});