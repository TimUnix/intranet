const errorSkuEmpty = 0;
let baseUrl;

let currentHeader = '';
let lastHeader = '';
let isColumnSortingReversed = false;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {

    let itemsLimit = 100;
    let itemsOffset = 0;
    let itemsRequested = false;

	baseUrl = $('#drivesScript').data('baseUrl');

    syncStickyHeader();

    $('#warranty-').hide();
    $('#shipping-').hide();
	const parameters = new URLSearchParams(window.location.search);
    const searchItem = parameters.get('search');
    const parameterSearchSelected = parameters.get('searchSelected');
    const parameterOrderBy = parameters.get('orderBy');
    const parameterSortOrder = parameters.get('sortOrder');

    	$('#searchSelected').find(':selected').prop('selected', false);
        $(`#searchSelected > option[value="${parameterSearchSelected}"]`).prop('selected', true);

        const data = {
            'searchItem': searchItem,
            'searchSelected' : parameterSearchSelected,
            'orderBy' : parameterOrderBy,
            'sortOrder': parameterSortOrder
        };

        $('#itemSearch').val(searchItem);
        const colspan = $('#product-table > thead > tr').children().length;
        $('#exportCSV').prop('disabled', true);
        $('#product_table_body').html(`<tr><td colspan="${colspan}">${spinner}</td></tr>`);
        $.get(`${baseUrl}index.php/drives/search_item`, data, function(response) {
            $('#product_table_body').html(response);
             $('#exportCSV').prop('disabled', false);
            const tableWidth = $('#product_table_body').width();
            $('.scroller-content').width(tableWidth);
            $('.sticky-table').width(tableWidth);
            syncStickyHeader();
        });

        const countUrl = `${baseUrl}index.php/drives/load_listing_count`;

        $.get(countUrl, data, function(response, _, xhr) {
        if (xhr.status === 200) {
            $('.task-count').html(response);
            syncStickyHeader();
        }
    });


    $('#searchColumn').click(function(event){
        event.preventDefault();
        var searchSelected = $('#searchSelected').val();
        const searchItem = $('#itemSearch').val();
        search();
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        search();
    });

    $('#submit').click(function(event){


        if($("#sku").val() == " " && !$("#sku").val()){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please Input SKU</h5>
                    </div>`);
            $('#sku').focus();
        }
        else if(/[Gg][Bb][Pp][Ss]/.test($('#speed').val())==false && ($('#speed').val()) !== ""){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please include unit (Gbps)</h5>
                    </div>`);
            $('#speed').focus();
        }else if(/[Hh][Gg][Ss][Tt]/.test($('#brand').val())==true || /[Ww][Dd]/.test($('#brand').val())==true 
                || /[Ss][Ee][Aa][Gg][Aa][Tt][Ee]/.test($('#brand').val())==true && ($('#bar').val()) !== "" ){
            $("#success").html(`<div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Please leave barcode as blank</h5>
                    </div>`);
            $('#bar').focus();
        }
        else{
            event.preventDefault();
            //$('#product_table_body').html(`<tr class="odd"><td valign="top" colspan="100%" class="datatables_empty">${spinner}</td></tr>`);

            var datas = {
                'sku' : $("#sku").val(),
                'bar' : $("#bar").val(),
                'upc' : $("#upc").val(),
                'brand': $("#brand").val(),
                'ff': $("#ff").val(),
                'size': $("#size").val(),
                'type': $("#type").val(),
                'rpm': $("#rpm").val(),
                'speed': $("#speed").val(),
                'interface': $("#interface").val(),
                'series': $("#series").val(),
                'con': $("#con").val(), 
                'request': $("#request").val(),
                ajax : '1'
            };
           
            $.ajax({
                url : `${baseUrl}index.php/drives/createListing`,
                type : "post",
                data : datas,
                success : function(response){
                    if (response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $('#sku').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else if(response == true){
                        $("#success").html(`<div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i>Please Input SKU</h5></div>`);

                        $('#speed').focus();
                        $("#submit").html(`<button type="button" class="btn btn-primary" id="submit">Submit</button>`);
                    }else{
                        const refreshUrl = window.location.href.split('?')[0];
                        window.location.href = refreshUrl;
                    }
                }
            });
        }

    });

    $('#orderBy').change(function(event) {
        search();
    });

    $('#sortOrder').change(function(event) {
        search();
    });

    $('#generateTemplateButton').click(function(event){
        $("#validate_template").html('');
        $('#title_template').val('');
        $('#capacity_template').val('');
        $('#interface_template').val('');
        $('#brand_template').val('');
        $('#model_template').val('');
        $('#form_factor_template').val('');
        $('#condition_template').val('');
        $('#warranty_template').val('');
        $('#description_template').val('');
        $('.html-box').hide();
        $('#generateTemplate').modal('show');
    });

    $('#preview_template_submit').click(function(event){

        var title = $('#title_template').val();
        var capacity = $('#capacity_template').val();
        var interface = $('#interface_template').val();
        var brand = $('#brand_template').val();
        var model = $('#model_template').val();
        var ff = $('#form_factor_template').val();
        var condition = $('#condition_template').val();
        var warranty = $('#warranty_template').val('');
        var description = $('#description_template').val();
       
        if(title == '' || capacity == '' || interface == '' || brand == '' || model == '' || ff == '' 
            || condition == '' || warranty == '' || description == ''){
                $("#validate_template").html(`<h5 style="color:red">Please complete the data below</h5>`);
            }else{
                $('#previewTemplate').modal('show');
     
        if(interface == 'SATA' || interface == 'SATA SSD'){
            $('.application_preview').text('Enterprise Storage | Data Center Servers');
        }else if(interface == 'SAS' || interface == 'SAS SSD'){
            $('.application_preview').text('Enterprise Storage or Home PC');
        }else if(interface == 'NVME'){
            $('.application_preview').text('Enterprise Storage | Data Center Servers');      
        }else{
            $('.application_preview').text();
        }

        $('.title_preview').text(title);
        $('.brand_preview').text(brand);
        $('.model_preview').text(model);
        $('.ff_preview').text(ff);
        $('.capacity_preview').text(capacity);
        $('.interface_preview').text(interface);
        $('.condition_preview').text(condition);
        $('.warranty_preview').text('30 Days Seller Warranty');
        $('.description_preview').text(description);

        document.getElementById("image_preview").src =`${baseUrl}resources/img/drives/${capacity}-${interface}.png`;
            }
        
    });

    $('#about_us').click(function(){
        $('#about-us').show();
        $('#warranty-').hide();
        $('#shipping-').hide();
    });
    $('#warranty').click(function(){
        $('#about-us').hide();
        $('#shipping-').hide();
        $('#warranty-').show();
    });
    $('#shipping').click(function(){
        $('#about-us').hide();
        $('#warranty-').hide();
        $('#shipping-').show();
    });
    
    $('#generate_template_submit').click(function(){
        var title = $('#title_template').val();
        var capacity = $('#capacity_template').val();
        var interface = $('#interface_template').val();
        var brand = $('#brand_template').val();
        var model = $('#model_template').val();
        var ff = $('#form_factor_template').val();
        var condition = $('#condition_template').val();
        var warranty = $('#warranty_template').val('');
        var description = $('#description_template').val();
        if(title == '' || capacity == '' || interface == '' || brand == '' || model == '' || ff == '' 
            || condition == '' || warranty == '' || description == ''){
                $("#validate_template").html(`<h5 style="color:red">Please complete the data below</h5>`);
        }else{
            $('.html-box').show();
                
                
            if(interface == 'SATA' || interface == 'SATA SSD'){
                $('.application_preview').text('Enterprise Storage | Data Center Servers');
            }else if(interface == 'SAS' || interface == 'SAS SSD'){
                $('.application_preview').text('Enterprise Storage or Home PC');
            }else if(interface == 'NVME'){
                $('.application_preview').text('Enterprise Storage | Data Center Servers');      
            }else{
                 $('.application_preview').text();
            }
            $('.title_preview').text(title);
            $('.brand_preview').text(brand);
            $('.model_preview').text(model);
            $('.ff_preview').text(ff);
            $('.capacity_preview').text(capacity);
            $('.interface_preview').text(interface);
            $('.condition_preview').text(condition);
            $('.warranty_preview').text('30 Days Seller Warranty');
            $('.description_preview').text(description);
            document.getElementById("image_preview_html").src =`https://intranet.unixsurplus.com/resources/img/drives/${capacity}-${interface}.png`;
        
            let clone = document.getElementById("whole_html").innerHTML;
        
            $('#copy_html_template').val(clone);
        }
       
    });

    $('#button_copy').click(function(){
        $("#copy_html_template").select();
        document.execCommand('copy');
    });

     $('#exportCSV').click(function() {
        const searchItem = $('#itemSearch').val();   
        const searchSelected = $('#searchSelected').val();   
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();

        const data = {
            'searchItem': searchItem,
            'orderBy': parameterOrderBy,
            'sortOrder': parameterSortOrder,
            'searchSelected' : parameterSearchSelected

        };

        const  url = `${baseUrl}index.php/drives/generate_csv`;

        $.get(url, data, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `drives_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });

    $(document).scroll(function() {
        const rowCount = $('#product_table_body > tr').length;

        // if the rows initially returned are less than 100, there are no more rows to load further
        if (rowCount < itemsLimit) return;
        const position = $(this).scrollTop();
        let maxHeight = 0;
       
        $('#product_table_body > tr').each(function() {
            maxHeight += $(this).height();
        });

        if (position > (maxHeight * 9 / 10) && !itemsRequested) {
            itemsRequested = true;
            itemsOffset += itemsLimit;

            const categories = $('.category-filter:checked').map(function() {
                return $(this).val();
            }).toArray();

            const data = {
                'searchItem': searchItem,
                'searchSelected' : parameterSearchSelected,
                'orderBy': currentHeader,
                'sortOrder': isColumnSortingReversed ? 'desc' : 'asc',
                'items_offset': itemsOffset,
                'categories' : categories
            };

            $.get(`${baseUrl}index.php/drives/search_item`, data, function(response) {
                $('#product_table_body').append(response);
                itemsRequested = false;
                syncStickyHeader();
            });
        }
    });

    function search() {
        const searchItem = $('#itemSearch').val();
        const searchSelected = $('#searchSelected').val()
        const orderBy = $('#orderBy').find(':selected').val();
        const sortOrder = $('#sortOrder').find(':selected').val();


        const route = `${baseUrl}index.php/drives?search=${searchItem}`+`&searchSelected=${searchSelected}&orderBy=${orderBy}` +`&sortOrder=${sortOrder}`;
        window.location.href = route;

       
    }

});
