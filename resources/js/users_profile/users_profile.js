let baseUrl;

$(window).on('resize', function() {
    syncStickyHeader();
});

$(document).ready(function() {
  
    baseUrl = $('#usersProfileScript').data('baseUrl');
    syncStickyHeader();


    $.get(`${baseUrl}index.php/users_profile/get_users_profile`, function(response) {
        $('#users_profile_body').html(response);
        const tableWidth = $('#users_profile').width();
        $('.scroller-content').width(tableWidth);
        $('.sticky-table').width(tableWidth);
        syncStickyHeader();
        
});
    $('#birthday').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#start_date').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#users_profile_body').on('click', '.edit-item', function(event){
        const button = $(event.target).closest('.edit-item');
        const id = button.closest('tr').find('.item-id').text().trim();
        const email = button.closest('tr').find('.item-email').text().trim();
        const picture = button.closest('tr').find('.item-picture').text().trim();
        const employee = button.closest('tr').find('.item-employee').text().trim();
        const number = button.closest('tr').find('.item-contact_number').text().trim();
        const birthday = button.closest('tr').find('.item-birthday').text().trim();
        const start = button.closest('tr').find('.item-start_date').text().trim();
        const position = button.closest('tr').find('.item-position').text().trim();
        const manager = button.closest('tr').find('.item-manager').text().trim();
        const department = button.closest('tr').find('.item-department').text().trim();
        const location = button.closest('tr').find('.item-location').text().trim();
        const address = button.closest('tr').find('.item-address').text().trim();
        const extension = button.closest('tr').find('.item-ext').text().trim();
        const days = button.closest('tr').find('.item-day_start').text().trim();
        const daye = button.closest('tr').find('.item-day_end').text().trim();
        const ein = button.closest('tr').find('.item-ein').text().trim();
       
        document.getElementById('users_picture').src=picture;
        $('#id').val(id); 
        $('#email').val(email);
        $('#ein').val(ein);
        $('#emailtext').text(email);
        $('#employee').text(employee);
        $('#contact_number').val(number);
        $('#birthday input').val(birthday);
        $('#start_date input').val(start);
        $('#position').val(position);
        $('#managers').val(manager);
        $('#departments').val(department);
        $('#locations').val(location);
        $('#ext').val(extension);
        $('#address').val(address);
        $('#daystart').val(days);
        $('#dayend').val(daye);

    });

    $('#submit').on('click', function(event){
        var data = {  
            'id' : $('#id').val(),
            'ein' : $('#ein').val(),
            'email' : $('#email').val(),          
            'contact_number' : $('#contact_number').val(),
            'birthday' :  $('#birthday input').val(),
            'start_date' :  $('#start_date input').val(),
            'position' : $('#position').val(),
            'manager' : $('#managers').val(),
            'department' : $('#departments').val(),
            'location' : $('#locations').val(),
            'extension' : $('#ext').val(),
            'address' : $('#address').val(),
            'day_start' : $('#daystart').val(),
            'day_end' : $('#dayend').val()
        };

        console.log(data);

        $.post(`${baseUrl}index.php/users_profile/update_columns`, data, function(response) {
            $('#usersModal').modal('hide');
            location.reload();
        });     

    });

    $('#submitf').on('click', function(event){
        var data = {  
           'email': $('#newemail').val()
        };

        console.log(data);

        $.post(`${baseUrl}index.php/users_profile/insert_new`, data, function(response) {
            $('#usersModal').modal('hide');
            location.reload();
        });     

    });
    
    $('#users_profile_body').on('click', '.generatePDF', function(event){
        const button = $(event.target).closest('.generatePDF');
        const id = button.closest('tr').find('.item-id').text().trim();
        const email = button.closest('tr').find('.item-email').text().trim();
        $('#form-id').val(id);
        $('#form-email').val(email);
        console.log( $('#form-id').val());
    });

    $('#exportCSV').click(function() {
		

        const  url = `${baseUrl}index.php/users_profile/generate_csv`;

        $.get(url, function(response, _, xhr) {
            if (xhr.status === 200) {
                const filename = `Users_Profile_${new Date().toLocaleDateString()}.csv`;

                const link = $('<a>');
                link.addClass('d-none');
                link.addClass('downloadCSVLink');
                link.prop('target', '_blank');
                link.prop('href', `data:text/csv;charset=utf-8,${encodeURIComponent(response)}`);
                link.prop('download', filename);

                $('body').append(link);
                link[0].click();
                link.remove();
                }
            });
    });
    $('#users_profile_body').on('click', '.deleterow', function(event){
        const button = $(event.target).closest('.deleterow');
        const email = button.closest('tr').find('.item-email').text().trim();
        $('#email').val(email);
    });

    $('#deleteuser').on('click', function(event){
        var data = { 
            'email': $('#email').val(),
        };

        console.log(data);

        $.post(`${baseUrl}index.php/users_profile/inactive_user`, data, function(response) {
            $('#confirmDelete').modal('hide');
            location.reload();
        });     

    });

    $('#searchColumn').click(function(event){
        var data = {
            'searchItem' : $('#itemSearch').val().trim()}
           
            $.get(`${baseUrl}index.php/users_profile/search_item`,data,  function(response) {
                $('#users_profile_body').html(response);
                
            });
    });

    $("#itemSearch").keypress(function(event) {
        if (event.keyCode == 13) {
            $("#searchColumn").click();
        }
    });

});

//used to design displayed data when connecting back end and front end