/*
*  Add an id of dropdownContainer to the dropdown btn-group div.
*  Add columnSelector class to each .dropdown-item div.
*  Add columnCheckbox class to each input checkbox inside the columnSelector/dropdown-item div.
*  Add class named 'header-"name of column"' to each respective table header th
*  Add class named 'title-"name of column"' to each respective table data td item field 
*/

$(document).ready(function() {
    $('#dropdownContainer .dropdown-menu').click(function(event) {
        event.stopPropagation();
    });

    $('.columnSelector').click(function() {
        const index = $('.columnSelector').index(this);
        const checkbox = $(this).find('.columnCheckbox');
        const columnName = checkbox.attr('name').toString();
        const toggleResult = !checkbox.prop('checked');
        const page = window.location.pathname.split('/').pop();

        checkbox.prop('checked', toggleResult);

        const subheader = $(`.header-${columnName}`);
              subheader.toggleClass('d-none');

        const header = subheader.data('parent-header');

        $(`.item-${columnName}`).toggleClass('d-none');
        $(`.parent-${columnName}`).toggleClass('d-none');

        const currentColspan = parseInt($(`.${header}`).attr('colspan'));
        console.log(currentColspan);

        if(!toggleResult){
            $(`.${header}`).prop('colspan', currentColspan - 1);

            if (currentColspan == 1) {
                $(`.${header}`).hide();
            }
        }else {
            $(`.${header}`).prop('colspan', currentColspan + 1);
            if (currentColspan == 0) {
                $(`.${header}`).show();
            }
        }

        const display = toggleResult ? '' : 'none';
        const data = { 'column_name': columnName, 'display': display, 'page': page };
        $.post(`${baseUrl}index.php/user/save_column_state`, data);
    });
});
