$(document).ready(function() {
    $("#product-table").on('click', '.btn-copy-row', function() {
        const copyButton = $(this);
        const copyButtonContent = copyButton.html();
        
        const colPartNumber = copyButton.closest('tr').find('.par');
        const colDescription = copyButton.closest('tr').find('.des');;
        const colCondition = copyButton.closest('tr').find('.con');
        const colQuantity = copyButton.closest('tr').find('.qty');
        const colPrice = copyButton.closest('tr').find('.price');

        
        const condition = colCondition.data('value');
        const partNumber = colPartNumber.data('value');
        const description = colDescription.data('value');
        const quantity = colQuantity.data('value');
        const price = colPrice.data('value');


        const copiedData =  "$"+ price + "  " + condition + "  " + partNumber + "  " + description +" - " + quantity + " qty " ;
      
        console.log(copiedData);
        $('.textarea-copy').val('');
        $('.textarea-copy').val(copiedData);
        
        copyButton.addClass('text-success');
        copyButton.html('<i class="fa fa-check"></i>');

        setTimeout(function() {
            copyButton.removeClass('text-success');
            copyButton.html(copyButtonContent);
        }, 2000);

        var $temp = document.createElement('input');
        document.body.append($temp);
        $temp.value = copiedData;
        $temp.select();

        var status = document.execCommand("copy");
        $temp.remove();
    });
});
