import { createApp } from 'vue';
import CollectorModule from './modules/collector/CollectorModule.vue';

createApp(CollectorModule).mount('#collectorModule');
