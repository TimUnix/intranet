<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array(
            'url', 'nav_order', 'user_access'
        ));
        $this->load->model(array(
            'inventory_model',
            'keywords_model',
            'search_history_model',
            'navigation_model',
            'google_model',
            'search_model',
            'ebay_items_model',
            'ebay_business_policies_model',
            'ebay_campaigns_model',
            'ebay_categories_model',
            'cpu_model',
            'drives_model',
            'memory_model',
            'motherboard_model',
            'task_model'
        ));
    }

    public function index() {
        $this->load->library('session');

        if ($this->session->has_userdata('user')) {
            $userdata = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($userdata['email']);
			$user_id = $get_user_id->id;
            $is_google = $userdata['is_google'];
			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $userdata['id'];
			}

            // $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'inventory');
            // $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            // if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);

            // User access is elevated when they are granted access to one or more pages.
            $is_user_access_elevated = isset($nav) && count($nav) > 0;

            $ordered_nav = organize_nav_order($nav);

            $categories = $this->inventory_model->get_categories();
            $conditions = $this->inventory_model->get_conditions();

            $picture = $userdata['picture'];

            if (is_null($userdata['picture']) || $userdata['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }
            
            $search_query = $this->input->post('search');
            $search_history = $this->search_history_model->get_search_history_for_user($g_id);
            $business_policies = array(
                'payment' => $this->ebay_business_policies_model
                ->load_policies_by_store_and_type('UNIXSurplusCom', 'payment'),
                'shipping' => $this->ebay_business_policies_model
                ->load_policies_by_store_and_type('UNIXSurplusCom', 'shipping'),
                'return' => $this->ebay_business_policies_model
                ->load_policies_by_store_and_type('UNIXSurplusCom', 'return')
            );
            $create_ad_categories = $this->ebay_categories_model->load_all();
            $campaigns = $this->ebay_campaigns_model->load_by_store('UNIXSurplusCom');
            $actions = $this->inventory_model->get_actions();

            $data['data'] = array(
                'categories' => $categories,
                'conditions' => $conditions,
                'user_picture' => $picture,
                'user_email' => $userdata['email'],
                'user_firstname' => $userdata['firstname'],
                'user_lastname' => $userdata['lastname'],
                'nav_menu' => $ordered_nav,
                'page_name' => 'Inventory',
                'is_google' => $is_google,
                'search_history' => $search_history,
                'is_user_access_elevated' => $is_user_access_elevated,
                'search_query' => $search_query,
                'business_policies' => $business_policies,
                'campaigns' => $campaigns,
                'create_ad_categories' => $create_ad_categories,
                'actions' => $actions
            );
            
            $this->load->view('modules/inventory/inventory', $data);
        } else {
            
            $data['data'] = array(
                'page_name' => 'Login'
            );
            
            $this->load->view('login', $data);
        }
    }

    function load_item_properties() {
        $sku = $this->input->post('sku');
        $category = $this->input->post('category');
        
        switch($category) {
        case 'CPU';
            $item = $this->cpu_model->load_sku($sku);
            break;
        case 'NVME':
        case 'SATA':
        case 'SAS':
        case 'SSD':
            $item = $this->drives_model->load_sku($sku);
            break;
        case 'Memory':
            $item = $this->memory_model->load_sku($sku);
        case 'Motherboard':
            $item = $this->motherboard_model->load_sku($sku);
            break;
        }

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($item));
    }

    public function find_item_number() {
        $par = $this->input->get('par');
        $store = $this->input->get('store');

        $result = $this->ebay_items_model->search_item_number($par, $store);

        if (isset($result) && $result !== '') {
            echo $result->item_number;
        } else {
            echo 'fail';
            return 'fail';
        }
    }

    function request(){
	    if ($this->uri->rsegment(3) == "load_items"){
	        self::_request_load_items();
	    }

        if ($this->uri->rsegment(3) == "load_summary") {
            self::_request_load_summary();
        }

        if ($this->uri->rsegment(3) == "save_search_history") {
            self::_request_save_search_history();
        }
    }

    function save_keyword() {
        $par = $this->input->post('part');
        $prase = $this->input->post('prase');

        $data_sess = $this->session->userdata('user');
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $fullname = $firstname.' '.$lastname;

        date_default_timezone_set('America/Los_Angeles');
        $datetime = date("Y-m-d H:i:s");

        $exist_check_result = $this->keywords_model->check_keyword_exists($par, $prase);
        $is_keyword_existing = isset($exist_check_result->keyword_par) && $exist_check_result->keyword_par >= 0;

        if ($is_keyword_existing) {
            $data = array(
                'keyword_prase' => $prase,
                'edited_by' => $fullname,
                'edited_date' => $datetime
            );
            $id = $this->keywords_model->update_keyword($data, $par);

        }else{

            $id = $this->keywords_model->save_keyword($par, $prase, $fullname, $datetime);

        }

        
        $is_save_successful = isset($id) && $id !== '' && ((int) $id) >= 0;
        $data = array(
            'data' => array(
                'is_save_successful' => $is_save_successful,
                'part' => $par
            )
        );
                
        return $this->output->set_content_type('application/json', 'utf-8')
                     ->set_output(json_encode($data));
    }

    private function _request_save_search_history() {
        $user_session = $this->session->userdata('user');
        $search = $this->input->post('search');

        if ($user_session && isset($search)) {
            $user = $this->google_model->getFields($user_session['email']);

            if ($user->id != 0) {
                $is_item_exists = $this->search_history_model
                                       ->check_users_history($user->id,
                                                             $search);
                $updated = date('Y-m-d H-i-s');
                
                if ($is_item_exists) {
                    $data = array(
                        'updated' => $updated
                    );

                    $this->search_history_model->update_search_history(
                        $data, 'user_id', $user->id, 'search', $search
                    );
                } else {
                    $data = array(
                        'user_id' => $user->id,
                        'search' => $search,
                        'updated' => $updated
                    );
                    
                    $this->search_history_model->insert_table($data);

                }
            }
        }
    }

    private function _request_load_summary() {
        $category_filters = $this->input->post('category_filters');
        $condition_filters = $this->input->post('condition_filters');
        $search = $this->input->post('search');
        $listing_filter = $this->input->post('listing_filter');
        $is_qty_zero = false;
    
        if (isset($condition_filters) && $condition_filters !== '') {
            $condition_filters = explode(',', $condition_filters);
            $qty_zero_index = array_search('Zero QTY', $condition_filters);

            if ($qty_zero_index !== false) {
                $is_qty_zero = true;
                unset($condition_filters[$qty_zero_index]);
            }

            $blank_index = array_search('Blank', $condition_filters);

            if ($blank_index !== false) {
                $condition_filters[$blank_index] = '';
            }
        }

        if (!isset($listing_filter)) {
            $listing_filter = 'all';
        }
        
        if (isset($category_filters) && $category_filters !== '') {
            $category_filters = explode(',', $category_filters);
        }
 
        $result = $this->inventory_model->get_summary(
            $search,
            $category_filters,
            $condition_filters,
            $listing_filter,
            $is_qty_zero
        );

        foreach($result as $get_sum){
            $qty = $get_sum->qty;
            $price = $get_sum->price;
            $sum_temp[] = $qty*$price;
            $sum = array_sum($sum_temp);
        }
        
        $total_count = count($result);
        
        $summary = array(
            'price' => $sum ? $sum : 0,
            'total' => $total_count
        );

        return $this->output->set_content_type('application/json')
                    ->set_output(json_encode(array( 'data' => $summary )));
    }

    private function _request_load_items() {
        $items_offset = (int) $this->input->post('items_offset');
        $category_filters = $this->input->post('category_filters');
        $condition_filters = $this->input->post('condition_filters');
        $sort_order = $this->input->post('sort_order');
        $order_by = $this->input->post('order_by');
        $search = $this->input->post('search');
        $listing_filter = $this->input->post('listing_filter');
        $is_qty_zero = false;
        $category_filters = $category_filters;
        $actions = $this->inventory_model->get_actions();

        if($order_by == 'pri'){
            $order_by = 'Price';
        }else if($order_by == 'bar'){
            $order_by = 'barcode_num';
        }else if($order_by == 'cat'){
            $order_by = 'category';
        }

        if (strpos($category_filters, '-') !== false) {
            $category_filters = str_replace("-"," & ",$category_filters);
        }

        if (isset($condition_filters) && ($condition_filters) != '') {
            $condition_filters = explode(',', $condition_filters );
            $qty_zero_index = array_search('Zero QTY', $condition_filters);

            if ($qty_zero_index !== false) {
                $is_qty_zero = true;
                unset($condition_filters[$qty_zero_index]);
            }

            $blank_index = array_search('Blank', $condition_filters);

            if ($blank_index !== false) {
                $condition_filters[$blank_index] = '';
            }
        }

        if (!isset($listing_filter)) {
            $listing_filter = '';
        }

        if (isset($category_filters) && ($category_filters) != '') {
            $category_filters = explode(',', $category_filters );
        }
     
        $filtered_items = $this->inventory_model->get_filtered_items(
            $search,
            $category_filters,
            $condition_filters,
            $listing_filter,
            $is_qty_zero,
            $sort_order,
            $order_by,
            $items_offset,
            $actions
        );
     
        $data = array('items' => $filtered_items, 'offset' => $items_offset, 'actions' => $actions);
     
        $this->load->view('modules/inventory/ajax/product_table', $data);
    }

    public function update_linn_desc(){
        $data_sess = $this->session->userdata('user');
        $fullname = $data_sess['firstname'].' '.$data_sess['lastname'];

        $sku = trim($this->input->post('sku'));
        $desc = $this->input->post('desc');
   
        $data = array('des' => $desc);
        $insert_data = array(
            'sku' => $sku, 
            'description' => $desc,
            'updated_by' => $fullname
        );
    
        $this->inventory_model->update_inventory_desc($data, $sku);
        $this->inventory_model->insert_linn_new_desc($insert_data);

    }

    public function update_linn_price(){
        $data_sess = $this->session->userdata('user');
        $fullname = $data_sess['firstname'].' '.$data_sess['lastname'];

        $sku = trim($this->input->post('sku'));
        $price = $this->input->post('price');

        $data = array('price' => $price);
        $insert_data = array(
            'sku' => $sku, 
            'price' => $price,
            'updated_by' => $fullname
        );
    
        $this->inventory_model->update_inventory_desc($data, $sku);
        $this->inventory_model->insert_linn_price($insert_data);
    }
    function drives_title() {
        $par = $this->input->post('par');
        $check_sku_drives = $this->inventory_model->check_sku($par);

        if (count($check_sku_drives) > 0){
        $result = $this->inventory_model->drives_title(
            $par
        );
        $title = $result->sku.' '.$result->bar.' '.$result->upc.' '.$result->brand.' '.$result->ff.' '.$result->size.' '.$result->type.' '.$result->rpm.' '.$result->speed.' '.$result->interface.' '.$result->series.' '.$result->ext_inf.' '.$result->con;
       
        return $this->output->set_content_type('application/json')
                    ->set_output(json_encode($title));
        }else{
            $data = array(
                'is_not_existing' => true
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }
    }

    
    public function addtask() {
        $sku = $this->input->post('par');
        $shipping = $this->input->post('shipping');
        $weight = "";
        $price_with_sign = $this->input->post('price');
        $price_replace = str_replace('$','', $price_with_sign);
        $price = trim($price_replace);
        $promotion = "";
        $volume_discount_2x = "";
        $volume_discount_3x = "";
        $volume_discount_4x = "";
        $request = $this->input->post('action');
        $banner = "";
        $notes = "";
        $photo = "";
        $moved = "";
        $lot = "";
        $exit_sku_empty = 0;
        $exit_price_empty = 1;
        $exit_sku_exists = 2;
        $exit_sku_exists_and_listing_id = "SKU already done and listed to ebay";
        $sku_successful_register = "SKU Successfully Registered";
        $sku_and_task_request_already_exist = "Sku and Task Request Already Exist";

        if ($promotion == null || trim($promotion) == '') {
            $promotion = 0;
        }

        if ($volume_discount_2x == null || trim($volume_discount_2x) == '') {
            $volume_discount_2x = 0;
        }

        if ($volume_discount_3x == null || trim($volume_discount_3x) == '') {
            $volume_discount_3x = 0;
        }

        if ($volume_discount_3x == null || trim($volume_discount_3x) == '') {
            $volume_discount_3x = 0;
        }

        if ($banner == "") {
            $banner = "NO";
        }

        if ($weight === '') {
            $weight = 0;
        }

        if (!isset($photo) || $photo === '') {
            $photo = 0;
        }

        $data_sess = $this->session->userdata('user');

        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);
        if ($sku === '') {
            return $exit_sku_empty;
        } else if (($request == 1 || $request == 3) && $price === '') {
            return $exit_price_empty;
        } else {
            $data = array(
                'date_requested' => date("Y-m-d H-i-s"),
                'request' => $request,
                'request_last_edited' => date("Y-m-d H-i-s"),
                'request_last_edited_by' => $fullname,
                'sku' => $sku,
                'photo' => $photo,
                'photo_last_edited' => date("Y-m-d H-i-s"),
                'photo_last_edited_by' => $fullname,
                'promotion' => $promotion,
                'promotion_last_edited' => date("Y-m-d H-i-s"),
                'promotion_last_edited_by' => $fullname,
                'volume_discount_2x' => $volume_discount_2x,
                'volume_discount_2x_last_edited' => date("Y-m-d H-i-s"),
                'volume_discount_2x_last_edited_by' => $fullname,
                'volume_discount_3x' => $volume_discount_3x,
                'volume_discount_3x_last_edited' => date("Y-m-d H-i-s"),
                'volume_discount_3x_last_edited_by' => $fullname,
                'volume_discount_4x' => $volume_discount_4x,
                'volume_discount_4x_last_edited' => date("Y-m-d H-i-s"),
                'volume_discount_4x_last_edited_by' => $fullname,
                'lot' => $lot,
                'lot_last_edited' => date("Y-m-d H-i-s"),
                'lot_last_edited_by' => $fullname,
                'shipping' => $shipping,
                'shipping_last_edited' => date("Y-m-d H-i-s"),
                'shipping_last_edited_by' => $fullname,
                'banner' => $banner,
                'banner_last_edited' => date("Y-m-d H-i-s"),
                'banner_last_edited_by' => $fullname,
                'request_by' => $fullname,
                'draft_id' => '0',
                'draft_by' => '',
                'listing_item_id' => '0',
                'listing_by' => ''
            );
            
            if (isset($moved) && $moved !== '') {
                $data['moved'] = $moved;
                $data['moved_last_edited'] = date("Y-m-d H-i-s");
                $data['moved_last_edited_by'] = $fullname;
            }

            if (isset($price) && $price !== '' && (float) $price > 0) {
                $data['price'] = $price;
 
                $data['price_last_edited'] = date("Y-m-d H-i-s");
                $data['price_last_edited_by'] = $fullname;
            }

            if (isset($weight) && $weight !== '' && (float) $weight > 0) {
                $data['weight'] = $weight;
                $data['weight_last_edited'] = date("Y-m-d H-i-s");
                $data['weight_last_edited_by'] = $fullname;
            }

            if (isset($notes) && $notes !== '') {
                $data['notes'] = $notes;
                $data['notes_last_edited'] = date("Y-m-d H-i-s");
                $data['notes_last_edited_by'] = $fullname;
            }

            $check_sku_and_task = $this->task_model->check_sku_and_task($sku, $request);
            $get_exclude_condition = $this->inventory_model->get_exclude_condition($request);

            foreach($get_exclude_condition as $key) {
                $get_exclude_condition = $key->exclude_condition;
            }

			if($get_exclude_condition == 0 && count($check_sku_and_task) > 0)
			{
				$data = array(
                    'is_existing' => true
                );

                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
			}
			else
			{
				$this->inventory_model->add_task($data);
                $data = array(
                    'is_success' => true
                );

                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
			}
        }
    }

    public function generate_csv(){
        $items_offset = (int)$this->input->post('items_offset');
        $category_filters = $this->input->post('category_filters');
        $condition_filters = $this->input->post('condition_filters');
        $sort_order = $this->input->post('sort_order');
        $order_by = $this->input->post('order_by');
        $search = $this->input->post('search');
        $listing_filter = $this->input->post('listing_filter');
        $is_qty_zero = false;
        $category_filters = $category_filters;
        $actions = $this->inventory_model->get_actions();

        if($order_by == 'tot'){
            $order_by = 'Total';
        }else if($order_by == 'pri'){
            $order_by = 'Price';
        }else if($order_by == 'bar'){
            $order_by = 'barcode_num';
        }else if($order_by == 'cat'){
            $order_by = 'category';
        }

        if (strpos($category_filters, '-') !== false) {
            $category_filters = str_replace("-"," & ",$category_filters);
        }

        if (isset($condition_filters) && ($condition_filters) != '') {
            $condition_filters = explode(',', $condition_filters );
            $qty_zero_index = array_search('Zero QTY', $condition_filters);

            if ($qty_zero_index !== false) {
                $is_qty_zero = true;
                unset($condition_filters[$qty_zero_index]);
            }

            $blank_index = array_search('Blank', $condition_filters);

            if ($blank_index !== false) {
                $condition_filters[$blank_index] = '';
            }
        }

        if (!isset($listing_filter)) {
            $listing_filter = '';
        }

        if (isset($category_filters) && ($category_filters) != '') {
            $category_filters = explode(',', $category_filters );
        }

        try{
            $csv = $this->inventory_model->export_csv(
                $search,
                $category_filters === '' ? null : $category_filters,
                $condition_filters === '' ? null : $condition_filters,
                $listing_filter,
                $is_qty_zero,
                $sort_order,
                $order_by   
            );

        } catch(Exception $e){
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function generate_csv_gdrive(){
        $items_offset = (int)$this->input->post('items_offset');
        $category_filters = $this->input->post('category_filters');
        $condition_filters = $this->input->post('condition_filters');
        $sort_order = $this->input->post('sort_order');
        $order_by = $this->input->post('order_by');
        $search = $this->input->post('search');
        $listing_filter = $this->input->post('listing_filter');
        $is_qty_zero = false;
        $category_filters = $category_filters;
        $actions = $this->inventory_model->get_actions();

        $accessToken = $this->input->post("accessToken");

        if($order_by == 'tot'){
            $order_by = 'Total';
        }else if($order_by == 'pri'){
            $order_by = 'Price';
        }else if($order_by == 'bar'){
            $order_by = 'barcode_num';
        }else if($order_by == 'cat'){
            $order_by = 'category';
        }

        if (strpos($category_filters, '-') !== false) {
            $category_filters = str_replace("-"," & ",$category_filters);
        }

        if (isset($condition_filters) && ($condition_filters) != '') {
            $condition_filters = explode(',', $condition_filters );
            $qty_zero_index = array_search('Zero QTY', $condition_filters);

            if ($qty_zero_index !== false) {
                $is_qty_zero = true;
                unset($condition_filters[$qty_zero_index]);
            }

            $blank_index = array_search('Blank', $condition_filters);

            if ($blank_index !== false) {
                $condition_filters[$blank_index] = '';
            }
        }

        if (!isset($listing_filter)) {
            $listing_filter = '';
        }

        if (isset($category_filters) && ($category_filters) != '') {
            $category_filters = explode(',', $category_filters );
        }

        try{
            $csv = $this->inventory_model->export_csv_gdrive(
                $search,
                $category_filters === '' ? null : $category_filters,
                $condition_filters === '' ? null : $condition_filters,
                $listing_filter,
                $is_qty_zero,
                $sort_order,
                $order_by   
            );

        } catch(Exception $e){
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        $filename = "exports/export_items".time().".csv";
        $fp = fopen($filename, 'w');

        fputcsv($fp, array_keys((array)$csv[0]));

        foreach ($csv as $fields) {
            fputcsv($fp,get_object_vars($fields));
        }

        fclose($fp);

        $apiURL = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=media'; 
        
        $file_content = file_get_contents($filename);
        $mime_type = mime_content_type($filename); 

        $ch = curl_init();         
        curl_setopt($ch, CURLOPT_URL, $apiURL);         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);         
        curl_setopt($ch, CURLOPT_POST, 1);         
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Authorization: Bearer '. $accessToken)); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $file_content); 
        $data = json_decode(curl_exec($ch), true); 
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);         
        
        if ($http_code != 200) { 
            $error_msg = 'Failed to upload file to Google Drive'; 
            if (curl_errno($ch)) { 
                $error_msg = curl_error($ch); 
            } 
            throw new Exception('Error '.$http_code.': '.$error_msg); 
        } 
        elseif($http_code == 200){
            $file_id = $data['id'];
            $file_meta = array( 
                'name' => basename($filename) 
            ); 

            $apiURL = 'https://www.googleapis.com/drive/v3/files/' . $file_id; 
         
            $ch = curl_init();         
            curl_setopt($ch, CURLOPT_URL, $apiURL);         
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);         
            curl_setopt($ch, CURLOPT_POST, 1);         
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer '. $accessToken)); 
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH'); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($file_meta)); 
            $data = json_decode(curl_exec($ch), true); 
            $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
            
        }
        
        unlink($filename);
        return json_encode(array('success' => true)); 
    }
}

?>
