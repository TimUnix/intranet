<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Time_record extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->library('fpdf/fpdf.php');
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'Time_record_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
           // $task = self::detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;
            $email = $data_sess['email'];

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'time_record');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $employees_name = $this->Time_record_model->get_all_employees();
            $get_manager = $this->Time_record_model->get_manager();
            $filter_menu = $this->Time_record_model->validate_sched_timeIn($email);


            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'  => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'all_employees' => $employees_name,
                'filter_menu' => $filter_menu,
                'managers' => $get_manager
            );

            $this->load->view('modules/time_record/index', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function load_item(){
        $filter_date = $this->input->get('filter_date');
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $get_item = $this->Time_record_model->get_item(
            $email
        ); 
        $data = array(
            'items' => $get_item
        );

        return $this->load->view('modules/time_record/ajax/own_time_table_body', $data);        
    }

    public function filter_date_employees(){
        $filter_date = $this->input->get('filter_date');

        $separate_date = explode(' - ', $filter_date);
        $min_date = $separate_date[0];
        $max_date = $separate_date[1];
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        $filter_menu = $this->Time_record_model->validate_sched_timeIn($email);

        $get_item = $this->Time_record_model->get_employees_filter_date(

            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        $data = array(
                'items' => $get_item,
                'filter' => $filter_menu
        );

        return $this->load->view('modules/time_record/ajax/time_table_body', $data);
    }



    public function load_employees(){
        $filter_employees = $this->input->get('filter_employee');
        $filter_date = $this->input->get('filter_date');

        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $filter_menu = $this->Time_record_model->validate_sched_timeIn($email);

        $filter_employee = trim($filter_employees);

        if(!$filter_date == ''){

            $separate_date = explode(' - ', $filter_date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = '';
            $max_date = '';
        }

        $get_employees = $this->Time_record_model->get_employees(
            $filter_employee,
            $min_date,
            $max_date
        );

        $data = array(
                'items' => $get_employees,
                'filter' => $filter_menu

        );

        return $this->load->view('modules/time_record/ajax/time_table_body', $data);
    }

    public function filter_employee(){
        $filter_employees = $this->input->get('filter_employee');
        $filter_date = $this->input->get('filter_date');

        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $filter_menu = $this->Time_record_model->validate_sched_timeIn($email);

        $filter_employee = trim($filter_employees);

        if(!$filter_date == ''){

            $separate_date = explode(' - ', $filter_date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }

        if($filter_employee == ''){
            $filter_employee = null;
        }
     
        $get_employees = $this->Time_record_model->filter_employee(
            $filter_employee === '' ? null : $filter_employee,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        $data = array(
            'items' => $get_employees,
             'filter' => $filter_menu
        );

        return $this->load->view('modules/time_record/ajax/time_table_body', $data);
        
        
    }

    public function filter_employee_eod(){
        $items_offset = $this->input->get('items_offset');
        $filter_employees = $this->input->get('filter_employee');
        $filter_date = $this->input->get('filter_date');

        
        $filter_employee = trim($filter_employees);

        

        if(!$filter_date == ''){

            $separate_date = explode(' - ', $filter_date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }

        if($filter_employee == ''){
            $filter_employee = null;
        }
     
        $get_eod = $this->Time_record_model->filter_employee_eod(
            $filter_employee === '' ? null : $filter_employee,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date,
            $items_offset
        ); 
        
        $data = array(
            'items_offset' => $items_offset,
            'items' => $get_eod
        );

        return $this->load->view('modules/time_record/ajax/eod_table_body', $data);
        
    }
    public function filter_date_eod(){
        $filter_date = $this->input->get('filter_date');
       

        $separate_date = explode(' - ', $filter_date);
        $min_date = $separate_date[0];
        $max_date = $separate_date[1];

        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        
        $get_eod = $this->Time_record_model->filter_date_eod(

            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        $data = array(
                'items' => $get_eod
        );
        
        return $this->load->view('modules/time_record/ajax/eod_table_body', $data);
    }
    //
    public function filter_employee_approver(){
        $filter_employees = $this->input->get('filter_employee');
        $filter_date = $this->input->get('filter_date');
        $approval_status = $this->input->get('approval_status');
        
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $filter_employee = trim($filter_employees);

        if(!$filter_date == ''){

            $separate_date = explode(' - ', $filter_date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }

        if($filter_employee == ''){
            $filter_employee = null;
        }

        if($approval_status == ''){
            $approval_status = null;
        }

        $get_employees = $this->Time_record_model->filter_employee_approval_request(
            $filter_employee === '' ? null : $filter_employee,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date,
            $approval_status === '' ? null : $approval_status,
            $email
            );
        
        $data = array(
            'items' => $get_employees
        );

        $this->load->view('modules/time_record/ajax/approver_table_body', $data);
    }
//to time in
    public function insert_timeIn(){

        date_default_timezone_set('America/Los_Angeles');
        $datetime = date("Y-m-d h:i:s A");
        $status = $this->input->post('status');
        $data_sess = $this->session->userdata('user');
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $email = $data_sess['email'];
        $fullname = $firstname.' '.$lastname;

        $separate_time = explode(' ', $datetime);
        $date= $separate_time[0];
        
        $time_in = $separate_time[1].' '.$separate_time[2];

        $get_leave_details = $this->Time_record_model->get_leave_details($email, $date);
        $validate_sched_timeIn = $this->Time_record_model->validate_sched_timeIn($email);
        if(!empty($get_leave_details)){
            foreach($get_leave_details as $half_day_leave){
                $leave_day_type = $half_day_leave->leave_day_type;
                $approved = $half_day_leave->time_request;
                $schedule_in_half_day = $half_day_leave->schedule_in;
                $status = $half_day_leave->status;

                if($status == 'Half Day Leave' && $leave_day_type == 'Half Day' && $approved == 'Approved'){
                    $schedule_in_convert = strtotime($schedule_in_half_day);
                    $thirty_min = $schedule_in_convert - (30*60);

                    $time_in_convert = strtotime($time_in);
                    $schedule_in = $schedule_in_half_day;


                    $data = array('time_in' => $time_in);
        
                    $this->Time_record_model->update_half_day_leave_time_in($data, $email, $date);
                    

                }else if($status == 'Leave' && $leave_day_type == 'Whole Day' && $approved =='Approved'){
                    $data = array(
                        'leave_whole_day_approved' => true
                    );

                    return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
                }else if($status == 'TITO' && $approved =='Approved'){
                    $data = array(
                        'tito_approved' => true
                    );

                    return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
                }else if($status == 'Time In' || $status == 'Time Out'){
                   
                    $data = array(
                        'is_error' => true
                    );

                    return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
                }
            }

        }else{

             foreach($validate_sched_timeIn as $sched_timeIn){
                $schedule_in = $sched_timeIn->schedule_in;
                $schedule_out = $sched_timeIn->schedule_out;
                $is_flexible = $sched_timeIn->is_flexible;

                $schedule_in_convert = strtotime($schedule_in);
                $thirty_min = $schedule_in_convert - (30*60);

                $time_in_convert = strtotime($time_in);

                if($schedule_in == null && $schedule_out == null && $is_flexible == '0'){
                    $data = array('no_exist_sched' => true);

                    return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
                }
                else if($is_flexible == '1'){
                    $data = array(
                        'employee' => $fullname,
                        'email' => $email,
                        'status' => $status,
                        'time_in' => $time_in,
                        'date_in' => $date,
                        'schedule_in' => 'Flexible',
                        'schedule_out' => 'Flexible'
                    );
                    $this->Time_record_model->insert_time_record($data);
                }
                else{

                    $data = array(
                        'employee' => $fullname,
                        'email' => $email,
                        'status' => $status,
                        'time_in' => $time_in,
                        'date_in' => $date,
                        'schedule_in' => $schedule_in,
                        'schedule_out' => $schedule_out
                    );
                    
                    $this->Time_record_model->insert_time_record($data);
                }
            }
        }
    }

    public function generate_csv() {
            $filter_employee = $this->input->get('filter_employee');

        try {
            $csv = $this->Time_record_model->export_csv($filter_employee === '' ? null : $filter_employee);
  
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function generate_csv_eod() {
        $filter_employee = $this->input->get('filter_employee');
        $filter_date = $this->input->get('filter_date');

        if(!$filter_date == ''){

            $separate_date = explode(' - ', $filter_date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }

        if($filter_employee == ''){
            $filter_employee = null;
        }

    try {
        $csv = $this->Time_record_model->export_csv_eod(
            $filter_employee === '' ? null : $filter_employee,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        );

    } catch (Exception $e) {
        $response = array(
            'message' => 'Something went wrong. Please contact the site admin.'
        );
        
        return $this->output->set_header(500)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response));
    }

    return $this->output->set_header(200)
                        ->set_content_type('text/csv', 'utf-8')
                        ->set_output($csv);
}

    public function own_generate_pdf(){
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $fullname = $firstname.' '.$lastname;

        $filter_date = $this->input->post('filter_date');

        if(!$filter_date == ''){

            $separate_date = explode(' - ', $filter_date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }

        try{
            $pdf_detail = $this->Time_record_model->own_generate_pdf(
                $min_date === '' ? null : $min_date,
                $max_date === '' ? null : $max_date,
                $email
            );
        }catch (Exception $e){
             $response = array(
                "is_error" => true,
                "message" => "An error occurred while attempting to print document. "
                . "Please try again in a few minutes."
            );

            return $this->output->set_header(500)
                                ->set_content_type("application/json", "utf-8")
                                ->set_output(json_encode($response));
        }

           
        $total = $this->Time_record_model->total_no_hours(
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date,
            $email
        );
        foreach($total as $total_no_hours){

            $total_hours = $total_no_hours->no_hours;

            $data = array(
                'pdf_detail' => $pdf_detail,
                'email' => $email,
                'fullname' => $fullname,
                'total_hours' => $total_hours
            );

            return $this->load->view("modules/time_record/page/print_own_time_record", $data);
        }
    }

    public function filter_own_record(){
        $filter_date = $this->input->get('filter_date');

        $separate_date = explode(' - ', $filter_date);
        $min_date = $separate_date[0];
        $max_date = $separate_date[1];

        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $get_item = $this->Time_record_model->filter_employee(
            $email === '' ? null : $email, 
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        $data = array(
                'items' => $get_item
        );

        return $this->load->view('modules/time_record/ajax/own_time_table_body', $data);
    }

    public function generate_own_PDF(){

    if (!empty($_POST['own_filter_date'])) {
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $fullname = $firstname.' '.$lastname;

        $date = $_POST['own_filter_date'];

        $pdf = new FPDF('p','mm','a4');
        $pdf->AliasNbPages();
        
        $pdf->AddPage();
        $pdf->SetFont('Arial','',10);
        $pdf->SetLeftMargin(15);
        $pdf->SetTopMargin(15);
        $pdf->Image('resources/img/thislogo.png',73,5,-100);
        $pdf->Ln();
        $pdf->SetX(80);
        $title = $email.' '.$date;
        $pdf->SetTitle($email.' '.$date);
        
        $pdf->Cell(50,40,'Time Sheet',0,0,'C');    
        $pdf->Ln();

        if(!$date == ''){

            $separate_date = explode(' - ', $date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }

        $counts = $this->Time_record_model->count_number_days($email, $min_date, $max_date);
        foreach($counts as $count){
            $number_of_days = $count->total;
        }

        $pdf->Cell(20,5,'Name:');
        $pdf->Cell(100,5,$fullname);
        $pdf->Cell(20,5,'Email:');
        $pdf->Cell(100,5,$email);
        $datetime = date("Y-m-d h:i:s A");
        $pdf->Cell(20,5,'Generated:');
        $pdf->Cell(40,5,$datetime);
        $pdf->Ln();
        $pdf->Cell(20,10,'Date:');
        $pdf->Cell(100,10,$date);

        $email = trim($email);

        $total = $this->Time_record_model->total_no_hours(
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date,
            $email
        );

        foreach($total as $total_no_hours){
            $total_hours = $total_no_hours->no_hours;
        }
        
        $pdf->Cell(35,10,'Total No. of Hours:');
        $pdf->Cell(40,10,$total_hours);
        $pdf->Ln();
        $pdf->Cell(141,5, 'No. of Days:', 0,0, 'R');
        $pdf->Cell(35,5, $number_of_days);
        $pdf->Ln();
        $pdf->SetFillColor(26, 178, 255);
        $pdf->Cell(26,10,'Status','1','0','C','true');
        $pdf->Cell(26,10,'Time In','1','0','C','true');
        $pdf->Cell(26,10,'Time Out','1','0','C','true');
        $pdf->Cell(26,10,'Date In','1','0','C','true');
        $pdf->Cell(26,10,'Schedule In','1','0','C','true');
        $pdf->Cell(26,10,'Schedule Out','1','0','C','true');
        $pdf->Cell(26,10,'No Hours','1','1','C','true');

     
        $get_employees = $this->Time_record_model->filter_employee(
            $email === '' ? null : $email,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        
        foreach($get_employees as $row){
            $status = $row->status;
            $timein = $row->time_in;
            $timeout = $row->time_out;
            $datein = $row->date_in;
            $schedulein = $row->schedule_in;
            $scheduleout = $row->schedule_out;
            $nohours = $row->no_hours;  
            $employeename = $row->employee; 
            $schedin = strtotime($row->schedule_in);
            $time_login = strtotime($row->time_in);
        if ($schedulein == "Flexible"){
            $pdf->Cell(26,10,$status,'1','0','C');
            $pdf->Cell(26,10,$timein,'1','0','C');
            $pdf->Cell(26,10,$timeout,'1','0','C');
            $pdf->Cell(26,10,$datein,'1','0','C');
            $pdf->Cell(26,10,$schedulein,'1','0','C');
            $pdf->Cell(26,10,$scheduleout,'1','0','C');
        }else{
        $pdf->Cell(26,10,$status,'1','0','C');
        if ($schedin < $time_login){
            $pdf->SetTextColor(194,8,8);
            $pdf->Cell(26,10,$timein,'1','0','C');
            $pdf->SetTextColor(0,0,0);
        }else{
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(26,10,$timein,'1','0','C');
        }
        $pdf->Cell(26,10,$timeout,'1','0','C');
        $pdf->Cell(26,10,$datein,'1','0','C');
        $pdf->Cell(26,10,$schedulein,'1','0','C');
        $pdf->Cell(26,10,$scheduleout,'1','0','C');
        }
        if ($nohours < 9){
            $pdf->SetTextColor(194,8,8);
            $pdf->Cell(26,10,$nohours,'1','1','C');
            $pdf->SetTextColor(0,0,0);
        }else{
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(26,10,$nohours,'1','1','C');
        
        }
        }   
        

        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(20,10,'I hereby certify that the above records are true and correct.');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(30,10,'Signed by:');
        $pdf->Cell(20,10,$fullname);

        $pdf->Output('D',$title.'.pdf','true');
        $pdf->Close();
        header('Content-Type: application/pdf');

        }
        


    }
    public function generate_PDF(){

        if ((!empty($_POST['filter_employee'])) && (!empty($_POST['filter_date']))) {

        $employee = $_POST['filter_employee'];
        $date = $_POST['filter_date'];
        $email = $employee;

        $pdf = new FPDF('p','mm','a4');
        $pdf->AliasNbPages();
        
        $pdf->AddPage();
        $pdf->SetFont('Arial','',10);
        $pdf->SetLeftMargin(15);
        $pdf->SetTopMargin(15);
        $pdf->Image('resources/img/thislogo.png',73,5,-100);
        $pdf->Ln();
        $pdf->SetX(80);
        $title = $employee.' '.$date;
        $pdf->SetTitle($employee.' '.$date);
        
        $pdf->Cell(50,40,'Time Sheet',0,0,'C');    
        $pdf->Ln();
        
                
        if(!$date == ''){

            $separate_date = explode(' - ', $date);
            $min_date = $separate_date[0];
            $max_date = $separate_date[1];
        }else{
            $min_date = null;
            $max_date = null;
        }
       
        $get_employees = $this->Time_record_model->filter_employee(
            $employee === '' ? null : $employee,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        
        foreach($get_employees as $row){
            $employeename = $row->employee;
        }

        $pdf->Cell(20,5,'Name:');
        $pdf->Cell(100,5,$employeename);
        $pdf->Cell(20,5,'Email:');
        $pdf->Cell(100,5,$employee);
        $datetime = date("Y-m-d h:i:s A");
        $pdf->Cell(20,5,'Generated:');
        $pdf->Cell(40,5,$datetime);
        $pdf->Ln();
        $pdf->Cell(20,10,'Date:');
        $pdf->Cell(100,10,$date);
       

        $employee = trim($employee);

        $total = $this->Time_record_model->total_no_hours(
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date,
            $email
        );

        foreach($total as $total_no_hours){
            $total_hours = $total_no_hours->no_hours;
        }

        $counts = $this->Time_record_model->count_number_days($employee, $min_date, $max_date);
        foreach($counts as $count){
            $number_of_days = $count->total;
        }
        
        $pdf->Cell(35,10,'Total No. of Hours:');
        $pdf->Cell(40,10,$total_hours);
        $pdf->Ln();
        $pdf->Cell(141,5, 'No. of Days:', 0,0, 'R');
        $pdf->Cell(35,5, $number_of_days);
        $pdf->Ln();
        $pdf->SetFillColor(26, 178, 255);
        $pdf->Cell(26,10,'Status','1','0','C','true');
        $pdf->Cell(26,10,'Time In','1','0','C','true');
        $pdf->Cell(26,10,'Time Out','1','0','C','true');
        $pdf->Cell(26,10,'Date In','1','0','C','true');
        $pdf->Cell(26,10,'Schedule In','1','0','C','true');
        $pdf->Cell(26,10,'Schedule Out','1','0','C','true');
        $pdf->Cell(26,10,'No Hours','1','1','C','true');

     
        $get_employees = $this->Time_record_model->filter_employee(
            $employee === '' ? null : $employee,
            $min_date === '' ? null : $min_date,
            $max_date === '' ? null : $max_date
        ); 
        
        foreach($get_employees as $row){
            $status = $row->status;
            $timein = $row->time_in;
            $timeout = $row->time_out;
            $datein = $row->date_in;
            $schedulein = $row->schedule_in;
            $scheduleout = $row->schedule_out;
            $nohours = $row->no_hours;  
            $employeename = $row->employee; 
            $schedin = strtotime($row->schedule_in);
            $time_login = strtotime($row->time_in);
        if ($schedulein == "Flexible"){
            $pdf->Cell(26,10,$status,'1','0','C');
            $pdf->Cell(26,10,$timein,'1','0','C');
            $pdf->Cell(26,10,$timeout,'1','0','C');
            $pdf->Cell(26,10,$datein,'1','0','C');
            $pdf->Cell(26,10,$schedulein,'1','0','C');
            $pdf->Cell(26,10,$scheduleout,'1','0','C');
        }else{
        $pdf->Cell(26,10,$status,'1','0','C');
        if ($schedin < $time_login){
            $pdf->SetTextColor(194,8,8);
            $pdf->Cell(26,10,$timein,'1','0','C');
            $pdf->SetTextColor(0,0,0);
        }else{
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(26,10,$timein,'1','0','C');
        }
        $pdf->Cell(26,10,$timeout,'1','0','C');
        $pdf->Cell(26,10,$datein,'1','0','C');
        $pdf->Cell(26,10,$schedulein,'1','0','C');
        $pdf->Cell(26,10,$scheduleout,'1','0','C');
        }
        if ($nohours < 9){
            $pdf->SetTextColor(194,8,8);
            $pdf->Cell(26,10,$nohours,'1','1','C');
            $pdf->SetTextColor(0,0,0);
        }else{
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(26,10,$nohours,'1','1','C');
        }
        }
        

        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(20,10,'I hereby certify that the above records are true and correct.');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(30,10,'Signed by:');
        $pdf->Cell(20,10,$employeename);

        $pdf->Output('D',$title.'.pdf','true');
        $pdf->Close();
        header('Content-Type: application/pdf');

        }
    }


    public function limit_time_in(){
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $get_latest_date = $this->Time_record_model->limit_time_in($email);
        $get_latest_date_out =$this->Time_record_model->limit_time_out($email);

        foreach($get_latest_date as $only_date){
            $date = $only_date->date_in;

            foreach($get_latest_date_out as $only_date_out){
               // $date_out = $only_date_out->date_out;
                $schedule_in = $only_date_out->schedule_in;
                $schedule_out = $only_date_out->schedule_out;

                $schedule_in_strtotime_check = strtotime($schedule_in);
                date_default_timezone_set('America/Los_Angeles');

                $date_out_convert_tomorrow = date("Y-m-d", strtotime('tomorrow')).' '.date("H:i:s", strtotime('+3 hours', strtotime($schedule_out)));

                $date_out_convert_today = date("Y-m-d H:i:s", strtotime('+3 hours', strtotime($schedule_out)));
                      
                if($schedule_in_strtotime_check >= strtotime('3:00:00 PM') && date('Y-m-d H:i:s') > $date_out_convert_tomorrow){
                    $date_out = true;
                }else if($schedule_in_strtotime_check < strtotime('3:00:00 PM') && date('Y-m-d H:i:s') > $date_out_convert_today){
                    $date_out = true;
                }else{
                    $date_out = false;
                }

                $data = array(
                    'latest_date' => $date,
                    'latest_date_out' => $date_out,

                );
            
                return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
            }
        }
    }

    public function insert_eod(){
        date_default_timezone_set('America/Los_Angeles');
        $datetime = date("Y-m-d h:i:s A");
        $status = $this->input->post('status');
        $finished_task = $this->input->post('finished_task');
        $challenges = $this->input->post('challenges');
        $tom_goal = $this->input->post('tom_goal');
        $data_sess = $this->session->userdata('user');
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $email = $data_sess['email'];
        $fullname = $firstname.' '.$lastname;

        $separate_time = explode(' ', $datetime);
        $date= $separate_time[0];
        
        $time_out = $separate_time[1].' '.$separate_time[2];
    
        $get_time_in = $this->Time_record_model->limit_time_in($email);
        $unaware_flexible = $this->Time_record_model->validate_sched_timeIn($email);
        if(empty($get_time_in)){
            $data = array(
                'is_error' => true
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));

        }else{
            foreach($get_time_in as $timeIn){
                $status_time_record = $timeIn->status;
                $leave_day_type = $timeIn->leave_day_type;
                $time_request = $timeIn->time_request;
                $existing_no_hours = $timeIn->no_hours;
                $existing_status = $timeIn->status;
                $date_timein = $timeIn->date_in;
                $in = $timeIn->schedule_in;
                $flexible_unaware = $timeIn->flexible_unaware;
                $no_hours = 0;
                    $time_in = $timeIn->time_in;
                    $date_in_logs = $timeIn->date_in;
                    $schedule_in = $timeIn->schedule_in_half_day;
                    $schedule_out = $timeIn->schedule_out_half_day;
    
                   
                if(($schedule_in == 'Flexible' && $schedule_out == 'Flexible') || ($flexible_unaware == '1')){
                    $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$time_in));
                    $start_date = new DateTime($convert_time_in);
                    $convert_time_out = date("Y-m-d H:i:s", strtotime($datetime));
                    $end_time = $start_date->diff(new DateTime($convert_time_out));
    
                    $pre_no_hours = round($end_time->h+($end_time->i/60), 2);
    
                    if($pre_no_hours > 9.00){
                        $no_hours = 9.00;
                    }else{
                        $no_hours = $pre_no_hours;
                    }
                }
                else{
                        
                    $schedule_in_strtotime_check = strtotime($schedule_in);
                    date_default_timezone_set('America/Los_Angeles');
                    $schedule_in_thirty_minutes_before = date("Y-m-d h:i:s A", strtotime('-30 minutes', strtotime($schedule_in)));
                    $date_now = date("Y-m-d");
                    
                    $time_in_strtotime = strtotime(date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$time_in)));
                    $schedule_in_strtotime = strtotime(date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$schedule_in)));
                    $time_out_strtotime = strtotime(date("Y-m-d H:i:s", strtotime($datetime)));
                    if($schedule_in_strtotime_check >= strtotime('3:00:00 PM') && $date_in_logs !== $date){
                        $schedule_out_strtotime = strtotime(date("Y-m-d H:i:s", strtotime($date.' '.$schedule_out)));
    
                    }else if($schedule_in_strtotime_check >= strtotime('3:00:00 PM') && $date_in_logs === $date){
    
                        $schedule_out_strtotime = strtotime(date("Y-m-d H:i:s", strtotime('tomorrow'.' '.$schedule_out)));
                    }
                    else{
                            
                        $schedule_out_strtotime = strtotime(date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$schedule_out)));
                    }
    
                    if($schedule_in_strtotime >= $time_in_strtotime && $schedule_out_strtotime <= $time_out_strtotime){
                        if ($existing_status == 'Half Day Leave'){
                            $no_hours = 4.50;
                        }else{
                        $no_hours = 9.00;}
                    }
                    else if($schedule_in_strtotime >= $time_in_strtotime && $schedule_out_strtotime > $time_out_strtotime
                        && $schedule_in_strtotime < $time_out_strtotime){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$schedule_in));
                        $start_date = new DateTime($convert_time_in);
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($datetime));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
                        
                        $no_hours = round($end_time->h+($end_time->i/60), 2);
                    }else if($schedule_in_strtotime < $time_in_strtotime && $schedule_out_strtotime > $time_out_strtotime){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$time_in));
                        $start_date = new DateTime($convert_time_in);
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($datetime));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
                            
                        $no_hours = round($end_time->h+($end_time->i/60), 2);

                    }else if($schedule_in_strtotime < $time_in_strtotime && $schedule_out_strtotime <= $time_out_strtotime){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in_logs.' '.$time_in));
                        $start_date = new DateTime($convert_time_in);
                            
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($date.' '.$schedule_out));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
    
                        $no_hours = round($end_time->h+($end_time->i/60), 2);
                    }
                }
    
                $validate_exist_timeOut = $this->Time_record_model->validate_timeOut_exist($email, $date_in_logs);
              
                $midshift_in_hours = date("Y-m-d h:i:s A", strtotime('+4 hours', strtotime($schedule_in.' '.$date_in_logs)));
                $midshift = date("Y-m-d h:i:s A", strtotime('+30 minutes', strtotime($midshift_in_hours)));
                $restricted_time = date("Y-m-d h:i:s A", strtotime('+12hour', strtotime($midshift)));
                

                if(strtotime($datetime)>= strtotime($restricted_time) && ($schedule_in !== 'Flexible') && ($flexible_unaware !== '1')){
                    $data = array(
                        'is_restricted' => true
                    );
    
                    return $this->output->set_header(200)
                                        ->set_content_type('application/json', 'utf-8')
                                        ->set_output(json_encode($data));
                } else {
                    
                    $data = array(
                        'employee' => $fullname,
                        'email' => $email,
                        'status' => $existing_status === 'Half Day Leave' ? 'Half Day Leave' : 'Time Out' ,
                        'time_out' => $time_out,
                        'date_out' => $date,
                        'no_hours' => $no_hours+$existing_no_hours,
                        'finished_task' => $finished_task,
                        'challenges' => $challenges,
                        'tom_goal' => $tom_goal,
                        'schedule_in' => $schedule_in,
                        'schedule_out' => $schedule_out
                    );

                    $this->Time_record_model->insert_eod($data, $email);
                    $this->load->view("modules/time_record/ajax/time_table_body");
                }
            }
        }
        
    }
//add logs 
    public function insert_logs(){
        $log_employee = trim($this->input->get('log_employee'));
        $email = $this->input->get('log_email');
        $log_status = $this->input->get('log_status');
        $date_in_logs = $this->input->get('date_in_logs');
        $time_logs = $this->input->get('time_logs');
        $date = $this->input->get('date_logs');
        $finished_task_logs = $this->input->get('finished_task_logs');
        $challenges_logs = $this->input->get('challenges_logs');
        $goals_logs = $this->input->get('goals_logs');

        if($log_status === 'Time In' && $log_employee !== '' && $time_logs !== '' && $date !==''){
            $validate_timeIn = $this->Time_record_model->validate_exist_timeIn($email, $date);
        
            $validate_sched_timeIn = $this->Time_record_model->validate_sched_timeIn($email);

            foreach($validate_sched_timeIn as $sched_timeIn){

                $schedule_in = $sched_timeIn->schedule_in;
                $schedule_out = $sched_timeIn->schedule_out;

                $schedule_in_convert = strtotime($schedule_in);
                $thirty_min = $schedule_in_convert - (30*60);

                $time_in_convert = strtotime($time_logs);
             
                if($schedule_in == null && $schedule_out == null){
                    $data = array('no_exist_sched' => true);

                    return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
                }
                else if(!empty($validate_timeIn)){
                    $data = array(
                        'validate_timeIn' =>true
                    );

                    return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
                }else{
                        $data = array(
                        'employee' => $log_employee,
                        'email' => $email,
                        'status' => $log_status,
                        'time_in' => $time_logs,
                        'date_in' => $date,
                        'schedule_in' => $schedule_in,
                        'schedule_out' => $schedule_out
                    );
                    

                    $this->Time_record_model->insert_time_record($data);
                    $this->load->view("modules/time_record/ajax/time_table_body");
                }

            }
        }

        else if($log_status === 'Time Out' && $log_employee !== '' && $time_logs !== '' && $date !=='' &&
            $finished_task_logs !== ' ' && $challenges_logs !== ' ' && $goals_logs !== ' '){

            $validate_timeOut = $this->Time_record_model->validate_exist_timeOut($email, $date_in_logs);

            if(empty($validate_timeOut)){
                 $data = array(
                    'validate_timeOut' =>true
                );

                return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
            }else{
                $get_time_in = $this->Time_record_model->date_modify_logs($email, $date_in_logs);
                foreach($get_time_in as $timeIn){

                    $time_in = $timeIn->time_in;
                    $date_in = $timeIn->date_in;
                    $schedule_in = $timeIn->schedule_in;
                    $schedule_out = $timeIn->schedule_out;

                    $schedule_in_strtotime_check = strtotime($schedule_in);
                    if($schedule_in_strtotime_check >= strtotime('3:00:00 PM')){
                        $time_in_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                        $schedule_in_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_in));
                        $time_out_strtotime = date("Y-m-d H:i:s", strtotime($date.' '.$time_logs));
                        $schedule_out_strtotime = date("Y-m-d H:i:s", strtotime($date.' '.$schedule_out));
                        
                    }else{
                        $time_in_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                        $schedule_in_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_in));
                        $time_out_strtotime = date("Y-m-d H:i:s", strtotime($date.' '.$time_logs));
                        $schedule_out_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_out));
                    }

                    if($schedule_in == 'Flexible' && $schedule_out == 'Flexible'){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                        $start_date = new DateTime($convert_time_in);
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($date.' '.$time_logs));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
        
                        $pre_no_hours = round($end_time->h+($end_time->i/60), 2);
        
                        if($pre_no_hours > 9.00){
                            $no_hours = 9.00;
                        }else{
                            $no_hours = $pre_no_hours;
                        }
                    }else{
                    
                    if($schedule_in_strtotime >= $time_in_strtotime && $schedule_out_strtotime <= $time_out_strtotime){
                        $no_hours = 9.00;
                        
                    }else if($schedule_in_strtotime >= $time_in_strtotime && $schedule_out_strtotime > $time_out_strtotime){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_in));
                        $start_date = new DateTime($convert_time_in);
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($date.' '.$time_logs));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
                        
                        $no_hours = round($end_time->h+($end_time->i/60), 2);
                    }else if($schedule_in_strtotime < $time_in_strtotime && $schedule_out_strtotime > $time_out_strtotime){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                        $start_date = new DateTime($convert_time_in);
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($date.' '.$time_logs));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
                      
                        $no_hours = round($end_time->h+($end_time->i/60), 2);
                    }else if($schedule_in_strtotime < $time_in_strtotime && $schedule_out_strtotime <= $time_out_strtotime){
                        $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                        $start_date = new DateTime($convert_time_in);
                        $convert_time_out = date("Y-m-d H:i:s", strtotime($date.' '.$schedule_out));
                        $end_time = $start_date->diff(new DateTime($convert_time_out));
                        
                        $no_hours = round($end_time->h+($end_time->i/60), 2);
                    }
                   
                    $data = array(
                        'employee' => $log_employee,
                        'email' => $email,
                        'status' => $log_status,
                        'time_out' => $time_logs,
                        'date_out' => $date,
                        'no_hours' => $no_hours,
                        'finished_task' => $finished_task_logs,
                        'challenges' => $challenges_logs,
                        'tom_goal' => $goals_logs

                    );

                    $this->Time_record_model->insert_time_out_logs($data, $email, $date_in_logs);
                    $this->load->view("modules/time_record/ajax/time_table_body");
                }
            }}
            
        }else{
            $data = array(
                'is_error' => true
            );

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
    }

    public function load_schedule(){
        $filter_employee = $this->input->get('filter_employee');

        $load_schedule = $this->Time_record_model->load_schedule($filter_employee); 
    
        $data = array(
                'items' => $load_schedule
        );

        return $this->load->view('modules/time_record/ajax/schedule_table_body', $data);
    }

    public function filter_schedule(){
        $email = $this->input->get('filter_employee');

        $filter_employee = $this->Time_record_model->validate_sched_timeIn($email);
        $data = array('items' => $filter_employee);

        return $this->load->view('modules/time_record/ajax/schedule_table_body', $data);
    }
//edit schedule in
    public function update_schedule_in() {
        $email = $this->input->get('email');
        $schedule_in = $this->input->get('schedule_in');
        $flexible = $this->input->get('flexible');
        

        $schedule_out_trim = date("h:i:s A", strtotime('+9 hours', strtotime($schedule_in)));
        $schedule_out = ltrim($schedule_out_trim, 0);

        if($schedule_in == '' && $flexible !== 'true'){
            $data = array('sched_not_exist' => true);

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }else if($flexible === 'true'){
             $data = array(
                'is_flexible' => '1',
                'schedule_in' => 'Flexible',
                'schedule_out' => 'Flexible'
            );
        }
        else{
             $data = array(
                'is_flexible' => '0',
                'schedule_in' => $schedule_in,
                'schedule_out' => $schedule_out
            );
           
        }
         $this->Time_record_model->update_schedule($data, $email);
    }
//edit schedule out
    public function update_schedule_out() {
        $email = $this->input->get('email');
        $schedule_out = $this->input->get('schedule_out');
        $flexible = $this->input->get('flexible');

        $schedule_in_trim = date("h:i:s A", strtotime('-9 hours', strtotime($schedule_out)));
        $schedule_in = ltrim($schedule_in_trim, 0);
        if($schedule_out == '' && $flexible === 'true'){
            $data = array('sched_not_exist' => true);

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }else if($flexible === 'true'){
             $data = array(
                'is_flexible' => '1',
                'schedule_in' => 'Flexible',
                'schedule_out' => 'Flexible'
            );
        }else{
             $data = array(
                'is_flexible' => '0',
                'schedule_out' => $schedule_out,
                'schedule_in' => $schedule_in
            );
           
       
        }  
     $this->Time_record_model->update_schedule($data, $email);
    }
//edit schedule etr
    public function update_schedule_record(){
        $id = $this->input->get('id');
        $status = $this->input->get('status');

        if($status == 'Schedule In'){
            $schedule_in = $this->input->get('schedule_in');
            $schedule_out_trim = date("h:i:s A", strtotime('+9 hours', strtotime($schedule_in)));
            $schedule_out = ltrim($schedule_out_trim, 0);
        }else if ($status == 'Schedule Out') {
             $schedule_out = $this->input->get('schedule_out');
             $schedule_in_trim = date("h:i:s A", strtotime('-9 hours', strtotime($schedule_out)));
             $schedule_in = ltrim($schedule_in_trim, 0);
        }
        

        $get_update_schedule_record = $this->Time_record_model->get_update_schedule_record($id);
        foreach($get_update_schedule_record as $record){
            $time_in = $record->time_in;
            $time_out = $record->time_out;
            $date_in = $record->date_in;
            $date_out = $record->date_out;
            $statustr = $record->status;

            $time_in_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
            $schedule_in_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_in));
            $time_out_strtotime = date("Y-m-d H:i:s", strtotime($date_out.' '.$time_out));

            if ($date_in != $date_out && strtotime($schedule_in) <= strtotime('3:00:00 PM')){
                $schedule_out_strtotime = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_out));
            }else{
            $schedule_out_strtotime = date("Y-m-d H:i:s", strtotime($date_out.' '.$schedule_out));
            }
            
            if($schedule_in_strtotime >= $time_in_strtotime && $schedule_out_strtotime <= $time_out_strtotime){
                $no_hours = 9.00;
                        
            }else if($schedule_in_strtotime > $time_out_strtotime && $schedule_in_strtotime > $time_in_strtotime){
                $no_hours = 0.00;
            }else if($schedule_out_strtotime < $time_in_strtotime && $schedule_out_strtotime < $time_out_strtotime){
                $no_hours = 0.00;
            }
            else if($schedule_in_strtotime >= $time_in_strtotime && $schedule_out_strtotime > $time_out_strtotime){
                $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$schedule_in));
                $start_date = new DateTime($convert_time_in);
                $convert_time_out = date("Y-m-d H:i:s", strtotime($date_out.' '.$time_out));
                $end_time = $start_date->diff(new DateTime($convert_time_out));
                        
                $no_hours = round($end_time->h+($end_time->i/60), 2);

            }else if($schedule_in_strtotime < $time_in_strtotime && $schedule_out_strtotime > $time_out_strtotime){
                $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                $start_date = new DateTime($convert_time_in);
                $convert_time_out = date("Y-m-d H:i:s", strtotime($date_out.' '.$time_out));
                $end_time = $start_date->diff(new DateTime($convert_time_out));
                      
                $no_hours = round($end_time->h+($end_time->i/60), 2);

            }else if($schedule_in_strtotime < $time_in_strtotime && $schedule_out_strtotime <= $time_out_strtotime){
                $convert_time_in = date("Y-m-d H:i:s", strtotime($date_in.' '.$time_in));
                $start_date = new DateTime($convert_time_in);
                $convert_time_out = date("Y-m-d H:i:s", strtotime($date_out.' '.$schedule_out));
                $end_time = $start_date->diff(new DateTime($convert_time_out));
                   
                $no_hours = round($end_time->h+($end_time->i/60), 2);
            }
            
            if ($statustr == 'Time In'){
                
                $data = array(
                    'schedule_in' => $schedule_in,
                    'schedule_out' => $schedule_out
                    
                );
            }
            elseif ($statustr == 'Time Out'){
            
            $data = array(
                'schedule_in' => $schedule_in,
                'schedule_out' => $schedule_out,
                'no_hours'=> $no_hours
            );
            }

            $this->Time_record_model->update_schedule_record($data, $id);
                        
        }

    }

    public function load_request(){
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];

        $load_request = $this->Time_record_model->load_request(
            $email
        ); 
        $data = array(
            'items' => $load_request
        );
  
        return $this->load->view('modules/time_record/ajax/request_table_body', $data);   
    }
//
    public function add_request(){
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $employee = $firstname.' '.$lastname;
        $manager = $this->input->get('manager');
        $date = $this->input->get('request_date');
        $time_request = 'Pending';
        $status = $this->input->get('status');
        $type = 'TITO';

        $validate_request_exist = $this->Time_record_model->validate_request_exist($email, $date, $type);
        $data_not_yet_exist = $this->Time_record_model->data_not_yet_exist($email, $date);
        if($date == '' || $manager == ''){
            $data = array('validate_fill_form' => true);

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
        else if(!empty($validate_request_exist)){
            $data = array('validate_request_exist' => true);

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }else if(empty($data_not_yet_exist)){
            $data = array(
                'manager' => $manager,
                'time_request' => $time_request,
                'request_date' => $date,
                'start_leave_date' => $date,
                'end_leave_date' => $date,
                'email' => $email,
                'employee' => $employee,
                'status' => $status
            );

            $this->Time_record_model->insert_time_record_requests($data);
        }
        else{
            $data = array(
                'manager' => $manager,
                'time_request' => $time_request,
                'status' => $status
            );

            //use to add request
            $this->Time_record_model->insert_time_out_logs_requests($data, $email, $date);

        }    
    }

    public function load_approver(){
        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        
        $load_approver = $this->Time_record_model->load_approver($email);

         $data = array(
            'items' => $load_approver
        );
  
        return $this->load->view('modules/time_record/ajax/approver_table_body', $data);

    }

    public function load_eod(){
        $items_offset = (int) $this->input->post('items_offset');
        $get_eod = $this->Time_record_model->get_eod($items_offset); 
    
        $data = array(
            'items_offset' => $items_offset,
                'items' => $get_eod
        );

        return $this->load->view('modules/time_record/ajax/eod_table_body', $data);

    }
    //update to 'Approved' time records request
    public function update_request_status(){
        $id = $this->input->get('id');
        $status_time_request = $this->input->get('status_time_request');
        $notes = $this->input->get('notes');

        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $employee = $firstname.' '.$lastname;

        date_default_timezone_set('America/Los_Angeles');
        $datetime = date("Y-m-d H:i:s");

        
        $get_update_schedule_record_requests = $this->Time_record_model->get_update_schedule_record_requests($id);
        foreach($get_update_schedule_record_requests as $get_email){
            $email = $get_email->email;
            $employeename = $get_email->employee;
            $time_in = $get_email->time_in;
            $time_out = $get_email->time_out;
            $status_checker = $get_email->time_request;
            $status = $get_email->status;
            $leave_day_type = $get_email->leave_day_type;
            $date_in = $get_email->request_date;
            $start_leave_date = $get_email->start_leave_date;
            $end_leave_date = $get_email->end_leave_date;
            $status = $get_email->status;
            $manager = $get_email->manager;
            $manager_note= $get_email->manager_note;
            $leave_type = $get_email->leave_type;
            $leave_reason = $get_email->leave_reason;
            $time_of_day = $get_email->time_of_day;
            $request_date = $get_email->request_date_leave;
            
            $validate_sched_timeIn = $this->Time_record_model->validate_sched_timeIn($email);
            foreach($validate_sched_timeIn as $get_sched){
                $schedule_in = $get_sched->schedule_in;
                $schedule_out = $get_sched->schedule_out;
  
                $begin = new DateTime($start_leave_date);
                $end = new DateTime($end_leave_date);
                $end = $end->modify('+1 day');

                $interval = new DateInterval('P1D');
                $period = new DatePeriod($begin, $interval ,$end);

                if($leave_day_type == 'Half Day'){
                    $no_hours = 4.50;
                }else{
                    $no_hours = 9.00;
                }
                if($status_time_request == 'Approved' && $status == 'Leave' && $leave_day_type == 'Whole Day' && $start_leave_date == $end_leave_date){
   
                    $data = array(
                        'time_request' => $status_time_request, 
                        'manager_note' => $notes, 
                        'no_hours' => $no_hours,
                        'time_in' => $schedule_in,
                        'time_out' => $schedule_out,
                        'schedule_in' => $schedule_in,
                        'schedule_out' => $schedule_out,
                        'date_in' => $start_leave_date,
                        'approved_date' => $datetime
                    );

                }else if($status_time_request == 'Approved' && $status == 'Leave' && $leave_day_type == 'Half Day'){
                    $status = 'Half Day Leave';
                    if($time_of_day == 'Morning' && $schedule_in != 'Flexible'){
                        $schedule_in_hours = date("h:i:s A", strtotime('+4 hours', strtotime($schedule_in)));
                        $schedule_in = date("h:i:s A", strtotime('+30 minutes', strtotime($schedule_in_hours)));
                    }else if($time_of_day == 'Afternoon'  && $schedule_in != 'Flexible'){
                        $schedule_out_hours = date("h:i:s A", strtotime('-4 hours', strtotime($schedule_out)));
                        $schedule_out = date("h:i:s A", strtotime('-30 minutes', strtotime($schedule_out_hours)));
                    }
       
                    $data = array(
                        'time_request' => $status_time_request,
                        'manager_note' => $notes, 
                        'schedule_in' => $schedule_in,
                        'schedule_out' => $schedule_out,
                        'date_in' => $start_leave_date,
                        'status' => $status,
                        'approved_date' => $datetime
                    );
                }
                else if($status_time_request == 'Approved' && $status == 'Leave' && $leave_day_type == 'Whole Day' && $start_leave_date != $end_leave_date){
                     $leave_dates = [];
                     $leave_dates_record = [];
                    foreach($period as $value){
                        $leave_date = $value->format('Y-m-d');
                        
                    
                        $leave_dates[] = array(
                            'employee' => $employeename,
                            'email' => $email,
                            'time_request' => $status_time_request, 
                            'manager_note' => $notes, 
                            'no_hours' => $no_hours,
                            'schedule_in' => $schedule_in,
                            'schedule_out' => $schedule_out,
                            'time_in' => $schedule_in,
                            'time_out' => $schedule_out,
                            'date_in' => $leave_date,
                            'status' => $status,
                            'request_date' => $leave_date,
                            'manager' => $manager,
                            'manager_note' => $notes,
                            'leave_day_type' => $leave_day_type,
                            'start_leave_date' => $leave_date,
                            'end_leave_date' => $leave_date,
                            'leave_type' => $leave_type,
                            'leave_reason' => $leave_reason
                        );

                          $leave_dates_record[] = array(
                            'employee' => $employeename,
                            'email' => $email,
                            'time_request' => $status_time_request, 
                            'manager_note' => $notes, 
                            'no_hours' => $no_hours,
                            'schedule_in' => $schedule_in,
                            'schedule_out' => $schedule_out,
                            'time_in' => $schedule_in,
                            'time_out' => $schedule_out,
                            'date_in' => $leave_date,
                            'status' => $status,
                            'request_date' => $leave_date,
                            'manager' => $manager,
                            'manager_note' => $notes,
                            'leave_day_type' => $leave_day_type,
                            'start_leave_date' => $leave_date,
                            'end_leave_date' => $leave_date,
                            'leave_type' => $leave_type,
                            'leave_reason' => $leave_reason,
                            'approved_date' => $datetime,
                            'request_date_leave' => $request_date
                        );
                    }

                    $this->Time_record_model->insert_many_leave($leave_dates);
                    $this->Time_record_model->insert_many_leave_records($leave_dates_record);

                    $data = array('time_request' => $status_time_request, 'approved_date' => $datetime);            
                }
                else if($status_time_request == 'Denied'){
                    $data = array(
                        'time_request' => $status_time_request, 
                        'manager_note' => $notes,
                        'approved_date' => $datetime
                    );
    
                }
      
                $this->Time_record_model->update_schedule_record_requests($data, $id);
                if($status == 'Half Day Leave'){
                    $explode_request_date = explode("-", $date_in);
                    $date_in = $explode_request_date[0]."-".$explode_request_date[1]."-".$explode_request_date[2];
                }
    
                $get_leave_details = $this->Time_record_model->get_tr_details($email, $date_in);
 
                if (!empty($get_leave_details)){
                    foreach($get_leave_details as $get_email){
                        $idnum = $get_email->id;
                        $employeename = $get_email->employee;
                        $no_hours_tr = $get_email->no_hours;
                        if($status_time_request == 'Approved' && $status == 'Half Day Leave'){
          
                            $dataup = array(
                                'time_request' => $status_time_request, 
                                'manager' => $manager, 
                                'manager_note' => $notes,
                                'request_date' => $date_in,
                                'no_hours' => $no_hours+$no_hours_tr,
                                'schedule_in' => $schedule_in,
                                'schedule_out' => $schedule_out,
                                'status'=> $status,
                                'leave_day_type' => $leave_day_type,
                                'start_leave_date' => $start_leave_date,
                                'end_leave_date' => $end_leave_date,
                                'leave_type' => $leave_type,
                                'leave_reason' => $leave_reason 
                            );
                        }else{
                            $dataup = array(
                                'time_request' => $status_time_request, 
                                'manager' => $manager, 
                                'manager_note' => $notes,
                                'request_date' => $date_in,
                                'no_hours' => $no_hours,
                                'time_in' => $schedule_in,
                                'time_out' => $schedule_out,
                                'schedule_in' => $schedule_in,
                                'schedule_out' => $schedule_out,
                                'status'=> $status,
                                'leave_day_type' => $leave_day_type,
                                'start_leave_date' => $start_leave_date,
                                'end_leave_date' => $end_leave_date,
                                'leave_type' => $leave_type,
                                'leave_reason' => $leave_reason 
                            );
                        }
                    }
                        $this->Time_record_model->update_time_record($dataup, $email, $date_in, $idnum);
                }else{
                    if($status_time_request == 'Approved' && $status == 'Half Day Leave'){

                            $dataup = array(
                                'email' => $email, 
                                'employee' => $employeename, 
                                'time_request' => $status_time_request, 
                                'manager' => $manager, 
                                'manager_note' => $notes,
                                'request_date' => $date_in,
                                'no_hours' => $no_hours,
                                'date_in' => $start_leave_date,
                                'schedule_in' => $schedule_in,
                                'schedule_out' => $schedule_out,
                                'status'=> $status,
                                'leave_day_type' => $leave_day_type,
                                'start_leave_date' => $start_leave_date,
                                'end_leave_date' => $end_leave_date,
                                'leave_type' => $leave_type,
                                'leave_reason' => $leave_reason 
                            );
                        $this->Time_record_model->insert_approved_request_to_time_record($dataup);
                        }else if ($status_time_request == 'Approved' && $status == 'Leave' && $leave_day_type == 'Whole Day' && $start_leave_date == $end_leave_date){
                            $dataup = array(
                                'email' => $email, 
                                'employee' => $employeename,
                                'status' => $status, 
                                'time_request' => $status_time_request,
                                'manager' => $manager,  
                                'manager_note' => $notes, 
                                'request_date' => $date_in,
                                'no_hours' => 9.00,
                                'time_in' => $schedule_in,
                                'time_out' => $schedule_out,
                                'schedule_in' => $schedule_in,
                                'schedule_out' => $schedule_out,
                                'date_in' => $start_leave_date,
                                'leave_day_type' => $leave_day_type,
                                'start_leave_date' => $start_leave_date,
                                'end_leave_date' => $end_leave_date,
                                'leave_type' => $leave_type,
                                'leave_reason' => $leave_reason 
                            );
                            $this->Time_record_model->insert_approved_request_to_time_record($dataup);
                        }                      
                    }
                
                foreach($period as $value){
                    $leave_date = $value->format('Y-m-d');
                    $check_leave_date = [];
                    $check_leave_date[]= $leave_date;

                    $check_many_leave_date = $this->Time_record_model->check_check($check_leave_date, $email);
                       
                    if(!empty($check_many_leave_date) && $status_time_request == 'Approved'){
                        $datas = array(
                            'time_request' => 'Replaced with Leave'
                        );
                        $this->Time_record_model->check_check_update($datas, $check_leave_date, $email);
                    }
                }

                $check_in_out_exist = $this->Time_record_model->check_in_out_exist_whole_day_leave($start_leave_date, $email);
               if(!empty($check_in_out_exist) && $status_time_request == 'Approved'){
                    $datas = array(
                        'time_request' => 'Replaced with Leave'
                    );
                    $this->Time_record_model->update_in_out_copy_whole_day_leave($datas, $start_leave_date, $email);
               }
            }
        }
    }

    public function update_my_request_status(){
        $id = $this->input->get('id');
        $manager = $this->input->get('manager');
        $date = $this->input->get('date');
        $leave_day_type = $this->input->get('leave_day_type');
        $start_leave_date = $this->input->get('start_leave_date');
        $end_leave_date = $this->input->get('end_leave_date');
        $time_of_day = $this->input->get('time_of_day');
        $leave_type = $this->input->get('leave_type');
        $leave_reason = $this->input->get('leave_reason');

        $explode_manager = explode(' ', $manager);
        $firstname = $explode_manager[0];
        $lastname = $explode_manager[1];

        $get_email_manager = $this->Time_record_model->get_email_manager($firstname, $lastname);

        foreach($get_email_manager as $email_manager){
            $email = $email_manager->email;

        $data = array(
            'manager' => $email, 
            'request_date' => $date, 
            'leave_day_type' => $leave_day_type,
            'start_leave_date' => $start_leave_date, 
            'end_leave_date' => $end_leave_date,
            'time_of_day' => $time_of_day, 
            'leave_type' => $leave_type,
            'leave_reason' => $leave_reason
        );
        $this->Time_record_model->update_schedule_record_requests($data, $id);
        
        }
    }

    function check_approved_denied_exist(){
        $email = $this->input->get('email');
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $get_update_schedule_record = $this->Time_record_model->check_approved_denied_exist($email, $start_date, $end_date);

        foreach($get_update_schedule_record as $get_email){
            $status_checker = $get_email->time_request;

            if($status_checker == 'Approved' && $status_checker !== null){
                $data = array('already_approved_denied' => true);

                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));

            }
        }
    }

    function manual_request() {
        if (!$this->session->userdata('user')) {
            $data = array(
                'is_error' => true,
                'error' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }

        $userdata = $this->session->userdata('user');
        $id = $userdata['id'];
        $user_fullname = trim($userdata['firstname']) . ' ' . trim($userdata['lastname']);
        $user_email = $userdata['email'];

        $request_type = $this->input->post('type');
        $manager = $this->input->post('manager');
            
        $date_time = new DateTime('now', new DateTimeZone('America/Los_Angeles'));
        $date = $date_time->format('Y-m-d');
        $time = $date_time->format('h:i:s A');
        
        // An open request is when an employee has clicked time-in and hasn't clicked time-out yet
        $open_requests = $this->Time_record_model->check_open_manual_requests($date, $user_email);

        if ($request_type === 'time_in' && $open_requests > 0) {
            $data = array(
                'is_error' => true,
                'error' => 'Unable to proceed. You may already have timed-in without timing-out.'
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }

        if ($request_type === 'time_out' && $open_requests === 0) {
            $data = array(
                'is_error' => true,
                'error' => 'Unable to proceed. You may not have timed-in yet.'
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }

        $status = 'Manual';
        $time_request = 'Pending';

        if ($request_type === 'time_in') {
            $data = array(
                'employee' => $user_fullname,
                'email' => $user_email,
                'status' => $status,
                'time_in' => $time,
                'date_in' => $date,
                'manager' => $manager,
                'time_request' => $time_request,
                'request_date' => "$date $time",
                'schedule_in' => 'Manual',
                'schedule_out' => 'Manual'
            );

            $this->Time_record_model->insert_time_record($data);

            return;
        }

        if ($request_type === 'time_out') {
            $last_open_request = $this->Time_record_model->load_latest_open_manual_request($date, $user_email);

            if ($last_open_request->id === null || $last_open_request->id < 0 || $last_open_request->id === '') {
                $data = array(
                    'is_error' => true,
                    'error' => 'Unable to proceed. An unexpected error occurred.'
                );

                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }

            $request_in = date('Y-m-d h:i:s A',
                                strtotime("{$last_open_request->date_in} {$last_open_request->time_in}"));
            $datetime_in = new DateTime($request_in);
            $request_out = date('Y-m-d h:i:s A', strtotime("$date $time"));
            $datetime_out = new DateTime($request_out);
            $datetime_hours = $datetime_in->diff($datetime_out);
            $no_hours = round($datetime_hours->h + ($datetime_hours->i / 60), 2);

            $data = array(
                'date_out' => $date,
                'time_out' => $time,
                'no_hours' => $no_hours,
                'request_date' => "$request_in - $request_out"
            );

            $this->Time_record_model->update_manual_request($last_open_request->id, $data);
        }
    }

    function manual_requests() {
        if (!$this->session->userdata('user')) {
            $data = array(
                'is_error' => true,
                'error' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }

        $userdata = $this->session->userdata('user');
        $user_email = $userdata['email'];
        
        $date_filter = $this->input->post('date');
        $employee_filter = $this->input->post('employee');

        $requests = $this->Time_record_model->manual_requests($user_email, $employee_filter, $date_filter);
        $data = array( 'requests' => $requests );

        return $this->load->view('modules/time_record/ajax/manual_requests', $data);
    }

    function record_manual_approval() {
        $employee = $this->input->post('employee');
        $date_in = $this->input->post('date_in');
        $time_in = $this->input->post('time_in');
        $date_out = $this->input->post('date_out');
        $time_out = $this->input->post('time_out');
        $notes = $this->input->post('notes');
        $time_request = $this->input->post('time_request');

        $filters = array(
            'email' => $employee,
            'date_in' => $date_in,
            'time_in' => $time_in,
            'date_out' => $date_out,
            'time_out' => $time_out
        );

        $data = array(
            'time_request' => $time_request,
            'manager_note' => $notes === '' ? null : $notes
        );
        
        try {
            $this->Time_record_model->record_manual_approval($filters, $data);
        } catch (Exception $e) {
            $data = array(
                'is_error' => true,
                'error' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }
    }

    function add_leave(){
        $chosen_manager_leave = $this->input->get('chosen_manager_leave');
        $leave_day_type = $this->input->get('leave_day_type');
        $start_leave_date = $this->input->get('start_leave_date');
        $end_leave_date = $this->input->get('end_leave_date');
        $time_of_day = $this->input->get('time_of_day');
        $leave_type = $this->input->get('leave_type');
        $leave_reason = $this->input->get('leave_reason');

        $data_sess = $this->session->userdata('user');
        $email = $data_sess['email'];
        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $employee = $firstname.' '.$lastname;

        $explode_start_leave_date = explode(' ', $start_leave_date);
        $start_date_leave = $explode_start_leave_date[0];
        $date = $start_date_leave;
        $type = 'Leave';

        date_default_timezone_set('America/Los_Angeles');
        $datetime = date("Y-m-d H:i:s");

         $validate_request_exist = $this->Time_record_model->validate_request_exist($email, $date, $type);

        if($end_leave_date == ''){
            $end_leave_date = $start_leave_date;
        }

        if($chosen_manager_leave== '' || $leave_day_type == '' || $start_leave_date == '' || $leave_type == '' || $leave_reason == ''){
            $data = array('need_input_all' => true);

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }else if(!empty($validate_request_exist)){
            $data = array('validate_request_exist' => true);

            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }else{
            $get_schedule = $this->Time_record_model->validate_sched_timeIn($email);
            foreach($get_schedule as $schedule){
                $schedule_in = $schedule->schedule_in;
                $schedule_out = $schedule->schedule_out;

                $schedule_in_date = date("Y-m-d h:i:s A", strtotime($start_date_leave.' '.$schedule_in));
                if(strtotime($schedule_in) > strtotime($schedule_out)){
                    $tomorrow_date = date($start_date_leave, strtotime('tomorrow'));
                    $schedule_out_date = date("Y-m-d h:i:s A", strtotime($tomorrow_date.' '.$schedule_out));
                }else{
                    $schedule_out_date = date("Y-m-d h:i:s A", strtotime($start_date_leave.' '.$schedule_out));
                }

                if($end_leave_date != null){
                    $request_date = $start_leave_date.'-'.$end_leave_date;
                }else{
                    $request_date = $start_leave_date;
                }
                $start_leave_date_strtotime = strtotime($start_leave_date);
                $end_leave_date_strtotime = strtotime($end_leave_date);
                $schedule_in_strtotime = strtotime($schedule_in_date);
                $schedule_out_strtotime = strtotime($schedule_out_date);

                    $data = array(
                        'time_request' => 'Pending',
                        'status' => 'Leave',
                        'leave_day_type' => $leave_day_type,
                        'start_leave_date' => $start_leave_date,
                        'end_leave_date' => $end_leave_date,
                        'time_of_day' => $time_of_day,
                        'leave_type' => $leave_type,
                        'leave_reason' => $leave_reason,
                        'employee' => $employee,
                        'email' => $email,
                        'manager' => $chosen_manager_leave,
                        'request_date' => $request_date,
                        'request_date_leave' => $datetime
                    );
                    $this->Time_record_model->insert_time_record_requests($data);
                
            }
        }
    }

    public function edit_leave(){
        $id = $this->input->get('id');
        $chosen_manager_leave = $this->input->get('chosen_manager_leave');
        $leave_day_type = $this->input->get('leave_day_type');
        $start_leave_date = $this->input->get('start_leave_date');
        $end_leave_date = $this->input->get('end_leave_date');
        $time_of_day = $this->input->get('time_of_day');
        $leave_type = $this->input->get('leave_type');
        $leave_reason = $this->input->get('leave_reason');

        date_default_timezone_set('America/Los_Angeles');
        $datetime = date("Y-m-d H:i:s");

        if($end_leave_date != '' || $end_leave_date != null){
            $request_date = $start_leave_date.'-'.$end_leave_date;
        }else{
            $request_date = $start_leave_date;
        }

        $explode_manager = explode(' ', $chosen_manager_leave);
        $firstname = $explode_manager[0];
        $lastname = $explode_manager[1];

        $get_email_manager = $this->Time_record_model->get_email_manager($firstname, $lastname);

        foreach($get_email_manager as $email_manager){
            $email = $email_manager->email;

            $data = array(
                'manager' => $email,
                'leave_day_type' => $leave_day_type,
                'start_leave_date' => $start_leave_date,
                'end_leave_date' => $end_leave_date,
                'time_of_day' => $time_of_day,
                'leave_type' => $leave_type,
                'leave_reason' => $leave_reason,
                'request_date' => $request_date,
                'request_date_leave' => $datetime
            );

            $this->Time_record_model->update_schedule_record_requests($data, $id);
        }
    }
}

?>
