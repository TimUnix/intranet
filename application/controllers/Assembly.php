<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assembly extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'assembly_model',
            'chassis_model',
            'work_orders_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
           // $task = self::detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'assembly');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $year = date('Y');
            $last_twoDigit_year = substr($year, -2);
            $day = date('z');
            $days = $day+1;
            $start = 0;
            $start2 = 1;
            $starting = $start.''.$start2;
            $year_date = $last_twoDigit_year.''.$days;
            $combination = $last_twoDigit_year.''.$days.''.$starting;
  

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);


            $columns = $this->users_model->get_table_columns('assembly', $g_id);
            $mounting = $this->assembly_model->mounting();
            $warranty = $this->assembly_model->warranty();
            $chassis = $this->assembly_model->chassis();
            $drives = $this->assembly_model->drives();
            $motherboard = $this->assembly_model->motherboard();
            $cpu = $this->assembly_model->cpu();
            $memory = $this->assembly_model->memory();
            $get_build_id = $this->assembly_model->get_buildID();

            foreach($get_build_id as $b){
                $d = $b->build_id;

                break;
        
            }

            if(isset($d) == true){
                $d = $d;
            }else{
                $d = "";
            }

             $pos = strpos($d, $year_date);
    
                if($pos !== false){

                    $a = $d+1;
                
                }
                else{
                    $a = $combination+1;
                }

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'  => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'columns' => json_decode($columns->assembly_columns),
                'mountings' => $mounting,
                'warranty' => $warranty,
                'chassis' => $chassis,
                'drives' => $drives,
                'motherboard' => $motherboard,
                'cpu' => $cpu,
                'memory' => $memory,
                'build_id' => $a

            );

            $this->load->view('modules/assembly/index', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function search_item() {
    	$user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
        	$items = $this->assembly_model->get(
        	$searchItem,
        	$searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
        	);
        }catch (Exception $e){
        	 $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
            $columns = $this->users_model->get_table_columns('assembly', $user['id']);
            $table_columns = json_decode($columns->assembly_columns);
            
            $data = array(
                'items' => $items,
                'columns' => $table_columns
            );
            return $this->load->view('modules/assembly/product_table_body', $data);
 
        
    }

    public function load_listing_count() {
        $user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
            $items = $this->assembly_model->count(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
        }catch (Exception $e){
             $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

            return $this->output->set_header(200)
                        ->set_content_type('text/html', 'utf-8')
                        ->set_output($items->listing_count);
    }

     public function generate_csv() {
        $searchItem = $this->input->get('searchItem');
        $sort_order = $this->input->get('sortOrder');
        $order_by = $this->input->get('orderBy');
        $searchSelected = $this->input->get('searchSelected');


        try {
            $csv = $this->assembly_model->export_csv(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
  
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function get_obnic_pcieslot(){
        $motherboard = $this->input->post('motherboard');
        $items = $this->assembly_model->get_obnic_pcieslot($motherboard);

        foreach($items as $item){
            $ob_nic = $item->ob_nic;
            $pcie_slot = $item->pci_count;

            $data = array(
            'ob_nic' => $ob_nic,
            'pcie_slot' => $pcie_slot
            );
        }

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));

    }

    public function chassis_detail(){
        $part_number = $this->input->post('chassis');
        $items = $this->chassis_model->search_chassis($part_number);

        
        foreach($items as $item){
            $chassis = $item->part_number;
            $mfr = $item->mfr;
            $backplane = $item->backplane;
            $drivebays = $item->drivebays;
            $rearbays = $item->rearbays;
            $node = $item->node;
            $psu = $item->psu;
            $bin = $item->bin;
            $drivebays = $item->drivebays;
            $rearbays = $item->rearbays;
            $dbays = $item->ff;
            $rbays = $item->rff;
            
            $drivebays_dbays = $drivebays." ".$dbays;
            $rearbays_rbays = $rearbays." ".$rbays;

             $data = array(
            'status' => 200,
            'message' => 'Success',
            'part_number' => $chassis,
            'mfr' => $mfr,
            'backplane' => $backplane,
            'drivebays' => $drivebays_dbays,
            'rearbays' => $rearbays_rbays,
            'node' => $node,
            'psu' => $psu,
            'bin' => $bin,
            'sample' => $this->assembly_model->chassis_detail($chassis),
            'c' => $this->assembly_model->chassis(),
           'dbays' => $dbays,
           'rbays' => $rbays
        );
            
        }
       
        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));


    }

    public function motherboard_for_chassis(){
        $chassis = $this->input->post('chassis');
      
        $search_chassis = $this->assembly_model->chassis_detail($chassis);


         if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        if(empty($motherboard)){
            $items['motherboard'] = "";
        }else{
            $items['motherboard'] = $this->assembly_model->motherboard_for_chassis($motherboard);
        }
        
         $this->load->view('modules/assembly/ajax/select_motherboard', $items);

    }

    public function obnic_pcislot_motherboard(){
        $chassis = $this->input->post('chassis');
        $search_chassis = $this->assembly_model->chassis_detail($chassis);

        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        if(empty($motherboard)){
            $motherboard_obnic_pci = "";
            $data = "";
        }else{
            $motherboard_obnic_pci = $this->assembly_model->motherboard_for_chassis($motherboard);
            foreach($motherboard_obnic_pci as $motherboard){
                $ob_nic = $motherboard->ob_nic;
                $pcie_slot = $motherboard->pci_count;

                $data = array(
                'ob_nic' => $ob_nic,
                'pcie_slot' => $pcie_slot
                );
            }
            $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            
        }

        
    }

    public function cpu_for_motherboard(){
        $chassis = $this->input->post('chassis');
      
        $search_chassis = $this->assembly_model->chassis_detail($chassis);
        
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        if(empty($motherboard)){
            $motherboard_chassis = "";
        }else{
            $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
            foreach($motherboard_chassis as $motherboard){
                $motherboard_qty_cpu = $motherboard->qty_cpu;
                break;
            }
        }
        
        
        if(isset($motherboard_qty_cpu)){
            $motherboard_qty_cpu = $motherboard_qty_cpu;
        }else{$motherboard_qty_cpu = null;}
        $cpu_fam_ = $this->assembly_model->cpu_for_motherboard($motherboard_qty_cpu);
        if($cpu_fam_){
            foreach($cpu_fam_ as $cpu1){
                $cpu_fam =  $cpu1->cpu_family;
                break;
            }
            if($cpu_fam != ""){$cpu = $this->work_orders_model->get_cpu_fam($cpu_fam);}else{$cpu = null;}
            
        }
        if(empty($cpu)){
            $cpu = null;
        }
        $search_cpu['cpu'] = $cpu;

        $this->load->view('modules/assembly/ajax/select_cpu', $search_cpu);
    }

    public function get_cpu(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu'] = $this->assembly_model->cpu_for_motherboard($qty_cpu);
        var_dump($qty_cpu);
        var_dump($get_cpu_motherboard);
        $this->load->view('modules/assembly/ajax/select_cpu', $get_cpu_motherboard);
    }

    public function cpu_for_motherboard2(){
        $motherboard = $this->input->post('motherboard');
        $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
            break;
        }
        if(empty($motherboard_qty_cpu)){
            $motherboard_qty_cpu = "";
        }else{
            $cpu_fam_ = $this->assembly_model->cpu_for_motherboard($motherboard_qty_cpu);
            if($cpu_fam_){
                foreach($cpu_fam_ as $cpu1){
                    $cpu_fam =  $cpu1->cpu_family;
                    break;
                }
                if($cpu_fam != ""){
                    $cpu = $this->work_orders_model->get_cpu_fam($cpu_fam);
                }else{
                    $cpu = null;
                }
                
            }
        }
        
        
        if(empty($cpu)){
            $cpu = null;
        }
        $search_cpu['cpu2'] = $cpu;
        $this->load->view('modules/assembly/ajax/select_cpu2', $search_cpu);
    }

    public function get_cpu2(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu2'] = $this->assembly_model->cpu_for_motherboard($qty_cpu);
        $this->load->view('modules/assembly/ajax/select_cpu', $get_cpu_motherboard);
    }

    public function memory_for_motherboard(){
        $chassis = $this->input->post('chassis');
      
        $search_chassis = $this->assembly_model->chassis_detail($chassis);
        

         if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }


        if(empty($motherboard)){
            $motherboard_chassis = "";
        }else{
            $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
            foreach($motherboard_chassis as $motherboard){
                $memory = $this->assembly_model->memory_for_motherboard($motherboard->memory_family);
            }
        }

        
        
        if(empty($memory)){
            $memory = null;
        }
        $search_memory['memory'] = $memory;
        $this->load->view('modules/assembly/ajax/select_memory', $search_memory);
    }

  /*  public function get_memory(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->assembly_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
            }
        }
        $get_memory['memory'] = $this->assembly_model->memory_for_motherboard($memory_family);
        
        
        $this->load->view('modules/assembly/ajax/select_memory', $get_memory);
    }*/

     public function memory_for_motherboard2(){
        $motherboard = $this->input->post('motherboard');
      
        $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $memory = $this->assembly_model->memory_for_motherboard($motherboard->memory_family);
        }
        if(empty($memory)){
            $memory = null;
        }
        $search_memory['memory2'] = $memory;
        $this->load->view('modules/assembly/ajax/select_memory2', $search_memory);
    }

    public function get_memory2(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->assembly_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
            }
        }
        $get_memory['memory2'] = $this->assembly_model->memory_for_motherboard($memory_family);
        
        
        $this->load->view('modules/assembly/ajax/select_memory2', $get_memory);
    }

    public function motherboard_for_chassis_editCH(){
        $chassis = $this->input->post('chassis');
      
        $search_chassis = $this->assembly_model->chassis_detail($chassis);
        

         if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }

        $items['motherboard'] = $this->assembly_model->motherboard_for_chassis($motherboard);
        $this->load->view('modules/assembly/ajax/select_motherboard_edit', $items);
    }

    public function cpu_for_motherboard_editCH(){
        $chassis = $this->input->post('chassis');
      
        $search_chassis = $this->assembly_model->chassis_detail($chassis);
        

         if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
        }
        if(empty($motherboard_qty_cpu)){
            $motherboard_qty_cpu = "";
        }
        $cpu_fam = $this->assembly_model->cpu_for_motherboard($motherboard_qty_cpu);
        if($cpu_fam){
            foreach($cpu_fam as $cpu_fam_data){
                $cpu_family = $cpu_fam_data->cpu_family;
                break;
            }
        }
        if(empty($cpu_family)){$cpu_family = "";}
        if($cpu_family != ""){$cpu = $this->work_orders_model->get_cpu_fam($cpu_family);}else{$cpu = null;}
        if(empty($cpu)){
            $cpu = null;
        }
        $search_cpu['cpu'] = $cpu;
        $this->load->view('modules/assembly/ajax/select_cpu_edit', $search_cpu);
    }

    public function get_cpu_editCH(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu'] = $this->assembly_model->cpu_for_motherboard($qty_cpu);
        $this->load->view('modules/assembly/ajax/select_cpu_edit', $get_cpu_motherboard);
    }

    public function memory_for_motherboard_editCH(){
        $chassis = $this->input->post('chassis');
      
        $search_chassis = $this->assembly_model->chassis_detail($chassis);
        

         if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $memory = $this->assembly_model->memory_for_motherboard($motherboard->memory_family);
        }
        if(empty($memory)){
            $memory = null;
        }
        $search_memory['memory'] = $memory;
        $this->load->view('modules/assembly/ajax/select_memory_edit', $search_memory);
    }

    public function get_memory_editCh(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->assembly_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
            }
        }
        $get_memory['memory'] = $this->assembly_model->memory_for_motherboard($memory_family);
        
        
        $this->load->view('modules/assembly/ajax/select_memory_edit', $get_memory);
    }

    public function cpu_for_motherboard_editMB(){
        $motherboard = $this->input->post('motherboard');
        $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
        }


        if(empty($motherboard_qty_cpu)){
            $motherboard_qty_cpu = "";
        }
        $cpu_fam = $this->assembly_model->cpu_for_motherboard($motherboard_qty_cpu);
        if($cpu_fam){
            foreach($cpu_fam as $cpu_fam_data){
                $cpu_family = $cpu_fam_data->cpu_family;
                break;
            }
        }
        if(empty($cpu_family)){$cpu_family = "";}
        if($cpu_family != ""){$cpu = $this->work_orders_model->get_cpu_fam($cpu_family);}else{$cpu = null;}

        if(empty($cpu)){
            $cpu = null;
        }
        $search_cpu['cpu'] = $cpu;
        $this->load->view('modules/assembly/ajax/select_cpu_mb_edit', $search_cpu);
    }

    public function get_cpu_editMB(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu2'] = $this->assembly_model->cpu_for_motherboard($qty_cpu);
        $this->load->view('modules/assembly/ajax/select_cpu_mb_edit', $get_cpu_motherboard);
    }

    public function memory_for_motherboard_editMB(){
        $motherboard = $this->input->post('motherboard');
      
        $motherboard_chassis = $this->assembly_model->motherboard_for_chassis($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $memory = $this->assembly_model->memory_for_motherboard($motherboard->memory_family);
        }
        if(empty($memory)){
            $memory = null;
        }
        $search_memory['memory2'] = $memory;
        $this->load->view('modules/assembly/ajax/select_memory_mb_edit', $search_memory);
    }

    public function get_memory_editMB(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->assembly_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
            }
        }
        $get_memory['memory2'] = $this->assembly_model->memory_for_motherboard($memory_family);
        
        
        $this->load->view('modules/assembly/ajax/select_memory_mb_edit', $get_memory);
    }

    public function create_listing(){
        
        $build_id = $this->input->post('build_id');
        $sales_rep = $this->input->post('sales_rep');
        $chassis = $this->input->post('chassis');
        $backplane = $this->input->post('backplane');
        $drivebays = $this->input->post('drivebays');
        $rearbays = $this->input->post('rearbays');
        $node = $this->input->post('node');
        $psu = $this->input->post('psu');
        $bin = $this->input->post('bin');
        $front_caddies = $this->input->post('front_caddies');
        $fc_qty = $this->input->post('fc_qty');
        $rear_caddies = $this->input->post('rear_caddies');
        $rc_qty = $this->input->post('rc_qty');
        $drives = $this->input->post('drives');
        $drives_qty = $this->input->post('drives_qty');
        $drives2 = $this->input->post('drives2');
        $drives_qty2 = $this->input->post('drives_qty2');
        $drives3 = $this->input->post('drives3');
        $drives_qty3 = $this->input->post('drives_qty3');
        $motherboard = $this->input->post('motherboard');
        $motherboard_qty = $this->input->post('motherboard_qty');
        $cpu = $this->input->post('cpu');
        $cpu_qty = $this->input->post('cpu_qty');
        $memory = $this->input->post('memory');
        $memory_qty = $this->input->post('memory_qty');
        $ob_nic = $this->input->post('ob_nic');
        $pcie_slot = $this->input->post('pcie_slot');
        $slot = $this->input->post('slot');
        $slot_qty = $this->input->post('slot_qty');
        $slot2 = $this->input->post('slot2');
        $slot_qty2 = $this->input->post('slot_qty2');
        $slot3 = $this->input->post('slot3');
        $slot_qty3 = $this->input->post('slot_qty3');
        $psu_watts = $this->input->post('psu_watts');
        $psu_qty = $this->input->post('psu_qty');
        $mounting = $this->input->post('mounting');
        $mounting_qty = $this->input->post('mounting_qty');
        $warranty = $this->input->post('warranty');

        if(strpos(strtolower($drivebays), '3.5') !== false){
                $ff = '3.5';
                $drivebays = str_replace('3.5', '', $drivebays);
        }else if(strpos(strtolower($drivebays), '2.5') !== false){
                $ff = '2.5';
                $drivebays = str_replace('2.5', '', $drivebays);
        }

        if(strpos(strtolower($rearbays), '3.5') !== false){
                $rff = '3.5';
                $rearbays = str_replace('3.5', '', $rearbays);
        }else if(strpos(strtolower($rearbays), '2.5') !== false){
                $rff = '2.5';
                $rearbays = str_replace('2.5', '', $rearbays);

        }

        $data = array(
            'build_id' => $build_id,
            'sales_rep' => $sales_rep,
            'chassis' => $chassis,
            'backplane' => $backplane,
            'drivebays' => $drivebays,
            'rearbays' => $rearbays,
            'ff' => $ff,
            'rff' => $rff,
            'node' => $node,
            'psu' => $psu,
            'bin' => $bin,
            'front_caddies' => $front_caddies,
            'fc_qty' => $fc_qty,
            'rear_caddies' => $rear_caddies,
            'rc_qty' => $rc_qty,
            'drives' => $drives,
            'drives_qty' => $drives_qty,
            'drives2' => $drives2,
            'drives_qty2' => $drives_qty2,
            'drives3' => $drives3,
            'drives_qty3' => $drives_qty3,
            'motherboard' => $motherboard,
            'motherboard_qty' => $motherboard_qty,
            'cpu' => $cpu,
            'cpu_qty' => $cpu_qty,
            'memory' => $memory,
            'memory_qty' => $memory_qty,
            'ob_nic' => $ob_nic,
            'pcie_slot' => $pcie_slot,
            'slot' => $slot,
            'slot_qty' => $slot_qty,
            'slot2' => $slot2,
            'slot_qty2' => $slot_qty2,
            'slot3' => $slot3,
            'slot_qty3' => $slot_qty3,
            'psu_watts' => $psu_watts,
            'psu_qty' => $psu_qty,
            'mounting' => $mounting,
            'mounting_qty' => $mounting_qty,
            'warranty' => $warranty
        );

        $this->assembly_model->insert_listing($data);
        $this->load->view("modules/assembly/product_table_body", $data);

    }

    public function delete_row(){
        $build_id = $this->input->post('build_id');
        $delete_assembly = $this->assembly_model->delete_row($build_id, $key);

         $this->load->view('modules/assembly/product_table_body', $delete_assembly);
    }

    public function update_row(){
        $build_id = $this->input->post('build_id');
        $chassis = $this->input->post('chassis');
        $backplane = $this->input->post('backplane');
        $rearbays = $this->input->post('rearbays');
        $drivebays = $this->input->post('drivebays');
        $node = $this->input->post('node');
        $psu = $this->input->post('psu');
        $bin = $this->input->post('bin');
        $front_caddies = $this->input->post('front_caddies');
        $fc_qty = $this->input->post('fc_qty');
        $rear_caddies = $this->input->post('rear_caddies');
        $rc_qty = $this->input->post('rc_qty');
        $drives = $this->input->post('drives');
        $drives_qty = $this->input->post('drives_qty');
        $drives2 = $this->input->post('drives2');
        $drives_qty2 = $this->input->post('drives_qty2');
        $drives3 = $this->input->post('drives3');
        $drives_qty3 = $this->input->post('drives_qty3');
        $motherboard = $this->input->post('motherboard');
        $motherboard_qty = $this->input->post('motherboard_qty');
        $cpu = $this->input->post('cpu');
        $cpu_qty = $this->input->post('cpu_qty');
        $memory = $this->input->post('memory');
        $memory_qty = $this->input->post('memory_qty');
        $ob_nic = $this->input->post('ob_nic');
        $pcie_slot = $this->input->post('pcie_slot');
        $slot = $this->input->post('slot');
        $slot_qty = $this->input->post('slot_qty');
        $slot2 = $this->input->post('slot2');
        $slot_qty2 = $this->input->post('slot_qty2');
        $slot3 = $this->input->post('slot3');
        $slot_qty3 = $this->input->post('slot_qty3');
        $psu_watts = $this->input->post('psu_watts');
        $psu_qty = $this->input->post('psu_qty');
        $mounting = $this->input->post('mounting');
        $mounting_qty = $this->input->post('mounting_qty');
        $warranty = $this->input->post('warranty');
     
        if(strpos(strtolower($drivebays), '3.5') !== false){
                $ff = '3.5';
                $drivebays = str_replace('3.5', '', $drivebays);
            }else if(strpos(strtolower($drivebays), '2.5') !== false){
                $ff = '2.5';
                $drivebays = str_replace('2.5', '', $drivebays);
            }

            if(strpos(strtolower($rearbays), '3.5') !== false){
                $rff = '3.5';
                $rearbays = str_replace('3.5', '', $rearbays);
            }else if(strpos(strtolower($rearbays), '2.5') !== false){
                $rff = '2.5';
                $rearbays = str_replace('2.5', '', $rearbays);

            }

        $data = array(
            'chassis' =>$chassis,
            'backplane' =>$backplane,
            'rearbays' =>$rearbays,
            'drivebays' =>$drivebays,
            'ff' => $ff,
            'rff' => $rff,
            'node' =>$node,
            'psu' =>$psu,
            'bin' =>$bin,
            'front_caddies' =>$front_caddies,
            'fc_qty' =>$fc_qty,
            'rear_caddies' =>$rear_caddies,
            'rc_qty' =>$rc_qty,
            'drives' =>$drives,
            'drives_qty' =>$drives_qty,
            'drives2' =>$drives2,
            'drives_qty2' =>$drives_qty2,
            'drives3' =>$drives3,
            'drives_qty3' =>$drives_qty3,
            'motherboard' =>$motherboard,
            'motherboard_qty' =>$motherboard_qty,
            'cpu' =>$cpu,
            'cpu_qty' =>$cpu_qty,
            'memory' =>$memory,
            'memory_qty' =>$memory_qty,
            'ob_nic' => $ob_nic,
            'pcie_slot' => $pcie_slot,
            'slot' =>$slot,
            'slot_qty' =>$slot_qty,
            'slot2' =>$slot2,
            'slot_qty2' =>$slot_qty2,
            'slot3' =>$slot3,
            'slot_qty3' =>$slot_qty3,
            'psu_watts' =>$psu_watts,
            'psu_qty' =>$psu_qty,
            'mounting' =>$mounting,
            'mounting_qty' =>$mounting_qty,
            'warranty' =>$warranty


        );
        
        $update_assembly = $this->assembly_model->update_row($build_id, $data);
        $this->load->view('modules/assembly/product_table_body', $update_assembly);

    }

    public function front_caddies_qty_checker(){
        $qty_front_caddies = $this->input->post('fc_qty');
        $drivebays = $this->input->post('drivebays');
        

        $explode_dbays = explode(' ', $drivebays);
        $first_dbay = $explode_dbays[0];
        
        if($qty_front_caddies > $first_dbay){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }

    }

    public function rear_caddies_qty_checker(){
        $qty_rear_caddies = $this->input->post('rc_qty');
        $rearbays = $this->input->post('rearbays');

        $explode_rbays = explode(' ', $rearbays);
        $first_rbay = $explode_rbays[0];

        if($qty_rear_caddies >= $first_rbay){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
    }



}

?>
