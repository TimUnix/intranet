<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('users_model');
	}
    
	public function index(){
		//load session library
		$this->load->library('session');
        
		//restrict users to go back to login if session has been set
		if($this->session->has_userdata('user')){
			redirect('home');
		}
		else{
			$this->load->view('login');
		}
	}
    
	public function login(){
		//load session library
		$this->load->library('session');
        
		$email = $_POST['email'];
		$password = $_POST['password'];
        
		$data = $this->users_model->login($email, $password);
		$data['is_google'] = false;
        
		if($data){
			$this->session->set_userdata('user', $data);
			
			if($this->session->userdata('user')){
				
				
				redirect('home');
			}else{
				
				$this->load->view('login');
			}
			
		}
		else{
			header('location:'.base_url().$this->index());
			$this->session->set_flashdata('error','Invalid login. User not found');
		} 
	}
    
	public function home(){
		//load session library
		$this->load->library('session');
        
		//restrict users to go to home if not logged in
		if($this->session->userdata('user')){
			$data_sess = $this->session->userdata('user');

			$userdata = array(
				'email'	=>	$data_sess['email'],
				'firstname'	=>	$data_sess['firstname'],
				'lastname'	=>	$data_sess['lastname'],
				'page_name'	=>	'Home',
				'home_name'	=>	'Home',
				'is_google' => $data_sess['is_google']
			);
			
			$this->load->view('home', $userdata);
		}
		else{
			redirect('/');
		}
        
	}

    
	public function logout(){
		//load session library
		$this->load->library('session');
		$this->session->unset_userdata('user');
		redirect('/');
	}
    
    public function save_column_state() {
        $this->load->library('session');
        
        $column_name = $this->input->post('column_name');
        $display = $this->input->post('display');
        $page = $this->input->post('page');

        $user = $this->session->userdata('user');

        $columns_result = $this->users_model->get_table_columns($page, $user['id']);
        $columns_result_array = (array) $columns_result;
        $columns = json_decode($columns_result_array["{$page}_columns"], true);

        $columns[str_replace('-', '_', $column_name)]['is_shown'] = $display === 'none' ? false : true;

        $this->users_model->save_table_columns($page, $user['id'], json_encode($columns));
    }

    public function verify_session() {
        $this->load->library('session');

        $user = $this->session->userdata('user');

        $is_session_active = isset($user) && isset($user['id']) && isset($user['email']);

        return $this->output->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode(array( 'isUserSessionActive' => $is_session_active )));
    }
}
?>
