<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_items extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array( 'url', 'nav_order', 'user_access' ));
		$this->load->model(array( 'users_model', 'navigation_model', 'google_model', 'menu_items_model' ));
    }

    function index() {
		$this->load->library('session');
        
		//restrict users to go to home if not logged in
		if($this->session->has_userdata('user')){
			$data_sess = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($data_sess['email']);
			$user_id = $get_user_id->id;
			
			$is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $data_sess['id'];
			}
            $nav = $this->navigation_model->get_navigation_menu($g_id);

            $ordered_nav = organize_nav_order($nav);

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            } 

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'menu_items');
            $is_user_permitted = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_permitted) return redirect('access_denied');

			$userdata['data'] = array(
				'user_picture'	=>	$picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
				'page_name'	=>	$user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
				'is_google' => $is_google
			);

			$this->load->view('modules/admin/menu_items', $userdata);
	    }
	    else{
            $data['data'] = array(
                'page_name' => 'Login'
            );
            
		    $this->load->view('login', $data);
	    }
    }

    function request() {
        switch ($this->uri->rsegment(3)) {
            case 'load_menu_info':
                self::_request_load_menu_info();
                break;
            case 'load_menu_infos':
                self::_request_load_menu_infos();
                break;
			case 'update_menu_item':
                self::_request_update_menu_item();
                break;
			case 'update_menu_infos':
				self::_request_update_menu_infos();
				break;
        }
    }

    private function _request_load_menu_info() {
        $result = $this->menu_items_model->get_menu_info();

        $data = array();

        foreach ($result as $row) {
            $data[] = array(
                "<button class=\"btn btn-default\" type=\"button\" " .
                "data-toggle=\"modal\" data-target=\"#editorModal\">" .
                "<i class=\"fa fa-edit\" aria-hidden=\"true\"></i></button>",
                $row->menu_order,
                $row->menu_id,
                $row->menu_title,
                $row->menu_file,
                $row->menu_parent
            );
        }

        header('Content-Type: application/json');
        echo json_encode(array( 'data' => $data ));
    }
    private function _request_load_menu_infos() {
        $result['menu_items'] = $this->menu_items_model->get_menu_info();
        $result['menu_information'] = $this->menu_items_model->get_menu_information();
        $this->load->view('modules/admin/ajax/menu_items', $result);

    }
    public function _request_update_menu_infos(){
        $title = $this->input->post('title');
        $parent = $this->input->post('parent');
        $order = $this->input->post('order');
        $menu_id = $this->input->post('id');
        $icons = $this->input->post('icons');
		
		$data = array(
			'menu_title' => $title,
			'menu_parent' => $parent,
			'menu_order' => $order,
			'icons' => $icons
		);

		$this->menu_items_model->update_menu_infos($data, 'menu_id', $menu_id);

        return true;
    }
    
    private function _request_update_menu_item() {
        $order = $this->input->post('order');
        $menu_id = $this->input->post('menuId');
        $title = $this->input->post('title');
        $file = $this->input->post('file');
		$parent = $this->input->post('parent');
		$icons = $this->input->post('icons');
		
        $this->menu_items_model->update_menu_item($order, $menu_id, $title,
                                                 $file, $parent, $icons);
    }
}
?>
