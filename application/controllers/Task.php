<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->has_userdata('user')) {
            $task = $this->detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'task');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $request = $this->task_model->get_request();
            $columns = $this->users_model->get_table_columns('task', $g_id);

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'actions' => $request,
                'task' => $task,
                'user_filters' => $this->users_model->get_users_with_access_to_page('Task'),
                'columns' => json_decode($columns->task_columns)
            );

            $this->load->view('modules/task/task_main_page', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    function request() {
        if ($this->uri->rsegment(3) == 'create_listing') {
            self::_request_create_listing();
        }
        
        
        if ($this->uri->rsegment(3) == "update_columns") {
            self::_request_update_columns();
        }
    }

    function archive() {
        $this->load->library('session');

        $user = $this->session->userdata('user');
        $user_name = trim($user['firstname']) . ' ' . trim($user['lastname']);
        
        $id = $this->input->post('id');
        $notes = $this->input->post('notes');
        $timestamp = date('Y-m-d H:i:s');
        $result = $this->task_model->archive($id, $notes, $user_name, $timestamp);
        $data = array( 'is_success' => (bool) $result );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    private function detect_listing_action() {
        $referred = $this->input->get('referred');
        $action = $this->input->get('createtask');
        $sku = $this->input->get('new_listing');
        $price = $this->input->get('price');

        if (isset($referred) && $referred == '1') {
            return array(
                'action' => $action,
                'sku' => $sku,
                'price' => $price
            );
        }

        return null;
    }

    public function export_csv() {
        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        $request_actions = $this->input->post('actions');
        $banner = $this->input->post('banner');
        $photo = $this->input->post('photo');
        $search_column = $this->input->post('search_column');
        $is_done_shown = $this->input->post('is_done_shown') === 'true';
        $is_only_me_enabled = $this->input->post('is_only_me_enabled') === 'true';

        $actions = isset($request_actions) && $request_actions !== '' ? explode(',', $request_actions) : null;

        if ($is_only_me_enabled) {
            $users = array(trim($user['firstname']) . ' ' . trim($user['lastname']));
        } else {
            $users = isset($request_users) && $request_users !== '' ? explode(',', $request_users) : null;
        }

        try {
            $csv = $this->task_model->export_csv(
                trim($search),
                $order_by,
                $sort_order,
                $actions,
                $banner,
                $photo,
                $users,
                $search_column,
                $is_done_shown
            );
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function load_listing() {
        $user = $this->session->userdata('user');

        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        $request_actions = $this->input->post('actions');
        $request_users = $this->input->post('users');
        $banner = $this->input->post('banner');
        $photo = $this->input->post('photo');
        $search_column = $this->input->post('search_column');

        $is_archived_shown = $this->input->post('is_archived_shown') === 'true';
        $is_done_shown = $this->input->post('is_done_shown') === 'true';
        $is_only_me_enabled = $this->input->post('is_only_me_enabled') === 'true';

        $actions = isset($request_actions) && $request_actions !== '' ? explode(',', $request_actions) : null;
        $categories = $this->input->post('categories');
        
        if ($is_only_me_enabled) {
            $users = array(trim($user['firstname']) . ' ' . trim($user['lastname']));
        } else {
            $users = isset($request_users) && $request_users !== '' ? explode(',', $request_users) : null;
        }

        $data = array(
            'listing_added' => $this->task_model->load(
                trim($search),
                $order_by,
                $sort_order,
                $actions,
                $banner,
                $photo,
                $users,
                $categories,
                $search_column,
                $is_done_shown,
                $is_archived_shown
            ),
            'is_done_shown' => $is_done_shown,
            'is_archived_shown' => $is_archived_shown,
            'columns' => $this->users_model->get_table_columns('task', $user['id'])
        );

        $this->load->view("modules/task/ajax/listingtable", $data);
    }

    public function load_listing_count() {
        $user = $this->session->userdata('user');

        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        $request_actions = $this->input->post('actions');
        $request_users = $this->input->post('users');
        $banner = $this->input->post('banner');
        $photo = $this->input->post('photo');
        $search_column = $this->input->post('search_column');
        $is_done_shown = $this->input->post('is_done_shown') === 'true';
        $is_archived_shown = $this->input->post('is_archived_shown') === 'true';
        $is_only_me_enabled = $this->input->post('is_only_me_enabled') === 'true';
        
        $actions = isset($request_actions) && $request_actions !== '' ? explode(',', $request_actions) : null;
        $categories = $this->input->post('categories');

        if ($is_only_me_enabled) {
            $users = array(trim($user['firstname']) . ' ' . trim($user['lastname']));
        } else {
            $users = isset($request_users) && $request_users !== '' ? explode(',', $request_users) : null;
        }

        try {
            $result = $this->task_model->count(
                trim($search),
                $order_by,
                $sort_order,
                $actions,
                $categories,
                $banner,
                $photo,
                $users,
                $search_column,
                $is_done_shown,
                $is_archived_shown
            );
        } catch (Exception $e) {
            $response['data'] = array(
                'status' => 500,
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/html', 'utf-8')
                            ->set_output($result->listing_count);
    }

    public function _request_create_listing() {
        $sku = $this->input->post('sku');
        $shipping = $this->input->post('shipping');
        $weight = $this->input->post('weight');
        $price_with_sign = $this->input->post('price');
        $price_replace = str_replace('$','', $price_with_sign);
        $price = trim($price_replace);
        $promotion = $this->input->post('promotion');
        $volume_discount_2x = $this->input->post('volume_discount_2x');
        $volume_discount_3x = $this->input->post('volume_discount_3x');
        $volume_discount_4x = $this->input->post('volume_discount_4x');
        $request = $this->input->post('request');
        $banner = $this->input->post('banner');
        $notes = $this->input->post('notes');
        $photo = $this->input->post('photo');
        $moved = $this->input->post('moved');
        $lot = $this->input->post('lot');
        $exit_sku_empty = 0;
        $exit_price_empty = 1;
        $exit_sku_exists = 2;
        $exit_sku_exists_and_listing_id = "SKU already done and listed to ebay";
        $sku_successful_register = "SKU Successfully Registered";
        $sku_and_task_request_already_exist = "Sku and Task Request Already Exist";

        if ($promotion == null || trim($promotion) == '') {
            $promotion = 0;
        }

        if ($volume_discount_2x == null || trim($volume_discount_2x) == '') {
            $volume_discount_2x = 0;
        }

        if ($volume_discount_3x == null || trim($volume_discount_3x) == '') {
            $volume_discount_3x = 0;
        }

        if ($volume_discount_3x == null || trim($volume_discount_3x) == '') {
            $volume_discount_3x = 0;
        }

        if ($banner == "") {
            $banner = "NO";
        }

        if ($weight === '') {
            $weight = 0;
        }

        if (!isset($photo) || $photo === '') {
            $photo = 0;
        }

        $data_sess = $this->session->userdata('user');

        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);
        if ($sku === '') {
            return $exit_sku_empty;
        } else if (($request == 1 || $request == 3) && $price === '') {
            return $exit_price_empty;
        } else {
            $data = array(
                'date_requested' => date("Y-m-d H-i-s"),
                'request' => $request,
                'request_last_edited' => date("Y-m-d H-i-s"),
                'request_last_edited_by' => $fullname,
                'sku' => $sku,
                'photo' => $photo,
                'photo_last_edited' => date("Y-m-d H-i-s"),
                'photo_last_edited_by' => $fullname,
                'promotion' => $promotion,
                'promotion_last_edited' => date("Y-m-d H-i-s"),
                'promotion_last_edited_by' => $fullname,
                'volume_discount_2x' => $volume_discount_2x,
                'volume_discount_2x_last_edited' => date("Y-m-d H-i-s"),
                'volume_discount_2x_last_edited_by' => $fullname,
                'volume_discount_3x' => $volume_discount_3x,
                'volume_discount_3x_last_edited' => date("Y-m-d H-i-s"),
                'volume_discount_3x_last_edited_by' => $fullname,
                'volume_discount_4x' => $volume_discount_4x,
                'volume_discount_4x_last_edited' => date("Y-m-d H-i-s"),
                'volume_discount_4x_last_edited_by' => $fullname,
                'lot' => $lot,
                'lot_last_edited' => date("Y-m-d H-i-s"),
                'lot_last_edited_by' => $fullname,
                'shipping' => $shipping,
                'shipping_last_edited' => date("Y-m-d H-i-s"),
                'shipping_last_edited_by' => $fullname,
                'banner' => $banner,
                'banner_last_edited' => date("Y-m-d H-i-s"),
                'banner_last_edited_by' => $fullname,
                'request_by' => $fullname,
                'draft_id' => '0',
                'draft_by' => '',
                'listing_item_id' => '0',
                'listing_by' => ''
            );
            
            if (isset($moved) && $moved !== '') {
                $data['moved'] = $moved;
                $data['moved_last_edited'] = date("Y-m-d H-i-s");
                $data['moved_last_edited_by'] = $fullname;
            }

            if (isset($price) && $price !== '' && (float) $price > 0) {
                $data['price'] = $price;
 
                $data['price_last_edited'] = date("Y-m-d H-i-s");
                $data['price_last_edited_by'] = $fullname;
            }

            if (isset($weight) && $weight !== '' && (float) $weight > 0) {
                $data['weight'] = $weight;
                $data['weight_last_edited'] = date("Y-m-d H-i-s");
                $data['weight_last_edited_by'] = $fullname;
            }

            if (isset($notes) && $notes !== '') {
                $data['notes'] = $notes;
                $data['notes_last_edited'] = date("Y-m-d H-i-s");
                $data['notes_last_edited_by'] = $fullname;
            }

            $check_sku_and_task = $this->task_model->check_sku_and_task($sku, $request);
            $get_exclude_condition = $this->task_model->get_exclude_condition($request);
        
            foreach($get_exclude_condition as $key) {
                $get_exclude_condition = $key->exclude_condition;
            }

			if($get_exclude_condition == 0 && count($check_sku_and_task) > 0)
			{
				echo $sku_and_task_request_already_exist;
				return $sku_and_task_request_already_exist;
			}
			else
			{
				$this->task_model->insert_listing($data);
				$data['listing_added'] = $this->task_model->show_added_listing();
				$data['actions'] = $this->task_model->get_request();
				return $this->load->view("modules/task/ajax/listingtable", $data);

				echo $sku_successful_register;
				return $sku_successful_register;
			}
           
        }
    }

    public function _request_update_columns() {
        $this->load->library('session');
        $data_sess = $this->session->userdata('user');
        $firstname = trim($data_sess['firstname']);
        $lastname = trim($data_sess['lastname']);
        $fullname = $firstname . " " . $lastname;

        $a = ($this->input->post('data'));
        $col = $a['col'];

        $listing_idrep = str_replace(array("\n", "\r"), '', trim($a['listing_id']));
        $listing_id = nl2br($listing_idrep);

        if ($col == 'notes') {
            $request = str_replace(array("\n", "\r"), '<br>', trim($a['request']));
        } else {
            $requestrep = str_replace(array("\n", "\r"), '', trim($a['request']));
            $request = nl2br($requestrep);
        }

        if (isset($request) && $request !== '') {
            if ($col == "draft_id") {
                $data = array(
                    "$col" => $request,
                    "draft_by" => $fullname,
                    "draft_date" => date("Y-m-d H-i-s"),
                    "draft_id_last_edited_by" => $fullname
                );
                $this->task_model->update_request($data, 'listing_id', $listing_id);
                echo 1;
            } elseif ($col == "listing_item_id") {
                $data = array(
                    "$col" => $request,
                    "listing_by" => $fullname,
                    "listing_date" => date("Y-m-d H-i-s"),
                    "listing_item_id_last_edited_by" => $fullname
                );
                $this->task_model->update_request($data, 'listing_id', $listing_id);
            } else {
                $data = array(
                    "$col" => $request,
                    "{$col}_last_edited" => date("Y-m-d H-i-s"),
                    "{$col}_last_edited_by" => $fullname
                );
                $this->task_model->update_request($data, 'listing_id', $listing_id);
            }
        }
    }

    public function get_actions() {
        $data = array(
            'actions' => $this->task_model->get_request()
        );

        $this->load->view('modules/task/ajax/actions', $data);
    }

    public function load_filters()
    {
        try {
            $actions = $this->task_model->get_request();
            $categories = $this->inventory_model->get_categories();
        } catch (Exception $e) {
            $data['data'] = array(
                'status' => 500,
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }

        $data = array(
            'actions' => $actions,
            'categories' => $categories
        );

        $this->load->view('modules/task/ajax/filters', $data);
    }

    public function search_sku_suggestions() {
        $search = $this->input->get('search');

        try {
            $suggestions = $this->inventory_model->search_sku_suggestions($search);
        } catch (Exception $e) {
            $data['data'] = array(
                'status' => 500,
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }

        $data['data'] = array(
            'status' => 200,
            'suggestions' => $suggestions
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }
}
