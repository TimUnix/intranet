<?php

class View_test_reports extends CI_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url', 'nav_order', 'user_access'));
		$this->load->model(array('users_model', 'navigation_model', 'google_model','reports_model', 'test_model'));
	}
	
	public function index(){
		//load session library
		$this->load->library('session');
        
		//restrict users to go to home if not logged in
		if($this->session->has_userdata('user')){
			$data_sess = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($data_sess['email']);
			$user_id = $get_user_id->id;
			
			$is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $data_sess['id'];
			}

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'view_test_reports');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');
            
            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            } 

			$userdata['data'] = array(
				'user_picture'	=>	$picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
				'page_name'	=>	$user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
				'is_google' => $is_google
			);
			
			$this->load->view("modules/view_test_reports/view_test_report_main_page", $userdata);
	    } else {
            $data['data'] = array(
                'page_name' => 'Login'
            );
		    $this->load->view('login', $data);
	}
}


function request(){
	if($this->uri->rsegment(3) == "view_test_reports"){
		self::_request_view_test_reports();
	}
}
public function _request_view_test_reports(){
	
	$view_test_reports['view_test_reports'] = $this->test_model->getSearch(array(),"",array("switch.tech"=>"ASC"),true);
	$this->load->view('modules/view_test_reports/ajax/view_test_reports', $view_test_reports);


}
public function _request_add_research(){
	
	$serialnumber = $this->input->post(mysql_real_escape_string('serialnumber'));
	$techname = $this->input->post(mysql_real_escape_string('techname'));
	$message = $this->input->post(mysql_real_escape_string('message'));


	$this->load->library("form_validation");
    
    $this->form_validation->set_rules('serialnumber', 'SerialNumber', 'trim|required');
	$this->form_validation->set_rules('techname', ' Techname', 'trim|required');
	$this->form_validation->set_rules('message', ' Message', 'trim|required');


	if($this->form_validation->run() == false){
		
		$this->load->view('modules/view_test_reports/ajax/sticky_notes/empty');
	}else{
		$switch_report_item = array(
			'serial_number' => $serialnumber,
			'tech' => $techname,
			'report' => $message

		);

		$switch_checking = $this->test_model->switch_checking($serialnumber, $techname);
		if($switch_checking){
			$last_update['last_update'] = date('Y-m-d H-i-s');
			$last_update['report'] = $message;
			$switch_update = $this->test_model->switch_update($last_update,'serial_number',$serialnumber, 'tech', $techname);
			if($switch_update){

				$this->load->view('modules/view_test_reports/ajax/sticky_notes/update', $view_test_reports);
			}
		}else{
			$switch_report_data = $this->test_model->insert_table($switch_report_item);
			if($switch_report_data == 0){
				
				$this->load->view('modules/view_test_reports/ajax/sticky_notes/add');
			}
		}
	}
}
}

?>
