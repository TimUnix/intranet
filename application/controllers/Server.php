<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Server extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'server_model'
        ));
    }

	public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
           // $task = self::detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'server');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);
            $columns = $this->users_model->get_table_columns('server', $g_id);

        
            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'  => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'columns' => json_decode($columns->server_columns)
            );

            $this->load->view('modules/server/index', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

	public function search_item() {
    	$user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');
        $items_offset = (int) $this->input->get('items_offset');
       
        try{
        	$items = $this->server_model->get(
        	$searchItem,
        	$searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order,
            $items_offset
        	);
        }catch (Exception $e){
        	 $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
            $columns = $this->users_model->get_table_columns('server', $user['id']);
            $table_columns = json_decode($columns->server_columns);
            
            $data = array(
                'items' => $items,
                'columns' => $table_columns,
                'offset' => $items_offset
            );
   
            return $this->load->view('modules/server/product_table_body', $data);
    }

    public function createListing(){
        $user = $this->session->userdata('user');
        $firstname = $user['firstname'];
        $lastname = $user['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);

        // $request = $this->input->post('request');
        $sku = $this->input->post('sku');
        $con = $this->input->post('con');
        $brand = $this->input->post('brand');
        $units = $this->input->post('units');
        $nodes = $this->input->post('nodes');
        $bays = $this->input->post('bays');
        $drive_ff = $this->input->post('drive_ff');
        $backplane = $this->input->post('backplane');
        $socket_supported = $this->input->post('socket_supported');
        $qty_of_power_supply = $this->input->post('qty_of_power_supply');

        $exit_sku_empty = 0;

        if($sku === '' ){
            return $exit_sku_empty;
        }else{
            $data = array(
                // 'request' => $request,
                'sku' => $sku,
                'con' => $con,
				'units' => $units,
                'brand' => $brand,
                'model' => $model,
                'nodes' => $nodes,
                'bays' => $bays,
                'drive_ff' => $drive_ff,
                'backplane' => $backplane,
                'socket_supported' => $socket_supported,
                'qty_of_power_supply' => $qty_of_power_supply
            );

        $this->server_model->insert_listing($data);
        $data['listing_added'] = $this->server_model->show_added_listing();
        $this->load->view("modules/server/product_table_body", $data);

        }
    }

    public function load_listing_count() {
        $user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
            $items = $this->server_model->count(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
        }catch (Exception $e){
             $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

            return $this->output->set_header(200)
                        ->set_content_type('text/html', 'utf-8')
                        ->set_output($items->listing_count);
    }

	public function createServer(){
        $user = $this->session->userdata('user');
        $firstname = $user['firstname'];
        $lastname = $user['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);

        $sku = $this->input->post('sku');
        $con = $this->input->post('con');
        $brand = $this->input->post('brand');
        $units = $this->input->post('units');
        $nodes = $this->input->post('nodes');
        $bays = $this->input->post('bays');
        $drive_ff = $this->input->post('drive_ff');
        $backplane = $this->input->post('backplane');
        $socket_supported = $this->input->post('socket_supported');
        $qty_of_power_supply = $this->input->post('qty_of_power_supply');

        $exit_sku_empty = 0;

        if($sku === '' ){
            return $exit_sku_empty;
        }else{
            $data = array(
                'sku' => $sku,
                'con' => $con,
				'units' => $units,
                'brand' => $brand,
                'model' => $model,
                'nodes' => $nodes,
                'bays' => $bays,
                'drive_ff' => $drive_ff,
                'backplane' => $backplane,
                'socket_supported' => $socket_supported,
                'qty_of_power_supply' => $qty_of_power_supply
            );

        $this->server_model->insert_server($data);
        $data['server_added'] = $this->server_model->show_added_server();
        $this->load->view("modules/server/product_table_body", $data);

        }
    }

	public function update_columns() {
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $type = $data['type'];
        $datatoupdate = $data['data'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->server_model->update_servercol($col, $sku);
        
    }  

	public function generate_csv() {
        $searchItem = $this->input->get('searchItem');
        $sort_order = $this->input->get('sortOrder');
        $order_by = $this->input->get('orderBy');
        $searchSelected = $this->input->get('searchSelected');


        try {
            $csv = $this->server_model->export_csv(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
  
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

}

?>
