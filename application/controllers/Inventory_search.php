<?php

class Inventory_search extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access', 'median_calculator'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'reports_model',
            'test_model',
            'research_model',
            'search_history_model',
            'configuration_model',
            'inventory_model'
        ));
        error_reporting(E_ERROR | E_PARSE);
    }

    public function index(){
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if($this->session->has_userdata('user')){
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

            if($is_google == true){
                $g_id = $user_id;
            }else{
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'inventory_search');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $search_query = $this->input->post('search');
            $search_history = $this->search_history_model->get_search_history_for_user($user_id);

            $userdata['data'] = array(
                'user_picture'  =>  $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name' =>  $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'search_history' => $search_history,
                'search_query' => $search_query
			);
			
			$this->load->view("modules/inventory_search/inventory_search_main_page", $userdata);
		}
		else{
            $data['data'] = array(
                'page_name' => 'Login'
            );

			$this->load->view('login', $data);
		}
	}

    function request(){
        if ($this->uri->rsegment(3) == "load_cat") {
            self::_request_load_cat();
        }

        if ($this->uri->rsegment(3) == "search_item_by_cat") {
            self::_request_search_item_by_cat();
        }if ($this->uri->rsegment(3) == "search_inventory") {
            self::_request_search_inventory();
        }
    }

    function search() {
        $categories = $this->input->post("categories");
        $conditions = $this->input->post("conditions");
        $part_number = $this->input->post("part_number");
        $search_history = $this->input->post("search_history");

        $qty_zero_key = isset($conditions) ? array_search("condition_zero_qty", $conditions) : false;
        $is_qty_zero_shown = $qty_zero_key !== false;

        if ($qty_zero_key !== false) {
            unset($conditions[$qty_zero_key]);
        }

        $items = $this->inventory_model->get_filtered_items(
            $part_number,
            $categories,
            $conditions,
            $is_qty_zero_shown,
            null,
            null
        );

        $data = array(
            "items" => $items
        );

        return $this->load->view("modules/inventory_search/ajax/table", $data);
    }
    
    function load_summary() {
        $categories = $this->input->post("categories");
        $conditions = $this->input->post("conditions");
        $part_number = $this->input->post("part_number");
        $search_history = $this->input->post("search_history");

        $qty_zero_key = isset($conditions) ? array_search("condition_zero_qty", $conditions) : false;
        $is_qty_zero_shown = $qty_zero_key !== false;

        if ($qty_zero_key !== false) {
            unset($conditions[$qty_zero_key]);
        }

        $summary = $this->inventory_model->get_summary(
            $part_number,
            $categories,
            $conditions,
            $is_qty_zero_shown
        );

        $data = array(
            "summary" => $summary
        );

        return $this->output->set_header(200)
                            ->set_content_type("application/json", "utf-8")
                            ->set_output(json_encode($data));
    }

    public function _request_load_cat() {
        $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

            if($is_google == true){
                $g_id = $user_id;
            }else{
                $g_id = $data_sess['id'];
            }
        $get_inventory = $this->inventory_model->get_inventory("cat");
        $get_inventory_con = $this->inventory_model->get_inventory("con");
        $data['inventory_cat'] = $get_inventory;
        $data['inventory_con'] = $get_inventory_con;
        $data['search_history'] = $this->search_history_model->get_search_history_for_user($user_id);
        
        $this->load->view("modules/inventory_search/ajax/inventory_search_ajax", $data);

    }

    function update_search_history() {
        $data_sess = $this->session->userdata('user');
        $get_user_id = $this->users_model->get_user_id($data_sess['email']);
        $search_query = $this->input->post('search');

        $info_user_id = "";

        foreach($get_user_id as $user_id){
            $info_user_id = $user_id->id;
        }
        if($info_user_id != 0){
            $check_users_history = $this->search_history_model->check_users_history($info_user_id, $ebayitem);
            if($check_users_history){
                $data = array(
                    'updated' => date('Y-m-d H-i-s')
                );
                $this->search_history_model
                ->update_search_history($data, 'user_id', $info_user_id, 'search', $ebayitem);
            }else{
                $get_all_rows_num = $this->search_history_model->get_all_rows_num();
                $sh_id = "";
                foreach($get_all_rows_num as $_get_all_rows_num){
                    $sh_id = $_get_all_rows_num->sh_id;
                }
                $data = array(
                    'sh_id' => $sh_id + 1,
                    'user_id' => $info_user_id,
                    'search' => $ebayitem,
                    'updated' => date('Y-m-d H-i-s')
                );
                $this->search_history_model->insert_table($data);
            }
        }
    }

    private function _request_local_items() {
        $part_number = $this->input->post('part_number');

        $local_items = $this->inventory_model->get_local_items($part_number);
        if($items = array('local_items' => $local_items)){
           $this->load->view('modules/research/ajax/local_items', $items);
            
        }
        else {
            $this->load->view('modules/research/ajax/result_zero');
        }
    }

}

?>
