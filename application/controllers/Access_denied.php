<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_denied extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index() {
        // $this->load->library('session');

        // $this->session->unset_userdata('user');
		// $this->session->unset_userdata('access_token');
        // $this->session->unset_userdata('user');
        // $this->session->sess_destroy();

        $data['data'] = array( 'page_name' => 'Access Denied' );

        $this->load->view('templates/access_denied', $data);
    }
}

?>
