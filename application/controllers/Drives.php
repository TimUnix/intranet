<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Drives extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'drives_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
           // $task = self::detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'drives');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);
            $get_capacity_template = $this->drives_model->get_capacity_template();
            $get_ff_template = $this->drives_model->get_ff_template();
            $get_interface_template = $this->drives_model->get_interface_template();
            $get_condition_template = $this->drives_model->get_condition_template();
            $get_warranty_template = $this->drives_model->get_warranty_template();
            $get_brand_template = $this->drives_model->get_brand_template();
            //$channels['channels'] = $this->task_model->get_channel();
           // $request = $this->task_model->get_request();
            $columns = $this->users_model->get_table_columns('drives', $g_id);

        
            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'  => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'columns' => json_decode($columns->drives_columns),
                'capacity_template' => $get_capacity_template,
                'form_factor_template' => $get_ff_template,
                'interface_template' => $get_interface_template,
                'condition_template' => $get_condition_template,
                'warranty_template' => $get_warranty_template,
                'brand_template' => $get_brand_template
                //'task' => $task

            );

            $this->load->view('modules/drives/index', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function search_item() {
    	$user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');
        $items_offset = (int) $this->input->get('items_offset');
       
        try{
        	$items = $this->drives_model->get(
        	$searchItem,
        	$searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order,
            $items_offset
        	);
        }catch (Exception $e){
        	 $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
            $columns = $this->users_model->get_table_columns('drives', $user['id']);
            $table_columns = json_decode($columns->drives_columns);
            
            $data = array(
                'items' => $items,
                'columns' => $table_columns,
                'offset' => $items_offset
            );
   
            return $this->load->view('modules/drives/product_table_body', $data);
    }

    public function createListing(){
        $user = $this->session->userdata('user');
        $firstname = $user['firstname'];
        $lastname = $user['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);

        // $request = $this->input->post('request');
        $sku = $this->input->post('sku');
        $barcode = $this->input->post('bar');
        $upc = $this->input->post('upc');
        $brand = $this->input->post('brand');
        $form_factor = $this->input->post('ff');
        $size = $this->input->post('size');
        $type = $this->input->post('type');
        $rpm = $this->input->post('rpm');
        $speed = $this->input->post('speed');
        $interface = $this->input->post('interface');
        $series = $this->input->post('series');
        $condition = $this->input->post('con');

        $exit_sku_empty = 0;

        if($sku === '' ){
            return $exit_sku_empty;
        }else{
            $data = array(
                // 'request' => $request,
                'sku' => $sku,
                'bar' => $barcode,
                'upc' => $upc,
                'brand' => $brand,
                'ff' => $form_factor,
                'size' => $size,
                'type' => $type,
                'rpm' => $rpm,
                'speed' => $speed,
                'interface' => $interface,
                'series' => $series,
                'con' => $condition
            );

        $this->drives_model->insert_listing($data);
        $data['listing_added'] = $this->drives_model->show_added_listing();
        $this->load->view("modules/drives/product_table_body", $data);

        }
    }

    public function load_listing_count() {
        $user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
            $items = $this->drives_model->count(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
        }catch (Exception $e){
             $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

            return $this->output->set_header(200)
                        ->set_content_type('text/html', 'utf-8')
                        ->set_output($items->listing_count);
    }

    public function update_columns() {
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $bar = $data['bar'];
        $type = $data['type'];
        $datatoupdate = $data['data'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->drives_model->update_drivescol($col, $sku, $bar);
        
    }    

     public function generate_csv() {
        $searchItem = $this->input->get('searchItem');
        $sort_order = $this->input->get('sortOrder');
        $order_by = $this->input->get('orderBy');
        $searchSelected = $this->input->get('searchSelected');


        try {
            $csv = $this->drives_model->export_csv(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
  
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function image_src(){
        $capacity_preview = $this->input->get('capacity_template');
        $interface_preview = $this->input->get('interface_template');

        $data = array(
            'capacity_preview' => $capacity_preview,
            'interface_preview' => $interface_preview
        );
       
        $this->load->view('modules/drives/ajax/previewTemplate', $data);

    }
}

?>
