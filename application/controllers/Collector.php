<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collector extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array( 'url', 'nav_order', 'user_access' ));
        $this->load->model(array( 'navigation_model', 'google_model' ));
    }

    public function index() {
        $this->load->library('session');

        if ($this->session->has_userdata('user')) {
            $userdata = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($userdata['email']);
			$user_id = $get_user_id->id;
            $is_google = $userdata['is_google'];
			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $userdata['id'];
			}

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'Collector');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);

            // User access is elevated when they are granted access to one or more pages.
            $is_user_access_elevated = isset($nav) && count($nav) > 0;
            $ordered_nav = organize_nav_order($nav);
            $picture = $userdata['picture'];

            if (is_null($userdata['picture']) || $userdata['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $data['data'] = array(
                'user_picture' => $picture,
                'user_email' => $userdata['email'],
                'user_firstname' => $userdata['firstname'],
                'user_lastname' => $userdata['lastname'],
                'nav_menu' => $ordered_nav,
                'page_name' => 'Collector',
                'is_google' => $is_google,
                'is_user_access_elevated' => $is_user_access_elevated,
            );

            $this->load->view('modules/collector/index', $data);
        } else {

            $data['data'] = array(
                'page_name' => 'Login'
            );

            $this->load->view('login', $data);
        }
    }
}
