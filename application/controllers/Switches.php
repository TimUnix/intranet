<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class switches extends CI_Controller {

        function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'navigation_model',
            'google_model',
            'switches_model',
            'users_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'switches');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $data = array(
                'data' => array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
            ));
            $this->load->view('modules/switches/index', $data);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function load_galactus(){
        $last_id = $this->input->post('last_id');
        $items_offset = (int) $this->input->post('items_offset');
        $get_item = $this->switches_model->get_galactus($last_id, $items_offset); 
        $done = $this->input->post('done');

        $data = array(
            'offset' => $items_offset,
            'items' => $get_item,
            'done' => $done
        );

        return $this->load->view('modules/switches/switches_table_body', $data);        
    }

   

    public function search_item() {
        $searchItem = $this->input->get('searchItem');
        $items_offset = (int) $this->input->post('items_offset');
        $done = $this->input->get('done');

            $data = array(
                'searchItem' => $searchItem,
                'offset' => $items_offset,
                'done' => $done
            );

            $get_items = $this->switches_model->search_it(
                $searchItem === '' ? null : $searchItem,
                $items_offset,
                $done      
            ); 
            
            $data = array(
                'items' => $get_items,
                'offset' => $items_offset,
                'done' => $done
            );

            return $this->load->view('modules/switches/switches_table_body', $data);      
    }

    function mark_done() {
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->switches_model->mark_done($id, $user);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    function mark_undone() {
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->switches_model->mark_undone($id, $user);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function update_columns() {
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $switchmodel = $data['switchmodel'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->switches_model->update_col($col,$sku,$switchmodel);
        
    }

    public function createListing(){
        $sku = $this->input->post('sku');
        $qty = $this->input->post('qty');
        $con = $this->input->post('con');
        $oem = $this->input->post('oem');
        $switch = $this->input->post('switch');
        $port = $this->input->post('port');
        $license = $this->input->post('license');
        $features = $this->input->post('features');
        $dual = $this->input->post('dual');
        $airflow = $this->input->post('airflow');
        $noedr = $this->input->post('noedr');

        $exit_qty_empty = 0;
        var_dump($sku);
        if($sku === '' ){
            return $exit_qty_empty;
        }else{
            $data = array(
                'sku' => $sku,
                'qty' => $qty,
                'con' => $con,
                'oemman' => $oem,
                'switchmodel' => $switch,
                'port' => $port,
                'license' => $license,
                'features' => $features,
                'dualsingle' => $dual,
                'airflow' => $airflow,
                'noedr' => $noedr
            );

        $this->switches_model->insert_listing($data);
        $this->load->view("modules/switches/switches_table_body", $data);

        }
    }

    public function get_done(){
        $done = $this->input->post('done');
        $items_offset = (int) $this->input->get('items_offset');
        $data = array (
            'done' => $done,
            'offset' => $items_offset,
        );

        $get_done = $this->switches_model->get_done($done === '' ? null : $done);
        $items_offset = (int) $this->input->get('items_offset');
        $data = array(
            'items' => $get_done,
            'offset' => $items_offset,
        );

        $this->load->view("modules/switches/switches_table_body", $data);
    }
}

?>
