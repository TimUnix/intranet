<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cpu_module extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'cpu_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'cpu_module');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $get_cpu= $this->cpu_model->get_cpu();
            $search_history = $this->cpu_model->get_search_history_for_user($g_id);
            $columns = $this->users_model->get_table_columns('cpu_module', $g_id);

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'get_cpu' => $get_cpu,
                'columns' => json_decode($columns->cpu_module_columns)
            );
            $userdata['search_history'] =  $search_history;
            $userdata['field'] = $this->cpu_model->get_field();
            

            $this->load->view('modules/cpu_module/cpu_main_page', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    function request() {
        if ($this->uri->rsegment(3) == 'create_listing') {
            self::_request_create_listing();
        }
        
        if ($this->uri->rsegment(3) == "update_columns") {
            self::_request_update_columns();
        }
        if ($this->uri->rsegment(3) == "delete_cpu") {
            self::_request_delete_cpu();
        }if ($this->uri->rsegment(3) == "insertcpu") {
            self::_request_insert_cpu();
        }
    }
    function getcpu(){
        $get_cpu['cpu']= $this->cpu_model->get_cpu();
        $user = $this->session->userdata('user');
        $columns = $this->users_model->get_table_columns('cpu_module', $user['id']);
        $get_cpu['columns'] = json_decode($columns->cpu_module_columns);
        $this->load->view('modules/cpu_module/ajax/table', $get_cpu);
    }
    function get_cpu_specific(){
        $partnumber = $this->input->post('part_number');
        $sortOrder = $this->input->post('sortOrder');
        $orderBy = $this->input->post('orderBy');echo $sortOrder;
        
        $data_sess = $this->session->userdata('user');
        $user_email = $data_sess['email'];
        $get_user_id = $this->cpu_model->get_user_id($user_email);
        foreach($get_user_id as $id){
            $user_id = $id->id;
            $get_search_exist = $this->cpu_model->get_search_exist($user_id, $partnumber);
            if($get_search_exist){
                $data = array('updated'=> date('Y-m-d H:i:s'));
                $insert_search = $this->cpu_model->update_cpu_search($user_id, $partnumber, $data);
            }else{echo 1;
                $data = array(
                    'user_id' => $user_id,
                    'search' => $partnumber,
                    'updated' => date('Y-m-d H:i:s')
                );
                $this->cpu_model->insert_cpu_search($data);
            }
            

        }

        $user = $this->session->userdata('user');
        $columns = $this->users_model->get_table_columns('cpu_module', $user['id']);
        $get_cpu['columns'] = json_decode($columns->cpu_module_columns);
        
        $get_cpu['cpu']= $this->cpu_model->get_cpu_search($partnumber, $sortOrder, $orderBy);
        $this->load->view('modules/cpu_module/ajax/table', $get_cpu);
        

    }

    private function detect_listing_action() {
        $referred = $this->input->get('referred');
        $action = $this->input->get('createtask');
        $sku = $this->input->get('new_listing');
        $price = $this->input->get('price');

        if (isset($referred) && $referred == '1') {
            return array(
                'action' => $action,
                'sku' => $sku,
                'price' => $price
            );

        }

        return null;
    }

    public function export_csv() {
        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        
        try {
            $csv = $this->cpu_model->generate_csv(
                trim($search),
                $sort_order,
                $order_by
                
               
            );
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function load_listing() {
        $user = $this->session->userdata('user');

        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        $request_channels = $this->input->post('channels');
        $request_actions = $this->input->post('actions');
        $request_users = $this->input->post('users');
        $banner = $this->input->post('banner');
        $search_column = $this->input->post('search_column');
        $is_done_shown = $this->input->post('is_done_shown') === 'true';
        $is_only_me_enabled = $this->input->post('is_only_me_enabled') === 'true';

        $channels = isset($request_channels) && $request_channels !== '' ? explode(',', $request_channels) : null;
        $actions = isset($request_actions) && $request_actions !== '' ? explode(',', $request_actions) : null;

        if ($is_only_me_enabled) {
            $users = array(trim($user['firstname']) . ' ' . trim($user['lastname']));
        } else {
            $users = isset($request_users) && $request_users !== '' ? explode(',', $request_users) : null;
        }

        $data['listing_added'] = $this->task_model->load(
            trim($search),
            $order_by,
            $sort_order,
            $channels,
            $actions,
            $banner,
            $users,
            $search_column,
            $is_done_shown
        );

        $columns = $this->users_model->get_table_columns('task', $user['id']);
        $data['columns'] = json_decode($columns->task_columns);

        $this->load->view("modules/task/ajax/listingtable", $data);
    }

    public function _request_update_columns() {
        $data = $this->input->post('data');
        $partnumber = $data['partnumber'];
        $stepcode = $data['stepcode'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->cpu_model->update_cpucol($col, $stepcode, $partnumber);
        
    }

    public function _request_delete_cpu(){
        $data = $this->input->post('data');
        $partnumber = $data['partnumber'];
        $stepcode = $data['stepcode'];
        $orderby = $data['orderby'];
        $sortorder = $data['sortorder'];
        $partnumbertext = $data['partnumbertext'];
        
        $deleteone = $this->cpu_model->delete_cpucol($partnumber, $stepcode);
        
            $get_cpu['cpu']= $this->cpu_model->get_cpu_search($partnumbertext, $sortorder, $orderby);
            $this->load->view('modules/cpu_module/ajax/table', $get_cpu);
        


        
    }


    public function _request_insert_cpu(){
        $partnumber = $this->input->post('partnumber');
        $stepcode = $this->input->post('stepcode');
        $speed = $this->input->post('speed');
        $numberofcores = $this->input->post('numberofcores');
        $codename = $this->input->post('codename');
        $tdp = $this->input->post('tdp');
        $cache = $this->input->post('cache');
        $socketsupported = $this->input->post('socketsupported');
        $grade = $this->input->post('grade');

        $this->load->helper(array('form', 'url'));

         $this->load->library('form_validation');

         
        $this->form_validation->set_rules('partnumber', 'PartNumber', 'trim|required');
        $this->form_validation->set_rules('stepcode', 'Stepcode', 'trim|required');
        $this->form_validation->set_rules('speed', 'Speed', 'trim|required');
        $this->form_validation->set_rules('numberofcores', 'Numberofcores', 'trim|required');
        $this->form_validation->set_rules('codename', 'Codename', 'trim|required');
        $this->form_validation->set_rules('tdp', 'Tdp', 'trim|required');
        $this->form_validation->set_rules('cache', 'Cache', 'trim|required');
        $this->form_validation->set_rules('socketsupported', 'Socketsupported', 'trim|required');
        $this->form_validation->set_rules('grade', 'Grade', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){
            /*$validation_message = $this->form_validation->error_array();
            foreach($validation_message as $valm){
                $emptyfield = $valm;
            }*/
            $message = validation_errors();
            $alert = "warning";
            $data = array(
                'status' => 200,
                'message' => $message,
                'isSuccess' => trim($alert)
            );
    
            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }else{
            $data = array(
                'part' =>  $partnumber,
                'stepcode' => $stepcode,
                'speed' => $speed,
                'number_of_cores' => $numberofcores,
                'codename' => $codename,
                'thermal_design_power' => $tdp,
                'cpu_cache' => $cache,
                'sockets_supported' => $socketsupported,
                'grade' => $grade
            );
            $check_cpu = $this->cpu_model->check_cpu_part($partnumber, $stepcode);
            if($check_cpu == "PartNumber is Already Exist"){
                $message = "PartNumber is Already Exist";
                $alert = "warning";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }else if($check_cpu == "StepNumber is Already Exist"){
                $message = "StepNumber is Already Exist";
                $alert = "warning";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }else if($check_cpu == "Not Found"){
                $this->cpu_model->insert_new_cpu($data);
                $message = "CPU Inserted Successfully";
                $alert = "success";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }
        }
        
        ///$this->cpu_model->insert_new_cpu($data);                
    }

    

    

    

    
}
