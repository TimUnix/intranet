<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_items extends CI_Controller {
    private $is_user_logged_in;
    private $is_user_authorized;

    function __construct() {
        parent::__construct();
        $this->load->helper(array( 'url', 'nav_order', 'user_access' ));
        $this->load->model(array(
            'ebay_ad_draft_model',
            'ebay_items_model',
            'ebay_items_logs_model',
            'ebay_store_model',
            'ebay_business_policies_model',
            'ebay_campaigns_model',
            'ebay_category_specifics_model',
            'ebay_categories_model',
            'google_model',
            'navigation_model',
            'inventory_model',
            'users_model',
            'search_history_model',
            'task_model',
            'ebay_store_model',
            'keywords_model'
        ));

        $this->load->library('session');

        if (!$this->session->has_userdata('user')) {
            $this->is_user_logged_in = false;
            return;
        }

        $this->is_user_logged_in = true;

        $data_sess = $this->session->userdata('user');
        $get_user_id = $this->google_model->getFields($data_sess['email']);
        $user_id = $get_user_id->id;

        $is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

        $g_id = $is_google ? $user_id : $data_sess['id'];

        $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'ebay_items');
        $this->is_user_authorized = check_user_access_permissions($user_access_result->menu_access);
    }

    function index() {
        if (!$this->is_user_logged_in) {
            $data['data'] = array( 'page_name' => 'Login' );

            return $this->load->view('login', $data);
        }

        if (!$this->is_user_authorized) return redirect('access_denied');

        $data_sess = $this->session->userdata('user');

        $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";
        $g_id = $data_sess['id'];
        $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'ebay_items');

        $picture = $data_sess['picture'];

        if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
            $picture = base_url() . 'resources/img/noimage.png';
        }

        $nav = $this->navigation_model->get_navigation_menu($g_id);
        $ordered_nav = organize_nav_order($nav);

        $categories = $this->inventory_model->get_categories();
        $columns = $this->users_model->get_table_columns('ebay_items', $g_id);
        $request = $this->task_model->get_request();
        $channels = $this->ebay_items_model->load_channel();

        $data['data'] = array(
            'user_picture' => $picture,
            'user_firstname' => $data_sess['firstname'],
            'user_lastname' => $data_sess['lastname'],
            'user_email' => $data_sess['email'],
            'page_name'	=>	$user_access_result->menu_title,
            'nav_menu' => $ordered_nav,
            'columns' => json_decode($columns->ebay_items_columns),
            'actions' => $request,
            'categories' => $categories,
            'is_google' => $is_google,
            'channels' => $channels,
        );

        return $this->load->view('modules/ebay_items/index', $data);
    }

    function save_keyword() {
        $par = $this->input->post('par');
        $prase = $this->input->post('prase');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('prase', 'Prase', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data = array(
                'data' => array(
                    'message' => 'Prase is empty'
                )
            );

            return $this->output->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($data));
        }else{
            $exist_check_result = $this->keywords_model->check_keyword_exists($par, $prase);
            $is_keyword_existing = isset($exist_check_result->id) && $exist_check_result->id >= 0;

            if ($is_keyword_existing) {
                $data = array(
                    'data' => array(
                        'is_save_successful' => false,
                        'par' => $par,
                        'message' => 'duplicate prase'
                    )
                );

                return $this->output->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
            }

            $id = $this->keywords_model->save_keyword($par, $prase);
            $is_save_successful = isset($id) && $id !== '' && ((int) $id) >= 0;

            $data = array(
                'data' => array(
                    'is_save_successful' => $is_save_successful,
                    'par' => $par
                )
            );

            return $this->output->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($data));
        }
    }

    function show_condition_description_field() {
        return $this->load->view('modules/ebay_items/ajax/condition_description');
    }

    function load_business_policies() {
        $store_identifier = $this->input->post('store');

        switch($store_identifier) {
        case 'com':
            $store = 'UNIXSurplusCom';
            break;
        case 'net':
            $store = 'UNIXSurplusNet';
            break;
        case 'plus':
            $store = 'UNIXPlusCom';
            break;
        case 'itr':
            $store = 'ITRecycleNow';
            break;
        }

        $shipping_policies = $this->ebay_business_policies_model->load_policies_by_store_and_type($store, 'shipping');

        $data = array(
            'shipping_policies' => $shipping_policies
        );

        return $this->load->view('modules/ebay_items/ajax/business_policies', $data);
    }

    function get_category_specifics() {
        $category_id = $this->input->post('category_id');
        $category_keys = $this->ebay_category_specifics_model->keys($category_id);

        $keys = array();

        foreach ($category_keys as $key) {
            $values = $this->ebay_category_specifics_model->values($key->id);
            $key->values = $values;
            $keys[] = $key;
        }

        $conditions = $this->ebay_category_specifics_model->conditions($category_id);

        $data = array(
            'keys' => $keys,
            'conditions' => $conditions
        );

        return $this->load->view('modules/ebay_items/ajax/category_specifics', $data);
    }

    public function get_logs(){
        $item_number = $this->input->post('item_number');
        $data['logs'] = $this->ebay_items_logs_model->get_item_logs($item_number);
        $this->load->view('modules/ebay_items/ajax/table_modal', $data);
    }

    public function change_ads_popover() {
        $this->load->view('modules/ebay_items/ajax/change_ads_popover');
    }

    public function search_item() {
        $user = $this->session->userdata('user');
        $items_offset = (int) $this->input->post('items_offset');
        $searchItem = ltrim($this->input->post('searchItem'));
        $searchSelected = $this->input->post('searchSelected');
        $is_filtered_none_sold = $this->input->post('filterNoneSold');
        $categories = $this->input->post('categories');
        $channels = $this->input->post('channels');
        $order_by = $this->input->post('orderBy');
        $sort_order = $this->input->post('sortOrder'); 
        $switch_filter = $this->input->post('switches');
     
        if ($searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all') {
            $explode_search_item = explode(" ",($searchItem));
        }
		
        try {
           	$items = $this->ebay_items_model->get(
				$searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all'
                ? $explode_search_item : $searchItem, 
                $searchSelected === '' ? null : $searchSelected,
                $categories,
                $channels,
                $order_by,
                $sort_order,
                $is_filtered_none_sold,
                $items_offset,
                $switch_filter === 'true'                                                             
        	);

        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
                                      
        if (is_array($items) && count($items) > 0) {
            $columns = $this->users_model->get_table_columns('ebay_items', $user['id']);
            $table_columns = json_decode($columns->ebay_items_columns);
			                   
            $data = array(                                          
                'is_filtered_none_sold' => $is_filtered_none_sold,
                'items' => $items,                                 
                'columns' => $table_columns,
                'offset' => $items_offset
            );

            return $this->load->view('modules/ebay_items/ajax/product_table_body', $data);
        }
        if($items_offset == 0){                   
            return $this->load->view('modules/ebay_items/no_items');
        }
        
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    function load_summary(){
        $user = $this->session->userdata('user');
        $searchItem = ltrim($this->input->post('searchItem'));
        $searchSelected = $this->input->post('searchSelected');
        $is_filtered_none_sold = $this->input->post('filterNoneSold');
        $categories = $this->input->post('categories');
        $channels = $this->input->post('channels');
        $order_by = $this->input->post('orderBy');
        $sort_order = $this->input->post('sortOrder'); 
        $switch_filter = $this->input->post('switches');
     
        if ($searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all') {
            $explode_search_item = explode(" ",($searchItem));
        }

        try {
            $items = $this->ebay_items_model->get_summary(
             $searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all'
             ? $explode_search_item : $searchItem, 
             $searchSelected === '' ? null : $searchSelected,
             $categories,
             $channels,
             $order_by,
             $sort_order,
             $is_filtered_none_sold,
             $switch_filter === 'true'                                                             
         );

        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
      
        if (is_array($items) && count($items) > 0) {
            $columns = $this->users_model->get_table_columns('ebay_items', $user['id']);
            $table_columns = json_decode($columns->ebay_items_columns);
			
            $total_count = count($items);

            $data = array(                                          
                'total' => $total_count,
            );

        }else{
            $data = array(                                          
                'total' => '0'
            );

        }
        return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($data));
        
    }

    //graph
    public function item_trend() {
        $item_number = $this->input->get('item_number');

        try {
            $trends_result = $this->ebay_items_model->item_trend($item_number);
            $trends_week = array_splice($trends_result, 1);
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        if (count($trends_week) > 0) {
            $trend_prices = array_map(function($trend) { return $trend->current_price; }, $trends_week);
            $prices_unique = array_unique($trend_prices);
            // Use array_values to reorder the indexes as array_unique may return inconsistent indices
            $prices = array_values($prices_unique);

            $last_trend = clone $trends_result[0];
            $trends_calculated = array();

            foreach ($trends_week as $trend) {
                $trend_calculated = clone $trend;
                $trend_calculated->quantity_sold = (int) $trend->quantity_sold - (int) $last_trend->quantity_sold;
                $trend_calculated->hit_count = (int) $trend->hit_count - (int) $last_trend->hit_count;
                $trend_calculated->velocity = number_format((float) $trend->total_sold_7_days / 7.0, 2, '.', '');

                $last_trend = clone $trend;
                $trends_calculated[] = $trend_calculated;
            }

            $data = array(
                'trends' => $trends_calculated
            );

            return $this->output->set_header(200)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($data));
        }

        $data = array( 'prices' => null, 'trends' => null );

        return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
    }

    public function save_search_history() {
        $search = $this->input->post('search');

        if (isset($search) && $search !== '') {
            $this->load->library('session');
            $user = $this->session->userdata('user');
            $id = $user['id'];

            $search_history_query = $this->search_history_model->check_users_history($id, $search);
            $is_history_existing = count($search_history_query) > 0;

            $data = array(
                'user_id' => $id,
                'search' => $search,
                'updated' => date('Y-m-d H:i:s')
            );

            if ($is_history_existing) {
                $this->search_history_model->update_search_history($data, 'user_id', $id, 'search', $search);
            } else {
                $this->search_history_model->insert_table($data);
            }
        }
    }

    public function generate_csv() {
        $columns = $this->input->post('columns');
        $searchItem = $this->input->post('search');
        $searchSelected = $this->input->post('searchSelected');
        $is_filtered_none_sold = $this->input->post('filterNoneSold');
        $categories = $this->input->post('categories');
        $channel_filters = $this->input->post('channels');

        if ($searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all'){
            $explode_search_item = explode(" ", $searchItem);
        }

        try {
            $csv = $this->ebay_items_model->export_csv(
				$searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all'
                ? $explode_search_item : $searchItem,
                $columns,
                $searchSelected === '' ? null : $searchSelected,
                $categories,
                $is_filtered_none_sold,
                $channel_filters
            );
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                    ->set_content_type('text/csv', 'utf-8')
                    ->set_output($csv);
    }

    function log_ebay_ad_change() {
        $this->load->library("session");

        $user = $this->session->userdata('user');
        $firstname = $user['firstname'];
        $lastname = $user['lastname'];
        $edited_by = trim($firstname) . " " . trim($lastname);
        $date_edited = date("Y-m-d H:i:s");

        $old_value = $this->input->post("old_value");
        $new_value = $this->input->post("new_value");
        $item_number = $this->input->post("item_number");
        $action_type_ads = $this->input->post("action_type_ads");

        $data_update = array(
            'item_number' => $item_number,
            'action_type_ads' => $action_type_ads,
            'old_value' => $old_value,
            'new_value'=> $new_value,
            'edited_by' => $edited_by,
            'date_edited' => $date_edited
        );

        try {
            $this->ebay_items_logs_model->insert($data_update);
        } catch (Exception $e) {
            $data = array(
                "is_error" => true,
                "msg" => "An unexpected error occurred. Please contact the site admin."
            );

            return $this->output->set_header(200)
                                ->set_content_type("application/json", "utf-8")
                                ->set_output(json_encode($data));
        }
    }

    public function create_listing() {
        $sku = $this->input->post('sku');
        $channel = $this->input->post('channel');
        $shipping = $this->input->post('shipping');
        $weight = $this->input->post('weight');
        $price = $this->input->post('price');
        $request = $this->input->post('request');
        $banner = $this->input->post('banner');
        $notes = $this->input->post('notes');
        $photo = $this->input->post('photo');
        $moved = $this->input->post('moved');
        $lot = $this->input->post('lot');

        $exit_sku_empty = 0;
        $exit_price_empty = 1;

        if ($banner == "") {
            $banner = "NO";
        }

        if ($weight === '') {
            $weight = 0;
        }

        $data_sess = $this->session->userdata('user');

        $firstname = $data_sess['firstname'];
        $lastname = $data_sess['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);
        if ($sku === '') {
            return $exit_sku_empty;
        } else if (($request == 1 || $request == 3) && $price === '') {
            return $exit_price_empty;
        } else {
            $data = array(
                'date_requested' => date("Y-m-d H-i-s"),
                'request' => $request,
                'request_last_edited' => date("Y-m-d H-i-s"),
                'request_last_edited_by' => $fullname,
                'sku' => $sku,
                'photo' => $photo,
                'photo_last_edited' => date("Y-m-d H-i-s"),
                'photo_last_edited_by' => $fullname,
                'channel' => $channel,
                'channel_last_edited' => date("Y-m-d H-i-s"),
                'channel_last_edited_by' => $fullname,
                'lot' => $lot,
                'lot_last_edited' => date("Y-m-d H-i-s"),
                'lot_last_edited_by' => $fullname,
                'shipping' => $shipping,
                'shipping_last_edited' => date("Y-m-d H-i-s"),
                'shipping_last_edited_by' => $fullname,
                'banner' => $banner,
                'banner_last_edited' => date("Y-m-d H-i-s"),
                'banner_last_edited_by' => $fullname,
                'request_by' => $fullname,
                'draft_id' => '0',
                'draft_by' => '',
                'listing_item_id' => '0',
                'listing_by' => ''
            );
            // notes
            if (isset($moved) && $moved !== '') {
                $data['moved'] = $moved;
                $data['moved_last_edited'] = date("Y-m-d H-i-s");
                $data['moved_last_edited_by'] = $fullname;
            }

            if (isset($price) && $price !== '' && (float) $price > 0) {
                $data['price'] = $price;
                $data['price_last_edited'] = date("Y-m-d H-i-s");
                $data['price_last_edited_by'] = $fullname;
            }

            if (isset($weight) && $weight !== '' && (float) $weight > 0) {
                $data['weight'] = $weight;
                $data['weight_last_edited'] = date("Y-m-d H-i-s");
                $data['weight_last_edited_by'] = $fullname;
            }

            if (isset($notes) && $notes !== '') {
                $data['notes'] = $notes;
                $data['notes_last_edited'] = date("Y-m-d H-i-s");
                $data['notes_last_edited_by'] = $fullname;
            }

            $this->task_model->insert_listing($data);
            $data['listing_added'] = $this->task_model->show_added_listing();
            $data['channels'] = $this->task_model->get_channel();
            $data['actions'] = $this->task_model->get_request();
            $this->load->view("modules/task/ajax/listingtable", $data);
            //echo 2;
            //$this->task_model->insert_listing($sku);
        }
    }

    public function update_description(){
        $id = $this->input->post('id');
        $description = $this->input->post('description');
        $this->ebay_items_model->update_description($id, $description);
    }

    public function update_price() {
        $id = $this->input->post('id');
        $price = $this->input->post('price');
        $this->ebay_items_model->update_price($id, $price);
    }

    function update_quantity() {
        $id = $this->input->post('id');
        $quantity = $this->input->post('quantity');
        $this->ebay_items_model->update_quantity($id, $quantity);
    }

    function upload_photo() {
        $data = array();
        $photo_count = count($_FILES["newEbayAdPhoto"]["name"]);
        $files = $_FILES;

        $empty_response = array();

        for ($photo_iterator = 0; $photo_iterator < $photo_count; $photo_iterator++) {
            if (!empty($_FILES["newEbayAdPhoto"]["name"][$photo_iterator])) {
                $_FILES["newEbayAdPhoto"]["name"] = $files["newEbayAdPhoto"]["name"][$photo_iterator];
                $_FILES["newEbayAdPhoto"]["type"] = $files["newEbayAdPhoto"]["type"][$photo_iterator];
                $_FILES["newEbayAdPhoto"]["tmp_name"] = $files["newEbayAdPhoto"]["tmp_name"][$photo_iterator];
                $_FILES["newEbayAdPhoto"]["error"] = $files["newEbayAdPhoto"]["error"][$photo_iterator];
                $_FILES["newEbayAdPhoto"]["size"] = $files["newEbayAdPhoto"]["size"][$photo_iterator];

                $upload_config = array(
                    "upload_path" => "./uploads/",
                    "allowed_types" => "gif|jpg|jpeg|png|tiff"
                );

                $this->load->library('upload', $upload_config);

                if ($this->upload->do_upload("newEbayAdPhoto")) {
                    $data[] = array(
                        "filename" => $this->upload->data('file_name'),
                        "filetype" => $this->upload->data('file_type')
                    );
                } else {
                    $empty_response['error'] = $this->upload->display_errors();
                }
            }
        }

        if (count($data) > 0) {
            return $this->output->set_header(200)
                                ->set_content_type("application/json", "utf-8")
                                ->set_output(json_encode($data));
        }

        return $this->output->set_header(200)
                            ->set_content_type("application/json", "utf-8")
                            ->set_output(json_encode($empty_response));
    }

    public function get_total_quantity_sold(){
        $sku = $this->input->post('sku');
        $sold_ebay_items = $this->ebay_items_model->total_sold_ebay_items($sku);
        $sold_ebay_archive = $this->ebay_items_model->total_sold_ebay_archive($sku);

        foreach($sold_ebay_items as $ebay_item){
            $sku_ebay_checker = $ebay_item->sku;
            $quantity_sold_ebay = $ebay_item->quantity_sold;
            $description_sold_ebay = $ebay_item->title;
        }

        foreach ($sold_ebay_archive as $ebay_archive ) {
            $sku_archive_checker = $ebay_item->sku;
            $quantity_sold_archive = $ebay_archive->quantity_sold;
            $description_sold_archive = $ebay_item->title;
        }

        if($quantity_sold_ebay >= $quantity_sold_archive){
            $get_quantity_sold = $quantity_sold_ebay;
            $get_description = $description_sold_ebay;
        }else if($quantity_sold_ebay < $quantity_sold_archive){
            $quantity_sold_ebay = intval($quantity_sold_ebay);
            $quantity_sold_archive = intval($quantity_sold_archive);
            $get_description = $description_sold_ebay;
            $get_quantity_sold = $quantity_sold_ebay + $quantity_sold_archive;
        }
        else if(isset($sku_ebay_checker) && empty($sku_archive_checker)){
            $get_quantity_sold = $quantity_sold_ebay;
            $get_description = $description_sold_ebay;
        }

        $data = array(
            'sku' => $sku,
            'description' => $get_description,
            'quantity_sold' => $get_quantity_sold
        );

        return $this->load->view('modules/ebay_items/ajax/sku_quantity_body', $data);
    }

    public function new() {
        if (!$this->is_user_logged_in) {
            $data['data'] = array( 'page_name' => 'Login' );

            return $this->load->view('login', $data);
        }

        if (!$this->is_user_authorized) {
            return redirect('access_denied');
        }


        $userdata = $this->session->userdata('user');
        $is_google = isset($userdata['is_google']) ? $userdata['is_google'] : "";

        $g_id = $userdata['id'];
        $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'ebay_items');

        $picture = $userdata['picture'];

        if (is_null($userdata['picture']) || $userdata['picture'] == '') {
            $picture = base_url() . 'resources/img/noimage.png';
        }

        $nav = $this->navigation_model->get_navigation_menu($g_id);
        $ordered_nav = organize_nav_order($nav);

        $business_policies = array(
            'shipping' => $this->ebay_business_policies_model->load_policies_by_store_and_type(
                'UNIXSurplusCom', 'shipping'
            ),
            'payment' => $this->ebay_business_policies_model->load_policies_by_store_and_type(
                'UNIXSurplusCom', 'payment'
            ),
            'return' => $this->ebay_business_policies_model->load_policies_by_store_and_type(
                'UNIXSurplusCom', 'return'
            )
        );

        $campaigns = $this->ebay_campaigns_model->load_by_store('UNIXSurplusCom');
        $create_ad_categories = $this->ebay_categories_model->load_all();

        $drafts_result = $this->ebay_ad_draft_model->load_for_user($g_id);
        $drafts = array();

        foreach ($drafts_result as $draft) {
            $drafts[$draft->draft_id][$draft->field] = $draft->value;

            $date = date("D, M d, Y h:i A", strtotime($draft->date_added));
            $drafts[$draft->draft_id]['date_added'] = $date;
        }

        $data = array (
            'data' => array(
                'user_picture' => $picture,
                'user_firstname' => $userdata['firstname'],
                'user_lastname' => $userdata['lastname'],
                'user_email' => $userdata['email'],
                'page_name'	=>	$user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google
            ),
            'business_policies' => $business_policies,
            'campaigns' => $campaigns,
            'create_ad_categories' => $create_ad_categories,
            'drafts' => $drafts
        );

        return $this->load->view('modules/ebay_items/new', $data);
    }

    /**
     * Save the create ebay ad form as a draft
     */
    public function save_ebay_ad_draft() {
        $fields = $this->input->post('fields');

        $session_data = $this->session->userdata('user');
        $user_id = $session_data['id'];

        $this->ebay_ad_draft_model->save_draft($user_id, $fields);

        $data = array(
            'title' => 'Draft',
            'message' => 'Draft added.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }


    /**
     * Archive a draft based on the id
     */
    public function archive_draft() {
        $id = $this->input->post('id');

        $this->ebay_ad_draft_model->archive_draft($id);
    }

    public function draft_fields() {
        $id = $this->input->post('id');

        $fields = $this->ebay_ad_draft_model->draft_fields($id);

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($fields));
    }
}
