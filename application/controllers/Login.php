<?php

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array( 'url', 'nav_order', 'user_access' ));
		$this->load->model(array(
            'users_model',
            'inventory_model',
            'navigation_model',
            'configuration_model'
        ));
		error_reporting(E_ERROR | E_PARSE);
	}

	public function index(){
		//load session library
        $this->load->library('session');

		$this->load->view('login');
	}

	public function login(){
		//load session library
		$this->load->library('session');

        $userdata = $this->session->userdata('user');

        if (isset($userdata)) {
            return redirect('time_record');
        }

		try{

			$email = $_POST['email'];
			$password = $_POST['password'];

			if(empty($email) || empty($password)){
                $data['data'] = array(
                    'page_name' => 'Login'
                );

				return $this->load->view('login', $data);
			}

            if (!$this->isCompanyEmail($email)) {
				$this->session->set_flashdata('error','Invalid login. User not found');
                return $this->load->view('login');
            }

			$data = $this->users_model->login($email, $password);
			$data['is_google'] = false;

			if($data){
				$this->session->set_userdata('user', $data);

				if($this->session->userdata('user')){
                    $this->loginRedirect();
				}
				else {
					redirect('login');
				}
			}
			else{
				header('location:'.base_url().$this->index());
				$this->session->set_flashdata('error','Invalid login. User not found');
			}
		}catch(\Exception $e){
			die($e->getMessage());
		}
	}

	public function logout(){
		//load session library
		$this->load->library('session');
		$this->session->unset_userdata('user');


		$this->session->unset_userdata('access_token');

        $this->session->unset_userdata('user');

        $this->session->sess_destroy();

		$this->load->view('logout');
	}
	public function trys($email){
		//load session library
		$this->load->library('session');

		$email = $email;
		$password = "a";

		$this->session->set_userdata('user', $data);

		if($this->session->userdata('user')){
			$data_sess = $this->session->userdata('user');

			$userdata = array(
				'email'	=>	$data_sess['email'],
				'firstname'	=>	$data_sess['firstname'],
				'lastname'	=>	$data_sess['lastname'],
				'page_name'	=>	'Home',
				'home_name'	=>	'Home',
				'is_google' => false
			);

			redirect('home', $userdata);
		}
		else {
			redirect('login');
		}

    }

    public function googleLogin(){
	    include_once APPPATH . "libraries/vendor/autoload.php";

        $google_client = new Google_Client();
        $client_id = $this->configuration_model->getConfiguration('google_client_id');
         $google_client->setClientId($client_id[0]->value); //Define your ClientID

         $client_secret = $this->configuration_model->getConfiguration('google_client_secret');
         $google_client->setClientSecret($client_secret[0]->value); //Define your Client Secret Key
 	    $url = $this->configuration_model->getConfiguration('google');

         $google_client->setRedirectUri($url[0]->value); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        if(isset($_GET["code"]))
        {
            $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

            if(!isset($token["error"]))
            {
                $google_client->setAccessToken($token['access_token']);
                $this->session->set_userdata('access_token', $token['access_token']);
                $google_service = new Google_Service_Oauth2($google_client);
                $data = $google_service->userinfo->get();
                $current_datetime = date('Y-m-d H:i:s');

                if (!$this->isCompanyEmail($data->email)) {
                    $this->logout();
				    $this->session->set_flashdata('error', 'Invalid login. User not found.');
                    return redirect('login');
                }

                $user = $this->users_model->get_user_id($data['email']);

                //insert data
                $user_data = array(
                    'id' => $user[0]->id,
                    'login_oauth_uid' => $data->id,
                    'email'  => $data->email,
                    'firstname' => $data->given_name,
                    'lastname'  => $data->family_name,
                    'picture'=> $data->picture,
                    'access_token'=> $token['access_token'],
                    'google_id'=>$data->id,
                    'last_url'=>"NULL",
                    'created_at'=> $current_datetime,
                    'updated_at'=> $current_datetime,
                    'is_google'=> true
                );

                $this->session->set_userdata('user', $user_data);

                $id_max = $this->users_model->get_user_max();
                if(!$user) {
                    $user_insert = array(
                        'firstname' => $data->given_name,
                        'lastname'  => $data->family_name,
                        'email'  => $data->email,
                        'picture'=> $data->picture,
                        'access_token'=> $token['access_token'],
                        'google_id'=>$data->id,
                        'last_url'=>"NULL",
                        'created_at'=> $current_datetime,
                        'updated_at'=> $current_datetime
                    );

					$insert_id = $this->users_model->insert_users($user_insert);
					$this->users_model->insert_menu_items($insert_id);
					$this->users_model->setdefaultmenu($insert_id);

					$user_data = array(
						'id' => $insert_id,
						'login_oauth_uid' => $data->id,
						'email'  => $data->email,
						'firstname' => $data->given_name,
						'lastname'  => $data->family_name,
						'picture'=> $data->picture,
						'access_token'=> $token['access_token'],
						'google_id'=>$data->id,
						'last_url'=>"NULL",
						'created_at'=> $current_datetime,
						'updated_at'=> $current_datetime,
						'is_google'=> true
					);

					$this->session->set_userdata('user', $user_data);
                }
            }
        }

        $login_button = '';
        if(!$this->session->userdata('access_token'))
        {
            $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="'.base_url().'images/glogin.png" /></a>';
            $data['login_button'] = $login_button;
            $loginbutton = $google_client->createAuthUrl();

            echo "<a class='btn btn-danger btn-lg mt-2 btn-block' href='$loginbutton' role='button' style='width: 253%;'><i class='fab fa-google mr-2' aria-hidden='true'></i>Login with Google</a>";
        } else {
            if ($this->session->userdata('user')){
                $this->loginRedirect();
            } else {
                $this->logout();
                redirect('login');
            }
        }
    }

    public function googleLogout(){
	    $this->load->library('session');
        $this->session->unset_userdata('access_token');

        $this->session->unset_userdata('user');

        $this->session->sess_destroy();

        $this->load->view('logout');
    }

    private function loginRedirect() {
        // $userdata = $this->session->userdata('user');
        // $menu_item = $this->navigation_model->get_first_available_menu_item($userdata['id']);
        // $route = get_login_redirect_route($menu_item);

        redirect('time_record');
    }

    private function isCompanyEmail($email) {
        $separated = explode('@', $email);

        return $separated[1] === "unixsurplus.com";
    }
}

?>
