<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Motherboard extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'motherboard_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
           // $task = self::detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'motherboard');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            //$channels['channels'] = $this->task_model->get_channel();
           // $request = $this->task_model->get_request();
            $columns = $this->users_model->get_table_columns('motherboard', $g_id);


            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'  => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'columns' => json_decode($columns->motherboard_columns),
                'is_google' => $is_google
                //'task' => $task

            );

            $this->load->view('modules/motherboard/index', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function search_item() {
    	$user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
        	$items = $this->motherboard_model->get(
        	$searchItem,
        	$searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
        	);
        }catch (Exception $e){
        	 $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
            $columns = $this->users_model->get_table_columns('motherboard', $user['id']);
            $table_columns = json_decode($columns->motherboard_columns);
            
            $data = array(
                'items' => $items,
                'columns' => $table_columns
            );
            return $this->load->view('modules/motherboard/product_table_body', $data);
 
        
    }

    public function createListing(){
        $user = $this->session->userdata('user');
        $firstname = $user['firstname'];
        $lastname = $user['lastname'];
        $fullname = trim($firstname) . " " . trim($lastname);

        $request = $this->input->post('request');
        $sku = $this->input->post('sku');
        $barcode = $this->input->post('barcode');
        $brand = $this->input->post('brand');
        $model = $this->input->post('model');
        $rev_version = $this->input->post('rev_version');
        $qty_cpu = $this->input->post('qty_cpu');
        $memory = $this->input->post('memory');
        $shield = $this->input->post('shield');
        $heatsink = $this->input->post('heatsink');

        $exit_sku_empty = 0;

        if($sku === '' ){
            return $exit_sku_empty;
        }else{
            $data = array(
                'request' => $request,
                'sku' => $sku,
                'barcode' => $barcode,
                'brand' => $brand,
                'model' => $model,
                'rev_version' => $rev_version,
                'qty_cpu' => $qty_cpu,
                'memory' => $memory,
                'shield' => $shield,
                'heatsink' => $heatsink
            );

        $this->motherboard_model->insert_listing($data);
        $data['listing_added'] = $this->motherboard_model->show_added_listing();
        $this->load->view("modules/motherboard/product_table_body", $data);

        }
    }

    public function load_listing_count() {
        $user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
            $items = $this->motherboard_model->count(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
        }catch (Exception $e){
             $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

            return $this->output->set_header(200)
                        ->set_content_type('text/html', 'utf-8')
                        ->set_output($items->listing_count);
    }

    public function update_columns() {
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $bar = $data['barcode'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->motherboard_model->update_motherboardcol($col, $sku, $bar);
    }

     public function generate_csv() {
        $searchItem = $this->input->get('searchItem');
        $sort_order = $this->input->get('sortOrder');
        $order_by = $this->input->get('orderBy');
        $searchSelected = $this->input->get('searchSelected');

        try {
            $csv = $this->motherboard_model->export_csv(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
  
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }
}

?>
