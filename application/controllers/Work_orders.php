<?php

class Work_orders extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access', 'median_calculator'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'reports_model',
            'test_model',
            'research_model',
            'search_history_model',
            'configuration_model',
            'inventory_model',
            'chassis_model',
            'work_orders_model',
            'assembly_model'
        ));
        error_reporting(E_ERROR | E_PARSE);
    }

    public function index(){
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if($this->session->has_userdata('user')){
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

            if($is_google == true){
                $g_id = $user_id;
            }else{
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'work_orders');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $columns = $this->users_model->get_table_columns('work_orders', $g_id);


            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }
            date_default_timezone_set('America/Los_Angeles');
            $date = date('Y-m-d H:i:s');
        
            $search_query = $this->input->post('search');
            $search_history = $this->search_history_model->get_search_history_for_user($user_id);
            $build_id = $this->work_orders_model->get_buildID();
            $ship_method = $this->work_orders_model->get_shipMethod();
            $drives = $this->chassis_model->get_drives();
            $warranty = $this->work_orders_model->get_warranty();
            $mounting = $this->chassis_model->get_mounting();
            $get_last_id = $this->work_orders_model->get_last_id();
            $chassis = $this->chassis_model->get_chassis();
            $motherboard = $this->chassis_model->get_all_motherboards();
           
            $userdata['data'] = array(
                'user_picture'  =>  $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name' => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'search_history' => $search_history,
                'search_query' => $search_query,
                'build_id' => $build_id,
                'created_date' => $date,
                'ship_method' => $ship_method,
                'drives' => $drives,
                'warranty' => $warranty,
                'mounting' => $mounting,
                'chassis' => $chassis,
                'motherboard' => $motherboard,
                'cpu' => $search_cpu,
                'columns' => json_decode($columns->work_orders_columns)
			);
            $userdata['chassis'] = $this->chassis_model->get_chassis();
            $userdata['cpu_'] = $this->chassis_model->get_cpu();
            $userdata['memory'] = $this->chassis_model->get_memory();
            $userdata['mounting'] = $this->chassis_model->get_mounting();
            //$userdata['drives'] = $this->chassis_model->get_drives();
            $userdata['motherboard_s'] = $this->chassis_model->get_all_motherboards();
            $userdata['cpu'] = $this->chassis_model->get_cpu();
            $userdata['motherboard'] = $this->chassis_model->get_all_motherboards();
            $userdata['memory_'] = $this->chassis_model->get_memory();
            $userdata['get_work_orders'] = $this->chassis_model->get_work_orders();
           // $userdata['warranty'] = $this->work_orders_model->get_warranty();
            $userdata['build_id'] = $this->work_orders_model->get_buildID();
            $userdata['sample'] = 1;
            $userdata['front_caddies'] = $this->work_orders_model->get_front_caddies();
            $userdata['rear_caddies'] = $this->work_orders_model->get_rear_caddies();
			
			$this->load->view('modules/work_orders/work_orders_main_page', $userdata);
		}
		else{
            $data['data'] = array(
                'page_name' => 'Login'
            );

			$this->load->view('login', $data);
		}
		
        
	}

    function request(){
        if ($this->uri->rsegment(3) == "load_work_orders") {
            self::_request_load_work_orders();
        }

        if ($this->uri->rsegment(3) == "search_ebay") {
            self::_request_search_ebay();
        } if ($this->uri->rsegment(3) == "search_chassis") {
            self::_request_search_chassis();
        }
        if ($this->uri->rsegment(3) == "motherboard") {
            self::_request_motherboard();
        }if ($this->uri->rsegment(3) == "cpu") {
            self::_request_cpu();
        }if ($this->uri->rsegment(3) == "get_cpu_for_motherboard") {
            self::_request_get_cpu_for_motherboard();
        }if ($this->uri->rsegment(3) == "memory") {
            self::_request_memory();
        }if ($this->uri->rsegment(3) == "get_memory_for_motherboard") {
            self::_request_get_memory_for_motherboard();
        }if ($this->uri->rsegment(3) == "add_order") {
            self::_request_add_order();
        }if ($this->uri->rsegment(3) == "delete_order") {
            self::_request_delete_order();
        }if ($this->uri->rsegment(3) == "view_work_order") {
            self::_request_view_work_order();
        }if ($this->uri->rsegment(3) == "add_order_filter_motherboard") {
            self::_request_add_order_filter_motherboard();
        }if ($this->uri->rsegment(3) == "get_build") {
            self::_request_get_build();
        }
    }


    public function get_motherboard(){
        $build_id = $this->input->post('build_id');

    }

    public function _request_get_build(){
        $build_id = $this->input->post('build_id');
        $get_build = $this->assembly_model->get_build($build_id);
        if($get_build){
            foreach($get_build as $get_build_data){
                $build_id = $get_build_data->build_id;
                $sales_rep = $get_build_data->sales_rep;
                $chassis = $get_build_data->chassis;
                $backplane = $get_build_data->backplane;
                $drivebays = $get_build_data->drivebays;
                $rearbays = $get_build_data->rearbays;
                $ff = $get_build_data->ff;
                $rff = $get_build_data->rff;
                $psu = $get_build_data->psu;
                $bin = $get_build_data->bin;
                $front_caddies = $get_build_data->front_caddies;
                $fc_qty = $get_build_data->fc_qty;
                $rear_caddies = $get_build_data->rear_caddies;
                $rc_qty = $get_build_data->rc_qty;
                $drives = $get_build_data->drives;
                $drives_qty = $get_build_data->drives_qty;
                $drives1 = $get_build_data->drives1;
                $drives_qty1 = $get_build_data->drives_qty1;
                $motherboard = $get_build_data->motherboard;
                $motherboard_qty = $get_build_data->motherboard_qty;
                $cpu = $get_build_data->cpu;
                $cpu_qty = $get_build_data->cpu_qty;
                $memory = $get_build_data->memory;
                $memory_qty = $get_build_data->memory_qty;
                $slot = $get_build_data->slot;
                $slot_qty = $get_build_data->slot_qty;
                $slot1 = $get_build_data->slot1;
                $slot_qty1 = $get_build_data->slot_qty1;
                $psu_watts = $get_build_data->psu_watts;
                $psu_qty = $get_build_data->psu_qty;
                $mounting = $get_build_data->mounting;
                $mounting_qty = $get_build_data->mounting_qty;
                $warranty = $get_build_data->warranty;
                $created_date = $get_build_data->created_date;
                break;
            }

                $drive_ff = $drivebays." ".$ff;
                $rear_rff = $rearbays." ".$rff;
            $response = array(
                'build_id' => $get_build_data->build_id,
                'sales_rep' => $get_build_data->sales_rep,
                'chassis' => $get_build_data->chassis,
                'backplane' => $get_build_data->backplane,
                'drivebays' => $drive_ff,
                'rearbays' => $rear_rff,
                'node' => $get_build_data->node,
                'psu' => $get_build_data->psu,
                'bin' => $get_build_data->bin,
                'front_caddies' => $get_build_data->front_caddies,
                'fc_qty' => $get_build_data->fc_qty,
                'rear_caddies' => $get_build_data->rear_caddies,
                'rc_qty' => $get_build_data->rc_qty,
                'drives' => $get_build_data->drives,
                'drives_qty' => $get_build_data->drives_qty,
                'drives2' => $get_build_data->drives2,
                'drives_qty2' => $get_build_data->drives_qty2,
                'drives3' => $get_build_data->drives3,
                'drives_qty3' => $get_build_data->drives_qty3,
                'motherboard' => $get_build_data->motherboard,
                'motherboard_qty' => $get_build_data->motherboard_qty,
                'cpu' => $get_build_data->cpu,
                'cpu_qty' => $get_build_data->cpu_qty,
                'memory' => $get_build_data->memory,
                'memory_qty' => $get_build_data->memory_qty,
                'slot' => $get_build_data->slot,
                'slot_qty' => $get_build_data->slot_qty,
                'slot2' => $get_build_data->slot2,
                'slot_qty2' => $get_build_data->slot_qty2,
                'slot3' => $get_build_data->slot3,
                'slot_qty3' => $get_build_data->slot_qty3,
                'psu_watts' => $get_build_data->psu_watts,
                'psu_qty' => $get_build_data->psu_qty,
                'mounting' => $get_build_data->mounting,
                'mounting_qty' => $get_build_data->mounting_qty,
                'warranty' => $get_build_data->warranty,
                'created_date' => $get_build_data->created_date
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
            
        }
    }

    public function search_item(){
        $user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
            $items = $this->work_orders_model->get_item(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
        }catch (Exception $e){
             $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }
            $columns = $this->users_model->get_table_columns('work_orders', $user['id']);
           $table_columns = json_decode($columns->assembly_columns);
            
            $data = array(
                'items' => $items,
                'columns' => $table_columns
            );
            return $this->load->view('modules/work_orders/product_table_body', $data);
    }

    public function load_listing_count() {
        $user = $this->session->userdata('user');
        $searchItem = $this->input->get('searchItem');
        $searchSelected = $this->input->get('searchSelected');
        $order_by = $this->input->get('orderBy');
        $sort_order = $this->input->get('sortOrder');

        try{
            $items = $this->work_orders_model->count(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
        }catch (Exception $e){
             $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

            return $this->output->set_header(200)
                        ->set_content_type('text/html', 'utf-8')
                        ->set_output($items->listing_count);
    }

     public function generate_csv() {
        $searchItem = $this->input->get('searchItem');
        $sort_order = $this->input->get('sortOrder');
        $order_by = $this->input->get('orderBy');
        $searchSelected = $this->input->get('searchSelected');


        try {
            $csv = $this->work_orders_model->export_csv(
            $searchItem,
            $searchSelected === '' ? null : $searchSelected,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order
            );
  
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );
            
            return $this->output->set_header(500)
                        ->set_content_type('application/json', 'utf-8')
                        ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function front_caddies_qty_checker(){
        $qty_front_caddies = $this->input->post('qty_front_caddies');
        $drivebays = $this->input->post('drivebays');
        

        $explode_dbays = explode(' ', $drivebays);
        $first_dbay = $explode_dbays[0];
        
        if($qty_front_caddies > $first_dbay){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }

    }

    public function rear_caddies_qty_checker(){
        $qty_rear_caddies = $this->input->post('qty_rear_caddies');
        $rearbays = $this->input->post('rearbays');

        $explode_rbays = explode(' ', $rearbays);
        $first_rbay = $explode_rbays[0];

        if($qty_rear_caddies > $first_rbay){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }

    }

    public function motherboard_qty_checker(){
        $nodes = $this->input->post('nodes');
        $qty_motherboard = $this->input->post('qty_motherboard');
        $qty_chassis = $this->input->post('qty_chassis');
        $cpu_total = $this->input->post('cpu_total');
        $memory_total = $this->input->post('memory_total');
    
        $max_mb = $nodes*$qty_chassis;
 
        if($qty_motherboard > $max_mb){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
    }

    public function automate_motherboard(){
        $nodes = $this->input->post('nodes');
        $qty_motherboard = $this->input->post('qty_motherboard');
        $qty_chassis = $this->input->post('qty_chassis');
        $cpu_total = $this->input->post('cpu_total');
        $memory_total = $this->input->post('memory_total');

        $chassis_cpu=$qty_chassis*$cpu_total*$qty_motherboard;
        $chassis_memory = $qty_chassis*$memory_total*$qty_motherboard;
        $motherboard_total = $qty_chassis*$qty_motherboard*$nodes;

        $data = array(
            'chassis_cpu' => $chassis_cpu,
            'chassis_memory' => $chassis_memory,
            'motherboard_total' => $motherboard_total
        );
        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function automate_cpu_qty(){
        $qty_cpu = $this->input->post('qty_cpu');
        $motherboard_total = $this->input->post('motherboard_total');
        $qty_chassis = $this->input->post('qty_chassis');

        $automate_cpu = $qty_cpu*$motherboard_total*$qty_chassis;
        $automate_cpu = intval($automate_cpu);
        if($automate_cpu == '0'){
            $automate_cpu = ' ';
        }
        $data = array(
            'automate_cpu' => $automate_cpu
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function automate_cpu_qty_mb(){
        $qty_cpu = $this->input->post('qty_cpu');
        $motherboard = $this->input->post('motherboard');

        $automate_cpu_mb = $qty_cpu*$motherboard;
        $automate_cpu_mb = intval($automate_cpu_mb);

        if($automate_cpu_mb == '0'){
            $automate_cpu_mb = ' ';
        }

        $data = array(
            'automate_cpu_mb' => $automate_cpu_mb
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function automate_memory_qty(){
        $qty_memory = $this->input->post('qty_memory');
        $motherboard_total = $this->input->post('motherboard_total');
        $qty_chassis = $this->input->post('qty_chassis');

        $automate_memory = $qty_memory*$motherboard_total*$qty_chassis;
        $automate_memory = intval($automate_memory);

        if($automate_memory == '0'){
            $automate_memory = ' ';
        }

        $data = array(
            'automate_memory' => $automate_memory
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function automate_memory_qty_mb(){
        $qty_memory = $this->input->post('qty_memory');
        $motherboard = $this->input->post('motherboard');

        $automate_memory_mb = $qty_memory*$motherboard;
        $automate_memory_mb = intval($automate_memory_mb);

        if($automate_memory_mb == '0'){
            $automate_memory_mb = ' ';
        }

        $data = array(
            'automate_memory_mb' => $automate_memory_mb
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    /*public function slot1_checker(){
        $psuchassis = $this->input->post('psuchassis');
        $qty_cntrlr = $this->input->post('qty_cntrlr');
        $qty_cntrlrtwo = $this->input->post('qty_cntrlrtwo');
        $qty_cntrlrthree = $this->input->post('qty_cntrlrthree');

        if($qty_cntrlrthree == ' ' || $qty_cntrlrtwo == ' ' || $qty_cntrlr == ' '){
            $qty_cntrlrthree == ' ';
            $qty_cntrlrtwo == ' ';
            $qty_cntrlr == ' ';
        }
        else if($qty_cntrlr>$psuchassis){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
        
    }

    public function slot2_checker(){
        $psuchassis = $this->input->post('psuchassis');
        $qty_cntrlr = $this->input->post('qty_cntrlr');
        $qty_cntrlrtwo = $this->input->post('qty_cntrlrtwo');
        $qty_cntrlrthree = $this->input->post('qty_cntrlrthree');

        if($qty_cntrlrthree == ' ' || $qty_cntrlrtwo == ' ' || $qty_cntrlr == ' '){
            $qty_cntrlrthree == ' ';
            $qty_cntrlrtwo == ' ';
            $qty_cntrlr == ' ';
        }
        else if($qty_cntrlrtwo>$psuchassis){
            $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
        
    }*/

    public function psu_checker(){
        $psuchassis = $this->input->post('psuchassis');
        $qty_psu = $this->input->post('qty_psu');

        if($qty_psu > $psuchassis){
             $data = array(
                'is_error' => true
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
        }
    }

    public function qty_chassis_checker(){
        $motherboard_total = $this->input->post('motherboard_total');
        $cpu_total = $this->input->post('cpu_total');
        $memory_total = $this->input->post('memory_total');
        $qty_chassis = $this->input->post('qty_chassis');


        $chassis_mb = $qty_chassis*$motherboard_total;
        $chassis_cpu = $qty_chassis*$cpu_total*$motherboard_total;
        $chassis_memory = $qty_chassis*$memory_total*$motherboard_total;


        $data = array(
            'chassis_mb' => $chassis_mb,
            'chassis_cpu' => $chassis_cpu,
            'chassis_memory' => $chassis_memory
        );
        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function qty_mounting_checker(){
        $mounting_qty = $this->input->post('mounting_qty');

        $data = array(
            'mounting_qty' => $mounting_qty
        );
        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function soft_delete_all(){
        $barcode = $this->input->post('barcode');
        $soft_delete = $this->work_orders_model->soft_delete_all($barcode);

        $this->load->view('modules/work_orders/product_table_body', $soft_delete);

    }

    public function soft_delete_each(){
        $work_order_id = $this->input->post('work_order_id');
        $soft_delete = $this->work_orders_model->soft_delete_each($work_order_id);

        $this->load->view('modules/work_orders/ajax/add_order_table_body', $soft_delete);

    }

    public function delete_row(){
        $barcode = $this->input->post('barcode');
        $delete_order = $this->work_orders_model->delete_row($barcode, $key);

         $this->load->view('modules/work_orders/product_table_body', $delete_order);
    }

    public function delete_row_add_order(){
        $work_order_id = $this->input->post('work_order_id');
        $delete_order = $this->work_orders_model->delete_row_add_order($work_order_id, $key);

         $this->load->view('modules/work_orders/ajax/add_order_table_body', $delete_order);
    }

    public function get_barcode(){

        $barcode = $this->input->post('barcode');

        $items = $this->work_orders_model->get_barcode($barcode);

        $data = array(
            'items' => $items
        );

        $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
    }

    public function check_barcode_exist(){
        $barcode = $this->input->post('barcode');
        $items = $this->work_orders_model->check_barcode_exist($barcode);
         

         if(count($items)>0){

            $data = array(
                'is_error' => true,
                'message' => "Barcode Exist"
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
         } 
            
    }

    public function get_barcode_from_wo(){

        $barcode = $this->input->post('barcode');

        $items = $this->work_orders_model->get_barcode_from_wo($barcode);

        $data = array(
            'items' => $items
        );

        $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
    }

    public function get_barcode_edit(){

        $barcode = $this->input->post('barcode');

        $items = $this->work_orders_model->get_barcode_from_wo($barcode);

        $data = array(
            'items' => $items
        );

        $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
    }

    public function get_customer_detail_edit(){
        $barcode = $this->input->post('barcode');
        $data_extact = $this->work_orders_model->get_customer_detail_edit($barcode);

        foreach($data_extact as $data_e){
            $ship_date = $data_e->ship_date;
            $ship_method = $data_e->ship_method;
            $client_name = $data_e->client_name;
            $contact_no = $data_e->contact_no;
            $email = $data_e->email;
            $address = $data_e->address;
        }
        $details_all = $client_name."\n".$address."\n".$contact_no;


        $data = array(
            'status' => 200,
            'ship_date' => $ship_date,
            'ship_method' => $ship_method,
            'details_all' => $details_all,

        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function get_chassis_detail_edit(){
        $chassis = $this->input->post('chassis');
        $front_caddies = $this->input->post('front_caddies');
        $rear_caddies = $this->input->post('rear_caddies');
        $item_harddrive_one = $this->input->post('item_harddrive_one');
        $item_harddrive_two = $this->input->post('item_harddrive_two');
        $item_harddrive_three = $this->input->post('item_harddrive_three');
        $motherboard = $this->input->post('motherboard');
        $cpu = $this->input->post('cpu');
        $memory = $this->input->post('memory');
        $cntrl = $this->input->post('cntrl');
        $controllertwo = $this->input->post('cntrltwo');
        $controllerthree = $this->input->post('cntrlthree');
        $psu = $this->input->post('psu');
        $mounting = $this->input->post('mounting');
        $warranty = $this->input->post('warranty');
        $os = $this->input->post('os');

        $data_extact = $this->work_orders_model->get_chassis_detail_edit($chassis, $front_caddies, 
            $rear_caddies, $item_harddrive_one, $item_harddrive_two, $item_harddrive_three, $motherboard, 
            $cpu, $memory, $cntrl, $controllertwo, $controllerthree, $psu, $mounting, $warranty, $os);

        foreach($data_extact as $data_e){
            $chassis = $data_e->chassis;
            $front_caddies = $data_e->front_caddies;
            $rear_caddies = $data_e->rear_caddies;
            $item_harddrive_one = $data_e->item_harddrive_one;
            $item_harddrive_two = $data_e->item_harddrive_two;
            $item_harddrive_three = $data_e->item_harddrive_three;
            $motherboard = $data_e->motherboard;
            $cpu = $data_e->cpu;
            $memory = $data_e->memory;
            $cntrl = $data_e->cntrl;
            $controllertwo = $data_e->controllertwo;
            $controllerthree = $data_e->controllerthree;
            $psu = $data_e->psu;
            $mounting = $data_e->mounting;
            $warranty = $data_e->warranty;
            $os = $data_e->os;
        }

        $data = array(
            'status' => 200,
            'chassis' => $chassis,
            'front_caddies' => $front_caddies,
            'rear_caddies' => $rear_caddies,
            'item_harddrive_one' => $item_harddrive_one,
            'item_harddrive_two' => $item_harddrive_two,
            'item_harddrive_three' => $item_harddrive_three,
            'motherboard' => $motherboard,
            'cpu' => $cpu,
            'memory' => $memory,
            'cntrl' => $cntrl,
            'cntrltwo' => $controllertwo,
            'cntrlthree' => $controllerthree,
            'psu' => $psu,
            'mounting' => $mounting,
            'warranty' => $warranty,
            'os' => $os
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function update_each_order(){
        
        
        
        
        
        $motherboard = $this->input->post('motherboard');
        $qty_motherboard = $this->input->post('qty_motherboard');
        $cpu = $this->input->post('cpu');
        $qty_cpu = $this->input->post('qty_cpu');
        $memory = $this->input->post('memory');
        $qty_memory = $this->input->post('qty_memory');
        $ob_nic = $this->input->post('ob_nic');
        $pcie = $this->input->post('pcie');
        $cntrl = $this->input->post('cntrl');
        $controllertwo = $this->input->post('cntrltwo');
        $controllerthree = $this->input->post('cntrlthree');
        $qty_cntrlr = $this->input->post('qty_cntrlr');
        $qty_cntrlrtwo = $this->input->post('qty_cntrlrtwo');
        $qty_cntrlrthree = $this->input->post('qty_cntrlrthree');
        $psu = $this->input->post('psu');
        $qty_psu = $this->input->post('qty_psu');
        $mounting = $this->input->post('mounting');
        $qty_mounting = $this->input->post('qty_mounting');
        $warranty = $this->input->post('warranty');
        $os = $this->input->post('os');
        $work_order_id = $this->input->post('work_order_id');
        $barcode = $this->input->post('barcode');
        $barcode_id = $this->input->post('barcode_id');
      
        

        $update_table = $this->work_orders_model->update_each_order_motherboard_only($motherboard,
                                                    $qty_motherboard,
                                                    $cpu,
                                                    $qty_cpu,
                                                    $memory,
                                                    $qty_memory,
                                                    $ob_nic,
                                                    $pcie,
                                                    $cntrl,
                                                    $controllertwo,
                                                    $controllerthree,
                                                    $qty_cntrlr,
                                                    $qty_cntrlrtwo,
                                                    $qty_cntrlrthree,
                                                    $psu,
                                                    $qty_psu,
                                                    $mounting,
                                                    $qty_mounting,
                                                    $warranty,
                                                    $os,
                                                    $work_order_id,
                                                    $barcode,
                                                    $barcode_id);


                                                    $items = $this->work_orders_model->get_barcode($barcode);

        
                                                    $table = $update_table;

        if($table == "orders"){
            $data = array(
                'items' => $items
            );
    
            $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
        }else if($table == "work_orders"){
            $items = $this->work_orders_model->get_barcode_from_wo($barcode_id);
            $data = array(
                'items' => $items
            );
            
            $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
        }


    }

    public function update_each_order_for_addOrder(){
        $chassis = $this->input->post('chassis');
        $qty_chassis = $this->input->post('qty_chassis');
        $backplane = $this->input->post('backplane');
        $drivebays = $this->input->post('drivebays');
        $rearbays = $this->input->post('rearbays');
        $nodes = $this->input->post('nodes');
        $psuchassis = $this->input->post('psuchassis');
        $bin = $this->input->post('bin');
        $front_caddies = $this->input->post('front_caddies');
        $qty_front_caddies = $this->input->post('qty_front_caddies');
        $rear_caddies = $this->input->post('rear_caddies');
        $qty_rear_caddies = $this->input->post('qty_rear_caddies');
        $item_harddrive_one = $this->input->post('item_harddrive_one');
        $item_harddrive_two = $this->input->post('item_harddrive_two');
        $item_harddrive_three = $this->input->post('item_harddrive_three');
        $qty_hard_drive_one = $this->input->post('qty_hard_drive_one');
        $qty_hard_drive_two = $this->input->post('qty_hard_drive_two');
        $qty_hard_drive_three = $this->input->post('qty_hard_drive_three');
        $motherboard = $this->input->post('motherboard');
        $qty_motherboard = $this->input->post('qty_motherboard');
        $cpu = $this->input->post('cpu');
        $qty_cpu = $this->input->post('qty_cpu');
        $memory = $this->input->post('memory');
        $qty_memory = $this->input->post('qty_memory');


        $ob_nic = $this->input->post('ob_nic');
        $pcie = $this->input->post('pcie');

        $cntrl = $this->input->post('cntrl');
        $controllertwo = $this->input->post('cntrltwo');
        $controllerthree = $this->input->post('cntrlthree');
        $qty_cntrlr = $this->input->post('qty_cntrlr');
        $qty_cntrlrtwo = $this->input->post('qty_cntrlrtwo');
        $qty_cntrlrthree = $this->input->post('qty_cntrlrthree');
        $psu = $this->input->post('psu');
        $qty_psu = $this->input->post('qty_psu');
        $mounting = $this->input->post('mounting');
        $qty_mounting = $this->input->post('qty_mounting');
        $warranty = $this->input->post('warranty');
        $os = $this->input->post('os');
        $work_order_id = $this->input->post('work_order_id');
        $barcode = $this->input->post('barcode');
        $barcode_id = $this->input->post('barcode_id');
      
        

        $update_table = $this->work_orders_model->update_each_order_for_addOrder( $chassis,
        $qty_chassis,
        $backplane,
        $drivebays,
        $rearbays,
        $nodes,
        $psuchassis,
        $bin,
        $front_caddies,
        $qty_front_caddies,
        $rear_caddies,
        $qty_rear_caddies,
        $item_harddrive_one,
        $item_harddrive_two,
        $item_harddrive_three,
        $qty_hard_drive_one,
        $qty_hard_drive_two,
        $qty_hard_drive_three,
        $motherboard,
        $qty_motherboard,
        $cpu,
        $qty_cpu,
        $memory,
        $qty_memory,


        $ob_nic,
        $pcie,

        $cntrl,
        $controllertwo,
        $controllerthree,
        $qty_cntrlr,
        $qty_cntrlrtwo,
        $qty_cntrlrthree,
        $psu,
        $qty_psu,
        $mounting,
        $qty_mounting,
        $warranty,
        $os,
        $work_order_id,
        $barcode,
        $barcode_id);


        $items = $this->work_orders_model->get_barcode($barcode);
        $table = $update_table;

        


        if($table == "orders"){
            $data = array(
                'items' => $items
            );
            $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
        }else if($table == "work_orders"){
            $items = $this->work_orders_model->get_barcode_from_wo($barcode_id);
            $data = array(
                'items' => $items
            );
            
            $this->load->view('modules/work_orders/ajax/add_order_table_body', $data);
        }

        


    }

    public function update_customer_detail(){
        $barcode = $this->input->post('barcode');
        $ship_date = $this->input->post('ship_date');
        $ship_method = $this->input->post('ship_method');
        $client_name = $this->input->post('client_name');
        $contact_no = $this->input->post('contact_no');
        $email = $this->input->post('email');
        $address1 = $this->input->post('address1');
        $address2 = $this->input->post('address2');

        $address = $address1."\n".$address2;
        
        $items = $this->work_orders_model->get_barcode_from_wo($barcode);
        $data = array(
            'items' => $items
        );

        $this->work_orders_model->update_customer_detail($ship_date, $ship_method, $client_name, $contact_no, 
            $email, $address, $barcode);
        $this->load->view('modules/work_orders/product_table_body', $data);


    }

    public function _request_search_chassis(){
        $part_number = $this->input->post('part_number');
        $qty_chassis = $this->input->post('qty_chassis');
        $motherboard_total = $this->input->post('motherboard_total');

        $data_extact = $this->chassis_model->search_chassis($part_number);


        foreach($data_extact as $data_e){
            $part_number = $data_e->part_number;
            $backplane = $data_e->backplane;
            $drivebays = $data_e->drivebays;
            $rearbays = $data_e->rearbays;
            $node = $data_e->node;
            $psu = $data_e->psu;
            $bin = $data_e->bin;
            $motherboard = $data_e->model;
            $dbays = $data_e->ff;
            $rbays = $data_e->rff;
            
            $drivebays_dbays = $drivebays." ".$dbays;
            $rearbays_rbays = $rearbays." ".$rbays;

        }

        $explode_dbays = explode(" ", $drivebays);
            $numeric_dbays = $explode_dbays[0];
            $numeric_dbays = intval($numeric_dbays);

            if($numeric_dbays == '0'){
                $numeric_dbays = ' ';
            }

        $explode_rbays = explode(" ", $rearbays);
        $numeric_rbays = $explode_rbays[0];
        
        if($numeric_rbays == '0'){
            $numeric_rbays = ' ';
        }

        $motherboard_default = $qty_chassis * $node;
        $motherboard_default = intval($motherboard_default);

        if($motherboard_default == '0'){
            $motherboard_total = ' ';
        }

        $psu_default = $qty_chassis*$psu;
        $psu_default = intval($psu_default);

        if($psu_default == '0'){
            $psu_default == ' ';
        }

        $data = array(
            'status' => 200,
            'message' => 'ok',
            'part_number' => $part_number,
            'backplane' => $backplane,
            'drivebays' => $drivebays_dbays,
            'rearbays' => $rearbays_rbays,
            'numeric_dbays' => $numeric_dbays,
            'numeric_rbays' => $numeric_rbays,
            'motherboard_default' => $motherboard_default,
            'psu_default' => $psu_default,
            'node' => $node,
            'psu' => $psu,
            'bin' => $bin,
            'sample' => $this->chassis_model->search_chassis($part_number),
            'c' => $this->chassis_model->get_chassis(),
            'dbays' => $dbays,
            'rbays' => $rbays,
            'motherboard' => $motherboard
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));

    }
    public function get_ob_nic_and_pcie_qty(){
        $motherboard_ = $this->input->post('motherboard_');
        $cpu_ = $this->input->post('cpu_');
        $memory_ = $this->input->post('memory_');


        $motherboard_exp = $motherboard_;
        $cpu_exp = $cpu_;
        $memory_exp = $memory_;
        $mb_exp_ = explode(" ",$motherboard_exp);
        $cpu_exp_ = explode(" ",$cpu_);
        $mem_exp_ = explode(" ",$memory_);
        
 
        
        $search_motherboard = $this->work_orders_model->search_for_ob_nic_and_pcie_slot($mb_exp_[0], $cpu_exp_[0], $mem_exp_[1]);
        foreach($search_motherboard as $search_motherboard_data){
            $ob_nic = $search_motherboard_data->ob_nic;
            $pci_count = $search_motherboard_data->pci_count;
        }
        
        $data = array(
            'status' => 200,
            'ob_nic' => $ob_nic,
            'pci_slot' => $pci_count
        );
        return $this->output->set_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($data));

        ///$this->load->view('modules/work_orders/ajax/select_motherboard', $search_motherboard);
    }
    public function _request_motherboard(){
        $part_number = $this->input->post('part_number');
        $motherboard_id = $this->input->post('motherboard_id');
 
        $search_chassis = $this->chassis_model->search_chassis($part_number);
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $search_motherboard['motherboard'] = $this->work_orders_model->search_motherboard($motherboard);
        $search_motherboard['motherboard_id'] = $motherboard_id;

        $this->load->view('modules/work_orders/ajax/select_motherboard', $search_motherboard);


    }
    public function _request_cpu(){
        $part_number = $this->input->post('part_number');
        $cpu_id = $this->input->post('cpu_id');

        $search_chassis = $this->chassis_model->search_chassis($part_number);
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $search_motherboard = $this->work_orders_model->search_motherboard($motherboard);
        foreach($search_motherboard as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
            break;
        }
        $get_cpu_motherboard = $this->work_orders_model->get_cpu_motherboard($motherboard_qty_cpu);
        foreach($get_cpu_motherboard as $get_cpu_motherboard_data){
            $get_cpu_fam_ = $get_cpu_motherboard_data->cpu_family;
            break;
        }
        if($get_cpu_fam_ != ""){$cpu = $this->work_orders_model->get_cpu_fam($get_cpu_fam_);}else{$cpu = null;}
        
        if(empty($cpu)){
            $cpu = null;
        }
        $search_cpu['cpu'] = $cpu;
        ///$search_cpu['cpu'] = $get_cpu_motherboard;
        $search_cpu['cpu_id'] = $cpu_id;

        $this->load->view('modules/work_orders/ajax/select_cpu', $search_cpu);
    }
    public function _request_get_cpu_for_motherboard(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu'] = $this->work_orders_model->get_cpu_motherboard($qty_cpu);
        $this->load->view('modules/work_orders/ajax/select_cpu', $get_cpu_motherboard);
    }

    public function cpu_for_motherboard2(){
        $motherboard = $this->input->post('motherboard');
        $cpu_id = $this->input->post('cpu_id');
        $motherboard_chassis = $this->work_orders_model->search_motherboard($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
            break;
        }
        $cpu = $this->work_orders_model->get_cpu_motherboard($motherboard_qty_cpu);
        foreach($cpu as $cpu_data){
            $get_cpu_fam_ = $cpu_data->cpu_family;
            break;
        }
        if($get_cpu_fam_ != ""){$cpu = $this->work_orders_model->get_cpu_fam($get_cpu_fam_);}else{$cpu = null;}
        
        if(empty($cpu)){
            $cpu = null;
        }
        
        $search_cpu['cpu2'] = $cpu;
        $search_cpu['cpu_id'] = $cpu_id;
        $this->load->view('modules/work_orders/ajax/select_cpu2', $search_cpu);
    }

    public function get_cpu2(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu2'] = $this->work_orders_model->get_cpu_motherboard($qty_cpu);
        $this->load->view('modules/work_orders/ajax/select_cpu2', $get_cpu_motherboard);
    }

    public function memory_for_motherboard2(){
        $motherboard = $this->input->post('motherboard');
        $memory_id = $this->input->post('memory_id');
        $motherboard_chassis = $this->work_orders_model->search_motherboard($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $memory = $this->work_orders_model->get_memory_motherboard($motherboard->memory_family);
        }
        if(empty($memory)){
            $memory = null;
        }
        $search_memory['memory2'] = $memory;
        $search_memory['memory_id'] = $memory_id;
        $this->load->view('modules/work_orders/ajax/select_memory2', $search_memory);
    }

    public function get_memory2(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->work_orders_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
            }
        }
        $get_memory['memory2'] = $this->work_orders_model->get_memory_motherboard($memory_family);
        
        
        $this->load->view('modules/work_orders/ajax/select_memory2', $get_memory);
    }

    public function motherboard_for_chassis_edit(){
        $part_number = $this->input->post('part_number');
        $motherboard_id = $this->input->post('motherboard_id');
        $search_chassis = $this->chassis_model->search_chassis($part_number);
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $search_motherboard['motherboard'] = $this->work_orders_model->search_motherboard($motherboard);
        $search_motherboard['motherboard_id'] = $motherboard_id;
        
        $this->load->view('modules/work_orders/ajax/select_motherboard_edit', $search_motherboard);
    }

    public function cpu_for_motherboard_edit(){
        $part_number = $this->input->post('part_number');
        $cpu_id = $this->input->post('cpu_id');;
        $search_chassis = $this->chassis_model->search_chassis($part_number);
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $search_motherboard = $this->work_orders_model->search_motherboard($motherboard);
        foreach($search_motherboard as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
            break;
        }
        $get_cpu_motherboard = $this->work_orders_model->get_cpu_motherboard($motherboard_qty_cpu);
        foreach($get_cpu_motherboard as $get_cpu_motherboard_data){
            $cpu_family = $get_cpu_motherboard_data->cpu_family;
        }
        if($cpu_family != ""){$cpu = $this->work_orders_model->get_cpu_fam($cpu_family);}else{$cpu = null;}
        
        if(empty($cpu)){
            $cpu = null;
        }
        ///$search_cpu['cpu'] = $get_cpu_motherboard;
        $search_cpu['cpu'] = $cpu;
        $search_cpu['cpu_id'] = $cpu_id;
        $this->load->view('modules/work_orders/ajax/select_cpu_edit', $search_cpu);
    }

    public function get_cpu_edit(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu'] = $this->work_orders_model->get_cpu_motherboard($qty_cpu);
        $this->load->view('modules/work_orders/ajax/select_cpu_edit', $get_cpu_motherboard);
    }

    public function memory_for_motherboard_edit(){
        $part_number = $this->input->post('part_number');
        $memory_id = $this->input->post('memory_id');;
        $search_chassis = $this->chassis_model->search_chassis($part_number);
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $search_motherboard = $this->work_orders_model->search_motherboard($motherboard);
        foreach($search_motherboard as $motherboard){
            $get_memory_motherboard = $this->work_orders_model->get_memory_motherboard($motherboard->memory_family);
            break;
        }
        $search_memory['memory'] = $get_memory_motherboard;
        $search_memory['memory_id'] = $memory_id;
       
        $this->load->view('modules/work_orders/ajax/select_memory_edit', $search_memory);
    }

    public function get_memory_edit(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->work_orders_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
                break;
            }
        }
        $get_memory['memory'] = $this->work_orders_model->get_memory_motherboard($memory_family);
        
        
        $this->load->view('modules/work_orders/ajax/select_memory_edit', $get_memory);
    }

    public function get_cpu_mb_edit(){
        $motherboard = $this->input->post('motherboard');
        $cpu_id = $this->input->post('cpu_id');
        $motherboard_chassis = $this->work_orders_model->search_motherboard($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $motherboard_qty_cpu = $motherboard->qty_cpu;
            break;
        }
        $cpu = $this->work_orders_model->get_cpu_motherboard($motherboard_qty_cpu);
        foreach($cpu as $cpu_data){
            $get_cpu_fam_ = $cpu_data->cpu_family;
            break;
        }
        if($get_cpu_fam_ != ""){$cpu = $this->work_orders_model->get_cpu_fam($get_cpu_fam_);}else{$cpu = null;}
        
        if(empty($cpu)){
            $cpu = null;
        }
        
        $search_cpu['cpu2'] = $cpu;
        $search_cpu['cpu_id'] = $cpu_id;
        $this->load->view('modules/work_orders/ajax/select_cpu_mb_edit', $search_cpu);
    }

    public function get_cpu_mb(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_cpu_motherboard['cpu2'] = $this->work_orders_model->get_cpu_motherboard($qty_cpu);
        $this->load->view('modules/work_orders/ajax/select_cpu_mb_edit', $get_cpu_motherboard);
    }

    public function get_memory_mb_edit(){
        $motherboard = $this->input->post('motherboard');
        $memory_id = $this->input->post('memory_id');
        $motherboard_chassis = $this->work_orders_model->search_motherboard($motherboard);
        foreach($motherboard_chassis as $motherboard){
            $memory = $this->work_orders_model->get_memory_motherboard($motherboard->memory_family);
        }
        if(empty($memory)){
            $memory = null;
        }
        $search_memory['memory2'] = $memory;
        $search_memory['memory_id'] = $memory_id;
        $this->load->view('modules/work_orders/ajax/select_memory_mb_edit', $search_memory);
    }

    public function get_memory_mb(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->work_orders_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
            }
        }
        $get_memory['memory2'] = $this->work_orders_model->get_memory_motherboard($memory_family);
        
        
        $this->load->view('modules/work_orders/ajax/select_memory_mb_edit', $get_memory);
    }

    public function _request_memory(){
        $part_number = $this->input->post('part_number');
        $memory_for_edit = $this->input->post('memory_for_edit');
        $memory_id = $this->input->post('memory_id');
        $search_chassis = $this->chassis_model->search_chassis($part_number);
        if($search_chassis){
            foreach($search_chassis as $chassis){
                $motherboard = $chassis->motherboard;
            }
        }
        $search_motherboard = $this->work_orders_model->search_motherboard($motherboard);
        foreach($search_motherboard as $motherboard){
            $get_memory_motherboard = $this->work_orders_model->get_memory_motherboard($motherboard->memory_family);
            break;
        }
        $search_memory['memory'] = $get_memory_motherboard;
        $search_memory['memory_for_edit'] = $memory_for_edit;
        $search_memory['memory_id'] = $memory_id;
        $this->load->view('modules/work_orders/ajax/select_memory', $search_memory);
        //$this->load->view('modules/work_orders/ajax/select_memory_edit', $search_memory);
    }
    public function _request_get_memory_for_motherboard(){
        $qty_cpu = $this->input->post('qty_cpu');
        $get_motherboard = $this->work_orders_model->get_motherboard($qty_cpu);
        if($get_motherboard){
            foreach($get_motherboard as $motherboard){
                $memory_family = $motherboard->memory_family;
                break;
            }
        }
        $get_memory['memory'] = $this->work_orders_model->get_memory_motherboard($memory_family);
                
        $this->load->view('modules/work_orders/ajax/select_memory', $get_memory);
    }

    public function get_cpu_motherboard_edit(){

    }
    public function _request_add_order(){
            $chassis = $this->input->post('chassis');
            $qty_chassis = $this->input->post('qty_chassis');
            $backplane = $this->input->post('backplane');
            $drivebays = $this->input->post('drivebays');
            $rearbays = $this->input->post('rearbays');
            $nodes = $this->input->post('nodes');
            $psu_slots = $this->input->post('psu_slots');
            $bin = $this->input->post('bin');
            $front_caddies = $this->input->post('front_caddies');
            $qty_front_caddies = $this->input->post('qty_front_caddies');
            $rear_caddies = $this->input->post('rear_caddies');
            $qty_rear_caddies = $this->input->post('qty_rear_caddies');
            $item_harddrive_one = $this->input->post('item_harddrive_one');
            $item_harddrive_two = $this->input->post('item_harddrive_two');
            $item_harddrive_three = $this->input->post('item_harddrive_three');
            $qty_hard_drive_one = $this->input->post('qty_hard_drive_one');
            $qty_hard_drive_two = $this->input->post('qty_hard_drive_two');
            $qty_hard_drive_three = $this->input->post('qty_hard_drive_three');
            $motherboard = $this->input->post('motherboard');
            $qty_motherboard = $this->input->post('qty_motherboard');
            $cpu = $this->input->post('cpu');
            $qty_cpu = $this->input->post('qty_cpu');
            $memory = $this->input->post('memory');
            $qty_memory = $this->input->post('qty_memory');


            $ob_nic = $this->input->post('ob_nic');
            $pcie= $this->input->post('pcie');

            $cntrl = $this->input->post('cntrl');
            $qty_cntrlr = $this->input->post('qty_cntrlr');
            $cntrltwo = $this->input->post('cntrltwo');
            $qty_cntrlrtwo = $this->input->post('qty_cntrlrtwo');
            $cntrlthree = $this->input->post('cntrlthree');
            $qty_cntrlrthree = $this->input->post('qty_cntrlrthree');
            $psu = $this->input->post('psu');
            $qty_psu = $this->input->post('qty_psu');
            $mounting = $this->input->post('mounting');
            $qty_mounting = $this->input->post('qty_mounting');
            $warranty = $this->input->post('warranty');
            $os = $this->input->post('os');


            $created = $this->input->post('created');
            $ship_date = $this->input->post('ship_date');
            $ship_method = $this->input->post('ship_method');
            $sales_rep = $this->input->post('sales_rep');
            $client_name = $this->input->post('client_name');
            $contact_no = $this->input->post('contact_no');
            $email = $this->input->post('email');
            $address1 = $this->input->post('address1');
            $address2 = $this->input->post('address2');
            $barcode = $this->input->post('barcode');

            $address = $address1."\n".$address2;

            if(strpos(strtolower($drivebays), '3.5') !== false){
                $ff = '3.5';
                $drivebays = str_replace('3.5', '', $drivebays);
            }else if(strpos(strtolower($drivebays), '2.5') !== false){
                $ff = '2.5';
                $drivebays = str_replace('2.5', '', $drivebays);
            }

            if(strpos(strtolower($rearbays), '3.5') !== false){
                $rff = '3.5';
                $rearbays = str_replace('3.5', '', $rearbays);
            }else if(strpos(strtolower($rearbays), '2.5') !== false){
                $rff = '2.5';
                $rearbays = str_replace('2.5', '', $rearbays);

            }

            $data = array(
                'chassis' => $chassis,
                'qty_chassis' => $qty_chassis,
                'backplane' => $backplane,
                'drivebays' => $drivebays,
                'ff' => $ff,
                'rearbays' => $rearbays,
                'rff' => $rff,
                'nodes' => $nodes,
                'psu_slots' => $psu_slots,
                'bin' => $bin,
                'front_caddies' => $front_caddies,
                'qty_front_caddies' => $qty_front_caddies,
                'rear_caddies' => $rear_caddies,
                'qty_rear_caddies' => $qty_rear_caddies,
                'item_harddrive_one' => $item_harddrive_one,
                'item_harddrive_two' => $item_harddrive_two,
                'item_harddrive_three' => $item_harddrive_three,
                'qty_hard_drive_one' => $qty_hard_drive_one,
                'qty_hard_drive_two' => $qty_hard_drive_two,
                'qty_hard_drive_three' => $qty_hard_drive_three,
                'motherboard' => $motherboard,
                'qty_motherboard' => $qty_motherboard,
                'cpu' => $cpu,
                'qty_cpu' => $qty_cpu,
                'memory' => $memory,
                'qty_memory' => $qty_memory,
                'ob_nic' => $ob_nic,
                'pcie' => $pcie,
                'cntrl' => $cntrl,
                'qty_cntrlr' => $qty_cntrlr,
                'controllertwo' => $cntrltwo,
                'qty_controllertwo' => $qty_cntrlrtwo,
                'controllerthree' => $cntrlthree,
                'qty_controllerthree' => $qty_cntrlrthree,
                'psu' => $psu,
                'qty_psu' => $qty_psu,
                'mounting' => $mounting,
                'qty_mounting' =>$qty_mounting,
                'warranty' => $warranty,
                'os' => $os,


                'created_date' => $created,
                'ship_date' => $ship_date,
                'ship_method' => $ship_method,
                'sales_rep' => $sales_rep,
                'client_name' => $client_name,
                'contact_no' => $contact_no,
                'email' => $email,
                'address' => $address1,
                'address2' => $address2,
                'barcode' => $barcode,
                'archive' => 0
            );
            $this->work_orders_model->insert_new_order($data);

            $this->load->view('modules/work_orders/product_table_body', $data);

    }

    public function insert_all_order(){
        $barcode = $this->input->post('barcode');
        $items = $this->work_orders_model->get_barcode($barcode);
        

        $data = array(
            'items' => $items
        );

        $this->work_orders_model->insert_all_order($barcode);
        $this->load->view('modules/work_orders/product_table_body', $data);
    }



    public function print_overview_on_edit(){
        $barcode = $this->input->post("barcode");


        try {
            $orders = $this->work_orders_model->get_barcode_from_wo($barcode);
        } catch (Exception $e) {
            $response = array(
                "is_error" => true,
                "message" => "An error occurred while attempting to print document. "
                . "Please try again in a few minutes."
            );

            return $this->output->set_header(500)
                                ->set_content_type("application/json", "utf-8")
                                ->set_output(json_encode($response));
        }


        $data = array(
            "barcode_svg" => $barcode,
            "orders" => $orders
        );

        $motherboard_name = array();
        $ob_nic_data = array();
        $password = array();
        foreach($orders as $order){
            
            $mother_board = $order->motherboard;
            $cpu = $order->cpu;
            $memory = $order->memory;


            $cpu_explode = explode(" ",$cpu);
            $memory_explode = explode(" ",$memory);

            $get_mother_board_brand_using_cpu = $this->work_orders_model->get_mother_board_brand_using_cpu($cpu_explode[0]);
            if($get_mother_board_brand_using_cpu){
                foreach($get_mother_board_brand_using_cpu as $get_cpu){
                    $part_cpu = $get_cpu->part;
                }
            }
            $get_mother_board_brand_using_memory = $this->work_orders_model->get_mother_board_brand_using_memory($memory_explode[2], $memory_explode[1], $memory_explode[0]);
            if($get_mother_board_brand_using_memory){
                foreach($get_mother_board_brand_using_memory as $get_memory_bar){
                    $memory_family = $get_memory_bar->memory_family;
                }
            }
            $get_motherboard_brand = $this->work_orders_model->get_motherboard_brand($part_cpu, $memory_family, $mother_board);
            if($get_motherboard_brand){
                foreach($get_motherboard_brand as $motherboard_brand){
                    $mother_board_brand = $motherboard_brand->brand;
                    $ob_nic = $motherboard_brand->ob_nic;
                }
            }


            if($mother_board_brand == 'Dell' || $mother_board_brand == 'DELL' || $mother_board_brand == 'dell'){
                $mother_board_brand = 'root';
                $pass = 'calvin';
            }
            else if($mother_board_brand == 'Supermicro' || $mother_board_brand == 'SUPERMICRO' || $mother_board_brand == 'supermicro'){
                $mother_board_brand = 'ADMIN';
                $pass = 'PASSWORD';
            }
            else if($mother_board_brand == 'Asus' || $mother_board_brand == 'ASUS' || $mother_board_brand == 'asus'){
                $mother_board_brand = 'admin';
                $pass = 'admin';
            }
            else if($mother_board_brand == 'Tyan' || $mother_board_brand == 'TYAN' || $mother_board_brand == 'tyan'){
                $mother_board_brand = 'root';
                $pass = 'superpassword';
            }else{
                $mother_board_brand = '';
                $pass = '';
            }
            
            array_push($motherboard_name,$mother_board_brand);
            array_push($ob_nic_data,$ob_nic);
            array_push($password,$pass);
        }
        

        $data['motherboard_brand'] = $motherboard_name;
        $data['ob_nic'] = $ob_nic_data;
        $data['password'] = $password;
        return $this->load->view("modules/work_orders/ajax/print_overview", $data);
    }

    function print_overview() {
        $barcode_svg = $this->input->post("barcode_svg");
        $barcode = $this->input->post("barcode");
        $created = $this->input->post("created");
        $sales_rep = $this->input->post("sales_rep");
        $ship_date = $this->input->post("ship_date");
        $ship_method = $this->input->post("ship_method");
        $client_name = $this->input->post("client_name");
        $client_contact = $this->input->post("client_contact");
        $client_email = $this->input->post("client_email");
        $client_address = $this->input->post("client_address");

        $client_address_lines = explode("\n", $client_address);

        try {
            $orders = $this->work_orders_model->get_barcode($barcode);
        } catch (Exception $e) {
            $response = array(
                "is_error" => true,
                "message" => "An error occurred while attempting to print document. "
                . "Please try again in a few minutes."
            );

            return $this->output->set_header(500)
                                ->set_content_type("application/json", "utf-8")
                                ->set_output(json_encode($response));
        }

        $data = array(
            "barcode" => $barcode_svg,
            "created" => $created,
            "sales_rep" => $sales_rep,
            "ship_date" => $ship_date,
            "ship_method" => $ship_method,
            "client_name" => $client_name,
            "client_contact" => $client_contact,
            "client_email" => $client_email,
            "client_address" => $client_address_lines,
            "orders" => $orders
        );
        $motherboard_name = array();
        $ob_nic_data = array();
        $password = array();
        foreach($orders as $order){
            $chassis_num = $order->chassis;
            $mother_board = $order->motherboard;
            $front_caddies = $order->front_caddies;
            $drives = $order->item_harddrive_one;
            $cpu = $order->cpu;
            $memory = $order->memory;
            $cntrlone = $order->cntrl;
            $psu = $order->psu;


            $cpu_explode = explode(" ",$cpu);
            $memory_explode = explode(" ",$memory);

            $get_mother_board_brand_using_cpu = $this->work_orders_model->get_mother_board_brand_using_cpu($cpu_explode[0]);
            if($get_mother_board_brand_using_cpu){
                foreach($get_mother_board_brand_using_cpu as $get_cpu){
                    $part_cpu = $get_cpu->part;
                }
            }
            $get_mother_board_brand_using_memory = $this->work_orders_model->get_mother_board_brand_using_memory($memory_explode[2], $memory_explode[1], $memory_explode[0]);
            if($get_mother_board_brand_using_memory){
                foreach($get_mother_board_brand_using_memory as $get_memory_bar){
                    $memory_family = $get_memory_bar->memory_family;
                }
            }
            $get_motherboard_brand = $this->work_orders_model->get_motherboard_brand($part_cpu, $memory_family, $mother_board);
            if($get_motherboard_brand){
                foreach($get_motherboard_brand as $motherboard_brand){
                    $mother_board_brand = $motherboard_brand->brand;
                    $ob_nic = $motherboard_brand->ob_nic;
                }
            }


            if($mother_board_brand == 'Dell' || $mother_board_brand == 'DELL' || $mother_board_brand == 'dell'){
                $mother_board_brand = 'root';
                $pass = 'calvin';
            }
            else if($mother_board_brand == 'Supermicro' || $mother_board_brand == 'SUPERMICRO' || $mother_board_brand == 'supermicro'){
                $mother_board_brand = 'ADMIN';
                $pass = 'PASSWORD';
            }
            else if($mother_board_brand == 'Asus' || $mother_board_brand == 'ASUS' || $mother_board_brand == 'asus'){
                $mother_board_brand = 'admin';
                $pass = 'admin';
            }
            else if($mother_board_brand == 'Tyan' || $mother_board_brand == 'TYAN' || $mother_board_brand == 'tyan'){
                $mother_board_brand = 'root';
                $pass = 'superpassword';
            }else{
                $mother_board_brand = '';
                $pass = '';
            }
            array_push($motherboard_name,"$mother_board_brand");
            array_push($ob_nic_data,"$ob_nic");
            array_push($password,$pass);
        }
        
        
        $data['chassis_num'] = $chassis_num;
        $data['motherboard'] = $mother_board;
        $data['front_caddies'] = $front_caddies;
        $data['drives'] = $drives;
        $data['cpu'] = $cpu;
        $data['memory'] = $memory;
        $data['cntrlone'] = $cntrlone;
        $data['psu'] = $psu;
        $data['barcode_svg'] = $barcode;
        $data['motherboard_brand'] = $motherboard_name;
        $data['ob_nic'] = $ob_nic_data;
        $data['password'] = $password;

        return $this->load->view("modules/work_orders/ajax/print_overview", $data);
    }

    function print_detail() {
        $sku = $this->input->post("sku");
        $barcode_svg = $this->input->post("barcode_svg");
        $barcode = $this->input->post("barcode");
        $created = $this->input->post("created");
        $sales_rep = $this->input->post("sales_rep");
        $ship_date = $this->input->post("ship_date");
        $ship_method = $this->input->post("ship_method");
        $client_name = $this->input->post("client_name");
        $client_contact = $this->input->post("client_contact");
        $client_email = $this->input->post("client_email");
        $client_address = $this->input->post("client_address");
        $motherboard = $this->input->post("motherboard");
        $work_order_id = $this->input->post("work_order_id");

        $client_address_lines = explode("\n", $client_address);


        $database = $this->input->post("database");

        try {
            if($database == "work_orders")
                $order = $this->work_orders_model->load_work_order($barcode, $sku, $motherboard, $work_order_id);
            else
                $order = $this->work_orders_model->load_order($barcode, $sku, $motherboard, $work_order_id);
        } catch (Exception $e) {
            $response = array(
                "is_error" => true,
                "message" => "An error occurred while attempting to print document. "
                . "Please try again in a few minutes."
            );

            return $this->output->set_header(500)
                                ->set_content_type("application/json", "utf-8")
                                ->set_output(json_encode($response));
        }

        $data = array(
            "barcode" => $barcode_svg,
            "created" => $created,
            "sales_rep" => $sales_rep,
            "ship_date" => $ship_date,
            "ship_method" => $ship_method,
            "client_name" => $client_name,
            "client_contact" => $client_contact,
            "client_email" => $client_email,
            "client_address" => $client_address_lines,
            "order" => $order,
            'barcode_code' => $barcode
        );
        

        return $this->load->view("modules/work_orders/ajax/print_detail", $data);
    }

    public function delete_all_order(){
        $barcode = $this->input->post('barcode');
        $delete_order = $this->work_orders_model->delete_all_order($barcode, $key);

        $this->load->view('modules/work_orders/product_table_body');

    }

    public function _request_delete_order(){
        $part_number = $this->input->post('part_number');
        $barcode = $this->input->post('barcode');
        $created = $this->input->post('created');
        $delete_order['drives'] = $this->work_orders_model->get_drives();
        $delete_order['get_display'] = $this->work_orders_model->delete_order($part_number, $barcode, $created);


        $this->load->view('modules/work_orders/product_table_body', $delete_order);
    }

    public function sss(){
        $data_extact['data_extact'] = $this->chassis_model->get_chassis();
        $this->load->view('modules/work_orders/ajax/select_chassis', $data_extact);
    }

}

?>
