<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->library('session');
        
        $this->load->helper(array(
            'url', 'nav_order', 'user_access', 'form'
        ));

        $this->load->model(array(
            'google_model',
            'users_model',
            'inventory_model',
            'listing_model',
            'navigation_model'
        ));
    }

    function index() {
        if ($this->session->has_userdata('user')) {
            $userdata = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($userdata['email']);
			$user_id = $get_user_id->id;
            $is_google = $userdata['is_google'];

			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $userdata['id'];
			}

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'listing');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);


            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);

            // User access is elevated when they are granted access to one or more pages.
            $is_user_access_elevated = isset($nav) && count($nav) > 0;

            $ordered_nav = organize_nav_order($nav);

            $picture = $userdata['picture'];

            if (is_null($userdata['picture']) || $userdata['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $search_query = $this->input->get('search_query');
            $category_filters = $this->input->get('category_filter[]');
            $user_filters = $this->input->get('user_filter[]');
            $channel_filters = $this->input->get('channel_filter[]');
            $done_filter = $this->input->get('done_filter');
            $listing = $this->listing_model->load_listing(
                $search_query,
                $category_filters,
                $channel_filters,
                $done_filter
                
            );

            $categories = $this->listing_model->load_categories();
            $inventory_categories = $this->inventory_model->get_categories();
            $user_filter = $this->listing_model->get_pick_users();
           
            $data = array(
                'listing' => $listing,
                'base_row_index' => 0,
                'search_query' => $search_query,
                'categories' => $categories,
                'inventory_categories' => $inventory_categories,
                'channel_filters' => $channel_filters,
                'category_filters' => $category_filters,
                'done_filter' => $done_filter,
                'user_filters' => $user_filter,
                'data' => array(
                    'user_picture' => $picture,
                    'user_email' => $userdata['email'],
                    'user_firstname' => $userdata['firstname'],
                    'user_lastname' => $userdata['lastname'],
                    'nav_menu' => $ordered_nav,
                    'page_name' => 'Listing',
                    'is_google' => $is_google,
                    'is_user_access_elevated' => $is_user_access_elevated
            ));

            $this->load->view('modules/listing/index', $data);
        } else {
            
            $data['data'] = array(
                'page_name' => 'Login'
            );
            
            $this->load->view('login', $data);
        }
    }

    function load_listing() {
        $userdata = $this->session->userdata('user');

        $last_id = $this->input->post('last_id');
        $base_row_index = $this->input->post('current_max_row');
        $search_query = $this->input->post('search_query');
        $request_users = $this->input->post('users');
        $category_filters = $this->input->post('category_filter[]');
        $channel_filters = $this->input->post('channel_filters[]');
        $users = $this->input->post('users');
        $done_filter = $this->input->post('done_filter');
        $inventory_categories = $this->inventory_model->get_categories();
       
        $listing = $this->listing_model->load_listing(
            $search_query,
            $category_filters,
            $channel_filters,
            $last_id,
            $users,
            $done_filter
    
        );
    
        $data = array(
            'listing' => $listing,
            'inventory_categories' => $inventory_categories,
            'base_row_index' => $base_row_index
        );

        return $this->load->view('modules/listing/ajax/product_listing', $data);
    }

    function create_listing() {
        $input = $this->input->post();

        $is_sku_exists = $this->listing_model->check_sku_exists($input['sku']);

        if ($is_sku_exists) {
            echo 'exists';
            return;
        }
        
        $result = $this->listing_model->create_listing($input);

        if (!isset($result)) {
            echo '';
            return;
        }

        $userdata = $this->session->userdata('user');

        $data = array(
            'listing' => $result,
            'base_row_index' => 0
        );

        return $this->load->view('modules/listing/ajax/product_listing', $data);
    }

    
    function mark_done() {
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->listing_model->mark_done($id, $user);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    function mark_undone() {
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->listing_model->mark_undone($id, $user);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    function save_column(){
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $value = $this->input->post('value');
        $column = $this->input->post('column');
        $result = $this->listing_model->save_column($id, $user, $value, $column);
        
        $GetLowPrice = $this->listing_model->getprice($id);
      
        foreach($GetLowPrice as $Price){
            $price_low_last_edited = $Price->price_low_last_edited;
            $price_low_last_edited_by = $Price->price_low_last_edited_by;
            $price_high_last_edited = $Price->price_high_last_edited;
            $price_high_last_edited_by = $Price->price_high_last_edited_by;
            $link_last_edited = $Price->link_last_edited;
            $link_last_edited_by = $Price->link_last_edited_by;
            $note_last_edited = $Price->note_last_edited;
            $note_last_edited_by = $Price->note_last_edited_by;
            $channel_last_edited = $Price->channel_last_edited;
            $channel_last_edited_by = $Price->channel_last_edited_by;
            $comp_link_last_edited = $Price->comp_link_last_edited;
            $comp_link_last_edited_by = $Price->comp_link_last_edited_by;

            $data = array(
                'is_success' => (bool) $result,
                'price_low_last_edited' => $price_low_last_edited,
                'price_low_last_edited_by' => $price_low_last_edited_by,
                'price_high_last_edited' => $price_high_last_edited,
                'price_high_last_edited_by' => $price_high_last_edited_by,
                'link_last_edited' => $link_last_edited,
                'link_last_edited_by' => $link_last_edited_by,
                'note_last_edited' => $note_last_edited,
                'note_last_edited_by' => $note_last_edited_by,
                'channel_last_edited' => $channel_last_edited,
                'channel_last_edited_by' => $channel_last_edited_by,
                'comp_link_last_edited' => $comp_link_last_edited,
                'comp_link_last_edited_by' => $comp_link_last_edited_by
                
            );
            return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));


        }

    }

    public function generate_csv() {
        $filter_sku = $this->input->get('filter_sku');
    try {
        $csv = $this->listing_model->export_csv($filter_sku === '' ? null : $filter_sku);

    } catch (Exception $e) {
        $response = array(
            'message' => 'Something went wrong. Please contact the site admin.'
        );
        
        return $this->output->set_header(500)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response));
    }

    return $this->output->set_header(200)
                        ->set_content_type('text/csv', 'utf-8')
                        ->set_output($csv);
    } 
    

    public function filter_pick_user(){
        $filter_user = $this->input->get('filter_user');

        $data_sess = $this->session->userdata('user');
        $firstname = $data_sess['firstname'];

        $filter_user = trim($filter_users);


        if($filter_user == ''){
            $filter_user = null;
        }

        $get_users = $this->listing_model->filter_pick_user_request(
            $filter_user === '' ? null : $filter_user,
            $firstname
            );
        
        $data = array(
            'items' => $get_users
        );

        $this->load->view('modules/listing/interactive_card', $data);
    }
}
