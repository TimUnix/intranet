<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_shipping extends CI_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->helper(array( 'url', 'nav_order', 'user_access'));
		$this->load->model(array( 
			'users_model', 
			'navigation_model',
            'google_model',
			'ebay_shipping_model' 
		));
	}

	public function index(){

		$this->load->library('session');
		
		if ($this->session->userdata('user')) {
			$data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'ebay_shipping');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google
            );

            $this->load->view('modules/ebay_items_shipping_details/index', $userdata);
		}else{
			 $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
		}
	}

	public function get_shipping(){
		$shipped_tomorrow = $this->ebay_shipping_model->get_tomorrow_shipping();
		$shipped_today = $this->ebay_shipping_model->get_today_shipping();

		$data = array(
			'shipped_tomorrow' => $shipped_tomorrow,
			'shipped_today' => $shipped_today
		);

		header('Content-Type: application/json');
        echo json_encode($data);
	}

	public function display_shipped_tomorrow(){
		$shipped_tomorrow = $this->ebay_shipping_model->display_shipped_tomorrow();

		$data = array(
			'items' => $shipped_tomorrow
		);
		var_dump($data);
		return $this->load->view('modules/ebay_items_shipping_details/ajax/shipped_body', $data);
	}

	public function display_shipped_today(){
		$shipped_today = $this->ebay_shipping_model->display_shipped_today();

		$data = array(
			'items' => $shipped_today
		);
		var_dump($data);
		return $this->load->view('modules/ebay_items_shipping_details/ajax/shipped_body', $data);
	}

	
	public function get_shipping_data(){
		$get_shipping_data = $this->ebay_shipping_model->get_shipping_data();


		$data = array(
			'get_shipping_data' => $get_shipping_data
		);


		$this->load->view('modules/ebay_items_shipping_details/ajax/shippng_day_table', $data);
	}

	public function get_quantity(){
		$sold_today = $this->ebay_shipping_model->get_today_quantity_sold();
		$sold_tomorrow = $this->ebay_shipping_model->get_tomorrow_quantity_sold();
		$data = array(
			'sold_today' => $sold_today,
			'sold_tomorrow' => $sold_tomorrow
		);

		header('Content-Type: application/json');
        echo json_encode($data);
	}

	public function display_quantity_today(){
		$quantity_shipped_today = $this->ebay_shipping_model->display_quantity_today();

		$data = array(
			'items' => $quantity_shipped_today
		);
		var_dump($data);
		return $this->load->view('modules/ebay_items_shipping_details/ajax/quantity_shipped_body', $data);
	}

	public function display_quantity_tomorrow(){
		$quantity_shipped_tomorrow = $this->ebay_shipping_model->display_quantity_tomorrow();
	
		$data = array(
			'items' => $quantity_shipped_tomorrow
		);
		var_dump($data);
		return $this->load->view('modules/ebay_items_shipping_details/ajax/quantity_shipped_body', $data);
	}


	public function display_quantity_shiping(){
		$display_quantity_shiping = $this->ebay_shipping_model->display_quantity_shiping();

		$data = array(
			'items' => $display_quantity_shiping
		);
		var_dump($data);
		return $this->load->view('modules/ebay_items_shipping_details/ajax/quantity_shipped_body', $data);
	}
}