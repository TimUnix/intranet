<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_competitors extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array( 'url', 'nav_order', 'user_access', 'median_calculator' ));
        $this->load->model(array(
            'ebay_store_model',
            'google_model',
            'navigation_model',
            'inventory_model',
            'search_history_model',
            'users_model',
            'ebay_items_model' 
        ));
    }

    function index() {
        $this->load->library('session');

        if ($this->session->has_userdata('user')) {
            $data_sess = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($data_sess['email']);
			$user_id = $get_user_id->id;
			
			$is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $data_sess['id'];
			}
            
            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'ebay_competitors');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            } 

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $conditions = $this->ebay_store_model->conditions();
            $categories = $this->ebay_store_model->categories();
            $sellers = $this->ebay_store_model->sellers();
            $search_history = $this->search_history_model->get_search_history_for_user($g_id);
            $columns = $this->users_model->get_table_columns('ebay_competitors', $g_id);
            $table_columns = json_decode($columns->ebay_competitors_columns);

            $data['data'] = array(
                'user_picture' => $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
				'page_name'	=>	$user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'conditions' => $conditions,
                'categories' => $categories,
                'sellers' => $sellers,
                'search_history' => $search_history,
                'columns' => $table_columns,
				'is_google' => $is_google
            );
            
            return $this->load->view('modules/ebay_competitors/index', $data);
        }
        
        $data['data'] = array( 'page_name' => 'Login' );
        
        return $this->load->view('login', $data);
    }


    public function get_ebay_item_json(){
        $item_number = $this->input->post('item_number');
        $data = $this->ebay_store_model->get_ebay_item_json($item_number);
        
        $js = stripslashes(json_encode($data[0]));
        $jd = json_decode($js, true, JSON_UNESCAPED_SLASHES);
       
        $json_data = "{$js}";
        $qoute = preg_replace('/"/','',$json_data);
        $backslash = preg_replace('/\\\/','',$qoute);
        $opencurl = preg_replace('/{/','',$backslash);
        $value= preg_replace('/value/','',$opencurl);
        $semicolon= preg_replace('/::/',':',$value);
        $dollar= preg_replace('/\$/','',$semicolon);
        $space= preg_replace('/\s+/','',$dollar);



        ///$json = json_encode($bbb);
        ///$i =json_decode($json);

        //echo ($i);
        $lowestFixedPrice = 'lowestFixedPrice';
        $semilowestFixedPrice = ":";
        $pricelowestFixedPrice = '/[$ch]/';
        preg_match_all('/'.$lowestFixedPrice.$semilowestFixedPrice.'(\d+)' . "." . '(\d+)' . '/', $space, $arraylowestFixedPrice);
        ///print_r($array);
        $_arraylowestFixedPrice = $arraylowestFixedPrice;


        $originalPrice = 'originalPrice';
        $semioriginalPrice = ":";
        $price1 = '/[$ch]/';
        preg_match_all('/'.$originalPrice.$semioriginalPrice.'(\d+)' . "." . '(\d+)' . '/', $space, $arrayoriginalPrice);
        //print_r($array1);
        $_arrayoriginalPrice = $arrayoriginalPrice;


        $discountedPrice = 'discountedPrice';
        $semidiscountedPrice = ":";
        $pricediscountedPrice = '/[$ch]/';
        preg_match_all('/'.$discountedPrice.$semidiscountedPrice.'(\d+).(\d+)/', $space, $arraydiscountedPrice);
        //print_r($arraydiscountedPrice);
        $_arraydiscountedPrice = $arraydiscountedPrice;


        $originalRetailPrice = 'originalRetailPrice';
        $semioriginalRetailPrice = ":";
        $priceoriginalRetailPrice = '/[$ch]/';
        preg_match_all('/'.$originalRetailPrice.$semioriginalRetailPrice.'(\d+).(\d+)/', $space, $arrayoriginalRetailPrice);
        //print_r($arraydiscountedPrice);
        $_arrayoriginalRetailPrice = $arrayoriginalRetailPrice;

        $binPrice = 'binPrice:';
        $semibinPrice = "US";
        $pricebinPrice = '/[$ch]/';
        preg_match_all('/'.$binPrice.$semibinPrice.'(\d+).(\d+)/', $space, $arraybinPrice);
        $_arraybinPrice = $arraybinPrice;


        $binPriceDouble = 'binPriceDouble';
        $semibinPriceDouble = ":";
        $pricebinPriceDouble = '/[$ch]/';
        preg_match_all('/'.$binPriceDouble.$semibinPriceDouble.'(\d+).(\d+)/', $space, $arraybinPriceDouble);
        ///print_r($arraybinPriceDouble);
        $_arraybinPriceDouble = $arraybinPriceDouble;


        $dataarray = array(
            'lowestFixedPrice' => $_arraylowestFixedPrice,
            'originalPrice' => $_arrayoriginalPrice,
            'discountedPrice' => $_arraydiscountedPrice,
            'originalRetailPrice' => $_arrayoriginalRetailPrice,
            'binPrice' => $_arraybinPrice,
            'binPriceDouble' => $_arraybinPriceDouble

        );

        $this->load->view('modules/ebay_competitors/ajax/table_modal',$dataarray);
    }

    public function search_item() {
        $search_item = $this->input->post('searchItem');
        $seller = $this->input->post('seller');
        $sort_order = $this->input->post('sortOrder');
        $order_by = $this->input->post('orderBy');
        $conditions_response = $this->input->post('conditions');
        $categories_response = $this->input->post('categories');
        $selling_state = $this->input->post('sellingState');
        $current_offset = $this->input->post('currentOffset');

        $conditions = $conditions_response === '' ? null : explode(',', $conditions_response);
        $categories = $categories_response === '' ? null : explode(',', $categories_response);
        
        $items = $this->ebay_store_model->get(
            $search_item,
            $seller === '' ? null : $seller,
            $conditions,
            $categories,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order,
            $selling_state === '' ? null : 'Active',
            $current_offset
        );

        $item_count = count($items);
        $low = 0;
        $high = 0;
        $average = 0;
        $median = 0;

        if ($item_count > 0) {
            $prices = array_map(function($item) {
                return $item->price;
            }, $items);

            $prices = array_filter($prices, function($price) {
                return $price != null;
            });

            if (count($prices) > 0) {
                $low = min($prices);
                $high = max($prices);
                $total = array_reduce($prices, function($sum, $price) {
                    return $sum + $price;
                }, 0);

                $average = $total / count($prices);
                $median = getMedian($prices);
            }
        }

        if (count($items) > 0) {
            $this->load->library('session');
            $user = $this->session->userdata('user');
            $columns = $this->users_model->get_table_columns('ebay_competitors', $user['id']);
            $table_columns = json_decode($columns->ebay_competitors_columns);

            $data = array(
                'items' => $items,
                'columns' => $table_columns,
                'offset' => $current_offset
            );

            return $this->load->view('modules/ebay_competitors/ajax/product_table_body', $data);
        }

        return $this->load->view('modules/ebay_competitors/ajax/no_items');
    }

    function summary() {
        $search_item = $this->input->post('searchItem');
        $seller = $this->input->post('seller');
        $sort_order = $this->input->post('sortOrder');
        $order_by = $this->input->post('orderBy');
        $conditions_response = $this->input->post('conditions');
        $categories_response = $this->input->post('categories');
        $selling_state = $this->input->post('sellingState');

        $conditions = $conditions_response === '' ? null : explode(',', $conditions_response);
        $categories = $categories_response === '' ? null : explode(',', $categories_response);
        
        $summary = $this->ebay_store_model->summary(
            $search_item,
            $seller === '' ? null : $seller,
            $conditions,
            $categories,
            $order_by === '' ? null : $order_by,
            $sort_order === '' ? null : $sort_order,
            $selling_state === '' ? null : 'Active'
        );

        $data = array(
            'search_item' => $search_item,
            'item_count' => $summary->count,
            'high' => $summary->high,
            'low' => $summary->low,
            'average' => $summary->average,
            'median' => $summary->median,
        );

        return $this->load->view('modules/ebay_competitors/ajax/summary', $data);
    }

    public function save_search_history() {
        $search = $this->input->post('search');

        if (isset($search) && $search !== '') {
            $this->load->library('session');
            $user = $this->session->userdata('user');
            $id = $user['id'];

            $search_history_query = $this->search_history_model->check_users_history($id, $search);
            $is_history_existing = count($search_history_query) > 0;

            $data = array(
                'user_id' => $id,
                'search' => $search,
                'updated' => date('Y-m-d H:i:s')
            );

            if ($is_history_existing) {
                $this->search_history_model->update_search_history($data, 'user_id', $id, 'search', $search);
            } else {
                $this->search_history_model->insert_table($data);
            }
        }
    }


    public function generate_csv(){
       
        $id = json_decode($_GET['id']);

        $a=array();
        $count = 1;
        foreach($id as $itemnumber_input){
         foreach($itemnumber_input as $input){
             $part_number = $this->ebay_items_model->get_part_number(trim($input));
             if($part_number){
                 foreach($part_number as $item_number){
                     $itemnumber = $item_number->item_number;
                     $price = $item_number->price;
                     $link= $item_number->link;
                     $img = $item_number->img;
                     $seller = $item_number->seller;
                     $category_name = $item_number->category_name;
                     $title = $item_number->title;
                     $shipping_type = $item_number->shipping_type;
                     $ship_to_locations = $item_number->ship_to_locations;
                     $expedited_shipping = $item_number->expedited_shipping;
                     $one_day_shipping_available = $item_number->one_day_shipping_available;
                     $top_rated_listing = $item_number->top_rated_listing;
                     $handling_time = $item_number->handling_time;
                     $selling_state = $item_number->selling_state;
                     $condition_display_name = $item_number->condition_display_name;

                     array_push($a, array(
                         'count'=>$count ++,
                         'item_number'=>trim($itemnumber),
                         'price'=> trim($price),
                         'link' => trim($link),
                         'img' => trim($img),
                         'seller'=>trim($seller),
                         'category'=>trim($category_name),
                         'title'=>trim($title),
                         'shipping_type'=>trim($shipping_type),
                         'ship_to_locations'=>trim($ship_to_locations),
                         'expedited_shipping'=>trim($expedited_shipping),
                         'one_day_shipping_available'=>trim($one_day_shipping_available),
                         'top_rated_listing'=>trim($top_rated_listing),
                         'handing_time'=>trim($handling_time),
                         'selling_state'=>trim($selling_state),
                         'condition_display_name'=>trim($condition_display_name)
                        ));
                 }
             }
             
         }
     }
        
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Ebay_Competitors".date('Ymd').".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        /*$sum = 0;
           foreach($usersData as $key) {
           $sum = $sum + $key->pri; 
           }*/
           
        /*var_dump($a);
        foreach($a as $a){
            echo $a['a'] . "-" . $a['b'] . "</br>";
        }*/
        
        fputcsv($handle, array("", "UnixSurplus"));
        fputcsv($handle, array("Date Exported", date('Y-m-d')));
        fputcsv($handle, array("No","Item Number", 'Price', 'Link', 'Img', 'Seller', 'Category', 'Title', 'Shipping Type', 'Ship To Locations', 'Expedited Shipping',
        'One Day Shipping Available',
        'Top Rated Listing',
        'Handing Time',
        'Selling State',
        'Condition Display Name' ));
        foreach ($a as $key) {
            
            fputcsv($handle, $key);
            

        }
        fclose($handle);
        exit;
    }
}

?>
