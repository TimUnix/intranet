<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class photo_request extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'navigation_model',
            'google_model',
            'Photo_request_model',
            'users_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'photo_request');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $base_row_index = 0;

            $data = array(
                'base_row_index' => $base_row_index,
                'data' => array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
            ));
            $this->load->view('modules/photo_request/index', $data);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function createrequest(){
        $name = $this->input->post('name');
        $sku = $this->input->post('sku');
        $price = $this->input->post('price');
        $angles = $this->input->post('angles');
        $comments = $this->input->post('comments');
        $location = $this->input->post('location');
        $userdata = $this->session->userdata('user');
        $userfirst = trim($userdata['firstname']);
        $userlast = trim($userdata['lastname']);

        $exit_qty_empty = 0;

        if($sku === '' ){
            return $exit_qty_empty;
        }else{
            $data = array(
                'name' => $userfirst.' '.$userlast,
                'sku' => $sku,
                'price' => $price,
                'angles' => $angles,
                'comments' => $comments,
                'location' => $location
            );

        $this->Photo_request_model->insert_request($data);
        $this->load->view("modules/photo_request/photo_table_body", $data);

        }
    }

    public function load_request(){
        $get_request = $this->Photo_request_model->get_request(); 
    
        $data = array(
                'items' => $get_request
        );
		
        return $this->load->view('modules/photo_request/photo_table_body', $data);        
    }

    public function search_item() {
        $searchItem = $this->input->get('searchItem');
            $data = array(
                'searchItem' => $searchItem
            );

            $get_items = $this->Photo_request_model->search_it(
                $searchItem === '' ? null : $searchItem
            ); 
            
            $data = array(
                'items' => $get_items

            );
            return $this->load->view('modules/photo_request/photo_table_body', $data);
 
        
    }

    public function search_archive() {
        $searchItem = $this->input->get('searchItem');
            $data = array(
                'searchItem' => $searchItem
            );

            $get_items = $this->Photo_request_model->search_archive(
                $searchItem === '' ? null : $searchItem
            ); 
            
            $data = array(
                'items' => $get_items
            );
            return $this->load->view('modules/photo_request/photo_table_body', $data);
 
        
    }

	public function search_done() {
        $searchItem = $this->input->get('searchItem');
            $data = array(
                'searchItem' => $searchItem
            );

            $get_items = $this->Photo_request_model->search_done(
                $searchItem === '' ? null : $searchItem
            ); 
            
            $data = array(
                'items' => $get_items
            );
            return $this->load->view('modules/photo_request/photo_table_body', $data);
 
        
    }

    public function get_done(){
        $done = $this->input->post('done');
        $data = array(
            'done' => $done
        );

        $get_done = $this->Photo_request_model->get_done($done === '' ? null : $done); 
    
        $data = array(
                'items' => $get_done
        );

        return $this->load->view('modules/photo_request/photo_table_body', $data);        
    }

    public function update_columns() {
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $name = $data['name'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->Photo_request_model->update_col($col,$sku, $name);
        
    }

    public function update_columns_addtolinn() {
        $data = $this->input->post('data');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);

        $sku = $data['sku'];
        $name = $data['name'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->Photo_request_model->update_col_addtolinn($col,$sku, $name,$datatoupdate,$user);
        
    }

    function mark_done() {
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->Photo_request_model->mark_done($id, $user);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    function mark_undone() {
        $id = $this->input->post('id');

        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->Photo_request_model->mark_undone($id, $user);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    
    function archive() {
        $id = $this->input->post('id');
        $notes = $this->input->post('notes');
        $userdata = $this->session->userdata('user');
        $user = trim($userdata['firstname']);
        $result = $this->Photo_request_model->archive($id, $user,$notes);

        $data = array(
            'is_success' => (bool) $result
        );

        return $this->output->set_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($data));
    }

    public function get_archive(){
        $is_archive = $this->input->post('is_archive');
        $data = array(
            'is_archive' => $is_archive    
        );

        $get_archive = $this->Photo_request_model->get_archive($is_archive === '' ? null : $is_archive); 
    
        $data = array(
                'items' => $get_archive
			
        );
        return $this->load->view('modules/photo_request/photo_table_body', $data);        
    }

}

?>
