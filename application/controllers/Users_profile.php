<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_profile extends CI_Controller {

		function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->library('fpdf/fpdf.php');
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'Users_profile_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
           // $task = self::detect_listing_action();
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'users_profile');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $get_manager = $this->Users_profile_model->get_manager();
            $usersprofile = $this->Users_profile_model->get_users_profile();

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'  => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'managers' => $get_manager

            );

            //ask if mag add for number, birthday, address

            $this->load->view('modules/users_profile/index', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

   
    function get_users_profile(){
        $usersprofile = $this->Users_profile_model->get_users_profile();

        $data = array(
            'items' => $usersprofile,
        );
        
        return $this->load->view('modules/users_profile/ajax/users_profile_body', $data);  
    }

    function insert_new(){
        $email = $this->input->post('email');
        $data = array(
            'email' => $email,
        );
    
       $this->Users_profile_model->insert_new($data);
    }

    function update_new(){
        $usersprofile = $this->Users_profile_model->get_users_profile();
        if (!empty($usersprofile)){
            $data = array(
                'items' => $usersprofile,
            );
            
            return $this->load->view('modules/users_profile/ajax/users_profile_body', $data);  
        }else{
        $this->Users_profile_model->update_new();}
    }

    function inactive_user(){
        $email = $this->input->post('email');
    
       $this->Users_profile_model->inactive($email);
    }

    public function generate_PDF(){
        if ((!empty($_POST['form-id'])) && (!empty($_POST['form-email']))) {

        $id = $_POST['form-id'];
        $email = $_POST['form-email'];
        
        $pdf = new FPDF('p','mm','a4');
        $title = "this";
        $pdf->AddPage();
        $pdf->SetFont('Arial','',10);
        $pdf->SetLeftMargin(15);
        $pdf->SetTopMargin(15);
        $pdf->Image('resources/img/thislogo.png',73,10,-100);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','',12);
        $usersprofile = $this->Users_profile_model->get_user($email);
        foreach($usersprofile as $row){

        $fullname = $row->firstname.' '.$row->lastname;
        $contactnumber = $row->contact_number;
        $bday = $row->birthday;
        $address = $row->address;
        $start_date = $row->start_date;
        $position = $row->position;
        $manager = $row->manager;
        $picture = $row->picture;
        $department = $row->department;
        $location = $row->location;
        $ext = $row->extension;
        $days = $row->day_start;
        $daye = $row->day_end;
        $schedin = $row->schedule_in;
        $schedout = $row->schedule_out;
        
        try{
            $pdf->Image($picture.'#.png',90,40,-70);
        }catch (Exception $e){
            
        }

        try{
            $pdf->Image($picture.'#.jpg',90,40,-70);
        }catch (Exception $e){
            
        }

        $pdf->Ln();
        $pdf->SetY(90);
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(20,7,'Name:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$fullname);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(20,7,'Email:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$email);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(35,7,'Contact Number:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$contactnumber);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Birthday:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$bday);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Address:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$address);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Start Date:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$start_date);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Position:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$position);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Manager:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$manager);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(27,7,'Department:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$department);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Location:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$location);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Extension:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$ext);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Day Start:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$days);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(25,7,'Day End:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$daye);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(27,7,'Schedule In:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$schedin);
        $pdf->Ln();
        $pdf->SetX(80);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(30,7,'Schedule Out:');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,7,$schedout);

        }

        $pdf->Output('D',$fullname.'.pdf','true');
        $pdf->Close();
        header('Content-Type: application/pdf');

        }else{
            echo "no value!";
           
        }
    }

    public function update_columns() {
        $id = $this->input->post('id');
        $ein = $this->input->post('ein');
        $email = $this->input->post('email');
        $contact_number = $this->input->post('contact_number');
        $birthday = $this->input->post('birthday');
        $start_date = $this->input->post('start_date');
        $position = $this->input->post('position');
        $manager = $this->input->post('manager');
        $department = $this->input->post('department');
        $location = $this->input->post('location');
        $extension = $this->input->post('extension');
        $address = $this->input->post('address');
        $daystart = $this->input->post('day_start');
        $dayend = $this->input->post('day_end');
        
        $data = array(
            'ein' => $ein,
            'contact_number' => $contact_number,
            'birthday' => $birthday,
            'start_date' => $start_date,
            'position' => $position,
            'manager' => $manager,
            'department' => $department,
            'location' => $location,
            'extension' => $extension,
            'address' => $address,
            'day_start' => $daystart,
            'day_end' => $dayend
        );
       
        $update_col = $this->Users_profile_model->update_userscol($id,$email,$data);  
      
    }

    public function search_item() {
        $searchItem = $this->input->get('searchItem');
            $data = array(
                'searchItem' => $searchItem
            );

            $get_items = $this->Users_profile_model->search_it(
                $searchItem === '' ? null : $searchItem
            ); 
            
            $data = array(
                'items' => $get_items
            );
            return $this->load->view('modules/users_profile/ajax/users_profile_body',$data);

    }

    public function generate_csv() {
    try {
        $csv = $this->Users_profile_model->get_csv();

    } catch (Exception $e) {
        $response = array(
            'message' => 'Something went wrong. Please contact the site admin.'
        );
        
        return $this->output->set_header(500)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response));
    }

    return $this->output->set_header(200)
                        ->set_content_type('text/csv', 'utf-8')
                        ->set_output($csv);
}
    
}

?>

