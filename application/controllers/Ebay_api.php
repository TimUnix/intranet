<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_api extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model(array(
            'configuration_model',
            'ebay_items_model',
            'ebay_items_logs_model',
            'ebay_business_policies_model'
        ));
    }

    function create_ebay_ad() {
        $sku = $this->input->post('sku');
        $upc = $this->input->post('upc');
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $store = $this->input->post('store');
        $location = $this->input->post('location');
        $shipping_policy = $this->input->post('shipping_policy');
        $photos = $this->input->post('photos');
        $local_photos = $this->input->post('local_photos');
        $price = $this->input->post('price');
        $best_offer_enabled = $this->input->post('best_offer_enabled') === 'true';
        $best_offer_max = $this->input->post('best_offer_max');
        $best_offer_min = $this->input->post('best_offer_min');
        $quantity = $this->input->post('quantity');
        $lot_enabled = $this->input->post('lot_enabled') === 'true';
        $lot = $this->input->post('lot');
        $weight = $this->input->post('weight');
        $format = $this->input->post('format');
        $category = $this->input->post('category');
        $duration = $this->input->post('duration');
        $condition = $this->input->post('condition');
        $condition_description = $this->input->post('condition_description');
        $campaign_id = $this->input->post('campaign_id');
        $promotion_rate = $this->input->post('promotion_rate');
        $volume_price_two_or_more = $this->input->post('volume_price_two_or_more');
        $volume_price_three_or_more = $this->input->post('volume_price_three_or_more');
        $volume_price_four_or_more = $this->input->post('volume_price_four_or_more');

        if (trim($sku) === '' || trim($title) === '' || trim($price) === '' || trim($quantity) === ''
            || trim($weight) === '' || trim($category) === '') {
            $missing_fields_error = 0;
            $response = array(
                'is_error' => true,
                'error_type' => $missing_fields_error,
                'message' => 'A required field may have been left blank. Please check and verify the form before '
                . 'submitting again.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }

        $acquired_fields = array(
            'sku', 'title', 'store', 'shipping_policy', 'photos', 'price', 'quantity', 'weight', 'format', 'category',
            'condition', 'condition_description', 'description', 'duration', 'best_offer_enabled', 'best_offer_min',
            'best_offer_max', 'campaign_id', 'lot', 'lot_enabled', 'promotion_rate'
        );

        $category_specifics = array_filter($this->input->post(), function($key) use ($acquired_fields) {
            return !in_array($key, $acquired_fields);
        }, ARRAY_FILTER_USE_KEY);

        switch($store) {
            case 'com':
                $refresh_token_result = $this->configuration_model->getConfiguration('ebay_refresh_token_com');
                $payment_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXSurplusCom' ,'payment');
                $return_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXSurplusCom', 'return');

                break;
            case 'net':
                $refresh_token_result = $this->configuration_model->getConfiguration('ebay_refresh_token_net');
                $payment_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXSurplusNet' ,'payment');
                $return_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXSurplusNet', 'return');
                break;
            case 'plus':
                $refresh_token_result = $this->configuration_model->getConfiguration('ebay_refresh_token_plus');
                $payment_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXPlusCom' ,'payment');
                $return_policy = $this->business_policies_model
                    ->load_default_by_store_and_type('UNIXPlusCom', 'return');
                break;
            case 'itr':
                $refresh_token_result = $this->configuration_model->getConfiguration('ebay_refresh_token_itr');
                $payment_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('ITRecycleNow' ,'payment');
                $return_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('ITRecycleNow', 'return');
                break;
            default:
                $refresh_token_result = $this->configuration_model->getConfiguration('ebay_refresh_token_com');
                $payment_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXSurplusCom' ,'payment');
                $return_policy = $this->ebay_business_policies_model
                    ->load_default_by_store_and_type('UNIXSurplusCom', 'return');
        }

        $refresh_token = $refresh_token_result[0]->value;

        $item_aspects = array();

        foreach ($category_specifics as $aspect => $value) {
            $transformed_aspect = str_replace('_', ' ', $aspect);

            if (is_numeric($value)) {
                if (strpos($value, '.') !== false) {
                    $item_aspects[$transformed_aspect] = array((float) $value);
                } else {
                    $item_aspects[$transformed_aspect] = array((int) $value);
                }
            } else if (is_array($value)) {
                $item_aspects[$transformed_aspect] = $value;
            } else {
                $item_aspects[$transformed_aspect] = array($value);
            }
        };

        $create_item_request_payload = array(
            'condition' => $condition,
            'product' => array(
                'title' => $title,
                'description' => $description,
                'imageUrls' => $photos,
                'aspects' => $item_aspects
            ),
            'packageWeightAndSize' => array(
                'weight' => array(
                    'unit' => 'POUND',
                    'value' => (float) $weight
                )
            ),
            'availability' => array(
                'shipToLocationAvailability' => array(
                    'availabilityDistributions' => array(
                        array(
                            'merchantLocationKey' => $location,
                            'quantity' => (int) $quantity
                        )
                    ),
                    'quantity' => (int) $quantity
                )
            )
        );

        if (isset($upc) && $upc !== '') {
            $create_item_request_payload['product']['upc'] = $upc;
        }

        if (isset($condition_description) && trim($condition_description) !== '') {
            $create_item_request_payload['conditionDescription'] = $condition_description;
        }

        $oauth_scope = urlencode(
            'https://api.ebay.com/oauth/api_scope/sell.inventory '
            . 'https://api.ebay.com/oauth/api_scope/sell.marketing'
        );

        $token_response = json_decode($this->refresh_ebay_oauth_token($refresh_token, $oauth_scope));
        $oauth_token = $token_response->access_token;

        $create_item_url = "https://api.ebay.com/sell/inventory/v1/inventory_item/$sku";
        $content_language = 'en-US';

        $create_item_headers = array(
            "Authorization: Bearer $oauth_token",
            "Content-Language: $content_language",
            'Content-Type: application/json'
        );

        $curl_create_item = curl_init($create_item_url);
        curl_setopt($curl_create_item, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl_create_item, CURLOPT_HTTPHEADER, $create_item_headers);
        curl_setopt($curl_create_item, CURLOPT_POSTFIELDS, json_encode($create_item_request_payload));
        curl_setopt($curl_create_item, CURLOPT_RETURNTRANSFER, true);

        $create_item_response = json_decode(curl_exec($curl_create_item));
        curl_close($curl_create_item);

        if (isset($create_item_response->errors)) {
            return self::create_listing_error($create_item_response);
        }

        $create_offer_url = 'https://api.ebay.com/sell/inventory/v1/offer';
        $create_offer_request_payload = array(
            'sku' => $sku,
            'marketplaceId' => 'EBAY_US',
            'format' => $format,
            'availableQuantity' => $quantity,
            'categoryId' => $category,
            'listingDescription' => $description,
            'listingDuration' => $duration,
            'merchantLocationKey' => $location,
            'listingPolicies' => array(
                'fulfillmentPolicyId' => $shipping_policy,
                'paymentPolicyId' => $payment_policy->policy_id,
                'returnPolicyId' => $return_policy->policy_id,
                'bestOfferTerms' => array(
                    'bestOfferEnabled' => $best_offer_enabled
                )
            ),
            'pricingSummary' => array(
                'price' => array(
                    'currency' => 'USD',
                    'value' => $price
                )
            )
        );

        if ($lot_enabled && isset($lot) && trim($lot) !== '') {
            $create_offer_request_payload['lotSize'] = (int) $lot;
        }

        if ($best_offer_enabled && isset($best_offer_max) && trim($best_offer_max) !== '') {
            $create_offer_request_payload['listingPolicies']['bestOfferTerms']['autoAcceptPrice']['currency'] = 'USD';
            $create_offer_request_payload['listingPolicies']
            ['bestOfferTerms']['autoAcceptPrice']['value'] = $best_offer_max;
        }
        if ($best_offer_enabled && isset($best_offer_min) && trim($best_offer_min) !== '') {
            $create_offer_request_payload['listingPolicies']['bestOfferTerms']['autoDeclinePrice']
            ['currency'] = 'USD';
            $create_offer_request_payload['listingPolicies']['bestOfferTerms']['autoDeclinePrice']
            ['value'] = $best_offer_min;
        }

        $create_offer_headers = array(
            "Authorization: Bearer $oauth_token",
            "Content-Language: $content_language",
            'Content-Type: application/json'
        );

        $curl_create_offer = curl_init($create_offer_url);
        curl_setopt($curl_create_offer, CURLOPT_POST, true);
        curl_setopt($curl_create_offer, CURLOPT_HTTPHEADER, $create_offer_headers);
        curl_setopt($curl_create_offer, CURLOPT_POSTFIELDS, json_encode($create_offer_request_payload));
        curl_setopt($curl_create_offer, CURLOPT_RETURNTRANSFER, true);

        $create_offer_response = json_decode(curl_exec($curl_create_offer));
        curl_close($curl_create_offer);

        if (isset($create_offer_response->errors)) {
            return self::create_listing_error($create_offer_response);
        }

        $create_offer_id = $create_offer_response->offerId;

        $publish_offer_headers = array(
            "Authorization: Bearer $oauth_token",
            'Content-Type: application/json'
        );

        $publish_offer_url = "https://api.ebay.com/sell/inventory/v1/offer/$create_offer_id/publish/";
        $curl_publish_offer = curl_init($publish_offer_url);
        curl_setopt($curl_publish_offer, CURLOPT_POST, true);
        curl_setopt($curl_publish_offer, CURLOPT_HTTPHEADER, $publish_offer_headers);
        curl_setopt($curl_publish_offer, CURLOPT_RETURNTRANSFER, true);

        $publish_offer_response = json_decode(curl_exec($curl_publish_offer));
        curl_close($curl_publish_offer);

        if (isset($publish_offer_response->errors)) {
            return self::create_listing_error($publish_offer_response);
        }

        $item_number = (string) $publish_offer_response->listingId;

        if (isset($campaign_id) && $campaign_id !== '' && isset($promotion_rate) && $promotion_rate !== '') {
            $create_campaign_ad_url = "https://api.ebay.com/sell/marketing/v1/ad_campaign/$campaign_id/ad";

            $create_campaign_ad_headers = array(
                "Authorization: Bearer $oauth_token",
                'Content-Type: application/json'
            );

            $create_campaign_ad_request_payload = array(
                'bidPercentage' => $promotion_rate,
                'listingId' => $item_number
            );

            $curl_create_campaign_ad = curl_exec($create_campaign_ad_url);
            curl_setopt($curl_create_campaign_ad, CURLOPT_POST, true);
            curl_setopt($curl_create_campaign_ad, CURLOPT_POSTFIELDS, $create_campaign_ad_request_payload);
            curl_setopt($curl_create_campaign_ad, CURLOPT_HTTPHEADER, $create_campaign_ad_headers);
            curl_setopt($curl_create_campaign_ad, CURLOPT_RETURNTRANSFER, true);
            $create_campaign_ad_response = curl_exec($curl_create_campaign_ad);

            if (isset($create_campaign_ad_response->errors)) {
                return self::create_listing_error($create_campaign_ad_response);
            }
        }

        if (isset($volume_price_two_or_more) && trim($volume_price_two_or_more) !== '' &&
            isset($volume_price_three_or_more) && trim($volume_price_three_or_more) !== '' &&
            isset($volume_price_four_or_more) && trim($volume_price_four_or_more) !== '') {
            $create_item_promotion_url = 'https://api.ebay.com/sell/marketing/v1/item_promotion';

            $create_item_promotion_headers = array(
                "Authorization: Bearer $oauth_token",
                'Content-Type: application/json'
            );

            $now = new DateTime();
            $start_date = $now->format('Y-m-d\TH:i:sZ');

            $create_item_promotion_payload = array(
                'name' => "Volume pricing for $sku",
                'startDate' => $start_date,
                'endDate' => null,
                'marketplaceId' => 'EBAY_US',
                'promotionStatus' => 'SCHEDULED',
                'promotionType' => 'VOLUME_DISCOUNT',
                'applyDiscountToSingleItemOnly' => true,
                'inventoryCriterion' => array(
                    'inventoryCriterionType' => 'INVENTORY_BY_VALUE',
                    'listingIds' => array($item_number)
                ),
                'discountRules' => array(
                    array(
                        'discountSpecification' => array(
                            'minQuantity' => 1
                        ),
                        'discountBenefit' => array(
                            'percentageOffOrder' => '0'
                        ),
                        'ruleOrder' => 1
                    ),
                    array(
                        'discountSpecification' => array(
                            'minQuantity' => 2
                        ),
                        'discountBenefit' => array(
                            'percentageOffOrder' => $volume_price_two_or_more
                        ),
                        'ruleOrder' => 2
                    ),
                    array(
                        'discountSpecification' => array(
                            'minQuantity' => 3
                        ),
                        'discountBenefit' => array(
                            'percentageOffOrder' => $volume_price_three_or_more
                        ),
                        'ruleOrder' => 3
                    ),
                    array(
                        'discountSpecification' => array(
                            'minQuantity' => 4
                        ),
                        'discountBenefit' => array(
                            'percentageOffOrder' => $volume_price_four_or_more
                        ),
                        'ruleOrder' => 4
                    ),
                )
            );

            $curl_create_item_promotion = curl_init($create_item_promotion_url);
            curl_setopt($curl_create_item_promotion, CURLOPT_HTTPHEADER, $create_item_promotion_headers);
            curl_setopt($curl_create_item_promotion, CURLOPT_POST, true);
            curl_setopt($curl_create_item_promotion, CURLOPT_POSTFIELDS, $create_item_promotion_payload);
            curl_setopt($curl_create_item_promotion, CURLOPT_RETURNTRANSFER, true);
            $create_item_promotion_response = curl_exec($curl_create_item_promotion);

            if (isset($create_item_promotion_response->errors)){
                return self::create_listing_error($create_item_promotion_response);
            }
        }

        $user = $this->session->userdata('user');
        $firstname = $user['firstname'];
        $lastname = $user['lastname'];
        $edited_by = trim($firstname) . " " . trim($lastname);
        $date_edited = date("Y-m-d H:i:s");

        $data_logs = array(
            'item_number' => $item_number,
            'old_value' => '',
            'new_value' => $item_number,
            'action_type_ads' => 1,
            'edited_by' => $edited_by,
            'date_edited' => $date_edited
        );

        $this->ebay_items_logs_model->insert($data_logs);

        foreach ($local_photos as $photo) {
            unlink("./uploads/$photo");
        }

        $data = array(
            'is_error' => false,
            'title' => 'Create Ad Success',
            'message' => "eBay ad created successfully with id $item_number",
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function end_item_request() {
        $store = $this->input->post('store');
        $item_number = $this->input->post('item_number');
        $sku = $this->input->post('sku');

        try {
            switch(strtolower($store)) {
                case 'unixpluscom':
                    $code = 'ebay_refresh_token_plus';
                    break;
                case 'unixsurplusnet':
                    $code = 'ebay_refresh_token_net';
                    break;
                case 'itrecyclenow':
                    $code = 'ebay_refresh_token_itr';
                    break;
                default:
                    $code = 'ebay_refresh_token_com';
                    break;
            }

            $result = $this->configuration_model->getConfiguration($code);
            $refresh_token = $result[0]->value;
        } catch (Exception $e) {
            $data['data'] = array (
                'status' => 500,
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $scope = urlencode('https://api.ebay.com/oauth/api_scope/sell.inventory');

        $oauth_token_result = json_decode($this->refresh_ebay_oauth_token($refresh_token, $scope));
        $oauth_token = $oauth_token_result->access_token;

        $fetch_item_response = $this->fetch_inventory_item($sku, $oauth_token);

        if (!isset($fetch_item_response)) {
            $migrate_result = $this->migrate_listing($item_number, $oauth_token);

            if ($migrate_result['is_error']) {
                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($migrate_result));
            }

            $fetch_item_response = $this->fetch_inventory_item($sku, $oauth_token);
        }

        if (isset($fetch_item_response->errors)) {
            $data = array(
                'is_error' => true,
                'errors' => $offers_response->errors
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $sku_encoded = urlencode($sku);
        $delete_item_url = "https://api.ebay.com/sell/inventory/v1/inventory_item/{$sku_encoded}";

        $delete_item_headers = array(
            "Authorization: Bearer $oauth_token"
        );

        $curl_delete_item = curl_init($delete_item_url);
        curl_setopt($curl_delete_item, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl_delete_item, CURLOPT_HTTPHEADER, $delete_item_headers);
        curl_setopt($curl_delete_item, CURLOPT_NOBODY, true);
        curl_exec($curl_delete_item);
        $delete_item_status = curl_getinfo($curl_delete_item, CURLINFO_HTTP_CODE);

        curl_close($curl_delete_item);

        $data = array(
            'is_error' => true,
            'errors' => array(
                'An unexpected error occurred while attempting to end item. Please contact the site admin.'
            )
        );

        switch($delete_item_status) {
            case 204:
                $this->ebay_items_model->archive_item($item_number);

                $data = array (
                    'title' => 'End Item Success',
                    'message' => "Successfully ended item listing with item number $item_number."
                );

                break;
            case 400:
                $data = array (
                    'title' => 'Malformed Item Error',
                    'message' => 'A malformed request was formed while attempting to end the item. '
                    . 'Please contact the site admin.'
                );

                break;
            case 404:
                $data = array (
                    'title' => 'Migration Error',
                    'message' => 'A pre-end migration of the item failed. Please try again or contact the site '
                    . 'admin after persistent failures through multiple requests.'
                );

                break;
            case 500:
                $data = array (
                    'title' => 'eBay Error',
                    'message' => 'The request was unable to be fulfilled by eBay due to an error on their side. '
                    . 'Please try again or end the item through eBay\'s website.'
                );

                break;
            default:
                $data = array(
                    'is_error' => true,
                    'errors' => array(
                        'An unexpected error occurred while attempting to end item. Please contact the site admin.'
                    )
                );
        }

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function change_ad_price_quantity_request() {
        $this->load->library('session');

        $store = $this->input->post('store');
        $item_number = $this->input->post('item_number');
        $sku = $this->input->post('sku');
        $new_value = $this->input->post('new_value');
        $action = (int) $this->input->post('action_type_ads');

        $action_price = 3;
        $action_quantity = 4;

        try {
            switch(strtolower($store)) {
                case 'unixpluscom':
                    $code = 'ebay_refresh_token_plus';
                    break;
                case 'unixsurplusnet':
                    $code = 'ebay_refresh_token_net';
                    break;
                case 'itrecyclenow':
                    $code = 'ebay_refresh_token_itr';
                    break;
                default:
                    $code = 'ebay_refresh_token_com';
            }

            $token_result = $this->configuration_model->getConfiguration($code);
            $refresh_token = $token_result[0]->value;
        } catch (Exception $e) {
            $data['data'] = array (
                'status' => 200,
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $scope = urlencode('https://api.ebay.com/oauth/api_scope/sell.inventory');

        $oauth_token_result = json_decode($this->refresh_ebay_oauth_token($refresh_token, $scope));
        $oauth_token = $oauth_token_result->access_token;

        $offers_response = $this->fetch_offers($sku, $oauth_token);

        if (!isset($offers_response)) {
            $migrate_result = $this->migrate_listing($item_number, $oauth_token);

            if ($migrate_result['is_error']) {
                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($migrate_result));
            }

            $offers_response = $this->fetch_offers($sku, $oauth_token);
        }

        if (isset($offers_response->errors)) {
            $data = array(
                'is_error' => true,
                'errors' => $offers_response->errors
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $published_offers = array_filter($offers_response->offers, function($offer) {
            return $offer->status === 'PUBLISHED';
        });

        $offers_with_updated_values = array_map(function($offer)
            use ($new_value, $action_price, $action_quantity, $action) {
                if ($action === $action_price) {
                    return array(
                        'offerId' => $offer->offerId,
                        'price' => array(
                            'currency' => 'USD',
                            'value' => $new_value
                        )
                    );
                }

                if ($action === $action_quantity) {
                    return array(
                        'offerId' => $offer->offerId,
                        'availableQuantity' => (int) $new_value
                    );
                }
            }, $published_offers);

        $new_offers = array(
            array(
                'offers' => $offers_with_updated_values,
                'sku' => $sku
            )
        );

        if ($action === $action_quantity) {
            $new_offers[0]['shipToLocationAvailability']['availabilityDistributions'] = array(
                array(
                    'merchantLocationKey' => 'itr-staclara-ca',
                    'quantity' => (int) $new_value
                )
            );

            $new_offers[0]['shipToLocationAvailability']['quantity'] = (int) $new_value;
        }

        $update_price_quantity_request_payload = array(
            'requests' => $new_offers
        );

        $update_price_quantity_url = 'https://api.ebay.com/sell/inventory/v1/bulk_update_price_quantity';

        $update_price_quantity_headers = array(
            "Authorization: Bearer $oauth_token",
            'Content-Type: application/json'
        );

        $curl_update_price_quantity = curl_init($update_price_quantity_url);
        curl_setopt($curl_update_price_quantity, CURLOPT_POST, true);
        curl_setopt($curl_update_price_quantity, CURLOPT_HTTPHEADER, $update_price_quantity_headers);
        curl_setopt($curl_update_price_quantity, CURLOPT_POSTFIELDS,
                    json_encode($update_price_quantity_request_payload));
        curl_setopt($curl_update_price_quantity, CURLOPT_RETURNTRANSFER, true);

        $update_price_quantity_response = json_decode(curl_exec($curl_update_price_quantity));
        curl_close($curl_update_price_quantity);

        $has_errors = array_reduce($update_price_quantity_response->responses, function($has_error, $element) {
            return $has_error || array_key_exists('errors', $element);
        });

        if ($has_errors) {
            $errors = array_reduce($update_price_quantity_response->responses, function($errorList, $element) {
                if (array_key_exists('errors', $element)) {
                    foreach($element['errors'] as $error) {
                        array_push($errorList, $error);
                    }

                    return $errorList;
                }
            }, array());

            $response = array(
                'is_error' => true,
                'errors' => $errors
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }
    }

    function change_description_request() {
        $store = $this->input->post('store');
        $item_number = $this->input->post('item_number');
        $sku = str_replace(' ', '%20', $this->input->post('sku'));
        $new_description = $this->input->post('new_description');
        $previous_description = $this->input->post('previous_description');

        try {
            switch(strtolower($store)) {
                case 'unixpluscom':
                    $code = 'ebay_refresh_token_plus';
                    break;
                case 'unixsurplusnet':
                    $code = 'ebay_refresh_token_net';
                    break;
                case 'itrecyclenow':
                    $code = 'ebay_refresh_token_itr';
                    break;
                default:
                    $code = 'ebay_refresh_token_com';
                    break;
            }

            $result = $this->configuration_model->getConfiguration($code);
            $refresh_token = $result[0]->value;
        } catch (Exception $e) {
            $data["data"] = array (
                "status" => 500,
                "message" => "Something went wrong. Please contact the site admin."
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $scope = urlencode('https://api.ebay.com/oauth/api_scope/sell.inventory');

        $oauth_token_result = json_decode($this->refresh_ebay_oauth_token($refresh_token, $scope));
        $oauth_token = $oauth_token_result->access_token;

        $inventory_item_response = $this->fetch_inventory_item($sku, $oauth_token);

        if ($inventory_item_response === null) {
            $migrate_result = $this->migrate_listing($item_number, $oauth_token);

            if ($migrate_result['is_error']) {
                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($migrate_result));
            }

            $inventory_item_response = $this->fetch_inventory_item($sku, $oauth_token);
        }

        if (array_key_exists('errors', $inventory_item_response)) {
            $data = array(
                'is_error' => true,
                'errors' => $inventory_item_response['errors']
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        if (array_key_exists('availability', $inventory_item_response) &&
            array_key_exists('allocationByFormat',
                             $inventory_item_response['availability']['shipToLocationAvailability'])) {
            unset($inventory_item_response['availability']['shipToLocationAvailability']['allocationByFormat']);
        }

        if (array_key_exists('groupIds', $inventory_item_response)) {
            unset($inventory_item_response['groupIds']);
        }

        if (array_key_exists('inventoryItemGroupIds', $inventory_item_response)) {
            unset($inventory_item_response['inventoryItemGroupIds']);
        }

        if (array_key_exists('locale', $inventory_item_response)) {
            unset($inventory_item_response['locale']);
        }

        if (array_key_exists('sku', $inventory_item_response)) {
            unset($inventory_item_response['sku']);
        }

        $inventory_item_response['product']['title'] = $new_description;

        $update_description_headers = array(
            "Authorization: Bearer $oauth_token",
            'Content-Language: en-US',
            'Content-Type: application/json'
        );

        $update_description_url = "https://api.ebay.com/sell/inventory/v1/inventory_item/$sku";
        $curl_update_description = curl_init($update_description_url);
        curl_setopt($curl_update_description, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl_update_description, CURLOPT_HTTPHEADER, $update_description_headers);
        curl_setopt($curl_update_description, CURLOPT_POSTFIELDS, json_encode($inventory_item_response));
        curl_setopt($curl_update_description, CURLOPT_RETURNTRANSFER, true);

        $update_description_response = json_decode(curl_exec($curl_update_description), true);
        curl_close($curl_update_description);

        if (isset($update_description_response) && array_key_exists('errors', $update_description_response)) {
            $data = array(
                'is_error' => true,
                'errors' => $update_description_response['errors']
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'is_error' => false
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function upload_photos() {
        $store = $this->input->post('store');
        $photo_infos = $this->input->post('photos');
        $title = $this->input->post('title');

        if (count($photo_infos) === 0) {
            $no_photo_error = 0;
            $response = array(
                'is_error' => true,
                'error_type' => $no_photo_error
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }

        switch(strtolower($store)) {
            case 'unixpluscom':
                $code = 'ebay_token_plus';
                break;
            case 'unixsurplusnet':
                $code = 'ebay_token_net';
                break;
            case 'itrecyclenow':
                $code = 'ebay_token_itr';
                break;
            default:
                $code = 'ebay_token';
                break;
        }

        $result = $this->configuration_model->getConfiguration($code);
        $ebay_auth_token = $result[0]->value;

        $ebay_trading_base_url = 'https://api.ebay.com/ws/api.dll';
        $ebay_site_id = 0;
        $ebay_call_name = "UploadSiteHostedPictures";
        $ebay_compatibility_level = 967;
        $formdata_boundary = '------MIME_boundary';

        $headers = array(
            "X-EBAY-API-SITEID: $ebay_site_id",
            "X-EBAY-API-COMPATIBILITY-LEVEL: $ebay_compatibility_level",
            "X-EBAY-API-CALL-NAME: $ebay_call_name",
            'Cache-Control: no-cache',
            "Content-Type: mulitpart/form-data; boundary=$formdata_boundary"
        );

        $curl = curl_init($ebay_trading_base_url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $this->load->helper('file');

        $ebay_photo_urls = array();

        foreach ($photo_infos as $photo_info) {
            $photo = file_get_contents($photo_info['filepath']);
            $filename = $photo_info['filename'];
            $filetype = $photo_info['filetype'];

            $request_body = $this->ebay_upload_photo_request_builder(
                $ebay_auth_token, $title, $filename, $filetype, $photo
            );

            curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);

            $response_body = curl_exec($curl);
            $response = simplexml_load_string($response_body);

            if ((string) $response->Ack === 'Failure') {
                $data = array(
                    'is_error' => true,
                    'error_title' => 'Photo Upload Error',
                    'short_message' => (string) $response->Errors->ShortMessage,
                    'long_message' => (string) $response->Errors->LongMessage,
                    'response' => $response->asXML()
                );

                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            } else {
                $ebay_photo_urls[] = (string) $response->SiteHostedPictureDetails->FullURL;
            }
        }

        $data = array('photo_urls' => $ebay_photo_urls );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    private function fetch_inventory_item($sku, $oauth_token) {
        $sku_encoded = urlencode($sku);
        $get_inventory_item_url = "https://api.ebay.com/sell/inventory/v1/inventory_item/$sku_encoded";

        $get_inventory_item_headers = array(
            "Authorization: Bearer $oauth_token"
        );

        $curl_get_inventory_item = curl_init($get_inventory_item_url);
        curl_setopt($curl_get_inventory_item, CURLOPT_HTTPHEADER, $get_inventory_item_headers);
        curl_setopt($curl_get_inventory_item, CURLOPT_RETURNTRANSFER, true);

        $get_inventory_item_result = curl_exec($curl_get_inventory_item);
        curl_close($curl_get_inventory_item);

        $get_inventory_item_response = json_decode($get_inventory_item_result, true);

        $sku_exists = true;

        if (array_key_exists('errors', $get_inventory_item_response)) {
            $sku_exists = array_reduce($get_inventory_item_response['errors'], function($sku_found, $error) {
                return $sku_found && $error['errorId'] === 25702;
            }, true);
        }

        if (!$sku_exists) {
            return null;
        }

        return $get_inventory_item_response;
    }

    private function migrate_listing($item_number, $oauth_token) {
        $bulk_migrate_url = 'https://api.ebay.com/sell/inventory/v1/bulk_migrate_listing';

        $bulk_migrate_request_payload = array (
            'requests' => array (
                array (
                    'listingId' => $item_number
                )
            )
        );

        $bulk_migrate_headers = array(
            "Authorization: Bearer $oauth_token",
            'Content-Type: application/json'
        );

        $curl_bulk_migrate = curl_init($bulk_migrate_url);
        curl_setopt($curl_bulk_migrate, CURLOPT_HTTPHEADER, $bulk_migrate_headers);
        curl_setopt($curl_bulk_migrate, CURLOPT_POST, true);
        curl_setopt($curl_bulk_migrate, CURLOPT_POSTFIELDS, json_encode($bulk_migrate_request_payload));
        curl_setopt($curl_bulk_migrate, CURLOPT_RETURNTRANSFER, true);
        $bulk_migrate_response = json_decode(curl_exec($curl_bulk_migrate));
        curl_close($curl_bulk_migrate);

        $has_errors = false;

        if (isset($bulk_migrate_response->responses)) {
            $has_errors = array_reduce($bulk_migrate_response->responses, function($has_error, $element) {
                return $has_error || array_key_exists('errors', $element);
            });
        }

        if ($has_errors) {
            $errors = array_reduce($bulk_migrate_response->responses, function($errorList, $element) {
                if (array_key_exists('errors', $element)) {
                    foreach($element['errors'] as $error) {
                        array_push($errorList, $error);
                    }

                    return $errorList;
                }
            }, array());

            $response = array(
                'is_error' => true,
                'errors' => $errors
            );

            return $response;
        }

        return array(
            'is_error' => false
        );
    }

    private function fetch_offers($sku, $oauth_token) {
        $sku_encoded = urlencode($sku);
        $get_offers_url = "https://api.ebay.com/sell/inventory/v1/offer?sku=$sku_encoded";

        $curl_get_offers = curl_init($get_offers_url);

        $get_offers_headers = array(
            "Authorization: Bearer $oauth_token"
        );

        curl_setopt($curl_get_offers, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_get_offers, CURLOPT_HTTPHEADER, $get_offers_headers);

        $get_offers_response = json_decode(curl_exec($curl_get_offers));
        curl_close($curl_get_offers);

        if (!isset($get_offers_response)) {
            return null;
        }

        if (!isset($get_offers_response->offers)) {
            return null;
        }

        if (count($get_offers_response->offers) === 0) {
            return null;
        }

        return $get_offers_response;
    }

    private function create_listing_error($response) {
        $data = array(
            'is_error' => true,
            'errors' => $response->errors,
        );

        if (isset($response->warnings)){
            $data['warnings'] = $response->warnings;
        }

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    private function ebay_upload_photo_request_builder($auth_token, $title, $filename, $mime_type, $binary_data) {
        return "------MIME_boundary\n"
            . "Content-Disposition: form-data; name=\"XML Payload\"\n"
            . "Content-Type: text/xml; charset=utf-8\n\n"
            . "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
            . "<UploadSiteHostedPicturesRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">"
            . "<RequesterCredentials>"
            . "<eBayAuthToken>$auth_token</eBayAuthToken>"
            . "</RequesterCredentials>"
            . "<ErrorLanguage>en_US</ErrorLanguage>"
            . "<WarningLevel>High</WarningLevel>"
            . "<PictureName>$title</PictureName>"
            . "</UploadSiteHostedPicturesRequest>\n\n"
            . "------MIME_boundary\n"
            . "Content-Disposition: form-data; name=\"$title\"; filename=\"$filename\"\n"
            . "Content-Type: $mime_type\n\n"
            . "$binary_data\n\n"
            . "------MIME_boundary--";
    }

    private function refresh_ebay_oauth_token($refresh_token, $scope = null) {
        $url = 'https://api.ebay.com/identity/v1/oauth2/token';
        $client_id = 'UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2';
        $client_secret = 'PRD-81f16f178339-a79c-482c-9342-4c37';
        $redirect_uri = 'UNIXSurplus__In-UNIXSurp-itrecy-lereszia';

        $content_type = 'application/x-www-form-urlencoded';
        $oauth_credentials = base64_encode("$client_id:$client_secret");
        $grant_type = 'refresh_token';

        if (!isset($scope)) {
            $scope = urlencode('https://api.ebay.com/oauth/api_scope/sell.account.readonly');
        }

        $headers = array(
            "Content-Type: $content_type",
            "Authorization: Basic $oauth_credentials"
        );

        $request_body = "grant_type=$grant_type&refresh_token=$refresh_token&scope=$scope";

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}

?>
