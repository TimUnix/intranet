<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Nebula extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->library('NebulaSessionHandler');
        $this->load->model(array(
            'configuration_model',
            'google_model',
            'navigation_model',
            'nebula_model',
            'nebula_vendor_model',
            'users_model'
        ));
    }

    public function index() {
        $this->load->library('session');

        if ($this->session->has_userdata('user')) {
            $userdata = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($userdata['email']);
            $user_id = $get_user_id->id;

            $is_google = $userdata['is_google'];
            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $userdata['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'nebula');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);

            // User access is elevated when they are granted access to one or more pages.
            $is_user_access_elevated = isset($nav) && count($nav) > 0;

            $ordered_nav = organize_nav_order($nav);
            $picture = $userdata['picture'];

            if (is_null($userdata['picture']) || $userdata['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $columns = $this->users_model->get_table_columns('nebula', $g_id);
            $purchase_orders = $this->nebula_model->purchase_orders();
            $directory_names = $this->nebula_model->directory_names();
            $directories_merged = array_merge($purchase_orders, $directory_names);

            $directories = array();

            foreach ($directories_merged as $directory) {
                $is_directory_already_added = false;

                foreach ($directories as $reference) {
                    if ($reference->name === $directory->name) {
                        $is_directory_already_added = true;
                        break;
                    }
                }

                if (!$is_directory_already_added) {
                    $directories[] = $directory;
                }
            }

            $lots = $this->nebula_model->load_lots();
            $vendors = $this->nebula_vendor_model->load_vendors();
            $vendorspo = $this->nebula_model->load_vendors();

            $data = array(
                'data' => array(
                    'user_picture' => $picture,
                    'user_email' => $userdata['email'],
                    'user_firstname' => $userdata['firstname'],
                    'user_lastname' => $userdata['lastname'],
                    'nav_menu' => $ordered_nav,
                    'page_name' => 'Nebula',
                    'is_google' => $is_google,
                    'is_user_access_elevated' => $is_user_access_elevated,
                    'columns' => json_decode($columns->nebula_columns)
                ),
                'directories' => $directories,
                'lots' => $lots,
                'vendors' => $vendors,
                'vendor_po' => $vendorspo
            );

            $this->load->view('modules/thanos/nebula', $data);
        } else {
            $data['data'] = array(
                'page_name' => 'Login'
            );

            $this->load->view('login', $data);
        }
    }

    /**
     * Create a new Nebula device
     *
     * return Output
     */
    public function create() {
        $serial_number = $this->input->post('serial_number');
        $part_number = $this->input->post('part_number');
        $vendor = $this->input->post('vendor');
        $purchase_order = $this->input->post('purchase_order');
        $size = $this->input->post('size');
        $result = $this->input->post('result');
        $status = $this->input->post('status');

        if (!isset($serial_number) || $serial_number === '') {
            return  $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode(array(
                    'is_error' => true,
                    'message' => 'Empty Serial Number'
                )));
        }

        $serial_number_count = $this->nebula_model->find_serial_number($serial_number);

        if ($serial_number_count > 0) {
            return  $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode(array(
                    'is_error' => true,
                    'message' => 'Serial Number exists'
                )));
        }

        $nebula = array();
        $nebula['device_serial_number'] = $serial_number;

        if (isset($part_number) && $part_number !== '') $nebula['device_product_name'] = $part_number;

        if (isset($size) && $size !== '') $nebula['device_size'] = $size;

        if (isset($result) && $result !== '') {
            $nebula['process_result'] = $result;
            $nebula['result'] = $result;
        }

        if (isset($status) && $status !== '') $nebula['status'] = $status;
        if (isset($purchase_order) && $purchase_order !== '') $nebula['parent_dir'] = $purchase_order;

        if (isset($vendor) && $vendor !== '') {
            $nebula['vendor'] = $vendor;
        }

        $insert_id = $this->nebula_model->create($nebula);

        if ($insert_id < 0 || $insert_id === false) {
            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode(array(
                    'is_error' => true,
                    'message' => 'Failed to add Serial Number'
                )));
        }

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode(array(
                'is_error' => false,
                'message' => 'Successfully added Serial Number'
            )));
    }

    function removeitems(){
        $serial_number = $this->input->post('serial_number');
        $removeitems = $this->nebula_model->removeitems($serial_number);

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode(array(
                'is_error' => false,
                'message' => 'Successfully removed Serial Number'
            )));
    }

    function getvendor(){
        $vendor = $this->input->post('vendor');
        $loadvendor = $this->nebula_model->load_vendor($vendor);
        if (!$loadvendor > 0){
            $data = 'No matched PO';
        }else{
            $data = array('purchase_order' => $loadvendor);
        }
    
    //    echo json_encode($data);
       return $this->output->set_header(200)
       ->set_content_type('application/json', 'utf-8')
       ->set_output(json_encode($data));
    }

    function certificate() {
        $this->load->library('session');

        if ($this->session->has_userdata('user')) {
            $userdata = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($userdata['email']);
            $user_id = $get_user_id->id;

            $is_google = $userdata['is_google'];
            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $userdata['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'nebula');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);

            // User access is elevated when they are granted access to one or more pages.
            $is_user_access_elevated = isset($nav) && count($nav) > 0;

            if (count($this->input->get()) === 0) {
                $drives = array();
                $drive_count = 0;
                $drives_passed_count = 0;
                $drives_failed_count = 0;
                $drives_undetected_count = 0;
            } else {
                $search_query = $this->input->get('searchQuery');
                $search_column = $this->input->get('searchColumn');
                $order_by = $this->input->get('orderBy');
                $sort_order = $this->input->get('sortOrder');
                $directories = $this->input->get('directories');

                if (isset($directories) && $directories !== '') {
                    $directories = explode(',', $directories);
                }

                $drives = $this->nebula_model->load_for_certificate(
                    trim($search_query),
                    $search_column,
                    $directories,
                    $order_by,
                    $sort_order
                );
                $drive_count = count($drives);
                $drives_passed = array_filter($drives, function($drive) {
                    return $drive->result === 'Erased';
                });

                $drives_failed = array_filter($drives, function($drive) {
                    return !in_array($drive->result, array('Erased', 'Defective', 'Undetected'));
                });

                $drives_undetected = array_filter($drives, function($drive) {
                    return in_array($drive->result, array('Defective', 'Undetected'));
                });

                $drives_passed_count = count($drives_passed);
                $drives_failed_count = count($drives_failed);
                $drives_undetected_count = count($drives_undetected);
            }

            $vendor_param = $this->input->get('vendor');

            if ($vendor_param === 'unixsurplus') {
                $vendor = 'resources/img/unixlogo.png';
            } else {
                $vendor = $this->nebula_vendor_model->load_vendor($vendor_param)->logo_path;
            }

            $data['data'] = array(
                'page_name' => 'Erase Certificate',
                'drives' => $drives,
                'drive_count' => $drive_count,
                'drives_passed_count' => $drives_passed_count,
                'drives_failed_count' => $drives_failed_count,
                'drives_undetected_count' => $drives_undetected_count,
                'vendor_image' => $vendor
            );

            return $this->load->view('modules/thanos/certificate', $data);
        } else {
            $data['data'] = array(
                'page_name' => 'Login'
            );

            $this->load->view('login', $data);
        }
    }

    public function load() {
        $user = $this->session->userdata('user');
        $search_query = $this->input->post('searchQuery');
        $sort_order = $this->input->post('sortOrder');
        $order_by = $this->input->post('orderBy');
        $search_column = $this->input->post('searchColumn');
        $directories = $this->input->post('directories');
        $current_offset = $this->input->post('currentOffset');
        $verify_mode = $this->input->post('verifyMode') === 'true';

        if (!isset($current_offset)) {
            $current_offset = 0;
        }

        $reports_verified = 0;

        if ($verify_mode) {
            $verifier = "{$user['firstname']} {$user['lastname']}";
            $verify_timestamp = date('Y-m-d H:i:s');

            $reports_verified = $this->nebula_model->scan(
                trim($search_query),
                $search_column,
                $directories,
                $verifier,
                $verify_timestamp
            );
        }

        try {
            $reports = $this->nebula_model->load(
                trim($search_query),
                $order_by,
                $sort_order,
                $search_column,
                $directories,
                $current_offset
            );
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }

        $columns = $this->users_model->get_table_columns('nebula', $user['id']);
        $table_columns = json_decode($columns->nebula_columns);

        $data = array(
            'reports' => $reports,
            'columns' => $table_columns,
            'offset' => $current_offset,
            'reports_verified' => $reports_verified
        );

        $view = $this->load->view('modules/thanos/ajax/reports', $data, true);

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode(array(
                'reports_verified' => $reports_verified,
                'view' => $view
            )));
    }

    function load_full_report() {
        $serial_number = $this->input->post('serial_number');

        try {
            $report = $this->nebula_model->full_report($serial_number);
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }

        $data = array(
            'report' => $report
        );

        return $this->load->view('modules/thanos/ajax/nebula_full_report_modal_content', $data);
    }

    function summary() {
        $search_query = $this->input->post('searchQuery');
        $sort_order = $this->input->post('sortOrder');
        $order_by = $this->input->post('orderBy');
        $search_column = $this->input->post('searchColumn');
        $directories_filter = $this->input->post('directories');

        $totals = array();
        $current_date_totals = array();

        $directory_names = $this->nebula_model->directory_names(
            trim($search_query),
            $search_column,
            $directories_filter
        );

        $device_results = $this->nebula_model->device_results(
            trim($search_query),
            $search_column,
            $directories_filter
        );

        $device_size_results = array();
        $device_size_totals = array();

        foreach ($device_results as $device_result) {
            $device_sizes = $this->nebula_model->device_sizes(
                trim($search_query),
                $search_column,
                $directories_filter,
                $device_result->result
            );

            $devices_verified = $this->nebula_model->devices_verified(
                trim($search_query),
                $search_column,
                $directories_filter
            );

            foreach ($device_sizes as $device_size) {
                $result_split = str_split($device_result->result);
                $result_split[0] = strtoupper($result_split[0]);
                $result = implode($result_split);

                $device_size_results[$result][$device_size->device_size]['size'] =
                    $device_size->device_count;
            }

            foreach ($devices_verified as $device_verified) {
                $result_split = str_split($device_verified->result);
                $result_split[0] = strtoupper($result_split[0]);
                $result = implode($result_split);

                $device_size_results[$result][$device_verified->device_size]['verified'] =
                    $device_verified->verified;
            }

            $device_count = array_map(function($device_total) {
                return $device_total->device_count;
            }, $device_sizes);
            $device_size_totals[$device_result->result] = array_sum($device_count);
        }

        $directories = array_map(function($directory) { return $directory->name; }, $directory_names);

        try {
            foreach ($directories as $directory) {
                $directory_total = $this->nebula_model->directory_totals(
                    trim($search_query),
                    $search_column === '' ? null : $search_column,
                    $directory
                );

                $totals[] = array(
                    'directory' => $directory,
                    'total' => $directory_total->total
                );

                $directory_current_date_total = $this->nebula_model->directory_current_date_totals(
                    trim($search_query),
                    $search_column === '' ? null : $search_column,
                    $directory
                );

                $current_date_totals[] = array(
                    'directory' => $directory,
                    'total' => $directory_current_date_total->total
                );
            }
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }

        $data = array(
            'totals' => $totals,
            'current_date_totals' => $current_date_totals,
            'device_size_results' => $device_size_results,
            'device_size_totals' => $device_size_totals
        );

        return $this->load->view('modules/thanos/ajax/nebula_summary', $data);
    }

    public function exportCSV() {
        $search_query = $this->input->post('searchQuery');
        $sort_order = $this->input->post('sortOrder');
        $order_by = $this->input->post('orderBy');
        $search_column = $this->input->post('searchColumn');
        $directories = $this->input->post('directories');

        try {
            $drives_result = $this->nebula_model->summary_csv(
                trim($search_query),
                $search_column,
                $directories
            );

            $items = $this->nebula_model->reports_csv(
                trim($search_query),
                $order_by,
                $sort_order,
                $search_column,
                $directories
            );
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response));
        }

        $drives_passed = array_filter($drives_result, function ($drive) {
            return $drive->result === 'Erased';
        });

        $drives_failed = array_filter($drives_result, function ($drive) {
            return !in_array($drive->result, array('Erased', 'Defective', 'Undetected'));
        });

        $drives_undetected = array_filter($drives_result, function($drive) {
            return in_array($drive->result, array('Defective', 'Undetected'));
        });

        $drives_count = count($drives_result);
        $drives_passed_count = count($drives_passed);
        $drives_failed_count = count($drives_failed);
        $drives_undetected_count = count($drives_undetected);

        $summary_csv = "\"drives\", \"pass\", \"failed\", \"undetected\", \"erase method\", \"pass 1\", \"pass 2\", "
            . "\"pass 3\", \"verification\"\n"
            . "$drives_count, $drives_passed_count, $drives_failed_count, $drives_undetected_count, "
            . "\"US DoD 5220.22-M\", \"Pass 1 (0x000000000000)\", \"N/A\", \"N/A\", \"10%\"\n\n"
            . $items;

        return $this->output->set_header(200)
            ->set_content_type('text/csv', 'utf-8')
            ->set_output($summary_csv);
    }

    public function batch_assign_cost_sell() {
        $serial_numbers = $this->input->post('serial_numbers');
        $assignment_option = $this->input->post('assignment_option');
        $value = $this->input->post('value');

        $serial_number_count = count($serial_numbers);
        $reports_updated = $this->nebula_model->batch_assign_cost_sell($serial_numbers, $assignment_option, $value);

        if ($serial_number_count === $reports_updated) {
            $data = array(
                'title' => 'Batch Assign Success',
                'message' => "Successfully assigned reports with $assignment_option \$$value"
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'title' => 'Batch Assign Failed',
            'message' => "Failed to assign reports with $assignment_option \$$value"
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    public function update_disposition() {
        $serial_numbers = $this->input->post('serial_numbers');
        $disposition_input = $this->input->post('disposition');
        $disposition = $disposition_input === 'Clear' ? null: $disposition_input;

        $serial_number_count = count($serial_numbers);

        try {
            $reports_updated = $this->nebula_model->update_disposition($serial_numbers, $disposition);
        } catch (Exception $e) {
            $data = array(
                'is_error' => true,
                'message' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        if ($reports_updated === $serial_number_count) {
            $data = array(
                'is_error' => false,
                'message' => 'Successfully updated reports.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'is_error' => true,
            'message' => 'Unable to update reports.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function apply_report_edit() {
        $serial_number = $this->input->post('serial_number');
        $field = $this->input->post('field');
        $value = $this->input->post('value');

        $data = array( $field => $value );

        if ($field === 'status') {
            $userdata = $this->session->userdata('user');
            $current_user_firstname = $userdata['firstname'];
            $current_user_lastname = $userdata['lastname'];
            $current_user = "$current_user_firstname $current_user_lastname";
            $data['status_last_edited'] = date('Y-m-d H:m:s');
            $data['status_last_edited_by'] = $current_user;
        }

        try {
            $reports_updated = $this->nebula_model->update($serial_number, $data);
        } catch (Exception $e) {
            $data = array(
                'is_error' => true,
                'message' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        if ($reports_updated > 0) {
            $userdata = $this->session->userdata('user');
            $current_user = "{$userdata['firstname']} {$userdata['lastname']}";
            $timestamp = date('Y-m-d H:m:s');

            $data = array(
                'is_error' => false,
                'current_user' => $current_user,
                'timestamp' => $timestamp,
                'message' => "Successfully updated report with $field \$$value."
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'is_error' => true,
            'message' => "Unable to update report with $field \$$value."
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function create_purchase_order() {
        $location = $this->input->post('location');
        $vendor = $this->input->post('vendor');
        $name = $this->input->post('name');
        $name = "PO$name";

        try {
            $result = $this->nebula_model->check_purchase_order_exists($name);

            if ($result > 0) {
                $data = array(
                    'is_error' => true,
                    'title' => 'Purchase Order Exists',
                    'message' => 'Purchase order already exists'
                );

                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            }

            $this->nebula_model->create_purchase_order($location, $name, $vendor);

            $location_ip = $location === 'merced' ? 'nebula_merced_ip' : 'nebula_ip';
            $location_port = $location === 'merced' ? 'nebula_merced_port' : 'nebula_port';

            $nebula_ip = $this->configuration_model->getConfiguration($location_ip)[0];
            $nebula_port = $this->configuration_model->getConfiguration($location_port)[0];
            $nebula_home = $this->configuration_model->getConfiguration('nebula_home')[0];
            $nebula_username = $this->configuration_model->getConfiguration('nebula_username')[0];
            $nebula_password = $this->configuration_model->getConfiguration('nebula_password')[0];

            $session_handler = new NebulaSessionHandler($nebula_ip->value, $nebula_port->value, $nebula_home->value);
            $session_handler->login($nebula_username->value, $nebula_password->value);
            $session_handler->create_purchase_order($name);
            $session_handler->disconnect();
        } catch (Exception $e) {
            if ($results > 0) {
                $data = array(
                    'is_error' => true,
                    'title' => 'Create Purchase Order Error',
                    'message' => 'An unexpected error occurred while attempting to create Purchase Order.'
                );

                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            }
        }

        $data = array(
            'title' => 'Create Purchase Order Success',
            'message' => 'Successfully created purchase order.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function rename_purchase_order() {
        $old_name = $this->input->post('old_name');
        $new_name = $this->input->post('new_name');

        $old_purchase_order = $this->nebula_model->find_purchase_order($old_name);

        try {
            $location_ip = $old_purchase_order->location === 'merced' ? 'nebula_merced_ip' : 'nebula_ip';
            $location_port = $old_purchase_order->location === 'merced' ? 'nebula_merced_port' : 'nebula_port';
            $nebula_ip = $this->configuration_model->getConfiguration($location_ip)[0];
            $nebula_port = $this->configuration_model->getConfiguration($location_port)[0];
            $nebula_home = $this->configuration_model->getConfiguration('nebula_home')[0];
            $nebula_username = $this->configuration_model->getConfiguration('nebula_username')[0];
            $nebula_password = $this->configuration_model->getConfiguration('nebula_password')[0];

            $session_handler = new NebulaSessionHandler($nebula_ip->value, $nebula_port->value, $nebula_home->value);
            $session_handler->login($nebula_username->value, $nebula_password->value);

            if (strpos(strtolower($new_name), 'po') === false) {
                $new_name = "PO$new_name";
            }

            if (!$session_handler->rename_purchase_order($old_name, $new_name)) {
                $data = array(
                    'is_error' => true,
                    'title' => 'Rename Purchase Order Failed',
                    'message' => 'Unable to rename purchase order at the remote server.'
                );

                $session_handler->disconnect();

                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            }

            $session_handler->disconnect();

            $this->nebula_model->rename_purchase_order($old_name, $new_name);
            $this->nebula_model->change_lot_purchase_orders($old_name, $new_name);
        } catch (Exception $e) {
            $data = array(
                'is_error' => true,
                'title' => 'Rename Purchase Order Failed',
                'message' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
        }

        $data = array(
            'title' => 'Rename Purchase Order Success',
            'message' => 'Successfully renamed purchase order.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function create_lot() {
        $directory = $this->input->post('directory');
        $customer_code = $this->input->post('customer_code');

        try {
            $results = $this->nebula_model->check_lot_exists($directory, $customer_code);
            $purchase_order = $this->nebula_model->find_purchase_order($directory);

            if ($results > 0) {
                $data = array(
                    'is_error' => true,
                    'error_type' => 'lot_exists'
                );

                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            }

            $this->nebula_model->create_lot($directory, $customer_code);

            $location_ip = $purchase_order->location === 'merced' ? 'nebula_merced_ip' : 'nebula_ip';
            $location_port = $purchase_order->location === 'merced' ? 'nebula_merced_port' : 'nebula_port';

            $nebula_ip = $this->configuration_model->getConfiguration($location_ip)[0];
            $nebula_port = $this->configuration_model->getConfiguration($location_port)[0];
            $nebula_home = $this->configuration_model->getConfiguration('nebula_home')[0];
            $nebula_username = $this->configuration_model->getConfiguration('nebula_username')[0];
            $nebula_password = $this->configuration_model->getConfiguration('nebula_password')[0];

            $session_handler = new NebulaSessionHandler($nebula_ip->value, $nebula_port->value, $nebula_home->value);
            $session_handler->login($nebula_username->value, $nebula_password->value);

            $lot = "$directory $customer_code";

            $session_handler->create_lot($lot, $directory);
            $session_handler->disconnect();
        } catch (Exception $e) {
            $data = array(
                'title' => 'Create Lot Error',
                'message' => 'Unable to create lot.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'title' => 'Create Lot Success',
            'message' => 'Successfully created lot.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function rename_lot() {
        $lot_selected = $this->input->post('lot');
        $new_customer_code = $this->input->post('customer_code');

        $lot_split = explode(' ', $lot_selected);
        $purchase_order_selected = $lot_split[0];

        try {
            $purchase_order = $this->nebula_model->find_purchase_order($purchase_order_selected);

            $nebula_ip_code = $purchase_order->location === 'merced' ? 'nebula_merced_ip' : 'nebula_ip';
            $nebula_port_code = $purchase_order->location === 'merced' ? 'nebula_merced_port' : 'nebula_port';

            $nebula_ip = $this->configuration_model->getConfiguration($nebula_ip_code)[0];
            $nebula_port = $this->configuration_model->getConfiguration($nebula_port_code)[0];
            $nebula_home = $this->configuration_model->getConfiguration('nebula_home')[0];
            $nebula_username = $this->configuration_model->getConfiguration('nebula_username')[0];
            $nebula_password = $this->configuration_model->getConfiguration('nebula_password')[0];

            $session_handler = new NebulaSessionHandler(
                $nebula_ip->value, (int) ($nebula_port->value), $nebula_home->value
            );

            if (!$session_handler->login($nebula_username->value, $nebula_password->value)) {
                $data = array(
                    'is_error' => true,
                    'title' => 'Rename Lot Failed',
                    'message' => 'Unable to rename lot at the remote server.'
                );

                $session_handler->disconnect();

                return $this->output->set_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            }

            $old_lot = implode(' ', array_slice($lot_split, 1));
            $new_lot = "{$purchase_order->name} $new_customer_code";

            $session_handler->rename_lot($purchase_order->name, $old_lot, $new_lot);
            $this->nebula_model->rename_lot($purchase_order->name, $old_lot, $new_customer_code);

            $session_handler->disconnect();
        } catch (Exception $e) {
            $data = array(
                'is_error' => true,
                'title' => 'Rename Lot Failed',
                'message' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'title' => 'Rename Lot Success',
            'message' => 'Lot successfully renamed.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }

    function load_lots() {
        $directories = $this->input->post('directories');

        try {
            $lots = $this->nebula_model->load_lots($directories);
        } catch (Exception $e) {
            return "An unexpected error occurred. Please contact the site admin.";
        }

        $data = array(
            'lots' => $lots
        );

        return $this->load->view('modules/thanos/ajax/nebula_lots', $data);
    }

    function assign_lot() {
        $lot = $this->input->post('lot');
        $reports = $this->input->post('reports');

        try {
            $lots_assigned = $this->nebula_model->assign_lot($lot, $reports);
        } catch (Exception $e) {
            $data = array(
                'is_error' => true,
                'title' => 'Assign Lot Error',
                'message' => 'An unexpected error occurred. Please contact the site admin.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        if ($lots_assigned === count($reports)) {
            $data = array(
                'title' => 'Assign Lot Success',
                'message' => 'Successfully assigned lots to the selected reports.'
            );

            return $this->output->set_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $data = array(
            'title' => 'Assign Lot Mismatch',
            'message' => 'Lots assigned to some of the reports selected.'
        );

        return $this->output->set_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data));
    }
}
?>
