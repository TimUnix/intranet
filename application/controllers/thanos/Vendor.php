<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {
    private $is_user_logged_in;
    private $is_user_authorized;

    public function __construct() {
        parent::__construct();

        $this->load->helper(array('nav_order', 'user_access'));
        $this->load->model(array(
            'google_model',
            'navigation_model',
            'nebula_vendor_model',
            'users_model'
        ));

        $this->load->library('session');

        $this->is_user_logged_in = $this->session->has_userdata('user');
        $this->is_user_authorized = $this->check_user_access();
    }

    public function index() {
        if (!$this->is_user_logged_in) {
            $data['data'] = array(
                'page_name' => 'Login'
            );

			return $this->load->view('login', $data);
        }

        if (!$this->is_user_authorized) {
            return redirect('access_denied');
        }

        $userdata = $this->session->userdata('user');
        $nav = $this->navigation_model->get_navigation_menu($userdata['id']);
        $is_google = isset($userdata['is_google']) ? $userdata['is_google'] : '';

        // User access is elevated when they are granted access to one or more pages.
        $is_user_access_elevated = isset($nav) && count($nav) > 0;

        $ordered_nav = organize_nav_order($nav);
        $picture = $userdata['picture'];

        if (is_null($userdata['picture']) || $userdata['picture'] == '') {
            $picture = base_url() . 'resources/img/noimage.png';
        }

        $vendors = $this->nebula_vendor_model->load_vendors();

        $data = array(
            'data' => array(
                'user_picture' => $picture,
                'user_email' => $userdata['email'],
                'user_firstname' => $userdata['firstname'],
                'user_lastname' => $userdata['lastname'],
                'nav_menu' => $ordered_nav,
                'page_name' => 'Vendor',
                'is_google' => $is_google,
                'is_user_access_elevated' => $is_user_access_elevated,
            ),
            'vendors' => $vendors
        );

        return $this->load->view('modules/thanos/vendor/index', $data);
    }

    public function add_vendor() {
        if (!$this->is_user_logged_in || !$this->is_user_authorized) {
            return;
        }

        $vendor_name = $this->input->post('vendor-name');
        $filename = strtolower(str_replace(' ', '_', $vendor_name));

        $is_vendor_existing = $this->nebula_vendor_model->check_vendor_exists($vendor_name);

        if ($is_vendor_existing) {
            $data = array(
                'error' => array(
                    'title' => 'Add Vendor Error',
                    'message' => 'Vendor already exists'
                )
            );

            return $this->output->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $logo_path = null;

        if ($this->input->post('logo-path') !== null) {
            $upload_config = array(
                'upload_path' => './nebula/vendors/',
                'allowed_types' => 'gif|jpg|png|svg',
                'file_name' => $filename
            );

            $this->load->library('upload', $upload_config);

            if (!$this->upload->do_upload('logo-path')){
                $data = array(
                    'error' => array(
                        'title' => 'Upload Error',
                        'message' => $this->upload->display_errors(),
                    )
                );

                return $this->output->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($data));
            }

            $upload_data = $this->upload->data();
            $logo_path = "nebula/vendors/{$upload_data['file_name']}";
        }

        $this->nebula_vendor_model->add_vendor($vendor_name, $logo_path);

        return $this->output->set_status_header(204);
    }

    public function change_name() {
        if (!$this->is_user_logged_in || !$this->is_user_authorized) {
            return;
        }

        $old_name = $this->input->post('old_name');
        $new_name = $this->input->post('new_name');

        $vendor_exists = $this->nebula_vendor_model->check_vendor_exists($new_name);

        if ($vendor_exists) {
            $data = array(
                'error' => array(
                    'title' => 'Change Name Error',
                    'message' => 'Name is already taken'
                )
            );

            return $this->output->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $name_changed = $this->nebula_vendor_model->change_name($old_name, $new_name);

        if (!$name_changed) {
            $data = array(
                'error' => array(
                    'title' => 'Change Name Error',
                    'message' => 'Failed to change vendor\'s name'
                )
            );

            return $this->output->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }
    }

    public function update_logo() {
        if (!$this->is_user_logged_in || !$this->is_user_authorized) {
            return;
        }

        $vendor = $this->input->post('vendor_name');

        $vendor_logo = $this->nebula_vendor_model->load_logo_path($vendor);

        unlink($vendor_logo->logo_path);

        $filename = strtolower(str_replace(' ', '_', $vendor));

        $upload_config = array(
            'upload_path' => './nebula/vendors/',
            'allowed_types' => 'gif|jpg|png|svg',
            'file_name' => $filename
        );

        $this->load->library('upload', $upload_config);

        if (!$this->upload->do_upload('vendor_logo')){
            $data = array(
                'error' => array(
                    'title' => 'Upload Error',
                    'message' => $this->upload->display_errors(),
                )
            );

            return $this->output->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data));
        }

        $upload_data = $this->upload->data();
        $logo_path = "nebula/vendors/{$upload_data['file_name']}";
        $this->nebula_vendor_model->update_logo($vendor, $logo_path);

        return $this->output->set_status_header(204);
    }

    private function check_user_access() {
        $userdata = $this->session->userdata('user');
        $get_user_id = $this->google_model->getFields($userdata['email']);
        $user_id = $get_user_id->id;

        $is_google = isset($userdata['is_google']) ? $userdata['is_google'] : "";

        if ($is_google == true){
            $g_id = $user_id;
        } else {
            $g_id = $userdata['id'];
        }

        $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'nebula/vendor');

        return check_user_access_permissions($user_access_result->menu_access);
    }
}
