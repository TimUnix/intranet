<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_access extends CI_Controller {
    private $is_user_authorized;

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model', 'navigation_model', 'google_model', 'access_model'
        ));
        $this->load->library('session');
    }

    public function index() {
        if (!$this->session->has_userdata('user')) {
            $data['data'] = array(
                'page_name' => 'Login'
            );

			return $this->load->view('login', $data);
        }

        $data_sess = $this->session->userdata('user');
        $get_user_id = $this->google_model->getFields($data_sess['email']);
        $user_id = $get_user_id->id;

        $is_google = isset($data_sess['is_google']) ? $data_sess['is_google']: "";

        if ($is_google == true){
            $g_id = $user_id;
        } else {
            $g_id = $data_sess['id'];
        }

        $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'users_access');
        $this->is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

        if (!$this->is_user_authorized) return redirect('access_denied');

        $picture = $data_sess['picture'];

        $nav = $this->navigation_model->get_navigation_menu($g_id);
        $ordered_nav = organize_nav_order($nav);

        if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
            $picture = base_url() . 'resources/img/noimage.png';
        }

        $menu_headers = $this->access_model->get_menu_headers();
        $user_count = $this->access_model->get_user_count();
        $users_access_result = $this->access_model->get_users_access();
		
        $users_access = array();
	
        foreach ($users_access_result as $access) {
            $current_user = "{$access->user_firstname} {$access->user_lastname}";
            $users_access[$current_user][] = array(
                'user_id' => $access->user_id,
                'menu_id' => $access->menu_id,
                'menu_access' => $access->menu_access,
				'menu_title' => $access->menu_title
            );
        }

        $userdata = array(
            'data' => array(
                'user_picture'	=>	$picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'	=>	$user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'menu_headers' => $menu_headers
            ),
            'user_count' => $user_count,
            'users_access' => $users_access
        );
        return $this->load->view('modules/admin/users_access', $userdata);
		
    }

	function get_users(){
        $users = $this->access_model->get_users();

        $data = array(
            'items' => $users,
        );
	
        return $this->load->view('modules/admin/ajax/users_access_body', $data);  
    }

	function get_modules(){
        $modules = $this->access_model->get_modules();

        $data = array(
            'items' => $modules,
        );
	
        return $this->load->view('modules/admin/ajax/modules_access_body', $data);  
    }

    public function update_menu_access() {
        $access = $this->input->get('access');
        $menu_id = $this->input->get('menu_id');
		$user_id = $this->input->get('user_id');

		$this->access_model->update_menu_access($access, $menu_id, $user_id);
    }

	public function search_users() {
        $searchItem = $this->input->get('searchItem');
            $data = array(
                'searchItem' => $searchItem
            );

            $get_items = $this->access_model->search_users(
                $searchItem === '' ? null : $searchItem
            ); 
            
            $data = array(
                'items' => $get_items
            );
            return $this->load->view('modules/admin/ajax/users_access_body',$data);

    }

	public function search_modules() {
        $searchItem = $this->input->get('searchItem');
            $data = array(
                'searchItem' => $searchItem
            );

            $get_items = $this->access_model->search_modules(
                $searchItem === '' ? null : $searchItem
            ); 
            
            $data = array(
                'items' => $get_items
            );
            return $this->load->view('modules/admin/ajax/modules_access_body',$data);
    }

	public function show_modules() {
		$user_id = $this->input->get('user_id');

		$show_modules = $this->access_model->show_modules(
			$user_id
		);

		if (empty($show_modules)) {
			$show_menu = $this->access_model->get_modules();

			$data = array (
				'menus' => $show_menu
			);

			return $this->load->view('modules/admin/ajax/menu_access_body',$data);

		} else {
			$data = array (
				'items' => $show_modules
			);
			
			return $this->load->view('modules/admin/ajax/modules_access_body',$data);
		}
	}

	public function show_modules_checker(){
		$user_id = $this->input->get('user_id');

		$show_modules = $this->access_model->show_modules(
			$user_id
		);

		if (empty($show_modules)) {
			$data = array (
				'menu_checker' => true
			);
			
		return $this->output->set_header(200)
									->set_content_type('application/json', 'utf-8')
									->set_output(json_encode($data));
		}
	}

	public function insert_modules(){
		$user_id = $this->input->get('user_id');
		$menus = $this->access_model->get_menus();
        $insert_menu = [];

        foreach($menus as $menu){
            $menu_id = $menu->menu_id;

            $insert_menu[] = array(
                'user_id' => $user_id,
                'menu_id' => $menu_id,
                'menu_access' => '0'
            );
        }
		
		$this->access_model->insert_modules($insert_menu);
	}



	public function superadmin() {
		$access = $this->input->get('access');
		$user_id = $this->input->get('user_id');

		$this->access_model->superadmin($access, $user_id);
	}

	public function check_superadmin() {
		$user_id = $this->input->get('user_id');

		$count_menu = $this->access_model->count_menu();
		$count_users = $this->access_model->count_users($user_id);

		foreach($count_menu as $menu) {
			foreach($count_users as $users) {
				$total_menu = $menu->total;
				$total_users = $users->total;

				if ($total_menu != $total_users) {
					$admin = 0;
				} else if ($total_menu == $total_users) {
					$admin = 1;
				}

				$data = array (
					'admin' => $admin
				);

				return $this->output->set_header(200)
                                        ->set_content_type('application/json', 'utf-8')
                                        ->set_output(json_encode($data));
			}
		}


	}
}
