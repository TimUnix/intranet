<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chassis_module extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'cpu_model',
            'chassis_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'chassis_module');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $get_chassis= $this->chassis_model->get_chassis();
            $search_history = $this->chassis_model->get_search_history_for_user($g_id);
            $columns = $this->users_model->get_table_columns('chassis_module', $g_id);

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'get_chassis' => $get_chassis,
                'columns' => json_decode($columns->chassis_module_columns)
            );
            $userdata['search_history'] =  $search_history;
            $userdata['field'] = $this->chassis_model->get_field();
            

            $this->load->view('modules/chassis_module/chassis_main_page', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    function request() {
        if ($this->uri->rsegment(3) == 'create_listing') {
            self::_request_create_listing();
        }
        
        if ($this->uri->rsegment(3) == "update_columns") {
            self::_request_update_columns();
        }
        if ($this->uri->rsegment(3) == "delete_chassis") {
            self::_request_delete_chassis();
        }if ($this->uri->rsegment(3) == "insertchassis") {
            self::_request_insert_chassis();
        }
    }
    function getchassis(){
        $get_chassis['chassis']= $this->chassis_model->get_chassis();
        $user = $this->session->userdata('user');
        $columns = $this->users_model->get_table_columns('chassis_module', $user['id']);
        $get_chassis['columns'] = json_decode($columns->chassis_module_columns);
        $this->load->view('modules/chassis_module/ajax/table', $get_chassis);
    }
    function get_chassis_specific(){
        $partnumber = $this->input->post('part_number');
        $sortOrder = $this->input->post('sortOrder');
        $orderBy = $this->input->post('orderBy');
        
        $data_sess = $this->session->userdata('user');
        $user_email = $data_sess['email'];
        $get_user_id = $this->chassis_model->get_user_id($user_email);
        foreach($get_user_id as $id){
            $user_id = $id->id;
            $get_search_exist = $this->chassis_model->get_search_exist($user_id, $partnumber);
            if($get_search_exist){
                $data = array('updated'=> date('Y-m-d H:i:s'));
                $insert_search = $this->chassis_model->update_chassis_search($user_id, $partnumber, $data);
            }else{
                $data = array(
                    'user_id' => $user_id,
                    'search' => $partnumber,
                    'updated' => date('Y-m-d H:i:s')
                );
                $this->chassis_model->insert_chassis_search($data);
            }
            

        }

        $user = $this->session->userdata('user');
        $columns = $this->users_model->get_table_columns('chassis_module', $user['id']);
        $get_chassis['columns'] = json_decode($columns->chassis_module_columns);
        
        $get_chassis['chassis']= $this->chassis_model->get_chassis_search($partnumber, $sortOrder, $orderBy);
        $this->load->view('modules/chassis_module/ajax/table', $get_chassis);
        

    }

    private function detect_listing_action() {
        $referred = $this->input->get('referred');
        $action = $this->input->get('createtask');
        $sku = $this->input->get('new_listing');
        $price = $this->input->get('price');

        if (isset($referred) && $referred == '1') {
            return array(
                'action' => $action,
                'sku' => $sku,
                'price' => $price
            );

        }

        return null;
    }

    public function export_csv() {
        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        
        try {
            $csv = $this->chassis_model->generate_csv(
                trim($search),
                $sort_order,
                $order_by
                
               
            );
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function load_listing() {
        $user = $this->session->userdata('user');

        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        $request_channels = $this->input->post('channels');
        $request_actions = $this->input->post('actions');
        $request_users = $this->input->post('users');
        $banner = $this->input->post('banner');
        $search_column = $this->input->post('search_column');
        $is_done_shown = $this->input->post('is_done_shown') === 'true';
        $is_only_me_enabled = $this->input->post('is_only_me_enabled') === 'true';

        $channels = isset($request_channels) && $request_channels !== '' ? explode(',', $request_channels) : null;
        $actions = isset($request_actions) && $request_actions !== '' ? explode(',', $request_actions) : null;

        if ($is_only_me_enabled) {
            $users = array(trim($user['firstname']) . ' ' . trim($user['lastname']));
        } else {
            $users = isset($request_users) && $request_users !== '' ? explode(',', $request_users) : null;
        }

        $data['listing_added'] = $this->task_model->load(
            trim($search),
            $order_by,
            $sort_order,
            $channels,
            $actions,
            $banner,
            $users,
            $search_column,
            $is_done_shown
        );

        $columns = $this->users_model->get_table_columns('task', $user['id']);
        $data['columns'] = json_decode($columns->task_columns);

        $this->load->view("modules/task/ajax/listingtable", $data);
    }

    public function _request_update_columns() {
        $data = $this->input->post('data');
        $partnumber = $data['part_number'];
        //$stepcode = $data['stepcode'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->chassis_model->update_chassiscol($col, $stepcode, $partnumber);
    }

    public function _request_delete_chassis(){
        $data = $this->input->post('data');
        $partnumber = $data['part_number'];
        ///$stepcode = $data['stepcode'];
        $orderby = $data['orderby'];
        $sortorder = $data['sortorder'];
        $partnumbertext = $data['partnumbertext'];
        
        $deleteone = $this->chassis_model->delete_chassiscol($partnumber, $stepcode);
        
            $get_chassis['chassis']= $this->chassis_model->get_chassis_search($partnumbertext, $sortorder, $orderby);
            $this->load->view('modules/chassis_module/ajax/table', $get_chassis);
        


        
    }


    public function _request_insert_chassis(){
        $partnumber = $this->input->post('partnumber');
        $mfr = $this->input->post('mfr');
        $u = $this->input->post('u');
        $model = $this->input->post('model');
        $ff = $this->input->post('ff');
        $psu = $this->input->post('psu');
        $motherboard = $this->input->post('motherboard');
        $controller = $this->input->post('controller');
        $bin = $this->input->post('bin');
        $node = $this->input->post('node');
        $backplane = $this->input->post('backplane');
        $drivebays = $this->input->post('drivebays');
        $rearbays = $this->input->post('rearbays');
        $con = $this->input->post('con');
        

        $this->load->helper(array('form', 'url'));

         $this->load->library('form_validation');

         
        $this->form_validation->set_rules('partnumber', 'PartNumber', 'trim|required');
        $this->form_validation->set_rules('mfr', 'MFR', 'trim|required');
        $this->form_validation->set_rules('u', 'U', 'trim|required');
        $this->form_validation->set_rules('ff', 'FF', 'trim|required');
        $this->form_validation->set_rules('psu', 'PSU', 'trim|required');
        $this->form_validation->set_rules('motherboard', 'Motherboard', 'trim|required');
        $this->form_validation->set_rules('bin', 'Bin', 'trim|required');
        $this->form_validation->set_rules('node', 'Node', 'trim|required');
        $this->form_validation->set_rules('backplane', 'backplane', 'trim|required');
        $this->form_validation->set_rules('drivebays', 'Drivebays', 'trim|required');
        $this->form_validation->set_rules('rearbays', 'Rearbays', 'trim|required');
        $this->form_validation->set_rules('con', 'Con', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){
            /*$validation_message = $this->form_validation->error_array();
            foreach($validation_message as $valm){
                $emptyfield = $valm;
            }*/
            $message = validation_errors();
            $alert = "warning";
            $data = array(
                'status' => 200,
                'message' => $message,
                'isSuccess' => trim($alert)
            );
    
            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }else{
            $data = array(
                'part_number' =>  $partnumber,
                'mfr' => $mfr,
                'u' => $u,
                'model' => $model,
                'ff' => $ff,
                'psu' => $psu,
                'motherboard' => $motherboard,
                'controller' => $controller,
                'bin' => $bin,
                'node' => $node,
                'backplane' => $backplane,
                'drivebays' => $drivebays,
                'rearbays' => $rearbays,
                'con' => $con
            );
            $check_chassis = $this->chassis_model->check_chassis_part($partnumber);
            if($check_chassis == "PartNumber is Already Exist"){
                $message = "PartNumber is Already Exist";
                $alert = "warning";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }else if($check_chassis == "Not Found"){
                $this->chassis_model->insert_new_chassis($data);
                $message = "Chassis Inserted Successfully";
                $alert = "success";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }
        }
        
        ///$this->cpu_model->insert_new_cpu($data);                
    }

    

    

    

    
}
