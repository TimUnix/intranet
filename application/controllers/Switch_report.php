<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class switch_report extends CI_Controller {

        function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'navigation_model',
            'google_model',
            'switch_report_model',
            'users_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'switch_report');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $data = array(
                'data' => array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
            ));
            $this->load->view('modules/switch_report/index', $data);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    public function load_report(){
        $last_id = $this->input->post('last_id');
        $items_offset = (int) $this->input->post('items_offset');
        $get_item = $this->switch_report_model->get_report($last_id, $items_offset); 
      
        $data = array(
            'offset' => $items_offset,
            'items' => $get_item
        );

        return $this->load->view('modules/switch_report/switch_report_table_body', $data);        
    }

   

    public function search_item() {
        $searchItem = $this->input->get('searchItem');
        $items_offset = (int) $this->input->post('items_offset');
       

            $data = array(
                'searchItem' => $searchItem,
                'offset' => $items_offset,
             
            );

            $get_items = $this->switch_report_model->search_it(
                $searchItem === '' ? null : $searchItem,
                $items_offset
            ); 
            
            $data = array(
                'items' => $get_items,
                'offset' => $items_offset
            );

            return $this->load->view('modules/switch_report/switch_report_table_body', $data);      
    }

    public function update_columns() {
        $data = $this->input->post('data');
        $serial = $data['serial'];
        $id = $data['id'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $this->switch_report_model->update_col($col,$serial,$id);  
    }

    public function createListing(){
        $eqm_mpn = $this->input->post('eqm_mpn');
        $eqm_des = $this->input->post('eqm_des');
        $data_dest = $this->input->post('data_dest');
        $qty = $this->input->post('qty');
        $boxed = $this->input->post('boxed');
        $despostion = $this->input->post('despostion');
        $serial = $this->input->post('serial');
        $recieved = $this->input->post('recieved');
        $notes = $this->input->post('notes');
    

        $exit_qty_empty = 0;
      
        if($serial === '' ){
            return $exit_qty_empty;
        }else{
            $data = array(
                'eqm_mpn' => $eqm_mpn,
                'eqm_des' => $eqm_des,
                'data_dest' => $data_dest,
                'qty' => $qty,
                'boxed' => $boxed,
                'despostion' => $despostion,
                'serial' => $serial,
                'recieved' => $recieved,
                'notes' => $notes
            );

        $this->switch_report_model->insert_listing($data);
        $this->load->view("modules/switch_report/switch_report_table_body", $data);

        }
    }


}

?>
