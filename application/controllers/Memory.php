<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Memory extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'task_model',
            'inventory_model',
            'memory_model'
        ));
    }

    public function index() {
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if ($this->session->userdata('user')) {
            
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google'] : "";

            if ($is_google == true) {
                $g_id = $user_id;
            } else {
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'memory');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $get_memory= $this->memory_model->get_memory();
            $search_history = $this->memory_model->get_search_history_for_user($g_id);
            $columns = $this->users_model->get_table_columns('memory', $g_id);

            $userdata['data'] = array(
                'user_picture'    =>    $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name'    =>    $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'get_memory' => $get_memory,
                'columns' => json_decode($columns->memory_columns)
            );
            $userdata['search_history'] =  $search_history;
            $userdata['field'] = $this->memory_model->get_field();
            

            $this->load->view('modules/memory/memory_main_page', $userdata);
        } else {
            $data['data'] = array('page_name' => 'Login');

            $this->load->view('login', $data);
        }
    }

    function request() {
        if ($this->uri->rsegment(3) == 'create_listing') {
            self::_request_create_listing();
        }
        
        if ($this->uri->rsegment(3) == "update_columns") {
            self::_request_update_columns();
        }
        if ($this->uri->rsegment(3) == "delete_memory") {
            self::_request_delete_memory();
        }if ($this->uri->rsegment(3) == "insertmem") {
            self::_request_insert_mem();
        }
    }
    function getmemory(){
        $get_memory['memory']= $this->memory_model->get_memory();
        $user = $this->session->userdata('user');
        $columns = $this->users_model->get_table_columns('memory', $user['id']);
        $get_memory['columns'] = json_decode($columns->memory_columns);
        $this->load->view('modules/memory/ajax/memory_table', $get_memory);
    }
    function get_memory_specific(){
        $sku = $this->input->post('sku');
        $sortOrder = $this->input->post('sortOrder');
        $orderBy = $this->input->post('orderBy');echo $sortOrder;
        
        $data_sess = $this->session->userdata('user');
        $user_email = $data_sess['email'];
        $get_user_id = $this->memory_model->get_user_id($user_email);
        foreach($get_user_id as $id){
            $user_id = $id->id;
            $get_search_exist = $this->memory_model->get_search_exist($user_id, $sku);
            if($get_search_exist){
                $data = array('updated'=> date('Y-m-d H:i:s'));
                $insert_search = $this->memory_model->update_memory_search($user_id, $sku, $data);
            }else{
                $data = array(
                    'user_id' => $user_id,
                    'search' => $sku,
                    'updated' => date('Y-m-d H:i:s')
                );
                $this->memory_model->insert_memory_search($data);
            }
            

        }

        
        $get_memory['memory'] = $this->memory_model->get_memory_search($sku, $sortOrder, $orderBy);
        $this->load->view('modules/memory/ajax/memory_table', $get_memory);
        

    }
    public function _request_update_columns() {
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $barcode = $data['barcode'];
        $datatoupdate = $data['data'];
        $type = $data['type'];
        $col = array("$type"=> $datatoupdate);
        $update_col = $this->memory_model->update_memorycol($col, $barcode, $sku);
        
    }

    private function detect_listing_action() {
        $referred = $this->input->get('referred');
        $action = $this->input->get('createtask');
        $sku = $this->input->get('new_listing');
        $price = $this->input->get('price');

        if (isset($referred) && $referred == '1') {
            return array(
                'action' => $action,
                'sku' => $sku,
                'price' => $price
            );

        }

        return null;
    }

    public function export_csv() {
        $search = $this->input->post('itemsearch');
        $historysearch = $this->input->post('historysearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        
        try {
            if($search == ""){
                $csv = $this->memory_model->generate_csv(
                    trim($historysearch),
                    $sort_order,
                    $order_by
                    
                   
                );
            }else{
                $csv = $this->memory_model->generate_csv(
                    trim($search),
                    $sort_order,
                    $order_by
                );
            }
            
        } catch (Exception $e) {
            $response = array(
                'message' => 'Something went wrong. Please contact the site admin.'
            );

            return $this->output->set_header(500)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($response));
        }

        return $this->output->set_header(200)
                            ->set_content_type('text/csv', 'utf-8')
                            ->set_output($csv);
    }

    public function load_listing() {
        $user = $this->session->userdata('user');

        $search = $this->input->post('itemsearch');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        $request_channels = $this->input->post('channels');
        $request_actions = $this->input->post('actions');
        $request_users = $this->input->post('users');
        $banner = $this->input->post('banner');
        $search_column = $this->input->post('search_column');
        $is_done_shown = $this->input->post('is_done_shown') === 'true';
        $is_only_me_enabled = $this->input->post('is_only_me_enabled') === 'true';

        $channels = isset($request_channels) && $request_channels !== '' ? explode(',', $request_channels) : null;
        $actions = isset($request_actions) && $request_actions !== '' ? explode(',', $request_actions) : null;

        if ($is_only_me_enabled) {
            $users = array(trim($user['firstname']) . ' ' . trim($user['lastname']));
        } else {
            $users = isset($request_users) && $request_users !== '' ? explode(',', $request_users) : null;
        }

        $data['listing_added'] = $this->task_model->load(
            trim($search),
            $order_by,
            $sort_order,
            $channels,
            $actions,
            $banner,
            $users,
            $search_column,
            $is_done_shown
        );

        $columns = $this->users_model->get_table_columns('task', $user['id']);
        $data['columns'] = json_decode($columns->task_columns);

        $this->load->view("modules/task/ajax/listingtable", $data);
    }

    public function _request_delete_memory(){
        $data = $this->input->post('data');
        $sku = $data['sku'];
        $barcode = $data['barcode'];
        $orderby = $data['orderby'];
        $sortorder = $data['sortorder'];
        $textsku = $data['skutext'];
        
        $deleteone = $this->memory_model->delete_memorycol($sku, $barcode);
        
            $get_memory['memory']= $this->memory_model->get_memory_search($textsku, $sortorder, $orderby);
            $this->load->view('modules/memory/ajax/memory_table', $get_memory);
        


        
    }


    public function _request_insert_mem(){
        $sku = $this->input->post('sku');
        $barcode = $this->input->post('barcode');
        $brand = $this->input->post('brand');
        $con = $this->input->post('con');
        $memory_size = $this->input->post('memory_size');
        $memory_family = $this->input->post('memory_family');
        $memory_type = $this->input->post('memory_type');
        $speed = $this->input->post('speed');
        $registry = $this->input->post('registry');
        $cycles = $this->input->post('cycles');
        $rank = $this->input->post('rank');
        $voltage = $this->input->post('voltage');
        $notes = $this->input->post('notes');

        $this->load->helper(array('form', 'url'));

         $this->load->library('form_validation');

         
        $this->form_validation->set_rules('sku', 'Sku', 'trim|required');
        $this->form_validation->set_rules('barcode', 'Barcode', 'trim|required');
        $this->form_validation->set_rules('brand', 'Brand', 'trim|required');
        $this->form_validation->set_rules('con', 'Condition', 'trim|required');
        $this->form_validation->set_rules('memory_size', 'Memory Size', 'trim|required');
        $this->form_validation->set_rules('memory_family', 'Memory Family', 'trim|required');
        $this->form_validation->set_rules('memory_type', 'Memory Type', 'trim|required');
        $this->form_validation->set_rules('speed', 'Speed', 'trim|required');
        $this->form_validation->set_rules('registry', 'Registry', 'trim|required');
        $this->form_validation->set_rules('cycles', 'Cycles', 'trim|required');
        $this->form_validation->set_rules('rank', 'Rank', 'trim|required');
        $this->form_validation->set_rules('voltage', 'Voltage', 'trim|required');
        $this->form_validation->set_rules('notes', 'Notes', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){
            /*$validation_message = $this->form_validation->error_array();
            foreach($validation_message as $valm){
                $emptyfield = $valm;
            }*/
            $message = validation_errors();
            $alert = "warning";
            $data = array(
                'status' => 200,
                'message' => $message,
                'isSuccess' => trim($alert)
            );
    
            return $this->output->set_header(200)
                                ->set_content_type('application/json', 'utf-8')
                                ->set_output(json_encode($data));
        }else{
            $data = array(
                'sku' =>  $sku,
                'barcode' => $barcode,
                'brand' => $brand,
                'con' => $con,
                'memory_size' => $memory_size,
                'memory_family' => $memory_family,
                'memory_type' => $memory_type,
                'speed' => $speed,
                'registry' => $registry,
                'cycles' => $cycles,
                'rank' => $rank,
                'voltage' => $voltage,
                'notes' => $notes
            );
            $check_memory = $this->memory_model->check_memory($sku, $barcode);
            if($check_memory == "SKU is Already Exist"){
                $message = "SKU is Already Exist";
                $alert = "warning";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }else if($check_memory == "BarCode is Already Exist"){
                $message = "BarCode is Already Exist";
                $alert = "warning";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }else if($check_memory == "Not Found"){
                $this->memory_model->insert_new_mem($data);
                $message = "Memory Inserted Successfully";
                $alert = "success";
                $data = array(
                    'status' => 200,
                    'message' => $message,
                    'isSuccess' => trim($alert)
                );
        
                return $this->output->set_header(200)
                                    ->set_content_type('application/json', 'utf-8')
                                    ->set_output(json_encode($data));
            }
        }
        
        ///$this->cpu_model->insert_new_cpu($data);                
    }

    

    

    

    
}
