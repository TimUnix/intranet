<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->helper(array( 'url', 'research_link_generator' ));
		$this->load->model(array( 'users_model', 'search_model' ));
	}
    
	public function index(){

		$this->load->library('session');
		
		if($this->session->has_userdata('user')){
            redirect('home');
		}
		else{
			$this->load->view('login_page');
		}
	}

	function request(){
		if($this->uri->rsegment(3) == "load_items"){
		    self::_request_load_items();
		}
		if($this->uri->rsegment(3) == "load_category"){
		    self::_request_load_category();
		}
		if($this->uri->rsegment(3) == "get_specific_cat_and_con"){
		    self::_request_get_specific_cat_and_con();
		}
		if($this->uri->rsegment(3) == "list_category_and_condition"){
		    self::_request_list_category_and_condition();
		}
        if($this->uri->rsegment(3) == "load_summary") {
            self::_request_load_summary();
        }
        if($this->uri->rsegment(3) == "brand_items_and_price") {
            self::total_items_by_items_and_price();
        }
        if ($this->uri->rsegment(3) == "category_and_condition_count") {
            self::_request_category_and_condition_count();
        }
        if ($this->uri->rsegment(3) == "year_count") {
            self::_request_year_count();
        }
         if ($this->uri->rsegment(3) == "pie_area_count") {
            self::_request_pie_area_count();
        }
        
    }

    function _request_load_items() {
        $result = $this->search_model->get_items();

        $items = array();

        foreach ($result as $item) {
            $items[] = self::render_item_row($item);
        }

        header('Content-Type: application/json');
        echo json_encode(array( 'data' => $items ));
    }

    function _request_load_summary() {
        $summary = $this->search_model->get_count_and_total();

        $data = array(
            'amount' => $summary[0]->amount,
            'price' => $summary[0]->price
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function _request_load_category(){
	    $injected['categories'] = $this->search_model->categories();
	    $injected['conditions'] = $this->search_model->conditions();
	    $this->load->view('navigation/search/ajax/load_category_and_condition', $injected);
    }

    public function _request_get_specific_cat_and_con(){
	    $cat = $_POST['cat'];
	    $con = $_POST['con'];
	    $input_cat = $this->input->post('cat');
        $input_con = $this->input->post('con');
        
        $get_specific_cat_and_con = $this->search_model->getSearch(array('env.cat ='=>$input_cat, 'env.con ='=>$input_con),"",array("env.con"=>"ASC"),true);
	    var_dump($get_specific_cat_and_con);
    }

    function _request_list_category_and_condition(){
        if (isset($_POST['cat_con'])) {
		    $a = $_POST['cat_con'];
		    /* $a = (explode(",",$str)); */
		    
		    $count = 0;
		    $arg = array();

            $total_price = 0;
            

		    foreach($a as $aaa){
			    $data = $aaa;
			    /* echo $data; */
			    $gets = $this->search_model->cat($data);

			    foreach($gets as $item){
				    if($item->cat == $aaa){
					    /* $arg[] = array(
					       'par' => $getsx->par, 
					       'bar' => $getsx->bar,
					       'man' => $getsx->man, 
					       'con' => $getsx->con,
					       'pri' => $getsx->pri, 
					       'qty' => $getsx->qty,
					       'des' => $getsx->cat, 
					       'cat' => $getsx->cat,
					       'bin' => $getsx->bin,
					       'arc' => $getsx->arc,
					       'loc' => $getsx->loc
					       ); */
                        $arg[] = self::render_item_row($item);
		            }
	            }
                $total_price = array_reduce(
                    $gets,
                    array($this, '_calculate_price_sum'),
                    $total_price
                );
	            $get = $this->search_model->con($data);
	            foreach($get as $item){
		            if($item->con == $data){
			            /* $arg[] = array(
			               'par' => $getx->par, 
			               'bar' => $getx->bar,
			               'man' => $getx->man, 
			               'con' => $getx->con,
			               'pri' => $getx->pri, 
			               'qty' => $getx->qty,
			               'des' => $getx->cat, 
			               'cat' => $getx->cat,
			               'bin' => $getx->bin,
			               'arc' => $getx->arc,
			               'loc' => $getx->loc
			               ); */
		                $arg[] = self::render_item_row($item);
                    }
	            }
                $total_price = array_reduce(
                    $get,
                    array($this, '_calculate_price_sum'),
                    $total_price
                );
            }

            //echo $count($arg);

            //$this->load->view('navigation/search/ajax/sample', $get);

            /*foreach($arg as $r){
               echo $r['cat'] . "</br>";
               
               /* $cat_cons['args'] = $arg; */

            $amount = sizeof($arg);

            header('Content-Type: application/json');
            /* var_dump($arg); */
            echo json_encode(array(
                'data' => $arg,
                'amount' => $amount,
                'total_price' => $total_price
            ));
        }
        /* $this->load->view('navigation/search/ajax/cat_con', $cat_cons); */
    }

    // Export data in CSV format 
    public function exportcsv(){ 
        $usersData = $this->search_model->get_items();
        $price_add = $this->search_model->price_add();
        $add_price = 0;
        foreach($usersData as $price){
            $pri = $price->pri;
            $qty = $price->qty;
            $add = $pri * $qty;
            $add_price = $add_price + $add;
        }
        $get_add_price = $add_price;
        $i = 0;
        foreach($price_add as $p){$i = $p->pri; }
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Items".date('Ymd').".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        $tot_items = count($usersData);
        /*$sum = 0;
           foreach($usersData as $key) {
           $sum = $sum + $key->pri; 
           }*/
        
        fputcsv($handle, array("", "", "UnixSurplus"));
        fputcsv($handle, array("Date Exported", date('Y-m-d')));
        fputcsv($handle, array("Items Quantity", floor($tot_items)));
        fputcsv($handle, array("Items Total Price", number_format($get_add_price, 2)));
        fputcsv($handle, array("No","Part Number","OEM PN", "Description", "Con", "Category", "Price", "Qty", "Total", "Bin"));
        $cnt=0;
        foreach ($usersData as $key) {
            $inc = 1;
            $narray=array($cnt = $cnt + $inc,$key->par,$key->bar,$key->des,$key->con,$key->cat,$key->pri,$key->qty,number_format($key->pri * $key->qty, 2),$key->bin);
            fputcsv($handle, $narray);
            $cnt = $cnt;
            

        }
        fclose($handle);
        exit;
    }

    private function _calculate_price_sum($accumulator, $item) {
        $accumulator += floor($item->pri * $item->qty);
        return $accumulator;
    }

    private function render_item_row($item) {
        $copy_btn = '<button type="button" class="btn btn-link copy-click"><i class="fa fa-copy"></i></button>';

        $bar = (!isset($item->bar) || $item->bar === '') ?
               '' : $item->bar;

        $con = (!isset($item->con) || $item->con === '') ?
               '' : $item->con;

        $cat = (!isset($item->cat) || $item->cat === '') ?
               '' : $item->cat;
        
        $price = round($item->pri);
        $total = round($item->pri * $item->qty);
        
        $bin = (!isset($item->bin) || $item->bin === '') ?
               '' : $item->bin;

        $part_number_research = (!isset($item->par) || $item->par === '') ?
                                '' : generate_research_link($item->par);

        $bar_research = (!isset($item->bar) || $item->bar === '') ?
                        '' : generate_research_link($item->bar);

        return array(
            $copy_btn,
            $item->par,
            $part_number_research,
            $bar,
            $bar_research,
            $item->des,
            $con,
            $cat,
            "$$price",
            $item->qty,
            "\$$total",
            $bin
        );
    }

    private function generate_research_link($query) {
        $url = base_url() . "index.php/research?search={$query}";
        return "<a href=\"{$url}\">R</a>";
    }
    public function total_items(){
        $usersData = $this->search_model->get_items();
        $price_add = $this->search_model->price_add();
        $add_price = 0;
        foreach($usersData as $price){
            $pri = $price->pri;
            $qty = $price->qty;
            $add = $pri * $qty;
            $add_price = $add_price + $add;
        }
        $get_add_price = $add_price;
        $i = 0;
        foreach($price_add as $p){$i = $p->pri; }
        
        $tot_items = count($usersData);
        $tot_items_data = $tot_items;
        $number_format_data = number_format($get_add_price, 2);
        /*$sum = 0;
           foreach($usersData as $key) {
           $sum = $sum + $key->pri; 
           }*/
        echo "<pre>";
        var_dump($usersData);
        echo "</pre>";
        
    }
    public function total_items_by_items_and_price(){
        $total_items['total_items'] = $this->search_model->total_items_by_items();
        $total_price = $this->search_model->total_items_by_price();
        $data_price = array();
        foreach($total_price as $total_price_data){
            $get_price_data = $total_price_data->pri;
            $get_total_item = $total_price_data->countman;
            $brand_name = $total_price_data->man;

            $get_the_price = $get_price_data * $get_total_item;
            $datas['get_price_data'] = $get_price_data;
            $datas['get_total_item'] = $get_total_item;
            $datas['brand_name'] = $brand_name;
            $datas['get_the_price'] = $get_the_price;
            $data_price[] = $datas;
        }
        $get_data_price['get_data_price'] =  $data_price;///for price
        
        $data_total_items['data_total_items'] = $total_items;///for items

        $data = array(
            'price' => $data_price,
            'items' => $total_items
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    private function _request_category_and_condition_count() {
        $categories = $this->search_model->get_category_count();
        $conditions = $this->search_model->get_condition_count();
        $qty_zero = $this->search_model->get_qty_zero_count();

        $conditions[] = array(
            'con' => 'Qty Zero',
            'con_count' => $qty_zero->count
        );

        $data = array(
            'categories' => $categories,
            'conditions' => $conditions
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    private function _request_year_count() {
        $years = $this->search_model->get_year_count();

        $data = array(
            'years' => $years
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }
     private function _request_pie_area_count() {
        $pieInv = $this->search_model->get_pieInv_count();
        $inv_count = $this->search_model->inv_count();
       $uni_count = $this->search_model->uni_count();
       $itr_count = $this->search_model->itr_count();
        $data= array(
           'pieInventories' => $pieInv,
           'inv_count' => $inv_count,
            'uni_count' => $uni_count,
            'itr_count' => $itr_count,
        );
       /*$testarray = array(
            'testuni' => $test
        );*/

        header('Content-Type: application/json');
        echo json_encode($data);
       /* echo json_encode($testarray);
        echo json_encode(array(
            'pieInventories' => $pieInv,
           'yearInventories' => $yearInventories,
           'test' => $test
        ));*/
    }
    
}
