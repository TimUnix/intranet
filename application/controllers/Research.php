<?php

class Research extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access', 'median_calculator'));
        $this->load->model(array(
            'users_model',
            'navigation_model',
            'google_model',
            'reports_model',
            'test_model',
            'research_model',
            'search_history_model',
            'configuration_model',
            'inventory_model'
        ));
        error_reporting(E_ERROR | E_PARSE);
    }

    public function index(){
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if($this->session->has_userdata('user')){
            $data_sess = $this->session->userdata('user');
            $get_user_id = $this->google_model->getFields($data_sess['email']);
            $user_id = $get_user_id->id;

            $is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

            if($is_google == true){
                $g_id = $user_id;
            }else{
                $g_id = $data_sess['id'];
            }

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'research');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $search_query = $this->input->post('search');
            $search_history = $this->search_history_model->get_search_history_for_user($user_id);

            $userdata['data'] = array(
                'user_picture'  =>  $picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
                'page_name' => $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'is_google' => $is_google,
                'search_history' => $search_history,
                'search_query' => $search_query
			);
			
			$this->load->view("modules/research/research_main_page", $userdata);
		}
		else{
            $data['data'] = array(
                'page_name' => 'Login'
            );

			$this->load->view('login', $data);
		}
		
        
	}

    function request(){
        if ($this->uri->rsegment(3) == "local_items") {
            self::_request_local_items();
        }

        if ($this->uri->rsegment(3) == "search_ebay") {
            self::_request_search_ebay();
        }
    }

    /**
     *  Search eBay items stored locally.
     *  If no items are found, search through the api
     **/
    public function _request_search_ebay() {
        $search_item = $this->input->post('ebayitem');
        $order_by = $this->input->post('order_by');
        $sort_order = $this->input->post('sort_order');
        self::update_search_history($search_item);
        $sort = $this->uri->rsegment(4);
        $order = $this->uri->rsegment(5);
        if($sort == "DESC" && $order == "seller"){
            $type_list = "seller";
            $column = SORT_DESC;
        }else if($sort == "ASC"  && $order == "seller"){
            $type_list = "seller";
            $column = SORT_ASC;
        }else if($sort == "DESC" && $order == "title"){
            $type_list = "title";
            $column = SORT_DESC;
        }else if($sort == "ASC"  && $order == "title"){
            $type_list = "title";
            $column = SORT_ASC;
        }else if($sort == "DESC" && $order == "price"){
            $type_list = "price";
            $column = SORT_DESC;
        }else if($sort == "ASC"  && $order == "price"){
            $type_list = "price";
            $column = SORT_ASC;
        }

        $items_found = array();

        $prices = array();
        $low = 0;
        $high = 0;
        $total = 0;
        $average = 0;
        $median = 0;

        $items = array(); 
        $items = array_merge($items, self::search_ebay_api($search_item));

        $prices = array_map(function($ebay_item) {
            return $ebay_item->price;
        }, $items);

        $low = min($prices);
        $high = max($prices);

        $total = array_reduce($prices, function($accumulator, $price) {
            return $accumulator + $price;
        }, 0);

        $average = $total / count($items);
        $median = getMedian($prices);

        $items_found = $items;


        $keys = array_column($items_found, $type_list);
        array_multisort($keys, $column, $items_found);


        $items_found_count = count($items_found);

        if ($items_found > 0) {
            $data = array(
                'ebayitem' => $search_item,
                'items' => $items_found_count,
                'min' => $low,
                'max' => $high,
                'average' => $average,
                'median' => $median,
                'ebay' => $items_found
            );

            $this->load->view('modules/research/ajax/ebay_data', $data);
        } else {
            $this->load->view('modules/research/ajax/result_zero');
        }

    }

    private function search_ebay_api($item){
        $ebay_base_url = "https://svcs.ebay.com/services/search/FindingService/v1?";
        $ebay_operation_name = "findItemsAdvanced";
        $ebay_service_version = "1.13.0";
        $ebay_app_id = "UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2";
        $ebay_global_id = "EBAY-US";
        $pageInput= 20;

        $api_url = $ebay_base_url
            . "OPERATION-NAME=$ebay_operation_name"
            . "&SECURITY-APPNAME=$ebay_app_id"
            . "&SERVICE-VERSION=$ebay_service_version"
            . "&GLOBAL-ID=$ebay_global_id"
            . "&descriptionSearch=True"
            . "&keywords=$item"
            ."&outputSelector(0)=SellerInfo"
            . "&paginationInput.entriesPerPage=$pageInput"
            . "&sortOrder=FixedPrice";

        $response = new SimpleXMLElement($api_url, 0, true);
        

        $items = array();
        $max_results = 20;

        if ($response->ack == "Success") {
            foreach($response->searchResult->item as $item){
                $viewitemurl = $item->viewItemURL;
                $itemnumber = $item->itemId;
                $galleryurl = $item->galleryURL;
                $currentprice = (int) $item->sellingStatus->currentPrice;
                $sellerusername = "{$item->sellerInfo->sellerUserName}";
                $title = "{$item->title}";
                $categoryId = $item->primaryCategory->categoryId;
                $categoryName = $item->primaryCategory->categoryName;
                $shipping_type = $item->shippingInfo->shippingType;
                $ship_to_locations = $item->shippingInfo->shipToLocations;
                $expedited_shipping = $item->shippingInfo->expeditedShipping == 'true' ? 1 : 0 ;
                $one_day_shipping_available = $item->shippingInfo->oneDayShippingAvailable == 'true' ? 1 : 0;
                $handling_time = $item->shippingInfo->handlingTime;
                $top_rated_listing = $item->topRatedListing == 'true' ? 1 : 0;
                $selling_state = $item->sellingStatus->sellingState;
                $condition_id = $item->condition->conditionId;
                $condition_display_name = $item->condition->conditionDisplayName;
   
                 $data = array(
                        'item_number' => $item->itemId,
                        'price' => $currentprice,
                        'title' => "$title",
                        'link' => $viewitemurl,
                        'img' => $galleryurl,
                        'seller' => "$sellerusername",
                        'category_id' => $categoryId,
                        'category_name' => $categoryName,
                        'shipping_type' => $shipping_type,
                        'ship_to_locations' => $ship_to_locations,
                        'expedited_shipping' => $expedited_shipping,
                        'one_day_shipping_available' => $one_day_shipping_available,
                        'top_rated_listing' => $top_rated_listing,
                        'handling_time' => $handling_time,
                        'selling_state' => $selling_state,
                        'condition_id' => $condition_id,
                        'condition_display_name' => $condition_display_name
                    );

                    $items[] = (object) $data;                 
                
            }
        }
        

        return $items;
        
    }

    private function update_search_history($ebayitem) {
        $data_sess = $this->session->userdata('user');
        $get_user_id = $this->users_model->get_user_id($data_sess['email']);

        $info_user_id = "";

        foreach($get_user_id as $user_id){
            $info_user_id = $user_id->id;
        }
        if($info_user_id != 0){
            $check_users_history = $this->search_history_model->check_users_history($info_user_id, $ebayitem);
            if($check_users_history){
                $data = array(
                    'updated' => date('Y-m-d H-i-s')
                );
                $this->search_history_model
                ->update_search_history($data, 'user_id', $info_user_id, 'search', $ebayitem);
            }else{
                $get_all_rows_num = $this->search_history_model->get_all_rows_num();
                $sh_id = "";
                foreach($get_all_rows_num as $_get_all_rows_num){
                    $sh_id = $_get_all_rows_num->sh_id;
                }
                $data = array(
                    'sh_id' => $sh_id + 1,
                    'user_id' => $info_user_id,
                    'search' => $ebayitem,
                    'updated' => date('Y-m-d H-i-s')
                );
                $this->search_history_model->insert_table($data);
            }
        }
    }

    private function _request_local_items() {
        $part_number = $this->input->post('part_number');

        $local_items = $this->inventory_model->get_local_items($part_number);
        if($items = array('local_items' => $local_items)){
           $this->load->view('modules/research/ajax/local_items', $items);
            
        }
        else {
            $this->load->view('modules/research/ajax/result_zero');
        }
        

    }

}

?>
