<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price_update extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'nav_order', 'user_access'));
        $this->load->model(array(
            'inventory_model', 'navigation_model', 'google_model', 'update_price_model'
        ));
    }

    public function index() {
		$this->load->library('session');
		
		if ($this->session->has_userdata('user')) {
			$userdata = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($userdata['email']);
			$user_id = $get_user_id->id;
			$is_google = $userdata['is_google'];
			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $userdata['id'];
			}

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'price_update');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');
            
			$result = $this->inventory_model->get_categories($g_id);

            $nav = $this->navigation_model->get_navigation_menu($g_id);
			$ordered_nav = organize_nav_order($nav);

			$categories = array();

			foreach ($result as $row) {
				if (!is_null($row->subcat)){

					if (!array_key_exists($row->category , $categories)) {
						$categories[$row->category] = array();
					}
					
					array_push($categories[$row->category], array(
						'display_name' => $row->subcat,
						'category_name' => $row->category_name
					));
				}
			}
			
			$picture = $userdata['picture'];

			if (is_null($userdata['picture']) || $userdata['picture'] == '') {
				$picture = base_url() . 'resources/img/noimage.png';
			} 
			
			$data['data'] = array(
				'categories' => $categories,
				'user_picture' => $picture,
				'user_email' => $userdata['email'],
				'user_firstname' => $userdata['firstname'],
				'user_lastname' => $userdata['lastname'],
				'nav_menu' => $ordered_nav,
				'page_name' => $user_access_result->menu_title,
				'is_google' => $is_google
			);
            
            $this->load->view('modules/price_update/price_update', $data);
        } else {
            $data['data'] = array(
                'page_name' => 'Login'
            );
            
            $this->load->view('login', $data);
        }
    }

    function request(){
        if ($this->uri->rsegment(3) === 'apply_filter') {
            self::_request_apply_filter();
        }

        if ($this->uri->rsegment(3) === 'save_new_price') {
            self::_request_save_new_price();
        }

        if ($this->uri->rsegment(3) === 'save_new_price_notes') {
            self::_request_save_notes();
        }
    }

    private function _request_save_new_price() {
        $this->load->library('session');

        $par = $this->input->post('par');
        $price = $this->input->post('price');

        $userdata = $this->session->userdata('user');
        $is_google = $userdata['is_google'];

		if($is_google){
            $user = $this->google_model->getFields($userdata['email']);
			$user_id = $user->id;
		} else {
			$user_id = $userdata['id'];
		}

        if ((isset($par) && $par !== '') && (isset($price) && $price !== '' && $price !== 0 && is_numeric($price))) {
            $is_par_exists = $this->update_price_model->check_if_par_exists($par);

            if ($is_par_exists) {
                $this->update_price_model->update_price($par, $price, $user_id);
            } else {
                $this->update_price_model->add_updated_price($par, $price, $user_id);
            }
        }
    }

    private function _request_save_notes() {
        $this->load->library('session');

        $par = $this->input->post('par');
        $notes = $this->input->post('notes');

        $userdata = $this->session->userdata('user');
        $is_google = $userdata['is_google'];

		if($is_google){
            $user = $this->google_model->getFields($userdata['email']);
			$user_id = $user->id;
		} else {
			$user_id = $userdata['id'];
		}

        if ((isset($par) && $par !== '')) {
            $is_par_exists = $this->update_price_model->check_if_par_exists($par);

            if ($is_par_exists) {
                $this->update_price_model->update_notes($par, $notes, $user_id);
            } else {
                $this->update_price_model->add_updated_notes($par, $notes, $user_id);
            }
        }
    }

    private function _request_apply_filter() {
        $filter = $this->input->post('filter');

        if (isset($filter) && $filter !== '') {
            $filtered_items = $this->inventory_model->get_updated_prices($filter);

            $data = array('items' => $filtered_items);

            $this->load->view('modules/price_update/ajax/filtered_items', $data);
        }
	}

    function get_inventory_list(){
        $filter = $this->input->post('segment');
        $data['inv_list'] = $this->inventory_model->get_categories($filters);

        $this->load->view('modules/price_update',$data);
    }
}

?>
