<?php

class Reports extends CI_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url', 'nav_order', 'user_access'));
		$this->load->model(array(
            'users_model', 'navigation_model', 'google_model', 'reports_model'
        ));
	}
	
	public function index(){
		//load session library
		$this->load->library('session');

		//restrict users to go to home if not logged in
		if($this->session->has_userdata('user')){
			$data_sess = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($data_sess['email']);
			$user_id = $get_user_id->id;
			
			$is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $data_sess['id'];
			}
            
            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'reports');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('access_denied');

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            // Minimum year in database is 18
            $begin_year = 2018;

            $dates = range((int) date('Y'), $begin_year);

			$userdata['data'] = array(
				'user_picture'	=>	$picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
				'page_name'	=> $user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
                'dates' => $dates,
				'is_google' => $is_google
			);
			
			$this->load->view("modules/reports/items_reports", $userdata);
		}
		else{
            $data['data'] = array(
                'page_name' => 'Login'
            );
            
			$this->load->view('login', $data);
		}
        
	}


	function request(){
		if($this->uri->rsegment(3) == "load_items"){
			self::_request_load_items();
		}

        if ($this->uri->rsegment(3) == "filtered") {
            self::_request_load_items_filtered();
        }

        if ($this->uri->rsegment(3) == "load_categories") {
            self::_request_load_categories();
        }
	}
	public function _request_load_items(){
		$load_items['load_items'] = $this->reports_model->load_items();
		$this->load->view('modules/reports/ajax/load_products_reports.php', $load_items);


	}

    private function _request_load_items_filtered() {
        $from = (int) $this->input->post('from');
        $to = (int) $this->input->post('to');
        $categories = $this->input->post('categories');

        if ($from <= $to) {
            $from = $from - 2000;
            $to = $to - 2000;

            $items['load_items'] = $this->reports_model->load_items_by_date_range($from, $to, $categories);
            $this->load->view('modules/reports/ajax/load_products_reports.php', $items);
        }
    }

    private function _request_load_categories() {
        $categories = $this->reports_model->load_categories();

        $data = array ( 'categories' => $categories );

        $this->load->view('modules/reports/ajax/category_filters.php', $data);
    }
}

?>
