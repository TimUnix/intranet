<?php

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url', 'nav_order', 'user_access'));
		$this->load->model(array('users_model', 'navigation_model', 'google_model'));
	}

	public function index(){
		//load session library
		$this->load->library('session');

		//restrict users to go to home if not logged in
		if($this->session->has_userdata('user')){
			$data_sess = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($data_sess['email']);
			$user_id = $get_user_id->id;

			$is_google = isset($data_sess['is_google']) ? $data_sess['is_google']:"";

			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $data_sess['id'];
			}

            $user_access_result = $this->navigation_model->get_user_menu_access($g_id, 'home');
            $is_user_authorized = check_user_access_permissions($user_access_result->menu_access);

            if (!$is_user_authorized) return redirect('time_record');

            $picture = $data_sess['picture'];

            if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
                $picture = base_url() . 'resources/img/noimage.png';
            }

            $nav = $this->navigation_model->get_navigation_menu($g_id);
            $ordered_nav = organize_nav_order($nav);

			$userdata['data'] = array(
				'user_picture'	=>	$picture,
                'user_firstname' => $data_sess['firstname'],
                'user_lastname' => $data_sess['lastname'],
                'user_email' => $data_sess['email'],
				'page_name'	=>	$user_access_result->menu_title,
                'nav_menu' => $ordered_nav,
				'is_google' => $is_google
			);

			$this->load->view('home', $userdata);
		}
		else{
            $data['data'] = array(
                'page_name' => 'Login'
            );
			$this->load->view('login', $data);
		}

	}
	public function googlelog(){
        //load session library
        $this->load->library('session');

        //restrict users to go to home if not logged in
        if($this->session->userdata('user')){
			$data_sess = $this->session->userdata('user');
			$get_user_id = $this->google_model->getFields($data_sess['email']);
			$user_id = $get_user_id->id;
			$is_google = $data_sess['is_google'];
			if($is_google == true){
				$g_id = $user_id;
			}else{
				$g_id = $data_sess['id'];
			}
            $nav = $this->navigation_model->get_navigation_menu($g_id);


			///
			if($get_user_id)
			{
			    //update data
			    $user_data = array(
			        'updated_at' => date('Y-m-d H:i:s')
			    );

			    $this->google_model->update_table($user_data, 'email', $data_sess['email']);
			}
			else
			{
			    //insert data
			    $user_data = array(
				    'email'  => $data_sess['email'],
				    'firstname' => $data_sess['firstname'],
				    'lastname'  => $data_sess['lastname'],
				    'picture'=> $data_sess['picture'],
				    'access_token'=> $data_sess['access_token'],
				    'google_id'=>$data_sess['google_id'],
				    'last_url'=>"NULL",
				    'created_at'=> date('Y-m-d H:i:s'),
				    'updated_at'=> date('Y-m-d H:i:s')

			    );

			    $this->google_model->insert_table($user_data);
			}

	        $ordered_nav = organize_nav_order($nav);

	        $picture = $data_sess['picture'];

	        if (is_null($data_sess['picture']) || $data_sess['picture'] == '') {
		        $picture = base_url() . 'resources/img/noimage.png';
	        }

	        $userdata['data'] = array(
		        'user_picture'	=>	$picture,
		        'user_firstname' => $data_sess['firstname'],
		        'user_lastname' => $data_sess['lastname'],
		        'user_email' => $data_sess['email'],
		        'page_name'	=>	'Home',
		        'nav_menu' => $ordered_nav,
		        'is_google' => $is_google
	        );

	        $this->load->view('home', $userdata);
        }
        else{
            $data['data'] = array(
                'page_name' => 'Login'
            );

	        $this->load->view('login', $data);
        }
	}
}

?>
