<div class="modal fade" id="filterModal" tabindex="-1"
     role="dialog" aria-labelledby="filterModalLabel">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Categories and Conditions</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- /.card-header -->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="cat">Categories</label>
                    <select multiple="multiple" id="cat" name="cat[]"
                            class="filters form-control"
                            data-allow-clear="true" data-width="100%"
                            data-placeholder="Select Categories">
                        <?php foreach($categories as $filter): ?>
                            <option value="<?php echo $filter->cat; ?>">
                                <?php echo $filter->cat; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="con">Conditions</label>
                    <select multiple="multiple" id="con" name="con[]"
                            class="filters form-control"
                            data-allow-clear="true" data-width="100%"
                            data-placeholder="Select Conditions">
                        <?php foreach($conditions as $filter): ?>
                            <option value="<?php echo $filter->con; ?>">
                                <?php echo $filter->con; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
                <!--<div class="form-group">
                     <label>Minimal</label>
                     <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" >
                     <option selected="selected" data-select2-id="3"></option>
                     
                     </select><span class="select2 select2-container select2-container--default select2-container--below select2-container--open" dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                     </div>-->
                <!-- /.form-group -->
                
                <!-- /.form-group -->
                <!-- /.col -->
                <!-- /.form-group -->
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-secondary" id="clear"
                        data-dismiss="modal">
                    Clear
                </button>
                <button type="submit" class="btn btn-primary" id="save"
                    data-dismiss="modal">
                    Apply
                </button>
            </div>
        </div>
        <!-- /.col -->
    </div>
</div>
    <!-- /.row -->
    
    <!-- </div> -->
<!-- /.modal-body -->

<script>
 $(document).ready(function(){
     $("#save").click(function(){
         let categories = $("#cat").select2('data');
         let conditions = $("#con").select2('data');
         let data = categories.concat(conditions);

         let filters = data.map(function(filter) {
             if (filter.element.value === 'Blank') {
                 return '';
             } else {
                 return filter.element.value;
             }
         });

         if (filters.length > 0) {
             var datas = {
                 cat_con: filters,
                 ajax : '1'
             };
             $.ajax({
                 url : "<?php echo base_url(); ?>index.php/search/request/list_category_and_condition",
                 type : "post",
                 data : datas,
                 beforeSend : function(e) {
                     $('#product-table-body').html(`<tr class="odd"><td valign="top" colspan="11" class="datatables_empty">${spinner}</td></tr>`);
                     $('#total-items').html(spinnerLight);
                     $('#total-price').html(spinnerLight);
                 },
                 success : function(data){
                     productTable.clear().draw();
                     productTable.rows.add(data['data']).draw();

                     $('#total-items').html(data['amount']);
                     $('#total-price').html(`$${data['total_price']}`);
                 },
             });
         }
     });

     $('#clear').click(function() {
         $('.filters').val(null).trigger('change');

         $('#total-items').html(spinnerLight);
         $('#total-price').html(spinnerLight);

         $('#product-table-body').html(`<tr class="odd"><td valign="top" colspan="11" class="datatables_empty">${spinner}</td></tr>`);

         productTable.ajax.reload();

         loadSummary();
     });
 });
</script>

<script>
 var values = []; // declare it outside document.ready
 $( document ).ready(function() {
     $('#cat').click(function(i) {
         addFilter($(this).val()[0]);
     });

     $('#con').click(function(i) {
         addFilter($(this).val()[0]);
     });

     function addFilter(val) {
         if (!values.includes(val)) {
             values.push(val);

             updateFilters();
         }
     }

     function removeFilter(filter) {
         values = values.filter((value) => value != filter);

         updateFilters();
     }

     function updateFilters() {
         let filters = values.map((value) => `<button class="btn btn-outline-info btn-filter mr-2" type="button">${value}<i class="fa fa-times ml-2" aria-hidden="true"></i></button>`);

         $('#child').html(filters);
     }
 }); 
</script>
