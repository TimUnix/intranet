<label for="categoryFilters">Category</label>
<select class="form-control" id="categoryFilters" style="width: 100%;" multiple
        data-allow-clear="true" data-placeholder="Select Categories">
    <?php foreach ($categories as $category): ?>
        <option value="<?= $category->cat; ?>"><?= $category->cat; ?></option>
    <?php endforeach; ?>
</select>
<script>
 $(document).ready(function() {
     $("#categoryFilters").select2();
 });
</script>
