<!-- Default box -->
<div class="container-fluid">
        <div class="card">
        <div class="card-header">
            <h3 class="card-title">UnixSurplus Items</h3>
            <div class="card-tools">
                <button class="btn btn-sm btn-outline-primary" type="button" id="exportCSV">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <button class="btn btn-sm btn-outline-primary" type="button" id="filterButton"
                        data-toggle="modal" data-target="#filterModal">
                    <i class=" fa fa-filter" aria-hidden="true"></i>
                    Filter Date
                </button>
            </div>
		</div>
        <div class="card-body">
			<table id="product_table" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Items in Category</th>
						<th>Total Qty</th>
						<th>Total Value</th>
					</tr>
				</thead>
        <tbody>
		<?php  //echo $rate;echo "<pre>"; var_dump($rate);echo "</pre>";

foreach($load_items as $item){
   echo "<tr>";
    echo "<td>" . $item->cat ."</td>";
    echo "<td>" . $item->ite ."</td>";
    echo "<td>" . $item->qty ."</td>";
    echo "<td>" . $item->val ."</td>";
    
   echo "</tr>";
    
}


?>
        	</tbody>
   		 </table>
	</div>
</div>






<script>
 $(document).ready(function() {
     $('#product_table').DataTable();
 } );

 $("#exportCSV").click(function() {
     download_table_as_csv("product_table");
 });
</script>


