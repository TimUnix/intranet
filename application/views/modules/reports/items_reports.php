<?php $this->load->view('templates/header_new'); ?>

<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="fromDate">From</label>
                            <select class="form-control" id="fromDate">
                                <?php foreach ($data['dates'] as $date): ?>
                                    <option value="<?= $date; ?>"><?= $date; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                Start date cannot be higher than end date
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="toDate">To</label>
                            <select class="form-control" id="toDate">
                                <?php foreach ($data['dates'] as $date): ?>
                                    <option value="<?= $date; ?>"><?= $date; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                Start date cannot be higher than end date
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div id="categoryFiltersContainer" class="form-group"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnApplyFilters">
                    Apply
                </button>
            </div>
        </div>
    </div>
</div>

<div id="load_products" style="margin-bottom: 80px;">
<div class="container-fluid">
        <div class="card">
        <div class="card-header">
            <h3 class="card-title">UnixSurplus Items</h3>
            <div class="card-tools">
                <button class="btn btn-sm btn-outline-primary" type="button" id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <button class="btn btn-sm btn-outline-primary" type="button" id="dateRangeFilterButton"
                        data-toggle="modal" data-target="#dateRangeModal">
                    <i class=" fa fa-filter" aria-hidden="true"></i>
                    Filter Date
                </button>
            </div>
		</div>
        <div class="card-body">
			<table id="product_table" class="table table-striped table-bordered" style="width:100%">
			<div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="product_table_length"><label>Show <select name="product_table_length" aria-controls="product_table" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="product_table_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="product_table"></label></div></div></div>
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Items in Category</th>
						<th>Total Qty</th>
						<th>Total Value</th>
					</tr>
				</thead>
        <tbody id="list">
		
        	</tbody>
   		 </table>
	</div>
</div></div>
<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script src="<?php echo base_url(); ?>resources/js/exportCSV.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		$('#list').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
	    $("#load_products").load("<?php echo base_url();    ?>index.php/reports/request/load_items");

        $("#categoryFiltersContainer").load("<?php echo base_url(); ?>index.php/reports/request/load_categories");

        $("#btnApplyFilters").click(function() {
            const fromDate = $("#fromDate").find(":selected").val();
            const toDate = $("#toDate").find(":selected").val();

            if (fromDate <= toDate) {
                $("tbody").html(`<tr class="odd"><td valign="top" colspan="4" class="datatables_empty">${spinner}</td></tr>`);
                const categories = $("#categoryFilters").select2('data').map(function(category) {
                    return category.id;
                });

                $("#load_products").load("<?php echo base_url(); ?>index.php/reports/request/filtered",
                                         { 'from': fromDate, 'to': toDate, 'categories': categories });


                $("#filterModal").modal('hide');

                $("#fromDate").removeClass('is-invalid');
                $("#toDate").removeClass('is-invalid');
            } else {
                $("#fromDate").addClass('is-invalid');
                $("#toDate").addClass('is-invalid');
            }
        });
	});
</script>
