<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
				<form>
                <div class="row">
                
                <div class="col-md-6" >
                    <label for="sku">SKU</label>
                    <input  type="text" class="form-control" id="sku" list="skuSuggestions" required/>
                    <datalist id="skuSuggestions"></datalist>
                </div>
                <div class="col-md-6">
                    <label for="qty">Quantity</label>
                    <input type="text" class="form-control" id="qty">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="con">Condition</label>
                    <input type="text" class="form-control" id="con"/>
                </div>
                <div class="col-md-6">
                    <label for="oem">OEM</label>
					<input type="text" class="form-control" id="oem">
                </div>
                </div>
                <div class="row">
                 
                <div class="col-md-6">
                    <label for="switch">Switch Model</label>
                    <input type="text" class="form-control" id="switch">
                </div>
                <div class="col-md-6">
                    <label for="port">Port Qty/Speed/Type</label>
					<input type="text" class="form-control" id="port">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="License">License</label>
                    <input type="text" id="License" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="features">Features</label>
                    <input type="text" id="features" class="form-control">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="dual">Dual|Single</label>
					<input type="text" class="form-control" id="dual">
                </div>
                <div class="col-md-6">
                    <label for="airflow">Airflow</label>
                    <input type="text" id="airflow" class="form-control">
                </div>
                </div>
                <div class="row">
    
                <div class="col-md-6">
                    <label for="noedr">No ears/No Rails</label>
					<input type="text" class="form-control" id="noedr">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitf">Submit</button>
            </div>
				</form>
        </div>
    </div>
</div>

<div class="modal fade mark-listing-done-modal" id="markListingDoneModal" tabindex="-1"
     aria-labelledby="markListingDoneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="markListingDoneModalLabel">Mark <span class="sku-done"></span> Done </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-done">Confirm</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade mark-listing-undone-modal" id="markListingUndoneModal" tabindex="-1"
     aria-labelledby="markListingUndoneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="markListingUnoneModalLabel">Mark <span class="sku-done"></span> Undone </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-undone">Confirm</button>
            </div>
        </div>
    </div>
</div>