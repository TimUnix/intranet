<?php if(!empty($items)) { foreach($items as $row_index => $item): ?>
    <?php $index = $offset + $row_index; ?>

	<tr class = "myrow" style="background-color : #00000 ">
		<td data-id="<?= $item->switchid ?>" style="width:20px;" scope="col"><?= $index + 1 ?></td>
		<td style="max-width:200px; white-space: normal; word-wrap:break-word;" scope="col" class="sku item-sku<?= isset($columns->sku->is_shown) 
                        ? ($columns->sku->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->sku ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel skubtn" id="skubtn">
                    <i class="fa fa-edit fa-sm skupenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->sku ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="qty item-qty<?= isset($columns->qty->is_shown) 
                        ? ($columns->qty->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->qty ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel qtybtn">
                    <i class="fa fa-edit fa-sm qtypenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->qty ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="con item-con<?= isset($columns->con->is_shown) 
                        ? ($columns->con->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->con ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel conbtn">
                    <i class="fa fa-edit fa-sm conpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->con ?></td>

		<td style="max-width:100px; white-space: normal; word-wrap:break-word;" scope="col" class="oemman item-oemman<?= isset($columns->oemman->is_shown) 
                        ? ($columns->oemman->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->oemman ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel oemmanbtn">
                <i class="fa fa-edit fa-sm oemmanpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->oemman ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="switchmodel item-switchmodel<?= isset($columns->switchmodel->is_shown) 
                        ? ($columns->switchmodel->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->switchmodel ?>">

                <button type="button" class="btn btn-link text-primary draftid-cancel switchmodelbtn">
                    <i class="fa fa-edit fa-sm switchmodelpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->switchmodel ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="port item-rpm<?= isset($columns->port->is_shown) 
                        ? ($columns->port->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->port ?>">
           
                <button type="button" class="btn btn-link text-primary draftid-cancel portbtn">
                    <i class="fa fa-edit fa-sm portpenbtn" aria-hidden="true"></i>
                </button>
           
        <?= $item->port ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="license item-license<?= isset($columns->license->is_shown) 
                        ? ($columns->license->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->license ?>">
              <button type="button" class="btn btn-link text-primary draftid-cancel licensebtn">
                    <i class="fa fa-edit fa-sm licensepenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->license ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="features item-features<?= isset($columns->features->is_shown) 
                        ? ($columns->features->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->features ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel featuresbtn">
                    <i class="fa fa-edit fa-sm featurespenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->features ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="dualsingle item-dualsingle<?= isset($columns->dualsingle->is_shown) 
                        ? ($columns->dualsingle->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->dualsingle ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel dualsinglebtn">
                    <i class="fa fa-edit fa-sm dualsinglepenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->dualsingle ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="airflow item-airflow<?= isset($columns->airflow->is_shown) 
                        ? ($columns->airflow->is_shown ? '' : ' d-none') : '' ?>" nowrap nowrap data-value="<?= $item->airflow ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel airflowbtn">
                    <i class="fa fa-edit fa-sm airflowpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->airflow ?></td>

        <td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="noedr item-noedr<?= isset($columns->noedr->is_shown) 
                        ? ($columns->noedr->is_shown ? '' : ' d-none') : '' ?>" nowrap nowrap data-value="<?= $item->noedr ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel noedrbtn">
                    <i class="fa fa-edit fa-sm noedrpenbtn" aria-hidden="true"></i>
                </button>
                <?= $item->noedr ?></td></td>


	</tr>

<?php endforeach; }?>