<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td style = "display: none;" scope="col" class="id item-id<?= isset($columns->id->is_shown) 
                        ? ($columns->id->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->id ?>"><?= $item->id ?></td>
        <td class="employee" data-id="<?= $item->id ?>" data-column="emplopyee" data-value="<?= $item->firstname ?> <?= $item->lastname ?>"
            style="max-width: 80ch;">
            <div class="cell-content-container">
                <div class="cell-value"><?= $item->firstname ?> <?= $item->lastname ?></div>
            </div>
        </td>
        <td class="contact_number" data-value="<?= $item->contact_number ?>" style="max-width: 80ch;">
            <?php if ($item->is_active): ?>
                <div class="cell-edit-container d-none">
                    <textarea class="form-control cell-edit w-auto"><?= $item->contact_number ?></textarea>
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times cancel_number" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check save_number" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if ($item->is_active): ?>
                    <button class="btn btn-link btn-edit" style="display: inline-block;">
                        <i class="fa fa-edit edit_number" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
               <div class="cell-value" style="display: inline-block;"><?= $item->contact_number ?></div>
            </div>
        </td>

        <td class="birthday" data-value="<?= $item->birthday ?>" style="max-width: 80ch;">
            <?php if ($item->is_active): ?>
                <div class="cell-edit-container d-none">
                    <textarea class="form-control cell-edit w-auto"><?= $item->birthday ?></textarea>
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times cancel_birthday" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check save_birthday" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if ($item->is_active): ?>
                    <button class="btn btn-link btn-edit" style="display: inline-block;">
                        <i class="fa fa-edit edit_birthday" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value" style="display: inline-block;"><?= $item->birthday ?></div>
            </div>
        </td>
        <td class="address" data-value="<?= $item->address ?>" style="max-width: 80ch;">
            <?php if ($item->is_active): ?>
                <div class="cell-edit-container d-none">
                    <textarea class="form-control cell-edit w-auto height-160"><?= $item->address ?></textarea>
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times cancel_address" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check save_address" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if ($item->is_active): ?>
                    <button class="btn btn-link btn-edit" style="display: inline-block;">
                        <i class="fa fa-edit edit_address" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value" style="display: inline-block;"><?= $item->address ?></div>
            </div>
        </td>
    </tr>

<?php endforeach; } ?>

<!-- display data from db -->