<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<button id="insertListing" type="button" class="btn btn-info btn-lg ml-2 mb-4" data-toggle="modal"
        data-target="#myModal">
    Create New CPU
</button>
<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-left">
                <h3 class="modal-title w-100">New</h3>
            </div>
            <div class="modal-body">
                <div id="alert"></div>
                <div class="form-group">
                    <label for="request">Part Number</label>
                    <input type="text" class="form-control" id="partnumber">
                </div>

                <div class="form-group" novalidate>
                    <label for="sku">StepCode</label>
                    <input type="text" class="form-control" id="stepcode">
                </div>
                <div class="form-group">
                    <label for="channel">Speed</label>
                    <input type="number" class="form-control" id="speed">
                </div>
                <div class="form-group">
                    <label for="weight">Number Of Cores</label>
                    <input type="number" class="form-control" id="numberofcores">
                </div>
                <div class="form-group">
                    <label for="shipping">Code Name</label>
                    <input type="text" class="form-control" id="codename">
                </div>
                <div class="form-group">
                    <label for="moved">Thermal Design Power</label>
                    <input type="number" class="form-control" id="tdp">
                </div>
                <div class="form-group">
                    <label for="notes">CPU Cache</label>
                    <input type="text" class="form-control" id="cache">
                </div>
                <div class="form-group">
                    <label for="price">Socket Supported</label>
                    <input type="text" class="form-control" id="socketsupported">
                </div>
                <div class="form-group">
                    <label for="lot">Grade</label></br>
                    <input type="text" class="form-control" id="grade">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="regcpu">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filtersModal" tabindex="-1" aria-labelledby="filtersModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filtersModalLabel">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body filters-container"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="applyFilters" class="btn btn-primary" data-dismiss="modal">
                    Apply Filters
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pb-5">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="search_cpu">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                
                                <input class="form-control input-lg" type="search" id="partsearch"
                                       placeholder="Search Part" list="searchSuggestions">
                                <datalist id="searchSuggestions"></datalist>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                        <div class="form-group">
                            <label for="searchHistory">Search History:</label>
                            <select class="form-control" id="searchHistory"
                                    style="width: 100%;" 
                                    name="searchHistory[]">
                                <option value="" disabled selected>
                                    Part Number
                                </option>
                                <?php foreach ($search_history as $history): ?>
                                    <option value="<?= $history->search; ?>">
                                        <?= $history->search; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                <div class="form-group col-md-2">
                    <label for="orderBy">Order By</label>
                    <select type="button" class="form-control" id="orderByitem">
                        <option value="part" selected>part</option>
                        <option value="stepcode">stepcode</option>
                        <option value="speed">speed</option>
                        <option value="number_of_cores">number_of_cores</option>
                        <option value="codename">codename</option>
                        <option value="thermal_design_power">thermal_design_power</option>
                        <option value="cpu_cache">cpu_cache</option>
                        <option value="sockets_supported">sockets_supported</option>
                        <option value="grade">grade</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="sortOrder">Sort Order</label>
                    <select type="button" class="form-control" id="sortOrderitem">
                        <option value="ASC" selected>Ascending</option>
                        <option value="DESC">Descending</option>
                    </select>
                </div>
            </div>


            <!--<div class="row mt-4">
                <div class="col-md-1 text-center d-flex h-100">
                    <button type="button"
                            class="btn btn-primary btn-copy-textarea">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                        Copy
                    </button>
                </div>
                <div class="col-md-11">
                    <textarea class="textarea-copy form-control"
                              rows="4" name="copy-area">
                    </textarea>
                </div>
            </div>-->
        </div>
    </div>
    <div class="card">
        


        <div class="card-header">
                <h3 class="card-title">Cpu Module</h3>
                <div class="card-tools">
                <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-table" aria-hidden="true"></i>
                            Show/Hide Columns
                        </button>
                        <div class="dropdown-menu" id="columnDropdownSelector"
                             style="height: 200px; overflow-y: auto;">
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="part" <?= isset($data['columns']->part->is_shown)
                                                      ? ($data['columns']->part->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                part
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="stepcode" <?= isset($data['columns']->stepcode->is_shown)
                                                  ? ($data['columns']->stepcode->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                stepcode
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="speed" <?= isset($data['columns']->speed->is_shown)
                                                    ? ($data['columns']->speed->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                speed
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="number_of_cores" <?= isset($data['columns']->number_of_cores->is_shown)
                                                      ? ($data['columns']->number_of_cores->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                number of_cores
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="codename" <?= isset($data['columns']->codename->is_shown)
                                                    ? ($data['columns']->codename->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                codename
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="thermal_design_power" <?= isset($data['columns']->thermal_design_power->is_shown)
                                                    ? ($data['columns']->thermal_design_power->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                thermal design power
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="cpu_cache" <?= isset($data['columns']->cpu_cache->is_shown) ?
                                                      ($data['columns']->cpu_cache->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                cpu cache
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sockets_supported" <?= isset($data['columns']->sockets_supported->is_shown)
                                                       ? ($data['columns']->sockets_supported->is_shown ? 'checked' : '')
                                                         : 'checked' ?>>
                                sockets supported
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="grade" <?= isset($data['columns']->grade->is_shown)
                                                     ? ($data['columns']->grade->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                grade
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        Export CSV
                    </button>
                    
                    
                    
                </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body table-responsive p-0 table-responsive">
            <table id="inventory-table" class="table paginated table-bordered table-striped table-hover w-100"
                   cellspacing="0" style="border-collapse: separate;">
                    <thead class="bg-white border">
                    <tr>
                        <th scope="col">&#35;</th>
                        <th scope="col" class="header-part<?= isset($data['columns']->part->is_shown)
                                                     ? ($data['columns']->part->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            part
                        </th>
                        <th scope="col" class="header-stepcode<?= isset($data['columns']->stepcode->is_shown)
                                                     ? ($data['columns']->stepcode->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            step code
                        </th>
                        <th scope="col" class="header-speed<?= isset($data['columns']->speed->is_shown)
                                                     ? ($data['columns']->speed->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            speed
                        </th>
                        
                        <th scope="col" class="header-number_of_cores<?= isset($data['columns']->number_of_cores->is_shown)
                                                     ? ($data['columns']->number_of_cores->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            number_of_cores
                        </th>
                        <th scope="col" class="header-codename<?= isset($data['columns']->codename->is_shown)
                                                     ? ($data['columns']->codename->is_shown ? '' : ' d-none')
                                                       : '' ?>">codename</th>
                        <th scope="col" class="header-thermal_design_power<?= isset($data['columns']->thermal_design_power->is_shown)
                                                     ? ($data['columns']->thermal_design_power->is_shown ? '' : ' d-none')
                                                       : '' ?>">thermal_design_power</th>
                        <th scope="col" class="header-cpu_cache<?= isset($data['columns']->cpu_cache->is_shown)
                                                     ? ($data['columns']->cpu_cache->is_shown ? '' : ' d-none')
                                                       : '' ?>">cpu_cache</th>
                        <th scope="col" class="header-sockets_supported<?= isset($data['columns']->sockets_supported->is_shown)
                                                     ? ($data['columns']->sockets_supported->is_shown ? '' : ' d-none')
                                                       : '' ?>">sockets_supported</th>
                        <th scope="col" class="header-grade<?= isset($data['columns']->grade->is_shown)
                                                     ? ($data['columns']->grade->is_shown ? '' : ' d-none')
                                                       : '' ?>">grade</th>
                        
                    </tr>
                </thead>
                <tbody id="load_cpu"></tbody>
                
            </table>
        </div>
    </div>
        </div>
    </div>
    
</div>

<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>
<script type="text/javascript" src="<?= base_url() ?>resources/js/cpu_module/cpu_module_paginate.js"></script>
<script src="<?php echo base_url(); ?>resources/js/cpu_module/cpu_module_fieldCopy.js" type="text/javascript"></script>

<script id="listingScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/cpu_module/cpu_module_listing.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/cpu_module/cpu_module_FieldEdit.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/cpu_module/cpu_module_search.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/cpu_module/cpu_module_rowInventoryCopy.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/cpu_module/cpu_module_batchinventorysearchCopy.js"></script>


<script>
    $(document).ready(function(){
 
    $('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
    $("#load_cpu").load("<?= base_url(); ?>index.php/cpu_module/getcpu", function(){
        paginate();
    });

    $("#search_cpu").click(function(){
        $('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
        var datas = {
         part_number : $("#partsearch").val(),
         sortOrder : $("#sortOrderitem").val(),
         orderBy : $("#orderByitem").val(),
         ajax : '1'
            };
         $.ajax({
         url : "<?php echo base_url();   ?>index.php/cpu_module/get_cpu_specific",
         type : "post",
         data : datas,
            success : function(msg){                         
                $("#load_cpu").html(msg);
                                                
                     paginate();                           
                 }
        });
    });


    $("#searchHistory").change(function(){
                    
        $('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                    
                    
         var datas = {
         part_number : $("#searchHistory").val(),
         sortOrder : $("#sortOrderitem").val(),
         orderBy : $("#orderByitem").val(),
         ajax : '1'
            };
         $.ajax({
         url : "<?php echo base_url();   ?>index.php/cpu_module/get_cpu_specific",
         type : "post",
         data : datas,
            success : function(msg){                         
                $("#load_cpu").html(msg);
                paginate();
                const tableWidth = $('#load_cpu').width();
                $('.scroller-content').width(tableWidth);
                 }
             });
         });


         $("#orderByitem").change(function(){
                    
                    $('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                       if($('#partsearch').val() ==""){
                        var datas = {
                        part_number : $("#searchHistory").val(),
                        sortOrder : $("#sortOrderitem").val(),
                        orderBy : $("#orderByitem").val(),
                        ajax : '1'
                            };
                        $.ajax({
                        url : "<?php echo base_url();   ?>index.php/cpu_module/get_cpu_specific",
                        type : "post",
                        data : datas,
                            success : function(msg){                         
                                $("#load_cpu").html(msg);
                                                                
                                paginate();                          
                                const tableWidth = $('#load_cpu').width();
                                $('.scroller-content').width(tableWidth);
                            }
                        });
                        } else{
                            var datas = {
                            part_number : $("#partsearch").val(),
                            sortOrder : $("#sortOrderitem").val(),
                            orderBy : $("#orderByitem").val(),
                            ajax : '1'
                                };
                            $.ajax({
                            url : "<?php echo base_url();   ?>index.php/cpu_module/get_cpu_specific",
                            type : "post",
                            data : datas,
                                success : function(msg){                         
                                    $("#load_cpu").html(msg);
                                                                    
                                    paginate();                          
                                    const tableWidth = $('#load_cpu').width();
                                    $('.scroller-content').width(tableWidth);
                                }
                            });
                            
                        }        
                               
                     
                     });
                     $("#sortOrderitem").change(function(){
                    
                    $('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                       if($('#partsearch').val() ==""){
                        var datas = {
                        part_number : $("#searchHistory").val(),
                        sortOrder : $("#sortOrderitem").val(),
                        orderBy : $("#orderByitem").val(),
                        ajax : '1'
                            };
                        $.ajax({
                        url : "<?php echo base_url();   ?>index.php/cpu_module/get_cpu_specific",
                        type : "post",
                        data : datas,
                            success : function(msg){                         
                                $("#load_cpu").html(msg);
                                                                
                                paginate();                           
                                const tableWidth = $('#load_cpu').width();
                                $('.scroller-content').width(tableWidth);
                            }
                        });
                        } else{
                            var datas = {
                            part_number : $("#partsearch").val(),
                            sortOrder : $("#sortOrderitem").val(),
                            orderBy : $("#orderByitem").val(),
                            ajax : '1'
                                };
                            $.ajax({
                            url : "<?php echo base_url();   ?>index.php/cpu_module/get_cpu_specific",
                            type : "post",
                            data : datas,
                                success : function(msg){                         
                                    $("#load_cpu").html(msg);
                                                                    
                                    paginate();                         
                                    const tableWidth = $('#load_cpu').width();
                                    $('.scroller-content').width(tableWidth);
                                }
                            });
                            
                        }        
                               
                     
                     });sortOrderitem


                     $("#regcpu").click(function(){
                    //$('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                    var datas = {
                        partnumber : $("#partnumber").val(),
                        stepcode : $("#stepcode").val(),
                        speed : $("#speed").val(),
                        numberofcores : $("#numberofcores").val(),
                        codename : $("#codename").val(),
                        tdp : $("#tdp").val(),
                        cache : $("#cache").val(),
                        socketsupported : $("#socketsupported").val(),
                        grade : $("#grade").val(),
                        ajax : '1'
                        };
                    $.ajax({
                    url : "<?php echo base_url();   ?>index.php/cpu_module/request/insertcpu",
                    type : "post",
                    data : datas,
                        success : function(msg){  
                            if(msg.isSuccess == "success"){
                                location.reload();
                            }else{
                                $("#alert").html(`<div class='alert alert-${msg.isSuccess} alert-dismissible'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                ${msg.message}!
                                </div>`);
                            }                       
                            
                            //alert(msg.status)
                            console.log(msg.status)                               
                        }
                    });
                });
});
    </script>
    <script>
        /*
*  Add an id of dropdownContainer to the dropdown btn-group div.
*  Add columnSelector class to each .dropdown-item div.
*  Add columnCheckbox class to each input checkbox inside the columnSelector/dropdown-item div.
*  Add class named 'header-"name of column"' to each respective table header th
*  Add class named 'title-"name of column"' to each respective table data td item field 
*/

$(document).ready(function() {
    $('#dropdownContainer .dropdown-menu').click(function(event) {
        event.stopPropagation();
    });

    $('.columnSelector').click(function() {
        const index = $('.columnSelector').index(this);
        const checkbox = $(this).find('.columnCheckbox');
        const columnName = checkbox.attr('name').toString();
        const toggleResult = !checkbox.prop('checked');
        const page = window.location.pathname.split('/').pop();
        checkbox.prop('checked', toggleResult);


        $(`.header-${columnName}`).toggleClass('d-none');
        $(`.item-${columnName}`).toggleClass('d-none');

        const display = toggleResult ? '' : 'none';
        const data = { 'column_name': columnName, 'display': display, 'page': page };
        $.post(`${baseUrl}index.php/user/save_column_state`, data);
    });
});




        </script>
