<?php if(isset($item) && $item != "") {?>
<?php foreach ($item as $indexs => $items): ?>

<?php

?>
<tr>
    <th scope="row"><?= $indexs + 1; ?></th>
    <td><input type="checkbox" value="<?= $items->par; ?>" class="checkbox-row"></td>
    <td>
        <button type="button" class="btn btn-link btn-copy-row">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
    </td>
    
    
    <td class="par" nowrap>
        <button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
        <?= $items->par; ?>
    </td>
    
    <td>
        <button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
        <?= $items->bar; ?>
    </td>
    
    <td>
        <button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
        <?= $items->des; ?>
    </td>
    <td>
        <button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
        <?= $items->con; ?></td>
    <td>
        <button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
        <?= $items->cat; ?></td>
    <td>$<?= $items->pri; ?></td>
    <td><?= $items->qty; ?></td>
    <td>$<?= $items->tot; ?></td>
    <td>
        <button type="button" class="btn btn-link copy-field">
            <i class="fa fa-copy" aria-hidden="true"></i>
        </button>
        <?= $items->bin; ?></td>
    
</tr>
<?php endforeach; ?>
<?php } ?>