<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>


<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header text-left">
                <h3 class="modal-title w-100">New Task</h3>
            </div>
            <div id="success"></div>
            <div class="modal-body">
                
                <div class="form-group">
                    <label for="request">Task</label>
                    <select class="form-control" id="request">
                        <?php foreach ($data['actions'] as $action): ?>
                            <option value="<?= $action->action_id ?>"
                                    <?php if (isset($data['task']) && $data['task'] !== ''): ?>
                                    <?= $action->action_name === $data['task']['action'] ? 'selected' : '' ?>
                                    <?php endif; ?>>
                                <?= $action->action_name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" novalidate>
                    <label for="sku">SKU</label>
                    <input type="text" class="form-control" id="sku" list="skuSuggestions" required
                           <?= isset($data['task']) ? "value=\"{$data['task']['sku']}\"" : '' ?>>
                    <datalist id="skuSuggestions"></datalist>
                </div>
                <div class="form-group">
                    <label for="weight">Weight</label>
                    <input type="number" class="form-control" id="weight" step="any">
                </div>
                <div class="form-group">
                    <label for="shipping">Shipping</label>
                    <input type="text" class="form-control" id="shipping" value="Calculated">
                </div>
                <div id="moved-form" class="form-group d-none">
                    <label for="moved">Link</label>
                    <input type="text" class="form-control" id="moved">
                </div>
                <div class="form-group mt-5">
                    <label for="notes">Notes</label><textarea class="form-control" id="notes" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" class="form-control" id="price" step="any"
                           <?= isset($data['task']) ? "value=\"{$data['task']['price']}\"" : '' ?>>
                </div>
                <div class="promotion-input-container">
                    <label for="promotion">Maximum Promotion Percentage</label>
                    <div class="input-group col-md-6">
                        <input class="form-control" type="number" id="promotion" step="any"
                               aria-label="Maximum Promotion Percentage">
                        <div class="input-group-append">
                            <span class="input-group-text">&#37;</span>
                        </div>
                    </div>
                </div>
                <div class="volume-discount-container mt-3">
                    <div class="volume-discount-parent-label">
                        <label>Volume Discount</label>
                    </div>
                    <div class="volume-discount-content-children row">
                        <div class="volume-discount-content-child col-md-4">
                            <label for="volumeDiscount2x">2 or more</label>
                            <div class="input-group">
                                <input class="form-control" type="number" id="volumeDiscount2x"
                                       aria-label="2 or more">
                                <div class="input-group-append">
                                    <span class="input-group-text">&#37;</span>
                                </div>
                            </div>
                        </div>
                        <div class="volume-discount-content-child col-md-4">
                            <label for="volumeDiscount3x">3 or more</label>
                            <div class="input-group">
                                <input class="form-control" type="number" id="volumeDiscount3x"
                                       aria-label="3 or more">
                                <div class="input-group-append">
                                    <span class="input-group-text">&#37;</span>
                                </div>
                            </div>
                        </div>
                        <div class="volume-discount-content-child col-md-4">
                            <label for="volumeDiscount4x">4 or more</label>
                            <div class="input-group">
                                <input class="form-control" type="number" id="volumeDiscount4x"
                                       aria-label="4 or more">
                                <div class="input-group-append">
                                    <span class="input-group-text">&#37;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-4">
                    <label for="lot">Lot Size</label></br><input type="text" class="form-control" id="lot" value="1">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filtersModal" tabindex="-1" aria-labelledby="filtersModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filtersModalLabel">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body filters-container"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="applyFilters" class="btn btn-primary" data-dismiss="modal">
                    Apply Filters
                </button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('modules/task/confirm_archive_modal'); ?>

<div class="container-fluid pb-5">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="listingsearch">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchColumn" class="custom-select col-md-2">
                                    <option value="sku">SKU</option>
                                    <option value="moved">Moved</option>
                                    <option value="price">Price</option>
                                    <option value="promotion">Maximum Promotion Percentage</option>
                                    <option value="volume_discount_2x">Volume Discount (2 or more)</option>
                                    <option value="volume_discount_3x">Volume Discount (3 or more)</option>
                                    <option value="volume_discount_4x">Volume Discount (4 or more)</option>
                                    <option value="qty_3">Qty 3</option>
                                    <option value="lot">Lot Size</option>
                                    <option value="shipping">Shipping</option>
                                    <option value="weight">Weight</option>
                                    <option value="draft_id">Drafted</option>
                                    <option value="listing_item_id">Listed</option>
                                    <option value="notes">Notes</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemsearch"
                                       placeholder="Search" list="searchSuggestions">
                                <datalist id="searchSuggestions"></datalist>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-group col-md-4">
                    <label for="filterUsers">Users</label>
                    <select class="form-control filters" id="filterUsers" data-allow-clear="true"
                            data-placeholder="Pick users" data-width="100%" multiple disabled>
                        <?php foreach ($data['user_filters'] as $user): ?>
                            <option value="<?= trim($user->firstname) . ' ' . trim($user->lastname) ?>">
                                <?= $user->firstname ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <div class="custom-control custom-switch pt-3">
                        <input type="checkbox" class="custom-control-input" id="onlyMeSwitch" checked>
                        <label class="custom-control-label" for="onlyMeSwitch">Only Me</label>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <label for="orderBy">Order By</label>
                    <select type="button" class="form-control" id="orderBy">
                        <option value="date_requested" selected>Date</option>
                        <option value="request_by">Requester</option>
                        <option value="sku">SKU</option>
                        <option value="price">Price</option>
                        <option value="notes">Notes</option>
                        <option value="draft_id">Draft</option>
                        <option value="listing_item_id">Listing</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="sortOrder">Sort Order</label>
                    <select type="button" class="form-control" id="sortOrder">
                        <option value="ASC" selected>Ascending</option>
                        <option value="DESC">Descending</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Summary</h2>
        </div>
        <div class="card-body">
            <span><strong>Total Tasks:</strong> <span class="task-count"></span></span>
        </div>
    </div>
    <div id="load_listing">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Items</h3>
                <button type="button" class="btn btn-sm btn-primary ml-2 btn-create-ad-blank" id="insertListing" type="button" data-toggle="modal"
        data-target="#myModal">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Create New Task
            </button>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-table" aria-hidden="true"></i>
                            Show/Hide Columns
                        </button>
                        <div class="dropdown-menu" id="columnDropdownSelector"
                             style="height: 200px; overflow-y: auto;">
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="request" <?= isset($data['columns']->request->is_shown)
                                                      ? ($data['columns']->request->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                Task
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sku" <?= isset($data['columns']->sku->is_shown)
                                                  ? ($data['columns']->sku->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                SKU
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="photo" <?= isset($data['columns']->photo->is_shown)
                                                    ? ($data['columns']->photo->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                Photo
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="moved" <?= isset($data['columns']->moved->is_shown)
                                                    ? ($data['columns']->moved->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                Moved
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="price" <?= isset($data['columns']->price->is_shown)
                                                    ? ($data['columns']->price->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                Price
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="promotion" <?= isset($data['columns']->promotion->is_shown)
                                                    ? ($data['columns']->promotion->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                Maximum Promotion Percentage
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="volume-discount-2x" <?= isset($data['columns']->volume_discount_2x
                                                                                           ->is_shown)
                                                                 ? ($data['columns']->volume_discount_2x->is_shown
                                                                     ? 'checked' : '')
                                                                   : 'checked' ?>>
                                Volume Discount (2 or more)
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="volume-discount-3x" <?= isset($data['columns']->volume_discount_3x
                                                                                           ->is_shown)
                                                                 ? ($data['columns']->volume_discount_3x->is_shown
                                                                     ? 'checked' : '')
                                                                   : 'checked' ?>>
                                Volume Discount (3 or more)
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="volume-discount-4x" <?= isset($data['columns']->volume_discount_4x
                                                                                           ->is_shown)
                                                                 ? ($data['columns']->volume_discount_4x->is_shown
                                                                     ? 'checked' : '')
                                                                   : 'checked' ?>>
                                Volume Discount (4 or more)
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="lot" <?= isset($data['columns']->lot->is_shown) ?
                                                      ($data['columns']->lot->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                Lot Size
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="shipping" <?= isset($data['columns']->shipping->is_shown)
                                                       ? ($data['columns']->shipping->is_shown ? 'checked' : '')
                                                         : 'checked' ?>>
                                Shipping
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="weight" <?= isset($data['columns']->weight->is_shown)
                                                     ? ($data['columns']->weight->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Weight
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="banner" <?= isset($data['columns']->banner->is_shown)
                                                     ? ($data['columns']->banner->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Banner
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="qty3" <?= isset($data['columns']->qty3->is_shown)
                                                      ? ($data['columns']->qty3->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                Qty 3
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="drafted" <?= isset($data['columns']->drafted->is_shown)
                                                      ? ($data['columns']->drafted->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                Drafted
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="listed" <?= isset($data['columns']->listed->is_shown)
                                                     ? ($data['columns']->listed->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Listed
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="notes" <?= isset($data['columns']->notes->is_shown)
                                                    ? ($data['columns']->notes->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                Notes
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        Export CSV
                    </button>
                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                            data-target="#filtersModal">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        Filters
                    </button>
                    <button type="button" class="btn btn-sm btn-outline-primary" id="clearFilters">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        Clear Filters
                    </button>
                    <div class="btn-group ml-2">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="doneSwitch">
                            <label class="custom-control-label" for="doneSwitch">Done</label>
                        </div>
                    </div>
                    <div class="btn-group ml-2">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="archiveSwitch">
                            <label class="custom-control-label" for="archiveSwitch">Archived</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="horizontal-scroller sticky-top">
                <div class="scroller-content"></div>
            </div>
            


            <!---->
            <div class="card-body p-0 sticky-headers d-none">
            <table class="table paginated table-bordered table-striped table-hover sticky-table"
                   cellspacing="0" style="border-collapse: separate;">
                <thead class="bg-white border">
                    <tr class="sticky-header-row">
                    <th scope="col" class="header-request<?= isset($data['columns']->request->is_shown)
                                                     ? ($data['columns']->request->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                                Task
                            </th>
                            <th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown)
                                                 ? ($data['columns']->sku->is_shown ? '' : ' d-none')
                                                   : '' ?>">
                                SKU
                            </th>
                            <th scope="col" class="header-category<?= isset($data['columns']->category->is_shown)
                                                 ? ($data['columns']->category->is_shown ? '' : ' d-none')
                                                   : '' ?>">
                                Category
                            </th>
                            <th scope="col" class="header-photo<?= isset($data['columns']->photo->is_shown)
                                                   ? ($data['columns']->photo->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Photo
                            </th>
                            <th scope="col" class="header-moved<?= isset($data['columns']->moved->is_shown)
                                                   ? ($data['columns']->moved->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Moved
                            </th>
                            <th scope="col" class="header-price<?= isset($data['columns']->price->is_shown)
                                                   ? ($data['columns']->price->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Price
                            </th>
                            <th scope="col" class="header-promotion<?= isset($data['columns']->promotion->is_shown)
                                                   ? ($data['columns']->promotion->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Maximum Promotion Percentage
                            </th>
                            <th scope="col" class="header-volume-discount-2x<?= isset($data['columns']->volume_discount_2x
                                                                                          ->is_shown)
                                                                ? ($data['columns']->volume_discount_2x->is_shown 
                                                                    ? '' : ' d-none') 
                                                                  : '' ?>">
                                Volume Discount (2 or more)
                            </th>
                            <th scope="col" class="header-volume-discount-3x<?= isset($data['columns']->volume_discount_3x
                                                                                          ->is_shown)
                                                                ? ($data['columns']->volume_discount_3x->is_shown 
                                                                    ? '' : ' d-none')
                                                                  : '' ?>">
                                Volume Discount (3 or more)
                            </th>
                            <th scope="col" class="header-volume-discount-4x<?= isset($data['columns']->volume_discount_4x
                                                                                          ->is_shown)
                                                                ? ($data['columns']->volume_discount_4x->is_shown 
                                                                    ? '' : ' d-none')
                                                                  : '' ?>">
                                Volume Discount (4 or more)
                            </th>
                            <th scope="col" class="header-lot<?= isset($data['columns']->lot->is_shown)
                                                 ? ($data['columns']->lot->is_shown ? '' : ' d-none')
                                                   : '' ?>">
                                Lot Size
                            </th>
                            <th scope="col" class="header-shipping<?= isset($data['columns']->shipping->is_shown)
                                                      ? ($data['columns']->shipping->is_shown
                                                          ? '' : ' d-none') : '' ?>">
                                Shipping
                            </th>
                            <th scope="col" class="header-weight<?= isset($data['columns']->weight->is_shown)
                                                    ? ($data['columns']->weight->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Weight
                            </th>
                            <th scope="col" class="header-banner<?= isset($data['columns']->banner->is_shown)
                                                    ? ($data['columns']->banner->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Banner
                            </th>
                            <th scope="col" class="header-qty3<?= isset($data['columns']->qty3->is_shown)
                                                    ? ($data['columns']->qty3->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Qty 3
                            </th>
                            <th scope="col" class="header-drafted<?= isset($data['columns']->drafted->is_shown)
                                                     ? ($data['columns']->drafted->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                                Drafted
                            </th>
                            <th scope="col" style="display:none;">DraftBy</th>
                            <th scope="col" style="display:none;">DraftDate</th>
                            <th scope="col" class="header-listed<?= isset($data['columns']->listed->is_shown)
                                                    ? ($data['columns']->listed->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Listed
                            </th>
                            <th scope="col" style="display:none;">ListingBy</th>
                            <th scope="col" style="display:none;">ListingDate</th>
                            <th scope="col" class="header-notes<?= isset($data['columns']->notes->is_shown)
                                                   ? ($data['columns']->notes->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Notes
                            </th>
                            <th scope="col" class="header-archive">
                                Archive
                            </th>
                    </tr>
                </thead>
                
            </table>
        </div>


        <!-- new table -->
        <div class="card-body p-0 inventory-container table-responsive table-container">
            <table class="table paginated table-bordered table-striped table-hover main-table" id="product-table"
                   cellspacing="0" style="border-collapse: separate;">
                <thead class="bg-white border">
                    <tr class="main-header-row">
                    <th scope="col" class="header-request<?= isset($data['columns']->request->is_shown)
                                                     ? ($data['columns']->request->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                                Task
                            </th>
                            <th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown)
                                                 ? ($data['columns']->sku->is_shown ? '' : ' d-none')
                                                   : '' ?>">
                                SKU
                            </th>
                            <th scope="col" class="header-category<?= isset($data['columns']->category->is_shown)
                                                 ? ($data['columns']->category->is_shown ? '' : ' d-none')
                                                   : '' ?>">
                                Category
                            </th>
                            <th scope="col" class="header-photo<?= isset($data['columns']->photo->is_shown)
                                                   ? ($data['columns']->photo->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Photo
                            </th>
                            <th scope="col" class="header-moved<?= isset($data['columns']->moved->is_shown)
                                                   ? ($data['columns']->moved->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Moved
                            </th>
                            <th scope="col" class="header-price<?= isset($data['columns']->price->is_shown)
                                                   ? ($data['columns']->price->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Price
                            </th>
                            <th scope="col" class="header-promotion<?= isset($data['columns']->promotion->is_shown)
                                                   ? ($data['columns']->promotion->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Maximum Promotion Percentage
                            </th>
                            <th scope="col" class="header-volume-discount-2x<?= isset($data['columns']->volume_discount_2x
                                                                                          ->is_shown)
                                                                ? ($data['columns']->volume_discount_2x->is_shown 
                                                                    ? '' : ' d-none') 
                                                                  : '' ?>">
                                Volume Discount (2 or more)
                            </th>
                            <th scope="col" class="header-volume-discount-3x<?= isset($data['columns']->volume_discount_3x
                                                                                          ->is_shown)
                                                                ? ($data['columns']->volume_discount_3x->is_shown 
                                                                    ? '' : ' d-none')
                                                                  : '' ?>">
                                Volume Discount (3 or more)
                            </th>
                            <th scope="col" class="header-volume-discount-4x<?= isset($data['columns']->volume_discount_4x
                                                                                          ->is_shown)
                                                                ? ($data['columns']->volume_discount_4x->is_shown 
                                                                    ? '' : ' d-none')
                                                                  : '' ?>">
                                Volume Discount (4 or more)
                            </th>
                            <th scope="col" class="header-lot<?= isset($data['columns']->lot->is_shown)
                                                 ? ($data['columns']->lot->is_shown ? '' : ' d-none')
                                                   : '' ?>">
                                Lot Size
                            </th>
                            <th scope="col" class="header-shipping<?= isset($data['columns']->shipping->is_shown)
                                                      ? ($data['columns']->shipping->is_shown
                                                          ? '' : ' d-none') : '' ?>">
                                Shipping
                            </th>
                            <th scope="col" class="header-weight<?= isset($data['columns']->weight->is_shown)
                                                    ? ($data['columns']->weight->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Weight
                            </th>
                            <th scope="col" class="header-banner<?= isset($data['columns']->banner->is_shown)
                                                    ? ($data['columns']->banner->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Banner
                            </th>
                            <th scope="col" class="header-qty3<?= isset($data['columns']->qty3->is_shown)
                                                    ? ($data['columns']->qty3->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Qty 3
                            </th>
                            <th scope="col" class="header-drafted<?= isset($data['columns']->drafted->is_shown)
                                                     ? ($data['columns']->drafted->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                                Drafted
                            </th>
                            <th scope="col" style="display:none;">DraftBy</th>
                            <th scope="col" style="display:none;">DraftDate</th>
                            <th scope="col" class="header-listed<?= isset($data['columns']->listed->is_shown)
                                                    ? ($data['columns']->listed->is_shown ? '' : ' d-none')
                                                      : '' ?>">
                                Listed
                            </th>
                            <th scope="col" style="display:none;">ListingBy</th>
                            <th scope="col" style="display:none;">ListingDate</th>
                            <th scope="col" class="header-notes<?= isset($data['columns']->notes->is_shown)
                                                   ? ($data['columns']->notes->is_shown ? '' : ' d-none')
                                                     : '' ?>">
                                Notes
                            </th>
                            <th scope="col" class="header-archive">
                                Archive
                            </th>
                    </tr>
                </thead>
                <tbody id="items"></tbody>
            </table>
        </div>
            <!---->
        </div>
    </div>
</div>

<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script src="<?php echo base_url(); ?>resources/js/fieldCopy.js" type="text/javascript"></script>
<script id="listingScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/task.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/taskFieldEdit.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>
