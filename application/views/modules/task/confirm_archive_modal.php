<div class="modal fade" id="confirmArchiveModal" role="dialog" tabindex="-1" aria-hidden="true"
     aria-labelledby="confirmArchiveModalLabel">
    <div class="modal-dialog modal dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 id="confirmArchiveModalLabel" class="modal-title"></h5>
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label form="confirmArchiveNotes">Notes</label>
                    <textarea class="form-control confirm-archive-notes" id="confirmArchiveNotes"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary confirm-archive">Yes</button>
            </div>
        </div>
    </div>
</div>
