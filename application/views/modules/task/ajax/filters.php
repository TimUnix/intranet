<div class="form-group filters-modal-group">
    <label for="filterActions">Task</label>
    <select class="form-control filters modal-filters" data-allow-clear="true" data-placeholder="Actions"
            data-width="100%"
            id="filterActions" multiple>
        <?php foreach ($actions as $action) : ?>
            <option value="<?= $action->action_id ?>"><?= $action->action_name ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group filters-modal-group">
    <label for="filterCategories">Categories</label>
    <select class="form-control filters modal-filters" data-allow-clear="true" data-placeholder="Categories"
            data-width="100%" id="filterCategories" multiple>
        <?php foreach ($categories as $category): ?>
            <option value="<?= $category->cat ?>"><?= $category->cat ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group filters-modal-group">
    <label for="filterBanner">Banner</label>
    <select class="form-control" id="filterBanner">
        <option class="option-none" value="" selected>None</option>
        <option value="YES">YES</option>
        <option value="NO">NO</option>
    </select>
</div>
<div class="form-group filters-modal-group">
    <label for="filterPhoto">Photo</label>
    <select class="form-control" id="filterPhoto">
        <option class="option-none" value="" selected>None</option>
        <option value="1">YES</option>
        <option value="0">NO</option>
    </select>
</div>
