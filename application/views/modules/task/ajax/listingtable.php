<?php foreach($listing_added as $a): ?>
    <tr>
        <td class="listing_id d-none" data-value="<?= $a->listing_id ?>">
            <?= $a->listing_id ?>
        </td>
        <td class="request item-request<?= isset($columns->request->is_shown)
                                       ? ($columns->request->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->request ?>"
            data-last-edited="<?= date_format(date_create($a->request_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->request_last_edited_by) ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel requestbtn">
                <i class="fa fa-pen requestpenbtn" aria-hidden="true"></i>
            </button>
            <?= $a->request ?><br>
            <?php if(isset($a->request_last_edited_by) && $a->request_last_edited_by != ''): ?>
                <?php
                $fullname = trim($a->request_last_edited_by);
                $firstname = explode(' ', $fullname);
                $ini = substr($firstname[1],0, 1);
                ?>
                <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                    <?= $firstname[0]?> <?=$ini?>.
                </div>
            <?php endif; ?>
            <?php if (trim($a->request_last_edited_by) !== ''): ?>
                <?= date_format(date_create($a->request_last_edited), 'D m/d/Y') ?>
            <?php endif; ?>
        </td>
        <td class="sku item-sku<?= isset($columns->sku->is_shown)
                               ? ($columns->sku->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->sku ?>" data-requester="<?= trim($a->request_by) ?>"
            data-date-requested="<?= date_format(date_create($a->date_requested), 'D m/d/Y') ?>"
            data-last-edited="<?= date_format(date_create($a->sku_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->sku_last_edited_by) ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel skubtn">
                <i class="fa fa-pen skubtn" aria-hidden="true"></i>
            </button>
            <?= $a->sku ?><br>
            <?php if (isset($a->sku_last_edited_by) && $a->sku_last_edited_by != ''): ?>
                <?php
                $fullname = trim($a->sku_last_edited_by);
                $firstname = explode(' ', $fullname);
                $ini = substr($firstname[1],0, 1);
                ?>

                <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                    <?= $firstname[0] ?> <?=$ini?>.
                </div>

            <?php elseif (isset($a->request_by) && $a->request_by != ''): ?>
                <?php
                $fullname = trim($a->request_by);
                $firstname = explode(' ', $fullname);
                $ini = substr($firstname[1],0, 1);
                ?>

                <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                    <?= $firstname[0] ?> <?=$ini?>.
                </div>
            <?php endif; ?>
            <?php if (isset($a->sku_last_edited) && $a->sku_last_edited != ''): ?>
                <?= date_format(date_create($a->sku_last_edited), 'D m/d/Y') ?>
            <?php elseif (isset($a->date_requested) && $a->date_requested != ''): ?>
                <?= date_format(date_create($a->date_requested), 'D m/d/Y') ?>
            <?php endif; ?>
        </td>
        <td class="category item-category<?= isset($columns->category->is_shown)
                               ? ($columns->category->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->category ?>"><?= $a->category ?></td> 
        <td class="photo item-photo<?= isset($columns->photo->is_shown)
                                   ? ($columns->photo->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->photo == 1 ? 'YES' : 'NO' ?>"
            data-last-edited="<?= date_format(date_create($a->photo_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->photo_last_edited_by) ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel photobtn">
                <i class="fa fa-pen" aria-hidden="true"></i>
            </button>
            <?= $a->photo == 1 ? 'YES' : 'NO' ?><br>
            <?php if (isset($a->photo_last_edited_by) && trim($a->photo_last_edited_by) != ''): ?>
                <?php
                $fullname = trim($a->photo_last_edited_by);
                $firstname = explode(' ', $fullname);
                $ini = substr($firstname[1],0, 1);
                ?>
                <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                    <?= $firstname[0] ?> <?=$ini?>.
                </div>
                <?= date_format(date_create($a->photo_last_edited), 'D m/d/Y') ?>
            <?php endif; ?>
        </td>
        <td class="moved item-moved<?= isset($columns->moved->is_shown)
                                   ? ($columns->moved->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->moved ?>"
            data-last-edited="<?= date_format(date_create($a->moved_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->moved_last_edited_by) ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel movedbtn">
                <i class="fa fa-pen" aria-hidden="true"></i>
            </button>
            <?= $a->moved ?><br>
            <?php if (isset($a->moved) && $a->moved != '' && isset($a->moved_last_edited_by)
                      && trim($a->moved_last_edited_by) != ''): ?>
                <?php
                $fullname = trim($a->moved_last_edited_by);
                $firstname = explode(' ', $fullname);
                $ini = substr($firstname[1],0, 1);
                ?>
                <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                    <?= $firstname[0] ?> <?=$ini?>.
                </div>
                <?= date_format(date_create($a->moved_last_edited), 'D m/d/y') ?><br>
            <?php endif; ?>
        </td>
        <td class="price item-price<?= isset($columns->price->is_shown)
                                   ? ($columns->price->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->price ?>"
            data-last-edited="<?= date_format(date_create($a->price_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->price_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel pricebtn">
                    <i class="fa fa-pen pricepenbtn" aria-hidden="true"></i>
                </button>
                <?php if ((float) $a->price > 0): ?>
                    $<?= $a->price ?><br>
                    <?php if (isset($a->price_last_edited_by) &&
                              trim($a->price_last_edited_by) !== ''): ?>
                        <?php
                        $fullname = trim($a->price_last_edited_by);
                        $firstname = explode(' ', $fullname);
                        $ini = substr($firstname[1],0, 1);
                        ?>
                        <div data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                            <?= $firstname[0] ?> <?=$ini?>.
                        </div>
                        <?= date_format(date_create($a->price_last_edited), 'D m/d/Y') ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="promotion item-promotion<?= isset($columns->promotion->is_shown)
                                   ? ($columns->promotion->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->promotion ?>"
            data-last-edited="<?= date_format(date_create($a->promotion_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->promotion_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel promotionbtn">
                    <i class="fa fa-pen promotionbtn" aria-hidden="true"></i>
                </button>
                <?php if ($a->promotion != ''): ?>
                    <?= $a->promotion ?>&#37;<br>
                    <?php if (isset($a->promotion_last_edited_by) &&
                              trim($a->promotion_last_edited_by) !== ''): ?>
                        <?php
                        $fullname = trim($a->promotion_last_edited_by);
                        $firstname = explode(' ', $fullname);
                        $ini = substr($firstname[1],0, 1);
                        ?>
                        <div data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                            <?= $firstname[0] ?> <?=$ini?>.
                        </div>
                        <?= date_format(date_create($a->promotion_last_edited), 'D m/d/Y') ?>
                    <?php endif; ?>
                <?php else: ?>
                    None
                <?php endif; ?>
            </div>
        </td>
        <td class="volume-discount-2x item-volume-discount-2x<?= isset($columns->volume_discount_2x->is_shown)
                                                             ? ($columns->volume_discount_2x->is_shown ? '' 
                                                              : ' d-none') : '' ?>"
            data-value="<?= $a->volume_discount_2x ?>"
            data-last-edited="<?= date_format(date_create($a->volume_discount_2x_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->volume_discount_2x_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel volumediscount2xbtn">
                    <i class="fa fa-pen volumediscount2xbtn" aria-hidden="true"></i>
                </button>
                <?php if ($a->volume_discount_2x != ''): ?>
                    <?= $a->volume_discount_2x ?>&#37;<br>
                    <?php if (isset($a->volume_discount_2x_last_edited_by) &&
                              trim($a->volume_discount_2x_last_edited_by) !== ''): ?>
                        <?php
                        $fullname = trim($a->volume_discount_2x_last_edited_by);
                        $firstname = explode(' ', $fullname);
                        $ini = substr($firstname[1],0, 1);
                        ?>
                        <div data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                            <?= $firstname[0] ?> <?=$ini?>.
                        </div>
                        <?= date_format(date_create($a->volume_discount_2x_last_edited), 'D m/d/Y') ?>
                    <?php endif; ?>
                <?php else: ?>
                    None
                <?php endif; ?>
            </div>
        </td>
        <td class="volume-discount-3x item-volume-discount-3x<?= isset($columns->volume_discount_3x->is_shown)
                                                             ? ($columns->volume_discount_3x->is_shown ? '' 
                                                              : ' d-none') : '' ?>"
            data-value="<?= $a->volume_discount_3x ?>"
            data-last-edited="<?= date_format(date_create($a->volume_discount_3x_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->volume_discount_3x_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel volumediscount3xbtn">
                    <i class="fa fa-pen volumediscount3xbtn" aria-hidden="true"></i>
                </button>
                <?php if ($a->volume_discount_3x != ''): ?>
                    <?= $a->volume_discount_3x ?>&#37;<br>
                    <?php if (isset($a->volume_discount_3x_last_edited_by) &&
                              trim($a->volume_discount_3x_last_edited_by) !== ''): ?>
                        <?php
                        $fullname = trim($a->volume_discount_3x_last_edited_by);
                        $firstname = explode(' ', $fullname);
                        $ini = substr($firstname[1],0, 1);
                        ?>
                        <div data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                            <?= $firstname[0] ?> <?=$ini?>.
                        </div>
                        <?= date_format(date_create($a->volume_discount_3x_last_edited), 'D m/d/Y') ?>
                    <?php endif; ?>
                <?php else: ?>
                    None
                <?php endif; ?>
            </div>
        </td>
        <td class="volume-discount-4x item-volume-discount-4x<?= isset($columns->volume_discount_4x->is_shown)
                                                             ? ($columns->volume_discount_4x->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->volume_discount_4x ?>"
            data-last-edited="<?= date_format(date_create($a->volume_discount_4x_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->volume_discount_4x_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel volumediscount4xbtn">
                    <i class="fa fa-pen volumediscount4xbtn" aria-hidden="true"></i>
                </button>
                <?php if ($a->volume_discount_4x != ''): ?>
                    <?= $a->volume_discount_4x ?>&#37;<br>
                    <?php if (isset($a->volume_discount_4x_last_edited_by) &&
                              trim($a->volume_discount_4x_last_edited_by) !== ''): ?>
                        <?php
                        $fullname = trim($a->volume_discount_4x_last_edited_by);
                        $firstname = explode(' ', $fullname);
                        $ini = substr($firstname[1],0, 1);
                        ?>
                        <div data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                            <?= $firstname[0] ?> <?=$ini?>.
                        </div>
                        <?= date_format(date_create($a->volume_discount_4x_last_edited), 'D m/d/Y') ?>
                    <?php endif; ?>
                <?php else: ?>
                    None
                <?php endif; ?>
            </div>
        </td>
        <td class="lot item-lot<?= isset($columns->lot->is_shown)
                               ? ($columns->lot->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->lot ?>"
            data-last-edited="<?= date_format(date_create($a->lot_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->lot_last_edited_by) ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel lotbtn">
                <i class="fa fa-pen" aria-hidden="true"></i>
            </button>
            <?= $a->lot ?><br>
            <?php if (isset($a->lot_last_edited_by) && trim($a->lot_last_edited_by) != ''): ?>
                <?php
                $fullname = trim($a->lot_last_edited_by);
                $firstname = explode(' ', $fullname);
                $ini = substr($firstname[1],0, 1);
                ?>
                <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                    <?= $firstname[0] ?> <?=$ini?>.
                </div>
                <?= date_format(date_create($a->lot_last_edited), 'D m/d/Y') ?>
            <?php endif; ?>
        </td>
        <td class="shipping item-shipping<?= isset($columns->shipping->is_shown)
                                         ? ($columns->shipping->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->shipping ?>"
            data-last-edited="<?= date_format(date_create($a->shipping_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->shipping_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel shippingbtn">
                    <i class="fa fa-pen shippingpenbtn" aria-hidden="true"></i>
                </button>
                <?= $a->shipping ?><br>
                <?php if ($a->shipping !== '' && isset($a->shipping_last_edited_by)
                          && $a->shipping_last_edited_by !== ''): ?>
                    <?php
                    $fullname = trim($a->shipping_last_edited_by);
                    $firstname = explode(' ', $fullname);
                    $ini = substr($firstname[1],0, 1);
                    ?>
                    <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                        <?= $firstname[0] ?> <?=$ini?>.
                    </div>
                    <?= date_format(date_create($a->shipping_last_edited), 'D m/d/Y') ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="weight item-weight<?= isset($columns->weight->is_shown)
                                     ? ($columns->weight->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->weight ?>"
            data-last-edited="<?= date_format(date_create($a->weight_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->weight_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel penbtn">
                    <i class="fa fa-pen penbtn" aria-hidden="true"></i>
                </button>
                <?php if ((float) $a->weight > 0): ?>
                    <?= $a->weight ?> lbs<br>
                    <?php if (isset($a->weight_last_edited_by) && trim($a->weight_last_edited_by) !== ''): ?>
                        <?php
                        $fullname = trim($a->weight_last_edited_by);
                        $firstname = explode(' ', $fullname);
                        $ini = substr($firstname[1],0, 1);
                        ?>
                        <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                            <?= $firstname[0] ?> <?=$ini?>.
                        </div>
                        <?= date_format(date_create($a->weight_last_edited), 'D m/d/Y') ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="banner item-banner<?= isset($columns->banner->is_shown)
                                     ? ($columns->banner->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->banner; ?>"
            data-last-edited="<?= date_format(date_create($a->banner_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->banner_last_edited_by) ?>">
            <div class="form-group">
                <button type="button" class="btn btn-link text-primary draftid-cancel bannerbtn">
                    <i class="fa fa-pen bannerpenbtn" aria-hidden="true"></i>
                </button>
                <?= $a->banner ?><br>
                <?php if (isset($a->banner_last_edited_by) && trim($a->banner_last_edited_by) !== ''): ?>
                    <?php
                    $fullname = trim($a->banner_last_edited_by);
                    $firstname = explode(' ', $fullname);
                    $ini = substr($firstname[1],0, 1);
                    ?>
                    <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                        <?= $firstname[0] ?> <?=$ini?>.
                    </div>
                    <?= date_format(date_create($a->banner_last_edited), 'D m/d/Y') ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="qty3 item-qty3<?= isset($columns->qty3->is_shown)
                                     ? ($columns->qty3->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->qty_3 ?>"
            data-last-edited="<?= date_format(date_create($a->qty_3_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->qty_3_last_edited_by) ?>">
            
                <button type="button" class="btn btn-link text-primary draftid-cancel qty3btn">
                    <i class="fa fa-pen qty3btn" aria-hidden="true"></i>
                </button>
                <?= $a->qty_3 ?><br>
                <?php if (isset($a->qty_3_last_edited_by) && trim($a->qty_3_last_edited_by) !== ''): ?>
                    <?php
                    $fullname = trim($a->qty_3_last_edited_by);
                    $firstname = explode(' ', $fullname);
                    $ini = substr($firstname[1],0, 1);
                    ?>
                    <div  data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                        <?= $firstname[0] ?> <?=$ini?>.
                    </div>
                    <?= date_format(date_create($a->qty_3_last_edited), 'D m/d/Y') ?>
                <?php endif; ?>
            
        </td>
        <td class="draftid item-drafted<?= isset($columns->drafted->is_shown)
                                       ? ($columns->drafted->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->draft_id ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel draft">
                <i class="fa fa-pen draftpen" aria-hidden="true"></i>
            </button>
            <a href="https://bulksell.ebay.com/ws/eBayISAPI.dll?SingleList&sellingMode=SellLikeItem&draftId=<?= $a->draft_id ?>" target="_blank">
                <?=$a->draft_id?>
            </a><br>
            <?php $firstname=explode(' ', trim($a->draft_by));
                   ?>
            <?php if($a->draft_by != null && $a->draft_by != ''): ?>
                <?php $ini = substr($firstname[1],0, 1);?>
                <div  data-toggle="tooltip" data-placement="top" title="<? trim($a->draft_by) ?>">
                    <?= $firstname[0];?> <?=$ini?>.
                </div>
            <?php endif; ?>
            <?php if ($a->draft_id != '0'): ?>
                <?= date_format(date_create($a->draft_date), 'D m/d/Y') ?>
            <?php endif; ?>
        </td>
        <td class="draftby d-none" data-value="<?= $a->draft_by ?>"></td>
        <td class="draftdate d-none" data-value="<?= date_format(date_create($a->draft_date), 'D m/d/Y') ?>"></td>
        <td class="listingitemid item-listed<?= isset($columns->listed->is_shown)
                                            ? ($columns->listed->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->listing_item_id ?>">
            <button type="button" class="btn btn-link text-primary draftid-cancel listing">
                <i class="fa fa-pen listingpen" aria-hidden="true"></i>
            </button>
            <a href='https://www.ebay.com/itm/<?= $a->listing_item_id ?>' target='_blank'>
                <?= $a->listing_item_id?>
            </a><br>
            <?php $firstname=explode(' ', trim($a->listing_by));
                 ?>
            <?php if($a->listing_by != null && $a->listing_by != ''): ?>
                <?php $ini = substr($firstname[1],0, 1);?>
                <div  data-toggle="tooltip" data-placement="top" title="<?= trim($a->listing_by) ?>">
                    <?= $firstname[0] ?> <?= $ini ?>.
                </div>
            <?php endif; ?>
            <?php if ($a->listing_item_id != '0'): ?>
                <?= date_format(date_create($a->listing_date), 'D m/d/Y') ?><br>
            <?php endif; ?>
        </td>
        <td class="listingby d-none" data-value="<?= $a->listing_by ?>"></td>
        <td class="listingdate d-none" data-value="<?= date_format(date_create($a->listing_date), 'D m/d/Y') ?>">
        </td>
        <td class="notes item-notes<?= isset($columns->notes->is_shown)
                                   ? ($columns->notes->is_shown ? '' : ' d-none') : '' ?>"
            data-value="<?= $a->notes ?>"
            data-last-edited="<?= date_format(date_create($a->notes_last_edited), 'D m/d/Y') ?>"
            data-editor="<?= trim($a->notes_last_edited_by) ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel notebtn">
                    <i class="fa fa-pen notepenbtn" aria-hidden="true"></i>
                </button>
                <?= $a->notes ?><br>
                <?php if (trim($a->notes) !== '' && isset($a->notes_last_edited_by)
                          && trim($a->notes_last_edited_by) !== ''): ?>
                    <?php
                    $fullname = trim($a->notes_last_edited_by);
                    $firstname = explode(' ', $fullname);
                    $ini = substr($firstname[1],0, 1);
                    ?>
                    <div data-toggle="tooltip" data-placement="top" title="<?= $fullname ?>">
                        <?= $firstname[0] ?> <?=$ini?>.
                    </div>
                    <?= date_format(date_create($a->notes_last_edited), 'D m/d/Y') ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="text-center">
            <?php if (!$is_archived_shown): ?>
                <button type="button" class="btn btn-link btn-archive" data-toggle="modal"
                        data-target="#confirmArchiveModal">
                    <i class="fa fa-archive" aria-hidden="true"></i>
                </button>
            <?php else: ?>
                Archived by <?= $a->archived_by ?> on <?= $a->date_archived ?>
            <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
