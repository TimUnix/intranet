<div class="card">
    <div class="card-header">
        <h3 class="card-title">Local Items</h3>
    </div>
    <div class="card-body table-responsive p-0">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>&#35;</th>
                    <th>Image</th>
                    <th>Part Number</th>
                    <th>Description</th>
                    <th>Con</th>
                    <th>Price</th>
                    <th>QTY</th>
                    <th>Total</th>
                    <th>
                        <img src="<?php echo base_url(); ?>resources/img/up-icon.png"
                             width="20">
                    </th>
                    <th>
                        <img src="<?php echo base_url(); ?>resources/img/net-icon.png"
                             width="20">
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($local_items as $index => $item): ?>
                    <tr>
                        <td><?= $index + 1 ?></td>
                        <td>
                            <img src="<?= 
                                      ($item->img == '/i/noimage.png') 
                                      ? base_url()  . 'resources/img/noimage.png'
                                      : $item->img 
                                      ?>"
                                 width="80">
                        </td>
                        <td><?= $item->par; ?></td>
                        <td><?= $item->des; ?></td>
                        <td><?= $item->con; ?></td>
                        <td>$<?= $item->pri; ?></td>
                        <td><?= $item->qty; ?></td>
                        <td>
                            $<?= $item->tot; ?>
                        </td>
                        
                        <td></td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
