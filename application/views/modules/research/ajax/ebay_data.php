<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body">
		<h3>Ebay Search: <?php echo $ebayitem;  ?></h3>
        
        <strong>Total items: </strong> <?php echo $items;  ?> <strong>-</strong>
		<strong>Low: </strong> <?php echo $min;  ?> <strong>-</strong>
		<strong>High: </strong> <?php echo $max;  ?> <strong>-</strong>
		<strong>Average: </strong> <?php echo number_format($average, 2);  ?> <strong>-</strong>
		<strong>Median: </strong> <?php echo number_format($median, 2);  ?>
    </div>
    <!-- /.card-body -->
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Ebay Results</h3>
	</div>
	
    <div class="card-body table-responsive p-0 table-scroll"  style="height: 80vh;">
		<table id="product_table" class="table paginated table-bordered table-striped table-hover" style="width: 100%; border-collapse: separate;" cellspacing="0">
			<thead  class="bg-white position-sticky border" style="top: 0;">
				<tr>
                    <th>&#35;</th>
					<th>Image</th>
					<th>Price</th>
					<th>Seller</th>
					<th>Title</th>
                <!--<th>Quantity</th>--> 
				</tr>
			</thead>
            <tbody>
                
               <?php foreach($ebay as $index => $item): ?>
                 <?php switch($item->seller){ 
                    case "unixsurpluscom": ?>
                        <tr style="background-color: #add8e6 ">
                   <?php break;?>
                   <?php case "unixpluscom": ?>
                        <tr style="background-color: #add8e6 ">
                   <?php break;?>
                   <?php case "unixsurplusnet": ?>
                        <tr style="background-color: #add8e6 ">
                   <?php break;?>
                   <?php case "itrecyclenow": ?>
                        <tr style="background-color: #add8e6 ">
                   <?php break;?>
                   <?php default: ?>
                    <tr>
                    <?php }?>
                               
                       <td><?= ($index + 1); ?></td>
                        <td>
                            <img src="<?= $item->img; ?>">
                        </td>
	                    <td>
                            <button class="btn btn-link copy-field">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                           $<?= $item->price; ?>
                        </td>
                        
                        <td>
                            <button class="btn btn-link copy-field">
                                <i class="fa fa-copy" aria-hidden="true"></i> 
                            </button>
                                <?= $item->seller; ?>
                           
                        </td>
                        <td>
                            <button class="btn btn-link copy-field">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            <a href="<?= $item->link; ?>" target="_blank" rel="noreferrer noopener">
                                <?= $item->title; ?>
                            </a>
                        </td>
                      <!-- <td><?= number_format($item->qty) ?></td> -->
                    </tr>
                <?php endforeach; ?>
        	</tbody>
   		</table>
	</div>
</div>
<script src="<?= base_url(); ?>resources/js/fieldCopy.js"
        type="text/javascript"></script>
<script>
    
</script>
