<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    
    <!-- /.card-body -->
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Ebay Results</h3>
	</div>
	
    <div class="card-body">
		<table id="product_table" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
                    <th>&#35;</th>
                    <th>Item Number</th>
					<th>Image</th>
					<th>Price</th>
					<th>Seller</th>
					<th>Title</th>
                    <th>Link</th>
				</tr>
			</thead>
            <tbody>
		        <?php
                //echo $rate;echo "<pre>"; var_dump($rate);echo "</pre>";

                $num = 0;
                foreach($datas as $item): ?>
                    <tr>
                        <td><?php echo ($num = $num + 1); ?></td>
                        <td>
                            <?php echo $item->item_number; ?>
                        </td>
                        <td>
                            <img src="<?php echo $item->img; ?>">
                        </td>
	                    <td>
                            
                            $<?php echo $item->price; ?>
                        </td>
                        <td>
                            
                            <?php echo $item->seller; ?>
                        </td>
                        <td>
                            
                            <?php echo $item->title; ?>
                        </td>
                        <td>
                            
                            <a href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
        	</tbody>
   		</table>
	</div>
</div>
<script src="<?php echo base_url(); ?>resources/js/fieldCopy.js"
        type="text/javascript"></script>
