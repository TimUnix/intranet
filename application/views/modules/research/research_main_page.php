<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<div class="container-fluid" style="margin-bottom: 80px;">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Search Ebay</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <ddiv class="card-body">
                    <form id="searchForm">
                        <div class="row">
                            <div class="col-md-12">
                                <!--<div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-lg btn-primary"
                                                    id="ebaybutton"
                                                    type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <input class="form-control form-control-lg"
                                               type="search" id="ebayitem"
                                               value="<?= $data['search_query']; ?>"
                                               placeholder="Search Part Number">
                                    </div>
                                </div>-->


                                <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchForm">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchHistory"
                                            style="width: 100%;" 
                                            name="searchHistory[]" class="custom-select col-md-2">
                                        <option value="" disabled selected>
                                            History
                                        </option>
                                        <?php foreach ($data['search_history'] as $history): ?>
                                            <option value="<?= $history->search; ?>">
                                                <?= $history->search; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                <input class="form-control input-lg" type="search" id="ebayitem" placeholder="Search" list="searchSuggestions">
                                <!--<datalist id="searchSuggestions"></datalist>-->
                            </div>
                        </div>



                            </div>
                        </div>
                        <div class="row">
                            <!--<div class="col-md-8">
                                <div class="form-group">
                                    <label for="searchHistory">Search History:
                                    </label>
                                    <select class="form-control"
                                            id="searchHistory"
                                            style="width: 100%;" 
                                            name="searchHistory[]">
                                        <option value="" disabled selected>
                                            Part Number
                                        </option>
                                        <?php foreach ($data['search_history'] as $history): ?>
                                            <option value="<?= $history->search; ?>">
                                                <?= $history->search; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>-->
                            <!--<div class="col-md-2">
                                <div class="form-group">
                                    <label for="sortOrder">Sort Order:</label>
                                    <select class="form-control" id="sortOrder"
                                            style="width: 100%;" tabindex="-1"
                                            aria-hidden="true">
                                            <?php if($_GET['sortOrder'] == "DESC"){
                                            ?>
                                                <option value="DESC">Descending</option>
                                                <option value="ASC">Ascending</option> <?php
                                            }else{
                                                ?>
                                                    
                                                    <option value="ASC">Ascending</option>
                                                    <option value="DESC">Descending</option> <?php
                                                    
                                                
                                                
                                            }
                                            ?>
                                        
                                    </select>
                                </div>
                            </div>-->
                            <!--<div class="col-md-2">
                                <div class="form-group">
                                    <label for="orderBy">Order By:</label>
                                    <select class="form-control" style="width: 100%;"
                                            tabindex="-1" aria-hidden="true"
                                            id="orderBy">
                                            <?php if($_GET['orderBy'] == "seller"){
                                                ?>
                                                <option value="seller">Seller</option>
                                                <option value="title">Title</option>
                                                <option value="price">Price</option>
                                                <?php

                                            }else if($_GET['orderBy'] == "title"){
                                                ?>
                                                <option value="title">Title</option>
                                                <option value="seller">Seller</option>
                                                <option value="price">Price</option>
                                                <?php

                                            }else if($_GET['orderBy'] == "price"){
                                                ?>
                                                <option value="price">Price</option>
                                                <option value="seller">Seller</option>
                                                <option value="title">Title</option>
                                                
                                                <?php
                                            }else{
                                                ?>
                                                 <option value="price">Price</option>
                                                <option value="seller">Seller</option>
                                                <option value="title">Title</option>
                                                <?php
                                            }
                                            ?>
                                        
                                    </select>
                                </div>-->
                            </div>
                        </div>
                    </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- card-row -->
    <div id="local_item_container"></div>
    <!-- /.row -->
    <div id="load_products">
    </div>
</div>
<?php $this->load->view('templates/common/spinner'); ?>
<?php $this->load->view('templates/common/footer'); ?>

<script>
$(document).ready(function(){
     $("#searchForm").submit(function(event){
         event.preventDefault();
         
         const sortOrder = $("#sortOrder").val();
         const orderBy = $("#orderBy").val();
         const search = $("#ebayitem").val();
         //window.location.href = `<?= base_url(); ?>index.php/research?search=${search}&sortOrder=${sortOrder}&orderBy=${orderBy}`;
         window.location.href = `<?= base_url(); ?>index.php/research?search=${search}`;
     });
 });
</script>
<script>
 $(document).ready(function(){
     const params = new URLSearchParams(window.location.search);
     const searchParam = params.get('search');

     const wasItemRequested = searchParam !== null;

     
     if (wasItemRequested) {
         $("#load_products").html(spinner);
         $("#ebayitem").val(searchParam);

         const referred = params.get('referred');
         const wasReferred = referred == 1;

         if (wasReferred) {
	         $("#local_item_container").load("<?php echo base_url(); ?>index.php/research/request/local_items", {
                 'part_number': $("#ebayitem").val()
                 //'sort_order': $("#sortOrder").find(":selected").val(),
                 //'order_by': $("#orderBy").find(":selected").val()
             });
         }

         /*$.post("<?php echo base_url(); ?>index.php/research/request/search_ebay/<?php echo $_GET['sortOrder'] . '/' . $_GET['orderBy']; ?>",
                { ebayitem : $("#ebayitem").val(), ajax : '1' },
                function(response) {
                    $("#load_products").html(response);
                }
         );*/
         $.post("<?php echo base_url(); ?>index.php/research/request/search_ebay/",
                { ebayitem : $("#ebayitem").val(), ajax : '1' },
                function(response) {
                    $("#load_products").html(response);
                }
         );
     }

     $("#searchHistory").on('change', function() {
         $("#ebayitem").val($(this).val());
     });
 });
</script>
<script src="<?php echo base_url(); ?>resources/js/fieldCopy.js"
        type="text/javascript"></script> 
        <script>
            $(document).ready(function() {

	
                $("#searchHistory").change(function(){
                    
                    const search = $("#ebayitem").val();
                    //const sortOrder = $("#sortOrder").val();
                    //const orderBy = $("#orderBy").val();
                    //window.location.href = `<?= base_url(); ?>index.php/research?search=${search}&sortOrder=${sortOrder}&orderBy=${orderBy}`;
                    window.location.href = `<?= base_url(); ?>index.php/research?search=${search}`;
                        
                    });
                    $("#sortOrder").change(function(){
                    
                    const search = $("#ebayitem").val();
                    //const sortOrder = $("#sortOrder").val();
                    //const orderBy = $("#orderBy").val();
                    //window.location.href = `<?= base_url(); ?>index.php/research?search=${search}&sortOrder=${sortOrder}&orderBy=${orderBy}`;
                    window.location.href = `<?= base_url(); ?>index.php/research?search=${search}`;
                        
                    });
                    $("#orderBy").change(function(){
                    
                    const search = $("#ebayitem").val();
                    //const sortOrder = $("#sortOrder").val();
                    //const orderBy = $("#orderBy").val();
                    //window.location.href = `<?= base_url(); ?>index.php/research?search=${search}&sortOrder=${sortOrder}&orderBy=${orderBy}`;
                    window.location.href = `<?= base_url(); ?>index.php/research?search=${search}`;
                        
                    });
                });
            </script>
