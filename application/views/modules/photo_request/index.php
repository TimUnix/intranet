<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<div id="load"></div>

<div class="container-fluid" style="padding-bottom: 60px;">
            <div class="card">
                        <div class="card-header ">
                            <div class="card-title">
                                Search
                        </div>
                        </div>
                    <div class="card-body">
                        <form id="searchForm">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <div class="row">
                                        <div class="col input-group input-group-lg">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-lg btn-primary" type="button" id="searchColumn">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <input class="form-control input-lg" type="search" id="itemSearch"
                                                placeholder="Search" required>
                                            </div>  
                                </div>
                            </div>
                            </div>
                        </form>
                    </div> 
                </div>
            </div>
        <div class="card" id="photo_request_table">
            <div class="card-header"> 
                <div class="row">
                    <div> 
                    <button id="insertListing" type="button" class="btn btn-sm btn-outline-primary ret" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        Request Photo
                    </button>
                    </div>
                    <div class="col-md-auto custom-control custom-switch" style="margin-left: 15px; margin-top: 5px;">
                        <input type="checkbox" class="done-filter form-control custom-control-input" id="doneSwitch" name="done_filter" value="1">
                                
                        <label class="custom-control-label" for="doneSwitch">Show Done Only</label>
                    </div>
                    <div class="col-md-auto custom-control custom-switch" style="margin-left: 15px; margin-top: 5px;">
                        <input type="checkbox" class="archive-filter form-control custom-control-input" id="archiveSwitch" name="archive_filter" >
                                
                        <label class="custom-control-label" for="archiveSwitch">Show Archives</label>
                    </div>
                </div>    
            </div>
			<div class="horizontal-scroller sticky-top">
				<div class="scroller-content"></div>
			</div>
			<div class="card-body p-0 sticky-headers d-none">
                <table class="table paginated table-bordered table-striped table-hover sticky-table" cellspacing="0" style="border-collapse: separate;" >
                       <thead class="bg-white border">
                                <col width="5%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />    
                                <col width="13%" />
                                <col width="15%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="12%" />
                                <col width="10%" />
                        <tr class="sticky-header-row" style="background-color : #ffffff">
                            <th class="align-middle text-center" scope="col">&#35;</th>
                            <th class="align-middle text-center" scope="col" >Time</th>
                            <th class="align-middle text-center" scope="col" >Name</th>
                            <th class="align-middle text-center" scope="col" >SKU</th> 
                            <th class="align-middle text-center" scope="col" >Price</th> 
                            <th class="align-middle text-center" scope="col" >Kind of Photos Needed(Angles)</th>    
                            <th class="align-middle text-center" scope="col" >Comments</th>                     
                            <th class="align-middle text-center" scope="col" >Added to Linnworks</th>                                                                 
                            <th class="align-middle text-center" scope="col" >Location</th>
                            <th class="align-middle text-center" scope="col" >Done</th> 
                            <th class="align-middle text-center" scope="col" >Archive</th>                                                                                   
                        </tr>
                    </thead>
                </table>
			</div>

            <div class="card-body p-0 inventory-container table-responsive table-container">
                    <table class="table paginated table-bordered table-striped table-hover main-table" id="photo_request"
						cellspacing="0" style="border-collapse: separate;" >
                       <thead class="bg-white border">
                                <col width="5%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />    
                                <col width="13%" />
                                <col width="15%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="12%" />
                                <col width="10%" />
                        <tr class="main-header-row" style="background-color : #ffffff">
                            <th class="align-middle text-center" scope="col">&#35;</th>
                            <th class="align-middle text-center" scope="col" >Time</th>
                            <th class="align-middle text-center" scope="col" >Name</th>
                            <th class="align-middle text-center" scope="col" >SKU</th> 
                            <th class="align-middle text-center" scope="col" >Price</th> 
                            <th class="align-middle text-center" scope="col" >Kind of Photos Needed(Angles)</th>    
                            <th class="align-middle text-center" scope="col" >Comments</th>                     
                            <th class="align-middle text-center" scope="col" >Added to Linnworks</th>                                                                 
                            <th class="align-middle text-center" scope="col" >Location</th>
                            <th class="align-middle text-center" scope="col" >Done</th> 
                            <th class="align-middle text-center" scope="col" >Archive</th>                                                                                   
                        </tr>
                    </thead>
                    <tbody id="photo_table_body">
                        <tr>
                            <td colspan="100%">Search an item to see results here.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
	</div>
</div>
</div>
    <!--show data-->

    
<?php $this->load->view('templates/common/spinner'); ?>

<script id="photoScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/photo_request/photoreq.js"></script>
<script id="photoScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/photo_request/photo_editable.js"></script>
<?php $this->load->view('modules/photo_request/photo_modal'); ?>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>

<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
