<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
				<form>
                <div class="row">
                        <div class="col">
                            <div >
                                <label for="sku">SKU</label>
                                <input type="text" class="form-control" id="formsku">
                            </div> 
                            <div>
                                <label for="price">Price</label>
                                <div class= "input-group mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                                </div>
                                <input type="number" class="form-control" id="price">
                                <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                                </div>
                                </div>
                            </div>        
                            <div>
                                <label for="angles">Kind of Photos Needed(Angles)</label>
                                <input type="text" list="angleslist" class="form-control" id="angles">
                                <datalist id="angleslist">
                                        <option>Front-Facing</option>
                                        <option>Sides</option>
                                        <option>Back</option>
                                        <option>Label Information</option>
                                        <option>System Specs on Monitor</option>
                                        <option>All of the Above</option>
                                        <option>Other</option>
                                </datalist>     
                            </div>
                        </div>
                        <div class= "col">
                            <div>
                                <label for="comments">Comments</label>
                                <textarea class="form-control" aria-label="Comments" id="comments"></textarea>
                                </div>
                            <div>
                                <label for="location">Location</label>
                                <select id="location" class="custom-select form-control">
                                        <option value="Sta Clara">Sta Clara</option>
                                        <option value="Merced">Merced</option>
                                </select>
                            </div>
                        </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitf">Submit</button>
            </div>
				</form>
        </div>
    </div>
</div>
</div>
<div class="modal fade mark-listing-done-modal" id="markListingDoneModal" tabindex="-1"
     aria-labelledby="markListingDoneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="markListingDoneModalLabel">Mark <span class="sku-done"></span> Done </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-done">Confirm</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade mark-listing-undone-modal" id="markListingUndoneModal" tabindex="-1"
     aria-labelledby="markListingUndoneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="markListingUnoneModalLabel">Mark <span class="sku-done"></span> Undone </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-undone">Confirm</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade archive-modal" id="archiveModal" tabindex="-1"
     aria-labelledby="archivemodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="archivemodalLabel">Archive <span class="sku-archive">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?
            <div>
            <label for="notes">Notes</label>
            <textarea class="form-control" aria-label="Notes" id="notes"></textarea>
            </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-archive">Confirm</button>
            </div>
        </div>
    </div>
</div>