<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <?php if ($item->done){ ?>
    <tr style="background-color : #abf7d3 ">
    <?php   } else { ?>
	<tr class = "myrow" style="background-color : #00000 "><?php } ?>
		<td class="id" data-id="<?= $item->id ?>"scope="col"><?=  $index + 1 ?></td>
		<td style="max-width:200px; white-space: normal; word-wrap:break-word;" scope="col" id="time" class="time item-time<?= isset($columns->time->is_shown) 
                        ? ($columns->time->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time ?>">
        <?= $item->time ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="name item-name<?= isset($columns->name->is_shown) 
                        ? ($columns->name->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->name ?>">
        <?= $item->name ?></td>

		<td style="max-width:200px; white-space: normal; word-wrap:break-word;" scope="col" id="sku" class="sku item-sku<?= isset($columns->sku->is_shown) 
                        ? ($columns->sku->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->sku ?>">
        <?= $item->sku ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="price item-price<?= isset($columns->price->is_shown) 
                        ? ($columns->price->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->price ?>">
        <?= $item->price ?></td>

		<td style="max-width:100px; white-space: normal; word-wrap:break-word;" scope="col" class="angles item-angles<?= isset($columns->angles->is_shown) 
                        ? ($columns->angles->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->angles ?>">
        <?php if (!$item->done): ?>
        <button type="button" class="btn btn-link text-primary draftid-cancel anglesbtn">
                <i class="fa fa-edit fa-sm anglespenbtn" aria-hidden="true"></i>
            </button>
        <?php endif; ?>
        <?= $item->angles ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="comments item-comments<?= isset($columns->comments->is_shown) 
                        ? ($columns->comments->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->comments ?>">
        <?php if (!$item->done): ?>
        <button type="button" class="btn btn-link text-primary draftid-cancel commentsbtn">
                <i class="fa fa-edit fa-sm commentspenbtn" aria-hidden="true"></i>
            </button>
            <?php endif; ?>
        <?= $item->comments ?></td>

        <td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="addtolinn item-addtolinn<?= isset($columns->added_to_linnworks->is_shown) 
                        ? ($columns->added_to_linnworks->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->added_to_linnworks ?>">
                        <?php if (!$item->date_added == null): ?>  
                            <p><?= $item->added_by ?></p>
                            <p><?= date('D Y-m-d', strtotime($item->date_added)) ?></p>
                        <?php endif; ?>
        <?php if (!$item->done): ?>
        <button type="button" class="btn btn-link text-primary draftid-cancel addtolinnbtn">
                <i class="fa fa-edit fa-sm addtolinnpenbtn" aria-hidden="true"></i>
            </button>
            <?php endif; ?>
        <?= $item->added_to_linnworks ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="location item-location<?= isset($columns->location->is_shown) 
                        ? ($columns->location->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->location ?>">
        <?= $item->location ?>
    </td>

    <?php
            if ($item->done == '1'){?>
                <td class="undone" style="max-width:120px; white-space: normal; word-wrap:break-word;">
                <p><?= $item->marked_done_by ?></p>
                <p><?= date('D Y-m-d', strtotime($item->done_date)) ?></p>
                <button class="btn btn-secondary btn-undone text-nowrap " data-toggle="modal"
                        data-target="#markListingUndoneModal">
                    <i class="far fa-square" aria-hidden="true"></i>
                    Undone
                </button>
            </td> 
           <?php } else { ?>
                <td class="done" style="max-width:120px; white-space: normal; word-wrap:break-word;">
                <button class="btn btn-secondary btn-done text-nowrap " data-toggle="modal"
                        data-target="#markListingDoneModal">
                    <i class="far fa-square" aria-hidden="true"></i>
                     Done 
                </button>
        </td>
        <?php  }
        ?>
       <?php
            if ($item->is_archived == 0){?>
                <td class="archive" style="max-width:120px; white-space: normal; word-wrap:break-word;">
                <button type="button" class="btn btn-link btn-archive text-nowrap " data-toggle="modal"
                        data-target="#archiveModal">
                    <i class="fa fa-archive" aria-hidden="true"></i>
                </button>
            </td> 
           <?php } else { ?>
            <td>
            Archived by <?= $item->archived_by ?> on <?= date('D Y-m-d', strtotime($item->archived_date)) ?>
        </td>
        <?php  }
        ?>
	</tr>

<?php endforeach; }?>