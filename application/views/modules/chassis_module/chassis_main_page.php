<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<button id="insertListing" type="button" class="btn btn-info btn-lg ml-2 mb-4" data-toggle="modal"
        data-target="#myModal">
    Create New Chassis
</button>
<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-left">
                <h3 class="modal-title w-100">New</h3>
            </div>
            <div class="modal-body">
                <div id="alert"></div>
                <div class="form-group">
                    <label for="part_number">Part Number</label>
                    <input type="text" class="form-control" id="partnumber">
                </div>

                <div class="form-group" novalidate>
                    <label for="mfr">Brand</label>
                    <input type="text" class="form-control" id="mfr">
                </div>
                <div class="form-group">
                    <label for="u">U</label>
                    <input type="text" class="form-control" id="u">
                </div>
                <div class="form-group">
                    <label for="model">Model</label>
                    <input type="text" class="form-control" id="model">
                </div>
                <div class="form-group">
                    <label for="ff">Form Factor</label>
                    <input type="text" class="form-control" id="ff">
                </div>
                <div class="form-group">
                    <label for="psu">PSU</label>
                    <input type="number" class="form-control" id="psu">
                </div>
                <div class="form-group">
                    <label for="motherboard">Motherboard</label>
                    <input type="text" class="form-control" id="motherboard">
                </div>
                <div class="form-group">
                    <label for="controller">Controller</label>
                    <input type="text" class="form-control" id="controller">
                </div>
                <div class="form-group">
                    <label for="bin">Bin</label>
                    <input type="text" class="form-control" id="bin">
                </div>
                <div class="form-group">
                    <label for="node">Node</label></br>
                    <input type="number" class="form-control" id="node">
                </div>
                <div class="form-group">
                    <label for="backplane">Backplane</label></br>
                    <input type="text" class="form-control" id="backplane">
                </div>
                <div class="form-group">
                    <label for="drivebays">Drivebays</label></br>
                    <input type="text" class="form-control" id="drivebays">
                </div>
                <div class="form-group">
                    <label for="rearbays">Rearbays</label></br>
                    <input type="text" class="form-control" id="rearbays">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="regchassis">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pb-5">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="search_chassis">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                
                                <input class="form-control input-lg" type="search" id="partsearch"
                                       placeholder="Search Part" list="searchSuggestions">
                                <datalist id="searchSuggestions"></datalist>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                        <div class="form-group">
                            <label for="searchHistory">Search History:</label>
                            <select class="form-control" id="searchHistory"
                                    style="width: 100%;" 
                                    name="searchHistory[]">
                                <option value="" disabled selected>
                                    Part Number
                                </option>
                                <?php foreach ($search_history as $history): ?>
                                    <option value="<?= $history->search; ?>">
                                        <?= $history->search; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                <div class="form-group col-md-2">
                    <label for="orderBy">Order By</label>
                    <select type="button" class="form-control" id="orderByitem">
                        <option value="part_number" selected>Part Number</option>
                        <option value="mfr">Manufacturer</option>
                        <option value="u">U</option>
                        <option value="model">Model</option>
                        <option value="ff">Form Factor</option>
                        <option value="psu">PSU</option>
                        <option value="motherboard">Motherboard</option>
                        <option value="controller">Controller</option>
                        <option value="bin">Bin</option>
                        <option value="node">Node</option>
                        <option value="backplane">Backplane</option>
                        <option value="drivebays">Drivebays</option>
                        <option value="rearbays">Rearbays</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="sortOrder">Sort Order</label>
                    <select type="button" class="form-control" id="sortOrderitem">
                        <option value="ASC" selected>Ascending</option>
                        <option value="DESC">Descending</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
             <h3 class="card-title">Cpu Module</h3>
                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-table" aria-hidden="true"></i>
                            Show/Hide Columns
                        </button>
                        <div class="dropdown-menu" id="columnDropdownSelector"
                             style="height: 200px; overflow-y: auto;">
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="par" <?= isset($data['columns']->par->is_shown)
                                                      ? ($data['columns']->par->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                Part Number
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="mfr" <?= isset($data['columns']->mfr->is_shown)
                                                  ? ($data['columns']->mfr->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                Brand
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="u" <?= isset($data['columns']->u->is_shown)
                                                    ? ($data['columns']->u->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                U
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="model" <?= isset($data['columns']->model->is_shown)
                                                      ? ($data['columns']->model->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                Model
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="ff" <?= isset($data['columns']->ff->is_shown)
                                                    ? ($data['columns']->ff->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                Form Factor
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="psu" <?= isset($data['columns']->psu->is_shown)
                                                    ? ($data['columns']->psu->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                PSU
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="motherboard" <?= isset($data['columns']->motherboard->is_shown) ?
                                                      ($data['columns']->motherboard->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                Motherboard
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="controller" <?= isset($data['columns']->controller->is_shown)
                                                       ? ($data['columns']->controller->is_shown ? 'checked' : '')
                                                         : 'checked' ?>>
                                Controller
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="bin" <?= isset($data['columns']->bin->is_shown)
                                                     ? ($data['columns']->bin->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Bin
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="node" <?= isset($data['columns']->node->is_shown)
                                                     ? ($data['columns']->node->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Node
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="backplane" <?= isset($data['columns']->backplane->is_shown)
                                                     ? ($data['columns']->backplane->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Backplane
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="drivebays" <?= isset($data['columns']->drivebays->is_shown)
                                                     ? ($data['columns']->drivebays->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Drivebays
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="rearbays" <?= isset($data['columns']->rearbays->is_shown)
                                                     ? ($data['columns']->rearbays->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                Rearbays
                            </div>    
                        </div>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        Export CSV
                    </button>   
                </div>
            </div>
            <div class="card-body table-responsive p-0 table-scroll" style="height: 80vh">
                <table id="inventory-table" class="table paginated table-bordered table-striped table-hover w-100" cellspacing="0" style="border-collapse: separate;">
                    <thead class="bg-white position-sticky border" style="top: 0;">
                    <tr>
                        <th scope="col">&#35;</th>
                        <th scope="col" class="header-par<?= isset($data['columns']->par->is_shown)
                                                     ? ($data['columns']->par->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Part Number
                        </th>
                        <th scope="col" class="header-mfr<?= isset($data['columns']->mfr->is_shown)
                                                     ? ($data['columns']->mfr->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Brand
                        </th>
                        <th scope="col" class="header-u<?= isset($data['columns']->u->is_shown)
                                                     ? ($data['columns']->u->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            U
                        </th>
                        
                        <th scope="col" class="header-model<?= isset($data['columns']->model->is_shown)
                                                     ? ($data['columns']->model->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Model
                        </th>
                        <th scope="col" class="header-ff<?= isset($data['columns']->ff->is_shown)
                                                     ? ($data['columns']->ff->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Form Factor</th>
                        <th scope="col" class="header-psu<?= isset($data['columns']->psu->is_shown)
                                                     ? ($data['columns']->psu->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            PSU</th>
                        <th scope="col" class="header-motherboard<?= isset($data['columns']->motherboard->is_shown)
                                                     ? ($data['columns']->motherboard->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Motherboard</th>
                        <th scope="col" class="header-controller<?= isset($data['columns']->controller->is_shown)
                                                     ? ($data['columns']->controller->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Controller</th>
                        <th scope="col" class="header-bin<?= isset($data['columns']->bin->is_shown)
                                                     ? ($data['columns']->bin->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Bin</th>
                        <th scope="col" class="header-node<?= isset($data['columns']->node->is_shown)
                                                     ? ($data['columns']->node->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            Node</th>
                        <th scope="col" class="header-backplane<?= isset($data['columns']->backplane->is_shown)
                                                     ? ($data['columns']->backplane->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                        Backplane</th>
                        <th scope="col" class="header-drivebays<?= isset($data['columns']->drivebays->is_shown)
                                                     ? ($data['columns']->drivebays->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                        Drivebays</th>
                        <th scope="col" class="header-rearbays<?= isset($data['columns']->rearbays->is_shown)
                                                     ? ($data['columns']->rearbays->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                        Rearbays</th>
                    </tr>
                </thead>
                <tbody id="load_chassis"></tbody>  
            </table>
        </div>
    </div>
    </div>
</div>   
</div>

<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>
<script id="listingFieldEditScript" src="<?= base_url(); ?>resources/js/chassis_module/chassis_module_FieldEdit.js"></script>

<?php $this->load->view('templates/common/footer'); ?>


