<?php if(!empty($get_shipping_data)) { foreach($get_shipping_data as $index => $item): ?>
	<tr>
		<?php if($item->counter == 0) : ?>
			<?php if($item->shipped_date != '0000-00-00T00:00:00.000Z') : ?>
				<td style="background-color: #f7ef03;" scope="col"><?= $index + 1 ?></td>
				<td style="background-color: #f7ef03;" scope="col" data-value="<?= $item->item_number ?>" nowrap><?= $item->item_number ?></td>
				<td style="background-color: #f7ef03;" scope="col" data-value="<?= $item->title ?>"><?= $item->title ?></td>
				<td style="background-color: #f7ef03;" scope="col" data-value="<?= $item->sku ?>" nowrap><?= $item->sku ?></td>
				<td style="background-color: #f7ef03;" scope="col" data-value="<?= $item->shipped_date ?>" nowrap><?= $item->shipped_date ?></td>
		<?php endif; ?>
		<?php elseif($item->counter == 1) : ?>
			<?php if($item->shipped_date != '0000-00-00T00:00:00.000Z') : ?>
				<td style="background-color: #28a745;" scope="col"><?= $index + 1 ?></td>
				<td style="background-color: #28a745;" scope="col" data-value="<?= $item->item_number ?>" nowrap><?= $item->item_number ?></td>
				<td style="background-color: #28a745;" scope="col" data-value="<?= $item->title ?>"><?= $item->title ?></td>
				<td style="background-color: #28a745;" scope="col" data-value="<?= $item->sku ?>" nowrap><?= $item->sku ?></td>
				<td style="background-color: #28a745;" scope="col" data-value="<?= $item->shipped_date ?>" nowrap><?= $item->shipped_date ?></td>
			<?php endif; ?>
		<?php endif; ?>
	</tr>#
<?php endforeach; }?>