<?php $this->load->view('templates/header_new'); ?>





<div class="container-fluid" style="margin-bottom: 80px;">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Shipped per Order</h3>
                </div>
                <div class="card-body">
                    <canvas id="shipping" style="min-height: 300px; height: 300px; max-height: 300px; max-width: 100%;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Quantity Shipped per Day</h3>
                </div>
                <div class="card-body">
                    <canvas id="quantitySold" style="min-height: 300px; height: 300px; max-height: 300px; max-width: 100%;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
<div class="col-12 col-sm-6 col-lg-6">
<!---->
<div class="card">
              <div class="card-header">
                <h5 class="m-0">Shipped per Order Today</h5>
              </div>
              <div class="card-body">
                
                <table class="table table-bordered table-striped table-hover paginated" style="width: 100%; border-collapse: separate;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" nowrap>&#35;</th>
                                    <th scope="col" nowrap>Item Number</th>
                                    <th scope="col">Title</th>
                                    <th scope="col" nowrap>SKU</th>
                                    <th scope="col" nowrap>Ship Date</th>
                                </tr>
                            </thead>
                            <tbody id="shipped_today_body_today">
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                
              </div>
            </div>
            <!---->
</div>


<div class="col-12 col-sm-6 col-lg-6">
<!---->
<div class="card">
              <div class="card-header">
                <h5 class="m-0">Quantity Shipped per Day Today</h5>
              </div>
              <div class="card-body">
                
              <table class="table table-bordered table-striped table-hover paginated" style="width: 100%; border-collapse: separate;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" nowrap>&#35;</th>
                                    <th scope="col" nowrap>Item Number</th>
                                    <th scope="col">Title</th>
                                    <th scope="col" nowrap>SKU</th>
                                    <th scope="col" nowrap>QTY Sold</th>
                                    <th scope="col" nowrap>Ship Date</th>
                                </tr>
                            </thead>
                            <tbody id="quantity_shipped_today_body_table">
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                
              </div>
            </div>
            <!---->
</div>
</div>


                        


                        

                        


                        










                        






<?php $this->load->view('templates/common/spinner'); ?>

<script id="ebayShippingScript" data-base-url="<?php echo base_url();?>"
        src="<?php echo base_url(); ?>resources/js/ebay_shipping.js"
        type="text/javascript">
</script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>


<?php $this->load->view('templates/common/footer'); ?>