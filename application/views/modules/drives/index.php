<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/drives/ajax/createDrives'); ?>
<?php $this->load->view('modules/drives/ajax/generateTemplate'); ?>
<?php $this->load->view('modules/drives/ajax/previewTemplate'); ?>
<?php $this->load->view('modules/drives/ajax/htmlFile'); ?>
<!-- <div id="load"></div> -->


<div class="container-fluid" style="padding-bottom: 60px;">
	<div class="card">
		<div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
        	<form id="searchForm">
        		<div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchColumn">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchSelected" class="custom-select col-md-2">
                                    <option value="sku">SKU</option>
                                    <option value="bar">Barcode</option>
                                    <option value="brand">Brand</option>
                                    <option value="upc">UPC</option>
                                    <option value="ff">Form factor</option>
                                    <option value="size">Size</option>
                                    <option value="type">Type</option>
                                    <option value="rpm">Rpm</option>
                                    <option value="speed">Speed</option>
                                    <option value="interface">Interface</option>
                                    <option value="series">Series</option>
                                    <option value="con">Condition</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemSearch"
                                       placeholder="Search">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="orderBy">Order By</label>
                        <select type="button" class="form-control" id="orderBy">   
                            <option value="bar">Barcode</option>
                            <option value="brand">Brand</option>
                            <option value="upc">UPC</option>
                            <option value="con">Condition</option>
                            <option value="ff">Form factor</option>
                            <option value="interface">Interface</option>
                            <option value="rpm">Rpm</option>
                            <option value="size">Size</option>
                            <option value="sku" selected>SKU</option>
                            <option value="speed">Speed</option>
                            <option value="series">Series</option>
                            <option value="type">Type</option>                                                       
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sortOrder">Sort Order</label>
                        <select type="button" class="form-control" id="sortOrder">
                            <option value="ASC" selected>Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
        	</form>
        </div>
	</div>
   
  <!--  <div class="card">
        <div class="card-header">
            <h2 class="card-title">Summary</h2>
        </div>
        <div class="card-body">
            <span><strong>Total:</strong> <span class="task-count"></span></span>
        </div>
    </div> -->
	<div class = "card">
        <div class="card-header">
        <button id="insertListing" type="button" class="btn btn-info btn-lg ml-2" style = "padding: 0.25rem 0.5rem; font-size:.875rem;" data-toggle="modal"
                data-target="#myModal">
            Create Drives
        </button>
        <button id="generateTemplateButton" type="button" class="btn btn-info btn-lg ml-2" style = "padding: 0.25rem 0.5rem; font-size:.875rem;" data-toggle="modal"
            data-target="#generateTemplate">
            Generate Template 
        </button>
            <div class="card-tools">
                <button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <div class="btn-group" id="dropdownContainer">
                    <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>Show/Hide Columns</button>

                <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sku" <?= isset($data['columns']->sku->is_shown)
                                                      ? ($data['columns']->sku->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            SKU
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="bar" <?= isset($data['columns']->bar->is_shown)
                                                      ? ($data['columns']->bar->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Barcode
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="upc" <?= isset($data['columns']->upc->is_shown)
                                                      ? ($data['columns']->upc->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            UPC
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="brand" <?= isset($data['columns']->brand->is_shown)
                                                      ? ($data['columns']->brand->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Brand
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="ff" <?= isset($data['columns']->ff->is_shown)
                                                      ? ($data['columns']->ff->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Form Factor
                    </div>
                   <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="size" <?= isset($data['columns']->size->is_shown)
                                                      ? ($data['columns']->size->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Size
                    </div>
                     <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="type" <?= isset($data['columns']->type->is_shown)
                                                      ? ($data['columns']->type->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Type
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="rpm" <?= isset($data['columns']->rpm->is_shown)
                                                      ? ($data['columns']->rpm->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Rpm
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="speed" <?= isset($data['columns']->speed->is_shown)
                                                      ? ($data['columns']->speed->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Speed
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="interface" <?= isset($data['columns']->interface->is_shown)
                                                      ? ($data['columns']->interface->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Interface
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="series" <?= isset($data['columns']->series->is_shown)
                                                      ? ($data['columns']->series->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Series
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1"
                               name="con" <?= isset($data['columns']->con->is_shown)
                                          ? ($data['columns']->con->is_shown ? 'checked' : '')
                                            : 'checked' ?>>
                        Condition
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="horizontal-scroller">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body sticky-headers p-0 d-none" style="z-index: 2;">
			<table class="table table-bordered table-striped table-hover sticky-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="bsticky-thead bg-white">
					<tr class="sticky-header-row">
						<th scope="col" nowrap>&#35;</th>
						<th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown) ? ($data['columns']->sku->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
						<th scope="col" class="header-bar<?= isset($data['columns']->bar->is_shown) ? ($data['columns']->bar->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Barcode</th>
                        <th scope="col" class="header-upc<?= isset($data['columns']->upc->is_shown) ? ($data['columns']->upc->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>UPC</th>
						<th scope="col" class="header-brand<?= isset($data['columns']->brand->is_shown) ? ($data['columns']->brand->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Brand</th>
						<th scope="col" class="header-ff<?= isset($data['columns']->ff->is_shown) ? ($data['columns']->ff->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Form Factor</th>
						<th scope="col" class="header-size<?= isset($data['columns']->size->is_shown) ? ($data['columns']->size->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Size</th>
						<th scope="col" class="header-type<?= isset($data['columns']->type->is_shown) ? ($data['columns']->type->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Type</th>
						<th scope="col" class="header-rpm<?= isset($data['columns']->rpm->is_shown) ? ($data['columns']->rpm->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Rpm</th>
						<th scope="col" class="header-speed<?= isset($data['columns']->speed->is_shown) ? ($data['columns']->speed->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Speed</th>
						<th scope="col" class="header-interface<?= isset($data['columns']->interface->is_shown) ? ($data['columns']->interface->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Interface</th>
						<th scope="col" class="header-series<?= isset($data['columns']->series->is_shown) ? ($data['columns']->series->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Series</th>
						<th scope="col" class="header-con<?= isset($data['columns']->con->is_shown) ? ($data['columns']->con->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Condition</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="card-body p-0 table-container table-responsive">
			<table class="table table-bordered table-striped table-hover main-table" id="product-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="main-thead bg-white border">
					<tr class="main-header-row">
						<th scope="col" nowrap>&#35;</th>
						<th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown) ? ($data['columns']->sku->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
						<th scope="col" class="header-bar<?= isset($data['columns']->bar->is_shown) ? ($data['columns']->bar->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Barcode</th>
                        <th scope="col" class="header-upc<?= isset($data['columns']->upc->is_shown) ? ($data['columns']->upc->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>UPC</th>
						<th scope="col" class="header-brand<?= isset($data['columns']->brand->is_shown) ? ($data['columns']->brand->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Brand</th>
						<th scope="col" class="header-ff<?= isset($data['columns']->ff->is_shown) ? ($data['columns']->ff->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Form Factor</th>
						<th scope="col" class="header-size<?= isset($data['columns']->size->is_shown) ? ($data['columns']->size->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Size</th>
						<th scope="col" class="header-type<?= isset($data['columns']->type->is_shown) ? ($data['columns']->type->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Type</th>
						<th scope="col" class="header-rpm<?= isset($data['columns']->rpm->is_shown) ? ($data['columns']->rpm->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Rpm</th>
						<th scope="col" class="header-speed<?= isset($data['columns']->speed->is_shown) ? ($data['columns']->speed->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Speed</th>
						<th scope="col" class="header-interface<?= isset($data['columns']->interface->is_shown) ? ($data['columns']->interface->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Interface</th>
						<th scope="col" class="header-series<?= isset($data['columns']->series->is_shown) ? ($data['columns']->series->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Series</th>
						<th scope="col" class="header-con<?= isset($data['columns']->con->is_shown) ? ($data['columns']->con->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Condition</th>
					</tr>
				</thead>
				<tbody id="product_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
			</table>
		</div>
	</div>
</div>
</div> <!-- for footer -->
<?php $this->load->view('templates/common/spinner'); ?>

<script id="drivesScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/drives.js"></script>
<script id="drivesFieldEditScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/drivesFieldEdit.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
