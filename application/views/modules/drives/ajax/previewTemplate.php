<div class="modal fade" id="previewTemplate" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <img src="<?= base_url() ?>resources/img/PlusDrives_Banner1.png" alt="banner" 
                    style="height:150px; width:1250px;">  
            </div>
            <div class="modal-body">
                <div style="text-align:center; font-family: 'Montserrat', sans-serif;"><strong><span class="title_preview" style="font-size: 150%">
                    </span></strong></div> <br>
                <div class="row">
                    
                    <div class="col-md-6">
                        <input type="image" id="image_preview" style="height:470px; width:500px;">
                    </div>
                    <div class="col-md-6">
                        <strong><h5 style="color:orange; font-family: 'Montserrat', sans-serif;">TECHNICAL SPECIFICATIONS</h5></strong>
                        <table class="table" style="font-family: 'Montserrat', sans-serif;">
                            <tbody>
                                <tr>
                                    <td><strong>Brand:</strong></td>
                                    <td class="brand_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Model:</strong></td>
                                    <td class="model_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Application:</strong></td>
                                    <td class="application_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Form Factor:</strong></td>
                                    <td class="ff_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Capacity:</strong></td>
                                    <td class="capacity_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Interface:</strong></td>
                                    <td class="interface_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Condition:</strong></td>
                                    <td class="condition_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Warranty:</strong></td>
                                    <td class="warranty_preview">
										<p>30 Days Seller Warranty</p>
									</td>
                                </tr>
                                <tr>
                                    <td><strong>Description:</strong></td>
                                    <td class="description_preview"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist" style="font-family: 'Montserrat', sans-serif;">
                    <li class="nav-item">
                 	    <a class="nav-link active" id="about_us" data-toggle="pill" href="#about-us" role="tab" aria-controls="about-us" aria-selected="true">About Us</a>
                    </li>
                    <li class="nav-item">
                 	    <a class="nav-link active" id="warranty" data-toggle="pill" href="#warranty-" role="tab" aria-controls="warranty-" aria-selected="true">Warranty</a>
                    </li>
                    <li class="nav-item">
                 	    <a class="nav-link active" id="shipping" data-toggle="pill" href="#shipping-" role="tab" aria-controls="shipping-" aria-selected="true">Shipping</a>
                    </li>
                </ul>
                <div class="tab-content" style="font-family: 'Montserrat', sans-serif;">
                    <div class="tab-pane fade show active" id="about-us" role="tabpanel" aria-labelledby="about_us">
                        <br><h2>About Us</h2>
                    
                        <h5>We know our customers rely on our equipment, so quality assurance is our top priority. Every single Hard Drive is tested by experienced technicians.</h5>
                                        
                        <br><h4>SERVICE</h4>
                        <p>Our business is taking care of you. Our tailored solutions are created to help you reach your goals by fitting you with the most cost efficient solutions while providing the best service in the industry when it comes to basics like.</p>
                        
                                        <ul>
                                            <li>Fast Shipping</li>
                                            <li>Packaged by Experts</li>
                                            <li>Extended Warranties</li>
                                            <li>No Hassle Warranty Claims</li>
                                            <li>Discounts on large Quantity Purchases</li>
                                            <li>OEM and Compatible products</li>
                                            <li>Tested and Reliable</li>
                                        </ul>
                                        
                        <br><h4>FLEXIBILITY</h4>
                        <p>Our Hard Drive solutions are tailored to meet your needs. Our technical sales staff understand real world implementations. We’ll make your life easier by offering the best value proposition. </p>
                            
                        <br><h4>PARTNERSHIP</h4>
                        <p>We can’t do it all. We have strategic partnerships with some of the largest hardware manufacturers and software developers in the industry. Our relationship allows for access to source support and product information.</p>
                    </div>
                    <div class="tab-pane fade show active" id="warranty-" role="tabpanel" aria-labelledby="warranty">
						<br>
							<h2>Warranty</h2>
							<p>100% Guaranteed Working Condition w/ Warranty. Ships from the USA!
							<br>All Hard Drives have a 30 day warranty unless otherwise stated.</p>
						<br>
							<h2>Return</h2>
							<p>After receiving the item, contact seller within 30 Days
							<br>Refund will be given as Money Back, Replacement
							<br>Seller pays for return shipping </p>

                    </div>
                    <div class="tab-pane fade show active" id="shipping-" role="tabpanel" aria-labelledby="shipping">
                        <br>
							<p>Free 2 day shipping <br>
							We understand the importances of fast reliable delivery, and we prioritize getting you tracking information within a couple hours of purchase.</p>
						<br>
							<p>Will usually ship within 1 business day of receiving cleared payment.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
