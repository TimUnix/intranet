<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
                <div class="row">
                    <div class="col-md-6" novalidate>
                        <label for="sku">SKU</label>
                        <input type="text" class="form-control" id="sku" list="skuSuggestions" required
                            <?= isset($data['task']) ? "value=\"{$data['task']['sku']}\"" : '' ?>>
                        <datalist id="skuSuggestions"></datalist>
                    </div>
                    <div class="col-md-6">
                        <label for="bar">Barcode</label>
                        <input type="text" class="form-control" id="bar">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="upc">UPC</label>
                        <input type="text" class="form-control" id="upc">
                    </div>
                    <div class="col-md-6">
                        <label for="brand">Brand</label>
                        <input type="text" class="form-control" id="brand">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="ff">Form Factor</label>
                        <select id="ff" class="form-control">
                            <option value="null"> </option>
                            <option value="1.8in">1.8in</option>
                            <option value="2.5in">2.5in</option>
                            <option value="3.5in">3.5in</option>
                            <option value="U.2">U.2</option>
                            <option value="M.2">M.2</option>
                            <option value="PCIe">PCIe</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="size">Size</label>
                        <input type="number" class="form-control" id="size">
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-6">
                        <label for="con">Type</label>
                        <select id="type" class="form-control">
                            <option value="null"> </option>
                            <option value="SAS">SAS</option>
                            <option value="SATA">SATA</option>
                            <option value="SSD">SSD</option>
                        </select>
                    </div> 
                    <div class="col-md-6">
                        <label for="rpm">Rpm</label>
                        <input type="text" id="rpm" class="form-control">
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-6">
                        <label for="speed">Speed</label>
                        <input type="text" id="speed" class="form-control">
                    </div>   
                    <div class="col-md-6">
                        <label for="interface">Interface</label>
                        <select id="Interface" class="form-control">
                            <option value="null"> </option>
                            <option value="SSD">SSD</option>
                        </select>
                    </div>
                </div>
                <div class="row">  
                    <div class="col-md-6">
                        <label for="series">Series</label>
                        <input type="text" id="series" class="form-control">
                    </div>       
                    <div class="col-md-6">
                        <label for="con">Condition</label>
                        <select id="con" class="form-control">
                            <option value="null"> </option>
                            <option value="NEW">NEW</option>
                            <option value="NP3">NP3</option>
                            <option value="NP6">NP6</option>
                            <option value="GB">Grade B</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="brandError" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="brandErrorLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDescriptionModalLabel">Please leave blank for HGST, WD, and Seagate</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>