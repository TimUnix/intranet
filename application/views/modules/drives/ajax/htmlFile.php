<div style="display:none;">
<div class="" id="whole_html">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
<link href="https://fonts.googleapis.com/css2?family=Montserrat&amp;display=swap" rel="stylesheet">
<style>img, input{max-width:100%}
  main {
min-width: 320px;
max-width: 400px;
padding: 50px;
margin: 0 auto;
background: #fff;
height: auto;
}
section {
display: none;
padding: 20px 0 0;
border-top: 1px solid #ddd;
}
label {
display: inline-block;
margin: 0 0 -1px;
padding: 15px 25px;
font-weight: 600;
text-align: center;
color: #bbb;
border: 1px solid transparent;
}
label:hover {
color: #888;
cursor: pointer;
}
input:checked + label {
color: #555;
border: 1px solid #ddd;
border-top: 2px solid orange;
border-bottom: 1px solid #fff;
}
#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4 {
display: block;
}
p, h5, li, h2, h4, div{
line-height: 200%;
font-family: 'Montserrat', sans-serif;
}
td{
font-family: 'Montserrat';
}
.picture {
width: 35%;
float: left;
display: inline-block;
}
#desc{
width: 48%;
display: inline-block; 
margin-left: 2rem;
}
@media screen and (max-width: 280px) {
label {
padding: 15px 25px;
display: inline-block;
margin: 0 0 -1px;
font-weight: 600;
font-size: xx-small;
border: 1px solid transparent;
}
section {
display: none;
padding: 20px 25px 1px 1px;
border-top: 1px solid #ddd;
font-size: x-small;
}
.picture{
float: none;
width: 100%;
display: inline-block;
}
#desc{
width: 82%;
display: inline-block; 
margin-left: 2rem;
margin-top: 2rem;
}
}
@media screen and (min-width: 281px) and (max-width: 480px) {
label {
padding: 15px 25px;
display: inline-block;
margin: 0 0 -1px;
font-weight: 600;
font-size: small;
border: 1px solid transparent;
}
.picture{
float: none;
width: 100%;
display: inline-block;
}
#desc{
width: 89%;
display: inline-block; 
margin-left: 2rem;
margin-top: 2rem;
}
}
@media screen and (min-width: 821px) and (max-width: 1388px){
label {
padding: 15px 25px;
display: inline-block;
margin: 0 0 -1px;
font-weight: 600;
font-size: small;
border: 1px solid transparent;
}
.picture{
float: left;
width: 42%;
display: inline-block;
}
#desc{
width: 48%;
margin-left: 2rem;
display: inline-block;
}
}
@media screen and (min-width: 541px) and (max-width: 820px){
label {
padding: 15px 25px;
display: inline-block;
margin: 0 0 -1px;
font-weight: 600;
font-size: small;
border: 1px solid transparent;
}
.picture{
float: left;
width: 47%;
display: inline-block;
}
#desc{
width: 48%;
margin-left: 2rem;
display: inline-block; 
}
}
@media screen and (min-width: 481px) and (max-width: 540px){
label {
padding: 15px 25px;
display: inline-block;
margin: 0 0 -1px;
font-weight: 600;
font-size: small;
border: 1px solid transparent;
}
.picture{
text-align: center;
width: 100%;
display: inline-block;
}
#desc{
width: 90%;
margin-left: 2rem;
margin-top: 2rem;
display: inline-block; 
}
}
</style>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <img src='https://intranet.unixsurplus.com/resources/img/PlusDrives_Banner1.png' alt='Banner'> 
</head>
<body>
<div style="text-align: center; margin: 10px 8px 58px 0px">
<strong><span class="title_preview" style="font-size: 200%"></span></strong>
</div> <br>
<div style="width: 100%; margin-right: 2rem;">
<div class="picture">
<input type="image" id="image_preview_html" style="height:470px; width:430px;"></div><div id="desc"><h5 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-family: inherit; font-weight: 500; line-height: 1.2; color: orange; font-size: 1.25rem;">TECHNICAL SPECIFICATIONS</h5>
<span style="color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;;
font-size: 16px; background-color: rgb(255, 255, 255);">
</span>

<div style="width: 95%; display: inline-block;">
<table class="table" style="border-collapse: collapse; width: 100%; margin-bottom: 1rem; color: rgb(33, 37, 41); background-color: rgb(255, 255, 255); font-size: 16px;">
<tbody style="box-sizing: border-box;">
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Brand:</span>
</td>
<td class="brand_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Model:</span>
</td>
<td class="model_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Application:</span>
</td>
<td class="application_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Form Factor:</span>
</td>
<td class="ff_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Capacity:</span>
</td>
<td class="capacity_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Interface:</span>
</td>
<td class="interface_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Condition:</span>
</td>
<td class="condition_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Warranty:</span>
</td>
<td class="warranty_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Description:</span>
</td>
<td class="description_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div style="width: 100%; margin-right: 2rem;"><div style="width: 48%; display: inline-block; margin-left: 2rem;"><div style="width: 95%; display: inline-block;"><br></div></div></div><div style="width: 100%; margin-right: 2rem;"><div style="width: 48%; display: inline-block; margin-left: 2rem;"><div style="width: 95%; display: inline-block;"><br></div></div></div>
<div><input id="tab1" type="radio" name="tabs" checked="" style="display:none;"><label for="tab1">About Us</label><input id="tab2" type="radio" name="tabs" style="display:none;"><label for="tab2">Warranty</label><input id="tab3" type="radio" name="tabs" style="display:none;"><label for="tab3">Shipping</label><section id="content1">
<h2>About Us</h2><h5>We know our customers rely on our equipment, so quality assurance is our top priority. Every single Hard Drive is tested by experienced technicians.</h5>
<h4>SERVICE</h4><p>Our business is taking care of you. Our tailored solutions are created to help you reach your goals by fitting you with the most cost efficient solutions while providing the best service in the industry when it comes to basics like.</p>
<ul><li>Fast Shipping</li><li>Packaged by Experts</li><li>Extended Warranties</li><li>No Hassle Warranty Claims</li><li>Discounts on large Quantity Purchases</li><li>OEM and Compatible products</li><li>Tested and Reliable</li></ul>
<h4>FLEXIBILITY</h4><p>Our Hard Drive solutions are tailored to meet your needs. Our technical sales staff understand real world implementations. We’ll make your life easier by offering the best value proposition. </p>
<h4>PARTNERSHIP</h4><p>We can’t do it all. We have strategic partnerships with some of the largest hardware manufacturers and software developers in the industry. Our relationship allows for access to source support and product information.</p>
</section>
<section id="content2">
<h2>Warranty</h2>
<p>100% Guaranteed Working Condition w/ Warranty. Ships from the USA!</p><p>All Hard Drives have a 30 day warranty unless otherwise stated.</p>
<h2>Return</h2>
<p>After receiving the item, contact seller within 30 Days</p><p>Refund will be given as Money Back, Replacement</p><p>Seller pays for return shipping </p>
</section>
<section id="content3">
<h2>Shipping</h2>
<p>Free 2 day shipping</p><p>We understand the importances of fast reliable delivery, and we prioritize getting you tracking information within a couple hours of purchase.</p>
<p>Will usually ship within 1 business day of receiving cleared payment.</p>
</section>
</div>
</div></div>
