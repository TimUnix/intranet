<div class="modal fade" id="generateTemplate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div id="validate_template"></div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" id="title_template">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="capacity">Capacity:</label>
                        <input list="capacity_templates" class="form-control" id="capacity_template">
                     <!--   <select id = "capacity_template" class ="form-control"> -->
                        <datalist id="capacity_templates">
                        <?php foreach($data['capacity_template'] as $capacity): ?>
                            <option value="<?= $capacity->name ?>"></option>
                            <?php endforeach; ?>
                        </datalist>
                    </div>
                    <div class="col-md-3">
                        <label for="form_factor">Form Factor:</label>
                     <!--   <select id = "form_factor_template" class ="form-control"> -->
                     <input list="form_factor_templates" class="form-control" id="form_factor_template">
                     <datalist id="form_factor_templates">
                        <?php foreach($data['form_factor_template'] as $form_factor): ?>
                            <option value="<?= $form_factor->name ?>"><?= $form_factor->name ?></option>
                            <?php endforeach; ?>
                        </datalist>
                    </div>
                    <div class="col-md-6">
                        <label for="Model">Model:</label>
                        <input type="text" class="form-control" id="model_template" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <label for="interface">Interface:</label>
                    <input list="interface_templates" class="form-control" id="interface_template">
                     <datalist id="interface_templates">
                    <!--<select id = "interface_template" class ="form-control">-->
                        <?php foreach($data['interface_template'] as $interface): ?>
                            <option value="<?= $interface->name ?>"><?= $interface->name ?></option>
                            <?php endforeach; ?>
                        </datalist>
                    </div>
                    <div class="col-md-6">
                    <label for="condition">Condition:</label>
                    <!--<select id = "condition_template" class ="form-control">-->
                    <input list="condition_templates" class="form-control" id="condition_template">
                     <datalist id="condition_templates">
                        <?php foreach($data['condition_template'] as $condition): ?>
                            <option value="<?= $condition->name ?>"><?= $condition->name ?></option>
                            <?php endforeach; ?>
                        </datalist>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <label for="warranty">Warranty:</label>
                    <!--<select id = "warranty_template" class ="form-control">--> 
                    <input list="warranty_templates" type="text" class="form-control" id="warranty_template" disabled/>
						<label class="warranty-value"for="warranty_template" style="position:absolute; top:50px;">
							30 Days Seller Warranty</label> 
                    </div>
                    <div class="col-md-6">
                    <label for="brand">Brand:</label>
                    <input type="text" class="form-control" id="brand_template" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="description">Description:</label>
                        <textarea class="form-control" row="2" id="description_template"></textarea>
                    </div>
                </div><br>
                <div class="row html-box" style="display:none;">
                    <div class="col-md-1">
                        <button class="btn btn-primary" type="button" id="button_copy" style="height:61px">Copy</button>
                    </div>
                    <div class="col-md-11">
                        <textarea class="form-control" row="2" id="copy_html_template"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="generate_template_submit">Generate HTML</button>
                <button type="button" class="btn btn-primary" id="preview_template_submit">Preview Template</button>
            </div>  
        </div>
    </div>
</div>
