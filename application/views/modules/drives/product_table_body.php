<?php if(!empty($items)) { foreach($items as $row_index => $item): ?>
    <?php $index = $offset + $row_index; ?>
	<tr>
		<td scope="col"><?= $index + 1 ?></td>
		<td scope="col" class="sku item-sku<?= isset($columns->sku->is_shown) 
                        ? ($columns->sku->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->sku ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel">
                <i class="far fa-file-alt templatepenbtn" aria-hidden="true"></i>
            </button>
        <button type="button" class="btn btn-link text-primary draftid-cancel skubtn">
                <i class="fa fa-edit skupenbtn" aria-hidden="true"></i>
            </button>

        <?= $item->sku ?></td>
		<td scope="col" class=" bar item-bar<?= isset($columns->bar->is_shown) 
                        ? ($columns->bar->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->bar ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel barbtn">
                <i class="fa fa-edit barpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->bar ?></td>
        <td scope="col" class="upc item-upc<?= isset($columns->upc->is_shown) 
                        ? ($columns->upc->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->upc ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel upcbtn">
                <i class="fa fa-edit upcpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->upc ?></td>
		<td scope="col" class="brand item-brand<?= isset($columns->brand->is_shown) 
                        ? ($columns->brand->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->brand ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel brandbtn">
                <i class="fa fa-edit brandpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->brand ?></td>
		<td scope="col" class="ff item-ff<?= isset($columns->ff->is_shown) 
                        ? ($columns->ff->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->ff ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel ffbtn">
                <i class="fa fa-edit ffpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->ff ?></td>
		<td scope="col" class="size item-size<?= isset($columns->size->is_shown) 
                        ? ($columns->size->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->size ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel sizebtn">
                <i class="fa fa-edit sizepenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->size ?></td>
		<td scope="col" class="type item-type<?= isset($columns->type->is_shown) 
                        ? ($columns->type->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->type ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel typebtn">
                <i class="fa fa-edit typepenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->type ?></td>
		<td scope="col" class="rpm item-rpm<?= isset($columns->rpm->is_shown) 
                        ? ($columns->rpm->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->rpm ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel rpmbtn">
                <i class="fa fa-edit rpmpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->rpm ?></td>
		<td scope="col" class="speed item-speed<?= isset($columns->speed->is_shown) 
                        ? ($columns->speed->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->speed ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel speedbtn">
                <i class="fa fa-edit speedpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->speed ?></td>
		<td scope="col" class="interface item-interface<?= isset($columns->interface->is_shown) 
                        ? ($columns->interface->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->interface ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel interfacebtn">
                <i class="fa fa-edit interfacepenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->interface ?></td>
		<td scope="col" class="series item-series<?= isset($columns->series->is_shown) 
                        ? ($columns->series->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->interface ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel seriesbtn">
                <i class="fa fa-edit seriespenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->series ?></td>
		<td scope="col" class="con item-con<?= isset($columns->con->is_shown) 
                        ? ($columns->con->is_shown ? '' : ' d-none') : '' ?>" nowrap nowrap data-value="<?= $item->con ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel conbtn">
                <i class="fa fa-edit conpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->con ?></td>

	</tr>

<?php endforeach; }?>