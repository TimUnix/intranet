<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td style = "display: none;" scope="col" class="item-id<?= isset($item->id->is_shown) 
                        ? ($item->id->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->id ?>"><?= $item->id ?></td>
        <td style = "display: none;" scope="col" class="item-email<?= isset($item->email->is_shown) 
                        ? ($item->email->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->email ?>"><?= $item->email ?></td>
        <td scope="col" data-value="<?= $item->ein ?>" class="item-ein"><?= $item->ein ?></td>
        <td scope="col" data-value="<?= $item->firstname ?><?= $item->lastname ?>" class="item-employee"><?= $item->firstname ?> <?= $item->lastname ?></td>
        <td style="display: none;" data-value="<?= $item->picture ?>" class="item-picture"><?= $item->picture ?></td>
        <td scope="col" data-value="<?= $item->contact_number ?>" class="item-contact_number"><?= $item->contact_number ?></td>
        <td scope="col" data-value="<?= $item->birthday ?>" class="item-birthday"><?= $item->birthday ?></td>
        <td scope="col" data-value="<?= $item->address ?>" class="item-address"><?= $item->address ?></td>
        <td scope="col" data-value="<?= $item->start_date ?>" class="item-start_date"><?= $item->start_date ?></td>
        <td scope="col" data-value="<?= $item->position ?>" class="item-position"><?= $item->position ?></td>
        <td scope="col" data-value="<?= $item->manager ?>" class="item-manager"><?= $item->manager ?></td>
        <td scope="col" data-value="<?= $item->department ?>" class="item-department"><?= $item->department ?></td>
        <td scope="col" data-value="<?= $item->location ?>" class="item-location"><?= $item->location ?></td>
        <td scope="col" data-value="<?= $item->extension ?>" class="item-ext"><?= $item->extension ?></td>
        <td scope="col" data-value="<?= $item->day_start ?>" class="item-day_start"><?= $item->day_start ?></td>
        <td scope="col" data-value="<?= $item->day_end ?>" class="item-day_end"><?= $item->day_end ?></td>
        <td scope="col" data-value="<?= $item->schedule_in ?>" class="item-schedule_in"><?= $item->schedule_in ?></td>
        <td scope="col" data-value="<?= $item->schedule_out ?>" class="item-schedule_out"><?= $item->schedule_out ?></td>
        <td scope="col" class="text-center">
            <button type="button" class="btn btn-link btn-lg edit-item" data-toggle="modal" data-target="#usersModal">
                <i class="fas fa-pen edit-item" aria-hidden="true"></i>
            </button> 
            <button type="button" class="btn btn-link btn-lg deleterow" data-toggle="modal" data-target="#confirmDelete">
                <i class="fas fa-trash" aria-hidden="true"></i>
            </button>

           
            <form action="<?= base_url() ?>index.php/users_profile/generate_PDF" method="POST" id="PDFform">
				<input type='hidden' name='form-id' id='form-id' value="<?= $item->id ?>" required/>
				<input type='hidden' name='form-email' id='form-email' value="<?= $item->email ?>" required/>

				<button type="submit" class="btn btn-link btn-lg generatePDF"
                        id="generatePDF"  on-click="generate_PDF()">
                    <i class="fa fa-print" aria-hidden="true"></i>
                </button>
            </form>
        </td>
    </tr>

<?php endforeach; } ?>

<!-- display data from db -->
