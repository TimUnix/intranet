<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<div class="container-fluid" style="padding-bottom: 60px;">

	<div class="modal fade" id="usersModal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<div id="id" style="display:none;"></div>
					<div id="email" style="display:none;"></div>

					<div class="row" style="text-align:center;">
						<div class="col-md-12" >
							<img id="users_picture" class="users_profile-img rounded-circle" src="" style="width: 10rem;">
						</div>
					</div>
					<div class="row" style="text-align:center;">
						<div class="col-md-12" style="margin-top:1rem;">
							<span id="employee" class="employee-name" style="font-size:1.5rem; font-weight:bold;"></span>
						</div>
					</div>
					<div class="row" style="text-align:center;">
						<div class="col-md-12" style="margin-bottom:2rem;">
							<span id="emailtext" class="email" style="font-size:1rem;"></span>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div>
								<label for="ein">EIN</label>
								<input id="ein" type="text" class="form-control form-control-sm">
							</div>
							<div>
								<label for="contact_number">Contact Number</label>
								<input id="contact_number" type="text" class="form-control form-control-sm">
							</div>
							<div>
								<label for="address">Address</label>
								<input id="address" type="text" class="form-control form-control-sm">
							</div>
							<div>
								<label for="birthday">Birthday</label>											
								<div class="input-group date" id="birthday" data-target-input="nearest">
									<input type="text" id="birthday" class="form-control form-control-sm datetimepicker-input" placeholder="Select Date" data-target="#birthday" />
									<div class="input-group-append" data-target="#birthday" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>	
							</div>	
							<div>
								<label for="start_date">Start Date</label>											
								<div class="input-group date" id="start_date" data-target-input="nearest">
									<input type="text" id="start_date" class="form-control form-control-sm datetimepicker-input" placeholder="Select Date" data-target="#start_date" />
									<div class="input-group-append" data-target="#start_date" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>	
							</div>
					
							<div class="row">
							<div class=col>
								<label for="daystart">Day Start</label>
								<select id="daystart" class="form-control form-control-sm" >
								<option>Monday</option>
								<option>Tuesday</option>	
								<option>Wednesday</option>
								<option>Thursday</option>
								<option>Friday</option>
								</select>
							</div>
							<div class=col>
								<label for="dayend">Day End</label>
								<select id="dayend" class="form-control form-control-sm" >
								<option>Monday</option>
								<option>Tuesday</option>	
								<option>Wednesday</option>
								<option>Thursday</option>
								<option>Friday</option>
								</select>
							</div>
							</div>
						</div>
				
					
						<div class="col">
						<div>
								<label for="position">Position</label>
								<input type="text" class="form-control form-control-sm" id="position" >
						</div>
						<div>
							<label for="managers" >Manager</label>
							<select id="managers" class="form-control form-control-sm" >
								<?php foreach ($data['managers'] as $manager): ?>
								<option value="<?= $manager->firstname ?> <?= $manager->lastname ?>">
									<?= $manager->firstname ?> <?= $manager->lastname ?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div>
							<label for="departments" >Department</label>
							<select id="departments" class="form-control form-control-sm" >
							<option>Operations</option>
							<option>Marketing</option>	
							<option>HR</option>
							<option>Inventory</option>
							<option>Tech</option>
							<option>IT</option>
							<option>Warehouse</option>
							<option>Accounting</option>
							<option>Customer Support</option>
							<option>Software</option>
							<option>Sales</option>
							<option>Shipping</option>
							<option>Recieving</option>
							</select>
						</div>

						<div>
							<label for="locations">Location</label>
							<select id="locations" class="form-control form-control-sm">
							<option>China</option>
							<option>Merced</option>	
							<option>Santa Clara</option>
							<option>Philippines</option>
							</select>
						</div>

						<div>
							<label for="ext" >Extension</label>
							<input type="text" class="form-control form-control-sm" id="ext" >
						</div>
						</div>
					</div>
									
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="submit">Submit</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="new" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
				<label for="email">Email</label>
				<input type="text" class="form-control" id="newemail">
				</div>
				<div class="modal-footer" style="text-align:center;">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="submitf">Submit</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="confirmDelete" role="dialog">
    <div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header"><h4>Delete<h4></div>
            <div class="modal-body">
			<div id="email" style="display:none;"></div>
                <div style="text-align:center;">    
				<i class="fa fa-question-circle" aria-hidden="true" style="font-size:48px;color:red"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h3>Are you sure you want to proceed?</h3></strong><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="deleteuser">Confirm</button>
				</div>
        </div>
    </div>
	</div>
	
	<div class="card">
                        <div class="card-header ">
                            <div class="card-title">
                                Search
                        </div>
                        </div>
                    <div class="card-body">
                        <form id="searchForm" onsubmit="return false">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <div class="row">
                                        <div class="col input-group input-group-lg">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-lg btn-primary" type="button" id="searchColumn">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <input class="form-control input-lg" type="search" id="itemSearch"
                                                placeholder="Search" required>
                                            </div>  
                                </div>
                            </div>
                            </div>
                        </form>
                    </div> 
                </div>
            </div>

	<div class="card" id="user_table">
		<div class="card-header">
		<div class="card-tools">
				<div style="padding-left:5px;">
			 	<button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
				</div>
		</div>
		</div>
		<div class="horizontal-scroller">
			<div class="scroller-content"></div>
		</div>	
		<div class="card-body sticky-headers p-0 d-none" style="z-index: 2;">
			<table class="table table-bordered table-striped table-hover sticky-table"
				style="border-collapse: separate;" cellspacing="0">
				<thead class="bsticky-thead bg-white">
					<tr class="sticky-header-row">
						<th scope="col">&#35;</th>
						<th scope="col" style="text-align: center;" class="header-ein<?= isset($data['columns']->ein->is_shown) ? ($data['columns']->ein->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>EIN</th>
						<th scope="col" style="text-align: center;" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Employee</th>
						<th scope="col" style="text-align: center;" class="header-contact_number<?= isset($data['columns']->contact_number->is_shown) ? ($data['columns']->contact_number->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Contact Number</th>
						<th scope="col" style="text-align: center;" class="header-birthday<?= isset($data['columns']->birthday->is_shown) ? ($data['columns']->birthday->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Birthday</th> 
						<th scope="col" style="text-align: center;" class="header-address<?= isset($data['columns']->address->is_shown) ? ($data['columns']->address->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Address</th>   
						<th scope="col" style="text-align: center;" class="header-start_date<?= isset($data['columns']->start_date->is_shown) ? ($data['columns']->start_date->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Start Date</th> 
						<th scope="col" style="text-align: center;" class="header-position<?= isset($data['columns']->position->is_shown) ? ($data['columns']->position->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Position</th>
						<th scope="col" style="text-align: center;" class="header-manager<?= isset($data['columns']->manager->is_shown) ? ($data['columns']->manager->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Manager</th>
						<th scope="col" style="text-align: center;" class="header-department<?= isset($data['columns']->department->is_shown) ? ($data['columns']->department->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Department</th>
						<th scope="col" style="text-align: center;" class="header-location<?= isset($data['columns']->location->is_shown) ? ($data['columns']->location->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Location</th>
						<th scope="col" style="text-align: center;" class="header-ext<?= isset($data['columns']->ext->is_shown) ? ($data['columns']->ext->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Extension</th>
						<th scope="col" style="text-align: center;" class="header-day_start<?= isset($data['columns']->day_start->is_shown) ? ($data['columns']->day_start->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Day Start</th>
						<th scope="col" style="text-align: center;" class="header-day_end<?= isset($data['columns']->day_end->is_shown) ? ($data['columns']->day_end->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Day End</th>
						<th scope="col" style="text-align: center;" class="header-schedule_in<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Schedule In</th>  
						<th scope="col" style="text-align: center;" class="header-schedule_out<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Schedule Out</th>  
						<th scope="col" style="text-align: center;" class="header-modify<?= isset($data['columns']->modiify->is_shown) ? ($data['columns']->modify->is_shown ? '' : ' d-none') 
															: '' ?>"nowrap>Modify</th>  
					</tr>
				</thead>
			</table>
		</div>
		<div class="card-body p-0 table-container table-responsive">
			<table class="table table-bordered table-striped table-hover main-table" id="users_profs"
					style="border-collapse: separate;" cellspacing="0">
				<thead class="main-thead bg-white border">
					<tr class="main-header-row">
		            	<th scope="col">&#35;</th>
						<th scope="col" style="text-align: center;" class="header-ein<?= isset($data['columns']->ein->is_shown) ? ($data['columns']->ein->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>EIN</th>
		            	<th scope="col" style="text-align: center;" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Employee</th>
		            	<th scope="col" style="text-align: center;" class="header-contact_number<?= isset($data['columns']->contact_number->is_shown) ? ($data['columns']->contact_number->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Contact Number</th>
	                    <th scope="col" style="text-align: center;" class="header-birthday<?= isset($data['columns']->birthday->is_shown) ? ($data['columns']->birthday->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Birthday</th> 
	                    <th scope="col" style="text-align: center;" class="header-address<?= isset($data['columns']->address->is_shown) ? ($data['columns']->address->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Address</th>   
						<th scope="col" style="text-align: center;" class="header-start_date<?= isset($data['columns']->start_date->is_shown) ? ($data['columns']->start_date->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Start Date</th> 
						<th scope="col" style="text-align: center;" class="header-position<?= isset($data['columns']->position->is_shown) ? ($data['columns']->position->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Position</th>
						<th scope="col" style="text-align: center;" class="header-manager<?= isset($data['columns']->manager->is_shown) ? ($data['columns']->manager->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Manager</th>
						<th scope="col" style="text-align: center;" class="header-department<?= isset($data['columns']->department->is_shown) ? ($data['columns']->department->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Department</th>
						<th scope="col" style="text-align: center;" class="header-location<?= isset($data['columns']->location->is_shown) ? ($data['columns']->location->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Location</th>
						<th scope="col" style="text-align: center;" class="header-ext<?= isset($data['columns']->ext->is_shown) ? ($data['columns']->ext->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Extension</th>
						<th scope="col" style="text-align: center;" class="header-day_start<?= isset($data['columns']->day_start->is_shown) ? ($data['columns']->day_start->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Day Start</th>
						<th scope="col" style="text-align: center;" class="header-day_end<?= isset($data['columns']->day_end->is_shown) ? ($data['columns']->day_end->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Day End</th>
						<th scope="col" style="text-align: center;" class="header-schedule_in<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Schedule In</th>  
						<th scope="col" style="text-align: center;" class="header-schedule_out<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Schedule Out</th>  
						<th scope="col" style="text-align: center;" class="header-modify<?= isset($data['columns']->modiify->is_shown) ? ($data['columns']->modify->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Modify</th>  
		            </tr>
		        </thead>
		        <tbody id="users_profile_body">
	                <tr>
	                    <td colspan="100%">Search an item to see results here.</td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>
</div>
    <!--show data-->
<script id="usersProfileScript" data-base-url="<?= base_url() ?>" type="text/javascript"
    src="<?= base_url() ?>resources/js/users_profile/users_profile.js"></script>
<?php $this->load->view('templates/common/footer'); ?>
