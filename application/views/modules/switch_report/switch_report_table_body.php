<?php if(!empty($items)) { foreach($items as $row_index => $item): ?>
    <?php $index = $offset + $row_index; ?>

	<tr class = "myrow" style="background-color : #00000 ">
		<td data-id="<?= $item->id ?>" class="idnum" style="width:20px;" scope="col"><?= $index + 1 ?></td>
		<td style="max-width:200px; white-space: normal; word-wrap:break-word;" scope="col" class="eqm_mpn item-eqm_mpn<?= isset($columns->eqm_mpn->is_shown) 
                        ? ($columns->eqm_mpn->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->eqm_mpn ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel eqm_mpnbtn" id="eqm_mpnbtn">
                    <i class="fa fa-edit fa-sm eqm_mpnpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->eqm_mpn ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="eqm_des item-eqm_des<?= isset($columns->eqm_des->is_shown) 
                        ? ($columns->eqm_des->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->eqm_des ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel eqm_desbtn">
                    <i class="fa fa-edit fa-sm eqm_despenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->eqm_des ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="data_dest item-data_dest<?= isset($columns->data_dest->is_shown) 
                        ? ($columns->data_dest->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->data_dest ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel data_destbtn">
                    <i class="fa fa-edit fa-sm data_destpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->data_dest ?></td>

		<td style="max-width:80px; white-space: normal; word-wrap:break-word;" scope="col" class="qty item-qty<?= isset($columns->qty->is_shown) 
                        ? ($columns->qty->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->qty ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel qtybtn">
                <i class="fa fa-edit fa-sm qtypenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->qty ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="boxed item-boxed<?= isset($columns->boxed->is_shown) 
                        ? ($columns->boxed->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->boxed ?>">

                <button type="button" class="btn btn-link text-primary draftid-cancel boxedbtn">
                    <i class="fa fa-edit fa-sm boxedpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->boxed ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="despostion item-despostion<?= isset($columns->despostion->is_shown) 
                        ? ($columns->despostion->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->despostion ?>">
           
                <button type="button" class="btn btn-link text-primary draftid-cancel despostionbtn">
                    <i class="fa fa-edit fa-sm despostionpenbtn" aria-hidden="true"></i>
                </button>
           
        <?= $item->despostion ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="serial item-serial<?= isset($columns->serial->is_shown) 
                        ? ($columns->serial->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->serial ?>">
              <button type="button" class="btn btn-link text-primary draftid-cancel serialbtn">
                    <i class="fa fa-edit fa-sm serialpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->serial ?></td>

		<td style="max-width:200px; white-space: normal; word-wrap:break-word;" scope="col" class="notes item-notes<?= isset($columns->notes->is_shown) 
                        ? ($columns->notes->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->notes ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel notesbtn">
                    <i class="fa fa-edit fa-sm notespenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->notes ?></td>

		<td style="max-width:50px; white-space: normal; word-wrap:break-word;" scope="col" class="recieved item-recieved<?= isset($columns->recieved->is_shown) 
                        ? ($columns->recieved->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->recieved ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel recievedbtn">
                    <i class="fa fa-edit fa-sm recievedpenbtn" aria-hidden="true"></i>
                </button>
        <?= $item->recieved ?></td>

		


	</tr>

<?php endforeach; }?>