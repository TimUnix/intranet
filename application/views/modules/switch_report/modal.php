<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
				<form>
                <div class="row">
                
                <div class="col-md-6" >
                    <label for="eqm_mpn">Equipment MPN/Type</label>
                    <input  type="text" class="form-control" id="eqm_mpn" list="skuSuggestions" required/>
                    <datalist id="skuSuggestions"></datalist>
                </div>
                <div class="col-md-6">
                    <label for="eqm_des">Equipment Description (MFG)</label>
                    <input type="text" class="form-control" id="eqm_des">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="data_dest">Data Destruction</label>
                    <select class="form-control" id="data_dest">
                    <option value="Yes">Yes</option>
                    <option value="No">No</option></select>
                </div>
                <div class="col-md-6">
                    <label for="qty">Qty</label>
					<input type="number" class="form-control" id="qty">
                </div>
                </div>
                <div class="row">
                 
                <div class="col-md-6">
                    <label for="boxed">Boxed</label>
                    <select class="form-control" id="boxed">
                    <option value="Yes">Yes</option>
                    <option value="No">No</option></select>
                </div>
                <div class="col-md-6">
                    <label for="despostion">Desposition</label>
					<input type="text" class="form-control" id="despostion">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <div>
                        <label for="serial">Serial #</label>
                        <input type="text" id="serial" class="form-control">
                    </div>
                    <div>
                    <label for="recieved">Recieved</label>
                    <input type="text" id="recieved" class="form-control">
                    </div>
                    </div>
                    <div class="col-md-6">
                        <label for="notes">Notes</label>
                        <textarea id="notes" name="notes" rows="4" cols="50"></textarea>
                    </div>
                </div>            
               
               
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitf">Submit</button>
            </div>
				</form>
        </div>
    </div>
</div>