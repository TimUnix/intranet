<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<div id="load"></div>

<div class="container-fluid" style="padding-bottom: 60px;">
            <div class="card">
                        <div class="card-header ">
                            <div class="card-title">
                                Search
                        </div>
                        </div>
                    <div class="card-body">
                        <form id="searchForm">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group input-group-lg">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-lg btn-primary" type="button" id="searchColumn">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <input class="form-control input-lg" type="search" id="itemSearch"
                                                placeholder="Search" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
            </div>
        <div class="card" id="switchreport_table">
            <div class="card-header">  
                    <button id="insertListing" type="button" class="btn btn-sm btn-outline-primary ret" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        Create New
                    </button>
            </div>
            <div class="horizontal-scroller">
                <div class="scroller-content"></div>
            </div>
            <div class="card-body sticky-headers p-0 d-none" style="z-index: 2;">
                <table class="table table-bordered table-striped table-hover sticky-table"
                    style="border-collapse: separate;" cellspacing="0">
                    <thead class="bsticky-thead bg-white">
                        <tr class="sticky-header-row">
                            <th scope="col" nowrap>&#35;</th>
                            <th scope="col" class="header-modelnum<?= isset($data['columns']->modelnum->is_shown) ? ($data['columns']->modelnum->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Equipment MPN/Type</th>
                            <th scope="col" class="header-qty<?= isset($data['columns']->qty->is_shown) ? ($data['columns']->qty->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Equipment Description (MFG)</th>
                            <th scope="col" class="header-numports<?= isset($data['columns']->numports->is_shown) ? ($data['columns']->numports->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Data Destruction</th>
                            <th scope="col" class="header-pspeed<?= isset($data['columns']->pspeed->is_shown) ? ($data['columns']->pspeed->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Qty</th>
                            <th scope="col" class="header-numupports<?= isset($data['columns']->numupports->is_shown) ? ($data['columns']->numupports->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Boxed</th>
                            <th scope="col" class="header-uplinkspeed<?= isset($data['columns']->uplinkspeed->is_shown) ? ($data['columns']->uplinkspeed->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Desposition</th>
                            <th scope="col" class="header-poewatts<?= isset($data['columns']->poewatts->is_shown) ? ($data['columns']->poewatts->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Serial #</th>
                            <th scope="col" class="header-psuqty<?= isset($data['columns']->psuqty->is_shown) ? ($data['columns']->psuqty->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Notes</th>
                            <th scope="col" class="header-psupn<?= isset($data['columns']->psupn->is_shown) ? ($data['columns']->psupn->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Recieved</th>
                         
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="card-body p-0 table-container table-responsive">
			<table class="table table-bordered table-striped table-hover main-table" id="product-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="main-thead bg-white border">
					<tr class="main-header-row">
						<th scope="col" nowrap>&#35;</th>
                        <th scope="col" class="header-modelnum<?= isset($data['columns']->modelnum->is_shown) ? ($data['columns']->modelnum->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Equipment MPN/Type</th>
                            <th scope="col" class="header-qty<?= isset($data['columns']->qty->is_shown) ? ($data['columns']->qty->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Equipment Description (MFG)</th>
                            <th scope="col" class="header-numports<?= isset($data['columns']->numports->is_shown) ? ($data['columns']->numports->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Data Destruction</th>
                            <th scope="col" class="header-pspeed<?= isset($data['columns']->pspeed->is_shown) ? ($data['columns']->pspeed->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Qty</th>
                            <th scope="col" class="header-numupports<?= isset($data['columns']->numupports->is_shown) ? ($data['columns']->numupports->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Boxed</th>
                            <th scope="col" class="header-uplinkspeed<?= isset($data['columns']->uplinkspeed->is_shown) ? ($data['columns']->uplinkspeed->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Desposition</th>
                            <th scope="col" class="header-poewatts<?= isset($data['columns']->poewatts->is_shown) ? ($data['columns']->poewatts->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Serial #</th>
                            <th scope="col" class="header-psuqty<?= isset($data['columns']->psuqty->is_shown) ? ($data['columns']->psuqty->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Notes</th>
                            <th scope="col" class="header-psupn<?= isset($data['columns']->psupn->is_shown) ? ($data['columns']->psupn->is_shown ? '' : ' d-none') 
                                                        : '' ?>"nowrap>Recieved</th>
                            
					</tr>
				</thead>
				<tbody id="switch_report_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
			</table>
		</div>
	</div>
    </div>
</div>

    <!--show data-->

    
<?php $this->load->view('templates/common/spinner'); ?>
<?php $this->load->view('modules/switch_report/modal'); ?>
<script id="switches_reportScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/switch_report/switches.js"></script>
<script id="switch_edit_Script" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/switch_report/switch_report_edit.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>

<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
