<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<button id="insertListing" type="button" class="btn btn-info btn-lg ml-2 mb-4" data-toggle="modal"
        data-target="#myModal">
    Create New Memory
</button>
<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-left">
                <h3 class="modal-title w-100">New</h3>
            </div>
            <div class="modal-body">
                <div id="alert"></div>
                <div class="form-group">
                    <label for="request">Sku</label>
                    <input type="text" class="form-control" id="sku">
                </div>

                <div class="form-group" novalidate>
                    <label for="sku">Barcode</label>
                    <input type="text" class="form-control" id="barcode">
                </div>
                <div class="form-group">
                    <label for="channel">Brand</label>
                    <input type="text" class="form-control" id="brand">
                </div>
                <div class="form-group">
                    <label for="channel">Condition</label>
                    <input type="text" class="form-control" id="con">
                </div>
                <div class="form-group">
                    <label for="weight">Memory size</label>
                    <input type="text" class="form-control" id="memory_size">
                </div>
                <div class="form-group">
                    <label for="shipping">Memory family</label>
                    <input type="text" class="form-control" id="memory_family">
                </div>
                <div class="form-group">
                    <label for="moved">Memory type</label>
                    <input type="text" class="form-control" id="memory_type">
                </div>
                <div class="form-group">
                    <label for="notes">Speed</label>
                    <input type="text" class="form-control" id="speed">
                </div>
                <div class="form-group">
                    <label for="price">Registry</label>
                    <input type="text" class="form-control" id="registry">
                </div>
                <div class="form-group">
                    <label for="lot">Cycles</label></br>
                    <input type="text" class="form-control" id="cycles">
                </div>
                <div class="form-group">
                    <label for="lot">Rank</label></br>
                    <input type="text" class="form-control" id="rank">
                </div>
                <div class="form-group">
                    <label for="lot">Voltage</label></br>
                    <input type="text" class="form-control" id="voltage">
                </div>
                <div class="form-group">
                    <label for="lot">Notes</label></br>
                    <input type="text" class="form-control" id="notes">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="regmem">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filtersModal" tabindex="-1" aria-labelledby="filtersModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filtersModalLabel">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body filters-container"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="applyFilters" class="btn btn-primary" data-dismiss="modal">
                    Apply Filters
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pb-5">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form id="searchForm">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="search_memory">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                
                                <input class="form-control input-lg" type="search" id="skusearch"
                                       placeholder="Search SKU" list="searchSuggestions">
                                <datalist id="searchSuggestions"></datalist>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                        <div class="form-group">
                            <label for="searchHistory">Search History:</label>
                            <select class="form-control" id="searchHistory"
                                    style="width: 100%;" 
                                    name="searchHistory[]">
                                <option value="" disabled selected>
                                    SKU
                                </option>
                                <?php foreach ($search_history as $history): ?>
                                    <option value="<?= $history->search; ?>">
                                        <?= $history->search; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                <div class="form-group col-md-2">
                    <label for="orderBy">Order By</label>
                    <select type="button" class="form-control" id="orderByitem">
                    <?php foreach ($field as $fieldname) : ?>
                            <?php if($fieldname != 'id') :  ?>
                                <option value="<?= $fieldname ?>"><?= $fieldname ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="sortOrder">Sort Order</label>
                    <select type="button" class="form-control" id="sortOrderitem">
                        <option value="ASC" selected>Ascending</option>
                        <option value="DESC">Descending</option>
                    </select>
                </div>
            </div>


            <!--<div class="row mt-4">
                <div class="col-md-1 text-center d-flex h-100">
                    <button type="button"
                            class="btn btn-primary btn-copy-textarea">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                        Copy
                    </button>
                </div>
                <div class="col-md-11">
                    <textarea class="textarea-copy form-control"
                              rows="4" name="copy-area">
                    </textarea>
                </div>
            </div>-->
        </div>
    </div>
    <div class="card">
        <div class="card-header">
                <h3 class="card-title">Items</h3>
                <div class="card-tools">
                <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-table" aria-hidden="true"></i>
                            Show/Hide Columns
                        </button>
                        <div class="dropdown-menu" id="columnDropdownSelector"
                             style="height: 200px; overflow-y: auto;">
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sku" <?= isset($data['columns']->sku->is_shown)
                                                      ? ($data['columns']->sku->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                sku
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="barcode" <?= isset($data['columns']->barcode->is_shown)
                                                  ? ($data['columns']->barcode->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                barcode
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="brand" <?= isset($data['columns']->brand->is_shown)
                                                    ? ($data['columns']->brand->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                brand
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="con" <?= isset($data['columns']->con->is_shown)
                                                    ? ($data['columns']->con->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                con
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="memory_size" <?= isset($data['columns']->memory_size->is_shown)
                                                      ? ($data['columns']->memory_size->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                                memory size
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="memory_family" <?= isset($data['columns']->memory_family->is_shown)
                                                    ? ($data['columns']->memory_family->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                memory family
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="memory_type" <?= isset($data['columns']->memory_type->is_shown)
                                                    ? ($data['columns']->memory_type->is_shown ? 'checked' : '')
                                                      : 'checked' ?>>
                                memory type
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="speed" <?= isset($data['columns']->speed->is_shown) ?
                                                      ($data['columns']->speed->is_shown ? 'checked' : '')
                                                    : 'checked' ?>>
                                speed
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="registry" <?= isset($data['columns']->registry->is_shown)
                                                       ? ($data['columns']->registry->is_shown ? 'checked' : '')
                                                         : 'checked' ?>>
                                registry
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="cycles" <?= isset($data['columns']->cycles->is_shown)
                                                     ? ($data['columns']->cycles->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                cycles
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="rank" <?= isset($data['columns']->rank->is_shown)
                                                     ? ($data['columns']->rank->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                rank
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="voltage" <?= isset($data['columns']->voltage->is_shown)
                                                     ? ($data['columns']->voltage->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                voltage
                            </div>
                            <div class="dropdown-item columnSelector">
                                <input type="checkbox" class="columnCheckbox mr-1"
                                       name="notes" <?= isset($data['columns']->notes->is_shown)
                                                     ? ($data['columns']->notes->is_shown ? 'checked' : '')
                                                       : 'checked' ?>>
                                notes
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                    <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        Export CSV
                    </button>
                    
                    
                    
                </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body table-responsive p-0">
            <table id="inventory-table" class="table paginated table-bordered table-striped table-hover w-100"
                   cellspacing="0" style="border-collapse: separate;">
                    <thead class="bg-white border">
                    <tr>
                        <th scope="col">&#35;</th>
                        <!--<th scope="col"><input type="checkbox" class="check-all-toggle"></th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-batch-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                        </th>-->
                        
                        
                        <th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown)
                                                     ? ($data['columns']->sku->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            <!--<button type="button" class="btn btn-link btn-par-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->
                            Sku
                        </th>
                        <th scope="col" class="header-barcode<?= isset($data['columns']->barcode->is_shown)
                                                     ? ($data['columns']->barcode->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            <!--<button type="button" class="btn btn-link btn-stepcode-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->
                            Barcode
                        </th>
                        <th scope="col" class="header-brand<?= isset($data['columns']->brand->is_shown)
                                                     ? ($data['columns']->brand->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            <!--<button type="button" class="btn btn-link btn-speed-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->
                            Brand
                        </th>

                        <th scope="col" class="header-con<?= isset($data['columns']->con->is_shown)
                                                     ? ($data['columns']->con->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            <!--<button type="button" class="btn btn-link btn-speed-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->
                            Condition
                        </th>
                        
                        <th scope="col" class="header-memory_size<?= isset($data['columns']->memory_size->is_shown)
                                                     ? ($data['columns']->memory_size->is_shown ? '' : ' d-none')
                                                       : '' ?>">
                            <!--<button type="button" class="btn btn-link btn-number_of_cores-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->
                            Memory Size
                        </th>
                        <th scope="col" class="header-memory_family<?= isset($data['columns']->memory_family->is_shown)
                                                     ? ($data['columns']->memory_family->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button" class="btn btn-link btn-codename-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Memory Family</th>
                        <th scope="col" class="header-memory_type<?= isset($data['columns']->memory_type->is_shown)
                                                     ? ($data['columns']->memory_type->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button" class="btn btn-link btn-thermal_design_power-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Memory Type</th>
                        <th scope="col" class="header-speed<?= isset($data['columns']->speed->is_shown)
                                                     ? ($data['columns']->speed->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button" class="btn btn-link btn-cpu_cache-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Speed</th>
                        <th scope="col" class="header-registry<?= isset($data['columns']->registry->is_shown)
                                                     ? ($data['columns']->registry->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button" class="btn btn-link btn-sockets_supported-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Registry</th>
                        <th scope="col" class="header-cycles<?= isset($data['columns']->cycles->is_shown)
                                                     ? ($data['columns']->cycles->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button"  class="btn btn-link btn-grade-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Cycles</th>
                            <th scope="col" class="header-rank<?= isset($data['columns']->rank->is_shown)
                                                     ? ($data['columns']->rank->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button"  class="btn btn-link btn-grade-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Rank</th>
                            <th scope="col" class="header-voltage<?= isset($data['columns']->voltage->is_shown)
                                                     ? ($data['columns']->voltage->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button"  class="btn btn-link btn-grade-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Voltage</th>
                            <th scope="col" class="header-notes<?= isset($data['columns']->notes->is_shown)
                                                     ? ($data['columns']->notes->is_shown ? '' : ' d-none')
                                                       : '' ?>"><!--<button type="button"  class="btn btn-link btn-grade-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Notes</th>
                            
                            <th scope="col"><!--<button type="button"  class="btn btn-link btn-grade-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>-->Delete</th>
                        
                    </tr>
                </thead>
                <tbody id="load_memory"></tbody>
                
            </table>
        </div>
    </div>
        </div>
    </div>
    
</div>

<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script src="<?php echo base_url(); ?>resources/js/memory_module/memory_module_fieldCopy.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/memory_module/memory_module_paginate.js"></script>
<script id="listingScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/memory_module/memory_module_listing.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/memory_module/memory_module_FieldEdit.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/memory_module/memory_module_search.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/memory_module/memory_module_rowInventoryCopy.js"></script>
<script id="listingFieldEditScript" data-base-url="<?= base_url(); ?>" src="<?= base_url(); ?>resources/js/memory_module/memory_module_batchinventorysearchCopy.js"></script>


<script>
    $(document).ready(function(){
 
    $('#load_memory').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
        $("#load_memory").load("<?= base_url(); ?>index.php/memory/getmemory");
            

            $("#search_memory").click(function(){
                $('#load_memory').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
        var datas = {
         sku : $("#skusearch").val(),
         sortOrder : $("#sortOrderitem").val(),
         orderBy : $("#orderByitem").val(),
         ajax : '1'
            };
         $.ajax({
         url : "<?php echo base_url();   ?>index.php/memory/get_memory_specific",
         type : "post",
         data : datas,
            success : function(msg){                         
                $("#load_memory").html(msg);
                const tableWidth = $('#product_table_body').width();
                $('.scroller-content').width(tableWidth);
            }
         });
    });


    $("#searchHistory").change(function(){
                    
        $('#load_memory').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                    
                    
         var datas = {
         sku : $("#searchHistory").val(),
         sortOrder : $("#sortOrderitem").val(),
         orderBy : $("#orderByitem").val(),
         ajax : '1'
            };
         $.ajax({
         url : "<?php echo base_url();   ?>index.php/memory/get_memory_specific",
         type : "post",
         data : datas,
            success : function(msg){                         
                $("#load_memory").html(msg);
                const tableWidth = $('#product_table_body').width();
                $('.scroller-content').width(tableWidth);
            }
         });
         });


         $("#orderByitem").change(function(){
                    
                    $('#load_memory').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                       if($('#skusearch').val() ==""){
                        var datas = {
                        sku : $("#searchHistory").val(),
                        sortOrder : $("#sortOrderitem").val(),
                        orderBy : $("#orderByitem").val(),
                        ajax : '1'
                            };
                        $.ajax({
                        url : "<?php echo base_url();   ?>index.php/memory/get_memory_specific",
                        type : "post",
                        data : datas,
                            success : function(msg){                         
                                $("#load_memory").html(msg);
                                const tableWidth = $('#product_table_body').width();
                                $('.scroller-content').width(tableWidth);
                            }
                        });
                        } else{
                            var datas = {
                            sku : $("#skusearch").val(),
                            sortOrder : $("#sortOrderitem").val(),
                            orderBy : $("#orderByitem").val(),
                            ajax : '1'
                                };
                            $.ajax({
                            url : "<?php echo base_url();   ?>index.php/memory/get_memory_specific",
                            type : "post",
                            data : datas,
                                success : function(msg){                         
                                    $("#load_memory").html(msg);
                                    const tableWidth = $('#product_table_body').width();
                                    $('.scroller-content').width(tableWidth);
                                }
                            });
                            
                        }        
                               
                     
                     });
                     $("#sortOrderitem").change(function(){
                    
                    $('#load_memory').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                       if($('#skusearch').val() ==""){
                        var datas = {
                        sku : $("#searchHistory").val(),
                        sortOrder : $("#sortOrderitem").val(),
                        orderBy : $("#orderByitem").val(),
                        ajax : '1'
                            };
                        $.ajax({
                        url : "<?php echo base_url();   ?>index.php/memory/get_memory_specific",
                        type : "post",
                        data : datas,
                            success : function(msg){                         
                                $("#load_memory").html(msg);
                                const tableWidth = $('#product_table_body').width();
                                $('.scroller-content').width(tableWidth);
                                
                                
                            }
                        });
                        } else{
                            var datas = {
                            sku : $("#skusearch").val(),
                            sortOrder : $("#sortOrderitem").val(),
                            orderBy : $("#orderByitem").val(),
                            ajax : '1'
                                };
                            $.ajax({
                            url : "<?php echo base_url();   ?>index.php/memory/get_memory_specific",
                            type : "post",
                            data : datas,
                                success : function(msg){                         
                                    $("#load_memory").html(msg);
                                    const tableWidth = $('#product_table_body').width();
                                    $('.scroller-content').width(tableWidth);
                                }
                            });
                            
                        }        
                               
                     
                     });sortOrderitem

                     $("#regmem").click(function(){
                    //$('#load_cpu').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                    var datas = {
                        sku : $("#sku").val(),
                        barcode : $("#barcode").val(),
                        brand : $("#brand").val(),
                        con : $("#con").val(),
                        memory_size : $("#memory_size").val(),
                        memory_family : $("#memory_family").val(),
                        memory_type : $("#memory_type").val(),
                        speed : $("#speed").val(),
                        registry : $("#registry").val(),
                        cycles : $("#cycles").val(),
                        rank : $("#rank").val(),
                        voltage : $("#voltage").val(),
                        notes : $("#notes").val(),
                        ajax : '1'
                        };
                    $.ajax({
                    url : "<?php echo base_url();   ?>index.php/memory/request/insertmem",
                    type : "post",
                    data : datas,
                        success : function(msg){  
                            if(msg.isSuccess == "success"){
                                location.reload();
                            }else{
                                $("#alert").html(`<div class='alert alert-${msg.isSuccess} alert-dismissible'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                ${msg.message}!
                                </div>`);
                            }                       
                            
                            //alert(msg.status)
                            console.log(msg.status)                               
                        }
                    });
                });
                     
    
  
});
    </script>
    <script>
        /*
*  Add an id of dropdownContainer to the dropdown btn-group div.
*  Add columnSelector class to each .dropdown-item div.
*  Add columnCheckbox class to each input checkbox inside the columnSelector/dropdown-item div.
*  Add class named 'header-"name of column"' to each respective table header th
*  Add class named 'title-"name of column"' to each respective table data td item field 
*/

$(document).ready(function() {
    $('#dropdownContainer .dropdown-menu').click(function(event) {
        event.stopPropagation();
    });

    $('.columnSelector').click(function() {
        const index = $('.columnSelector').index(this);
        const checkbox = $(this).find('.columnCheckbox');
        const columnName = checkbox.attr('name').toString();console.log(columnName)
        const toggleResult = !checkbox.prop('checked');
        const page = window.location.pathname.split('/').pop();
        checkbox.prop('checked', toggleResult);


        $(`.header-${columnName}`).toggleClass('d-none');
        $(`.item-${columnName}`).toggleClass('d-none');

        const display = toggleResult ? '' : 'none';
        const data = { 'column_name': columnName, 'display': display, 'page': page };
        $.post(`${baseUrl}index.php/user/save_column_state`, data);
    });
});




        </script>
