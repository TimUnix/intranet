<div class="container-fluid" style="padding-bottom: 60px;">
    
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            
                <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-lg btn-primary" id="search_item" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <input class="form-control form-control-lg" type="search" id="part_number" value="" placeholder="Part Number">
                                    </div>
                                </div>
                    </div>




                    <div class="col-md-12">
                        <div style="margin-bottom: 5px;margin-left: -7px;"> 
                            <h3>Categories</h3>
                        </div>
                    </div>



                    
                    <?php 
foreach($inventory_cat as $cat): ?>
   <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
    <div class="input-group-text">
        <input class="form-check-input" type="checkbox" id="<?php  echo $cat->cat; ?>" name="<?php  echo $cat->cat; ?>" value="<?php  echo $cat->cat; ?>">
        <label class="form-check-label" for="<?php  echo $cat->cat; ?>"><?php  echo $cat->cat; ?></label>
    </div>
</div>

  <?php endforeach; ?>


            <div class="col-md-12">
                <div style="margin-bottom: 5px;margin-left: -7px; margin-top:10px;"> 
                    <h3>Conditions</h3>
                </div>
            </div>



  
  <?php 
foreach($inventory_con as $con): ?>
<?php if($con->con == ""): ?>
    <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
    <div class="input-group-text">
        <input class="form-check-input" type="checkbox" id="<?php  echo $con->con; ?>" name="<?php  echo $con->con; ?>" value="<?php  echo $con->con; ?>">
        <label class="form-check-label" for="<?php  echo $con->con; ?>"><?php  echo "UnKnown"; ?></label>
    </div>
</div>
    <?php else:  ?>
        <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
    <div class="input-group-text">
        <input class="form-check-input" type="checkbox" id="<?php  echo $con->con; ?>" name="<?php  echo $con->con; ?>" value="<?php  echo $con->con; ?>">
        <label class="form-check-label" for="<?php  echo $con->con; ?>"><?php  echo $con->con; ?></label>
    </div>
</div>
    <?php endif; ?>
   

  <?php endforeach; ?>
  
                </div>
    </br>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="searchHistory">Search History:</label>
                            <select class="form-control" id="searchHistory"
                                    style="width: 100%;" 
                                    name="searchHistory[]">
                                <option value="" disabled selected>
                                    Part Number
                                </option>
                                <?php foreach ($search_history as $history): ?>
                                    <option value="<?= $history->search; ?>">
                                        <?= $history->search; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="sortOrder">Sort Order:</label>
                            <select class="form-control" id="sortOrder"
                                    style="width: 100%;" tabindex="-1"
                                    aria-hidden="true">
                                <option value="DESC" selected>Descending</option>
                                <option value="ASC">Ascending</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="orderBy">Order By:</label>
                            <select class="form-control" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true"
                                    id="orderBy">
                                <option value="par">Part Number</option>
                                <option value="bar">OEM PN</option>
                                <option value="pri">Price</option>
                                <option value="qty">QTY</option>
                                <option value="cat">Category</option>
                                <option value="con">Condition</option>
                                <option value="tot" selected>Total</option>
                            </select>
                        </div>
                    </div>
                </div>
            
            <div class="row mt-4">
                <div class="col-md-1 text-center d-flex h-100">
                    <button type="button"
                            class="btn btn-primary btn-copy-textarea">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                        Copy
                    </button>
                </div>
                <div class="col-md-11">
                    <textarea class="textarea-copy form-control"
                              rows="4" name="copy-area">
                    </textarea>
                </div>
            </div>




                                </br>
            <div class="row">
                    <span>
                        <strong>Items: </strong>
                        <span id="total-items"></span>
                    </span>
                    <span class="mx-3">
                        -
                    </span>
                    <span>
                        <strong>Total Price: </strong>
                        <span id="total-price"></span>
                    </span>
                </div>



                
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-outline-primary"
                        onclick="download_table_as_csv('product-table')"
                        id="exportCSV">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                
            </div>
        </div>
        <div class="card-body p-0 inventory-container table-responsive">
            <table class="table table-bordered table-striped table-hover" id="product-table">
                <thead>
                    <tr>
                        <th scope="col">&#35;</th>
                        <th scope="col"><input type="checkbox" class="check-all-toggle"></th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-batch-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                        </th>
                        
                        
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-par-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Part Number
                        </th>
                        
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-bar-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            OEM PN
                        </th>
                        
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-des-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Description
                        </th>
                        <th scope="col">Con</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">QTY</th>
                        <th scope="col">Total</th>
                        <th scope="col">Bin</th>
                        
                    </tr>
                </thead>
                <tbody id="product_table_body"></tbody>
                
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        cat_array = new Array();
        $("input.form-check-input").click(function(event) {
            return event.target.id;
        });
        $("#search_item").click(function(event) {
            $('#product_table_body').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
            $('#total-price').html(`${spinner}`);
            $('#total-items').html(`${spinner}`);

            arr = $('input.form-check-input:checked').map(function(event){
                return this.id;
            });

            for(i=0; i < arr.length; i++){
                cat_array.push(arr[i]);
            }
         var datas ={ item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val() };
                                        $.ajax({
                                            url : "<?php echo base_url(); ?>index.php/inventory_search/request/search_item_by_cat",
                                            type : "post",
                                            data : datas,
                                            success : function(msg){
                                                
                                                //alert(msg);
                                                $("#product_table_body").html(msg);

                                                for(i=0; i < arr.length; i++){
                                                cat_array.push(arr[i]);
                                                    }
                                                $("#total-price").load("<?php echo base_url(); ?>index.php/inventory_search/price", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
             });


             $("#total-items").load("<?php echo base_url(); ?>index.php/inventory_search/item", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
             });
                                            }
                                        });


                                        cat_array = [];
        });
    })
</script>
        <script>
            $(document).ready(function() {

	
                $("#searchHistory").change(function(){
                    
                    $('#product_table_body').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                    $('#total-price').html(`${spinner}`);
            $('#total-items').html(`${spinner}`);
                    
                    const sortOrder = $("#sortOrder").val();
                    const orderBy = $("#orderBy").val();
                    
                                        var datas = {
                                            part_number : $("#searchHistory").val(),
                                            sortOrder : $("#sortOrder").val(),
                                            orderBy : $("#orderBy").val(),
                                            ajax : '1'
                                        };
                                        $.ajax({
                                            url : "<?php echo base_url();   ?>index.php/inventory_search/request/search_inventory",
                                            type : "post",
                                            data : datas,
                                            success : function(msg){
                                                
                                                //alert(msg);
                                                $("#product_table_body").html(msg);
                                                const tableWidth = $('#product_table_body').width();
                                                $('.scroller-content').width(tableWidth);


                                                
                                                $("#total-price").load("<?php echo base_url(); ?>index.php/inventory_search/price", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val(), types : "total_price"
                                                });


                                                $("#total-items").load("<?php echo base_url(); ?>index.php/inventory_search/item", {
                                                                                        item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val(), types : "total_items"
                                                });
                                                
                                                
                                            }
                                        }); 
                    });
                    $("#sortOrder").change(function(){
                    
                        $('#product_table_body').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                        $('#total-price').html(`${spinner}`);
            $('#total-items').html(`${spinner}`);
                        var datas = {
                                            part_number : $("#part_number").val(),
                                            sortOrder : $("#sortOrder").val(),
                                            orderBy : $("#orderBy").val(),
                                            ajax : '1'
                                        };
                                        $.ajax({
                                            url : "<?php echo base_url();   ?>index.php/inventory_search/request/search_inventory",
                                            type : "post",
                                            data : datas,
                                            success : function(msg){
                                                
                                                $("#product_table_body").html(msg);
                                                const tableWidth = $('#product_table_body').width();
                                                $('.scroller-content').width(tableWidth);
                                                
                                                if($("#part_number").val() != ""){
                                                    $("#total-price").load("<?php echo base_url(); ?>index.php/inventory_search/price", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });


                                                $("#total-items").load("<?php echo base_url(); ?>index.php/inventory_search/item", {
                                                                                        item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });
                                                }else{
                                                    $("#total-price").load("<?php echo base_url(); ?>index.php/inventory_search/price", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });


                                                $("#total-items").load("<?php echo base_url(); ?>index.php/inventory_search/item", {
                                                                                        item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });
                                                }
                                            }
                                        });
                        
                    });
                    $("#orderBy").change(function(){
                    
                        $('#product_table_body').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
                        $('#total-price').html(`${spinner}`);
            $('#total-items').html(`${spinner}`);
                        var datas = {
                                            part_number : $("#part_number").val(),
                                            sortOrder : $("#sortOrder").val(),
                                            orderBy : $("#orderBy").val(),
                                            ajax : '1'
                                        };
                                        $.ajax({
                                            url : "<?php echo base_url();   ?>index.php/inventory_search/request/search_inventory",
                                            type : "post",
                                            data : datas,
                                            success : function(msg){
                                                
                                                $("#product_table_body").html(msg);
                                                const tableWidth = $('#product_table_body').width();
                                                $('.scroller-content').width(tableWidth);
                                                
                                                if($("#part_number").val() != ""){
                                                    $("#total-price").load("<?php echo base_url(); ?>index.php/inventory_search/price", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });


                                                $("#total-items").load("<?php echo base_url(); ?>index.php/inventory_search/item", {
                                                                                        item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });
                                                }else{
                                                    $("#total-price").load("<?php echo base_url(); ?>index.php/inventory_search/price", {
                                                    item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });


                                                $("#total-items").load("<?php echo base_url(); ?>index.php/inventory_search/item", {
                                                                                        item : '1', cat : cat_array, part_number : $("#part_number").val(), ajax : '1', sortOrder : $("#sortOrder").val(), orderBy : $("#orderBy").val(), searchHistory : $("#searchHistory").val()
                                                });
                                                }
                                            }
                                        });
                        
                    });
                });
            </script>


<script id="inventoryScript" data-base-url="<?php echo base_url(); ?>"
        type="text/javascript"
        src="<?php echo base_url() ?>resources/js/inventorysearch/inventory_search.js"></script>
</script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/inventorysearch/rowInventoryCopy.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/inventorysearch/batchinventorysearchCopy.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/fieldCopy.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/exportCSV.js"></script>
