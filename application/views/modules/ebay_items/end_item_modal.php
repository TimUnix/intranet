<div class="modal fade" id="endItemModal" tabindex="-1" aria-labelledby="endItemModalLabel"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="endItemModalLabel" class="modal-title">Warning</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to end the following listing?</p>
                <p class="endItemTitle"></p>

                <div class="end-item-spinner-container text-center d-none w-100" role="status">
                    <div class="spinner-border text-info">
                        <span class="sr-only">
                            Please wait. Requesting end item...
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary">Cancel</button>
                <button type="button" class="btn btn-danger btnEndItemContinue">Continue</button>
            </div>
        </div>
    </div>
</div>
