<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/ebay_items/change_ad_price_modal'); ?>
<?php $this->load->view('modules/ebay_items/change_description_modal'); ?>
<?php $this->load->view('modules/ebay_items/change_quantity_modal'); ?>
<?php $this->load->view('modules/ebay_items/change_task_price_modal'); ?>
<?php $this->load->view('modules/ebay_items/change_title_modal'); ?>
<?php $this->load->view('modules/ebay_items/edit_description_modal'); ?>
<?php $this->load->view('modules/ebay_items/end_item_modal'); ?>
<?php $this->load->view('modules/ebay_items/export_csv_modal'); ?>
<?php $this->load->view('modules/ebay_items/filter_modal'); ?>
<?php $this->load->view('modules/ebay_items/keyword_modal'); ?>
<?php $this->load->view('modules/ebay_items/logs_modal'); ?>
<?php $this->load->view('modules/ebay_items/quantity_modal'); ?>

<style>
 .header-sortable:hover {
     cursor: pointer;
 }
 
 .category-filter-container {
     padding: 2px 8px;
 }

 .item-image-link-image {
     max-height: 30px;
     max-width: 30px;
 }

 .dropdown-header-filter {
     position: absolute;
     right: 0;
     top: 0;
 }

 .btn-header-filter:before {
     margin: 0;
 }

 .btn-header-filter:after {
     display: none;
 }

 .header-sold, .header-net_sold, .header-hits {
     position: relative;
 }

 .item-image-link:hover {
     position: relative;
 }

 .item-image-link-image:hover {
     position: absolute;
     width: 20vw;
     max-height: unset;
     max-width: unset;
 }
</style>

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <?= $data['page_name'] ?>
            </div>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <form id="searchForm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchColumn">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchSelected" class="custom-select col-md-2">
                                    <option value="all">Search</option>
                                    <option value="item_number">Item Number</option>
                                    <option value="title">Description</option>
                                    <option value="current_price">Current Price</option>
                                    <option value="sku">SKU</option>
                                    <option value="quantity">Qty Available</option>
                                    <option value="quantity_sold">Total Sold</option>
                                    <option value="total_sold_7_days">Total Sold 7 Days</option>
                                    <option value="hit_count">Hits</option>
                                    <option value="total_hits_7_days">Total Hits 7 Days</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemSearch"
                                       placeholder="Search eBay Item">
                            </div>
                        </div>
                    </div>
                </div>
                <legend>Channels</legend>
                <?php foreach ($data['channels'] as $index => $channel): ?>
                    <div class="form-check form-check-inline mx-0 mb-2">
                        <div class="input-group-text channel-filter-container">
                            <input class="form-check-input channel-filter" type="checkbox"
                                   name="<?= $channel->seller ?>" id="checkbox<?= $index ?>"
                                   data-position="<?= $index ?>" value="<?= $channel->seller ?>">
                            <label for="checkbox<?= $index ?>" class="form-check-label">
                                <?= $channel->seller ?>
                            </label>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php $seller_count = count($data['channels']); ?>
                <div class="form-check form-check-inline mx-0 mb-2">
                    <div class="input-group-text channel-filter-container">
                        <input class="form-check-input channel-filter" type="checkbox" name="Blank"
                               id="checkbox<?= $seller_count ?>"
                               data-position="<?= $seller_count ?>" value="">
                        <label for="checkbox<?= $seller_count ?>" class="form-check-label">Blank</label>
                    </div>
                    <button type="button" class="btn btn-sm btn-secondary btnClearChannelFilters ml-1">
                        <i class="fa fa-backspace" aria-hidden="true"></i>
                        Clear Channel Filters
                    </button>
                </div>
                <legend>Categories</legend>
                <?php foreach ($data['categories'] as $index => $category): ?>
                    <div class="form-check form-check-inline mx-0 mb-2">
                        <div class="input-group-text category-filter-container">
                            <input class="form-check-input category-filter" type="checkbox"
                                   name="<?= $category->category ?>" id="checkbox<?= $index ?>"
                                   data-position="<?= $index ?>" value="<?= $category->category ?>">
                            <label for="checkbox<?= $index ?>" class="form-check-label">
                                <?= $category->category ?>
                            </label>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php $cat_count = count($data['categories']); ?>
                <div class="form-check form-check-inline mx-0 mb-2">
                    <div class="input-group-text category-filter-container">
                        <input class="form-check-input category-filter" type="checkbox" name="Blank"
                               id="checkbox<?= $cat_count ?>"
                               data-position="<?= $cat_count ?>" value="">
                        <label for="checkbox<?= $cat_count ?>" class="form-check-label">Blank</label>
                    </div>
                    <button type="button" class="btn btn-sm btn-secondary btnClearCategoryFilters ml-1">
                        <i class="fa fa-backspace" aria-hidden="true"></i>
                        Clear Category Filters
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title"></h3>
        <a class="btn btn-sm btn-primary ml-2" href="<?= base_url() ?>index.php/ebay_items/new">
            <i class="fa fa-plus mr-1" aria-hidden="true"></i>
            Create eBay Ad
        </a>
        <strong>Items: </strong><span id="total-items"></span>
        <div class="card-tools">
            <div class="btn-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="switchWeekNoneSold">
                    <label class="custom-control-label" for="switchWeekNoneSold"></label>
                </div>
            </div>
            <div class="btn-group ml-2" id="dropdownContainer">
                <div class="input-group">
                    <div id = "switches"></div>
                    <select id="NoneSoldSelected" class="custom-select col-md-12">
                        <option value="past_week">Past Week None Sold </option>
                        <option value="3_weeks">3 Weeks None Sold</option>
                        <option value="1_month">1 Month None Sold</option>
                        <option value="3_months">3 Months None Sold</option>
                        <option value="6_months">6 Months None Sold</option>
                        <option value="past_year">Past Year None Sold</option>
                    </select>
                </div>
            </div>
            <button type="button" class="btn btn-sm btn-outline-primary ret ml-2" id="toggleExportCSVModal"
                    data-toggle="modal" data-target="#exportCsvModal" disabled>
                <i class="fa fa-download" aria-hidden="true"></i>
                Export CSV
            </button>
            <div class="btn-group ml-2" id="dropdownContainer">
                <button class="btn btn-outline-primary btn-sm dropdown-toggle" type="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    Show/Hide Columns
                </button>
                <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="item-number"
                               <?= isset($data['columns']->item_number->is_shown) ?
                                   ($data['columns']->item_number->is_shown ? 'checked' : '') : 'checked' ?>>
                        Item Number
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="sku"
                               <?= isset($data['columns']->sku->is_shown) ?
                                   ($data['columns']->sku->is_shown ? 'checked' : '') : 'checked' ?>>
                        SKU
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="image"
                               <?= isset($data['columns']->image->is_shown) ?
                                   ($data['columns']->image->is_shown ? 'checked' : '') : 'checked' ?>>
                        Image
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="title"
                               <?= isset($data['columns']->title->is_shown) ?
                                   ($data['columns']->title->is_shown ? 'checked' : '') : 'checked' ?>>
                        Title
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="pricelh" data-parent-header = "pricelh"
                               <?= isset($data['columns']->pricelh->is_shown) ?
                                   ($data['columns']->pricelh->is_shown ? 'checked' : '') : 'checked' ?>>
                        Price
                    </div>     
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="price" data-parent-header = "pricelh"
                               <?= isset($data['columns']->price->is_shown) ?
                                   ($data['columns']->price->is_shown ? 'checked' : 'd-none') : 'checked' ?>>
                        Price Listed
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="price_research_market_low"
                               <?= isset($data['columns']->price_research_market_low->is_shown) ?
                                   ($data['columns']->price_research_market_low->is_shown ? 'checked' : '') : 'checked' ?>>
                        Price Low
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="price_research_market_high" 
                               <?= isset($data['columns']->price_research_market_high->is_shown) ?
                                   ($data['columns']->price_research_market_high->is_shown ? 'checked' : '') : 'checked' ?>>
                        Price High
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="shipping-cost"
                               <?= isset($data['columns']->shipping_cost->is_shown) ?
                                   ($data['columns']->shipping_cost->is_shown ? 'checked' : '') : 'checked' ?>>
                        Shipping Cost
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="quanty" 
                               <?= isset($data['columns']->quanty->is_shown) ?
                                   ($data['columns']->quanty->is_shown ? 'checked' : '') : 'checked' ?>>
                        Quanty
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="quantity"
                               <?= isset($data['columns']->quantity->is_shown) ?
                                   ($data['columns']->quantity->is_shown ? 'checked' : '') : 'checked' ?>>
                        eBay
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="inventory_qty"
                               <?= isset($data['columns']->inventory_qty->is_shown) ? 
                                   ($data['columns']->inventory_qty->is_shown ? 'checked' : '') : 'checked' ?>>
                        Stock
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="sold"
                               <?= isset($data['columns']->sold->is_shown) ?
                                   ($data['columns']->sold->is_shown ? 'checked' : '') : 'checked' ?>>
                        Sold
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="total_sold_7_days"
                               <?= isset($data['columns']->total_sold_7_days->is_shown) ?
                                   ($data['columns']->total_sold_7_days->is_shown ? 'checked' : '') : 'checked' ?>>
                         Sold 7 days
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="quantity_sold"
                               <?= isset($data['columns']->quantity_sold->is_shown) ?
                                   ($data['columns']->quantity_sold->is_shown ? 'checked' : '') : 'checked' ?>>
                        Sold Total
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="net_sold"
                               <?= isset($data['columns']->net_sold->is_shown) ?
                                   ($data['columns']->net_sold->is_shown ? 'checked' : '') : 'checked' ?>>
                        Net Sold
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="net_sold_7_days"
                               <?= isset($data['columns']->net_sold_7_days->is_shown) ?
                                   ($data['columns']->net_sold_7_days->is_shown ? 'checked' : '') : 'checked' ?>>
                         Net Sold 7 days
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="net_sold_total"
                               <?= isset($data['columns']->net_sold_total->is_shown) ?
                                   ($data['columns']->net_sold_total->is_shown ? 'checked' : '') : 'checked' ?>>
                        Net Sold Total
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="hits" 
                               <?= isset($data['columns']->hits->is_shown) ?
                                   ($data['columns']->hits->is_shown ? 'checked' : '') : 'checked' ?>>
                        Hits
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="total_hits_7_days" 
                               <?= isset($data['columns']->total_hits_7_days->is_shown) ?
                                   ($data['columns']->total_hits_7_days->is_shown ? 'checked' : '') : 'checked' ?>>
                        Hits 7 days
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="hit_count"
                               <?= isset($data['columns']->hit_count->is_shown) ?
                                   ($data['columns']->hit_count->is_shown ? 'checked' : '') : 'checked' ?>>
                        Hits Total
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="date"
                               <?= isset($data['columns']->date->is_shown) ?
                                   ($data['columns']->date->is_shown ? 'checked' : '') : 'checked' ?>>
                        Date
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="seller"
                               <?= isset($data['columns']->seller->is_shown) ?
                                   ($data['columns']->seller->is_shown ? 'checked' : '') : 'checked' ?>>
                        Seller
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="keyword_prase"
                               <?= isset($data['columns']->keyword_prase->is_shown) ?
                                   ($data['columns']->keyword_prase->is_shown ? 'checked' : '') : 'checked' ?>>
                        Keyword Prase
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1" name="velocity"
                               <?= isset($data['columns']->velocity->is_shown)
                               ? ($data['columns']->velocity->is_shown ? 'checked' : '') : 'checked' ?>>
                        Velocity (7 Days)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="horizontal-scroller">
        <div class="scroller-content"></div>
    </div>
    <?php $this->load->view('modules/ebay_items/sticky_header'); ?>
    <div class="card-body p-0 inventory-container table-responsive table-container">
        <table class="table table-bordered table-striped table-hover main-table" id="product-table" cellspacing="0"
               style="border-collapse: separate;">
            <thead class="main-thead bg-white border">
                <tr class="main-header-row">
                    <?php $this->load->view('modules/ebay_items/headers'); ?>
                </tr>
                <tr class="main-subheader-row">
                    <?php $this->load->view('modules/ebay_items/subheaders'); ?>
                </tr>
            </thead>
            <tbody id="product_table_body" class="table-container">
                <tr>
                    <td colspan="100%">Search an item to see results here.</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('templates/common/spinner'); ?>

<script id="ebayScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/ebay_items/ebay_items.js"></script>
<script id="ebayApiScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/ebay_items/ebay_api.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/exportCSV.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/fieldCopy.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
