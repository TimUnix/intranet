<div class="modal fade" id="filterModal" tabindex="-1" role="dialog"
     aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Graph</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <span class="graph-description"></span>
                    <canvas class="trend-graph d-none"></canvas>
                    <div class="modal-graph-spinner text-center">
                        <div class="spinner-border text-info" role="status">
                            <span class="sr-only">
                                Please wait. Loading data...
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
