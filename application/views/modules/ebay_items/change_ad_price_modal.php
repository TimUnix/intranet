<div class="modal fade" id="changeAdPriceModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="changeAdPriceModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeAdPriceModalLabel">Change Ad Price</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="display: none;">
                    <strong>ID:</strong> <span class="change-ad-price-current-id"></span>
                </div>
                <div><strong>Item Number:</strong> <span class="change-ad-price-current-item-number"></span></div>
                <div><strong>SKU:</strong> <span class="change-ad-price-current-item-sku"></span></div>
                <div><strong>Store:</strong> <span class="change-ad-price-current-item-store"></span></div>
                <div><strong>Description:</strong></div>
                <p class="change-ad-price-current-description"></p>
                <div><strong>Current Price:</strong> <span class="change-ad-price-current-price"></span></div>
                <label for="newAdPrice">New Price</label>
                <form class="input-group">
                    <input id="newAdPrice" class="form-control new-ad-price" type="number" step="any">
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="change-ad-price-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary request-change-ad-price">Submit</button>
            </div>
        </div>
    </div>
</div>