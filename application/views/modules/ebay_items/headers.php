<?php
    $item_number_visibility = isset($data['columns']->item_number->is_shown) ?
                        ($data['columns']->item_number->is_shown ? '' : 'd-none') : '';
    $sku_visibility = isset($data['columns']->sku->is_shown) ?
                        ($data['columns']->sku->is_shown ? '' : 'd-none') : '';
    $image_visibility = isset($data['columns']->image->is_shown) ?
                        ($data['columns']->image->is_shown ? '' : 'd-none') : '';
    $title_visibility = isset($data['columns']->title->is_shown) ?
                        ($data['columns']->title->is_shown ? '' : 'd-none') : '';
    $pricelh_visibility = isset($data['columns']->pricelh->is_shown) ?
                        ($data['columns']->pricelh->is_shown ? '' : 'd-none') : '';
    $shipping_cost_visibility = isset($data['columns']->shipping_cost->is_shown) ?
                        ($data['columns']->shipping_cost->is_shown ? '' : 'd-none') : '';
    $quanty_visibility = isset($data['columns']->quanty->is_shown) ?
                        ($data['columns']->quanty->is_shown ? '' : 'd-none') : '';
    $sold_visibility = isset($data['columns']->sold->is_shown) ?
                        ($data['columns']->sold->is_shown ? '' : 'd-none') : '';
    $net_sold_visibility = isset($data['columns']->net_sold->is_shown) ?
                        ($data['columns']->net_sold->is_shown ? '' : 'd-none') : '';
    $hits_visibility = isset($data['columns']->hits->is_shown) ?
                        ($data['columns']->hits->is_shown ? '' : 'd-none') : '';
    $date_visibility = isset($data['columns']->date->is_shown) ?
                        ($data['columns']->date->is_shown ? '' : 'd-none') : '';
    $seller_visibility = isset($data['columns']->seller->is_shown) ?
                        ($data['columns']->seller->is_shown ? '' : 'd-none') : '';
    $keyword_visibility = isset($data['columns']->keyword_prase->is_shown) ?
                        ($data['columns']->keyword_prase->is_shown ? '' : 'd-none') : '';
    $velocity_visibility = isset($data['columns']->velocity->is_shown) ?
                        ($data['columns']->velocity->is_shown ? '' : 'd-none') : '';                     
?>

<th scope="col" rowspan="2">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    &#35;
</th>
<th scope="col" rowspan="2" class="header-sortable header-item-number <?= $item_number_visibility ?>"
    data-column="item_number">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Item Number
</th>
<th scope="col" rowspan="2" class="header-sortable header-sku <?= $sku_visibility ?>" data-column="sku">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    SKU
</th>
<th scope="col" rowspan="2" class="header-image <?= $image_visibility ?>">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Image
</th>
<th scope="col" rowspan="2" class="header-sortable header-title <?= $title_visibility ?>" nowrap data-column="title">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Title
</th>
<th style="text-align: center;" scope="col" colspan="3" class="header-pricelh <?= $pricelh_visibility?>" data-parent-header= "header-pricelh" nowrap>
    Price
</th>
<th scope="col" rowspan="2" class="header-sortable header-shipping-cost <?= $shipping_cost_visibility ?>"
    data-column="shipping_cost">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Shipping
</th>
<th style="text-align: center;" scope="col" colspan="2" class="header-quanty <?= $quanty_visibility ?>" data-parent-header = "header-quanty" nowrap>
    Quanty
</th> 
<th style="text-align: center;" scope="col" colspan="2" class="header-sold <?= $sold_visibility ?>" data-parent-header = "header-sold" nowrap>
    Sold
</th> 
<th style="text-align: center;" scope="col" colspan="2" class="header-net_sold <?= $net_sold_visibility ?>" data-parent-header = "header-net_sold" nowrap>
    Net Sold
</th>
<th style="text-align: center;" scope="col" colspan="2" class="header-hits <?= $hits_visibility ?>" data-parent-header = "header-hits" nowrap>
    Hits
</th>
<th scope="col" rowspan="2" class="header-sortable header-date <?= $date_visibility ?>" data-column="date">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Date
</th>
<th scope="col" rowspan="2" class="header-sortable header-seller <?= $seller_visibility ?>" data-column="seller">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Seller
</th>
<th scope="col" rowspan="2" class="header-keyword_prase <?= $keyword_visibility ?>" data-column="keyword">
    Keyword
</th>
<th scope="col" rowspan="2" class="header-sortable header-velocity <?= $velocity_visibility ?>"
    data-column="velocity">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Velocity
</th>
<th scope="col" rowspan="2" class="header-enditem">End Item</th>