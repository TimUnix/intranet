<div class="modal fade" id="editDescriptionModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="editDescriptionModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDescriptionModalLabel">Edit Description Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="display:none;"><strong>ID:</strong> <span class="current-id" id="current_id"></span></div>
                <div><strong>Item Number:</strong> <span class="current-item-number" id="current_item_number"></span></div>
                <div><strong>SKU:</strong> <span class="current-item-sku"></span></div>
                <div><strong>Store:</strong> <span class="current-item-store"></span></div>
                <div><strong>Current Description:</strong></div>
                <p class="current-description"></p>
                <label for="newDescription">New Description</label>
                <p><em>Description must not exceed 80 characters.</em></p>
                <form class="input-group">
                    <textarea id="newDescription" class="form-control new-description" maxlength="80" rows="3"
                              value=""></textarea>
                </form>
                <div>

                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="change-description-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary request-change-description">Submit</button>
            </div>
        </div>
    </div>
</div>