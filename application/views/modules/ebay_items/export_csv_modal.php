<div class="modal fade" id="exportCsvModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="exportCsvModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDescriptionModalLabel">Export CSV</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="exportCsvForm">
                    <div class="form-group">
                        <label>Select Columns to Export</label>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportItemNumber"
                                   value="item_number AS 'Item Number'" checked>
                            <label class="form-check-label" for="exportItemNumber">Item Number</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportSku"
                                   value="sku AS 'SKU'" checked>
                            <label class="form-check-label" for="exportSku">SKU</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportTitle"
                                   value="title AS 'Title'" checked>
                            <label class="form-check-label" for="exportTitle">Title</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportCurrentPrice"
                                   value="CONCAT('$', current_price) AS 'Current Price'" checked>
                            <label class="form-check-label" for="exportCurrentPrice">Listed</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportPriceResearchMarketLow"
                                   value="CONCAT('$', price_research_market_low) AS 'Price Low'" checked>
                            <label class="form-check-label" for="exportPriceResearchMarketLow">Price Low</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportPriceResearchMarketHigh"
                                   value="CONCAT('$', price_research_market_high) AS 'Price High'" checked>
                            <label class="form-check-label" for="exportPriceResearchMarketHigh">Price High</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportShippingCost"
                                   value="shipping_cost AS 'Shipping Cost'" checked>
                            <label class="form-check-label" for="exportShippingCost">Shipping Cost</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportQuantity"
                                   value="quantity AS 'Qty Ebay'" checked>
                            <label class="form-check-label" for="exportQuantity">Qty Ebay</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportQtyInventory"
                                   value="inventory_qty AS 'Qty Listed'" checked>
                            <label class="form-check-label" for="exportQtyInventory">Qty Listed</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportSoldWeek"
                                   value="total_sold_7_days AS 'Total Sold 7 days'" checked>
                            <label class="form-check-label" for="exportSoldWeek">Total Sold 7 Days</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportSold"
                                   value="quantity_sold AS 'Total Sold'" checked>
                            <label class="form-check-label" for="exportSold">Total Sold</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportNetSoldWeek"
                                   value="CONCAT('$', net_sold_7_days) AS 'Net Sold 7 days'" checked>
                            <label class="form-check-label" for="exportNetSoldWeek">Net Sold 7 days</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportNetSold"
                                   value="CONCAT('$', net_sold_total) AS 'Net Sold'" checked>
                            <label class="form-check-label" for="exportNetSold">Net Sold</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportHitsWeek"
                                   value="total_hits_7_days AS 'Total Hits 7 days'" checked>
                            <label class="form-check-label" for="exportHitsWeek">Total Hits 7 days</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportHits"
                                   value="hit_count AS 'Total Hits'" checked>
                            <label class="form-check-label" for="exportHits">Total Hits</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportDate"
                                   value="date AS 'Date'" checked>
                            <label class="form-check-label" for="exportDate">Date</label>
                        </div>
                         <div class="form-check">
                            <input class="form-check-input export-column" type="checkbox" id="exportSeller"
                                   value="seller AS 'Seller'" checked>
                            <label class="form-check-label" for="exportSeller">Seller</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="export-csv-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="exportCSV">Export</button>
            </div>
        </div>
    </div>
</div>

