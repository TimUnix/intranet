<div class="modal fade" id="changePriceModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="changePriceModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title w-100" id="changePriceModalLabel">New</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="success_price"></div>
                <div class="form-group">
                    <label for="request">Task</label>
                    <select class="form-control" id="request_price">
                        <?php foreach ($data['actions'] as $action): ?>
                            <option value="<?= $action->action_id ?>"
                                    <?= $action->action_name === 'Change Price' ? 'selected': '' ?>>
                                <?= $action->action_name?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" class="form-control current-item-sku" id="sku_price">
                </div>
                <div class="form-group">
                    <label for="channel">Channel</label>
                    <select id="channel_price" class="form-control">
                        <?php foreach ($data['channel'] as $channel): ?>
                            <option value="<?= $channel->channel_id ?>"
                                    <?= $channel->channel_name === 'Com' ? 'selected' : '' ?>>
                                <?= $channel->channel_name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="weight">Weight</label>
                    <input type="number" class="form-control" id="weight_price" step="any">
                </div>
                <div class="form-group">
                    <label for="shipping">Shipping</label>
                    <input type="text" class="form-control" id="shipping_price" value="Calculated">
                </div>
                <div id="moved-form" class="form-group d-none">
                    <label for="moved">Link</label>
                    <input type="text" class="form-control" id="moved_price">
                </div>
                <div class="form-group mt-5">
                    <label for="notes">Notes</label><textarea class="form-control" id="notes_price" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" class="form-control current-item-price" id="price_price">
                </div>
                <div class="form-group">
                    <label for="lot">Lot Size</label></br><input type="text" class="form-control" id="lot_price" value="1">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitPrice">Submit</button>
            </div>
        </div>
    </div>
</div>
