<?php
    $price_visibility = isset($data['columns']->price->is_shown) ?
                        ($data['columns']->price->is_shown ? '' : 'd-none') : '';
    $price_research_market_low_visibility = isset($data['columns']->price_research_market_low->is_shown) ?
                        ($data['columns']->price_research_market_low->is_shown ? '' : 'd-none') : '';
    $price_research_market_high_visibility = isset($data['columns']->price_research_market_high->is_shown) ?
                        ($data['columns']->price_research_market_high->is_shown ? '' : 'd-none') : '';
    $quantity_visibility = isset($data['columns']->quantity->is_shown) ?
                        ($data['columns']->quantity->is_shown ? '' : 'd-none') : '';
    $inventory_qty_visibility = isset($data['columns']->inventory_qty->is_shown) ?
                        ($data['columns']->inventory_qty->is_shown ? '' : 'd-none') : '';
    $total_sold_7_days_visibility = isset($data['columns']->total_sold_7_days->is_shown) ?
                        ($data['columns']->total_sold_7_days->is_shown ? '' : 'd-none') : '';
    $quantity_sold_visibility = isset($data['columns']->quantity_sold->is_shown) ?
                        ($data['columns']->quantity_sold->is_shown ? '' : 'd-none') : '';
    $net_sold_7_days_visibility = isset($data['columns']->net_sold_7_days->is_shown) ?
                        ($data['columns']->net_sold_7_days->is_shown ? '' : 'd-none') : '';
    $net_sold_total_visibility = isset($data['columns']->net_sold_total->is_shown) ?
                        ($data['columns']->net_sold_total->is_shown ? '' : 'd-none') : '';
    $total_hits_7_days_visibility = isset($data['columns']->total_hits_7_days->is_shown) ?
                        ($data['columns']->total_hits_7_days->is_shown ? '' : 'd-none') : '';
    $hit_count_visibility = isset($data['columns']->hit_count->is_shown) ?
                        ($data['columns']->hit_count->is_shown ? '' : 'd-none') : '';
?>


<th class="header-sortable header-price parent-pricelh <?= $price_visibility ?>" data-parent-header = "header-pricelh" data-column="header-price" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Listed
</th>
<th class="header-sortable header-price_research_market_low parent-pricelh <?= $price_research_market_low_visibility ?>"  data-parent-header = "header-pricelh" data-column="header-price_research_market_low" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Low
</th>
<th class="header-sortable header-price_research_market_high parent-pricelh <?= $price_research_market_high_visibility ?>" data-parent-header = "header-pricelh" data-column="header-price_research_market_high" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    High
</th>
<th class="header-sortable header-quantity parent-quanty <?= $quantity_visibility ?>" data-parent-header = "header-quanty" data-column="quantity" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    eBay
</th>
<th class="header-sortable header-inventory_qty parent-quanty <?= $inventory_qty_visibility ?>" data-parent-header = "header-quanty" data-column="inventory_qty" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Stock
</th>
<th class="header-sortable header-total_sold_7_days <?= $total_sold_7_days_visibility ?>" data-parent-header = "header-sold" data-column="total_sold_7_days" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    7 days
</th>
<th class="header-sortable header-quantity_sold <?= $quantity_sold_visibility ?>" data-parent-header = "header-sold" data-column="quantity_sold" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Total
</th>
<th class="header-sortable header-net_sold_7_days parent-net_sold <?= $net_sold_7_days_visibility ?>" data-parent-header = "header-net_sold" data-column="net_sold_7_days" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    7 days
</th>
<th class="header-sortable header-net_sold_total parent-net_sold <?= $net_sold_total_visibility ?>" data-parent-header = "header-net_sold" data-column="net_sold_total" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Total
</th>
<th class="header-sortable header-total_hits_7_days parent-hits <?= $total_hits_7_days_visibility ?>" data-parent-header = "header-hits" data-column="total_hits_7_days" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    7 days
</th>
<th class="header-sortable header-hit_count parent-hits<?= $hit_count_visibility ?>" data-parent-header = "header-hits" data-column="hit_count" nowrap>
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Total
</th>
