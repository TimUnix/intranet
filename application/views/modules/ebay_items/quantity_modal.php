<div class="modal fade" id="sku_quantity" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-12">
                        <table class="table table-bordered table-striped table-hover paginated" id="sku-table" style="width: 100%; border-collapse: separate;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown) ? ($data['columns']->sku->is_shown ? '' : ' d-none') 
                                                                       : '' ?>"nowrap>SKU</th>
                                    <th scope="col" class="header-title<?= isset($data['columns']->title->is_shown) ? ($data['columns']->title->is_shown ? '' : ' d-none') 
                                                                         : '' ?>"nowrap>Description</th>
                                    <th scope="col" class="header-quantity_sold parent-sold <?= isset($data['columns']->quantity_sold->is_shown) ? ($data['columns']->quantity_sold->is_shown ? '' : ' d-none') 
                                                                                 : '' ?>" data-parent-header="header-sold" nowrap>Total Qty Sold</th>
                                </tr>
                            </thead>
                            <tbody id="sku_quantity_body">
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
