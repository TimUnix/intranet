<div class="card-body sticky-headers p-0 d-none">
    <div class="sticky-content">
        <table class="table table-bordered table-striped table-hover sticky-table" cellspacing="0"
               style="border-collapse: separate">
            <thead class="sticky-thead bg-white">
                <tr class="sticky-header-row">
                    <?php $this->load->view('modules/ebay_items/headers'); ?>
                </tr>
                <tr class="sticky-subheader-row">
                    <?php $this->load->view('modules/ebay_items/subheaders'); ?>
                </tr>
            </thead>
        </table>
    </div>
</div>
