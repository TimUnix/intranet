<div class="modal fade" id="newEbayAdDraftsModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="newEbayAdDraftsModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newEbayAdDraftsModalLabel">Drafts</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<?php foreach ($drafts as $draft_id => $draft): ?>
					<div class="card">
						<div class="row no-gutters">
							<div class="col-md-4">
								<?php if (isset($draft['img-0'])): ?>
									<img src="<?= base_url() ?>uploads/<?= $draft['img-0'] ?>" class="card-img"
										 alt="Product Image" height="100%">
								<?php else: ?>
									<img src="<?=base_url() ?>resources/img/noimage.png" class="card-img"
										 alt="No image available" height="100%">
								<?php endif; ?>
							</div>
							<div class="col-md-8">
								<div class="card-body h-100 d-flex flex-column">
									<h5 class="card-title"><?= $draft['sku'] ?? 'No sku added' ?></h5>
									<p class="card-text"><small>Added <?= $draft['date_added'] ?></small></p>
									<div class="draft-buttons mt-auto w-100 text-right">
										<button type="button" class="btn btn-sm btn-danger btn-draft-delete"
												data-id="<?= $draft_id ?>">
											<i class="fa fa-trash" aria-hidden="true"></i>
											Delete
										</button>
										<button type="button" class="btn btn-sm btn-success btn-draft-restore"
												data-id="<?= $draft_id ?>">
											<i class="fa fa-undo" aria-hidden="true"></i>
											Restore
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
        </div>
    </div>
</div>
