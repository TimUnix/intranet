<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('modules/ebay_items/new_ebay_ad_drafts_modal'); ?>

<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			Create eBay Ad
			<div class="card-tools">
				<button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
						data-target="#newEbayAdDraftsModal">
					<i class="fa fa-file" aria-hidden="true"></i>
					Drafts
				</button>
			</div>
		</div>
		<div class="card-body">
			<div id="newEbayAdErrorMessage" role="alert"
				 class="new-ebay-ad-error-message alert alert-warning d-none sticky-top"></div>
			<form id="onPremisesUploadForm" enctype="multipart/form-data">
				<div class="form-group">
					<label for="newEbayAdPhoto">Photo</label>
					<input type="file" class="form-control-file" id="newEbayAdPhoto" name="newEbayAdPhoto[]"
						   accept="image/jpeg, image/png, image/gif, image/tiff" required multiple>
				</div>
			</form>
			<div class="image-display row"></div>
			<form id="newEbayAdForm">
				<div class="form-group mt-4">
					<label for="newEbayAdSku">SKU</label>
					<input id="newEbayAdSku" type="text" class="form-control new-ebay-ad-main-field" name="sku"
						   required>
					<div class="invalid-feedback">
						Please provide a valid SKU
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdUPC">UPC</label>
					<input type="text" class="form-control" name="upc">
				</div>
				<div class="form-group">
					<label for="newEbayAdTitle">Title</label>
					<input id="newEbayAdTitle" type="text" class="form-control new-ebay-ad-main-field" maxlength="80"
						   name="title" required>
					<div class="new-ebay-ad-title-counter">80</div>
					<div class="invalid-feedback">
						Please provide a valid title
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdDescription">Description</label>
					<textarea class="form-control new-ebay-ad-main-field" id="newEbayAdDescription" maxlength="4000"
							  name="description" rows="12" required></textarea>
					<div class="invalid-feedback">
						Please provide a valid title
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdStore">Store</label>
					<select id="newEbayAdStore" class="custom-select new-ebay-ad-main-field" name="store" required>
						<option value="com">UnixSurplusCom</option>
						<option value="net">UnixSurplusNet</option>
						<option value="plus">UnixPlusCom</option>
						<option value="itr">ITRecycleNow</option>
					</select>
				</div>
				<div class="form-group">
					<label for="newEbayAdItemLocation">Item Location</label>
					<select id="newEbayAdItemLocation" class="custom-select new-ebay-ad-main-field"
							name="item_location" required>
						<option value="itr-staclara-ca">Sta. Clara, California</option>
					</select>
				</div>
				<div class="ebay-business-policies-container">
					<div class="form-group">
						<label for="newEbayAdShippingPolicy">Shipping Policy</label>
						<select id="newEbayAdShippingPolicy" class="custom-select new-ebay-ad-main-field"
								name="shipping_policy" required>
							<?php foreach ($business_policies['shipping'] as $policy): ?>
								<option value="<?= $policy->policy_id ?>"><?= $policy->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdPrice">Price</label>
					<input id="newEbayAdPrice" type="number" step="any" class="form-control new-ebay-ad-main-field"
						   name="price" placeholder="$" required>
					<div class="invalid-feedback">
						Please provide a valid price
					</div>
				</div>
				<div class="form-check mb-2">
					<input type="checkbox" class="form-check-input  new-ebay-ad-main-field" id="newEbayAdBestOffer"
						   name="best_offer">
					<label class="form-check-label" for="newEbayAdBestOffer"><strong>Best Offer</strong></label>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label for="newEbayAdBestOfferMin">Lowest Offer</label>
						<input class="form-control best-offer new-ebay-ad-main-field" type="number" step="any"
							   placeholder="$" name="lowest_offer" id="newEbayAdBestOfferMin" disabled>
					</div>
					<div class="form-group col">
						<label for="newEbayAdBestOfferMax">Highest Offer</label>
						<input class="form-control best-offer new-ebay-ad-main-field" type="number" step="any"
							   placeholder="$" name="highest_offer" id="newEbayAdBestOfferMax" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdQuantity">Quantity</label>
					<input id="newEbayAdQuantity" type="number" step="any" class="form-control" name="quantity"
						   required>
				</div>
				<div class="form-check form-check-inline">
					<input type="checkbox" class="form-check-input new-ebay-ad-main-field" id="newEbayAdLot"
						   name="sell_as_lot">
					<label for="newEbayAdLot"><strong>Sell as Lot</strong></label>
				</div>
				<div class="from-group mb-2">
					<label for="newEbayAdLotAmount">Number of Items in Lot</label>
					<input type="number" step="1" class="form-control new-ebay-ad-main-field" id="newEbayAdLotAmount"
						   name="lot_count" disabled>
				</div>
				<div class="form-group">
					<label for="newEbayAdWeight">Weight</label>
					<input id="newEbayAdWeight" type="number" step="any" class="form-control new-ebay-ad-main-field"
						   name="weight" required placeholder="lbs">
					<div class="invalid-feedback">
						Please provide a valid weight
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdFormat">Format</label>
					<select id="newEbayAdFormat" class="custom-select new-ebay-ad-main-field" name="format">
						<option value="FIXED_PRICE">Fixed Price</option>
						<option value="AUCTION" disabled>Auction (not yet supported)</option>
					</select>
				</div>
				<div class="form-group">
					<label for="newEbayAdDuration">Duration</label>
					<select id="newEbayAdDuration" class="custom-select new-ebay-ad-main-field" name="duration">
						<option value="GTC">Good 'Til Cancelled</option>
						<option value="Days_1">1 Day</option>
						<option value="Days_3">3 Days</option>
						<option value="Days_5">5 Days</option>
						<option value="Days_7">7 Days</option>
						<option value="Days_10">10 Days</option>
						<option value="Days_14">14 Days</option>
						<option value="Days_21">21 Days</option>
						<option value="Days_30">30 Days</option>
						<option value="Days_60">60 Days</option>
						<option value="Days_90">90 Days</option>
						<option value="Days_120">120 Days</option>
					</select>
				</div>
				<div class="form-group">
					<label for="newEbayAdPromotedListings">Promoted Listings</label>
					<select id="newEbayAdPromotedListings" class="custom-select new-ebay-ad-main-field"
							name="promoted_listing">
						<option value="">Do Not Apply</option>
						<?php foreach ($campaigns as $campaign): ?>
							<option value="<?= $campaign->ebay_id ?>"><?= $campaign->name ?></option>
						<?php endforeach; ?>
					</select>

					<div class="input-group mt-2">
						<div class="input-group-prepend">
							<span class="input-group-text">Ad Rate</span>
						</div>
						<input class="form-control new-ebay-ad-main-field" id="newEbayAdPromotionRate" type="number"
							   step="any" name="ad_rate" disabled>
						<div class="input-group-append">
							<span class="input-group-text">%</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Volume Pricing</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">Buy 2 or more and save</span>
						</div>
						<input type="number" class="form-control new-ebay-ad-main-field" step="any"
							   id="newEbayAdVolumePrice2OrMore" name="volume_pricing_two_or_more">
						<div class="input-group-append">
							<span class="input-group-text">%</span>
						</div>
					</div>
					<div class="input-group my-2">
						<div class="input-group-prepend">
							<span class="input-group-text">Buy 3 or more and save</span>
						</div>
						<input type="number" class="form-control new-ebay-ad-main-field" step="any"
							   id="newEbayAdVolumePrice3OrMore" name="volume_pricing_three_or_more">
						<div class="input-group-append">
							<span class="input-group-text">%</span>
						</div>
					</div>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">Buy 4 or more and save</span>
						</div>
						<input type="number" class="form-control new-ebay-ad-main-field" step="any"
							   id="newEbayAdVolumePrice4OrMore" name="volume_pricing_four_or_more">
						<div class="input-group-append">
							<span class="input-group-text">%</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="newEbayAdCategory">Category</label>
					<select id="newEbayAdCategory" class="custom-select new-ebay-ad-main-field" name="category">
						<option value="" disabled selected>Select a category</option>
						<?php foreach ($create_ad_categories as $category): ?>
							<option value="<?= $category->ebay_id ?>"><?= $category->name ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="new-ebay-ad-category-specifics-container"></div>
                <button type="submit" class="btn btn-primary" id="requestSubmitEbayAd">
                    List
                </button>
                <button type="button" class="btn btn-default" id="requestSaveDraftEbayAd"
							  formaction="<?= base_url() ?>index.php/ebay_items/save_draft">
                    Save Draft
                </button>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view('templates/common/spinner'); ?>
<script id="ebayApiScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/ebay_items/ebay_api.js"></script>
<script id="ebayItemsNewScript" data-base-url="<?= base_url() ?>" type="text/javascript"
			src="<?= base_url() ?>resources/js/ebay_items/ebayItemsNew.js"></script>
<?php $this->load->view('templates/common/footer'); ?>

