<div class="modal fade" id="changeQuantityModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="changeQuantityModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeQuantityModalLabel">Change Quantity</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="display: none;">
                    <strong>ID:</strong> <span class="change-quantity-current-id"></span>
                </div>
                <div><strong>Item Number:</strong> <span class="change-quantity-current-item-number"></span></div>
                <div><strong>SKU:</strong> <span class="change-quantity-current-item-sku"></span></div>
                <div><strong>Store:</strong> <span class="change-quantity-current-item-store"></span></div>
                <div><strong>Description:</strong></div>
                <p class="change-quantity-current-description"></p>
                <div><strong>Current Quantity:</strong> <span class="change-quantity-current-quantity"></span></div>
                <label for="newQuantity">New Quantity</label>
                <form class="input-group">
                    <input id="newQuantity" class="form-control new-quantity" type="number">
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="change-quantity-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary request-change-ebay-quantity">Submit</button>
            </div>
        </div>
    </div>
</div>
