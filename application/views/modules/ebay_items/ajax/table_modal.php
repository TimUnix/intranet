<table class="table table-bordered table-striped table-hover paginated" style="border-collapse: separate;"
       cellspacing="0">
    <thead class="bg-white position-sticky border" style="top: 0;">
        <tr>
            <th scope="col" nowrap="">#</th>
            <th scope="col" nowrap="">Item Number</th>
            <th scope="col">Action</th>
            <th scope="col" class="header-item-number" nowrap="">Old Value</th>
            <th scope="col" class="header-item-number" nowrap="">New Value</th>
            <!-- <th scope="col" class="header-item-count"nowrap>Item Count</th> -->
            <th scope="col" class="header-image" nowrap="">Edited By</th>
            <th scope="col" class="header-title" nowrap="">Date Edited</th>
            <!-- <th scope="col" class="header-date-count"nowrap>Date Count</th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach($logs as $index => $log): ?>
            <tr>
                <td><?= $index + 1 ?></td>
                <td><?= $log->item_number ?></td>
                <td><?= $log->action ?></td>
                <td><?= $log->old_value ?></td>
                <td><?= $log->action === 'Change Price' ? '$' : ''?><?= $log->new_value ?></td>
                <td><?= $log->edited_by ?></td>
                <td><?= $log->date_edited ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
