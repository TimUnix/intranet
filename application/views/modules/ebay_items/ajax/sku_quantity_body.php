
<tr>
	<td scope = "col" class="item-sku<?= isset($columns->sku->is_shown) 
                                       ? ($columns->sku->is_shown ? '' : ' d-none') 
                                         : '' ?>" data-item-sku="<?= $sku ?>"nowrap>
        <?= $sku ?>
    </td>
    <td scope = "col" class="item-title<?= isset($columns->title->is_shown) 
                                       ? ($columns->title->is_shown ? '' : ' d-none') 
                                         : '' ?>" data-item-sku="<?= $description ?>">
        <?= $description ?>
    </td>
    <td scope = "col" class="item-quantity_sold<?= isset($columns->quantity_sold->is_shown) 
                                       ? ($columns->quantity_sold->is_shown ? '' : ' d-none') 
                                         : '' ?>" data-item-sku="<?= $quantity_sold ?>">
        <?= $quantity_sold ?>
    </td>
</tr>
