<div class="my-2">
    <div><strong>Category Specifics</strong></div>
    <small>Fields marked with * are required</small>
</div>

<?php $is_condition_required = (int) $conditions[0]->required === 1; ?>
<div class="form-group">
    <label for="newEbayAdCondition">Condition<?= $is_condition_required ? ' *' : '' ?></label>
    <select id="newEbayAdCondition"
				class="form-control new-ebay-ad-main-field"<?= $is_condition_required ? ' required' : ''?>>
        <option value="" disabled selected>Select an option</option>
        <?php foreach ($conditions as $condition): ?>
            <option value="<?= $condition->ebay_enum ?>">
                <?= $condition->display_name ?>
            </option>
        <?php endforeach; ?>
    </select>
</div>

<div class="new-ebay-ad-condition-description-container"></div>

<?php foreach ($keys as $key): ?>
    <?php if (isset($key->values) && count($key->values) > 0): ?>
        <?php if ((int) $key->cardinality === 0): ?>
            <div class="new-ebay-ad-category-specific-container">
                <div class="new-ebay-ad-category-specific-key" data-requirement="<?= $key->requirement ?>"
                     data-key="<?= $key->name ?>">
                    <strong><?= $key->name ?><?= $key->requirement === 'REQUIRED' ? ' *' : '' ?></strong>
                </div>
                <?php foreach ($key->values as $value): ?>
                    <div class="form-check form-check-inline my-1 mb-2">
                        <input type="checkbox" class="form-check-input new-ebay-ad-category-specific"
                               id="newEbayAdCategorySpecificsValue<?= str_replace(' ', '', $value->value) ?>"
                               value="<?= $value->value ?>"<?= $key->requirement === 'REQUIRED' ? ' required' : ''?>
							   name="<?= $value->value ?>">
                        <label for="newEbayAdCategorySpecificsValue<?= str_replace(' ', '', $value->value) ?>">
                            <?= $value->value ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <div class="form-group new-ebay-ad-category-specific-container">
                <label for="newEbayAdCategorySpecificsKey<?= str_replace(' ', '', $key->name) ?>"
                       class="new-ebay-ad-category-specific-key" data-requirement="<?= $key->requirement ?>"
                       data-key="<?= $key->name ?>">
                    <?= $key->name ?><?= $key->requirement === 'REQUIRED' ? ' *' : '' ?>
                </label>
                <input type="text" id="newEbayAdCategorySpecificsKey<?= str_replace(' ', '', $key->name) ?>"
                       aria-describedby="newEbayAdCategorySpecificsKey<?= str_replace(' ', '', $key->name) ?>Help"
                       list="newEbayAdCategorySpecifics<?= str_replace(' ', '', $key->name) ?>Values"
                       class="form-control new-ebay-ad-category-specific"
                       name="<?= $key->name ?>"
                       <?= $key->requirement === 'REQUIRED' ? 'required' : ''?>>
                <datalist id="newEbayAdCategorySpecifics<?= str_replace(' ', '', $key->name) ?>Values">
                    <?php foreach($key->values as $value): ?>
                        <option value="<?= $value->value ?>">
                    <?php endforeach; ?>
                </datalist>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="form-group new-ebay-ad-category-specific-container">
            <label for="newEbayAdCategorySpecificsKey<?= str_replace(' ', '', $key->name) ?>"
                   class="new-ebay-ad-category-specific-key" data-requirement="<?= $key->requirement ?>"
                   data-key="<?= $key->name ?>">
                <?= $key->name ?><?= $key->requirement === 'REQUIRED' ? ' *' : '' ?>
            </label>
            <input type="text" id="newEbayAdCategorySpecificsKey<?= str_replace(' ', '', $key->name) ?>"
                   aria-describedby="newEbayAdCategorySpecificsKey<?= str_replace(' ', '', $key->name) ?>Help"
                   class="form-control new-ebay-ad-category-specific" name="<?= $key->name ?>"
                   <?php if ($key->max_length !== ''): ?>maxlength="<?= $key->max_length?>"<?php endif; ?>
                   <?php if ($key->format !== ''): ?>placeholder="<?= $key->format?>"<?php endif; ?>
                   <?= $key->requirement === 'REQUIRED' ? 'required' : ''?>>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
