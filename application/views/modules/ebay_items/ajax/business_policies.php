<div class="form-group">
    <label for="newEbayAdShippingPolicy">Shipping Policy</label>
    <select id="newEbayAdShippingPolicy" class="custom-select new-ebay-ad-main-field" name="shipping_policy" required>
        <?php foreach ($shipping_policies as $policy): ?>
            <option value="<?= $policy->policy_id ?>"><?= $policy->name ?></option>
        <?php endforeach; ?>
    </select>
</div>
