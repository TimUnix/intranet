<div class="form-group">
    <label for="newEbayAdConditionDescription">Condition Description *</label>
    <textarea id="newEbayAdConditionDescription" class="form-control new-ebay-ad-main-field" rows="3"
			  maxlength="1000" required></textarea>
</div>
