<?php if(!empty($items)) { foreach($items as $row_index => $item): ?>
    <?php $index = $offset + $row_index; ?>
    <tr>
        <td class="item-index" scope="col"><?= $index + 1 ?></td>
        <td data-value="<?= $item->item_number?>"
            class="item-item-number<?= isset($columns->item_number->is_shown) 
                                   ? ($columns->item_number->is_shown ? '' : ' d-none') 
                                     : '' ?>" nowrap data-item-number="<?= $item->item_number ?>">
            <button type="button" data-toggle="modal" data-target="#filterModal"
                    class="btn btn-sm btn-outline-primary check">
                <?= $item->item_number ?>
            </button><br>
             <button type="button" class="btn btn-link btn-lg copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-link btn-lg item-logs">
                <i class="fa fa-eye item-logs" aria-hidden="true"></i>
            </button>
        </td>

        <td  data-value="<?= $item->sku ?>" class="item-sku<?= isset($columns->sku->is_shown) 
            ? ($columns->sku->is_shown ? '' : ' d-none') 
                : '' ?>" data-item-sku="<?= $item->sku ?>">
            <?php 
                $break = $item->sku;
                $newtext = wordwrap($break, 12, "\n", true);  ?>
                <?= $newtext ?><br>
            <button type="button" class="btn btn-link btn-lg item-sku_qty">
                <i class="fa fa-eye item-sku_qty" aria-hidden="true"></i>
            </button>                                      
        </td>

        <td class="item-image p-0 text-center align-middle<?= isset($columns->image->is_shown)
                             ? ($columns->image->is_shown ? '' : ' d-none')  : '' ?>" nowrap>
            <a class="item-image-link" href="<?= $item->img ?>" target="_blank" rel="noopener noreferrer">
                <img class="item-image-link-image" src="<?= $item->img ?>" loading="lazy">
            </a>
        </td>

        <td data-value="<?= htmlentities($item->title) ?>"
            class="item-title<?= isset($columns->title->is_shown) 
                             ? ($columns->title->is_shown ? '' : ' d-none') : '' ?>">
            <div class="edit-description-ebay-ads mt-3">
                <button type="button" class="btn btn-link btn-md text-primary btn-edit-description-ebay-ads"
                        data-toggle="modal" data-target="#editDescriptionModal" style="white-space: nowrap">
                    <i class="fa fa-edit btn-edit-description-ebay-ads" aria-hidden="true"></i>
                </button>
                <?php if($item->link == null): ?>
                    <?= $item->title ?>
                <?php else: ?>
                    <a href="<?= $item->link ?>" target="_blank" rel="noopener noreferrer">
                        <?= $item->title ?>
                    </a>
                <?php endif; ?>
            </div>
        </td>

        <td data-value="<?= $item->current_price ?>" style="height: 0px;"
            class="item-price parent-pricelh <?= isset($columns->price->is_shown) 
                             ? ($columns->price->is_shown  ? '' : ' d-none') : '' ?>" nowrap>
            <div class="d-flex justify-content-between flex-column h-100">
                <div class="edit-price-ebay-ads">
                 <button type="button" class="btn btn-link btn-md text-primary btn-edit-price-ebay-ads"
                            data-toggle="modal" data-target="#changeAdPriceModal">
                        <i class="fa fa-edit btn-edit-price-ebay-ads" aria-hidden="true"></i>
                </button>
                    $<?= $item->current_price ?> 
                </div>
            </div>
        </td>

        <td data-value="<?= $item->price_research_market_low ?>" class="item-price_research_market_low parent-pricelh <?= isset($columns->price_research_market_low->is_shown)
                                                                              ? ($columns->price_research_market_low->is_shown ? '' 
                                                                               : ' d-none') : '' ?>">
            $<?= $item->price_research_market_low ?>
        </td>

        <td data-value="<?= $item->price_research_market_high ?>" class="item-price_research_market_high parent-pricelh <?= isset($columns->price_research_market_high->is_shown)
                                                                              ? ($columns->price_research_market_high->is_shown ? '' 
                                                                               : ' d-none') : '' ?>">
            $<?= $item->price_research_market_high ?>
        </td>

        <td data-value="<?= $item->shipping_cost ?>"
            class="item-shipping-cost<?= isset($columns->shipping_cost->is_shown)
                              ? ($columns->shipping_cost->is_shown ? '' : ' d-none') : '' ?>" nowrap>
            <?php if ((float) $item->shipping_cost >= 0): ?>
                $<?= $item->shipping_cost ?>
            <?php else: ?>
                None
            <?php endif; ?>
        </td>

        <td data-value="<?= $item->quantity?>" class="item-quantity parent-quanty <?= isset($columns->quantity->is_shown)
                                                                              ? ($columns->quantity->is_shown ? '' 
                                                                               : ' d-none') : '' ?>">
            <button type="button" class="btn btn-link btn-toggle-change-ebay-quantity" data-toggle="modal"
                    data-target="#changeQuantityModal">
                <i class="fa fa-edit btn-toggle-change-ebay-quantity" aria-hidden="true"></i>
            </button>
            <?= $item->quantity ?>
        </td>

        <td data-value="<?= $item->inventory_qty?>" class="item-inventory_qty parent-quanty <?= isset($columns->inventory_qty->is_shown)
                                                                              ? ($columns->inventory_qty->is_shown ? '' 
                                                                               : ' d-none') : '' ?>">
            <?php if ((float) $item->inventory_qty >= 0): ?><?= $item->inventory_qty ?><?php endif; ?>
        </td>

        <td data-value="<?= $item->total_sold_7_days ?>" class="item-total_sold_7_days parent-sold <?= isset($columns->total_sold_7_days->is_shown)
                                                                         ? ($columns->total_sold_7_days->is_shown ? '' : ' d-none')
                                                                           : '' ?>">
            <?= $item->total_sold_7_days ?>
        </td>

        <td data-value="<?= $item->quantity_sold ?>" class="item-quantity_sold parent-sold <?= isset($columns->quantity_sold->is_shown)
                                                                         ? ($columns->quantity_sold->is_shown ? '' : ' d-none')
                                                                           : '' ?>">
            <?= $item->quantity_sold ?>
        </td>

        <td data-value="<?= $item->net_sold_7_days ?>"
            class="item-net_sold_7_days parent-net_sold<?= isset($columns->net_sold_7_days->is_shown) ? ($columns->net_sold_7_days->is_shown ? '' 
                                                                                : ' d-none') : '' ?>">
            <?php if (isset($item->net_sold_7_days) && $item->net_sold_7_days !== ''): ?>
                $<?= $item->net_sold_7_days ?>
            <?php endif; ?>
        </td>

        <td data-value="<?= $item->net_sold_total ?>"
            class="item-net_sold_total parent-net_sold <?= isset($columns->net_sold_total->is_shown) ? ($columns->net_sold_total->is_shown ? '' 
                                                                               : ' d-none') : '' ?>">
            <?php if (isset($item->net_sold_total) && $item->net_sold_total !== ''): ?>
                $<?= $item->net_sold_total ?>
            <?php endif; ?>
        </td>

        <?php $item_hits = "$item->total_hits_7_days / $item->hit_count"; ?>
        <td data-value="<?= $item->total_hits_7_days ?>" class="item-total_hits_7_days parent-hits <?= isset($columns->total_hits_7_days->is_shown) 
                                                                         ? ($columns->total_hits_7_days->is_shown ? '' : ' d-none')
                                                                           : '' ?> item-hits-week">
            <?= $item->total_hits_7_days ?>
        </td>

        <td data-value="<?= $item->hit_count ?>" class="item-hit_count parent-hits <?= isset($columns->hit_count->is_shown) 
                                                                 ? ($columns->hit_count->is_shown ? '' : ' d-none')
                                                                   : '' ?> item-hits-total">
            <?= $item->hit_count ?>
        </td>

        <?php $date_only = explode(' ', $item->date); ?>
        <td data-value="<?= $date_only[0] ?>"
            class="item-date<?= isset($columns->date->is_shown) 
                            ? ($columns->date->is_shown ? '' : ' d-none') : '' ?>" nowrap>
            <?= $date_only[0] ?>
        </td>

        <td data-value="<?= $item->seller ?>"
            class="item-seller<?= isset($columns->seller->is_shown)
                              ? ($columns->seller->is_shown ? '' : ' d-none') : '' ?>" nowrap>
            <?= $item->seller ?>
        </td>

        <td class="keyword_prase item-keyword_prase<?= isset($columns->keyword_prase->is_shown) 
                                         ? ($columns->keyword_prase->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap>
            <button type="button" class="btn btn-link btn-lg mr-1 keyword-btn" data-toggle="modal"
                    data-target="#keywordModal">
                <i class="fa fa-highlighter keyword-btn" aria-hidden="true"></i>
            </button>
        </td>

        <?php $item_velocity = number_format($item->total_sold_7_days / 7.0, 2, '.', ''); ?>
        <td data-value="<?= $item_velocity ?>"
            class="item-velocity<?= isset($columns->velocity->is_shown) 
                                ? ($columns->velocity->is_shown ? '' : ' d-none') : '' ?>" nowrap>
            <?= $item_velocity ?>
        </td>
        
        <td style="display: none;" data-value="<?= $item->id ?>" class="item-id" ><?= $item->id ?></td>
        <td class="d-none"><?= $item->start_time ?></td>
        <td class="d-none"><?= $item->end_time ?></td>
        <td class="d-none"><?= $item->registration_date ?></td>
        <td scope="col">
            <button type="button" class="btn btn-link btnEndItem mx-auto" data-target="#endItemModal"
                    data-toggle="modal">
                <i class="fa fa-archive" aria-hidden="true"></i>
            </button>
        </td>
 
        <td class="d-none">
            <div class="edit-description-listing">
                <button type="button" class="btn btn-link btn-md text-primary btn-edit-description-listing"
                        data-toggle="modal" data-target="#newListingModal">
                    <i class="fa fa-edit btn-edit-description-listing" aria-hidden="true"></i>
                    Task
                </button>
            </div>
        </td>
        
        
        
    </tr>
<?php endforeach; } ?>
<script>
$(document).ready(function(){
    var help = $('.popover-dismiss');
    help.attr('data-content', help.html()).text(' ? ').popover({trigger: 'click', html: true,sanitize : false});

    var par;
    $(".table #product_table_body tr").on('click', function(){
        var id = $(this).closest("tr").find(".item-sku").text();
        $("#keywordPrase").val("")
        $("#keywordPar").val(id);
        return par = id
  })
  $("#save_prase").click(function(){
    var datas = {
        par : par,
        prase : $("#keywordPrase").val(),
         ajax : '1'
            };
         $.ajax({
         url : "<?php echo base_url();   ?>index.php/ebay_items/save_keyword",
         type : "post",
         data : datas,
            success : function(msg){ 
                 var is_save = msg.data.is_save_successful
                    if(is_save == true){
                        $('#keywordModal').modal('hide');
                    }else if(is_save == false){
                        $("#not_save").html(`<div><div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        
                        ${msg.data.message}
                        </div></div>`)
                    }
                    if(msg.data.message == "Prase is empty"){
                        $("#not_save").html(`<div><div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        
                        ${msg.data.message}
                        </div></div>`)
                    }
                 },
                 async: true
        });
  })
});
</script>