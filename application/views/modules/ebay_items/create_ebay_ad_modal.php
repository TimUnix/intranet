<div class="modal fade" id="createEbayAdModal" tabindex="-1" aria-labelledby="createEbayAdModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="createEbayAdModalLabel" class="modal-title">Create eBay Ad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="newEbayAdErrorMessage" role="alert"
                     class="new-ebay-ad-error-message alert alert-warning d-none sticky-top"></div>
                <form id="onPremisesUploadForm" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="newEbayAdPhoto">Photo</label>
                        <input type="file" class="form-control-file" id="newEbayAdPhoto" name="newEbayAdPhoto[]"
                               accept="image/jpeg, image/png, image/gif, image/tiff" required multiple>
                    </div>
                </form>
                <div class="image-display row"></div>
                <form id="newEbayAdForm" class="needs-validation">
                    <div class="form-group">
                        <label for="newEbayAdSku">SKU</label>
                        <input id="newEbayAdSku" type="text" class="form-control" required>
                        <div class="invalid-feedback">
                            Please provide a valid SKU
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdUPC">UPC</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdTitle">Title</label>
                        <input id="newEbayAdTitle" type="text" class="form-control" maxlength="80" required>
                        <div class="new-ebay-ad-title-counter">80</div>
                        <div class="invalid-feedback">
                            Please provide a valid title
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdDescription">Description</label>
                        <textarea class="form-control" id="newEbayAdDescription" maxlength="4000" rows="12"
								  required></textarea>
                        <div class="invalid-feedback">
                            Please provide a valid title
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdStore">Store</label>
                        <select id="newEbayAdStore" class="form-control" required>
                            <option value="com">UnixSurplusCom</option>
                            <option value="net">UnixSurplusNet</option>
                            <option value="plus">UnixPlusCom</option>
                            <option value="itr">ITRecycleNow</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdItemLocation">Item Location</label>
                        <select id="newEbayAdItemLocation" class="form-control" required>
                            <option value="itr-staclara-ca">Sta. Clara, California</option>
                        </select>
                    </div>
                    <div class="ebay-business-policies-container">
                        <div class="form-group">
                            <label for="newEbayAdShippingPolicy">Shipping Policy</label>
                            <select id="newEbayAdShippingPolicy" class="form-control" required>
                                <?php foreach ($data['business_policies']['shipping'] as $policy): ?>
                                    <option value="<?= $policy->policy_id ?>"><?= $policy->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdPrice">Price</label>
                        <input id="newEbayAdPrice" type="number" step="any" class="form-control"
                               placeholder="$" required>
                        <div class="invalid-feedback">
                            Please provide a valid price
                        </div>
                    </div>
                    <div class="form-check mb-2">
                        <input type="checkbox" class="form-check-input" id="newEbayAdBestOffer">
                        <label class="form-check-label" for="newEbayAdBestOffer"><strong>Best Offer</strong></label>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="newEbayAdBestOfferMin">Lowest Offer</label>
                            <input class="form-control best-offer" type="number" step="any" placeholder="$"
                                   id="newEbayAdBestOfferMin" disabled>
                        </div>
                        <div class="form-group col">
                            <label for="newEbayAdBestOfferMax">Highest Offer</label>
                            <input class="form-control best-offer" type="number" step="any" placeholder="$"
                                   id="newEbayAdBestOfferMax" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdQuantity">Quantity</label>
                        <input id="newEbayAdQuantity" type="number" step="any" class="form-control" required>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="checkbox" class="form-check-input" id="newEbayAdLot">
                        <label for="newEbayAdLot"><strong>Sell as Lot</strong></label>
                    </div>
                    <div class="from-group mb-2">
                        <label for="newEbayAdLotAmount">Number of Items in Lot</label>
                        <input type="number" step="1" class="form-control" id="newEbayAdLotAmount" disabled>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdWeight">Weight</label>
                        <input id="newEbayAdWeight" type="number" step="any" class="form-control" required
                               placeholder="lbs">
                        <div class="invalid-feedback">
                            Please provide a valid weight
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdFormat">Format</label>
                        <select id="newEbayAdFormat" class="form-control">
                            <option value="FIXED_PRICE">Fixed Price</option>
                            <option value="AUCTION" disabled>Auction (not yet supported)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdDuration">Duration</label>
                        <select id="newEbayAdDuration" class="form-control">
                            <option value="GTC">Good 'Til Cancelled</option>
                            <option value="Days_1">1 Day</option>
                            <option value="Days_3">3 Days</option>
                            <option value="Days_5">5 Days</option>
                            <option value="Days_7">7 Days</option>
                            <option value="Days_10">10 Days</option>
                            <option value="Days_14">14 Days</option>
                            <option value="Days_21">21 Days</option>
                            <option value="Days_30">30 Days</option>
                            <option value="Days_60">60 Days</option>
                            <option value="Days_90">90 Days</option>
                            <option value="Days_120">120 Days</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdPromotedListings">Promoted Listings</label>
                        <select id="newEbayAdPromotedListings" class="form-control">
                            <option value="">Do Not Apply</option>
                            <?php foreach ($data['campaigns'] as $campaign): ?>
                                <option value="<?= $campaign->ebay_id ?>"><?= $campaign->name ?></option>
                            <?php endforeach; ?>
                        </select>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Ad Rate</span>
                            </div>
                            <input class="form-control" id="newEbayAdPromotionRate" type="number" step="any" disabled>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Volume Pricing</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Buy 2 or more and save</span>
                            </div>
                            <input type="number" class="form-control" step="any" id="newEbayAdVolumePrice2OrMore">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                        <div class="input-group my-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Buy 3 or more and save</span>
                            </div>
                            <input type="number" class="form-control" step="any" id="newEbayAdVolumePrice3OrMore">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Buy 4 or more and save</span>
                            </div>
                            <input type="number" class="form-control" step="any" id="newEbayAdVolumePrice4OrMore">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newEbayAdCategory">Category</label>
                        <select id="newEbayAdCategory" class="form-control">
                            <option value="" disabled selected>Select a category</option>
                            <?php foreach ($data['create_ad_categories'] as $category): ?>
                                <option value="<?= $category->ebay_id ?>"><?= $category->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="new-ebay-ad-category-specifics-container"></div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="new-ebay-ad-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="button" class="btn btn-default mr-auto" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="requestSaveDraftEbayAd">
                    Save Draft
                </button>
                <button type="submit" class="btn btn-primary" id="requestSubmitEbayAd" form="newEbayAdForm">
                    List
                </button>
            </div>
        </div>
    </div>
</div>
