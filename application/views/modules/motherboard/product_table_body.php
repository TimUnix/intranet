<?php if(!empty($items)) { foreach($items as $index => $item): ?>
	<tr>
		<td scope="col"><?= $index + 1 ?></td>
		<td scope="col" class="sku item-sku<?= isset($columns->sku->is_shown) 
                        ? ($columns->sku->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->sku ?>" >
        <button type="button" class="btn btn-link text-primary draftid-cancel skubtn">
                <i class="fa fa-edit skupenbtn" aria-hidden="true"></i>
            </button>

        <?= $item->sku ?></td>
		<td scope="col" class=" barcode item-barcode<?= isset($columns->barcode->is_shown) 
                        ? ($columns->barcode->is_shown ? '' : ' d-none') : '' ?>" data-value="<?= $item->barcode ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel barcodebtn">
                <i class="fa fa-edit barcodepenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->barcode ?></td>
		<td scope="col" class="brand item-brand<?= isset($columns->brand->is_shown) 
                        ? ($columns->brand->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->brand ?>" nowrap>
        <button type="button" class="btn btn-link text-primary draftid-cancel brandbtn">
                <i class="fa fa-edit brandpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->brand ?></td>
		<td scope="col" class="model item-model<?= isset($columns->model->is_shown) 
                        ? ($columns->model->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->model ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel modelbtn">
                <i class="fa fa-edit modelpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->model ?></td>
		<td scope="col" class="rev_version item-rev_version<?= isset($columns->rev_version->is_shown) 
                        ? ($columns->rev_version->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->rev_version ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel rev_versionbtn">
                <i class="fa fa-edit rev_versionpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->rev_version ?></td>
		<td scope="col" class="qty_cpu item-qty_cpu<?= isset($columns->qty_cpu->is_shown) 
                        ? ($columns->qty_cpu->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->qty_cpu ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel qty_cpubtn">
                <i class="fa fa-edit qty_cpupenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->qty_cpu ?></td>
		<td scope="col" class="memory item-memory<?= isset($columns->memory->is_shown) 
                        ? ($columns->memory->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->memory ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel memorybtn">
                <i class="fa fa-edit memorypenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->memory ?></td>
		<td scope="col" class="shield item-shield<?= isset($columns->shield->is_shown) 
                        ? ($columns->shield->is_shown ? '' : ' d-none') : '' ?>"  data-value="<?= $item->shield ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel shieldbtn">
                <i class="fa fa-edit shieldpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->shield ?></td>
		<td scope="col" class="heatsink item-heatsink<?= isset($columns->heatsink->is_shown) 
                        ? ($columns->heatsink->is_shown ? '' : ' d-none') : '' ?>" data-value="<?= $item->heatsink ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel heatsinkbtn">
                <i class="fa fa-edit heatsinkpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->heatsink ?></td>
	</tr>

<?php endforeach; }?>