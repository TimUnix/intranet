<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>


<button id="insertListing" type="button" class="btn btn-info btn-lg ml-2 mb-4" data-toggle="modal"
        data-target="#myModal">
    Create Motherboard
</button>
<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
                <div class="row">
                
                <div class="col-md-6" novalidate>
                    <label for="sku">SKU</label>
                    <input type="text" class="form-control" id="sku" list="skuSuggestions" required
                           <?= isset($data['task']) ? "value=\"{$data['task']['sku']}\"" : '' ?>>
                    <datalist id="skuSuggestions"></datalist>
                </div>
                <div class="col-md-6">
                    <label for="barcode">Barcode</label>
                    <input type="text" class="form-control" id="barcode">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="brand">Brand</label>
                    <input type="text" class="form-control" id="brand">
                </div>
                <div class="col-md-6">
                    <label for="model">Model</label>
                    <input type="text" class="form-control" id="model">
                </div>
                </div>
                <div class="row">

                <div class="col-md-6">
                    <label for="rev_version">Rev Version</label>
                    <input type="text" class="form-control" id="rev_version">
                </div>
                <div class="col-md-6">
                    <label for="qty_cpu">Qty CPU</label>
                    <input type="text" class="form-control" id="qty_cpu">
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="memory">Memory</label>
                    <input type="number" id="memory" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="shield">Shield</label>
                    <select id="qty_cpu" class="form-control">
                        <option value="null"> </option>
                        <option value="I/O Shield">I/O Shield</option>
                    </select>
                </div>
                </div>
                <div class="row">
                
                <div class="col-md-6">
                    <label for="heatsink">Heatsink</label>
                    <input type="text" id="heatsink" class="form-control">
                </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-bottom: 60px;">
	<div class="card">
		<div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
        	<form id="searchForm">
        		<div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchColumn">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchSelected" class="custom-select col-md-2">
                                    <option value="sku">SKU</option>
                                    <option value="barcode">Barcode</option>
                                    <option value="brand">Brand</option>
                                    <option value="model">Model</option>
                                    <option value="rev_version">Rev Version</option>
                                    <option value="qty_cpu">Qty CPU</option>
                                    <option value="memory">Memory</option>
                                    <option value="shield">Shield</option>
                                    <option value="heatsink">Heatsink</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemSearch"
                                       placeholder="Search">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="orderBy">Order By</label>
                        <select type="button" class="form-control" id="orderBy">   
                            <option value="sku">SKU</option>
                            <option value="barcode">Barcode</option>
                            <option value="brand">Brand</option>
                            <option value="model">Model</option>
                            <option value="rev_version">Rev Version</option>
                            <option value="qty_cpu">Qty CPU</option>
                            <option value="memory">Memory</option>
                            <option value="shield">Shield</option>                                                       
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sortOrder">Sort Order</label>
                        <select type="button" class="form-control" id="sortOrder">
                            <option value="ASC" selected>Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
        	</form>
        </div>
	</div>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Summary</h2>
        </div>
        <div class="card-body">
            <span><strong>Total:</strong> <span class="task-count"></span></span>
        </div>
    </div>
	<div class = "card">
        <div class="card-header">
        <h3 class="card-title">Items</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <div class="btn-group" id="dropdownContainer">
                    <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>Show/Hide Columns</button>

                <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sku" <?= isset($data['columns']->sku->is_shown)
                                                      ? ($data['columns']->sku->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            SKU
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="barcode" <?= isset($data['columns']->barcode->is_shown)
                                                      ? ($data['columns']->barcode->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Barcode
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="brand" <?= isset($data['columns']->brand->is_shown)
                                                      ? ($data['columns']->brand->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Brand
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="model" <?= isset($data['columns']->model->is_shown)
                                                      ? ($data['columns']->model->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Model
                    </div>
                   <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="rev_version" <?= isset($data['columns']->rev_version->is_shown)
                                                      ? ($data['columns']->rev_version->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Rev Version
                    </div>
                     <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="qty_cpu" <?= isset($data['columns']->qty_cpu->is_shown)
                                                      ? ($data['columns']->qty_cpu->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Qty CPU
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="memory" <?= isset($data['columns']->memory->is_shown)
                                                      ? ($data['columns']->memory->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Memory
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="Shield" <?= isset($data['columns']->Shield->is_shown)
                                                      ? ($data['columns']->Shield->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Shield
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1"
                               name="heatsink" <?= isset($data['columns']->heatsink->is_shown)
                                               ? ($data['columns']->heatsink->is_shown ? 'checked' : '')
                                                 : 'checked' ?>>
                        Heatsink
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
		<div class="card-body p-0 inventory-container table-responsive">
			<table class="table table-bordered table-striped table-hover paginated" id="product-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="bg-white position-sticky border" style="top: 0;">
					<tr>
						<th scope="col" nowrap>&#35;</th>
						<th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown) ? ($data['columns']->sku->is_shown ? '' : ' d-none') 
                                                       : '' ?>">SKU</th>
						<th scope="col" class="header-barcode<?= isset($data['columns']->barcode->is_shown) ? ($data['columns']->barcode->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Barcode</th>
						<th scope="col" class="header-brand<?= isset($data['columns']->brand->is_shown) ? ($data['columns']->brand->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Brand</th>
						<th scope="col" class="header-model<?= isset($data['columns']->model->is_shown) ? ($data['columns']->model->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Model</th>
						<th scope="col" class="header-rev_version<?= isset($data['columns']->rev_version->is_shown) ? ($data['columns']->rev_version->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Rev Version</th>
						<th scope="col" class="header-qty_cpu<?= isset($data['columns']->qty_cpu->is_shown) ? ($data['columns']->qty_cpu->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Qty CPU</th>
						<th scope="col" class="header-memory<?= isset($data['columns']->memory->is_shown) ? ($data['columns']->memory->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Memory</th>
						<th scope="col" class="header-shield<?= isset($data['columns']->shield->is_shown) ? ($data['columns']->shield->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Shield</th>
						<th scope="col" class="header-heatsink<?= isset($data['columns']->heatsink->is_shown) ? ($data['columns']->heatsink->is_shown ? '' : ' d-none') 
                                                       : '' ?>">Heatsink</th>
					</tr>
				</thead>
				<tbody id="product_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
			</table>
		</div>
	</div>
</div>
</div> <!-- for footer -->
<?php $this->load->view('templates/common/spinner'); ?>

<script id="motherboardScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/motherboard_module/motherboard.js"></script>
<script id="motherboardFieldEditScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/motherboard_module/motherboardFieldEdit.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
