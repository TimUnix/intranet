<div class="card">
        <div class="card-header">
            <h3 class="card-title">Items</h3>
            
        </div>
        <div class="card-body p-0 inventory-container table-responsive" style="height: 30vh;">
            <table class="table table-bordered table-striped table-hover paginated" id="product-table" style="overflow-x: auto;">
                <thead class="bg-white position-sticky border" style="top: 0;">
                    <tr>
                        <th scope="col" class="header-item-number" nowrap="">lowestFixedPrice</th>
                        <th scope="col" class="header-item-number" nowrap="">originalPrice</th>
                        <!-- <th scope="col" class="header-item-count"nowrap>Item Count</th> -->
                        <th scope="col" class="header-image" nowrap="">discountedPrice</th>
                        <th scope="col" class="header-title" nowrap="">originalRetailPrice</th>
                        <th scope="col" class="header-title" nowrap="">binPrice</th>
                        <th scope="col" class="header-title" nowrap="">binPriceDouble</th>
                        <!-- <th scope="col" class="header-date-count"nowrap>Date Count</th> -->
                    </tr>
                </thead>
                <tbody id="product_table_body">    
                
  
    <tr>
        <?php if(empty($lowestFixedPrice[0][0])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $lowestFixedPrice_one = explode('lowestFixedPrice:', $lowestFixedPrice[0][0]); echo $lowestFixedPrice_one[1]; ?></td>
        <?php endif; ?>    
    
        <?php if(empty($originalPrice[0][0])):  ?>
            <td></td>
        <?php else:  ?>
            <td><?php $originalPrices_one = explode('originalPrice:', $originalPrice[0][0]); echo $originalPrice_one[1]; ?></td>
        <?php endif;  ?>
        <?php if(empty($discountedPrice[0][0])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $discountedPrice_one = explode('discountedPrice:', $discountedPrice[0][0]); echo $discountedPrice_one[1]; ?></td>
        <?php endif; ?>
        <?php if(empty($originalRetailPrice[0][0])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalRetailPrice_one = explode('originalRetailPrice:', $originalRetailPrice[0][0]); echo $originalRetailPrice_one[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($binPrice[0][0])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPrice_one = explode('binPrice:US', $binPrice[0][0]); echo $binPrice_one[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($binPriceDouble[0][0])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPriceDouble_one = explode('binPriceDouble:', $binPriceDouble[0][0]); echo $binPriceDouble_one[1]; ?></td>
        <?php endif; ?>
                
        
    </tr>


    <tr>
        <?php if(empty($lowestFixedPrice[0][1])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $lowestFixedPrices_two = explode('lowestFixedPrice:', $lowestFixedPrice[0][1]); echo $lowestFixedPrices_two[1]; ?></td>
        <?php endif; ?>    
    
        <?php if(empty($originalPrice[0][1])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalPrice_two = explode('originalPrice:', $originalPrice[0][1]); echo $originalPrice_two[1]; ?></td>
        <?php endif; ?>
        <?php if(empty($discountedPrice[0][1])): ?>
            <td></td>
        <?php else: ?>
            <td><?php $discountedPrice_two = explode('discountedPrice:', $discountedPrice[0][1]); echo $discountedPrice_two[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($originalRetailPrice[0][1])): ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalRetailPrice_two = explode('originalRetailPrice:', $originalRetailPrice[0][1]); echo $originalRetailPrice_two[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($binPrice[0][1])): ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPrice_two = explode('binPrice:US', $binPrice[0][1]); echo $binPrice_two[1]; ?></td>
        <?php endif; ?>
                
            <?php if(empty($binPriceDouble[0][1])): ?>
        <td></td>
            <?php else:  ?>
        <td><?php $binPriceDouble_two = explode('binPriceDouble:', $binPriceDouble[0][1]); echo $binPriceDouble_two[1]; ?></td>
            <?php endif; ?>
                        
        
    </tr>


    <tr>
    <?php if(empty($lowestFixedPrice[0][2])):  ?>
        <td></td>
        <?php else: ?>
            <td><?php $lowestFixedPrices_three = explode('lowestFixedPrice:', $lowestFixedPrice[0][2]); echo $lowestFixedPrices_three[1]; ?></td>
            <?php endif; ?>    
    
        <?php if(empty($originalPrice[0][2])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalPrice_three = explode('originalPrice:', $originalPrice[0][2]); echo $originalPrice_three[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($discountedPrice[0][2])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $discountedPrice_three = explode('discountedPrice:', $discountedPrice[0][2]); echo $discountedPrice_three[1]; ?></td>
        <?php endif; ?>
                
        <?php if(empty($originalRetailPrice[0][2])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalRetailPric_three = explode('originalRetailPrice:', $originalRetailPrice[0][2]); echo $originalRetailPric_three[1]; ?></td>
        <?php endif; ?>
            
        <?php if(empty($binPrice[0][2])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPrice_three = explode('binPrice:US', $binPrice[0][2]); echo $binPrice_three[1]; ?></td>
        <?php endif; ?>
                    
        <?php if(empty($binPriceDouble[0][2])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPriceDouble_three = explode('binPriceDouble:', $binPriceDouble[0][2]); echo $binPriceDouble_three[1]; ?></td>
        <?php endif; ?>
                            
        
    </tr>


    <tr>
    <?php if(empty($lowestFixedPrice[0][3])):  ?>
        <td></td>
        <?php else: ?>
            <td><?php $lowestFixedPrices_four = explode('lowestFixedPrice:', $lowestFixedPrice[0][3]); echo $lowestFixedPrices_four[1]; ?></td>
            <?php endif; ?>    
    
        <?php if(empty($originalPrice[0][3])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalPrice_four = explode('originalPrice:', $originalPrice[0][3]); echo $originalPrice_four[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($discountedPrice[0][3])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $discountedPrice_four = explode('discountedPrice:', $discountedPrice[0][3]); echo $discountedPrice_four[1]; ?></td>
        <?php endif; ?>
        
        <?php if(empty($originalRetailPrice[0][3])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $originalRetailPric_four = explode('originalRetailPrice:', $originalRetailPrice[0][3]); echo $binPrice_four[1]; ?></td>
        <?php endif; ?>
                
        <?php if(empty($binPrice[0][3])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPrice_four = explode('binPrice:US', $binPrice[0][3]); echo $binPrice_four[1]; ?></td>
        <?php endif; ?>
                        
        <?php if(empty( $binPriceDouble[0][3])):  ?>
            <td></td>
        <?php else: ?>
            <td><?php $binPriceDouble_four = explode('binPriceDouble:', $binPriceDouble[0][3]); echo $binPriceDouble_four[1]; ?></td>
        <?php endif; ?>
                                
        
    </tr>
    


    <?php
    /*foreach($lowestFixedPrice[0] as $lowestFixedPricedata){
        ?><tr><?php
        $lowestFixedPrice_data = explode('lowestFixedPrice:', $lowestFixedPricedata);
        ?><td><?php
        echo $lowestFixedPrice_data[1];
        ?></td><?php
    }
    
    ?></tr><?php*/

    ?>
    

</tbody>
            </table>
        </div>
    </div>


    <?php
    

    ?>