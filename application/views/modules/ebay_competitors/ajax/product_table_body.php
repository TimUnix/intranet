<?php foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $offset + $index + 1 ?></td>
        <td scope="col"><input type="checkbox" class="checkbox-row"></td>
        <td scope="col">
            <button type="button" class="btn btn-link text-primary btn-copy-row">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
        </td>
        <td scope="col" class="item-item-number<?= isset($columns->item_number->is_shown) 
                                               ? ($columns->item_number->is_shown ? '' : ' d-none') 
                                                 : '' ?>" nowrap>
            <button type="button" class="btn btn-link text-primary copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->item_number ?>
        </td>
        <td scope="col" style="padding: 0;" class="item-image<?= isset($columns->image->is_shown) 
                                                             ? ($columns->image->is_shown ? '' : ' d-none') 
                                                               : '' ?>">
            <img src="<?= $item->img ?>" width="120" loading="lazy">
        </td>
        <td scope="col" class="item-title<?= isset($columns->title->is_shown) 
                                         ? ($columns->title->is_shown ? '' : ' d-none') 
                                           : '' ?>">
            <button type="button" class="btn btn-link text-primary copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <a href="<?= $item->link ?>" target="_blank" rel="noopener noreferrer">
                <?= $item->title ?>
            </a>
        </td>

        <td scope="col" class="item-seller<?= isset($columns->seller->is_shown) 
                                          ? ($columns->seller->is_shown ? '' : ' d-none') 
                                            : '' ?>" nowrap>
            <button type="button" class="btn btn-link text-primary copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->seller ?>
        </td>
        <td scope="col" class="item-last_update<?= isset($columns->last_update->is_shown) 
                                               ? ($columns->last_update->is_shown ? '' : ' d-none') 
                                                 : '' ?>" nowrap>
            <?= $item->last_update ?>
        </td>
        <td scope="col" class="item-price<?= isset($columns->price->is_shown) 
                                         ? ($columns->price->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap>


            <button type="button" class="btn btn-link text-primary copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            $<?= $item->price ?>
        </td>
        <td scope="col" class="item-total_sold<?= isset($columns->total_sold->is_shown) 
                                              ? ($columns->total_sold->is_shown ? '' : ' d-none') 
                                                : '' ?>" nowrap>
            <button type="button" class="btn btn-link text-primary copy-field">
                <a href="<?= $item->history_purchase_link ?>" target="_blank" rel="noopener noreferrer">
                    <i class="fa fa-history" aria-hidden="true"></i>
                </a>
            </button>$<?= $item->total_sold ?>
        </td>
        <td scope="col" class="item-rwidgets<?= isset($columns->rwidgets->is_shown) 
                                            ? ($columns->rwidgets->is_shown ? '' : ' d-none') 
                                              : '' ?>" nowrap>
            <button type="button" class="btn btn-link btn-lg view-description" data-toggle="modal"
                    data-target="#viewDescriptionModal">
                <i class="fas fa-eye view-description" aria-hidden="true"></i>
            </button>
        </td>
        <td scope="col" class="item-category<?= isset($columns->category->is_shown) 
                                            ? ($columns->category->is_shown ? '' : ' d-none') 
                                              : '' ?>">
            <?= $item->category_name ?>
        </td>
        <td scope="col" class="item-condition<?= isset($columns->condition->is_shown) 
                                             ? ($columns->condition->is_shown ? '' : ' d-none') 
                                               : '' ?>">
            <?= $item->condition_display_name ?>
        </td>
        <td scope="col" class="item-shipping-type<?= isset($columns->shipping_type->is_shown) 
                                                 ? ($columns->shipping_type->is_shown ? '' : ' d-none') 
                                                   : '' ?>">
            <?= $item->shipping_type ?>
        </td>
        <td scope="col" class="item-ship-to-locations<?= isset($columns->ship_to_locations->is_shown) 
                                                     ? ($columns->ship_to_locations->is_shown ? '' : ' d-none') 
                                                       : '' ?>">
            <?= $item->ship_to_locations ?>
        </td>
        <td scope="col" class="item-expedited<?= isset($columns->expedited->is_shown) 
                                             ? ($columns->expedited->is_shown ? '' : ' d-none') 
                                               : '' ?>">
            <?php if ($item->expedited_shipping == 0): ?>
                No
            <?php elseif ($item->expedited_shipping == 1): ?>
                Yes
            <?php endif; ?>
        </td>
        <td scope="col" class="item-one-day<?= isset($columns->one_day->is_shown) 
                                           ? ($columns->one_day->is_shown ? '' : ' d-none') 
                                             : '' ?>">
            <?php if ($item->one_day_shipping_available == 0): ?>
                No
            <?php elseif ($item->one_day_shipping_available == 1): ?>
                Yes
            <?php endif; ?>
        </td>
		<td scope="col" class="item-top<?= isset($columns->top->is_shown) 
                                       ? ($columns->top->is_shown ? '' : ' d-none') 
                                         : '' ?>">
            <?php if ($item->top_rated_listing == 0): ?>
                No
            <?php elseif ($item->top_rated_listing == 1): ?>
                Yes
            <?php endif; ?>
        </td>
		<td scope="col" class="item-handling-time<?= isset($columns->handling_time->is_shown) 
                                                 ? ($columns->handling_time->is_shown ? '' : ' d-none') 
                                                   : '' ?>">
            <?php if ($item->handling_time != -1): ?>
                <?= $item->handling_time ?>
            <?php endif; ?>
        </td>
		<td scope="col" class="item-selling-state<?= isset($columns->selling_state->is_shown) 
                                                 ? ($columns->selling_state->is_shown ? '' : ' d-none') 
                                                   : '' ?>"><?= $item->selling_state ?></td>
    </tr>
<?php endforeach; ?>
