<div class="card">
    <div class="card-body">
		<h3>Result: <?= $search_item ?></h3>
        
        <strong>Total items: </strong> <?= $item_count ?> <strong>-</strong>
		<strong>Low: </strong> <?= $low ?> <strong>-</strong>
		<strong>High: </strong> <?= $high ?> <strong>-</strong>
		<strong>Average: </strong> <?= number_format($average, 2) ?> <strong>-</strong>
		<strong>Median: </strong> <?= number_format($median, 2) ?>
    </div>
</div>
