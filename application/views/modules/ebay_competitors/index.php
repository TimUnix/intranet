<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<div class="modal fade" id="filterModal" tabindex="-1" role="dialog"
     aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content"">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filters</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="conditions">Condition</label>
                    <select class="filters form-control" id="conditions"
                            name="conditions[]" multiple="multiple"
                            data-width="100%" data-allow-clear="true"
                            data-placeholder="Choose Conditions">
                        <?php foreach ($data['conditions'] as $condition): ?>
                            <option value="<?= $condition->name ?>">
                                <?= $condition->name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories">Categories</label>
                    <select class="filters form-control" id="categories"
                            name="categories[]" multiple="multiple"
                            data-width="100%" data-allow-clear="true"
                            data-placeholder="Choose Categories">
                        <?php foreach ($data['categories'] as $categories): ?>
                            <option value="<?= $categories->name ?>">
                                <?= $categories->name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnApplyFilters">Apply Filters</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="viewDescriptionModal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="viewDescriptionModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewDescriptionModalLabel">View Rwidgets</h5>
                
            </div>
            <div class="modal-body">
                <div style="display: none;"><strong>ID:</strong> <span class="current-id"></span></div>
                
                
                <div id="view"></div>
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="padding-bottom: 60px;">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <form id="searchForm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <input class="form-control input-lg"
                                       type="search" id="itemSearch"
                                       placeholder="Search eBay Item">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="searchHistory">Search History:</label>
                            <select class="form-control" id="searchHistory"
                                    style="width: 100%;" 
                                    name="searchHistory[]">
                                <option disabled selected></option>
                                <?php foreach ($data['search_history'] as $history): ?>
                                    <option value="<?= $history->search; ?>">
                                        <?= $history->search; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="seller">Seller:</label>
                            <select class="form-control" id="seller" tabindex="-1" aria-hidden="true">
                                <option value="">All</option>
                                <?php foreach ($data['sellers'] as $seller): ?>
                                    <option value="<?= $seller->store ?>">
                                        <?= $seller->store ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="sortOrder">Sort Order:</label>
                            <select class="form-control" id="sortOrder"
                                    style="width: 100%;" tabindex="-1"
                                    aria-hidden="true">
                                <option value="ASC">Ascending</option>
                                <option value="DESC" selected>Descending</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="orderBy">Order By:</label>
                            <select class="form-control" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true"
                                    id="orderBy">
                                <option value="category_name">Category</option>
                                <option value="condition_display_name">Condition</option>
                                <option value="price">Price</option>
                                <option value="seller">Seller</option>
                                <option value="last_update" selected>Last Update</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row mt-4">
                <div class="col-md-1 text-center d-flex h-100">
                    <button type="button"
                            class="btn btn-primary btn-copy-textarea">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                        Copy
                    </button>
                </div>
                <div class="col-md-11">
                    <textarea class="textarea-copy form-control"
                              rows="4" name="copy-area">
                    </textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="summary"></div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items</h3>
            <div class="card-tools">
            


            <div class="btn-group ml-2" style="padding-right: 10px;">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" checked class="custom-control-input" id="activeswitch">
                            <label class="custom-control-label" for="activeswitch">Actives</label>
                        </div>
                    </div>

                <button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <div class="btn-group ml-2" id="dropdownContainer">
                    <button class="btn btn-outline-primary btn-sm dropdown-toggle" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>
                        Show/Hide Columns
                    </button>
                    <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="item-number"
                                   <?= isset($data['columns']->item_number->is_shown) ?
                                       ($data['columns']->item_number->is_shown ? 'checked' : '') : 'checked' ?>>
                            Item Number
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="image"
                                   <?= isset($data['columns']->image->is_shown) ?
                                       ($data['columns']->image->is_shown ? 'checked' : '') : 'checked' ?>>
                            Image
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="title"
                                   <?= isset($data['columns']->title->is_shown) ?
                                       ($data['columns']->title->is_shown ? 'checked' : '') : 'checked' ?>>
                            Title
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="seller"
                                   <?= isset($data['columns']->seller->is_shown) ?
                                       ($data['columns']->seller->is_shown ? 'checked' : '') : 'checked' ?>>
                            Seller
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="price"
                                   <?= isset($data['columns']->price->is_shown) ?
                                       ($data['columns']->price->is_shown ? 'checked' : '') : 'checked' ?>>
                            Price
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="price"
                                   <?= isset($data['columns']->price->is_shown) ?
                                       ($data['columns']->price->is_shown ? 'checked' : '') : 'checked' ?>>
                            Price
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="rwidgets"
                                   <?= isset($data['columns']->rwidgets->is_shown) ?
                                       ($data['columns']->rwidgets->is_shown ? 'checked' : '') : 'checked' ?>>
                            Rwidgets
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="condition"
                                   <?= isset($data['columns']->condition->is_shown) ?
                                       ($data['columns']->condition->is_shown ? 'checked' : '') : 'checked' ?>>
                            Condition
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="shipping-type"
                                   <?= isset($data['columns']->shipping_type->is_shown) ?
                                       ($data['columns']->shipping_type->is_shown ? 'checked' : '') : 'checked' ?>>
                            Shipping Type
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="ship-to-locations"
                                   <?= isset($data['columns']->ship_to_locations->is_shown) ?
                                       ($data['columns']->ship_to_locations->is_shown ? 'checked' : '') : 'checked' ?>>
                            Ship to Locations
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="expedited"
                                   <?= isset($data['columns']->expedited->is_shown) ?
                                       ($data['columns']->expedited->is_shown ? 'checked' : '') : 'checked' ?>>
                            Expedited
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="one-day"
                                   <?= isset($data['columns']->one_day->is_shown) ?
                                       ($data['columns']->one_day->is_shown ? 'checked' : '') : 'checked' ?>>
                            One Day
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="top"
                                   <?= isset($data['columns']->top->is_shown) ?
                                       ($data['columns']->top->is_shown ? 'checked' : '') : 'checked' ?>>
                            Top
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="handling-time"
                                   <?= isset($data['columns']->handling_time->is_shown) ?
                                       ($data['columns']->handling_time->is_shown ? 'checked' : '') : 'checked' ?>>
                            Handling Time
                        </div>
                        <div class="dropdown-item columnSelector">
                            <input type="checkbox" class="columnCheckbox mr-1" name="selling-state"
                                   <?= isset($data['columns']->selling_state->is_shown) ?
                                       ($data['columns']->selling_state->is_shown ? 'checked' : '') : 'checked' ?>>
                            Selling State
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-sm btn-outline-primary ml-2"
                        data-toggle="modal" data-target="#filterModal">
                    <i class="fa fa-filter" aria-hidden="true"></i>
                    Filters
                </button>
            </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body p-0 items-container table-responsive">
            <table class="table table-bordered table-striped table-hover paginated" id="product-table" 
                   style="border-collapse: separate;" cellspacing="0">
                <thead class="bg-white border">
                    <tr>
                        <th scope="col" nowrap>&#35;</th>
                        <th scope="col" nowrap><input class="check-all-toggle" type="checkbox"></th>
                        <th scope="col" nowrap>
                            <button type="button" class="btn btn-link btn-batch-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                        </th>
                        <th scope="col"
                            class="header-item-number<?= isset($data['columns']->item_number->is_shown) 
                                                     ? ($data['columns']->item_number->is_shown ? '' : ' d-none') 
                                                       : '' ?>" nowrap>
                                                       <button type="button" class="btn btn-link btn-des-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Item Number
                        </th>
                        <th scope="col" class="header-image<?= isset($data['columns']->image->is_shown) 
                                                           ? ($data['columns']->image->is_shown ? '' : ' d-none') 
                                                             : '' ?>" nowrap>
                            Image
                        </th>
                        <th scope="col" class="header-title<?= isset($data['columns']->title->is_shown) 
                                                           ? ($data['columns']->title->is_shown ? '' : ' d-none') 
                                                             : '' ?>" nowrap>
                                <button type="button" class="btn btn-link btn-des-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Title&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        </th>
                        <th scope="col" class="header-seller<?= isset($data['columns']->seller->is_shown) 
                                                            ? ($data['columns']->seller->is_shown ? '' : ' d-none') 
                                                              : '' ?>" nowrap>
                            <button type="button" class="btn btn-link btn-bar-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Seller
                        </th>
                        <th scope="col" class="header-last_update<?= isset($data['columns']->last_update->is_shown) 
                                                           ? ($data['columns']->last_update->is_shown ? '' : ' d-none') 
                                                             : '' ?>" nowrap>
                                                             <button type="button" class="btn btn-link btn-des-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Last Update
                        </th>
                        <th scope="col" class="header-price<?= isset($data['columns']->price->is_shown) 
                                                           ? ($data['columns']->price->is_shown ? '' : ' d-none') 
                                                             : '' ?>" nowrap>
                                                             <button type="button" class="btn btn-link btn-des-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Price
                        </th>
                        <th scope="col" class="header-total_sold<?= isset($data['columns']->total_sold->is_shown) 
                                                           ? ($data['columns']->total_sold->is_shown ? '' : ' d-none') 
                                                             : '' ?>" nowrap>
                                                             <button type="button" class="btn btn-link btn-des-copy text-primary">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Total Sold
                        </th>
                        <th scope="col" class="header-rwidgets<?= isset($data['columns']->rwidgets->is_shown) 
                                                           ? ($data['columns']->rwidgets->is_shown ? '' : ' d-none') 
                                                             : '' ?>" nowrap>
                                                             
                            Rwidgets
                        </th>
                        <th scope="col" class="header-category<?= isset($data['columns']->category->is_shown) 
                                                              ? ($data['columns']->category->is_shown ? '' : ' d-none') 
                                                                : '' ?>" nowrap>
                            Category
                        </th>
                        <th scope="col" class="header-condition<?= isset($data['columns']->condition->is_shown) 
                                                               ? ($data['columns']->condition->is_shown ? '' : ' d-none') 
                                                                 : '' ?>" nowrap>
                            Condition
                        </th>
                        <th scope="col" class="header-shipping-type<?= isset($data['columns']->shipping_type->is_shown) 
                                                                   ? ($data['columns']->shipping_type->is_shown ? '' : ' d-none') 
                                                                     : '' ?>" nowrap>
                            Shipping Type
                        </th>
                        <th scope="col" class="header-ship-to-locations<?= isset($data['columns']->ship_to_locations->is_shown) 
                                                                       ? ($data['columns']->ship_to_locations->is_shown ? '' : ' d-none') 
                                                                         : '' ?>" nowrap>
                            Ship to Locations
                        </th>
                        <th scope="col" class="header-expedited<?= isset($data['columns']->expedited->is_shown) 
                                                               ? ($data['columns']->expedited->is_shown ? '' : ' d-none') 
                                                                 : '' ?>" nowrap>
                            <button type="button" class="btn btn-link text-dark" data-toggle="tooltip"
                                    data-placement="bottom" title="Expedited Shipping" style="padding: 0;">
                                <strong>Expedited</strong>
                            </button>
                        </th>
                        <th scope="col" class="header-one-day<?= isset($data['columns']->one_day->is_shown) 
                                                             ? ($data['columns']->one_day->is_shown ? '' : ' d-none') 
                                                               : '' ?>" nowrap>
                            <button type="button" class="btn btn-link text-dark" data-toggle="tooltip"
                                    data-placement="bottom" title="One Day Shipping Available" style="padding: 0;">
                                <strong>One Day</strong>
                            </button>
                        </th>
                        <th scope="col" class="header-top<?= isset($data['columns']->top->is_shown) 
                                                         ? ($data['columns']->top->is_shown ? '' : ' d-none') 
                                                           : '' ?>" nowrap>
                            <button type="button" class="btn btn-link text-dark" data-toggle="tooltip"
                                    data-placement="bottom" title="Top Rated Listing" style="padding: 0;">
                                <strong>Top</strong>
                        </th>
                        <th scope="col" class="header-handling-time<?= isset($data['columns']->handling_time->is_shown) 
                                                                   ? ($data['columns']->handling_time->is_shown ? '' : ' d-none') 
                                                                     : '' ?>" nowrap>Handling Time</th>
                        <th scope="col" class="header-selling-state<?= isset($data['columns']->selling_state->is_shown) 
                                                                   ? ($data['columns']->selling_state->is_shown ? '' : ' d-none') 
                                                                     : '' ?>" nowrap>Selling State</th>
                    </tr>
                </thead>
                <tbody id="product_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('templates/common/spinner'); ?>

<script id="ebayScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/ebay.js"></script>

<script type="text/javascript" src="<?= base_url() ?>resources/js/batchCopy.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/exportebaycompetitorsCsv.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/fieldebayCopy.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/rowebayCopy.js"></script>

<?php $this->load->view('templates/common/footer'); ?>


