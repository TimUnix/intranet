<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td scope="col" data-value="<?= $item->build_id ?>"class="item-build_id<?= isset($columns->build_id->is_shown) 
                                         ? ($columns->build_id->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap><?= $item->build_id ?></td>
        <td scope="col" data-value="<?= $item->chassis ?>" class="item-chassis<?= isset($columns->chassis->is_shown)
                                          ? ($columns->chassis->is_shown ? '' : ' d-none') 
                                            : '' ?>" nowrap>
            <?php if($item->chassis == null){ ?>
             <?=   $item->motherboard ?> <?= $item->cpu ?> <?= $item->memory ?>
          <?php  }else{ ?>
            <?=    $item->chassis ?>
          <?php  } ?> </td>
        <td scope="col" class="item-sales_rep<?= isset($columns->sales_rep->is_shown) 
                                       ? ($columns->sales_rep->is_shown ? '' : ' d-none') 
                                         : '' ?>" nowrap><?= $item->sales_rep ?></td>
        <td scope="col" class="text-center">
            <?php if($item->chassis != null){ ?>

            <button type="button" class="btn btn-link btn-lg edit-item" class="edit_row" data-toggle="modal" data-target="#editChassis">
                <i class="fas fa-pen edit-item" aria-hidden="true"></i>
            </button>
        <?php } else{ ?>
                 <button type="button" class="btn btn-link btn-lg edit-item" class="edit_row" data-toggle="modal" data-target="#editMotherboard">
                <i class="fas fa-pen edit-item" aria-hidden="true"></i>
            </button>
        <?php  } ?>
            <button type="button" class="btn btn-link btn-lg deleteBtn" data-toggle="modal" data-target="#confirmDelete">
                <i class="fas fa-trash" aria-hidden="true"></i>
            </button>
        </td>
        <td scope="col" class="item-created_date<?= isset($columns->created_date->is_shown) 
                                            ? ($columns->created_date->is_shown ? '' : ' d-none') 
                                              : '' ?>" nowrap><?= $item->created_date ?></td>
        <td scope="col" data-value="<?= $item->backplane ?>" class="item-backplane" style="display:none;"><?= $item->backplane ?></td>
        <td scope="col" data-value="<?= $item->drivebays ?>" class="item-drivebays" style="display:none;"><?= $item->drivebays ?></td>
        <td scope="col" data-value="<?= $item->rearbays ?>" class="item-rearbays" style="display:none;"><?= $item->rearbays ?></td>
        <td scope="col" data-value="<?= $item->ff ?>" class="item-ff" style="display:none;"><?= $item->ff ?></td>
        <td scope="col" data-value="<?= $item->rff ?>" class="item-rff" style="display:none;"><?= $item->rff ?></td>
        <td scope="col" data-value="<?= $item->node ?>" class="item-node" style="display:none;"><?= $item->node ?></td>
        <td scope="col" data-value="<?= $item->psu ?>" class="item-psu" style="display:none;"><?= $item->psu ?></td>
        <td scope="col" data-value="<?= $item->bin ?>" class="item-bin" style="display:none;"><?= $item->bin ?></td>
        <td scope="col" data-value="<?= $item->front_caddies ?>" class="item-front_caddies" style="display:none;"><?= $item->front_caddies ?></td>
        <td scope="col" data-value="<?= $item->fc_qty ?>" class="item-fc_qty" style="display:none;"><?= $item->fc_qty ?></td>
        <td scope="col" data-value="<?= $item->rear_caddies ?>" class="item-rear_caddies" style="display:none;"><?= $item->rear_caddies ?></td>
        <td scope="col" data-value="<?= $item->rc_qty ?>" class="item-rc_qty" style="display:none;"><?= $item->rc_qty ?></td>
        <td scope="col" data-value="<?= $item->drives ?>" class="item-drives" style="display:none;"><?= $item->drives ?></td>
        <td scope="col" data-value="<?= $item->drives_qty ?>" class="item-drives_qty" style="display:none;"><?= $item->drives_qty ?></td>
        <td scope="col" data-value="<?= $item->drives2 ?>" class="item-drives2" style="display:none;"><?= $item->drives2 ?></td>
        <td scope="col" data-value="<?= $item->drives_qty2 ?>" class="item-drives_qty2" style="display:none;"><?= $item->drives_qty2 ?></td>
        <td scope="col" data-value="<?= $item->drives3 ?>" class="item-drives3" style="display:none;"><?= $item->drives3 ?></td>
        <td scope="col" data-value="<?= $item->drives_qty3 ?>" class="item-drives_qty3" style="display:none;"><?= $item->drives_qty3 ?></td>
        <td scope="col" data-value="<?= $item->motherboard ?>" class="item-motherboard" style="display:none;"><?= $item->motherboard ?></td>
        <td scope="col" data-value="<?= $item->motherboard_qty ?>" class="item-motherboard_qty" style="display:none;"><?= $item->motherboard_qty ?></td>
        <td scope="col" data-value="<?= $item->cpu ?>" class="item-cpu" style="display:none;"><?= $item->cpu ?></td>
        <td scope="col" data-value="<?= $item->cpu_qty ?>" class="item-cpu_qty" style="display:none;"><?= $item->cpu_qty ?></td>
         <td scope="col" data-value="<?= $item->memory ?>" class="item-memory" style="display:none;"><?= $item->memory ?></td>
        <td scope="col" data-value="<?= $item->memory_qty ?>" class="item-memory_qty" style="display:none;"><?= $item->memory_qty ?> </td>
        <td scope="col" data-value="<?= $item->ob_nic ?>" class="item-ob_nic" style="display:none;"><?= $item->ob_nic ?> </td>
        <td scope="col" data-value="<?= $item->pcie_slot ?>" class="item-pcie_slot" style="display:none;"><?= $item->pcie_slot ?> </td>
        <td scope="col" data-value="<?= $item->slot ?>" class="item-slot" style="display:none;"><?= $item->slot ?></td>
        <td scope="col" data-value="<?= $item->slot_qty ?>" class="item-slot_qty" style="display:none;"><?= $item->slot_qty ?></td>
        <td scope="col" data-value="<?= $item->slot2 ?>" class="item-slot2" style="display:none;"><?= $item->slot2 ?></td>
        <td scope="col" data-value="<?= $item->slot_qty2 ?>" class="item-slot_qty2" style="display:none;"><?= $item->slot_qty2 ?></td>
        <td scope="col" data-value="<?= $item->slot3 ?>" class="item-slot3" style="display:none;"><?= $item->slot3 ?></td>
        <td scope="col" data-value="<?= $item->slot_qty3 ?>" class="item-slot_qty3" style="display:none;"><?= $item->slot_qty3 ?></td>
         <td scope="col" data-value="<?= $item->psu_watts ?>" class="item-psu_watts" style="display:none;"><?= $item->psu_watts ?></td>
        <td scope="col" data-value="<?= $item->psu_qty ?>" class="item-psu_qty" style="display:none;"><?= $item->psu_qty ?></td>
        <td scope="col" data-value="<?= $item->mounting ?>" class="item-mounting" style="display:none;"><?= $item->mounting ?></td>
        <td scope="col" data-value="<?= $item->mounting_qty ?>" class="item-mounting_qty" style="display:none;"><?= $item->mounting_qty ?></td>
        <td scope="col" data-value="<?= $item->warranty ?>" class="item-warranty" style="display:none;"><?= $item->warranty ?></td>
    </tr>
<?php endforeach; }?>