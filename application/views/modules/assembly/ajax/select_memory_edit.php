<select class='form-control form-control-sm' id='memory_edit'>
<?php

foreach($memory as $data){ ?>
    
   <option class="memory_option_edit" value="<?= $data->brand ?> <?= $data->memory_family ?> <?= $data->barcode ?>"><?= $data->brand ?> <?= $data->memory_family ?> <?= $data->barcode ?></option>
    
<?php 
}

?>
</select>
<script>
$(document).ready(function() {
    $('#memory_edit').select2({
        width: 440,
        tags: true
    });
 });
</script>