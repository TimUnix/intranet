<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<!-- delete confirmation -->
<div class="modal fade" id="confirmDelete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0275d8">
                <div >
                <h3 style="color:white">Delete Confirmation</h3>
                </div>
            </div>
            <div class="modal-body">
                <div style="text-align:center;">
                    <i class="fas fa-exclamation-triangle fa-5x" style="color:#df4759"></i>
                </div><br>
                <div style="text-align:center;">
                    <h5>Are you sure you want to delete?</h5><br>
                    <button class="btn btn-primary btn-lg" id="delete_row"> Yes </button>
                    <button class="btn btn-secondary btn-lg" data-dismiss="modal" id="close_modal"> No </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- include chassis -->
<button id="insertListing" type="button" class="btn btn-info btn-lg ml-2 mb-4" data-toggle="modal"
        data-target="#myModal">
    Assemble
</button>

<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
                <div class="alert alert-danger alert-dismissible" id="caddies_error" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Quantity exceeds number of bays.</h5>
                </div>
                <div class="form-row">
                   <strong class="col-2">Build ID:</strong><span class="col-2" id="build_id"><?= $data['build_id'] ?> </span>
                   <strong class="col-2">Sales Rep:</strong><span class="col-2" id="sales_rep"><?= $data['user_firstname'] ?></span>
                  <button class="col-4 btn btn-secondary btn-sm" id="remove_chassis">Motherboard only</button>
                </div><br>
                 <div id="chassis_all">
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>CHASSIS</strong></div><br>
                <div class="form-row" id="chassis_all">
                    <label for="chassis" class="col-2">Chassis</label>
                    <div class="col-10">
                        <select id="chassis" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['chassis'] as $chassis): ?>
                            <option value="<?= $chassis->part_number ?>">
                                <?= $chassis->part_number; ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                </div>
                <div>
                <div class="form-row">
                    <strong class="col-3">Backplane:</strong><span class="col-3" id="backplane"></span>
                    <strong class="col-3">Nodes:</strong> <span class="col-3" id="node"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Drive Bays:</strong><span class="col-3" id="drivebays"></span>
                    <strong class="col-3">PSU Slots:</strong> <span class="col-3" id="psu"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Rear Bays:</strong> <span class="col-3" id="rearbays"></span>
                    <strong class="col-3">Bin:</strong> <span class="col-3" id="bin"></span>
                </div>
                <span class="col-3" id="mfr" style="display:none;"></span>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>HARD DRIVES</strong></div><br>
                <div class="form-row">
                    <label for="front_caddies" class="col-2">Front Caddies</label>
                    <div class="col-7">
                        <select id ="front_caddies" name="front_caddies" class="form-control form-control-sm">
                            <option value="null"></option>
                        </select>
                    </div>
                    <label for="front_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="fc_qty">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="rear_caddies" class="col-2">Rear Caddies</label>
                    <div class="col-7">
                        <select id ="rear_caddies" name="rear_caddies" class="form-control form-control-sm">
                            <option value="null"></option>
                        </select>
                    </div>
                    <label for="rear_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="rc_qty">
                    </div>
                </div><br>
                <div class="form-row drives_list">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives">+</button>
                    </div>      
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-7 clonedInput" name="drives_name[]">
                        <select id="drives1" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-2 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="drives_qty">
                    </div>
                </div><br>
                <div class="form-row drives_list_copy" style="display:none;">
                     <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives2">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-7 clonedInput" name="drives_name[]">
                        <select id="drives2" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="drives_qty2">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row drives_list_copy2" style="display:none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives3">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-7 clonedInput" name="drives_name[]">
                        <select id="drives3" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="drives_qty3">
                    </div>
                     <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div></div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong>
                </div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-7" id="motherboard_chassis">
                        <select id="motherboard" class="form-control form-control-sm">
                         
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="motherboard_qty">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-7" id="cpu_motherboard">
                        <select id="cpu" class="form-control form-control-sm">
                            
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="cpu_qty">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-7" id="memory_motherboard">
                       <select id="memory" class="form-control form-control-sm">

                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="memory_qty">
                    </div>
                </div><br>
                <div class="form-row">
                   <label for="ob_nic" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic"></span>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="pcie_slot" class="col-2">PCIE Slot</label>
                    <div class="col-7">
                       <span id="pcie_slot"></span>
                    </div>
                </div><br>
                <div class="form-row slot_list">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty">
                    </div>
                </div><br>
                
                <div class="form-row slot_list_copy" style="display:none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller2">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot2">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty2">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>

                <div class="form-row slot_list_copy2" style="display: none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller3">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot3">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty3">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                    
                </div><br>

                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-7">
                        <input type="text" class="form-control form-control-sm" id="psu_watts">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="psu_qty">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-7">
                        <select id="mounting" class="form-control form-control-sm">
                            <?php foreach ($data['mountings'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="mounting_qty">
                    </div>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty" class="form-control form-control-sm">
                            <?php foreach ($data['warranty'] as $warranty): ?>
                            <option value="<?= $warranty->name ?>"
                                <?= $warranty->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranty->name; ?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div><hr><br>
                <div class="form-row justify-content-between">
                    <button type="button" class="col-4 btn btn-primary btn-sm" id="generate_ebayListing">Generate eBay Listing</button>
                    <button type="button" class="col-4 btn btn-primary btn-sm" id="generate_genListing">Generate Generic Listing</button>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" id="close_add_chassis">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editChassis" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="error_edit_chassis"></div>
                <div class="alert alert-danger alert-dismissible" id="caddies_error_edit" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Quantity exceeds number of bays.</h5>
                </div>
                <div class="form-row">
                   <strong class="col-2">Build ID:</strong><span class="col-2" id="build_id_edit"></span>
                   <strong class="col-2">Sales Rep:</strong><span class="col-2" id="sales_rep_edit"><?= $data['user_firstname'] ?></span>
                </div><br>
                 <div id="chassis_all">
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>CHASSIS</strong></div><br>
                <div class="form-row" id="chassis_all">
                    <label for="chassis" class="col-2">Chassis</label>
                    <div class="col-10">
                        <select id="chassis_edit" class="form-control form-control-sm">

                            <?php foreach ($data['chassis'] as $chassis): ?>
                            <option value="<?= $chassis->part_number ?>">
                                <?= $chassis->part_number; ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                </div>
                <div>
                <div class="form-row">
                    <strong class="col-3">Backplane:</strong><span class="col-3" id="backplane_edit"></span>
                    <strong class="col-3">Nodes:</strong> <span class="col-3" id="node_edit"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Drive Bays:</strong><span class="col-3" id="drivebays_edit"></span>
                    <strong class="col-3">PSU Slots:</strong> <span class="col-3" id="psu_edit"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Rear Bays:</strong> <span class="col-3" id="rearbays_edit"></span>
                    <strong class="col-3">Bin:</strong> <span class="col-3" id="bin_edit"></span>
                </div>
                <span class="col-3" id="mfr" style="display:none;"></span>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>HARD DRIVES</strong></div><br>
                <div class="form-row">
                    <label for="front_caddies" class="col-2">Front Caddies</label>
                    <div class="col-7">
                        <select id ="front_caddies_edit" name="front_caddies_edit" class="form-control form-control-sm">
                            <option class="current-frontcaddies"></option>
                        </select>
                    </div>
                    <label for="front_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="fc_qty_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="rear_caddies" class="col-2">Rear Caddies</label>
                    <div class="col-7">
                        <select id ="rear_caddies_edit" name="rear_caddies_edit" class="form-control form-control-sm current-rearcaddies">
                            <option class="current-rearcaddies"></option>
                        </select>
                    </div>
                    <label for="rear_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="rc_qty_edit">
                    </div>
                </div><br>
                <div class="form-row drives_list">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives">+</button>
                    </div>      
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-7 clonedInput" name="drives_name[]">
                        <select id="drives1_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-2 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="drives_qty_edit">
                    </div>
                </div><br>
                <div class="form-row drives_list_copy">
                     <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives2">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-7 clonedInput" name="drives_name[]">
                        <select id="drives2_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="drives_qty2_edit">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row drives_list_copy2">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives3">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-7 clonedInput" name="drives_name[]">
                        <select id="drives3_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="drives_qty3_edit">
                    </div>
                     <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div></div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong>
                </div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-7" id="motherboard_chassis_edit">
                        <select id="motherboard_edit" class="form-control form-control-sm">
                            <option class="current-motherboard"></option>
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="motherboard_qty_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-7" id="cpu_motherboard_edit">
                        <select id="cpu_edit" class="form-control form-control-sm">
                            <option class="current-cpu" id="cpu_edit"></option>
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="cpu_qty_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-7" id="memory_motherboard_edit">
                       <select id="memory_edit" class="form-control form-control-sm">
                            <option class="current-memory" id="memory_edit"></option>
                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="memory_qty_edit">
                    </div>
                </div><br>
                 <div class="form-row">
                   <label for="ob_nic" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic_edit"></span>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="pcie_slot" class="col-2">PCIE Slot</label>
                    <div class="col-7">
                       <span id="pcie_slot_edit"></span>
                    </div>
                </div><br>
                <div class="form-row slot_list">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty_edit">
                    </div>
                </div><br>
                
                <div class="form-row slot_list_copy">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller2">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot2_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty2_edit">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>

                <div class="form-row slot_list_copy2">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller3">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot3_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty3_edit">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                    
                </div><br>

                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-7">
                        <input type="text" class="form-control form-control-sm" id="psu_watts_edit">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="psu_qty_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-7">
                        <select id="mounting_edit" class="form-control form-control-sm">
                            <?php foreach ($data['mountings'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="mounting_qty_edit">
                    </div>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty_edit" class="form-control form-control-sm">
                            <?php foreach ($data['warranty'] as $warranty): ?>
                            <option value="<?= $warranty->name ?>"
                                <?= $warranty->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranty->name; ?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div><hr><br>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" id="edit_chassis_close">Close</button>
                <button type="button" class="btn btn-primary update_data" id="updateChassis">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="motherboard_modal" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="motherboard_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success_mb"></div>
                <div class="form-row">
                   <strong class="col-2">Build ID:</strong><span class="col-2" id="build_id2"><?= $data['build_id'] ?></span>
                   <strong class="col-2">Sales Rep:</strong><span class="col-2" id="sales_rep2"><?= $data['user_firstname'] ?></span>
                  <button class="col-4 btn btn-secondary btn-sm" id="include_chassis">Include Chassis</button>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong>
                </div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-7">
                        <select id="motherboard2" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['motherboard'] as $motherboard): ?>
                            <option value="<?= $motherboard->model ?>">
                                <?= $motherboard->model; ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="motherboard_qty2">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-7" id="cpu_motherboard2">
                        <select id="cpu2" class="form-control form-control-sm">
    
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="cpu_qty2">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-7" id="memory_motherboard2">
                       <select id="memory2" class="form-control form-control-sm">

                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="memory_qty2">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="ob_nic" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic_mb"></span>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="pcie_slot" class="col-2">PCIE Slot</label>
                    <div class="col-7">
                       <span id="pcie_slot_mb"></span>
                    </div>
                </div><br>
                <div class="form-row slot_list_mb">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot_mb">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty_mb">
                    </div>
                </div><br>

                <div class="form-row slot_list_mb2" style="display: none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb2">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot_mb2">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty_mb2">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot_mb2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                    
                </div><br>

                <div class="form-row slot_list_mb3" style="display: none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb3">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="slot_mb3">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="slot_qty_mb3">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot_mb3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                    
                </div><br>

                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-7">
                        <input type="text" class="form-control form-control-sm" id="psu_watts2">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="psu_qty2">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-7">
                        <select id="mounting2" class="form-control form-control-sm">
                            <?php foreach ($data['mountings'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="mounting_qty2">
                    </div>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty2" class="form-control form-control-sm">
                            <?php foreach ($data['warranty'] as $warranty): ?>
                            <option value="<?= $warranty->name ?>"
                                <?= $warranty->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranty->name; ?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div><br>
                <div class="form-row justify-content-between">
                    <button type="button" class="col-4 btn btn-primary btn-sm" id="generateMB_ebayListing">Generate eBay Listing</button>
                   <button type="button" class="col-4 btn btn-primary btn-sm" id="generateMB_genListing">Generate Generic Listing</button>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_motherboard">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editMotherboard" tabindex="-1" role="dialog" aria-hidden="true"
     aria-labelledby="motherboard_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="edit_error_mb"></div>
                <div class="form-row">
                   <strong class="col-2">Build ID:</strong><span class="col-2  current-buildID" id="build_id2_edit"></span>
                   <strong class="col-2">Sales Rep:</strong><span class="col-2" id="sales_rep2"><?= $data['user_firstname'] ?></span>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong>
                </div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-7">
                        <select id="motherboard2_edit" class="form-control form-control-sm current-motherboardmb">
                            <?php foreach ($data['motherboard'] as $motherboard): ?>
                            <option value="<?= $motherboard->model ?>">
                                <?= $motherboard->model; ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm current-motherboard_qty" id="motherboard_qty2_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-7" id="cpu_motherboard_mb_edit">
                        <select id="cpu2_edit" class="form-control form-control-sm">
                            <option class="current-cpu" id="cpu2_edit"></option>
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm current-cpu_qty" id="cpu_qty2_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-7" id="memory_motherboard_mb_edit">
                       <select id="memory2_edit" class="form-control form-control-sm ">
                            <option class="current-memory" id="memory2_edit"></option>
                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm current-memory_qty" id="memory_qty2_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="ob_nic" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic_mb_edit"></span>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="pcie_slot" class="col-2">PCIE Slot</label>
                    <div class="col-7">
                       <span id="pcie_slot_mb_edit"></span>
                    </div>
                </div><br>
                <div class="form-row slot_list_mb">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm current-slot" id="slot_mb_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm current-slot_qty" id="slot_qty_mb_edit">
                    </div>
                </div><br>

                <div class="form-row slot_list_mb2">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb2">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm current-slot2" id="slot_mb2_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm current-slot_qty2" id="slot_qty_mb2_edit">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot_mb2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                    
                </div><br>

                <div class="form-row slot_list_mb3">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb3">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-7">
                        <input type="text" name="slot[]" class="form-control form-control-sm current-slot3" id="slot_mb3_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm current-slot_qty3" id="slot_qty_mb3_edit">
                    </div>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot_mb3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                    
                </div><br>

                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-7">
                        <input type="text" class="form-control form-control-sm current-psu_watts" id="psu_watts2_edit">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm current-psu_qty" id="psu_qty2_edit">
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-7">
                        <select id="mounting2_edit" class="form-control form-control-sm current-mounting">
                            <?php foreach ($data['mountings'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm current-mounting_qty" id="mounting_qty2_edit">
                    </div>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty2_edit" class="form-control form-control-sm current-warranty">
                            <?php foreach ($data['warranty'] as $warranty): ?>
                            <option value="<?= $warranty->name ?>"
                                <?= $warranty->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranty->name; ?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" id="edit_mb_close">Close</button>
                <button type="button" class="btn btn-primary update_data" id="updateMotherboard">Update</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ebayListing" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-row">
                    <h4 class="modal-title col-12">Listing Specifications</h4><br>
                </div>
                <h6>Performance Specification</h6>
                <div class="form-row">
                    <div class="col-12"><strong>CPU:</strong> <span id="cpu-spec-qty"></span><span id="cpu-qty-symbol"> </span> <span id="cpu-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Memory:</strong> <span id="memory-spec-qty"></span><span id="memory-qty-symbol"> </span> <span id="memory-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Hard Drives:</strong> <span id="drives-spec-qty"></span><span id="drives-qty-symbol"> </span> <span id="drives-spec"></span><span id="drives-comma2"></span> <span id="drives-spec-qty2"></span><span id="drives-qty-symbol2"> </span> <span id="drives-spec2"></span><span id="drives-comma3"></span> <span id="drives-spec-qty3"></span><span id="drives-qty-symbol3"> </span> <span id="drives-spec3"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Controller:</strong> <span id="slot-spec-qty"></span><span id="slot-qty-symbol"> </span> <span id="slot-spec"></span><span id="slot-comma2"></span> <span id="slot-spec-qty2"></span><span id="slot-qty-symbol2"> </span> <span id="slot-spec2"></span><span id="slot-comma3"></span> <span id="slot-spec-qty3"></span><span id="slot-qty-symbo3l"> </span> <span id="slot-spec3"></span></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>OB NIC:</strong> <span id="ob_nic-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Motherboard Specs:</strong> <span id="mfr-spec"></span><span id=""> </span> <span id="u-spec"></span><span id="u-symbol-spec"></span> <span id="drivebays-spec"></span> <span id="rearbays-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Motherboard:</strong> <span id="motherboard-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Backplane:</strong> <span id="backplane-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>PCI-Expansion Slots:</strong> <span id="pcie_slot-spec"></span> <span id="slot_word-spec"></span></div>
                </div>
                 <div class="form-row">
                    <div class="col-12"><strong>Drive Bays:</strong> <span id="drivebays-spec-2"></span></div>
                </div>
                 <div class="form-row">
                    <div class="col-12"><strong>Rear Bays:</strong> <span id="rearbays-spec-2"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>PSU Slot:</strong> <span id="psu-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Power:</strong> <span id="power-spec-qty"></span><span id="power-qty-symbol"> </span> <span id="power-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Rail Kit:</strong> <span id="railkit-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Warranty:</strong> <span id="warranty-spec"></span></div>
                </div><br>
                <h7>Warranty Includes 30 Days Standard Warranty covers hardware for the duration stated Expert Pre-sales consulting. With our expertise we will help you find the best solutions for your needs.Expert Post sales Technical Support and consulting - WE SET UP FOR YOUR SUCCESS</h7><br>
                <br><h6><strong>Standard Warranty and Terms of Sale</strong></h6>
                <h7>- Standard 30 day limited warranty.<br> - Item tested to power on and all specs show in BIOS, We Warranty Hardware Only, not Software.<br> - Processing times estimated 3 Days. Tracking number will be emailed after Shipped.<br> - 30 Day Hardware parts Warranty. problems must be reported 30 days of when the item is delivered.<br> - Buyer Pays for all shipping, even on returns. (shipping is non-refundable)<br> - Restocking fee as described in the terms of the listing/ order. 20% Restocking fee if not stated.<br> - If you have issues please message us with an item number and the issue you are having.<br> - Our customer service representatives will assist you on getting your problem solved</h7><br>
                <br><h6><strong>eBay Message Us</strong></h6>
                <h7>- Please have eBay item number ready when contacting.<br>- Message us directly and we will try to answer the questions within 24 hours.<br>- Phones Open from 10 AM PST to 5 PM PST Monday through Friday.<br>- Direct Contact for sales, customization, troubleshooting, technical help, RMA's</h7>
                <br><br><div class="form-row">
                    <div class="col-12"><span>Build ID:</span> <span id="build_id-spec"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Author: </span><span id="author_date-spec"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Custom Label (SKU): </span><span id="custom_label-spec"></span></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="motherboard_ebayListing" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
               <div class="form-row">
                    <h4 class="modal-title col-12">Listing Specifications</h4><br>
                </div>
                <h6>Performance Specification</h6>
                <div class="form-row">
                    <div class="col-12"><strong>CPU:</strong> <span id="cpu-spec-qty-mb"></span><span id="cpu-qty-symbol-mb"> </span> <span id="cpu-spec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Memory:</strong> <span id="memory-spec-qty-mb"></span><span id="memory-qty-symbol-mb"> </span> <span id="memory-spec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Controller:</strong> <span id="slot-spec-qty-mb"></span><span id="slot-qty-symbol-mb"> </span> <span id="slot-spec-mb"></span><span id="slot-comma-mb2"></span> <span id="slot-spec-qty-mb2"></span><span id="slot-qty-symbol-mb2"> </span> <span id="slot-spec-mb2"></span><span id="slot-comma-mb3"></span> <span id="slot-spec-qty-mb3"></span><span id="slot-qty-symbol-mb3"> </span> <span id="slot-spec-mb3"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>OB NIC:</strong> <span id="ob_nic-spec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>PCI-Expansion Slots:</strong> <span id="pcie_slot-spec-mb"></span> <span id="slot_word-spec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Power:</strong> <span id="power-spec-qty-mb"></span><span id="power-qty-symbol-mb"> </span> <span id="power-spec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Rail Kit:</strong> <span id="railkit-spec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Warranty:</strong> <span id="warranty-spec-mb"></span></div>
                </div><br>
                <h7>Warranty Includes 30 Days Standard Warranty covers hardware for the duration stated Expert Pre-sales consulting. With our expertise we will help you find the best solutions for your needs.Expert Post sales Technical Support and consulting - WE SET UP FOR YOUR SUCCESS</h7><br>
                <br><h6><strong>Standard Warranty and Terms of Sale</strong></h6>
                <h7>- Standard 30 day limited warranty.<br> - Item tested to power on and all specs show in BIOS, We Warranty Hardware Only, not Software.<br> - Processing times estimated 3 Days. Tracking number will be emailed after Shipped.<br> - 30 Day Hardware parts Warranty. problems must be reported 30 days of when the item is delivered.<br> - Buyer Pays for all shipping, even on returns. (shipping is non-refundable)<br> - Restocking fee as described in the terms of the listing/ order. 20% Restocking fee if not stated.<br> - If you have issues please message us with an item number and the issue you are having.<br> - Our customer service representatives will assist you on getting your problem solved</h7><br>
                <br><h6><strong>eBay Message Us</strong></h6>
                <h7>- Please have eBay item number ready when contacting.<br>- Message us directly and we will try to answer the questions within 24 hours.<br>- Phones Open from 10 AM PST to 5 PM PST Monday through Friday.<br>- Direct Contact for sales, customization, troubleshooting, technical help, RMA's</h7>
                <br><br><div class="form-row">
                    <div class="col-12"><span>Build ID:</span> <span id="build_id-spec-mb"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Author: </span><span id="author_date-spec-mb"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Custom Label (SKU): </span><span id="custom_label-spec-mb"></span></div>
                </div>  
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="genericListing" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="form-row">
                    <h4 class="modal-title col-12">Listing Specifications</h4><br>
                </div>
                <h6>Performance Specification</h6>
                <div class="form-row">
                    <div class="col-12"><strong>CPU:</strong> <span id="cpu-genspec-qty"></span><span id="cpu-genqty-symbol"> </span> <span id="cpu-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Memory:</strong> <span id="memory-genspec-qty"></span><span id="memory-genqty-symbol"> </span> <span id="memory-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Hard Drives:</strong> <span id="drives-genspec-qty"></span><span id="drives-genqty-symbol"> </span> <span id="drives-genspec"></span><span id="drives-gencomma2"></span> <span id="drives-genspec-qty2"></span><span id="drives-genqty-symbol2"> </span> <span id="drives-genspec2"></span><span id="drives-gencomma3"></span> <span id="drives-genspec-qty3"></span><span id="drives-genqty-symbol3"> </span> <span id="drives-genspec3"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Controller:</strong> <span id="slot-genspec-qty"></span><span id="slot-genqty-symbol"> </span> <span id="slot-genspec"></span><span id="slot-gencomma2"></span> <span id="slot-genspec-qty2"></span><span id="slot-genqty-symbol2"> </span> <span id="slot-genspec2"></span><span id="slot-gencomma3"></span> <span id="slot-genspec-qty3"></span><span id="slot-genqty-symbol3"> </span> <span id="slot-genspec3"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>OB NIC:</strong> <span id="ob_nic-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Motherboard Specs:</strong> <span id="mfr-genspec"></span><span id=""> </span> <span id="u-genspec"></span><span id="u-symbol-genspec"></span> <span id="drivebays-genspec"></span> <span id="rearbays-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Motherboard:</strong> <span id="motherboard-genspec"></span></div>
                </div>
                 <div class="form-row">
                    <div class="col-12"><strong>Backplane:</strong> <span id="backplane-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>PCI-Expansion Slots:</strong> <span id="pcie_slot-genspec"></span> <span id="slot_word-genspec"></span></div>
                </div>
                 <div class="form-row">
                    <div class="col-12"><strong>Drive Bays:</strong> <span id="drivebays-genspec-2"></span></div>
                </div>
                 <div class="form-row">
                    <div class="col-12"><strong>Rear Bays:</strong> <span id="rearbays-genspec-2"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>PSU Slot:</strong> <span id="psu-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Power:</strong> <span id="power-genspec-qty"></span><span id="power-genqty-symbol"> </span> <span id="power-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Rail Kit:</strong> <span id="railkit-genspec"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Warranty:</strong> <span id="warranty-genspec"></span></div>
                </div><br>
                <br><h6><strong>Message Us</strong></h6>
                <h7>All Servers Are Fully Customizable.<br>Call Us At 1-877-864-9123 x2<br> Or Email Us Directly At: websales@unixsurplus.com<br> Leave a review of your product to get 5% off your next purchase, call to redeem coupon.</h7><br>
                <br><h6><strong>Standard Warranty and Terms of Sale</strong></h6>
                <h7>- Standard 30 day limited warranty.<br> - Item tested to power on and all specs show in BIOS, We Warranty Hardware Only, not Software.<br> - Processing times estimated 3 Days. Tracking number will be emailed after Shipped.<br> - 30 Day Hardware parts Warranty. problems must be reported 30 days of when the item is delivered.<br> - Buyer Pays for all shipping, even on returns. (shipping is non-refundable)<br> - Restocking fee as described in the terms of the listing/ order. 20% Restocking fee if not stated.<br> - If you have issues please message us with an item number and the issue you are having.<br> - Our customer service representatives will assist you on getting your problem solved</h7>
                <br><br><div class="form-row">
                    <div class="col-12"><span>Build ID:</span> <span id="build_id-genspec"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Author: </span><span id="author_date-genspec"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Custom Label (SKU): </span><span id="custom_label-genspec"></span></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="motherboard_genericListing" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-row">
                    <h4 class="modal-title col-12">Listing Specifications</h4><br>
                </div>
                <h6>Performance Specification</h6>
                <div class="form-row">
                    <div class="col-12"><strong>CPU:</strong> <span id="cpu-genspec-qty-mb"></span><span id="cpu-genqty-symbol-mb"> </span> <span id="cpu-genspec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Memory:</strong> <span id="memory-genspec-qty-mb"></span><span id="memory-genqty-symbol-mb"> </span> <span id="memory-genspec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Controller:</strong> <span id="slot-genspec-qty-mb"></span><span id="slot-genqty-symbol-mb"> </span> <span id="slot-genspec-mb"></span><span id="slot-gencomma-mb2"></span> <span id="slot-genspec-qty-mb2"></span><span id="slot-genqty-symbol-mb2"> </span> <span id="slot-genspec-mb2"></span><span id="slot-gencomma-mb3"></span> <span id="slot-genspec-qty-mb3"></span><span id="slot-genqty-symbol-mb3"> </span> <span id="slot-genspec-mb3"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>OB NIC:</strong> <span id="ob_nic-genspec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>PCI-Expansion Slots:</strong> <span id="pcie_slot-genspec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Power:</strong> <span id="power-genspec-qty-mb"></span><span id="power-genqty-symbol-mb"> </span> <span id="power-genspec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Rail Kit:</strong> <span id="railkit-genspec-mb"></span></div>
                </div>
                <div class="form-row">
                    <div class="col-12"><strong>Warranty:</strong> <span id="warranty-genspec-mb"></span></div>
                </div><br>
                <br><h6><strong>Message Us</strong></h6>
                <h7>All Servers Are Fully Customizable.<br>Call Us At 1-877-864-9123 x2<br> Or Email Us Directly At: websales@unixsurplus.com<br> Leave a review of your product to get 5% off your next purchase, call to redeem coupon.</h7><br>
                <br><h6><strong>Standard Warranty and Terms of Sale</strong></h6>
                <h7>- Standard 30 day limited warranty.<br> - Item tested to power on and all specs show in BIOS, We Warranty Hardware Only, not Software.<br> - Processing times estimated 3 Days. Tracking number will be emailed after Shipped.<br> - 30 Day Hardware parts Warranty. problems must be reported 30 days of when the item is delivered.<br> - Buyer Pays for all shipping, even on returns. (shipping is non-refundable)<br> - Restocking fee as described in the terms of the listing/ order. 20% Restocking fee if not stated.<br> - If you have issues please message us with an item number and the issue you are having.<br> - Our customer service representatives will assist you on getting your problem solved</h7>
                <br><br><div class="form-row">
                    <div class="col-12"><span>Build ID:</span> <span id="build_id-genspec-mb"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Author: </span><span id="author_date-genspec-mb"></span></div>
                </div><br>
                <div class="form-row">
                    <div class="col-12"><span>Custom Label (SKU): </span><span id="custom_label-genspec-mb"></span></div>
                </div>  
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-bottom: 60px;">
	<div class="card">
		<div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
        	<form id="searchForm">
        		<div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchColumn">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchSelected" class="custom-select col-md-2">
                                    <option value="build_id">Build ID</option>
                                    <option value="sku">SKU</option>
                                    <option value="sales_rep">Sales Rep</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemSearch"
                                       placeholder="Search">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="orderBy">Order By</label>
                        <select type="button" class="form-control" id="orderBy">   
                           <option value="build_id">Build ID</option>
                            <option value="sku">SKU</option>
                            <option value="sales_rep">Sales Rep</option>
                            <option value="created_date">Created Date</option>                                                       
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sortOrder">Sort Order</label>
                        <select type="button" class="form-control" id="sortOrder">
                            <option value="ASC" selected>Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
        	</form>
        </div>
	</div>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Summary</h2>
        </div>
        <div class="card-body">
            <span><strong>Total:</strong> <span class="task-count"></span></span>
        </div>
    </div>
	<div class = "card">
        <div class="card-header">
        <h3 class="card-title">Items</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <div class="btn-group" id="dropdownContainer">
                    <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>Show/Hide Columns</button>

                <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="build_id" <?= isset($data['columns']->build_id->is_shown)
                                                      ? ($data['columns']->build_id->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Build ID
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="chassis" <?= isset($data['columns']->chassis->is_shown)
                                                      ? ($data['columns']->chassis->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            SKU
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sales_rep" <?= isset($data['columns']->sales_rep->is_shown)
                                                      ? ($data['columns']->sales_rep->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Sales Rep
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1"
                               name="created_date" <?= isset($data['columns']->created_date->is_shown)
                                                   ? ($data['columns']->created_date->is_shown ? 'checked' : '')
                                                     : 'checked' ?>>
                        Created Date
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body p-0 inventory-container table-responsive">
			<table class="table table-bordered table-striped table-hover paginated" id="product-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="bg-white border">
					<tr>
						<th scope="col" nowrap>&#35;</th>
                        <th scope="col" class="header-build_id<?= isset($data['columns']->build_id->is_shown) ? ($data['columns']->build_id->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Build ID</th>
						<th scope="col" class="header-chassis<?= isset($data['columns']->chassis->is_shown) ? ($data['columns']->chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
						<th scope="col" class="header-sales_rep<?= isset($data['columns']->sales_rep->is_shown) ? ($data['columns']->sales_rep->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Sales Rep</th>
                        <th scope="col" class="header-modify<?= isset($data['columns']->modify->is_shown) ? ($data['columns']->modify->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Modify</th>
						<th scope="col" class="header-created_date<?= isset($data['columns']->created_date->is_shown) ? ($data['columns']->created_date->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Created date</th>
					</tr>
				</thead>
				<tbody id="product_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
			</table>
		</div>
	</div>
</div>
</div> <!-- for footer -->
<?php $this->load->view('templates/common/spinner'); ?>

<script id="assemblyScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/assembly_module/assembly.js"></script>
<script id="assemblyeBayListingScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/assembly_module/eBayListingScript.js"></script>
<script id="assemblygenListingScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/assembly_module/genListingScript.js"></script>
<script id="assemblyMBeBayListingScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/assembly_module/motherboard_eBayListingScript.js"></script>
<script id="assemblyMBgenListingScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/assembly_module/motherboard_genListingScript.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
