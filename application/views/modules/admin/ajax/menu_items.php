<!-- Default box -->
<div class="container-fluid">
        <div class="card">
        <div class="card-header">
          <h3 class="card-title">UnixSurplus Items</h3>
		</div>
		
        <div class="card-body">
			<table id="product_table" class="table table-striped table-bordered" style="width:100%">
				<thead>
                <tr>
                        <!-- Edit column -->
                        <!--<th>Order</th>-->
                        <th style="width: 5%;"></th>
                        
                        <th>ID</th>
                        <th>Title</th>
                        <th>File</th>
                        <th>Parent</th>
                    </tr>
				</thead>
        <tbody>
		<?php  //echo $rate;echo "<pre>"; var_dump($rate);echo "</pre>";
$num = 0;
foreach($menu_information as $item){
  $num = $num + 1;
   echo "<tr>";
   //echo "<td>" . $item->menu_order ."</td>";
   echo "<td>" . "<button class=\"btn btn-default\" type=\"button\" " .
   "data-toggle=\"modal\" data-target=\"#editorModal$item->menu_id\">" .
   "<i class=\"fa fa-edit\" aria-hidden=\"true\"></i></button>" ."</td>";
    
    echo "<td>" . $item->menu_id ."</td>";
    echo "<td>" . $item->menu_title ."</td>";
    echo "<td>" . $item->menu_file ."</td>";
    echo "<td>" . $item->parent ."</td>";
    
   echo "</tr>";


   ?>
   <div class="modal fade" id="editorModal<?php echo $item->menu_id; ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Menu Items</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              


              <div class="form-group">
                        
                        <input type="text" class="form-control" id="id<?php echo $item->menu_id;    ?>"
                               placeholder="Menu Title" value="<?php echo $item->menu_id;    ?>" style="border-color: white;
    color: white;
    margin-bottom: -70px;">
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title<?php echo $item->menu_id;    ?>"
                               placeholder="Menu Title" value="<?php echo $item->menu_title;    ?>">
                    </div>
                    <div class="form-group">
                        <label>Parent</label>
                        <select class="form-control" id="parent<?php echo $num;    ?>">
                        <?php
                        
                        foreach($menu_information as $mi){
                          if($mi->menu_title == "$item->parent"){
                            ?> <option value="<?php echo $mi->menu_id;   ?>"><?php echo $item->parent;   ?></option>   <?php
                          }
                        }
                        
                        ?>
                        
                        <?php
                        foreach($menu_information as $mi){
                            ?>

                            
                          <option value="<?php echo $mi->menu_id;   ?>"><?php echo $mi->menu_title;   ?></option>
                          

                            <?php
                        }


                            ?>
                          
                        </select>
                      </div>
                      
                    <div class="form-group">
                        <label for="order">Order</label>
                        <input type="text" class="form-control" id="order<?php echo $item->menu_id;    ?>"
                               placeholder="Menu Order" value="<?php echo $item->menu_order;    ?>">
                    </div>


                    <div class="form-group">
                        <label for="order">Icons</label>
                        <input type="text" class="form-control" id="icons<?php echo $item->menu_id;    ?>"
                               placeholder="Menu Order" value="<?php echo $item->icons;    ?>">
                    </div>
                
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="saveChanges<?php echo $item->menu_id;    ?>">Save changes</button>
            </div>
           
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>


      <script>
					$(document).ready(function(){
							$("#saveChanges<?php echo $item->menu_id;    ?>").click(function(){
								
									var params = {
											id : <?php echo $item->menu_id;    ?>, 
											title : 	$("#title<?php echo $item->menu_id;    ?>").val(), 
											parent:	$("#parent<?php echo $num;    ?>").val(),
											order :	 	$("#order<?php echo $item->menu_id;    ?>").val(),
											icons : 	$("#icons<?php echo $item->menu_id;    ?>").val()
									};
									
									$.ajax({
											url : "<?php echo base_url();   ?>index.php/menu_items/request/update_menu_infos",
											type : "post",
											data : params,
											success : function(msg){
													//alert($("#parent<?php echo $num;    ?>").val())
													//$("#status").html(msg);
													//alert(<?php //echo $item->menu_id;    ?>);
													
													
													window.location.href = '<?php echo base_url();   ?>index.php/menu_items';
													
													
											}
									});
							});
					});
			</script>
   <?php
    
}


?>
        	</tbody>
   		 </table>
	</div>
</div>






	<script>
	$(document).ready(function() {
    $('#product_table').DataTable();
} );
	</script>


