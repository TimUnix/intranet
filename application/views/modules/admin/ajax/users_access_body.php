<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col" data-value="<?= $item->firstname ?><?= $item->lastname ?>" class="item-users" style="padding-left: 5rem; font-size: 1.5rem;">
			<input id="users_checkbox" class="form-check-input users-access" type="checkbox" style="height: 15px; width: 15px; vertical-align: middle; position: relative; bottom: 4px;"
					value="<?= $item->id ?>">
			<label class="form-check-label" for="checkbox" style="display: inline-block; font-size: 1.5rem; margin-left: 0.5rem;"><?= $item->firstname ?> <?= $item->lastname ?></label>
		</td>      
    </tr>
<?php endforeach; } ?>

