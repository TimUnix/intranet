<?php if (!empty($menus)) { foreach($menus as $menu): ?>
	<tr>
        <td scope="col" data-value="<?= $menu->menu_title ?>" class="item-menu" style="padding-left: 5rem; font-size: 1.5rem;">
 			<input id="menu_checkbox" class="form-check-input menu-access" type="checkbox" style="height: 15px; width: 15px; vertical-align: middle; position: relative; bottom: 4px;"
                value="<?= $menu->menu_id ?>">
			<?= $menu->menu_title ?>
        </td> 
    </tr>
<?php endforeach; } ?>
