<?php $this->load->view('templates/header_new'); ?>

<div id="load_products">
<div class="container-fluid">
        <div class="card">
        <div class="card-header">
          <h3 class="card-title">UnixSurplus Items</h3>
		</div>
		
        <div class="card-body">
			<table id="product_table" class="table table-striped table-bordered" style="width:100%">
			<div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="product_table_length"><label>Show <select name="product_table_length" aria-controls="product_table" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="product_table_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="product_table"></label></div></div></div>
				<thead>
                <tr>
                        <!-- Edit column -->
                        <th style="width: 5%;"></th>
                        <th>Order</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>File</th>
                        <th>Parent</th>
                    </tr>
				</thead>
        <tbody id="list">
		
        	</tbody>
   		 </table>
	</div>
</div></div>
<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script>
	$(document).ready(function(){
		$('#list').html(`<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`);
		$("#load_products").load("<?php echo base_url();    ?>index.php/menu_items/request/load_menu_infos");
		
	});
</script>
