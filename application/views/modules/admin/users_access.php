<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<style>
	body{
		overflow: hidden;
	}
</style>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<div class="card-title">Access Control</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-6 col-lg-6">
				<div class="card-body table-responsive p-0 table-container" style="height: 80vh;">
					<table id="users_table" class="table table-striped table-bordered table-hover main-table"
					cellspacing="0" style="overflow: auto; border-collapse: separate;">
						<thead class="bg-white sticky-top border">
							<tr class="main-header-row">
								<th scope="col"><h3 style="font-weight: 700;">User</h3>
									<form id="searchForm" onsubmit="return false" style="margin-top: 1rem;">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<div class="row">
														<div class="col input-group input-group-lg">
															<div class="input-group-prepend">
																<button class="btn btn-lg btn-primary" type="button" id="users_searchColumn">
																	<i class="fa fa-search"></i>
																</button>
															</div>
															<input class="form-control input-lg" type="search" id="users_itemSearch" placeholder="Search" required>
														</div>  
													</div>
												</div>
											</div>
										</div>
									</form>
								</th>
							</tr>
						</thead>
						<tbody id="users_access_table">	
							<tr>

							</tr>
						</tbody>		
					</table>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-6">
				<div class="card-body table-responsive p-0 table-container" style="height: 80vh;">
					<table id="modules_table" class="table table-striped table-bordered table-hover main-table"
					cellspacing="0" style="overflow: auto; border-collapse: separate;">
						<thead class="bg-white sticky-top border">
							<tr class="main-header-row">
								<th scope="col">
									<div class="col" style="display: inline-block; width: 45%;"><h3 style="font-weight: 700;">Modules</h3></div>
										<span id="user-module_id" style="display: none;"></span>
			                            <div class="col" style="display: inline-block; width: 45%; text-align: right;">
										<input id="superadmin" type="checkbox" name="super-admin" style="height: 20px; width: 20px; vertical-align: middle; position: relative; bottom: 4px;">
										<label for="checkbox" class="form-check-label" style="font-weight: 700; display: inline-block; font-size: 1.5rem; margin-left: 0.5rem;">Super Admin</label>
									</div>
									<form id="searchForm" onsubmit="return false" style="margin-top: 0.5rem;">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<div class="row">
														<div class="col input-group input-group-lg">
															<div class="input-group-prepend">
																<button class="btn btn-lg btn-primary" type="button" id="modules_searchColumn">
																	<i class="fa fa-search"></i>
																</button>
															</div>
															<input class="form-control input-lg" type="search" id="modules_itemSearch" placeholder="Search" required>
														</div>  
													</div>
												</div>
											</div>
										</div>
									</form>
								</th>
							</tr>
						</thead>
						<tbody id="modules_access_table">
							<tr> 
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script id="usersAccessScript" data-base-url="<?= base_url() ?>" type="text/javascript"
			src="<?= base_url() ?>resources/js/usersAccess.js">
</script>
