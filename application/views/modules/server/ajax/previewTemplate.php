<div class="modal fade" id="previewTemplate" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <img src="<?= base_url() ?>resources/img/PlusDrives_Banner1.png" alt="banner" 
                    style="height:150px; width:1250px;">  
            </div>
            <div class="modal-body">
                <div style="text-align:center; font-family: 'Montserrat', sans-serif;"><strong><span class="title_preview" style="font-size: 150%">
                    </span></strong></div> <br>
                <div class="row">
                    
                    <div class="col-md-6">
                        <input type="image" id="image_preview" style="height:470px; width:500px;">
                    </div>
                    <div class="col-md-6">
                        <strong><h5 style="color:#FFF; font-family: 'Montserrat', sans-serif; background-color: #191970; padding: 1rem;">TECHNICAL SPECIFICATIONS</h5></strong>
                        <table class="table" style="font-family: 'Montserrat', sans-serif;">
                            <tbody>
                                <tr>
                                    <td><strong>Brand:</strong></td>
                                    <td class="brand_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Model:</strong></td>
                                    <td class="model_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Form Factor:</strong></td>
                                    <td class="ff_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Network Interface:</strong></td>
                                    <td class="interface_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Condition:</strong></td>
                                    <td class="condition_preview"></td>
                                </tr>
                                <tr>
                                    <td><strong>Warranty:</strong></td>
                                    <td class="warranty_preview">
									</td>
                                </tr>
                                <tr>
                                    <td><strong>Product Description:</strong></td>
                                    <td class="description_preview"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist" style="font-family: 'Montserrat', sans-serif; border-bottom: none;">
                    <li class="nav-item">
                 	    <a class="nav-link active" id="about_us" data-toggle="pill" href="#about-us" role="tab" aria-controls="about-us" aria-selected="true" style="border-top:none;">About Us</a>
                    </li>
                    <li class="nav-item">
                 	    <a class="nav-link active" id="warranty" data-toggle="pill" href="#warranty-" role="tab" aria-controls="warranty-" aria-selected="true" style="border-top:none;">Warranty</a>
                    </li>
                    <li class="nav-item">
                 	    <a class="nav-link active" id="shipping" data-toggle="pill" href="#shipping-" role="tab" aria-controls="shipping-" aria-selected="true" style="border-top:none;">Shipping</a>
                    </li>
                </ul>
                <div class="tab-content" style="font-family: 'Montserrat', sans-serif;">
                    <div class="tab-pane fade show active" id="about-us" role="tabpanel" aria-labelledby="about_us">
                        <br><h2 style="border-top: 1px solid black">About Us</h2>
                    
                        <p>Our Model is simple. With the support of the world's leading OEMs, CMs and Resellers, Server Part Deals is able to offer the Enterprise value that is simply unseen in the marketplace.
							We have perfected our processes to better serve our clients by conducting hundreds of surveys to find out what really helps our clients and how we can make that possible for every
							transaction. We offer the best in class service when it comes to the basics like:		
						</p>
							<ul>
								<li>Same Day Shipping</li>
								<li>Professional Packaging</li>
								<li>Warranty Claims without hassle</li>
								<li>Free Shipping</li>
								<li>Large Quantity Discounts and Flexiibility</li>
								<li>Testing and Longer Warranties</li>
								<li>OEM/Compatible Solutions at a fraction of Branded pricing</li>
							</ul>           
                        <p>Our process and model is proven successful with over 300,000 orders processed and 1000s of return customers. We invite you to experience the difference with Server Part Deals. Let's make a deal.</p>
                        
                    </div>
                    <div class="tab-pane fade show active" id="warranty-" role="tabpanel" aria-labelledby="warranty">
						<br>
							<h2 style="border-top: 1px solid black">Warranty</h2>
							<p>100% Guaranteed Working Condition w/ Warranty. Ships from the USA!
							<br>All Hard Drives have a 30 day warranty unless otherwise stated.</p>
						<br>
							<h2>Return</h2>
							<p>After receiving the item, contact seller within 30 Days
							<br>Refund will be given as Money Back, Replacement
							<br>Seller pays for return shipping </p>

                    </div>
                    <div class="tab-pane fade show active" id="shipping-" role="tabpanel" aria-labelledby="shipping">
                        <br>
							<p style="border-top: 1px solid black; padding-top: 1rem;">Free 2 day shipping <br>
							We understand the importances of fast reliable delivery, and we prioritize getting you tracking information within a couple hours of purchase.</p>
						<br>
							<p>Will usually ship within 1 business day of receiving cleared payment.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
