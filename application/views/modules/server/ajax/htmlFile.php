<div style="display:none;">
<div class="" id="whole_html">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
<link href="https://fonts.googleapis.com/css2?family=Montserrat&amp;display=swap" rel="stylesheet">
<style>img, input{max-width:100%}
  main {
min-width: 320px;
max-width: 400px;
padding: 50px;
margin: 0 auto;
background: #fff;
}
section {
display: none;
padding: 20px 0 0;
border-top: 1px solid #ddd;
}
label {
display: inline-block;
margin: 0 0 -1px;
padding: 15px 25px;
font-weight: 600;
text-align: center;
color: #bbb;
border: 1px solid transparent;
}
label:hover {
color: #888;
cursor: pointer;
}
input:checked + label {
color: #555;
border: 1px solid #ddd;
border-top: 2px solid orange;
border-bottom: 1px solid #fff;
}
#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4 {
display: block;
}
@media screen and (max-width: 650px) {
label {
font-size: 0;
}
label:before {
margin: 0;
font-size: 18px;
}
}
@media screen and (max-width: 400px) {
label {
padding: 15px;
}
}
p, h5, li, h2, h4, div{
line-height: 200%;
font-family: 'Montserrat', sans-serif;
}
td{
font-family: 'Montserrat';
}  
</style>
<img src="https://intranet.unixsurplus.com/resources/img/PlusDrives_Banner1.png" alt="Banner">&nbsp;&nbsp;<div style="text-align:center;">
<strong><span class="title_preview" style="font-size: 200%"></span></strong>
</div> <br>
<div style="width: 100%; margin-right: 2rem;">
<div style="width: 48%; display: inline-block;">
<input type="image" id="image_preview_html" style="height:470px; width:470px;"></div><div style="width: 48%; display: inline-block; margin-left: 2rem;"><h5 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-family: inherit; font-weight: 500; line-height: 1.2; color: #FFF; background-color: #191970; font-size: 1.25rem; padding: 1rem;">TECHNICAL SPECIFICATIONS</h5>
<div style="width: 95%; display: inline-block;">
<table class="table" style="border-collapse: collapse; width: 100%; margin-bottom: 1rem; color: rgb(33, 37, 41); background-color: rgb(255, 255, 255); font-size: 16px;">
<tbody style="box-sizing: border-box;">
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Brand:</span>
</td>
<td class="brand_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Model:</span>
</td>
<td class="model_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Form Factor:</span>
</td>
<td class="ff_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Network Interface:</span>
</td>
<td class="interface_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Condition:</span>
</td>
<td class="condition_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Warranty:</span>
</td>
<td class="warranty_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
<tr style="box-sizing: border-box;">
<td style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);">
<span style="box-sizing: border-box; font-weight: bolder;">Product Description:</span>
</td>
<td class="description_preview" style="box-sizing: border-box; padding: 0.75rem; vertical-align: top; border-top: 1px solid rgb(222, 226, 230);"></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div style="width: 100%; margin-right: 2rem;"><div style="width: 48%; display: inline-block; margin-left: 2rem;"><div style="width: 95%; display: inline-block;"><br></div></div></div><div style="width: 100%; margin-right: 2rem;"><div style="width: 48%; display: inline-block; margin-left: 2rem;"><div style="width: 95%; display: inline-block;"><br></div></div></div>
<div><input id="tab1" type="radio" name="tabs" checked="" style="display:none;"><label for="tab1">About Us</label><input id="tab2" type="radio" name="tabs" style="display:none;"><label for="tab2">Warranty</label><input id="tab3" type="radio" name="tabs" style="display:none;"><label for="tab3">Shipping</label><section id="content1">
<h2>About Us</h2><p>Our Model is simple. With the support of the world's leading OEMs, CMs and Resellers, Server Part Deals is able to offer the Enterprise value that is simply unseen in the marketplace. We have perfected our processes to better serve our clients by conducting hundreds of surveys to find out what really helps our clients and how we can make that possible for every transaction. We offer the best in class service when it comes to the basics like:</p>
<ul><li>Same Day Shipping</li><li>Professional Packaging</li><li>Warranty Claims without hassle</li><li>Free Shipping</li><li>Large Quantity Discounts and Flexiibility</li><li>Testing and Longer Warranties</li><li>OEM/Compatible Solutions at a fraction of Branded pricing</li></ul>
<p>Our process and model is proven successful with over 300,000 orders processed and 1000s of return customers. We invite you to experience the difference with Server Part Deals. Let's make a deal.</p>
</section>
<section id="content2">
<h2>Warranty</h2>
<p>100% Guaranteed Working Condition w/ Warranty. Ships from the USA!</p><p>All Hard Drives have a 30 day warranty unless otherwise stated.</p>
<h2>Return</h2>
<p>After receiving the item, contact seller within 30 Days</p><p>Refund will be given as Money Back, Replacement</p><p>Seller pays for return shipping </p>
</section>
<section id="content3">
<h2>Shipping</h2>
<p>Free 2 day shipping</p><p>We understand the importances of fast reliable delivery, and we prioritize getting you tracking information within a couple hours of purchase.</p>
<p>Will usually ship within 1 business day of receiving cleared payment.</p>
</section>
</div>
</div></div>
