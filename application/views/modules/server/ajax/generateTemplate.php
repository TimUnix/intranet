<div class="modal fade" id="generateTemplate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div id="validate_template"></div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" id="title_template">
                    </div>
					<div class="col-md-6">
                        <label for="Model">Units:</label>
                        <input list="units_templates" type="text" class="form-control" id="units_template" required>
						<datalist id="units_templates">
							<option value="1U">1U</option>
						</datalist>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-6">
                        <label for="Model">Brand:</label>
                        <input list="brand_templates" type="text" class="form-control" id="brand_template" required>
						<datalist id="brand_templates">
							<option value="DELL">DELL</option>
						</datalist>
                    </div>
                    <div class="col-md-6">
                        <label for="Model">Model:</label>
                        <input list="modeltemplates" type="text" class="form-control" id="model_template" required>
						<datalist id="modeltemplates">
							<option value="R6515">R6515</option>
						</datalist>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-6">
                        <label for="Model">Form Factor:</label>
                        <input type="text" class="form-control" id="ff_template" required>
                    </div>
                    <div class="col-md-6">
                        <label for="Model">Network Interface:</label>
                        <input type="text" class="form-control" id="interface_template" required>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-6">
                        <label for="Model">Condition:</label>
                        <input type="text" class="form-control" id="condition_template" required>
                    </div>
                    <div class="col-md-6">
                        <label for="Model">Warranty:</label>
                        <input type="text" class="form-control" id="warranty_template" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="description">Product Description:</label>
                        <textarea class="form-control" row="2" id="description_template"></textarea>
                    </div>
                </div><br>
                <div class="row html-box" style="display:none;">
                    <div class="col-md-1">
                        <button class="btn btn-primary" type="button" id="button_copy" style="height:61px">Copy</button>
                    </div>
                    <div class="col-md-11">
                        <textarea class="form-control" row="2" id="copy_html_template"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="generate_template_submit">Generate HTML</button>
                <button type="button" class="btn btn-primary" id="preview_template_submit">Preview Template</button>
            </div>  
        </div>
    </div>
</div>
