<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="success"></div>
				<div class="row">
					<div class="col-md-12">
                        <label for="sku">SKU</label>
                        <input type="text" class="form-control" id="sku">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="con">Condition</label>
                        <input type="text" class="form-control" id="con">
                    </div>
					<div class="col-md-6">
                        <label for="units">Units</label>
						<select id="units" class="form-control">
                            <option value="null"> </option>
                            <option value="1U">1U</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="brand">Brand</label>
                        <select id="brand" class="form-control">
                            <option value="null"> </option>
                            <option value="DELL">DELL</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="model">Model</label>
                        <select id="model" class="form-control">
                            <option value="null"> </option>
                            <option value="R6515">R6515</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="nodes">Nodes</label>
                        <input type="text" class="form-control" id="nodes">
                    </div>
                    <div class="col-md-6">
                        <label for="bays">Bays</label>
                        <input type="text" class="form-control" id="bays">
                    </div>
                </div>
                <div class="row">   
					<div class="col-md-6">
                        <label for="drive_ff">Drives Form Factor</label>
                        <input type="text" class="form-control" id="drive_ff">
                    </div>
                    <div class="col-md-6">
                        <label for="backplane">Backplane</label>
                        <input type="text" class="form-control" id="backplane">
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-6">
                        <label for="socket_supported">Socket Supported</label>
                        <input type="text" class="form-control" id="socket_supported">
                    </div>   
                    <div class="col-md-6">
                        <label for="qty_of_power_supply">Qty of Power Supply</label>
                        <input type="text" class="form-control" id="qty_of_power_supply">
                    </div> 
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Submit</button>
            </div>
        </div>
    </div>
</div>
