<?php if(!empty($items)) { foreach($items as $row_index => $item): ?>
    <?php $index = $offset + $row_index; ?>
	<tr>
		<td scope="col"><?= $index + 1 ?></td>
		<td scope="col" class="sku item-sku<?= isset($columns->sku->is_shown) 
                        ? ($columns->sku->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->sku ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel skubtn">
                <i class="fa fa-edit skupenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->sku ?></td>

		<td scope="col" class=" con item-con<?= isset($columns->con->is_shown) 
                        ? ($columns->con->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->con ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel conbtn">
                <i class="fa fa-edit conpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->con ?></td>

		<td scope="col" class="units item-units<?= isset($columns->units->is_shown) 
                        ? ($columns->units->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->units ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel unitsbtn">
                <i class="fa fa-edit unitspenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->units ?></td>

        <td scope="col" class="brand item-brand<?= isset($columns->brand->is_shown) 
                        ? ($columns->brand->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->brand ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel brandbtn">
                <i class="fa fa-edit brandpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->brand ?></td>

		<td scope="col" class="model item-model<?= isset($columns->model->is_shown) 
                        ? ($columns->model->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->model ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel modelbtn">
                <i class="fa fa-edit modelpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->model ?></td>

		<td scope="col" class="nodes item-nodes<?= isset($columns->nodes->is_shown) 
                        ? ($columns->nodes->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->nodes ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel nodesbtn">
                <i class="fa fa-edit nodespenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->nodes ?></td>

		<td scope="col" class="bays item-bays<?= isset($columns->bays->is_shown) 
                        ? ($columns->bays->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->bays ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel baysbtn">
                <i class="fa fa-edit bayspenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->bays ?></td>

		<td scope="col" class="drive_ff item-drive_ff<?= isset($columns->drive_ff->is_shown) 
                        ? ($columns->drive_ff->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->drive_ff ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel drive_ffbtn">
                <i class="fa fa-edit drive_ffpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->drive_ff ?></td>

		<td scope="col" class="backplane item-backplane<?= isset($columns->backplane->is_shown) 
                        ? ($columns->backplane->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->backplane ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel backplanebtn">
                <i class="fa fa-edit backplanepenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->backplane ?></td>

		<td scope="col" class="socket_supported item-socket_supported<?= isset($columns->socket_supported->is_shown) 
                        ? ($columns->socket_supported->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->socket_supported ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel socket_supportedbtn">
                <i class="fa fa-edit socket_supportedpenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->socket_supported ?></td>

		<td scope="col" class="qty_of_power_supply item-qty_of_power_supply<?= isset($columns->qty_of_power_supply->is_shown) 
                        ? ($columns->qty_of_power_supply->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->qty_of_power_supply ?>">
        <button type="button" class="btn btn-link text-primary draftid-cancel qty_of_power_supplybtn">
                <i class="fa fa-edit qty_of_power_supplypenbtn" aria-hidden="true"></i>
            </button>
        <?= $item->qty_of_power_supply ?></td>


	</tr>

<?php endforeach; }?>
