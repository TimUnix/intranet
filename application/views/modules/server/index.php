<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/server/ajax/createServer'); ?>
<?php $this->load->view('modules/server/ajax/generateTemplate'); ?>
<?php $this->load->view('modules/server/ajax/previewTemplate'); ?>
<?php $this->load->view('modules/server/ajax/htmlFile'); ?>
<!-- <div id="load"></div> -->


<div class="container-fluid" style="padding-bottom: 60px;">
	<div class="card">
		<div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
        	<form id="searchForm">
        		<div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchColumn">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchSelected" class="custom-select col-md-2">
                                    <option value="sku">SKU</option>
                                    <option value="con">Condition</option>
									<option value="units">Units</option>
                                    <option value="brand">Brand</option>
                                    <option value="model">Model</option>
                                    <option value="nodes">Nodes</option>
                                    <option value="bays">Bays</option>
                                    <option value="drive_ff">Drives Form Factor</option>
                                    <option value="backplane">Backplane</option>
                                    <option value="socket_supported">Socket Supported</option>
                                    <option value="qty_of_power_supply">Qty of Power Supply</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemSearch"
                                       placeholder="Search">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="orderBy">Order By</label>
                        <select type="button" class="form-control" id="orderBy">   
                            <option value="con">Condition</option>
							<option value="units">Units</option>
                            <option value="brand">Brand</option>
                            <option value="model">Model</option>
                            <option value="nodes">Nodes</option>
                            <option value="bays">Bays</option>
                            <option value="drive_ff">Drives Form Factor</option>
                            <option value="backplane">Backplane</option>
							<option value="sku" selected>SKU</option>
                            <option value="socket_supported">Socket Supported</option>
                            <option value="qty_of_power_supply">Qty of Power Supply</option>                                                     
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sortOrder">Sort Order</label>
                        <select type="button" class="form-control" id="sortOrder">
                            <option value="ASC" selected>Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
        	</form>
        </div>
	</div>
   
	<div class ="card">
        <div class="card-header">
        <button id="insertServer" type="button" class="btn btn-info btn-lg ml-2" style = "padding: 0.25rem 0.5rem; font-size:.875rem;" data-toggle="modal"
                data-target="#myModal">
            Create Server
        </button>
        <button id="generateTemplateButton" type="button" class="btn btn-info btn-lg ml-2" style = "padding: 0.25rem 0.5rem; font-size:.875rem;" data-toggle="modal"
            data-target="#generateTemplate">
            Generate Template 
        </button>
            <div class="card-tools">
                <button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <div class="btn-group" id="dropdownContainer">
                    <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>Show/Hide Columns</button>

                <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
					<div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sku" <?= isset($data['columns']->sku->is_shown)
                                                      ? ($data['columns']->sku->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            SKU
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="con" <?= isset($data['columns']->con->is_shown)
                                                      ? ($data['columns']->con->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Condition
                    </div>
					<div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="units" <?= isset($data['columns']->units->is_shown)
                                                      ? ($data['columns']->units->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Units
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="brand" <?= isset($data['columns']->brand->is_shown)
                                                      ? ($data['columns']->brand->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Brand
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="model" <?= isset($data['columns']->model->is_shown)
                                                      ? ($data['columns']->model->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Model
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="nodes" <?= isset($data['columns']->nodes->is_shown)
                                                      ? ($data['columns']->nodes->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Nodes
                    </div>
                   <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="bays" <?= isset($data['columns']->bays->is_shown)
                                                      ? ($data['columns']->bays->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Bays
                    </div>
                     <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="drive_ff" <?= isset($data['columns']->drive_ff->is_shown)
                                                      ? ($data['columns']->drive_ff->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Drives Form Factor
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="backplane" <?= isset($data['columns']->backplane->is_shown)
                                                      ? ($data['columns']->backplane->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Backplane
                    </div>
					<div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="socket_supported" <?= isset($data['columns']->socket_supported->is_shown)
                                                      ? ($data['columns']->socket_supported->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Socket Supported
                    </div>
					<div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="qty_of_power_supply" <?= isset($data['columns']->qty_of_power_supply->is_shown)
                                                      ? ($data['columns']->qty_of_power_supply->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Qty of Power Supply
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="horizontal-scroller">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body sticky-headers p-0 d-none" style="z-index: 2;">
			<table class="table table-bordered table-striped table-hover sticky-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="bsticky-thead bg-white">
					<tr class="sticky-header-row">
						<th scope="col" nowrap>&#35;</th>
						<th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown) ? ($data['columns']->sku->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
						<th scope="col" class="header-con<?= isset($data['columns']->con->is_shown) ? ($data['columns']->con->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Condition</th>
						<th scope="col" class="header-units<?= isset($data['columns']->units->is_shown) ? ($data['columns']->units->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Units</th>
                        <th scope="col" class="header-brand<?= isset($data['columns']->brand->is_shown) ? ($data['columns']->brand->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Brand</th>
						<th scope="col" class="header-model<?= isset($data['columns']->model->is_shown) ? ($data['columns']->model->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Model</th>
						<th scope="col" class="header-nodes<?= isset($data['columns']->nodes->is_shown) ? ($data['columns']->nodes->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Nodes</th>
						<th scope="col" class="header-bays<?= isset($data['columns']->bays->is_shown) ? ($data['columns']->bays->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Bays</th>
						<th scope="col" class="header-drive_ff<?= isset($data['columns']->drive_ff->is_shown) ? ($data['columns']->drive_ff->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Drives Form Factor</th>
						<th scope="col" class="header-backplane<?= isset($data['columns']->backplane->is_shown) ? ($data['columns']->backplane->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Backplane</th>
						<th scope="col" class="header-socket_supported<?= isset($data['columns']->socket_supported->is_shown) ? ($data['columns']->socket_supported->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Socket Supported</th>
						<th scope="col" class="header-qty_of_power_supply<?= isset($data['columns']->qty_of_power_supply->is_shown) ? ($data['columns']->qty_of_power_supply->is_shown ? '' : ' d-none') 
													   : '' ?>"nowrap>Qty of Power Supply</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="card-body p-0 table-container table-responsive">
			<table class="table table-bordered table-striped table-hover main-table" id="product-table"
                   style="border-collapse: separate;" cellspacing="0">
				<thead class="main-thead bg-white border">
					<tr class="main-header-row">
						<th scope="col" nowrap>&#35;</th>
						<th scope="col" class="header-sku<?= isset($data['columns']->sku->is_shown) ? ($data['columns']->sku->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
						<th scope="col" class="header-con<?= isset($data['columns']->con->is_shown) ? ($data['columns']->con->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Condition</th>
						<th scope="col" class="header-units<?= isset($data['columns']->units->is_shown) ? ($data['columns']->units->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Units</th>
                        <th scope="col" class="header-brand<?= isset($data['columns']->brand->is_shown) ? ($data['columns']->brand->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Brand</th>
						<th scope="col" class="header-model<?= isset($data['columns']->model->is_shown) ? ($data['columns']->model->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Model</th>
						<th scope="col" class="header-nodes<?= isset($data['columns']->nodes->is_shown) ? ($data['columns']->nodes->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Nodes</th>
						<th scope="col" class="header-bays<?= isset($data['columns']->bays->is_shown) ? ($data['columns']->bays->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Bays</th>
						<th scope="col" class="header-drive_ff<?= isset($data['columns']->drive_ff->is_shown) ? ($data['columns']->drive_ff->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Drives Form Factor</th>
						<th scope="col" class="header-backplane<?= isset($data['columns']->backplane->is_shown) ? ($data['columns']->backplane->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Backplane</th>
						<th scope="col" class="header-socket_supported<?= isset($data['columns']->socket_supported->is_shown) ? ($data['columns']->socket_supported->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Socket Supported</th>
						<th scope="col" class="header-qty_of_power_supply<?= isset($data['columns']->qty_of_power_supply->is_shown) ? ($data['columns']->qty_of_power_supply->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Qty of Power Supply</th>
					</tr>
				</thead>
				<tbody id="product_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
			</table>
		</div>
	</div>
</div>
</div> <!-- for footer -->
<?php $this->load->view('templates/common/spinner'); ?>

<script id="serverScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/server/server.js"></script>
<script id="serverFieldEditScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/server/serverFieldEdit.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
