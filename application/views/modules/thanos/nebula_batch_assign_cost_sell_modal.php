<div class="modal fade" id="batchAssignCostSellModal" tabindex="-1" aria-labelledby="batchAssignCostSellModalLabel"
	 aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="batchAssignCostSellModalLabel">Assign Cost/Sell</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="form-group">
					<label for="batchAssignCostSellOptions">Cost/Sell</label>
					<select id="batchAssignCostSellOptions" class="custom-select" placeholder="Select a vendor">
						<option value="cost">Cost</option>
						<option value="sell">Sell</option>
					</select>
				</div>
				<div class="form-group">
					<label for="batchAssignCostSellValue">Value</label>
					<input type="number" step="any" id="batchAssignCostSellValue" class="form-control">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-batch-assign-cost-sell">
					Submit
				</button>
			</div>
		</div>
    </div>
</div>
