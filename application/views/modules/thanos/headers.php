<th class="text-center" scope="col">&#35;</th>
<th class="text-center" scope="col">
    <input type="checkbox" class="header-selector" aria-label="Checkbox to select all shown rows">
</th>
<th class="text-center" scope="col"><i class="fa fa-file-pdf" aria-hidden="true"></i></th>
<th class="header-sortable" scope="col" data-column="device_product_name">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Part Number
</th>
<th class="header-sortable" scope="col" data-column="device_serial_number">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Serial Number
</th>
<th class="header-sortable" scope="col" data-column="created">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Created
</th>
<th class="header-sortable" scope="col" data-column="parent_dir">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Directory
</th>
<th class="header-sortable" scope="col" data-column="lot">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Lot
</th>
<th class="header-sortable" scope="col" data-column="disposition">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Disposition
</th>
<th class="header-sortable" scope="col" data-column="cost">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Cost
</th>
<th class="header-sortable" scope="col" data-column="sell">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Sell
</th>
<th class="header-sortable" scope="col" data-column="erase_method">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Erase Method
</th>
<th class="header-sortable" scope="col" data-column="device_size">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Device Size
</th>
<th class="header-sortable" scope="col" data-column="result">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Results
</th>
<th class="header-sortable" scope="col" data-column="status">
    <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
    <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
    Status
</th>
