<div class="modal fade" id="addDeviceModal" tabindex="-1" aria-labelledby="addDeviceModalLabel"
	 aria-hidden="true">
    <div class="modal-dialog" id="modalsize">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="addDeviceModalLabel">Add Serial</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col">
						<div class="custom-control custom-switch switch-multi-mode" style="float: right;">
							<input type="checkbox" class="custom-control-input" id="switchmultiMode">
							<label class="custom-control-label" for="switchmultiMode">Scan Mode</label>
						</div>
						<div class="form-group">
							<label for="addDevicePartNumber">Part Number</label>
							<input id="addDevicePartNumber" class="form-control" placeholder="Part Number" type="text">
						</div>
						<div class="form-group">
							<label for="addDeviceVendor">Vendor</label>
							<select id="addDeviceVendor" class="form-control" placeholder="Purchase Order"
                                    type="text">
									<option selected>Choose...</option>
								<?php foreach ($vendor_po as $vendor): ?>
									<option value="<?= $vendor->vendor_name ?>">
                                        <?= $vendor->vendor_name ?>
                                    </option>
								<?php endforeach; ?>
							</select>
						</div>
						
						<div class="form-group">
						<label for="createPurchaseOrderName">Purchase Order</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text" id="createPurchaseOrderPrefix">PO</div>
							</div>
							<select id="addDevicePO" class="form-control" placeholder="Select a Vendor first"
                                    type="text">
									<option selected>Select a Vendor first</option>
							</select>
						</div>
						</div>
						
						<div class="form-group">
						    <label for="addDeviceSize">Device Size</label>
						    <div class="input-group mb-3">
								<input id="addDeviceSize" class="form-control" placeholder="500, 1, etc." type="number" onkeydown="return event.keyCode !== 69">
								<div class="input-group-append">
									<select class="custom-select" id="sizeselect">
										<option value="GB">GB</option>
										<option value="TB">TB</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="addDeviceResultList">Result</label>
							<div class="input-group mb-3">
								<select id="addDeviceResultList" type="text" class="custom-select">
									<option selected>Choose...</option>
									<option value="Erased">Erased</option>
									<option value="Terminated">Terminated</option>
									<option value="Undetected">Undetected</option>
									<option value="Scanned In">Scanned In</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="addDeviceStatus">Status</label>
							<div class="input-group mb-3">
								<select id="addDeviceStatusList" type="text" class="custom-select">
									<option selected>Choose...</option>
									<option value="Defective">Defective</option>
								    <option value="Scanned">Scanned</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="addDeviceSerial">Serial Number</label>
							<input id="addDeviceSerial" class="form-control" placeholder="Serial Number" type="text">
						</div>
					</div>

					
					<div class="col collapse collapse-horizontal" id="listserial" style=" max-height: 600px; overflow-y: auto;">
						<div class="card card-body">
						<div class="card-tools">
								<div>
								<small>Press edit to remove data</small>
								<button id="removelistbtn" type="button" class="btn btn-link removelistbtn" style="float:right;" disabled>Remove</button>
								<button id="editlistbtn" type="button" class="btn btn-link" style="float:right;">Edit</button>
								</div>
								</div>
							<ul class="list-unstyled" id="serialList">
								
							</ul>
						</div>
						<div><small>Drives Count:</small><small id="serialcount"></small></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-add-device-submit">
					Submit
				</button>
			</div>
		</div>
    </div>
</div>
