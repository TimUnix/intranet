<div class="modal fade" id="viewCertificateModal" tabindex="-1" aria-labelledby="viewCertificateModalLabel"
	 aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="viewCertificateModalLabel">View Certificate</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="form-group">
					<label for="viewCertificateVendorOptions">Vendor</label>
					<select id="viewCertificateVendorOptions" class="custom-select" placeholder="Select a vendor">
						<option value="unixsurplus">UnixSurplus, Inc</option>
						<?php foreach ($vendors as $vendor): ?>
							<option value="<?= $vendor->vendor_name?>"><?= $vendor->vendor_name ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<a class="btn btn-primary btn-view-certificate" target="_blank"
				   href="<?= base_url() ?>index.php/thanos/nebula/certificate?searchQuery=
						 &searchColumn=device_serial_number&directories=&orderBy=created&sortOrder=ASC
						 &vendor=unixsurplus">
					View Certificate
				</a>
			</div>
		</div>
    </div>
</div>
