<div class="modal fade" id="createLotModal" tabindex="-1" aria-labelledby="createLotModalLabel" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="createLotModalLabel">Create Lot</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="create-lot-banner bg-warning py-1 pl-2 mb-2 d-none"></div>
				<form id="createLotForm">
					<div class="form-group">
						<label for="createLotDirectory">Purchase Order</label>
						<select class="custom-select create-lot-directory" id="createLotDirectory" required>
							<option></option>
							<?php foreach ($directories as $directory): ?>
								<option value="<?= $directory->name ?>"><?= $directory->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="createLotCustomerCode">Customer Code</label>
						<input id="createLotCustomerCode" type="text" class="form-control create-lot-customer-code"
							   required>
					</div>
				</form>
				<div class="w-100 text-center d-none create-lot-spinner">
					<div class="spinner-border text-info" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-create-lot-submit" form="createLotForm">
					Submit
				</button>
			</div>
		</div>
    </div>
</div>
