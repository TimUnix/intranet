<div class="col-2">
    <?php foreach ($totals as $directory_total): ?>
        <div class="directory-total">
            <span>
                <strong>Total Drives of <?= $directory_total['directory'] ?>:</strong> <?= $directory_total['total']?>
            </span>
        </div>
    <?php endforeach; ?>
</div>
<div class="col-2">
    <?php foreach ($current_date_totals as $directory_total): ?>
        <div class="current-date-directory-total">
            <span>
                <strong>Drives Today of <?= $directory_total['directory'] ?>:</strong>
                <?= $directory_total['total']?>
            </span>
        </div>
    <?php endforeach; ?>
</div>

<?php foreach ($device_size_results as $result => $device_results): ?>
	<div class="col-2">
		<h3>
			<?= $result ?>:
			<?= array_key_exists($result, $device_size_totals) ? $device_size_totals[$result] : 0 ?>
		</h3>
		<ul>
			<?php foreach ($device_results as $size => $device_result): ?>
				<li class="device-size-totals">
					<strong><?= $size ?>:</strong>
					<?php if (array_key_exists('size', $device_result)): ?>
						<?= $device_result['size'] ?>
					<?php else: ?>
						0
					<?php endif; ?>
					<?php if (array_key_exists('verified', $device_result)): ?>
						, <?= $device_result['verified'] ?> Scanned
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
	</ul>
    </div>
<?php endforeach; ?>
