<tr>
    <th scope="row">Filename</th>
    <td><?= $report->filename ?></td>
</tr>
<tr>
    <th scope="row">Created</th>
    <td><?= str_replace('2015', '2022', date_format(date_create($report->created), 'D m/d/Y')) ?></td>
</tr>
<tr>
    <th scope="row">Title</th>
    <td><?= $report->title ?></td>
</tr>
<tr>
    <th scope="row">Directory</th>
    <td class="report-parent-dir"><?= $report->parent_dir ?></td>
</tr>
<tr>
    <th scope="row">Erase Method</th>
    <td><?= $report->erase_method ?></td>
</tr>
<tr>
    <th scope="row">Process Integrity</th>
    <td><?= $report->process_integrity ?></td>
</tr>
<tr>
    <th scope="row">Fingerprint</th>
    <td><?= $report->fingerprint ?></td>
</tr>
<tr>
    <th scope="row">Device Title</th>
    <td><?= $report->device_title ?></td>
</tr>
<tr>
    <th scope="row">Device Serial Number</th>
    <td><?= $report->device_serial_number ?></td>
</tr>
<tr>
    <th scope="row">Device Platform Name</th>
    <td><?= $report->device_platform_name ?></td>
</tr>
<tr>
    <th scope="row">Device Product Name</th>
    <td><?= $report->device_product_name ?></td>
</tr>
<tr>
    <th scope="row">Device Type</th>
    <td><?= $report->device_type ?></td>
</tr>
<tr>
    <th scope="row">Device Revision</th>
    <td><?= $report->device_revision ?></td>
</tr>
<tr>
    <th scope="row">Device Size</th>
    <td><?= $report->device_size ?></td>
</tr>
<tr>
    <th scope="row">SMART Firmware Version</th>
    <td>
        <?php if (isset($report->smart_firmware_version)): ?>
            <?= $report->smart_firmware_version ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Capacity</th>
    <td>
        <?php if (isset($report->smart_capacity)): ?>
            <?= $report->smart_capacity ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART ATA Version</th>
    <td>
        <?php if (isset($report->smart_ata_version)): ?>
            <?= $report->smart_ata_version ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART ATA Standard</th>
    <td>
        <?php if (isset($report->smart_ata_standard)): ?>                                
            <?= $report->smart_ata_standard ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Support</th>
    <td>
        <?php if (isset($report->smart_support)): ?>
            <?= $report->smart_support ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Off-line Data Collection Status</th>
    <td>
        <?php if (isset($report->smart_offline_data_collection_status)): ?>
            <?= $report->smart_offline_data_collection_status ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Self-Test Execution Status</th>
    <td>
        <?php if (isset($report->smart_self_test_execution_status)): ?>
            <?= $report->smart_self_test_execution_status ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Time Off-line Data Collection, sec</th>
    <td>
        <?php if (isset($report->smart_time_offline_data_collection_sec)): ?>
            <?= $report->smart_time_offline_data_collection_sec ?>
        <?php endif; ?>    
    </td>
</tr>
<tr>
    <th scope="row">SMART Off-line Data Collection Capabilities</th>
    <td>
        <?php if (isset($report->smart_time_offline_data_collection_capabilities)): ?>
            <?= $report->smart_time_offline_data_collection_capabilities ?>
        <?php endif; ?>    
    </td>
</tr>
<tr>
    <th scope="row">SMART Capabilities</th>
    <td>
        <?php if (isset($report->smart_capabilities)): ?>
            <?= $report->smart_capabilities ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Error Logging Capabilities</th>
    <td>
        <?php if (isset($report->smart_error_logging_capabilities)): ?>
            <?= $report->smart_error_logging_capabilities ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">SMART Short Self-test Time, min</th>
    <td>
        <?php if (isset($report->smart_short_self_test_time_min)): ?>
            <?= $report->smart_short_self_test_time_min ?>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th scope="row">Geometry Partitioning</th>
    <td><?= $report->geometry_partitioning ?></td>
</tr>
<tr>
    <th scope="row">Geometry Total Sec</th>
    <td><?= $report->geometry_total_sec ?>
    </td>
</tr>
<tr>
    <th scope="row">Geometry First Sec</th>
    <td><?= $report->geometry_first_sec ?></td>
</tr>
<tr>
    <th scope="row">Geometry BPS</th>
    <td><?= $report->geometry_bps ?></td>
</tr>
<tr>
    <th scope="row">Results Started</th>
    <td>
        <?= str_replace('2015', '2022', date_format(date_create($report->results_started), 'D m/d/Y')) ?>
    </td>
</tr>
<tr>
    <th scope="row">Results Elapsed</th>
    <?php $elapsed_result = explode(' ', $report->results_elapsed); ?>
    <td>
        <?= $elapsed_result[0] ?>
    </td>
</tr>
<tr>
    <th scope="row">Results Errors</th>
    <td><?= $report->results_errors ?></td>
</tr>
<tr>
    <th scope="row">Result</th>
    <td><?= $report->result ?></td>
</tr>
<tr>
    <th scope="row">Process Name</th>
    <td><?= $report->process_name ?></td>
</tr>
<tr>
    <th scope="row">Process Started At</th>
    <td>
        <?= str_replace('2015', '2022', date_format(date_create($report->process_started_at), 'D m/d/Y')) ?>
    </td>
</tr>
<tr>
    <th scope="row">Process Elapsed</th>
    <?php $elapsed_process = explode(' ', $report->process_elapsed); ?>
    <td><?= $elapsed_process[0] ?></td>
</tr>
<tr>
    <th scope="row">Process Result</th>
    <td><?= $report->process_result ?></td>
</tr>
<tr>
    <th scope="row">Conclusion</th>
    <td><?= $report->conclusion ?></td>
</tr>
