<?php foreach ($lots as $lot): ?>
    <div class="form-check form-check-inline mx-0 mb-1">
        <div class="input-group-text lot-container">
            <input class="form-check-input purchase-order-lot" type="checkbox"
                   value="<?= $lot->purchase_order ?> <?= $lot->customer_code ?>" id="lot-<?= $lot->customer_code ?>">
            <label class="form-check-label" for="lot-<?= $lot->customer_code ?>">
                <?= $lot->purchase_order ?> <?= $lot->customer_code ?>
            </label>
        </div>
    </div>
<?php endforeach; ?>
