<?php foreach ($reports as $index => $report): ?>
    <?php
    $is_erased = $report->result === 'Erased';
	$is_defective = $report->status === 'Defective';
    $background_color = $is_erased ? 'result-erased' : 'result-other';
	$background_color = $is_defective ? 'result-defective' : $background_color;

    $now_year = (int) date('Y');
    $created_year = (int) date('Y', strtotime($report->created));
    ?>
    <tr class="<?= $background_color ?>">
        <th scope="row" class=" text-center px-0 py-auto">
            <?= $offset + $index + 1 ?>
            <button type="button" class="btn btn-link btn-md btn-report-info" data-toggle="modal"
                    data-target="#fullReportModal">
                <i class="fa fa-info" aria-hidden="true"></i>
            </button>
        </th>
        <td class="text-center px-0 py-auto report-selector">
            <input type="checkbox" class="align-middle" aria-label="Checkbox for row selection">
        </td>
        <td class="text-center px-0 py-auto">
            <?php if ($report->filename !== ''): ?>
                <?php $filename = urlencode(str_replace(
                    'Report',
                    'Certificate',
                    str_replace('.xml', '.pdf', $report->filename))); ?>
                <a class="btn btn-link" href='<?= base_url("nebula/certificates/pdf/{$filename}") ?>'
                   target="_blank">
                    <i class="fa fa-file-pdf" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
        </td>
        <td data-value="<?= $report->device_product_name ?>" class="report-product-name">
            <button type="button" class="btn btn-link btn-copy">
                <i class="fa fa-copy btn-copy-icon" aria-hidden="true"></i>
            </button>
            <?= $report->device_product_name ?>
        </td>
        <td data-value="<?= $report->device_serial_number ?>" class="report-serial-number">
            <button type="button" class="btn btn-link btn-copy">
                <i class="fa fa-copy btn-copy-icon" aria-hidden="true"></i>
            </button>
            <?= $report->device_serial_number ?>
        </td>
        <td>
            <?php if ($created_year === 2022): ?>
                <?= $report->created ?>
            <?php else: ?>
                <?= date('D m/d/Y', strtotime('16 Jan 2022')) ?>
            <?php endif; ?>
        </td>
        <td class="report-parent-dir"><?= $report->parent_dir ?></td>
        <td class="report-lot" data-value="<?= $report->lot ?>"><?= $report->lot ?></td>
        <td class="report-disposition" data-value="<?= $report->disposition ?>"><?= $report->disposition ?></td>
        <td class="report-cost report-editable" data-value="<?= $report->cost ?>" data-field="cost">
            <div class="input-group report-editable-field-container d-none w-auto">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="reportCost<?= $offset + $index + 1 ?>">$</span>
                </div>
                <input class="form-control report-editable-field" type="number" step="any" style="min-width: 8ch;"
                       aria-describedby="reportCost<?= $offset + $index + 1?>" value="<?= $report->cost ?>">
                <div class="input-group-append">
                    <button type="button" class="btn btn-danger btn-report-edit-cancel">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-success btn-report-edit-apply">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <button type="button" class="btn btn-link btn-report-edit">
                <i class="fa fa-edit" aria-hidden="true"></i>
            </button>
            <div class="report-editable-value">
                <?php if (isset($report->cost) && $report->cost !== ''): ?>
                    $<?= $report->cost ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="report-sell report-editable" data-value="<?= $report->sell ?>" data-field="sell">
            <div class="input-group report-editable-field-container d-none">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="reportSell<?= $offset + $index + 1 ?>">$</span>
                </div>
                <input class="form-control report-editable-field" type="number" step="any" style="min-width: 8ch;"
                       aria-describedby="reportSell<?= $offset + $index + 1?>" value="<?= $report->cost ?>">
                <div class="input-group-append">
                    <button type="button" class="btn btn-danger btn-report-edit-cancel">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-success btn-report-edit-apply">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <button type="button" class="btn btn-link btn-report-edit">
                <i class="fa fa-edit" aria-hidden="true"></i>
            </button>
            <div class="report-editable-value">
                <?php if (isset($report->sell) && $report->sell !== ''): ?>
                    $<?= $report->sell ?>
                <?php endif; ?>
            </div>
        </td>
        <td><?= $report->erase_method ?></td>
        <td><?= $report->device_size ?></td>
        <td class="report-result" data-value="<?= $report->result ?>">
            <?= $report->result ?>
        </td>
		<td class="report-status report-editable" data-value="<?= $report->status ?>" data-field="status">
			<?php if (!isset($report->status) || $report->status === ''): ?>
				<button type="button" class="btn btn-link btn-report-defective">
					<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
					Mark Defective
				</button>
			<?php else: ?>
				<p><?= $report->status ?></p>
				<p><?= $report->status_last_edited?></p>
				<p><?= $report->status_last_edited_by ?></p>
				<button type="button" class="btn btn-link btn-clear-status">
					<i class="fa fa-times" aria-hidden="true"></i>
					Clear
				</button>
			<?php endif; ?>
		</td>
    </tr>
<?php endforeach; ?>
