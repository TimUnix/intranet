<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/thanos/nebula_full_report_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_create_lot_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_rename_lot_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_create_purchase_order_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_rename_purchase_order_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_view_certificate_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_batch_assign_cost_sell_modal'); ?>
<?php $this->load->view('modules/thanos/nebula_add_device_modal'); ?>

<style>
 .header-sortable:hover {
     cursor: pointer;
 }

 .toast-status {
     font-size: 1.4rem;
 }

 .result-erased {
     background-color: #74eda0 !important;
 }

 .result-erased:hover {
     background-color: #58eb8d !important;
 }

 .result-other {
     background-color: #fff18f !important;
 }

 .result-other:hover {
     background-color: #ffee79 !important;
 }

 .result-defective {
	 background-color: #e08e8e !important;
 }

 .result-defective:hover {
	 background-color: #d78686 !important;
 }

 .btn-report-edit, .btn-report-defective, .btn-clear-status {
     display: none;
 }

 .report-editable:hover .btn-report-edit,
 .report-editable:hover .btn-report-defective,
 .report-status:hover .btn-clear-status {
     display: inline;
 }
</style>

<div class="container-fluid" style="margin-bottom: 80px;">
    <div class="card">
        <div class="card-header">
            Nebula
			<div class="card-tools">
				<div class="custom-control custom-switch switch-verify-mode">
					<input type="checkbox" class="custom-control-input" id="switchVerifyMode">
					<label class="custom-control-label" for="switchVerifyMode">Scan Mode</label>
				</div>
			</div>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form id="searchForm">
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <div class="input-group-prepend">
                                <button class="btn btn-lg btn-primary" type="submit" id="btnSearch">
                                    <i class="fa fa-search"></i></button>
                            </div>
                            <select id="searchColumn" class="custom-select col-md-2">
                                <option value="filename" selected>Filename</option>
                                <option value="device_size">Device Size</option>
                                <option value="device_serial_number" selected>Device Serial Number</option>
                                <option value="erase_method">Erase Method</option>
                                <option value="result">Results</option>
                            </select>
                            <input class="form-control input-lg" type="search" id="searchQuery"
                                   placeholder="Search">
                        </div>
                    </div>
                    <fieldset>
                        <legend>Purchase Orders</legend>
                        <?php foreach ($directories as $directory): ?>
                            <div class="form-check form-check-inline mx-0 mb-1 form-check-directory-filter">
                                <div class="input-group-text directory-filters-container">
                                    <input class="form-check-input directory-filter" type="checkbox"
                                           value="<?= $directory->name ?>" id="directory-<?= $directory->name ?>">
                                    <label class="form-check-label" for="directory-<?= $directory->name ?>">
                                        <?= $directory->name ?>
                                    </label>
                                </div>
                            </div>
                        <?php endforeach; ?>
						<div class="purchase-order-lots mt-1"></div>
                    </fieldset>
                </form>
            </div>
            <button type="button" data-toggle="modal" data-target="#viewCertificateModal"
					class="btn btn-lg btn-primary mt-3">
                <i class="fa fa-certificate mr-1" aria-hidden="true"></i>
                View Certificate
            </button>
			<button type="button" class="btn btn-primary btn-lg mt-3 btn-create-purchase-order" data-toggle="modal"
					data-target="#createPurchaseOrderModal">
				<i class="ion-document-text mr-1" aria-hidden="true"></i>
				Create Purchase Order
			</button>
			<button type="button" class="btn btn-primary btn-lg mt-3 btn-rename-purchase-order" data-toggle="modal"
						  data-target="#renamePurchaseOrderModal">
				<i class="ion-document-text mr-1" aria-hidden="true"></i>
				Rename Purchase Order
			</button>
			<button type="button" class="btn btn-primary btn-lg mt-3 btn-create-lot" data-toggle="modal"
						  data-target="#createLotModal">
				<i class="fa fa-tag mr-1" aria-hidden="true"></i>
				Create Lot
			</button>
			<button type="button" class="btn btn-primary btn-lg mt-3 btn-rename-lot" data-toggle="modal"
					data-target="#renameLotModal">
				<i class="fa fa-tag mr-1" aria-hidden="true"></i>
				Rename Lot
			</button>
			<button type="button" class="btn btn-primary btn-lg mt-3 btn-add-device" data-toggle="modal"
						  data-target="#addDeviceModal">
				<i class="fa fa-hdd mr-1" aria-hidden="true"></i>
				Add Serial
			</button>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Summary
            <div class="card-tools">
                <button class="btn btn-tool" type="button" data-card-widget="collapse">
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="totals-container row"></div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Reports
            <span class="dispositions-container mx-2">
                <button type="button" class="btn btn-sm btn-primary btn-disposition" data-disposition="Destroy">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    Destroy
                </button>
                <button type="button" class="btn btn-sm btn-primary btn-disposition" data-disposition="Recycle">
                    <i class="fa fa-recycle" aria-hidden="true"></i>
                    Recycle
                </button>
                <button type="button" class="btn btn-sm btn-primary btn-disposition" data-disposition="Inventory">
                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                    Inventory
                </button>
                <button type="button" class="btn btn-sm btn-primary btn-disposition" data-disposition="Clear">
                    <i class="fa fa-eraser" aria-hidden="true"></i>
                    Clear
                </button>
				<span class="dropdown dropright">
					<button class="btn btn-sm btn-outline-primary dropdown-toggle" role="button"
							id="dropdownMenuBatchSelector" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
						Filter Part Number
					</button>
					<div class="dropdown-menu batch-selector-options p-2"
						 aria-labelledby="dropdownMenuBatchSelector">
					</div>
				</span>
            </span>
            <div class="card-tools">
				<button type="button" class="btn btn-sm btn-primary btn-assign-cost-sell" data-toggle="modal"
						data-target="#batchAssignCostSellModal">
					<i class="fa fa-credit-card" aria-hidden="true"></i>
					Assign Cost/Sell
				</button>
				<button type="button" class="btn btn-sm btn-primary btn-assign-lot">
					<i class="fa fa-tags" aria-hidden="true"></i>
					Assign Lot
				</button>
                <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
            </div>
        </div>
        <div class="horizontal-scroller d-none"><div class="scroller-content"></div></div>
        <div class="card-body sticky-headers p-0 d-none">
            <table class="table table-bordered table-striped table-hover sticky-table">
                <thead class="bg-white border">
                    <tr class="sticky-header-row">
                        <?php $this->load->view('modules/thanos/headers.php'); ?>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-body table-responsive p-0 table-container">
            <table class="table table-bordered table-striped table-hover main-table" id="reportsTable"
                   cellspacing="0" style="border-collapse: separate;">
                <thead class="bg-white border">
                    <tr class="main-header-row">
						<?php $this->load->view('modules/thanos/headers.php'); ?> 
                    </tr>
                </thead>
                <tbody id="reports">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('templates/common/spinner'); ?>

<script type="text/javascript" id="nebulaScript" data-base-url="<?= base_url() ?>"
        src="<?= base_url() ?>resources/js/nebula/nebula.js"></script>
<?php $this->load->view('templates/common/footer'); ?>
