<div class="modal fade" id="renamePurchaseOrderModal" tabindex="-1" aria-labelledby="renamePurchaseOrderModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="renamePurchaseOrderModalLabel">Rename Purchase Order</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="rename-purchase-order-banner bg-warning py-1 pl-2 mb-2 d-none"></div>
				<form id="renamePurchaseOrderForm">
					<div class="form-group">
						<label for="renamePurchaseOrderOption">Purchase Order</label>
						<select id="renamePurchaseOrderOption" class="custom-select">
							<option></option>
							<?php foreach($directories as $directory): ?>
								<option value="<?= $directory->name ?>"><?= $directory->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="renamePurchaseOrderName">New Name</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text" id="renamePurchaseOrderPrefix">PO</div>
							</div>
							<input type="text" id="renamePurchaseOrderName" class="form-control" required
								   aria-describedby="renamePurchaseOrderPrefix">
						</div>
					</div>
				</form>
				<div class="w-100 text-center d-none rename-purchase-order-spinner">
					<div class="spinner-border text-info" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-rename-purchase-order-submit"
						form="renamePurchaseOrderForm">
					Submit
				</button>
			</div>
		</div>
    </div>
</div>
