<div class="modal fade" id="renameLotModal" tabindex="-1" aria-labelledby="renameLotModalLabel" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="renameLotModalLabel">Rename Lot</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="rename-lot-banner bg-warning py-1 pl-2 mb-2 d-none"></div>
				<form id="renameLotForm">
					<div class="form-group">
						<label for="renameLotOptions">Lot</label>
						<select class="custom-select rename-lot-options" id="renameLotOptions" required>
							<option></option>
							<?php foreach ($lots as $lot): ?>
								<option value="<?= $lot->purchase_order ?> <?= $lot->customer_code ?>">
									<?= $lot->purchase_order ?> <?= $lot->customer_code ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="renameLotCustomerCode">Customer Code</label>
						<input id="renameLotCustomerCode" type="text" class="form-control rename-lot-customer-code"
							   required>
					</div>
				</form>
				<div class="w-100 text-center d-none rename-lot-spinner">
					<div class="spinner-border text-info" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-rename-lot-submit" form="renameLotForm">
					Submit
				</button>
			</div>
		</div>
    </div>
</div>
