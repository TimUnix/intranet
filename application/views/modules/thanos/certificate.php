<html lang="en">
    <?php $this->load->view('templates/common/head'); ?>
    <body>
		<style>
		 .vendor-logo {
			 border-radius: 2px;
		 }

		 .summary-container, .report-container {
			 display: flex;
			 flex-flow: row nowrap;
			 justify-content: space-between;
		 }

		 .report-ruler {
			 margin-top: 0;
			 margin-bottom: 0;
			 padding: 0;
		 }

		 dl {
			 margin: 0 4px 4px 0;
			 padding: 0;
		 }
		</style>
        <div class="container-fluid">
            <div class="row text-center" style="background-color: powderblue;">
                <div class="col-4">
                    <img width="150" class="vendor-logo" src="<?= base_url() ?><?= $data['vendor_image'] ?>">
                </div>
                <div class="col-4 text-center">
                    <h2 class="ml-auto">A c t i v e @ K i l l D i s k 1 3 . 0 . 1 1</h2>
                </div>
                <div class="col-4"></div>
            </div>
            <div class="row text-center border">
                <div class="col-4 my-auto">
                    <button type="button" class="btn btn-primary btn-lg d-print-none btnPrintCertificate">
                        <i class="fa fa-certificate" aria-hidden="true"></i>
                        Print Certificate
                    </button>
                </div>
                <div class="col-4 my-auto"><h1>ERASE CERTIFICATE</h1></div>
                <div class="col-4 mx-auto">
                    <img src="<?= base_url() ?>resources/img/killdisk.png" width="100" class="mr-auto">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-2">
                <h3 class="my-auto">Summary</h3>
            </div>
			<div class="summary-container mt-1">
				<dl>
					<dt class="mr-1">Drives:</dt>
					<dd><?= $data['drive_count'] ?> Drives</dd>
				</dl>
				<dl>
					<dt class="mr-1">Pass:</dt>
					<dd><?= $data['drives_passed_count'] ?> Drives</dd>
				</dl>
				<dl>
					<dt class="mr-1">Failed:</dt>
					<dd><?= $data['drives_failed_count'] ?> Drives</dd>
				</dl>
				<dl>
					<dt class="mr-1">Undetected:</dt>
					<dd><?= $data['drives_undetected_count'] ?> Drives</dd>
				</dl>
				<dl>
					<dt class="mr-1">Erase Method:</dt>
					<dd>US DoD 5220.22-M</dd>
				</dl>
				<dl>
					<dt class="mr-1">Pass 1:</dt>
					<dd>Pass 1 (0x000000000000)</dd>
				</dl>
				<dl>
					<dt class="mr-1">Pass 2:</dt>
					<dd>Pass 2 (0x000000000000)</dd>
				</dl>
				<dl>
					<dt class="mr-1">Pass 3:</dt>
					<dd>Pass 3 (0x000000000000)</dd>
				</dl>
				<dl>
					<dt class="mr-1">Verification:</dt>
					<dd>10%</dd>
				</dl>
			</div>
			<div class="row mt-2">
				<h3>Drive Report</h3>
			</div>
            <?php $now_month = (int) date('m'); ?>
            <?php foreach($data['drives'] as $drive): ?>
                <hr class="report-ruler">
				<div class="report-container mt-1">
                    <dl>
                        <dt class="mr-1">Started:</dt>
                        <dd>
                            <?php $started_at_year = (int) date('Y', strtotime($drive->process_started_at)); ?>
                            <?php if ($started_at_year === 2022): ?>
                                <?= $drive->process_started_at ?>
                            <?php else: ?>
                                <?php $started_at_time = date('h:m:s', strtotime($drive->process_started_at)); ?>
                                <?= "2022-01-16 $started_at_time"?>
                            <?php endif; ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Serial Number:</dt>
                        <dd><?= $drive->device_serial_number ?></dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Result:</dt>
                        <dd><?= $drive->result ?></dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Completed:</dt>
                        <dd>
                            <?php $created_year = (int) date('Y', strtotime($drive->process_started_at)); ?>
                            <?php if ($created_year === 2022): ?>
                                <?= $drive->created ?>
                            <?php else: ?>
                                <?php $created_time = date('h:m:s', strtotime($drive->created)); ?>
                                <?= "2022-01-16 $created_time" ?>
                            <?php endif; ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Size:</dt>
                        <dd><?= $drive->device_size ?></dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Integrity:</dt>
                        <dd><?= $drive->process_integrity ?></dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Device Model:</dt>
                        <dd><?= $drive->device_product_name ?></dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Sectors:</dt>
                        <dd><?= $drive->geometry_total_sec ?></dd>
                    </dl>
                    <dl>
                        <dt class="mr-1">Errors:</dt>
                        <dd><?= $drive->results_errors ?></dd>
                    </dl>
                </div>
            <?php endforeach; ?>
        </div>
        <script type="text/javascript" src="<?= base_url() ?>resources/js/nebula/nebula_certificate.js"></script>
    </body>
</html>
