<div class="modal fade" id="createPurchaseOrderModal" tabindex="-1" aria-labelledby="createPurchaseOrderModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="createPurchaseOrderModalLabel">Create Purchase Order</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="modal-body">
				<div class="create-purchase-order-banner bg-warning py-1 pl-2 mb-2 d-none"></div>
				<form id="createPurchaseOrderForm">
					<div class="form-group">
						<label for="createPurchaseOrderLocation">Location</label>
						<select id="createPurchaseOrderLocation" class="custom-select" required>
							<option value="staclara">Sta. Clara</option>
							<option value="merced">Merced</option>
						</select>
					</div>
					<div class="form-group">
						<label for="createPurchaseOrderName">Purchase Order</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text" id="createPurchaseOrderPrefix">PO</div>
							</div>
							<input type="text" id="createPurchaseOrderName" class="form-control" required
								   aria-describedby="createPurchaseOrderPrefix">
						</div>
					</div>
                    <div class="form-group">
                        <label for="createPurchaseOrderVendor">Vendor</label>
					    <select id="createPurchaseOrderVendor" class="form-control" placeholder="Vendor" type="text">
						    <?php foreach ($vendors as $vendor): ?>
							    <option value="<?= $vendor->vendor_name ?>"><?= $vendor->vendor_name ?></option>
						    <?php endforeach; ?>
					    </select>
                    </div>
				</form>
				<div class="w-100 text-center d-none create-purchase-order-spinner">
					<div class="spinner-border text-info" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-create-purchase-order-submit"
						      form="createPurchaseOrderForm">
					Submit
				</button>
			</div>
		</div>
    </div>
</div>
