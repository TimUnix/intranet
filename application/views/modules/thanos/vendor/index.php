<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<style>
 .btn-edit-name {
     display: none;
 }

 .card:hover .btn-edit-name {
     display: inline-block;
 }

 .btn-vendor-upload {
     display: none;
 }

 .card-img-overlay:hover .btn-vendor-upload {
     display: inline-block;
 }
</style>

<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			Nebula Vendor
		</div>
		<div class="card-body">
			<h5>Add Vendor</h5>
			<form action="<?= base_url() ?>index.php/thanos/vendor/add_vendor" method="POST"
				  enctype="multipart/form-data" class="add-vendor-form">
				<div class="form-group">
					<label for="addVendorName">Vendor Name</label>
					<input type="text" name="vendor-name" id="addVendorName" class="form-control" required>
				</div>
				<div class="custom-file mb-2">
					<input type="file" name="logo-path" id="addVendorPhoto" class="custom-file-input"
                           accept="image/png, image/jpeg">
					<label class="custom-file-label" for="addVendorPhoto">Upload logo</label>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>

	<h4>Vendors</h4>
	<div class="card-columns">
		<?php foreach ($vendors as $vendor): ?>
			<div class="card card-vendor">
				<img class="card-img-top" 
					 <?php if (isset($vendor->logo_path) && $vendor->logo_path !== ''):  ?>
                     src="<?= base_url() ?><?= $vendor->logo_path ?>"
                     <?php else: ?>
                     src="<?= base_url() ?>resources/img/noimage.png"
                     <?php endif; ?>
                     alt="<?= $vendor->vendor_name?> Logo">
                <div class="card-img-overlay text-right">
                    <label class="btn btn-primary btn-vendor-upload">
                        <input class="file-vendor-upload" type="file" accept="image/png, image/jpeg" hidden>
                        <i class="fa fa-pen" aria-hidden="true"></i>
                    </label>
                </div>
				<div class="card-body">
                    <div class="input-group vendor-editable d-none">
                        <input class="form-control vendor-input" type="text" value="<?= $vendor->vendor_name ?>">
                        <div class="input-group-append">
                            <button class="btn btn-outline-danger btn-vendor-cancel-edit">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-outline-success btn-vendor-save-change">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="vendor">
					    <h5 class="card-title vendor-name" data-name="<?= $vendor->vendor_name?>">
                            <?= $vendor->vendor_name ?>
                        </h5>
                        <button type="button" class="btn btn-link position-relative">
                            <i class="fa fa-pen btn-edit-name" aria-hidden="true"></i>
                        </button>
                    </div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<?php $this->load->view('templates/common/spinner'); ?>

<script type="text/javascript" id="nebulaVendorScript" data-base-url="<?= base_url() ?>"
		src="<?= base_url() ?>resources/js/nebula/nebulaVendor.js"></script>
<?php $this->load->view('templates/common/footer'); ?>
