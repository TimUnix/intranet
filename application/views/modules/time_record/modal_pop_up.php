<div class="modal fade" id="insert_success_time_out" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<div style="text-align:center;">	
                    <i class="fas fa-check-circle fa-5x" style="color:blue;"></i>
                </div>
                <div style="text-align:center;">
                   <strong> <h1>Awesome!</h1></strong><br>
                    <h5>Time out successfully recorded</h5>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="okay_modal_time_out" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="insert_success" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-check-circle fa-5x" style="color:blue;"></i>
                </div>
                <div style="text-align:center;">
                   <strong> <h1>Awesome!</h1></strong><br>
                    <h5>Time in successfully recorded</h5>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="okay_modal" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="validate_thirtymin_before" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>Try again later</h1></strong><br>
                    <h5>You may log in 30 minutes before your schedule time</h5><br>
                    <strong><h5>Schedule Time: </h5></strong><h5 id="schedule_time"></h5>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_validate_thirty_min" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="already_time_in" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>You have already Time In</h1></strong><br>
                    <h5>You can only time in once per day</h5><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_validate_already_time_in" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="sched_no_exist" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>Your schedule does no exist</h1></strong><br>
                    <h5>Please contact Human Resource for your schedule</h5><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_validate_sched_exist" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="leave_whole_day_approved" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>You are on leave today</h1></strong><br>
                    <h5>Please check 'My Request' for your approved leave this day</h5><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_validate_leave_whole_day" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tito_approved" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>TITO approved</h1></strong><br>
                    <h5>No need to log in today</h5><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_validate_tito_approved" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="restricted" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>Time Out Restricted!</h1></strong><br>
                    <h5>You are not allowed to time out 8 hours before your scheduled time in</h5><br>
                    <h5><strong>Please contact HR</strong></h5><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_restricted" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="fillup" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>Fill out the filter fields</h1></strong><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_fillup" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="fillupown" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>Fill out the filter date</h1></strong><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_fillupown" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="eod_filter_fill" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                   <strong> <h1>Filter Date Required</h1></strong><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_eod_filter_fill" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>