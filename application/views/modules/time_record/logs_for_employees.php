<div class="modal fade" id="log_form" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Insert Time Logs</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_logs"></div>
                <div class="row">
                    <label for="log_employee" class="col-md-3">Employee:</label>
                    <select class="form-control col-md-9" id="log_employee">
                                    <option value="" default></option>
                                    <?php foreach ($data['all_employees'] as $all_employees): ?>
                                    <option value="<?= $all_employees->email ?>">
                                        <?= $all_employees->firstname ?> <?= $all_employees->lastname ?>
                                    </option>
                                    <?php endforeach; ?>
                    </select>  
                </div><br>
                <div class="row">
                    <label for="log_status" class="col-md-3">Status:</label>
                    <select class="form-control col-md-9" id="log_status">
                        <option value="time_in" default>Time In</option>
                        <option value="time_out">Time Out</option>
                    </select>
                </div><br>  
                <div class="row"  style="display: none;" id="div_date_logs">
                    <label for="date_to_modify" class="col-md-3">Date In:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="date_to_modify" data-target-input="nearest">
                                <input type="text" id="datetomodify_" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#date_to_modify"/>
                                <div class="input-group-append" data-target="#date_to_modify" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>  
                <div class="row">
                    <label for="time_logs" class="col-md-3">Time:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="time_logs" data-target-input="nearest">
                                <input type="text" id="timelogs_" class="form-control datetimepicker-input" placeholder="Select Time" data-target="#time_logs"/>
                                <div class="input-group-append" data-target="#time_logs" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <label for="date_logs" class="col-md-3">Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="date_logs" data-target-input="nearest">
                                <input type="text" id="datelogs_" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#date_logs"/>
                                <div class="input-group-append" data-target="#date_logs" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>  
                <div class="row" style="display: none;" id="div_task_logs">
                    <label for="finished_task_logs" class="col-md-3">Finished Task</label>
                    <textarea class="col-md-9" id="finished_task_logs" rows="3"></textarea>
                </div><br>
                 <div class="row" style="display: none;" id="div_challenges_logs">
                    <label for="challenges_logs" class="col-md-3">Challenges</label>
                    <textarea class="col-md-9" id="challenges_logs" rows="3"></textarea>
                </div><br>
                <div class="row" style="display: none;" id="div_goals_logs">
                    <label for="goals_logs" class="col-md-3">Tomorrow's Goal</label>
                    <textarea class="col-md-9" id="goals_logs" rows="3"></textarea>
                </div>            
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="logs_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>