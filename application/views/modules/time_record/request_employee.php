<div class="modal fade" id="add_request_form" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Apply TITO</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_add_request"></div>
                <div class="row">
                    <label for="chosen_manager" class="col-md-3">Manager:</label>
                    <select class="form-control col-md-9" id="chosen_manager">
                        <option value="" default></option>
                            <?php foreach ($data['managers'] as $managers): ?>
                            <option value="<?= $managers->email ?>">
                                        <?= $managers->firstname ?> <?= $managers->lastname ?>
                                    </option>
                                    <?php endforeach; ?>
                    </select>
                </div><br>
                <div class="row">
                    <label for="date_to_request" class="col-md-3">Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="date_to_request" data-target-input="nearest">
                                <input type="text" id="datetorequest_" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#date_to_request"/>
                                <div class="input-group-append" data-target="#date_to_request" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>  
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="request_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="status_editor_approved" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Status</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_status_approve"></div>
                <div id="id-requester-approved"></div>
                <div id="email-requester-approved"></div>
                <div id="date-requester-approved"></div>
                <div class="row">
                    <label for="approver_note" class="col-md-3">Notes:</label>
                    <textarea class="form-control col-md-9" id="approver_note_approved" rows="3"></textarea>
                </div><br>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="status_approve_submit"> Approved </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="status_editor_denied" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Status</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_status_approve"></div>
                <div id="id-requester-denied" style="display:none;"></div>
                <div class="row">
                    <label for="approver_note" class="col-md-3">Notes:</label>
                    <textarea class="form-control col-md-9" id="approver_note_denied" rows="3"></textarea>
                </div><br>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="status_denied_submit"> Denied </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="my_status_editor" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Edit TITO request</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_status_approve"></div>
                <div id="id-my-requester" style="display:none;"></div>
                <div class="row">
                    <label for="my_approver_change" class="col-md-3">Approver:</label>
                    <select class="form-control col-md-9" id="my_approver_change">
                    <option value="" default></option>
                            <?php foreach ($data['managers'] as $managers): ?>
                            <option value="<?= $managers->firstname ?> <?= $managers->lastname ?>">
                                        <?= $managers->firstname ?> <?= $managers->lastname ?>
                                    </option>
                                    <?php endforeach; ?>
                    </select>
                </div><br>
                <div class="row">
                    <label for="date_to_request_change" class="col-md-3">Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="date_to_request_change" data-target-input="nearest">
                                <input type="text" id="datetorequest_" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#date_to_request_change"/>
                                <div class="input-group-append" data-target="#date_to_request_change" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="my_approve_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="already_approved_denied" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div>
                <div style="text-align:center;">
                   <strong> <h1>Already Approved/Denied</h1></strong><br>
                    <h5>Please have the employee apply again</h5>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" style="width:100%" data-dismiss = "modal"> Close </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approved_success" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-check-circle fa-5x" style="color:blue;"></i>
                </div>
                <div style="text-align:center;">
                   <strong> <h1>Awesome!</h1></strong><br>
                    <h5>Successfully Approved</h5>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" data-dismiss = 'modal' style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="denied_success" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align:center;">    
                    <i class="fas fa-check-circle fa-5x" style="color:blue;"></i>
                </div>
                <div style="text-align:center;">
                   <strong> <h1>Awesome!</h1></strong><br>
                    <h5>Successfully Denied</h5>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" data-dismiss = 'modal' style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add_request_leave" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Apply Leave</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_add_request_leave"></div>
                <div class="row">
                    <label for="chosen_manager" class="col-md-3">Manager:</label>
                    <select class="form-control col-md-9" id="chosen_manager_leave">
                        <option value="" default></option>
                            <?php foreach ($data['managers'] as $managers): ?>
                            <option value="<?= $managers->email ?>">
                                        <?= $managers->firstname ?> <?= $managers->lastname ?>
                                    </option>
                                    <?php endforeach; ?>
                    </select>
                </div><br>
                <div class="row">
                    <label for="leave_day_type" class="col-md-3">Day Type:</label>
                    <select class="form-control col-md-9" id="leave_day_type">
                        <option value="Whole Day">Whole Day</option>
                        <option value="Half Day">Half Day</option>
                    </select>
                </div><br>
                <div class="row">
                    <label for="start_leave_date" class="col-md-3">Start Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="start_leave_date" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#start_leave_date"/>
                                <div class="input-group-append" data-target="#start_leave_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>
                <div class="row" id="div_end_leave_date">
                    <label for="end_leave_date" class="col-md-3">End Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="end_leave_date" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#end_leave_date"/>
                                <div class="input-group-append" data-target="#end_leave_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>
                <div class="row" id="div_time_of_day">
                    <label for="time_of_day" class="col-md-3">Choose:</label>
                    <select class="col-md-9 form-control" id="time_of_day">
                        <option value=""></option>
                        <option value="Morning">Morning</option>
                        <option value="Afternoon">Afternoon</option>
                    </select>
                </div><br>
                <div class="row">
                    <label for="leave_type" class="col-md-3">Type:</label>
                    <select class="col-md-9 form-control" id="leave_type">
                        <option value=""></option>
                        <option value="Sick Leave">Sick Leave</option>
                        <option value="Bereavement Leave">Bereavement Leave</option>
                        <option value="Vacation Leave">Vacation Leave</option>
                        <option value="Emergency Leave">Emergency Leave</option>
                        <!-- <option value="Leave without Pay">Leave without Pay</option> -->
                    </select>
                </div><br>
                <div class="row">
                    <label for="leave_reason" class="col-md-3">Reason for Leave:</label>
                    <textarea class="form-control col-md-9" rows="3" id="leave_reason"></textarea>
                </div>
            </div><br>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="leave_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_request_leave" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Edit Leave</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_edit_request_leave"></div>
                <div id="id-edit-leave" style="display:none;"></div>
                <div class="row">
                    <label for="chosen_manager" class="col-md-3">Manager:</label>
                    <select class="form-control col-md-9" id="edit_chosen_manager_leave">
                            <?php foreach ($data['managers'] as $managers): ?>
                            <option value="<?= $managers->firstname ?> <?= $managers->lastname ?>">
                                        <?= $managers->firstname ?> <?= $managers->lastname ?>
                                    </option>
                                    <?php endforeach; ?>
                    </select>
                </div><br>
                <div class="row">
                    <label for="leave_day_type" class="col-md-3">Day Type:</label>
                    <select class="form-control col-md-9" id="edit_leave_day_type">
                        <option value="Whole Day">Whole Day</option>
                        <option value="Half Day">Half Day</option>
                    </select>
                </div><br>
                <div class="row">
                    <label for="start_leave_date" class="col-md-3">Start Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="edit_start_leave_date" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#edit_start_leave_date"/>
                                <div class="input-group-append" data-target="#edit_start_leave_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>
                <div class="row" id="div_edit_end_leave_date">
                    <label for="end_leave_date" class="col-md-3">End Date:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="edit_end_leave_date" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select Date" data-target="#edit_end_leave_date"/>
                                <div class="input-group-append" data-target="#edit_end_leave_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>
                <div class="row" id="div_edit_time_of_day">
                    <label for="time_of_day" class="col-md-3">Choose:</label>
                    <select class="col-md-9 form-control" id="edit_time_of_day">
                        <option value="Morning">Morning</option>
                        <option value="Afternoon">Afternoon</option>
                    </select>
                </div><br>
                <div class="row">
                    <label for="leave_type" class="col-md-3">Type:</label>
                    <select class="col-md-9 form-control" id="edit_leave_type">
                        <option value="Sick Leave">Sick Leave</option>
                        <option value="Bereavement Leave">Bereavement Leave</option>
                        <option value="Vacation Leave">Vacation Leave</option>
                        <option value="Emergency Leave">Emergency Leave</option>
                        <option value="Leave without Pay">Leave without Pay</option>
                    </select>
                </div><br>
                <div class="row">
                    <label for="leave_reason" class="col-md-3">Reason for Leave:</label>
                    <textarea class="form-control col-md-9" rows="3" id="edit_leave_reason"></textarea>
                </div>
            </div><br>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="edit_leave_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>