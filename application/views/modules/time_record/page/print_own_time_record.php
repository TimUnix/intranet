
<div>
      <center><img  width="200px" src="<?php echo base_url();  ?>images/unixsurplus.png"></center>
</div><br>
<span><strong>Employee Name: </strong><?= $fullname ?></span><br>
<span></span><strong>Email: </strong><span><?= $email ?></span><br>
<h2><strong>Total No Hours: </strong><?= $total_hours ?></h2>
		
<div class="card-body p-0 inventory-container table-responsive table-scroll">
	<table class="table table-bordered table-hover paginated w-100" style="border-collapse: separate; text-align: center;" cellspacing="10px">
        <thead class="bg-white position-sticky border" style="top: 0;">
	        <tr>
	            <th scope="col" nowrap>Status</th>
	            <th scope="col" nowrap>Time In</th>
	            <th scope="col" nowrap>Time Out</th>
	            <th scope="col" nowrap>Date</th> 
                <th scope="col" nowrap>Schedule In</th>
                <th scope="col" nowrap>Schedule Out</th> 
                <th scope="col" nowrap>No of Hours</th>
            </tr>
	    </thead>
	    	
	    <tbody>
			<?php { foreach($pdf_detail as $item): ?>
	    		<tr>
	    			<td scope="col" nowrap><?= $item->status ?></td>
	    			<td scope="col" nowrap><?= $item->time_in ?></td>
	    			<td scope="col" nowrap><?= $item->time_out ?></td>
	    			<td scope="col" nowrap><?= $item->date_in ?></td>
	    			<td scope="col" nowrap><?= $item->schedule_in ?></td>
	    			<td scope="col" nowrap><?= $item->schedule_out ?></td>
	    			<td scope="col" nowrap><?= $item->no_hours ?></td>
	    		</tr>
	    	<?php endforeach; } ?>
	    </tbody>
	            
    </table>
</div>
