<div class="row">
	<div class="col-md-1">
		<div class="form-group" >
			<label>Clear Filter</label><br>
		        <button type="button" class="btn btn-sm btn-primary" id="clearFilters-schedule" >
		            <i class="fa fa-filter" ></i>Filter </button>
		                   
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label for="employee">Filter Employee</label><br>
				<select class="form-control" id="filter_employee-schedule">
					<option value="" default></option>
						<?php foreach ($data['all_employees'] as $all_employees): ?>
					<option value="<?= $all_employees->email ?>">
			            <?= $all_employees->firstname ?> <?= $all_employees->lastname ?>
			        </option>
			            <?php endforeach; ?>
				</select>   
		</div>
	</div>
			
</div>