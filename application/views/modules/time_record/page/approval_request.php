<div class="row">
	<div class="col-md-1">
		<div class="form-group" >
			<label>Clear Filter</label><br>
		        <button type="button" class="btn btn-sm btn-primary" id = "clearFilters-approvalRequest">
		            <i class="fa fa-filter" ></i>Filter</button>
		            
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label for="employee">Filter Employee</label><br>
				<select class="form-control" id="filter_employee-approvalRequest">
					<option value="" default></option>
						<?php foreach ($data['all_employees'] as $all_employees): ?>
					<option value="<?= $all_employees->email ?>">
			            <?= $all_employees->firstname ?> <?= $all_employees->lastname ?>
			        </option>
			            <?php endforeach; ?>
				</select>   
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label>Filter Date:</label>
			<div class="input-group input-group-sm">
	            <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                    <input type="text" class="form-control float-right" id="filter_date-approvalRequest">
                    <button type="button" class="btn  btn-primary btn-sm" id="date_range-approvalRequest" data-toggle="modal">Submit</button>
	        </div>
	    </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Approval Status:</label>
            <select class="form-control" id="filter_approval_status-approvalRequest">
                <option value="Pending">Pending</option>
                <option value="Approved">Approved</option>
                <option value="Denied">Denied</option>
            </select>
        </div>	
    </div>
</div>