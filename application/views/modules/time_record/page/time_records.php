<div class="row gx-5">
	<div class="col-md-1">
		<div class="form-group" style="padding-top:25px;">
		        <button type="button" class="btn btn-sm btn-primary" id="clearFilters">
		                        <i class="fa fa-filter" ></i>Clear</button>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label for="employee">Filter Employee</label><br>
				<select class="form-control" id="filter_employee">
					<option value="" default></option>
						<?php foreach ($data['all_employees'] as $all_employees): ?>
					<option value="<?= $all_employees->email ?>">
			            <?= $all_employees->firstname ?> <?= $all_employees->lastname ?>
			        </option>
			            <?php endforeach; ?>
				</select>   
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<label>Filter Date:</label>
			<div class="input-group input-group-sm">
	            <div class="input-group-prepend">
                     <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control float-right" id="filter_date">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" id="date_range">Submit</button>
		    </div>
	    </div>
    </div>

</div>