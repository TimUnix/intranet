<div class="row">
    <span class="my-auto">
        <button class="btn btn-primary btn-clear-manual-approval-filters" type="button">
            <i class="fa fa-times mr-1" aria-hidden="true"></i>
            Clear Filters
        </button>
    </span>
    <div class="col-3">
        <div class="form-group">
            <label for="manualRequestEmployeeFilter">Filter Employee</label>
            <select class="custom-select manual-request-employee-filter" id="manualRequestEmployeeFilter">
                <option value=""></option>
                <?php foreach ($data['all_employees'] as $employee): ?>
                    <option value="<?= $employee->email ?>">
                        <?= $employee->firstname ?> <?= $employee->lastname ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="manualRequestDateFilter">Filter Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-calendar-alt" aria-hidden="true"></i></span>
                </div>
                <input type="date" class="form-control manual-request-date-filter" id="manualRequestDateFilter">
            </div>
        </div>
    </div>
</div>
