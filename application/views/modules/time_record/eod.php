<div class="modal fade" id="eod" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>End of the Day</h4></strong>
            </div>
            <div class="modal-body">
                <div id="validate_eod"></div>
                <div class="form-group">
                    <label for="finished_task">Finished Tasks</label><textarea class="form-control" id="finished_task" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="challenges">Challenges</label><textarea class="form-control" id="challenges" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="tom_goal">Tomorrow's Objective/Goal</label><textarea class="form-control" id="tom_goal" rows="3"></textarea>
                </div>               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="eod_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>