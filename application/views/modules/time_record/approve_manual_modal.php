<div class="modal fade" tabindex="-1" id="approveManualModal" role="dialog" aria-labelledby="#approveManualModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="approveManualModalLabel">Approval</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="approveManualNotes">Notes</label>
                    <textarea class="form-control approve-manual-notes" cols="30" id="approveManualNotes"
                              rows="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-approve-manual-submit" type="button" data-dismiss="modal">
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>
