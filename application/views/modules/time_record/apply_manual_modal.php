<div class="modal fade" tabindex="-1" id="applyManualModal" role="dialog" aria-labelledby="#applyManualModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="applyManualModalLabel">Apply Manual</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php base_url('/time_record/manual_request') ?>" type="POST" id="manualRequestForm">
                    <div class="form-group row">
                        <label class="col col-form-label" for="manualRequestManager">Manager</label>
                        <div class="col">
                            <select class="custom-select" id="manualRequestManager" name="manager">
                                <option value=""></option>
                                <?php foreach ($data['managers'] as $manager): ?>
                                    <option value="<?= $manager->email ?>">
                                        <?= $manager->firstname ?> <?= $manager->lastname?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col col-form-label" for="manualRequestType">Request Type</label>
                        <div class="col">
                            <select class="custom-select" id="manualRequestType" name="type">
                                <option value="time_in">Time In</option>
                                <option value="time_out">Time Out</option>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="apply-manual-timer w-100 text-right text-danger">
                    <?php
                    $date = new DateTime('now', new DateTimeZone('America/Los_Angeles'));
                    ?>
                    <?= $date->format('l, F j, Y, g:i:s A T') ?>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" form="manualRequestForm">Submit</button>
            </div>
        </div>
    </div>
</div>
