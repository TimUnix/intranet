<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td scope="col" class="edit_show">
                <?php if($item->time_request == 'Pending' && $item->status !== 'Manual') { ?>
                <button type="button" class="btn-sm btn-primary draftid-cancel statusbtn">
                <i class="fa fa-edit statuseditpbtn" aria-hidden="true"> Edit</i></button>
                <?php } ?>
        </td>
        <td scope="col" class="employee item-employee<?= isset($columns->employee->is_shown) 
                        ? ($columns->employee->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->employee ?>">   
                        <?= $item->employee ?></td>
        <td style = "display: none;" scope="col" class="id item-id<?= isset($columns->id->is_shown) 
                        ? ($columns->id->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->id ?>"><?= $item->id ?></td>
        <td style = "display: none;" scope="col" class="leave_day_type item-leave_day_type<?= isset($columns->leave_day_type->is_shown) 
                        ? ($columns->leave_day_type->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->leave_day_type ?>"><?= $item->leave_day_type ?></td>
        <td style = "display: none;" scope="col" class="start_leave_date item-start_leave_date<?= isset($columns->start_leave_date->is_shown) 
                        ? ($columns->start_leave_date->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->start_leave_date ?>"><?= $item->start_leave_date ?></td>
        <td style = "display: none;" scope="col" class="end_leave_date item-end_leave_date<?= isset($columns->end_leave_date->is_shown) 
                        ? ($columns->end_leave_date->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->end_leave_date ?>"><?= $item->end_leave_date ?></td>
        <td style = "display: none;" scope="col" class="time_of_day item-time_of_day<?= isset($columns->time_of_day->is_shown) 
                        ? ($columns->time_of_day->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_of_day ?>"><?= $item->time_of_day ?></td>
        <td style = "display: none;" scope="col" class="leave_type item-leave_type<?= isset($columns->leave_type->is_shown) 
                        ? ($columns->leave_type->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->leave_type ?>"><?= $item->leave_type ?></td>
        <td style = "display: none;" scope="col" class="leave_reason item-leave_reason<?= isset($columns->leave_reason->is_shown) 
                        ? ($columns->leave_reason->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->leave_reason ?>"><?= $item->leave_reason ?></td>
        <td style = "display: none;" scope="col" class="email item-email<?= isset($columns->email->is_shown) 
                        ? ($columns->email->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->email ?>"><?= $item->email ?></td>
         <td scope="col" class="status item-status-type<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->status ?>">   
                        <?= $item->status ?></td>
        <td scope="col" class="request_date item-request_date<?= isset($columns->request_date->is_shown) 
                        ? ($columns->request_date->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->request_date ?>">
                        <?= $item->request_date ?></td>
        <?php if($item->time_request == 'Approved'){ ?> 
                <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_request ?>" style="color: green;">
                        <strong><?= $item->time_request ?></strong></td>
        <?php } else if($item->time_request == 'Denied'){ ?>
                <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_request ?>" style="color: red;">
                        <?= $item->time_request ?></td>
        <?php }else { ?>
        <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_request ?>" style="color:#FD5602">
                        <?= $item->time_request ?></td>
        <?php } ?>
        <td scope="col" class="manager item-manager<?= isset($columns->manager->is_shown) 
                        ? ($columns->manager->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->firstname ?> <?= $item->lastname ?>">
                        <?= $item->firstname ?> <?= $item->lastname ?></td>
        <td scope="col" class="manager_note item-manager_note<?= isset($columns->manager_note->is_shown) 
                        ? ($columns->manager_note->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->manager_note ?>">
                        <?= $item->manager_note ?></td>
    </tr>
<?php endforeach; } ?>
