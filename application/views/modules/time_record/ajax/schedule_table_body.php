<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td scope="col" class="employee item-employee<?= isset($columns->employee->is_shown) 
                        ? ($columns->employee->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->firstname ?> <?= $item->lastname ?>"><?= $item->firstname ?> <?= $item->lastname ?></td>
        <td style = "display: none;" scope="col" class="email item-email<?= isset($columns->email->is_shown) 
                        ? ($columns->email->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->email ?>"><?= $item->email ?></td>
        <td scope="col" class="schedule_in item-schedule_in<?= isset($columns->schedule_in->is_shown) 
                        ? ($columns->schedule_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->schedule_in ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel schedinbtn">
                <i class="fa fa-edit schedinpenbtn" aria-hidden="true"></i></button>
                        <?= $item->schedule_in ?></td>
        <td scope="col" class="schedule_out item-schedule_out<?= isset($columns->schedule_out->is_shown) 
                        ? ($columns->schedule_out->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->schedule_out ?>">
                <button type="button" class="btn btn-link text-primary draftid-cancel schedoutbtn">
                <i class="fa fa-edit schedoutpenbtn" aria-hidden="true"></i></button>
                        <?= $item->schedule_out ?></td>
    </tr>
<?php endforeach; } ?>