<?php if(!empty($items)) { foreach($items as $row_index => $item): ?>
        <?php $index = $items_offset + $row_index; ?>
 <tr> 
        <td scope="col"><?= $index + 1 ?></td>

        <td scope="col" class="employee item-employee<?= isset($columns->employee->is_shown) 
                        ? ($columns->employee->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->employee ?>"><?= $item->employee ?></td>
                        
        <td scope="col" style="overflow:hidden;" id="finished_task" class="finished_task item-finished-task<?= isset($columns->finished_task->is_shown) 
                        ? ($columns->finished_task->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->finished_task ?>"><?= $item->finished_task ?>
                        <button type="button" id="expandft" class="btn btn-link text-primary draftid-cancel expandbtn" data-toggle="collapse" data-target="#collapserow<?= $index + 1 ?>" style="float:right;">
                        <i class="fa fa-angle-down expandbtn" aria-hidden="true"></i></button>
                    </td>
        <td scope="col" style="overflow:hidden;" id="challenges" class="collapse show challenges item-challenges<?= isset($columns->challenges->is_shown) 
                        ? ($columns->challenges->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->challenges ?>"><?= $item->challenges ?>
                        <button type="button" id=""expandch class="btn btn-link text-primary draftid-cancel expandbtn" data-toggle="collapse" data-target="#collapserow<?= $index + 1 ?>" style="float:right;">
                <i class="fa fa-angle-down expandbtn" aria-hidden="true"></i></button>
                        </td>      
        <td scope="col" style="overflow:hidden;" id="tom_goal" class="tom_goal item-tom_goal<?= isset($columns->tom_goal->is_shown) 
                        ? ($columns->tom_goal->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->tom_goal ?>"><?= $item->tom_goal ?>
                        <button type="button" id="expandtg" class="btn btn-link text-primary draftid-cancel expandbtn" data-toggle="collapse" data-target="#collapserow<?= $index + 1 ?>" style="float:right;">
                <i class="fa fa-angle-down expandbtn" aria-hidden="true"></i></button>
                        </td>
        <td scope="col" class="date_in item-date_in<?= isset($columns->date_in->is_shown) 
                        ? ($columns->date_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->date_in ?>"><?= $item->date_in ?></td>
    </tr>

    <tr class="collapse" id="collapserow<?= $index + 1 ?>">
            <td></td>
            <td></td>
            <td><div class="card card-body" id="fts"<?= isset($columns->finished_task->is_shown) 
                        ? ($columns->finished_task->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->finished_task ?>"><?= $item->finished_task ?></div></td>
            <td><div class="card card-body" id="cha"<?= isset($columns->challenges->is_shown) 
                        ? ($columns->challenges->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->challenges ?>"><?= $item->challenges ?></div></td>
            <td><div class="card card-body" id="tgo"<?= isset($columns->tom_goal->is_shown) 
                        ? ($columns->tom_goal->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->tom_goal ?>"><?= $item->tom_goal ?></div></td>
            <td></td>
            
        </tr>
    
<?php endforeach; } ?>