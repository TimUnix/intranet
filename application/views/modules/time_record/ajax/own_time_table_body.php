<?php if(!empty($items)) { foreach($items as $index => $item): ?>
 <tr> 
        <td scope="col"><?= $index + 1 ?></td>
        <td scope="col" class="employee item-employee<?= isset($columns->employee->is_shown) 
                        ? ($columns->employee->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->employee ?>"><?= $item->employee ?></td>
        <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->status ?>"><?= $item->status ?></td>

        <td scope="col" class="schedule_in item-schedule_in<?= isset($columns->schedule_in->is_shown) 
                        ? ($columns->schedule_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->schedule_in ?>"><?= $item->schedule_in ?></td>      
        <td scope="col" class="schedule_out item-schedule_out<?= isset($columns->schedule_out->is_shown) 
                        ? ($columns->schedule_out->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->schedule_out ?>"><?= $item->schedule_out ?></td>
                        
        <!-- timein -->
        <?php 
        $schedule_login = strtotime($item->schedule_in);
        $time_login = strtotime($item->time_in);

        if($schedule_login < $time_login){
            if ($item->schedule_in == 'Manual'|| $item->schedule_in == 'Flexible'){?>
            <td scope="col" class="time_in item-time_in<?= isset($columns->time_in->is_shown) 
                        ? ($columns->time_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_in ?>"><?= $item->time_in ?></td>
            <?php }
            else{ ?>
                <td scope="col" class="time_in item-time_in<?= isset($columns->time_in->is_shown) 
                        ? ($columns->time_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_in ?>" style = "color: red"><?= $item->time_in ?></td>
                <?php } ?>
        <?php }else{ ?>
        <td scope="col" class="time_in item-time_in<?= isset($columns->time_in->is_shown) 
                        ? ($columns->time_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_in ?>"><?= $item->time_in ?></td>
        <?php } ?>
        <!-- endoftimein -->

        <td scope="col" class="time_out item-time_out<?= isset($columns->time_out->is_shown) 
                        ? ($columns->time_out->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->time_out ?>"><?= $item->time_out ?></td>

        <!-- nohours -->
        <?php 
         if($item->no_hours < 9){  ?>
        <td scope="col" class="no_hours item-no_hours<?= isset($columns->no_hours->is_shown) 
                        ? ($columns->no_hours->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->no_hours ?>" style = "color: red"><?= $item->no_hours ?></td>
         <?php }else{ ?>
        <td scope="col" class="no_hours item-no_hours<?= isset($columns->no_hours->is_shown) 
                        ? ($columns->no_hours->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->no_hours ?>" ><?= $item->no_hours ?></td><?php } ?>
        <!-- endnohours -->
        <td scope="col" class="date_in item-date_in<?= isset($columns->date_in->is_shown) 
                        ? ($columns->date_in->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->date_in ?>"><?= $item->date_in ?></td>
    </tr>
<?php endforeach; } ?>