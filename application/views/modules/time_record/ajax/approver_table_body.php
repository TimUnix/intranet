<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td scope="col" class = "approved_denied">
                <?php if($item->time_request == 'Pending') { ?>
                <button type="button" class="btn-sm  btn-success draftid-approved">
                <i class="fa fa-check approvedbtn" aria-hidden="true"> Approved</i></button>
                <button type="button" class="btn-sm  btn-danger draftid-denied ">
                <i class="fa fa-times deniedbtn" aria-hidden="true"> Denied</i></button>
                <?php } ?>
        </td>
        <td scope="col" class="employee item-employee<?= isset($columns->employee->is_shown) 
                        ? ($columns->employee->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->employee ?>">
               <strong>  <?= $item->employee ?></strong></td>
        <td style = "display: none;" scope="col" class="email item-email<?= isset($columns->email->is_shown) 
                        ? ($columns->email->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->email ?>"><?= $item->email ?></td>
        <td style = "display: none;" scope="col" class="start_date item-start_date<?= isset($columns->start_date->is_shown) 
                        ? ($columns->start_date->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->start_leave_date ?>"><?= $item->start_leave_date ?></td>
        <td style = "display: none;" scope="col" class="end_date item-end_date<?= isset($columns->end_date->is_shown) 
                        ? ($columns->end_date->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->end_leave_date ?>"><?= $item->end_leave_date ?></td>
           <td style = "display: none;" scope="col" class="id item-id<?= isset($columns->id->is_shown) 
                        ? ($columns->id->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->id ?>"><?= $item->id ?></td>
        <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->status ?>">   
                        <?= $item->status ?></td>   
        <td scope="col" class="request_date item-request_date<?= isset($columns->request_date->is_shown) 
                        ? ($columns->request_date->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->request_date ?>">
                        <?= $item->request_date ?></td>
        <td scope="col"><?= $item->request_date_leave ?></td>
        <td scope="col"><?= $item->approved_date ?></td>
        <?php if($item->time_request == 'Approved'){ ?> 
                <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->status ?>" style="color: green;">
                        <strong><?= $item->time_request ?></strong></td>
        <?php } else if($item->time_request == 'Denied'){ ?>
                <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->status ?>" style="color: red;">
                        <?= $item->time_request ?></td>
        <?php }else { ?>
        <td scope="col" class="status item-status<?= isset($columns->status->is_shown) 
                        ? ($columns->status->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->status ?>" style="color:#FD5602">
                        <?= $item->time_request ?></td>
        <?php } ?>
        <td scope="col"><?= $item->leave_day_type ?></td>
        <td scope="col"><?= $item->leave_type ?></td>
        <td scope="col"><?= $item->leave_reason ?></td>
        <td scope="col"><?= $item->start_leave_date ?></td>
        <td scope="col"><?= $item->end_leave_date ?></td>
        <td scope="col" class="notes item-notes<?= isset($columns->notes->is_shown) 
                        ? ($columns->notes->is_shown ? '' : ' d-none') : '' ?>" nowrap data-value="<?= $item->manager_note ?>">
                        <?= $item->manager_note ?></td>
    </tr>
<?php endforeach; } ?>