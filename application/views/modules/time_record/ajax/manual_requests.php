<?php foreach ($requests as $request): ?>
    <tr>
        <td style="width: 0;" nowrap>
            <button type="button" class="btn btn-success btn-approve-manual" data-toggle="modal"
                    data-target="#approveManualModal">
                <i class="fa fa-check" aria-hidden="true"></i>
                Approve
            </button>
            <button type="button" class="btn btn-danger btn-deny-manual mr-0" data-toggle="modal"
                    data-target="#approveManualModal">
                <i class="fa fa-times" aria-hidden="true"></i>
                Deny
            </button>
        </td>
        <td class="manual-employee" data-email="<?= $request->email?>"><?= $request->employee ?></td>
        <td class="manual-date-in" data-date-in="<?= $request->date_in ?>" data-time-in="<?= $request->time_in ?>">
            <?= $request->time_in ?> <?= date('M d, Y', strtotime($request->date_in)) ?>
        </td>
        <td class="manual-date-out" data-date-out="<?= $request->date_out ?>"
            data-time-out="<?= $request->time_out ?>">
            <?= $request->time_out ?> <?= date('M d Y', strtotime($request->date_out)) ?>
        </td>
        <td><?= $request->no_hours ?></td>
    </tr>
<?php endforeach ?>
