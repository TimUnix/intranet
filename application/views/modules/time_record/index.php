<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/time_record/eod'); ?>
<?php $this->load->view('modules/time_record/logs_for_employees'); ?>
<?php $this->load->view('modules/time_record/edit_schedule_employees'); ?>
<?php $this->load->view('modules/time_record/request_employee.php'); ?>
<?php $this->load->view('modules/time_record/modal_pop_up'); ?>
<?php $this->load->view('modules/time_record/apply_manual_modal.php'); ?>
<?php $this->load->view('modules/time_record/approve_manual_modal.php'); ?>

<link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet'>

<style>
 .apply-manual-timer {
     font-size: 1.5rem;
 }
</style>


<div class="container-fluid" style="padding-bottom: 30px;">

	<?php foreach ($data['filter_menu'] as $filter_menu): ?>
	<?php if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '0' && $filter_menu->is_eod == '0' && $filter_menu->is_sched == '1'){ ?>
		<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="time_sched_button" data-toggle="pill" href="#time_sched_button_tab" role="tab" aria-controls="time_sched_button_tab" aria-selected="false">Employees Time Schedule</a>
                </li>
            </ul>
        </div>
		</div>
	<?php }else if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '1' && $filter_menu->is_eod == '0' && $filter_menu->is_sched == '0'){ ?>
	<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="approval_request" data-toggle="pill" href="#approval_request_tab" role="tab" aria-controls="approval_request_tab" aria-selected="false">Approval Request</a>
                </li>
               <!-- <li class="nav-item">
                    <a class="nav-link btn-manual-approval" id="btnManualApproval" href="#manualApprovalTab"
                       data-toggle="pill" role="tab" aria-selected="false" aria-controls="manualApprovalTab">
                        Manual Approval
                    </a>
				</li> -->
            </ul>
        </div>
	</div>
	<?php }else if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '1' && $filter_menu->is_eod == '0' && $filter_menu->is_sched == '1'){ ?>
		<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="approval_request" data-toggle="pill" href="#approval_request_tab" role="tab" aria-controls="approval_request_tab" aria-selected="false">Approval Request</a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" id="time_sched_button" data-toggle="pill" href="#time_sched_button_tab" role="tab" aria-controls="time_sched_button_tab" aria-selected="false">Employees Time Schedule</a>
                </li>
            </ul>
        </div>
	</div>
	<?php }else if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '1' && $filter_menu->is_eod == '1' && $filter_menu->is_sched == '0'){ ?>
		<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" id="admin_button" data-toggle="pill" href="#admin_button_tab" role="tab" aria-controls="admin_button_tab" aria-selected="false">Employees Time Record</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="approval_request" data-toggle="pill" href="#approval_request_tab" role="tab" aria-controls="approval_request_tab" aria-selected="false">Approval Request</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link btn-manual-approval" id="btnManualApproval" href="#manualApprovalTab"
                       data-toggle="pill" role="tab" aria-selected="false" aria-controls="manualApprovalTab">
                        Manual Approval
                    </a>
				</li> -->
				<li class="nav-item">
                    <a class="nav-link" id="eod_records_button" data-toggle="pill" href="#eod_tab" role="tab" aria-controls="#eod_records_tab" aria-selected="false">End of the Day Records</a>
                </li>
            </ul>
        </div>
	</div>
	<?php }else if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '0' && $filter_menu->is_eod == '0' &&  $filter_menu->is_sched == '0'){ ?>
	<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
            </ul>
        </div>
	</div>
	<?php }else if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '0' && $filter_menu->is_eod == '1' &&  $filter_menu->is_sched == '0'){ ?>
		<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" id="admin_button" data-toggle="pill" href="#admin_button_tab" role="tab" aria-controls="admin_button_tab" aria-selected="false">Employees Time Record</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="eod_records_button" data-toggle="pill" href="#eod_tab" role="tab" aria-controls="#eod_records_tab" aria-selected="false">End of the Day Records</a>
                </li>
            </ul>
        </div>
	</div>
	<?php }else if($filter_menu->is_admin == '0' && $filter_menu->is_manager == '0' && $filter_menu->is_eod == '1' &&  $filter_menu->is_sched == '1'){ ?>
		<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" id="admin_button" data-toggle="pill" href="#admin_button_tab" role="tab" aria-controls="admin_button_tab" aria-selected="false">Employees Time Record</a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" id="time_sched_button" data-toggle="pill" href="#time_sched_button_tab" role="tab" aria-controls="time_sched_button_tab" aria-selected="false">Employees Time Schedule</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="eod_records_button" data-toggle="pill" href="#eod_tab" role="tab" aria-controls="#eod_records_tab" aria-selected="false">End of the Day Records</a>
                </li>
            </ul>
        </div>
	</div>
	<?php }else { ?>
	<div class="card card-primary card-tabs">
		<div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                 	<a class="nav-link active" id="my_record" data-toggle="pill" href="#my-time-record" role="tab" aria-controls="my-time-record" aria-selected="true">My Time Record</a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" id="my_request" data-toggle="pill" href="#my_request_tab" role="tab" aria-controls="my_request_tab" aria-selected="false">My Time Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="admin_button" data-toggle="pill" href="#admin_button_tab" role="tab" aria-controls="admin_button_tab" aria-selected="false">Employees Time Record</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="time_sched_button" data-toggle="pill" href="#time_sched_button_tab" role="tab" aria-controls="time_sched_button_tab" aria-selected="false">Employees Time Schedule</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="approval_request" data-toggle="pill" href="#approval_request_tab" role="tab" aria-controls="approval_request_tab" aria-selected="false">Approval Request</a>
                </li>
               <!-- <li class="nav-item">
                    <a class="nav-link btn-manual-approval" id="btnManualApproval" href="#manualApprovalTab"
                       data-toggle="pill" role="tab" aria-selected="false" aria-controls="manualApprovalTab">
                        Manual Approval
                    </a>
				</li> -->
				<li class="nav-item">
                    <a class="nav-link" id="eod_records_button" data-toggle="pill" href="#eod_tab" role="tab" aria-controls="#eod_records_tab" aria-selected="false">End of the Day Records</a>
                </li>
            </ul>
        </div>
	</div>	
<?php } ?>
	<?php endforeach; ?>
        <div class="card card-body">
            <div class="tab-content">
            	<div class="tab-pane fade show active" id="my-time-record" role="tabpanel" aria-labelledby="my_record" style="text-align: center;">
					<h2 id = "day_now" style="font-family: 'Orbitron'; letter-spacing: 7px; font-size: 30px"></h2><br>
						<strong><h4 id="date_now" style="font-family:'Orbitron'; letter-spacing: 7px; font-size: 30px; color: #FF0000"></h4></strong>
						<button class="btn btn-lg btn-primary" data-toggle="modal" id="time_in">
						        TIME IN</button>
						<button class="btn btn-lg btn-primary" data-toggle="modal" id="time_out">
						        TIME OUT</button></div>
					<div >
						<?php foreach ($data['filter_menu'] as $filter_menu): ?>
						<span id="fullname"><?= $filter_menu->firstname ?><?= $filter_menu->lastname ?></span>
						<?php endforeach; ?>
				</div>
				<div class="tab-pane fade" id="my_request_tab" role="tabpanel" aria-labelledby="my_request">
					<?php $this->load->view('modules/time_record/page/my_time_request.php'); ?>
				</div>
				<div class="tab-pane fade" id="admin_button_tab" role="tabpanel" aria-labelledby="admin_button">
					<?php $this->load->view('modules/time_record/page/time_records.php'); ?>
				</div>
				<div class="tab-pane fade" id="time_sched_button_tab" role="tabpanel" aria-labelledby="time_sched_button">
					<?php $this->load->view('modules/time_record/page/time_schedule.php'); ?>
				</div>
				<div class="tab-pane fade" id="approval_request_tab" role="tabpanel" aria-labelledby="approval_request">
					<?php $this->load->view('modules/time_record/page/approval_request.php'); ?>
				</div>
                <div class="tab-pane fade" role="tabpanel" aria-labelledby="btnManualApproval" id="manualApprovalTab">
                    <?php $this->load->view('modules/time_record/page/manual_approval'); ?>
                </div>
				<div class="tab-pane fade" id="eod_tab" role="tabpanel" aria-labelledby="eod">
					<?php $this->load->view('modules/time_record/page/eod_records.php'); ?>
				</div>
			</div>
		</div>
	<div class="card" id="own_time_table">
		<div class="card-header">
		<form action="<?= base_url() ?>index.php/time_record/generate_own_PDF" method="POST" id="ownPDFform">
			<div class="row justify-content-end">
			<div class="col-md-1">
					<div class="card-tools">
					 	<button type="button" class="btn btn-sm btn-primary ret"
		                        id="clearFilterspdf">
		                    <i class="fa fa-filter" aria-hidden="true"></i>
		                    Clear Filter
		                </button>
		            </div>
		        </div>
				<div class="col-md-3">
					<div class="form-group">
						<div class="input-group input-group-sm">
				            <div class="input-group-prepend">
			                     <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
			                </div>
			                <input type="text" class="form-control float-right" id="own_filter_date" name="own_filter_date">
			                    <button type="button" class="btn  btn-sm btn-primary" data-toggle="modal" id="own_date_range">Submit</button>
					    </div>
				    </div>
				</div>
		        <div class="col-md-1">
					<div class="card-tools">
					 	<button type="submit" class="btn btn-sm btn-outline-primary ret"
		                        id="generateownPDF">
		                    <i class="fa fa-download" aria-hidden="true"></i>
		                    Export PDF
		                </button>
		            </div>
		        </div>
	        </div></form>
		</div>
		<div class="horizontal-scroller">
                <div class="scroller-content"></div>
        </div>
		<div class="card-body sticky-headers p-0 d-none" style="z-index: 2;" id= "owntable">
			<table class="table table-bordered table-striped table-hover sticky-table"
                    style="border-collapse: separate;" cellspacing="0">
            	<thead class="bsticky-thead bg-white">
	            	<tr class="sticky-header-row">
					<th scope="col">&#35;</th>
	            		<th id="ownemp" scope="col" class="header-employee-own<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Employee</th>
	            		<th scope="col" class="header-status-own<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Status</th>
                         <th scope="col" class="header-schedule_in-own<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule In</th>
                        <th scope="col" class="header-schedule_out-own<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule Out</th>
	            		<th scope="col" class="header-time_in-own<?= isset($data['columns']->time_in->is_shown) ? ($data['columns']->time_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time In</th>      
                        <th scope="col" class="header-time_out-own<?= isset($data['columns']->time_out->is_shown) ? ($data['columns']->time_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time Out</th>
                        
                        <th scope="col" class="header-no_hours-own<?= isset($data['columns']->no_hours->is_shown) ? ($data['columns']->no_hours->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>No. of Hours</th>
	            		<th scope="col" class="header-date_in-own<?= isset($data['columns']->date_in->is_shown) ? ($data['columns']->date_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Date</th>                                                                                       
	            	</tr>
	            </thead>
				</table>
		</div>
		<div class="card-body p-0 table-container table-responsive">
			<table class="table table-bordered table-striped table-hover main-table" id="own-time-table"
                   style="border-collapse: separate;" cellspacing="0">
            	<thead class="main-thead bg-white border">
	            	<tr class="main-header-row">
	            		<th scope="col">&#35;</th>
	            		<th scope="col" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Employee</th>
	            		<th scope="col" class="header-status<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Status</th>
                         <th scope="col" class="header-schedule_in<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule In</th>
                        <th scope="col" class="header-schedule_out<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule Out</th>
	            		<th scope="col" class="header-time_in<?= isset($data['columns']->time_in->is_shown) ? ($data['columns']->time_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time In</th>      
                        <th scope="col" class="header-time_out<?= isset($data['columns']->time_out->is_shown) ? ($data['columns']->time_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time Out</th>
                        
                        <th scope="col" class="header-no_hours<?= isset($data['columns']->no_hours->is_shown) ? ($data['columns']->no_hours->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>No. of Hours</th>
	            		<th scope="col" class="header-date_in<?= isset($data['columns']->date_in->is_shown) ? ($data['columns']->date_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Date</th>                                                                  
	            	</tr>
	            </thead>
	            <tbody id="own_time_table_body">
                    <tr>
                    	 <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
	<div class="card" id="request_table">
		<div class="card-header">
		</div>
		<div class="card-body p-0 inventory-container table-responsive table-scroll">
			<table class="table table-bordered table-striped table-hover w-100" id="request-table"
	                   style="border-collapse: separate;" cellspacing="0">
	            <thead class="bg-white position-sticky border" style="top: 0;">
		            <tr>
		            	<th scope="col">&#35;</th>
                        <th scope="col"></th>
		            	<th scope="col" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Employee</th>
                        <th scope="col" class="header-status<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
                                                           : '' ?>"nowrap>Type</th>
		            	<th scope="col" class="header-request_date<?= isset($data['columns']->request_date->is_shown) ? ($data['columns']->request_date->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Request Date</th>
	                    <th scope="col" class="header-status<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Status</th>
	                    <th scope="col" class="header-manager<?= isset($data['columns']->manager->is_shown) ? ($data['columns']->manager->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Manager</th> 
                        <th scope="col" class="header-manager_note<?= isset($data['columns']->manager_note->is_shown) ? ($data['columns']->manager_note->is_shown ? '' : ' d-none') 
                                                           : '' ?>"nowrap>Manager Note&emsp;&emsp;&emsp;</th>                                                                  
		            </tr>
		        </thead>
		        <tbody id="request_table_body">
	                <tr>
	                    <td colspan="100%">Search an item to see results here.</td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>

	<div class="card" id="time_table">
		<div class="card-header">
			<?php foreach ($data['filter_menu'] as $filter_menu): ?>
			<?php if($filter_menu->is_eod == '1' && $filter_menu->is_admin == '0'){ ?>
			<div class="card-tools">
				<form action="<?= base_url() ?>index.php/time_record/generate_PDF" method="POST" id="PDFform">
				<input type='hidden' name='filter_employee' id="pdfFilter" required/>
				<input type='hidden' name='filter_date' id="pdfFilterdate" required/>
				<button type="submit" class="btn btn-sm btn-outline-primary ret"
                        id="generatePDF" name="generatePDF" on-click="generate_PDF()">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Generate Time Sheet
                </button></form>
            </div>
        	<?php } else{ ?> 
			<div class="card-tools">
				<div class ="row">
				<div>
				<button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="logs_for_employees">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    Add Logs
            	</button>
				</div>
				<!-- <div style="padding-left:5px;">
			 	<button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
				</div> -->
				<div style="padding-left:5px;">
				<form action="<?= base_url() ?>index.php/time_record/generate_PDF" method="POST" id="PDFform">
				<input type='hidden' name='filter_employee' id="pdfFilter" required/>
				<input type='hidden' name='filter_date' id="pdfFilterdate" required/>
				<button type="submit" class="btn btn-sm btn-outline-primary ret"
                        id="generatePDF" name="generatePDF" on-click="generate_PDF()">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Generate Time Sheet
                </button></form></div>
			</div>
            </div>
        <?php } ?> 
        <?php endforeach; ?>
		</div>
		<div class="horizontal-scroller">
                <div class="scroller-content"></div>
        </div>
		<div class="card-body sticky-headers p-0 d-none" style="z-index: 2;" id="timetable">
			<table class="table table-bordered table-striped table-hover sticky-table"
                    style="border-collapse: separate;" cellspacing="0">
					<colgroup>
						<col style="width:5.25%">
						<col style="width:23%">

						<col style="width:7.75%">
						<col style="width:13%">
						<col style="width:13%">

						<col style="width:9.25%">
						<col style="width:9.25%">	
						<col style="width:9.75%">
						<col style="width:9.75%">
					</colgroup>
            	<thead class="bsticky-thead bg-white">
	            	<tr class="sticky-header-row">
					<th scope="col">&#35;</th>
	            		<th scope="col" class="header-employee-time<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Employee</th>
	            		<th scope="col" class="header-status-time<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Status</th>
                         <th scope="col" class="header-schedule_in-time<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule In</th>
                        <th scope="col" class="header-schedule_out-time<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule Out</th>
	            		<th scope="col" class="header-time_in-time<?= isset($data['columns']->time_in->is_shown) ? ($data['columns']->time_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time In</th>      
                        <th scope="col" class="header-time_out-time<?= isset($data['columns']->time_out->is_shown) ? ($data['columns']->time_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time Out</th>
                        
                        <th scope="col" class="header-no_hours-time<?= isset($data['columns']->no_hours->is_shown) ? ($data['columns']->no_hours->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>No. of Hours</th>
	            		<th scope="col" class="header-date_in-time<?= isset($data['columns']->date_in->is_shown) ? ($data['columns']->date_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Date</th>                                                                            
	            	</tr>
	            </thead>
				</table>
		</div>
		<div class="card-body p-0 table-container table-responsive">
			<table class="table table-bordered table-striped table-hover main-table" id="time-table"
                   style="border-collapse: separate;" cellspacing="0">
            	<thead class="main-thead bg-white border">
	            	<tr class="main-header-row">
	            		<th scope="col">&#35;</th>
	            		<th scope="col" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Employee</th>
	            		<th scope="col" class="header-status<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Status</th>
                         <th scope="col" class="header-schedule_in<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule In</th>
                        <th scope="col" class="header-schedule_out<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Schedule Out</th>
	            		<th scope="col" class="header-time_in<?= isset($data['columns']->time_in->is_shown) ? ($data['columns']->time_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time In</th>      
                        <th scope="col" class="header-time_out<?= isset($data['columns']->time_out->is_shown) ? ($data['columns']->time_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Time Out</th>
                        
                        <th scope="col" class="header-no_hours<?= isset($data['columns']->no_hours->is_shown) ? ($data['columns']->no_hours->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>No. of Hours</th>
	            		<th scope="col" class="header-date_in<?= isset($data['columns']->date_in->is_shown) ? ($data['columns']->date_in->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Date</th>                                                                  
	            	</tr>
	            </thead>
	            <tbody id="time_table_body">
                    <tr>
                    	 <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
	<div class="card" id="schedule_table">
		<div class="card-header">
		</div>
		<div class="card-body p-0 inventory-container table-responsive table-scroll">
			<table class="table table-bordered table-striped table-hover w-100" id="schedule-table"
	                   style="border-collapse: separate;" cellspacing="0">
	            <thead class="bg-white position-sticky border" style="top: 0;">
		            <tr>
		            	<th scope="col">&#35;</th>
		            	<th scope="col" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Employee</th>
		            	<th scope="col" class="header-schedule_in<?= isset($data['columns']->schedule_in->is_shown) ? ($data['columns']->schedule_in->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Schedule In</th>
	                    <th scope="col" class="header-schedule_out<?= isset($data['columns']->schedule_out->is_shown) ? ($data['columns']->schedule_out->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Schedule Out</th>                                                                  
		            </tr>
		        </thead>
		        <tbody id="schedule_table_body">
	                <tr>
	                    <td colspan="100%">Search an item to see results here.</td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>

	
	<div class="card" id="approver_table">
		<div class="card-header">
		</div>
		<div class="card-body p-0 inventory-container table-responsive table-scroll">
			<table class="table table-bordered table-striped table-hover" id="approver-table"
	                   style="border-collapse: separate;" cellspacing="0">
	            <thead class="bg-white position-sticky border" style="top: 0;">
		            <tr>
		            	<th scope="col" rowspan="2">&#35;</th>
                        <th scope="col" rowspan="2"></th>
		            	<th scope="col" rowspan="2" class="header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Employee</th>
                        <th scope="col" rowspan="2" class="header-status<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
                                                           : '' ?>"nowrap>Type</th>
		            	<th scope="col" rowspan="2" class="header-leave_date<?= isset($data['columns']->leave_date->is_shown) ? ($data['columns']->leave_date->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Leave Date</th>
	                    <th scope="col" rowspan="2" class="header-request_date<?= isset($data['columns']->request_date->is_shown) ? ($data['columns']->request_date->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Request Date</th>
	                    <th scope="col" rowspan="2" class="header-approved_date<?= isset($data['columns']->approved_date->is_shown) ? ($data['columns']->approved_date->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Approved/Denied Date</th>
	                    <th scope="col" rowspan="2" class="header-status<?= isset($data['columns']->status->is_shown) ? ($data['columns']->status->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Status</th> 
                        <th nowrap colspan="5" style="text-align:center;">Leave</th>
	                    <th scope="col" rowspan="2" class="header-notes<?= isset($data['columns']->notes->is_shown) ? ($data['columns']->notes->is_shown ? '' : ' d-none') 
	                                                       : '' ?>"nowrap>Notes &emsp;&emsp;&emsp;&emsp;</th>                                                                  
		            </tr>
                    <tr>
                        <th>Day Type</th>
                        <th>Type</th>
                        <th>Reason</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
		        </thead>
		        <tbody id="approver_table_body">
	                <tr>
	                    <td colspan="100%">Search an item to see results here.</td>
	                </tr>
	            </tbody>
	        </table>
	    </div>
	</div>		
    <div class="card d-none manual-approval-container">
        <div class="card-header">Manual Requests Pending Approval</div>
        <div class="card-body p-0 table-responsive">
            <table class="table table-bordered table-striped table-hover 2-100 table-manual-approval" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col">Approval</th>
                        <th scope="col">Employee</th>
                        <th scope="col">Time In</th>
                        <th scope="col">Time Out</th>
                        <th scope="col">No. of Hours</th>
                    </tr>
                </thead>
                <tbody class="table-manual-approval-body"></tbody>
            </table>
        </div>
    </div>

	<div class="card" id="eod_table">
		<div class="card-header">
		<div class="card-tools">
			<div style="padding-left:5px;">
			 	<button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSVeod">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
				</div>
				</div>
		</div>
		<div class="horizontal-scroller">
                <div class="scroller-content"></div>
        </div>
		<div class="card-body sticky-headers p-0 d-none" style="z-index: 2;">
			<table class="table table-bordered table-striped table-hover sticky-table"
                    style="border-collapse: separate;" cellspacing="0">
					<colgroup>
						<col style="width:5%">
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:10%">
					</colgroup>
            	<thead class="bsticky-thead bg-white">
	            	<tr class="sticky-header-row">
					<th scope="col">&#35;</th>
						<th scope="col" class="th.lg header-employee-eod<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Employee</th>
	            		<th scope="col" class="th.lg header-task-eod<?= isset($data['columns']->finished_task->is_shown) ? ($data['columns']->finished_task->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Tasks Completed</th>
                         <th scope="col" class="th.lg header-challenges-eod<?= isset($data['columns']->challenges->is_shown) ? ($data['columns']->challenges->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Challenges</th>
                        <th scope="col" class="th.lg header-tomgoal-eod<?= isset($data['columns']->tom_goal->is_shown) ? ($data['columns']->tom_goal->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Tomorrow's Objective</th>
	            		<th scope="col" class="th.lg header-date_in-eod<?= isset($data['columns']->date_out->is_shown) ? ($data['columns']->date_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Date</th>                                                                  
	            	</tr>
	            </thead>
			</table>
			</div>
		<div class="card-body p-0 inventory-container table-responsive-sm table-scroll">
			<table class="table table-bordered table-hover paginated w-100" id="eod-table"
                   style="border-collapse: separate; table-layout:fixed;" cellspacing="0">
				   <colgroup>
						<col style="width:5%">
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:20%">
						<col style="width:10%">
					</colgroup>
            	<thead class="bg-white position-sticky border" style="top: 0;">
	            	<tr>
	            		<th scope="col">&#35;</th>
	            		<th scope="col" class="th.lg header-employee<?= isset($data['columns']->employee->is_shown) ? ($data['columns']->employee->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Employee</th>
	            		<th scope="col" class="th.lg header-task<?= isset($data['columns']->finished_task->is_shown) ? ($data['columns']->finished_task->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Tasks Completed</th>
                         <th scope="col" class="th.lg header-challenges<?= isset($data['columns']->challenges->is_shown) ? ($data['columns']->challenges->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Challenges</th>
                        <th scope="col" class="th.lg header-tomgoal<?= isset($data['columns']->tom_goal->is_shown) ? ($data['columns']->tom_goal->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Tomorrow's Objective</th>
	            		<th scope="col" class="th.lg header-date_in<?= isset($data['columns']->date_out->is_shown) ? ($data['columns']->date_out->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Date</th>                                                                  
	            	</tr>
	            </thead>
	            <tbody id="eod_table_body">
                    <tr>
                    	 <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
</div> 
</div>


<?php $this->load->view('templates/common/spinner'); ?>

<script id="timeRecordScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/time_record/time_record.js"></script>
<script id="timeScheduleScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/time_record/time_schedule.js"></script>
<script id="timeRequestScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/time_record/time_request.js"></script>
		<script id="eodRecordsScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/time_record/eod_records.js"></script>
<script class="time-manual-script" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/time_record/timeManualRequestAndApproval.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>

<?php $this->load->view('templates/common/footer'); ?>
