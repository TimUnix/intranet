<div class="modal fade" id="edit_schedulein_form" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Edit Schedule In</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_edit_schedulein"></div>
                <div class = "row" style="display:none;">
                    <span class = "id-current-in"></span>
                </div>
                <div class="row">
                    <div class="col-md-9 custom-control custom-switch pt-3">
                        <input type="checkbox" id="flexible_in" class="custom-control-input">
                        <label for="flexible_in" class="custom-control-label">Flexible</label>
                    </div>
                </div><br>
                <div class="row">
                    <label for="edit_schedule_in" class="col-md-3">Schedule In:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="edit_schedule_in" data-target-input="nearest">
                                <input type="text" id="editschedulein_" class="form-control datetimepicker-input" placeholder="Select Time" data-target="#edit_schedule_in"/>
                                <div class="input-group-append" data-target="#edit_schedule_in" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>         
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="edit_schedulein_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_scheduleout_form" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Edit Schedule Out</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_edit_scheduleout"></div> 
                <div class = "row" style="display:none;">
                    <span class = "id-current-out"></span>
                </div>
                 <div class="row">
                    <div class="col-md-9 custom-control custom-switch pt-3">
                        <input type="checkbox" id="flexible_out" class="custom-control-input">
                        <label for="flexible_out" class="custom-control-label">Flexible</label>
                    </div>
                </div><br>
                <div class="row">
                    <label for="edit_schedule_out" class="col-md-3">Schedule Out:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="edit_schedule_out" data-target-input="nearest">
                                <input type="text" id="editscheduleout_" class="form-control datetimepicker-input" placeholder="Select Time" data-target="#edit_schedule_out"/>
                                <div class="input-group-append" data-target="#edit_schedule_out" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>        
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="edit_scheduleout_submit"> Submit </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_schedulein_form_record" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Edit Schedule In</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_edit_schedulein_record"></div>
                <div class = "row" style="display:none;">
                    <span class = "id-current-in-record"></span>
                </div>
                <div class="row">
                    <label for="edit_schedule_in_record" class="col-md-3">Schedule In:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="edit_schedule_in_record" data-target-input="nearest">
                                <input type="text" id="editscheduleinrecord_" class="form-control datetimepicker-input" placeholder="Select Time" data-target="#edit_schedule_in_record"/>
                                <div class="input-group-append" data-target="#edit_schedule_in_record" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>         
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="edit_schedulein_submit_record"> Submit </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_scheduleout_form_record" role="dialog">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
               <strong><h4>Edit Schedule Out</h4></strong> 
            </div>
            <div class="modal-body">
                <div id="validate_edit_scheduleout"></div> 
                <div class = "row" style="display:none;">
                    <span class = "id-current-out-record"></span>
                </div>
                <div class="row">
                    <label for="edit_schedule_out_record" class="col-md-3">Schedule Out:</label>
                    <div class="col-md-9">
                        <div class="input-group date" id="edit_schedule_out_record" data-target-input="nearest">
                                <input type="text" id="editscheduleoutrecord_" class="form-control datetimepicker-input" placeholder="Select Time" data-target="#edit_schedule_out_record"/>
                                <div class="input-group-append" data-target="#edit_schedule_out_record" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
                        </div>
                    </div>
                </div><br>        
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="edit_scheduleout_submit_record"> Submit </button>
            </div>
        </div>
    </div>
</div>