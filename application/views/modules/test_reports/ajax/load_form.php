<div class="container-fluid">
<div class="card card-info">
<div id="status"></div>
              <div class="card-header" style="background-color: #007bff;">
                <h3 class="card-title">Add Test Report</h3>
              </div>
              <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Part Number</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Enter Part Number" id="partnumber">
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Serial Number</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Enter Serial Number" id="serialnumber">
                </div>

			        	<div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Tech Name</span>
                  </div>
                  <?php $data =$this->session->userdata('user');?>
                  <input type="text" class="form-control" placeholder="Enter Tech Name" id="techname" value="<?php echo $data['firstname'];?> <?php echo $data['lastname'];?>">
                                     
                </div>

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Purchase Order</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Enter PO" id="purchaseorder">
                </div>

				<div class="form-group">
                        <label>Tech Report</label>
                        <textarea class="form-control" rows="3" placeholder="Enter ..." style="height:150px;"id="message"></textarea>
                      </div>
					  </div>


					  <div class="card-footer">
                  <button type="submit" class="btn btn-primary" id="create_research">Submit</button>
                </div>


				<script>
                                $(document).ready(function(){
                                    $("#create_research").click(function(){
                                        var datas = {
                                          partnumber : $("#partnumber").val(),
                                          serialnumber : $("#serialnumber").val(), 
                                          techname : $("#techname").val(),
                                          message : $("#message").val(), 
                                          po : $("#purchaseorder").val(),
                                            ajax : '1'
                                        };
                                        console.log($("#techname").val());
                                        $.ajax({
                                            url : "<?php echo base_url();   ?>index.php/add_test_report/request/add_report",
                                            type : "post",
                                            data : datas,
                                            success : function(msg){
                                                $("#status").html(msg);
                                                $("#partnumber").val('');
                                                $("#serialnumber").val('');
                                                $("#message").val('');
                                                $("#purchaseorder").val('');
                                            }
                                        });
                                    });
                                });
                                </script>

                

                

                

                

                

                

                

                
                <!-- /.row -->

                

                

                
                <!-- /input-group -->

                
                
                <!-- /input-group -->

                
                
                <!-- /input-group -->
              </div>
              <!-- /.card-body -->
            </div>