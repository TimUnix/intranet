<?php $this->load->view('templates/header_new'); ?>

<div class="container-fluid">
    <div class="card">
        <div class="card-header"><h3 class="card-title">Filters</h3></div>
	    <div class="card-body">
		    <div class="categories">
			    <div class="selected-filters mb-3" id="selected-filters"></div>
			    <select class="custom-select" name="filters" id="filter-select">
				    <option selected disabled>Choose filter(s)</option>
				    <?php foreach ($data['categories'] as $category => $subcats): ?>
					    <optgroup label="<?= $category; ?>">
						    <?php foreach ($subcats as $subcat): ?>
							    <option value="<?= $subcat['category_name']; ?>">
								    <?= $subcat['display_name']; ?>
							    </option>
						    <?php endforeach; ?>
					    </optgroup>
				    <?php endforeach; ?>
			    </select>
		    </div>
	    </div>
    </div>
    <div class="card">
        <div class="card-header"><h3 class="card-title">Items</h3></div>
        <div class="card-body table-responsive p-0">
            <div id="inventory">
                <table id="inventory-table" class="table table-bordered table-striped table-hover"
                       style="width: 100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th>&#35;</th>
                            <th>Image</th>
                            <th>Part Number</th>
                            <th>Description</th>
                            <th>Con</th>
                            <th>LWPrice</th>
                            <th>New Price</th>
                            <th>Notes</th>
                            <th>QTY</th>
                            <th>Total</th>
                            <th><img src="<?php echo base_url(); ?>resources/img/up-icon.png" width="30"></th>
                            <th><img src="<?php echo base_url(); ?>resources/img/net-icon.png" width="30"></th>
                        </tr>
                    </thead>
                    <tbody id="items"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script id="priceUpdateScript" data-base-url="<?= base_url(); ?>"
        src="<?= base_url(); ?>resources/js/priceUpdate.js"></script>
