<?php foreach ($items as $index => $item): ?>
    <tr>
        <td><?= $index + 1; ?></td>
        <td>
            <img src="<?= $item->img === '/i/noimage.png' ? base_url() . 'resources/img/noimage.png' : $item->img; ?>"
                 width="80" loading="lazy">
        </td>
        <td class="par"><?= $item->par; ?></td>
        <td><?= $item->des; ?></td>
        <td><?= $item->con; ?></td>
        <td>$<?= number_format($item->pri, 2); ?></td>
        <td class="new-price" data-value="<?= $item->new_pri ? $item->new_pri : '' ?>">
            <?php if ($item->new_pri): ?>
                $<?= number_format($item->new_pri, 2); ?>
            <?php endif; ?>
        </td>
        <td class="notes" data-value="<?= $item->notes ? $item->notes : '' ?>">
            <?= $item->notes; ?>
        </td>
        <td><?= number_format($item->qty, 2); ?></td>
        <td>$<?= number_format($item->tot, 2); ?></td>
        <td></td>
        <td></td>
    </tr>
<?php endforeach; ?>
