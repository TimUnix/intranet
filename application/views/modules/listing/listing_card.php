<div class="card">
    <div class="horizontal-scroller">
        <div class="scroller-content"></div>
    </div>
    <div class="card-body p-0 sticky-headers d-none" style="z-index: 2;">
        <table class="table table-bordered listing-table sticky-table" cellspacing="0"
               style="border-collapse: separate;">
            <thead class="bg-white border">
                <tr class="sticky-header-row">
                    <th class="header-sortable" scope="col" rowspan="2" data-column="index">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        #
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="sku">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        par/sku
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="description">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Description
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="category">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Category
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="quantity">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Quantity
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="price">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Price
                    </th>
                    <th scope="col" colspan="2">Price Research Market</th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="note">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Note
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="link">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Link
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="comp_link">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Competitors Link
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="channel">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Channel
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="done">Done</th>
                </tr>
                <tr class="sticky-subheader-row">
                    <th class="header-sortable" scope="col" data-column="price_research_market_low">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Low
                    </th>
                    <th class="header-sortable" scope="col" data-column="price_research_market_high">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        High
                    </th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="card-body p-0 table-container">
        <table class="table table-bordered table-hover table-highlight listing-table main-table"
               cellspacing="0" style="border-collapse: separate;">
            <thead class="main-thead bg-white border">
                <tr class="main-header-row">
                    <th class="header-sortable" scope="col" rowspan="2" data-column="index">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        #
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="sku">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        par/sku
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="description">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Description
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="category">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Category
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="quantity">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Quantity
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="price">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Price
                    </th>
                    <th scope="col" colspan="2">Price Research Market</th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="note">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Note
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="link">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Link
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="comp_link">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Competitors Link
                    </th>
                    <th class="header-sortable" scope="col" rowspan="2" data-column="channel">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Channel
                    </th>
                    <th scope="col" rowspan="2" data-column="done">Done</th>
                </tr>
                <tr class="main-subheader-row">
                    <th class="header-sortable" scope="col" data-column="price_research_market_low">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        Low
                    </th>
                    <th class="header-sortable" scope="col" data-column="price_research_market_high">
                        <i class="fa fa-sort-up d-none" aria-hidden="true"></i>
                        <i class="fa fa-sort-down d-none" aria-hidden="true"></i>
                        High
                    </th>
                </tr>
            </thead>
            <tbody class="product-listing-body">
                <?php $this->load->view('modules/listing/ajax/product_listing.php'); ?>
            </tbody>
        </table>
    </div>
</div>
