<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>

<style>
 .btn-edit {
     display: none;
     position: absolute;
     right: 0;
     top: 0;
 }

 .cell-edit-buttons {
     bottom: 0;
 }

 .header-sortable:hover {
     cursor: pointer;
 }

 td {
     z-index: 0;
     position: relative;
 }
 
 td:hover > .cell-content-container > .btn-edit {
     display: inline-block;
 }

 .categories-container, .channels-container {
     padding: 2px 8px;
 }
</style>

<?php $this->load->view('modules/listing/create_listing_modal'); ?>
<?php $this->load->view('modules/listing/mark_listing_done_modal'); ?>
<?php $this->load->view('modules/listing/upload_csv_modal'); ?>

<div class="container-fluid" style="padding-bottom: 90px;">
    <?php $this->load->view('modules/listing/interactive_card'); ?>
    <?php $this->load->view('modules/listing/listing_card'); ?>
</div>

<?php $this->load->view('templates/common/spinner'); ?>
<script id="listingScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/listing.js"></script>
<?php $this->load->view('templates/common/footer'); ?>

