<?php foreach ($listing as $index => $item): ?>
    <?php if ($item->is_done){ ?>
    <tr style="background-color : #90EE90 ">
 <?php   } else { ?>
    <tr id="roww" style="background-color : #00000 "> <?php } ?>
        <th class="index" scope="row"><?= $base_row_index + $index + 1 ?></th>
        <td class="sku" data-id="<?= $item->id ?>" data-column="sku" data-value="<?= $item->sku ?>"
            style="max-width: 80ch;">
            <div class="cell-content-container">
                <div class="cell-value"><?= $item->sku ?></div>
            </div>
        </td>
        <td class="description" data-value="<?= $item->description ?>" style="max-width: 80ch;">
            <div class="cell-content-container">
                <div class="cell-value"><?= $item->description ?></div>
            </div>
        </td>
        <td class="category" data-value="<?= $item->category ?>">
            <div class="cell-content-container">
                <div class="cell-value"><?= $item->category ?></div>
            </div>
        </td>
        <td class="quantity" data-value="<?= $item->quantity ?>">
            <div class="cell-content-container">
                <div class="cell-value"><?= $item->quantity ?></div>
            </div>
        </td>
        <td class="price" data-value="<?= $item->price ?>">
            <div class="cell-content-container">
                <div class="cell-value">
                    <?php if ($item->price !== ''): ?>
                        $<?= $item->price ?>
                    <?php endif; ?>
                </div>
                    
            </div>
        </td>
        <td class="price-research-market-low" data-value="<?= $item->price_research_market_low ?>">
            <?php if (!$item->is_done): ?>
                <div class="cell-edit-container d-none">
                    <input class="form-control cell-edit w-auto" type="text"
                           value="<?= $item->price_research_market_low ?>">
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if (!$item->is_done): ?>
                    <button class="btn btn-link btn-edit">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value">
                    <?php if (isset($item->price_research_market_low) && $item->price_research_market_low !== ''): ?>
                        $<?= $item->price_research_market_low ?>
                    <?php endif; ?>
                    <?php if ((isset($item->price_low_last_edited) && $item->price_low_last_edited !== '') &&
                              (isset($item->price_low_last_edited_by) && $item->price_low_last_edited_by !== '')): ?>
                                <?= $item->price_low_last_edited_by ?>
                                <?= date_format(date_create($item->price_low_last_edited), 'D, d-m-Y') ?>
                    <?php endif; ?>
                </div>
            </div>
        </td>
        <td class="price-research-market-high" data-value="<?= $item->price_research_market_high ?>">
            <?php if (!$item->is_done): ?>
                <div class="cell-edit-container d-none">
                    <input class="form-control cell-edit w-auto" type="text"
                           value="<?= $item->price_research_market_high ?>">
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if (!$item->is_done): ?>
                    <button class="btn btn-link btn-edit">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value">
                    <?php if (isset($item->price_research_market_high) && $item->price_research_market_high !== ''): ?>
                        $<?= $item->price_research_market_high ?>
                    <?php endif; ?>
                    <?php if ((isset($item->price_high_last_edited) && $item->price_high_last_edited !== '') &&
                              (isset($item->price_high_last_edited_by) && $item->price_high_last_edited_by !== '')): ?>
                                <?= $item->price_high_last_edited_by ?>
                                <?= date_format(date_create($item->price_high_last_edited), 'D, d-m-Y') ?>
                    <?php endif; ?>
                </div>
            </div>
        </td>
        <td class="note" data-value="<?= $item->note ?>" style="max-width: 80ch;">
            <?php if (!$item->is_done): ?>
                <div class="cell-edit-container d-none">
                    <textarea class="form-control cell-edit w-auto"><?= $item->note ?></textarea>
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if (!$item->is_done): ?>
                    <button class="btn btn-link btn-edit">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value">
                    <?php if (isset($item->note) && $item->note !== ''): ?>
                        <?= $item->note ?>
                    <?php endif; ?>
                    <?php if ((isset($item->note_last_edited) && $item->note_last_edited !== '') &&
                              (isset($item->note_last_edited_by) && $item->note_last_edited_by !== '')): ?>
                               <p><?= $item->note_last_edited_by ?></p>
                               <p><?= date_format(date_create($item->note_last_edited), 'D, d-m-Y') ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </td>
        <td class="link" data-value="<?= $item->link ?>" style="max-width: 40ch;">
            <?php if (!$item->is_done): ?>
                <div class="cell-edit-container d-none">
                    <input class="form-control cell-edit w-auto" type="text" value="<?= $item->link ?>">
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if (!$item->is_done): ?>
                    <button class="btn btn-link btn-edit">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value">
                    <p><a href="<?= $item->link?>" target="_blank" rel="noopener noreferrer">
                        <?= $item->link ?>
                    </a></p>
                    <?php if ((isset($item->link_last_edited) && $item->link_last_edited !== '') &&
                              (isset($item->link_last_edited_by) && $item->link_last_edited_by !== '')): ?>
                               <p><?= $item->link_last_edited_by ?></p>
                               <p><?= date_format(date_create($item->link_last_edited), 'D, d-m-Y') ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </td>
        <td class="comp_link" data-value="<?= $item->comp_link ?>" style="max-width: 40ch;">
            <?php if (!$item->is_done): ?>
                <div class="cell-edit-container d-none">
                <textarea class="form-control cell-edit w-auto"><?= $item->comp_link ?></textarea>
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-comp_link btn-cancel text-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-comp_link btn-save text-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if (!$item->is_done): ?>
                    <button class="btn btn-link btn-edit">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value">
                    <p><a href="<?= $item->comp_link?>" target="_blank" rel="noopener noreferrer">
                        <?= $item->comp_link ?>
                    </a></p>
                    <?php if ((isset($item->comp_link_last_edited) && $item->comp_link_last_edited !== '') &&
                              (isset($item->comp_link_last_edited_by) && $item->comp_link_last_edited_by !== '')): ?>
                               <p><?= $item->comp_link_last_edited_by ?></p>
                               <p><?= date_format(date_create($item->comp_link_last_edited), 'D, d-m-Y') ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </td>
        <td class="channel" data-value="<?= $item->channel ?>">
            <?php if (!$item->is_done): ?>
                <div class="cell-edit-container d-none">
                    <select class="form-control cell-edit w-auto">
                        <option value="UnixSurplusCom" <?= $item->channel === 'UnixSurplusCom' ? 'selected' : '' ?>>
                            UnixSurplusCom
                        </option>
                        <option value="UnixSurplusNet" <?= $item->channel === 'UnixSurplusNet' ? 'selected' : '' ?>>
                            UnixSurplusNet
                        </option>
                        <option value="UnixPlusCom" <?= $item->channel === 'UnixPlusCom' ? 'selected' : '' ?>>
                            UnixPlusCom
                        </option>
                        <option value="ITRecycleNow" <?= $item->channel === 'ITRecycleNow' ? 'selected' : '' ?>>
                            ITRecycleNow
                        </option>
                    </select>
                    <div class="cell-edit-buttons w-100 text-right">
                        <button class="btn btn-link btn-cancel text-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-link btn-save text-success">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="cell-content-container">
                <?php if (!$item->is_done): ?>
                    <button class="btn btn-link btn-edit">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <div class="cell-value">
                    <?php if (isset($item->channel) && $item->channel !== ''): ?>
                        <?= $item->channel ?>
                    <?php endif; ?>
                    <?php if ((isset($item->channel_last_edited) && $item->channel_last_edited !== '') &&
                              (isset($item->channel_last_edited_by) && $item->channel_last_edited_by !== '')): ?>
                                <?= $item->channel_last_edited_by ?>
                                <?= date_format(date_create($item->channel_last_edited), 'D, d-m-Y') ?>
                    <?php endif; ?>
                </div>
            </div>
        </td>
        <?php
            if ($item->is_done == '1'){?>
                <td class="undone">
                <p><?= $item->marked_done_by ?></p>
                <p><?= date('D Y-m-d', strtotime($item->done_date)) ?></p>
                <button class="btn btn-secondary btn-undone text-nowrap" data-toggle="modal"
                        data-target="#markListingUndoneModal">
                    <i class="far fa-square" aria-hidden="true"></i>
                    Undone
                </button>
            </td> 
           <?php } else { ?>
                <td class="done">
                <button class="btn btn-secondary btn-done text-nowrap" data-toggle="modal"
                        data-target="#markListingDoneModal">
                    <i class="far fa-square" aria-hidden="true"></i>
                    Mark Done
                </button>
        </td>
        <?php  }
        ?>
    </tr>
<?php endforeach; ?>
