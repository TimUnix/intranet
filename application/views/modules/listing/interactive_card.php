<div class="card">
    <div class="card-header">
        <h5 class="card-title">Listing</h5>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">

        <form class="search-form mt-2" action="<?= base_url() ?>index.php/listing" method="GET">
            <div class="input-group">
                <div class="input-group-prepend">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
                <input class="form-control" placeholder="Search PAR/SKU" name="search_query"
                       value="<?= $search_query ?>">
            </div>
            <div class="form-group col-md-8">
                <table>
                    <tr>
                        <td>
                            <label for="filterUsers">Users</label>
                            <select class="form-control filters" id="filterUsers" data-allow-clear="true"
                                    data-placeholder="Pick users" data-width="100%" name="users" multiple>
                                <?php foreach ($user_filters as $users): ?>
                                    <option value="<?= trim($users->firstname)?>">
                                        <?= $users->firstname ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>  
                        </td>
                        <td>
                            <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV" style="margin-left: 10px; margin-top: 35px;">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                    Export CSV
                            </button> 
                        </td>
                        <td class="form-row">
                        <div class="custom-control custom-switch" style="margin-left: 10px; margin-top: 40px;">
                        <input type="checkbox" class="done-filter form-control custom-control-input" id="doneSwitch" name="done_filter" value="1" <?php if(isset($done_filter)) echo "checked='checked'"; ?> >
                        
                        <label class="custom-control-label" for="doneSwitch">Show Done Only</label>
                        </div>
                        </td>
                    </tr>
                </table>
            </div> 
            <div class="form-group">
                <fieldset>
                    <legend>Channels</legend>
                    <div class="form-check form-check-inline mx-0 mb-1">
                        <div class="input-group-text channels-container">
                            <input class="form-check-input channel-filter" type="checkbox" name="channel_filter[]"
                                   value="unixsurpluscom" id="channelFilterCom"
                                   <?= isset($channel_filters) && in_array('unixsurpluscom', $channel_filters) ?
                                       'checked' : '' ?>>
                            <label class="form-check-label" for="channelFilterCom">UnixSurplusCom</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline mx-0 mb-1">
                        <div class="input-group-text channels-container">
                            <input class="form-check-input channel-filter" type="checkbox" name="channel_filter[]"
                                   value="unixsurplusnet" id="channelFilterNet"
                                   <?= isset($channel_filters) && in_array('unixsurplusnet', $channel_filters) ?
                                       'checked' : '' ?>>
                            <label class="form-check-label" for="channelFilterNet">UnixSurplusNet</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline mx-0 mb-1">
                        <div class="input-group-text channels-container">
                            <input class="form-check-input channel-filter" type="checkbox" name="channel_filter[]"
                                   value="unixpluscom" id="channelFilterPlus"
                                   <?= isset($channel_filters) && in_array('unixpluscom', $channel_filters) ?
                                       'checked' : '' ?>>
                            <label class="form-check-label" for="channelFilterPlus">UnixPlusCom</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline mx-0 mb-1">
                        <div class="input-group-text channels-container">
                            <input class="form-check-input channel-filter" type="checkbox" name="channel_filter[]"
                                   value="itrecyclenow" id="channelFilterItr"
                                   <?= isset($channel_filters) && in_array('itrecyclenow', $channel_filters) ?
                                       'checked' : '' ?>>
                            <label class="form-check-label" for="channelFilterItr">ITRecycleNow</label>
                        </div>
                    </div>
                    <button type="button" class="btn btn-secondary btn-sm btn-clear-channel-filters">
                        <i class="fa fa-backspace mr-2" aria-hidden="true"></i>
                        Clear Channel Filters
                    </button>
                </fieldset>
            </div>
            <div class="form-group">
                <fieldset>
                    <legend>Categories</legend>
                    <?php foreach ($categories as $index => $category): ?>
                        <?php if (trim($category->name) !== ''): ?>
                            <div class="form-check form-check-inline mx-0 mb-2">
                                <div class="input-group-text categories-container">
                                    <input class="form-check-input category-filter" type="checkbox"
                                           name="category_filter[]" value="<?= $category->name ?>"
                                           id="category<?= $index?>"
                                           <?= isset($category_filters)
                                            && in_array($category->name, $category_filters) ? 'checked' : '' ?>>
                                    <label class="form-check-label" for="category<?= $index ?>">
                                        <?= $category->name ?>
                                    </label>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <button type="button" class="btn btn-secondary btn-sm btn-clear-category-filters">
                        <i class="fa fa-backspace mr-2" aria-hidden="true"></i>
                        Clear Category Filters
                    </button>
                </fieldset>                
            </div>
        </form>
    </div>
</div>
