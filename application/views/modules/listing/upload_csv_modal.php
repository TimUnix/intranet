<div class="modal fade upload-csv-modal" id="uploadCsvModal" tabindex="-1"
     aria-labelledby="uploadCsvModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="createListingModalLabel">Upload CSV</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="upload-csv-form text-center" enctype="multipart/form-data" id="uploadCsvForm">
                    <input id="uploadCsvInput" class="upload-csv-input"
                           type="file" name="listing" accept="text/csv">
                </form>
            </div>
            <div class="modal-footer">
                <div class="upload-csv-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="submit" class="btn btn-primary btn-upload-csv" form="uploadCsvForm">Submit</button>
            </div>
        </div>
    </div>
</div>

