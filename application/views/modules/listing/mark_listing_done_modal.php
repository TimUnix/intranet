<div class="modal fade mark-listing-done-modal" id="markListingDoneModal" tabindex="-1"
     aria-labelledby="markListingDoneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="markListingDoneModalLabel">Mark <span class="sku-done"></span> Done </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-done">Confirm</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade mark-listing-undone-modal" id="markListingUndoneModal" tabindex="-1"
     aria-labelledby="markListingUndoneModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="markListingUnoneModalLabel">Mark <span class="sku-done"></span> Unone </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to proceed?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-confirm-undone">Confirm</button>
            </div>
        </div>
    </div>
</div>