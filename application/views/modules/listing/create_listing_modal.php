<div class="modal fade create-listing-modal" id="createListingModal" tabindex="-1"
     aria-labelledby="createListingModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="createListingModalLabel">Create Listing</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="createListingForm">
                    <div class="form-group" >
                        <label for="newListingSku">par/sku</label>
                        <input type="text" class="form-control" name="sku" id="newListingSku" required>
                    </div>
                    <div class="form-group">
                        <label for="newListingDescription">Description</label>
                        <textarea class="form-control" name="description" id="newListingDescription"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="newListingCategory">Category</label>
                        <input type="text" class="form-control" name="category" id="newListingCategory">
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="newListingQuantity">Quantity</label>
                            <input type="number" class="form-control" name="quantity" id="newListingQuantity">
                        </div>
                        <div class="form-group col">
                            <label for="newListingPrice">Price</label>
                            <input type="number" class="form-control" name="price" id="newListingPrice" step="any">
                        </div>
                    </div>
                    <fieldset>
                        <legend>Price Research Market</legend>
                        <div class="row">
                            <div class="form-group col">
                                <label for="newListingPriceResearchMarketLow">Low</label>
                                <input type="text" class="form-control" name="price_research_market_low"
                                       id="newListingPriceResearchMarketLow">
                            </div>
                            <div class="form-group col">
                                <label for="newListingPriceResearchMarketHigh">High</label>
                                <input type="text" class="form-control" name="price_research_market_high"
                                       id="newListingPriceResearchMarketHigh">
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <label for="newListingNote">Note</label>
                        <textarea class="form-control" name="note" id="newListingNote"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="newListingLink">Link</label>
                        <input type="text" class="form-control" name="link" id="newListingLink">
                    </div>
                    <div class="form-group">
                        <label for="newListingChannel">Channel</label>
                        <select class="form-control" name="channel" id="newListingChannel">
                            <option selected disabled value="">Select a channel</option>
                            <option value="UnixSurplusCom">UnixSurplusCom</option>
                            <option value="UnixSurplusNet">UnixSurplusNet</option>
                            <option value="UnixPlusCom">UnixPlusCom</option>
                            <option value="ITRecycleNow">ITRecycleNow</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" form="createListingForm">Submit</button>
            </div>
        </div>
    </div>
</div>
