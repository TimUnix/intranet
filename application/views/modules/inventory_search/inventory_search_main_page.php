<?php $this->load->view('templates/header_new'); ?>


<div id="load_cat"></div>
 
    </div>
<?php $this->load->view('templates/common/spinner'); ?>
<?php $this->load->view('templates/common/footer'); ?>

<script>
$(document).ready(function(){
    $("#load_cat").load("<?php echo base_url(); ?>index.php/inventory_search/request/load_cat");
 });
</script>
<script>
 $(document).ready(function(){
     const params = new URLSearchParams(window.location.search);
     const searchParam = params.get('search');

     const wasItemRequested = searchParam !== null;

     if (wasItemRequested) {
         $("#load_products").html(spinner);
         $("#ebayitem").val(searchParam);

         const referred = params.get('referred');
         const wasReferred = referred == 1;

         if (wasReferred) {
	         $("#local_item_container").load("<?php echo base_url(); ?>index.php/research/request/local_items", {
                 'part_number': $("#ebayitem").val(),
                 'sort_order': $("#sortOrder").find(":selected").val(),
                 'order_by': $("#orderBy").find(":selected").val()
             });
         }

         $.post("<?php echo base_url(); ?>index.php/research/request/search_ebay/<?php echo $_GET['sortOrder'] . '/' . $_GET['orderBy']; ?>",
                { ebayitem : $("#ebayitem").val(), ajax : '1' },
                function(response) {
                    $("#load_products").html(response);
                }
         );
     }

     $("#searchHistory").on('change', function() {
         $("#ebayitem").val($(this).val());
     });
 });
</script>

        





