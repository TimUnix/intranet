<?php foreach ($items as $index => $item): ?>
    <tr>
        <th scope="row"><?= $index + 1 ?></th>
        <td><input type="checkbox" value="<?= $item->par ?>" class="checkbox-row"></td>
        <td>
            <button type="button" class="btn btn-link btn-copy-row">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
        </td>
        <td class="par" style="min-width: 120px;">
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->par ?>
        </td>
        <td>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->bar ?>
        </td>
        <td>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->des ?>
        </td>
        <td>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->con ?></td>
        <td>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->cat ?>
        </td>
        <td>$<?= $item->pri ?></td>
        <td><?= $item->qty ?></td>
        <td>$<?= $item->tot ?></td>
        <td>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->bin ?>
        </td>
    </tr>
<?php endforeach; ?>
