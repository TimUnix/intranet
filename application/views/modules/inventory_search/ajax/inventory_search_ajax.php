<div class="container-fluid" style="padding-bottom: 60px;">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <div class="input-group-prepend">
                                <button class="btn btn-lg btn-primary" id="search_item" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                            <select class="form-control col-2" id="searchHistory"
                                    style="width: 100%;" 
                                    name="searchHistory[]">
                                <option value="" disabled selected>
                                    Part Number
                                </option>
                                <?php foreach ($search_history as $history): ?>
                                    <option value="<?= $history->search; ?>">
                                        <?= $history->search; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <input class="form-control form-control-lg" type="search" id="part_number"
                                   placeholder="Part Number">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div style="margin-bottom: 5px;margin-left: -7px;"> 
                        <h3>Categories</h3>
                    </div>
                </div>
                <?php 
                foreach($inventory_cat as $cat): ?>
                    <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
                        <div class="input-group-text">
                            <input class="form-check-input checkbox-category" type="checkbox" id="<?= $cat->cat ?>"
                                   name="<?= $cat->cat ?>" value="<?= $cat->cat ?>">
                            <label class="form-check-label" for="<?= $cat->cat ?>"><?= $cat->cat ?></label>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="col-md-12">
                    <div style="margin-bottom: 5px;margin-left: -7px; margin-top:10px;"> 
                        <h3>Conditions</h3>
                    </div>
                </div>
                <?php 
                foreach($inventory_con as $con): ?>
                    <?php if($con->con == ""): ?>
                        <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
                            <div class="input-group-text">
                                <input class="form-check-input checkbox-condition" type="checkbox"
                    id="condition-blank" name="condition-blank" value="">
                                <label class="form-check-label" for="condition-blank">Blank</label>
                            </div>
                        </div>
                    <?php else:  ?>
                        <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
                            <div class="input-group-text">
                                <input class="form-check-input checkbox-condition" type="checkbox"
                                       id="<?= $con->con ?>" name="<?= $con->con ?>" value="<?= $con->con ?>">
                                <label class="form-check-label" for="<?= $con->con ?>"><?= $con->con ?></label>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="form-check form-check-inline" style="margin-right: 7px; margin-bottom: 3px;"> 
                    <div class="input-group-text">
                        <input class="form-check-input checkbox-condition" type="checkbox" id="condition_zero_qty"
                               name="condition_zero_qty" value="condition_zero_qty">
                        <label class="form-check-label" for="condition_zero_qty">Zero QTY</label>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-1 text-center d-flex h-100">
                    <button type="button"
                            class="btn btn-primary btn-copy-textarea">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                        Copy
                    </button>
                </div>
                <div class="col-md-11">
                    <textarea class="textarea-copy form-control"rows="4" name="copy-area"></textarea>
                </div>
            </div>
            <div class="row">
                <span>
                    <strong style="font-size: x-large;">Items: </strong>
                    <span id="total-items" style="font-size: x-large; font-weight: 900;"></span>
                </span>
                <span class="mx-3" style="font-size: x-large;font-weight: 900;">
                    -
                </span>
                <span>
                    <strong style="font-size: x-large;">Total Price: </strong>
                    <span id="total-price" style="font-size: x-large;font-weight: 900;"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-outline-primary"
                        onclick="download_table_as_csv('product-table')"
                        id="exportCSV">
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
            </div>
        </div>
        <div class="card-body p-0 inventory-container table-responsive">
            <table class="table table-bordered table-striped table-hover" id="product-table">
                <thead>
                    <tr>
                        <th scope="col">&#35;</th>
                        <th scope="col"><input type="checkbox" class="check-all-toggle"></th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-batch-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
							Copy
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-par-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Part Number
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-bar-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            OEM PN
                        </th>
                        
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-des-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Description
                        </th>
                        <th scope="col">Con</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">QTY</th>
                        <th scope="col">Total</th>
                        <th scope="col">Bin</th>
                    </tr>
                </thead>
                <tbody id="product_table_body"></tbody>
            </table>
        </div>
    </div>
</div>

<script>
 function search() {
     $('#product_table_body').html(
         `<tr class="odd"><td valign="top" colspan="12" class="datatables_empty">${spinner}</td></tr>`
     );
     
     $('#total-price').html(spinner);
     $('#total-items').html(spinner);

     const dataSearchHistory = { 'search': $('#part_number').val() };

     $.post("<?= base_url() ?>index.php/inventory_search/update_search_history", dataSearchHistory);

     const categories = $('input.checkbox-category:checked').map(function() {
         return $(this).val();
     }).get();

     const conditions = $('input.checkbox-condition:checked').map(function() {
         return $(this).val();
     }).get();

     const data = {
         "categories" : categories,
         "conditions": conditions,
         "part_number": $("#part_number").val(),
         "searchHistory": $("#searchHistory").val()
     };

     $.post("<?= base_url() ?>index.php/inventory_search/search", data, function(response){
         $("#product_table_body").html(response);
     });

     $.post("<?= base_url() ?>index.php/inventory_search/load_summary", data, function(response) {
         $("#total-price").html(response.summary.price === null ? '$0' : `\$${response.summary.price}`);
         $("#total-items").html(response.summary.total);
     });
 }

 $(document).ready(function() {
     $("#search_item").click(function() {
         search();
     });

     $("#searchHistory").change(function() {
         $('#part_number').val($(this).val());
         search();
     });
 });
</script>


<script id="inventoryScript" data-base-url="<?php echo base_url(); ?>"
        type="text/javascript"
        src="<?php echo base_url() ?>resources/js/inventorysearch/inventory_search.js"></script>
</script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/inventorysearch/rowInventoryCopy.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/inventorysearch/batchinventorysearchCopy.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/fieldCopy.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>resources/js/exportCSV.js"></script>
