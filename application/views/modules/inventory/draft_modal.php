<div class="modal fade" id="draftModal" tabindex="-1" aria-labelledby="draftModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="draftModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div id="modal-body" class="container" style="margin-top:10px;"> 
                <div class="row"> 
                    <div id="slider" class="col-7 border">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" id="carr">
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div id="details" class="col-5">
                        <p><strong>Title</strong></p>
                        <h5 id="draftDes"></h5>
                        <hr>
                        <label>SKU</label>
                        <p id="draftSku"></p>
                        <label>Condition</label>
                        <p id="draftCondition"></p>
                        <label>Category</label>
                        <p id="draftCategory"></p>
                        <hr>
                        <label>Quantity</label>
                        <p id="draftQty"></p>
                        <label>Price</label>
                        <p id="draftPrice">$ </p>
                    </div>                    
                </div>
			</div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col">
                        <div id="errormessage"></div>
                    </div>
                    <div class="col-md-auto">
                        <div id="shipping" style="display:none;" value="Calculated"></div>
                        <select class="form-control" id="action">
                            <?php foreach ($data['actions'] as $action): ?>
                                <option value="<?= $action->action_id ?>"
                                        <?php if (isset($data['task']) && $data['task'] !== ''): ?>
                                        <?= $action->action_name === $data['task']['actions'] ? 'selected' : '' ?>
                                        <?php endif; ?>>
                                    <?= $action->action_name ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-auto">
                        <button type="button" class="btn btn-primary" id="addtask"><i class="fa fa-plus" aria-hidden="true"></i>Add Task</button></div>
                </div>
            </div>  
        </div>   
    </div>    
</div>  

<div class="modal fade" id="existing" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
				<div style="text-align:center;">    
                    <i class="fas fa-times-circle fa-5x" style="color:red;"></i>
                </div><br>
                <div style="text-align:center;">
                    <h5>SKU and Task already Exists!</h5><br>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <button class="btn btn-primary btn-lg" id="close_existing" style="width:100%"> OK </button>
            </div>
        </div>
    </div>
</div>          
