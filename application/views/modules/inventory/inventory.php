<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/inventory/keyword_modal'); ?>
<?php $this->load->view('modules/inventory/linnworks_modal'); ?>
<?php $this->load->view('modules/inventory/draft_modal'); ?>

<style>
 .inventory-img-link {
     display: none;
     z-index: 100;
     position: absolute;
     width: 180px;
 }

 .inventory-img {
     white-space: nowrap;
 }

 .inventory-img:hover {
     position: relative;
 }

 .inventory-img:hover .inventory-img-link {
     display: inline-block;
 }

 .con-cat-container {
     padding: 2px 8px 2px 8px;
 }

 .summary {
	 display: inline;
	 margin-left: 8px;
 }

 #slider {
	 margin: 0 auto;
	 width: 420px;
	 max-width: 100%;
	 text-align: center;
	 float:left;
	 height: 500px;
 }

 #slide{
	 width: 420px;
	 max-width: 100%;
	 text-align: center;
	 float:left;
	 height: 500px;
 }

 #slide1{
	 width: 420px;
	 max-width: 100%;
	 text-align: center;
	 float:left;
	 height: 500px;
 } 
</style>

<div id="load"></div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header text-left">
                <h3 class="modal-title w-100">New</h3>
            </div>
            <div class="modal-body">
                <div id="success"></div>
                <div class="gettask"> 
                    <label for="" >Tasks</label>
                    <select id="request" class="form-control">
                        <?php foreach ($data['actions'] as $action): ?> 
                            <option class="tasks" value="<?= $action->action_id ?>">
                                <?= $action->action_name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group getsku" novalidate>
                    <label for="sku">SKU</label>
                    <input type="text" class="form-control" id="sku" list="skuSuggestions" required
                           <?= isset($data['task']) ? "value=\"{$data['task']['sku']}\"" : '' ?>>
                    <datalist id="skuSuggestions"></datalist>
                </div>
                <div class="form-group weight">
                    <label for="weight">Weight</label>
                    <input type="number" class="form-control" id="weight" step="any">
                </div>
                <div class="form-group">
                    <label for="shipping">Shipping</label>
                    <input type="text" class="form-control" id="shipping" value="Calculated">
                </div>
                <div id="moved-form" class="form-group d-none">
                    <label for="moved">Link</label>
                    <input type="text" class="form-control" id="moved">
                </div>
                <div class="form-group mt-5">
                    <label for="notes">Notes</label><textarea class="form-control" id="notes" rows="3"></textarea>
                </div>
                <div class="form-group getprice">
                    <label for="price">Price</label>
                    <input type="number" class="form-control" id="price" step="any"
                           <?= isset($data['task']) ? "value=\"{$data['task']['price']}\"" : '' ?>>
                </div>
                <div class="form-group">
                    <label for="promotion">Maximum Promotion Percentage</label>
                    <div class="input-group">
                        <input type="number" class="form-control" step="any" id="promotion">
                        <div class="input-group-append">
                            <span class="input-group-text">%</span>
                        </div>
                    </div>
                </div>
                <fieldset>
                    <legend>Volume Discount</legend>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="volumeDiscount2x">2 or more</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" step="any" id="volumeDiscount2x">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="volumeDiscount3x">3 or more</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" step="any" id="volumeDiscount3x">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="volumeDiscount4x">4 or more</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" step="any" id="volumeDiscount4x">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <label for="lot">Lot Size</label></br><input type="text" class="form-control" id="lot" value="">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filtersModal" tabindex="-1" aria-labelledby="filtersModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filtersModalLabel">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body filters-container"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="applyFilters" class="btn btn-primary" data-dismiss="modal">
                    Apply Filters
                </button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('modules/ebay_items/create_ebay_ad_modal'); ?>


<div class="container-fluid" style="padding-bottom: 60px;">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <?= $data['page_name'] ?>
            </div>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <form id="searchForm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary"
                                            type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select class="form-control form-control-lg col-2" id="searchHistory"
                                        name="searchHistory[]">
                                    <option value="" disabled selected>
                                        Search History
                                    </option>
                                    <?php foreach ($data['search_history'] as $history): ?>
                                        <option value="<?= $history->search; ?>">
                                            <?= $history->search; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <input class="form-control form-control-lg"
                                       type="search" id="itemSearch"
                                       placeholder="Search Part Number">
                            </div>
                        </div>
                    </div>
                </div>
                <p><strong>Categories</strong></p>
                <div class="input-group input-group-sm mb-2 category-filters">
                    <?php foreach($data['categories'] as $category): ?>
                        <?php
                        $str = $category->category;
                        $pattern = "/&/"; 
                        $and_char_text_checking = preg_match($pattern, $str);
                        ?>
                        <?php if($and_char_text_checking == 1) : ?>
                            <?php $category_val = str_replace(' & ', '-', $category->category) ?>
                            <div class="form-check form-check-inline mx-0 mb-2">
                                <div class="input-group-text con-cat-container">
                                    <input type="checkbox" class="form-check-input con-cat-filter"
                                           id="category_<?= $category_val ?>"
                                           value="<?= $category_val ?>">
                                    <label class="form-check-label" for="category_<?= $category_val ?>">
                                        <?= $category->category ?>
                                    </label>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="form-check form-check-inline mx-0 mb-2">
                                <div class="input-group-text con-cat-container">
                                    <input type="checkbox" class="form-check-input con-cat-filter"
                                           id="category_<?= str_replace(' ', '_', $category->category) ?>"
                                           value="<?= $category->category ?>">
                                    <label class="form-check-label"
                                           for="category_<?= str_replace(' ', '_', $category->category) ?>">
                                        <?= $category->category ?>
                                    </label>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <p><strong>Condition</strong></p>
                <div class="form-group mb-2 condition-filters">
                    <?php foreach($data['conditions'] as $condition): ?>
                        <div class="form-check form-check-inline mx-0 mb-2">
                            <?php $condition_display = trim($condition->con) === '' ? 'Blank' : $condition->con ?>
                            <div class="input-group-text con-cat-container">
                                <input type="checkbox" class="form-check-input con-cat-filter"
                                       value="<?= $condition_display ?>"
                                       id="condition_<?= str_replace(' ', '_', $condition_display) ?>">
                                <label class="form-check-label"
                                       for="condition_<?= str_replace(' ', '_', $condition_display) ?>">
                                    <?= $condition_display ?>
                                </label>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="form-check form-check-inline mx-0 mb-2">
                        <div class="input-group-text con-cat-container">
                            <input type="checkbox" class="form-check-input con-cat-filter" value="Zero QTY"
                                   id="condition_Zero_QTY">
                            <label class="form-check-label" for="condition_Zero_QTY">
                                Zero Qty
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="sortOrder">Sort Order:</label>
                            <select class="form-control form-control-sm" id="sortOrder" tabindex="-1"
                                    aria-hidden="true">
                                <option value="DESC" selected>Descending</option>
                                <option value="ASC">Ascending</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="orderBy">Order By:</label>
                            <select class="form-control form-control-sm" tabindex="-1" aria-hidden="true"
                                    id="orderBy">
                                <option value="par">Part Number</option>
                                <option value="bar">OEM PN</option>
                                <option value="pri">Price</option>
                                <option value="qty">QTY</option>
                                <option value="cat">Category</option>
                                <option value="con">Condition</option>
                                <option value="tot" selected>Total</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="listingFilter">Listing filter</label>
                            <select class="form-control form-control-sm" id="listingFilter">
                                <option value="all">All</option>
                                <option value="listed">Listed in eBay</option>
                                <option value="unlisted">Unlisted</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row mt-4 d-none">
                <div class="col-md-1 text-center d-flex h-100">
                    <button type="button"
                            class="btn btn-primary btn-copy-textarea">
                        <i class="fa fa-copy" aria-hidden="true"></i>
                        Copy
                    </button>
                </div>
                <div class="col-md-11">
                    <textarea class="textarea-copy form-control" rows="4" name="copy-area"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items</h3>
            <button type="button" class="btn btn-sm btn-primary ml-2 btn-create-ad-blank"
                    data-toggle="modal" data-target="#createEbayAdModal">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Create eBay Ad
            </button>
			<p class="summary">
				<strong style="font-size: 1.5rem;">Items: </strong>
				<span id="total-items" style="font-size: 1.5rem;"></span>
				-
				<strong style="font-size: 1.5rem;">Total Price: </strong>
				<span id="total-price" style="font-size: 1.5rem;"></span>
			</p>
            <div class="card-tools">
                <button type="button" class="btn btn-sm btn-outline-primary" id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <button type="button" class="btn btn-sm btn-outline-primary" id="exportToGdrive" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export to GDrive
                </button>
            </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body p-0 sticky-headers d-none">
            <table class="table paginated table-bordered table-striped table-hover sticky-table"
                   cellspacing="0" style="border-collapse: separate;">
                <thead class="bg-white border">
                    <tr class="sticky-header-row">
                        <th scope="col">&#35;</th>
                        <th scope="col"><input type="checkbox" class="check-all-toggle_sticky_header"></th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-batch-copy_sticky_header">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Copy
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link text-dark mr-1" data-toggle="tooltip"
                                    data-placement="bottom" title="Create Task">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </button>
                            Task
                        </th>
                        <!-- <th scope="col">
                             <button type="button" class="btn btn-link text-dark mr-1" data-toggle="tooltip"
                             data-placement="bottom" title="Create Task">
                             <i class="fa fa-edit" aria-hidden="true"></i>
                             </button>
                             Draft</th> -->
                        <th scope="col">
                            <i class="fa fa-highlighter" aria-hidden="true"></i>
                            Keyword
                        </th>
                        <!-- <th scope="col">Create Ad</th> -->
                        <!-- <th scope="col">Image</th> -->
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-par-copy_sticky_header">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Part Number
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-bar-copy_sticky_header">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            OEM PN
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-des-copy_sticky_header">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Description
                        </th>
                        <th scope="col">Con</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">QTY</th>
                        <th scope="col">Velocity</th>
                        <th scope="col">Total</th>
                        <th scope="col">Bin</th>
                        <th scope="col">Channel</th>
                    </tr>
                </thead>
                
            </table>
        </div>


        <!-- new table -->
        <div class="card-body p-0 inventory-container table-responsive table-container">
            <table class="table paginated table-bordered table-striped table-hover main-table" id="product-table"
                   cellspacing="0" style="border-collapse: separate;">
                <thead class="bg-white border">
                    <tr class="main-header-row">
                        <th scope="col">&#35;</th>
                        <th scope="col"><input type="checkbox" class="check-all-toggle"></th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-batch-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Copy
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link text-dark mr-1" data-toggle="tooltip"
                                    data-placement="bottom" title="Create Task">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </button>
                            Task
                        </th>
                        <!-- <th scope="col">
                             <i class="fa fa-store" aria-hidden="true"></i>
                             </button>
                             Draft
                             </th> -->
                        <th scope="col">
                            <i class="fa fa-highlighter" aria-hidden="true"></i>
                            Keyword
                        </th>
                        <!-- <th scope="col">Create Ad</th> -->
                        <!-- <th scope="col">Image</th> -->
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-par-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Part Number
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-bar-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            OEM PN
                        </th>
                        <th scope="col">
                            <button type="button" class="btn btn-link btn-des-copy">
                                <i class="fa fa-copy" aria-hidden="true"></i>
                            </button>
                            Description
                        </th>
                        <th scope="col">Con</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">QTY</th>
                        <th scope="col">Velocity</th>
                        <th scope="col">Total</th>
                        <th scope="col">Bin</th>
                        <th scope="col">Channel</th>
                    </tr>
                </thead>
                <tbody id="product_table_body"></tbody>
            </table>
        </div>
       <!-- end new table-->
    </div>
</div>

<?php $this->load->view('templates/common/spinner'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/paginate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/rowCopy.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/batchCopy.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/fieldCopy.js"></script>
<script id="listingScript" data-base-url="<?= base_url(); ?>"
        src="<?= base_url(); ?>resources/js/inventorylisting/listing.js"></script>
<script id="inventoryScript" data-base-url="<?php echo base_url(); ?>"
        type="text/javascript" src="<?php echo base_url() ?>resources/js/inventory.js"></script>
<script id="ebayApiScript" data-base-url="<?php echo base_url(); ?>"
        type="text/javascript" src="<?php echo base_url() ?>resources/js/ebay_items/ebay_api.js"></script>
<script id="inventoryScript" data-base-url="<?php echo base_url(); ?>"
        type="text/javascript" src="<?php echo base_url() ?>resources/js/google-drive.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php $this->load->view('templates/common/footer'); ?>


