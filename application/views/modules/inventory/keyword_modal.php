<div class="modal fade" id="keywordModal" tabindex="-1" aria-labelledby="keywordModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="keywordModalLabel">Keyword</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="keyword-alert alert alert-danger d-none" role="alert"></div>
                <form id="keywordForm" method="post">
                    <div class="form-group">
                        <label for="keywordPar">Keyword Par</label>
                        <input type="text" name="par" id="keywordPar" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="keywordPrase">Keyword Prase</label>
                        <textarea type="text" name="prase" id="keywordPrase" class="form-control"
                                  rows="3" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" form="keywordForm">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>
