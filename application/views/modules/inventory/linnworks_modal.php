<div class="modal fade" id="updateDescriptionLinn" tabindex="-1" aria-labelledby="updateLinnworksPriceLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="updateLinnworksModalLabel" class="modal-title">Update Description to Linnworks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="id" style="display:none"></span>
                <div><strong>SKU: </strong><span id="sku-linnworks"></span></div>
                <div><strong>Current Description: </strong><span id = "current-desc-linn"></span></div>
                <strong>New Description: </strong><br>
                <textarea type="text" class="form-control form-control-sm" id="new-desc-linn" row="3"></textarea>
            </div>
            <div class="modal-footer">
                <div class="new-ebay-ad-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="button" class="btn btn-default mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="updateLinnworksDescSubmit">
                    Update
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updatePriceLinn" tabindex="-1" aria-labelledby="updateLinnworksPriceLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="updateLinnworksModalLabel" class="modal-title">Update Price to Linnworks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="id" style="display:none"></span>
                <div><strong>SKU: </strong><span id="sku-linnworks-price"></span></div>
                <div><strong>Current Price: </strong><span id = "current-price-linn"></span></div>
                <strong>New Price: </strong><br>
                <textarea type="text" class="form-control form-control-sm" id="new-price-linn" row="1"></textarea>
            </div>
            <div class="modal-footer">
                <div class="new-ebay-ad-spinner-container w-100 text-center d-none">
                    <i class="fa fa-spinner fa-2x fa-pulse" aria-hidden="true"></i>
                </div>
                <button type="button" class="btn btn-default mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="updateLinnworksPriceSubmit">
                    Update
                </button>
            </div>
        </div>
    </div>
</div>