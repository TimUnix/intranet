<label for="request">Task</label>
                    <select class="form-control" id="request">
                        <?php foreach ($data['actions'] as $action): ?>
                            <option value="<?= $action->action_id ?>"
                                    <?php if (isset($data['task']) && $data['task'] !== ''): ?>
                                    <?= $action->action_name === $data['task']['action'] ? 'selected' : '' ?>
                                    <?php endif; ?>>
                                <?= $action->action_name ?>
                        <?php endforeach; ?>
                    </select>