<?php if(isset($items)) : ?>
<?php foreach ($items as $row_index => $item): ?>
    <?php
    $index = $offset + $row_index;
    $images = explode(',', $item->images);
    $images = array_map(function($image) {
        // "/i/noimage.png" is hardcoded in the database
        // so change it to the new noimage path
        return ($image === '' || $image === '/i/noimage.png') ?
               base_url() . 'resources/img/noimage.png' : $image;
    }, $images);
    ?>
    <tr>
        <th scope="row"><?= $index + 1 ?></th>
        <td><input type="checkbox" value="<?= $item->part; ?>" class="checkbox-row"></td>
        <td>
            <button type="button" class="btn btn-link btn-copy-row">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
        </td>
        <td scope="col" nowrap class="sku">
            <?php if (isset($item->part) && $item->part !== ''): ?>
                <button id="tglr<?= $index ?>" class="btn btn-link popover-toggle" data-toggle="tooltip"
                        data-placement="bottom" title="Promote Task">
                    <i class="fa fa-edit" aria-hidden="true" style="color: #007bff"></i>
                </button>
                <div id="listchannels<?= $index ?>" style="display: none">
                    <ul class="list-group list-group-flush">
                        <?php foreach ($actions as $action): ?> 
                        <li class="fa fa-edit" aria-hidden="true" style="color: #007bff; list-style: none;"
                           class="btn btn-info btn-lg ml-2 mb-4" data-toggle="modal" data-target="#myModal"> 
                            <a class="btn btn-link" href="#">
                                <option style="font-family: Source Sans Pro;" class="task_list" id="tasks" value="<?= $action->action_id ?>">  
                                    <?= $action->action_name ?> 
                                </option>
                            </a>
                        </i>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <script>
                 $(document).ready(function(){
                     $('#tglr<?= $index ?>').popover({
                         trigger: 'hover',html : true, sanitize : false, placement: 'auto', container: $('#tglr<?= $index ?>'), animation: true, 
                         content: function() {
                             return $('#listchannels<?= $index ?>').html();
                         } 
                     });  
                 });
                </script>
            <?php endif; ?>
        </td>
        <!-- <td scope="col">
             <button type="button" class="btn btn-link btn-draft " data-toggle="modal"
             data-target="#draftModal">
             <i class="fa fa-store" aria-hidden="true"></i>
             </button>
			 </td> -->
        
        <td scope="col" class="key" data-value="<?= $item->keyword_prase ?>">
            <button type="button" class="btn btn-link btn-lg mr-1 keyword-btn" data-toggle="modal"
                    data-target="keywordModal">
                <i class="fa fa-highlighter keyword-btn" aria-hidden="true"></i>
            </button>
           <?= $item->keyword_prase ?><br>
           <?php $firstname_explode = explode(' ', $item->edited_by) ?>
           <?php $firstname = $firstname_explode[0] ?>
           <?= $firstname ?><br>
           <?php if(isset($item->edited_date)){?>
           <?= date('D Y/m/d', strtotime($item->edited_date)) ?>
            <?php } ?>

        </td>
        <!-- <td>
             <button type="button" class="btn btn-link btn-create-ad" data-toggle="modal"
             data-target="#createEbayAdModal">
             <i class="fa fa-plus" aria-hidden="true"></i>
             </button>
			 </td> -->
        <!-- <td class="h">
             <?php foreach($images as $index => $image): ?>
             <a class="inventory-img" href="<?= $image ?>" rel="noreferrer noopener" target="_blank">
             <img id=im class="inventory-img-link" src="<?= $image ?>" loading="lazy">
             Image <?= $index + 1 ?>
             </a>
             <?php endforeach; ?>
			 </td> -->
        <td class="par" data-value="<?= $item->part ?>" style="max-width: 90px;">
            <?php if (isset($item->part) && $item->part !== ''): ?>
                <a class="btn btn-link"
                   href="<?= base_url(); ?>index.php/research?search=<?= urlencode($item->part) ?>&referred=1">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->part ?>
        </td>
        <td class="bar" data-value="<?= $item->barcode_num ?>" style="max-width: 90px;">
            <?php if (isset($item->barcode_num) && strlen($item->barcode_num) > 0): ?>
                <a class="btn btn-link"
                   href="<?= base_url(); ?>index.php/research?search=<?= $item->barcode_num; ?>&referred=1">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->barcode_num ?>
        </td>
        <td class="des" data-value="<?= $item->des ?>">
            <button type="button" class = "btn btn-link btn-md update-linnworks-desc" data-toggle="modal" 
            data-target="#updateDescriptionLinn">
                <i class="fa fa-edit update-linnworks-desc" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->des ?>
        </td>
        <td class="con" data-value="<?= $item->con ?>">
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->con ?></td>
        <td class="cat" data-value="<?= $item->category ?>">
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->category ?></td>
        <td class="price" data-value="<?= $item->price ?>" style="text-align: center">
            <button type="button" class = "btn btn-link btn-md update-linnworks-price" data-toggle="modal" 
                data-target="#updatePriceLinn">
                <i class="fa fa-edit update-linnworks-price" aria-hidden="true"></i>
            </button>
        $<?= (int)$item->price; ?></td>
        <td class="qty" data-value="<?= $item->qty ?>" style="text-align: center"><?= $item->qty; ?></td>
        <td class="velocity" data-value="<?= $item->velocity_difference ?>" style="text-align: center"><?= $item->velocity_difference; ?></td>
        <td class="tot">$<?= (int)$item->tot; ?></td>
        <td style="max-width: 80px;">
            <button type="button" class="btn btn-link copy-field">
                <i class="fa fa-copy" aria-hidden="true"></i>
            </button>
            <?= $item->bin; ?>
        </td>
        <td class="channels">
            <a href="<?= $item->link ?>" rel="noreferrer noopener" target="_blank">
                <?php if($item->seller == "UNIXPlusCom"): ?>
                    PLUS
                <?php endif; ?>
                <?php if($item->seller == "UNIXSurplusNet"): ?>
                    NET
                <?php endif; ?>
                <?php if($item->seller == "UNIXSurplusCom"): ?>
                    COM
                <?php endif; ?>
                <?php if($item->seller == "ITRecycleNow"): ?>
                    ITR
                <?php endif; ?>
            </a>
        </td>
       
    </tr>
<?php endforeach; ?>
<?php else : ?>
    <?php exit; ?>
<?php endif; ?>
<script>
 $(document).ready(function() {
     const help = $('.popover-dismiss');
     help.attr('data-content', help.html()).text(' ? ')
         .popover({trigger: 'click', html: true, sanitize: false});


         $(".sku").click(function(){
        $("#success").html("<div></div>");
    })
 });
</script>
