<?php if(!empty($items)) { foreach($items as $index => $item): ?>
    <tr>
        <td scope="col"><?= $index + 1 ?></td>
        <td scope="col" data-value="<?= $item->barcode ?>"class="barcode item-barcode<?= isset($columns->barcode->is_shown) 
                                         ? ($columns->barcode->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap><?= $item->barcode ?></td>
        <td scope="col" data-value="<?= $item->client_name ?>" class="item-client_name<?= isset($columns->client_name->is_shown)
                                          ? ($columns->client_name->is_shown ? '' : ' d-none') 
                                            : '' ?>" nowrap><?= $item->client_name ?></td>
        <td scope="col" class="item-sales_rep<?= isset($columns->sales_rep->is_shown) 
                                       ? ($columns->sales_rep->is_shown ? '' : ' d-none') 
                                         : '' ?>" nowrap><?= $item->sales_rep ?></td>
        <td scope="col" class="item-ship_method<?= isset($columns->ship_method->is_shown) 
                                       ? ($columns->ship_method->is_shown ? '' : ' d-none') 
                                         : '' ?>" nowrap><?= $item->ship_method ?></td>
        <td scope="col" class="item-ship_date<?= isset($columns->ship_date->is_shown) 
                                       ? ($columns->ship_date->is_shown ? '' : ' d-none') 
                                         : '' ?>" nowrap><?= $item->ship_date ?></td>
        <td scope="col" class="item-created_date<?= isset($columns->created_date->is_shown) 
                                            ? ($columns->created_date->is_shown ? '' : ' d-none') 
                                              : '' ?>" nowrap><?= $item->created_date ?></td>
        <td scope="col" class="text-center">
                   
    <button type="button" class="btn btn-link btn-lg edit-item" data-toggle="modal" data-target="#editorder">
                <i class="fas fa-pen edit-item" aria-hidden="true"></i>
            </button> 
            <button type="button" class="btn btn-link btn-lg deleterow" data-toggle="modal" data-target="#confirmDelete">
                <i class="fas fa-trash" aria-hidden="true"></i>
            </button>


            <button type="button" class="btn btn-link btn-lg print">
                <i class="fas fa-print" aria-hidden="true"></i>
            </button>
        </td>
    </tr>
<?php endforeach; }?>
