<?php $this->load->view('templates/header_new'); ?>
<?php $this->load->view('templates/scrollbar'); ?>
<?php $this->load->view('modules/work_orders/modal_pop_up'); ?>
<?php $this->load->view('modules/work_orders/customer_data'); ?>
<?php $this->load->view('modules/work_orders/include_chassis'); ?>
<?php $this->load->view('modules/work_orders/motherboard_only'); ?>
<button id="insertOrder" type="button" class="btn btn-info btn-lg ml-2 mb-4">Create Work Order</button>
<div id="load"></div>

<!-- Search Bar -->
<div class="container-fluid" style="padding-bottom: 60px;">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Search
            </div>
        </div>
        <div class="card-body">
            <form id="searchForm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <button class="btn btn-lg btn-primary" type="submit" id="searchColumn">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <select id="searchSelected" class="custom-select col-md-2">
                                    <option value="barcode">Barcode</option>
                                    <option value="client_name">Customer</option>
                                    <option value="sales_rep">Sales Rep</option>
                                    <option value="ship_method">Shipping Method</option>
                                    <option value="ship_date">Shipping Date</option>
                                </select>
                                <input class="form-control input-lg" type="search" id="itemSearch"
                                       placeholder="Search">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="orderBy">Order By</label>
                        <select type="button" class="form-control" id="orderBy">   
                           <option value="barcode">Barcode</option>
                            <option value="client_name">Customer</option>
                            <option value="sales_rep">Sales Rep</option>
                            <option value="ship_method">Shipping Method</option>
                            <option value="ship_date">Shipping Date</option>                                                       
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sortOrder">Sort Order</label>
                        <select type="button" class="form-control" id="sortOrder">
                            <option value="ASC" selected>Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Summary</h2>
        </div>
        <div class="card-body">
            <span><strong>Total:</strong> <span class="task-count"></span></span>
        </div>
    </div>
    <div class = "card">
        <div class="card-header">
        <h3 class="card-title">Items</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-sm btn-outline-primary ret"
                        id="exportCSV" disabled>
                    <i class="fa fa-download" aria-hidden="true"></i>
                    Export CSV
                </button>
                <div class="btn-group" id="dropdownContainer">
                    <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>Show/Hide Columns</button>

                <div class="dropdown-menu" id="columnDropdownSelector" style="height: 200px; overflow-y: auto;">
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="barcode" <?= isset($data['columns']->barcode->is_shown)
                                                      ? ($data['columns']->barcode->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Barcode
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="client_name" <?= isset($data['columns']->client_name->is_shown)
                                                      ? ($data['columns']->client_name->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Customer
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="ship_method" <?= isset($data['columns']->ship_method->is_shown)
                                                      ? ($data['columns']->ship_method->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Shipping Method
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="ship_date" <?= isset($data['columns']->ship_date->is_shown)
                                                      ? ($data['columns']->ship_date->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Shipping Date
                    </div>
                    <div class="dropdown-item columnSelector">
                         <input type="checkbox" class="columnCheckbox mr-1"
                                       name="sales_rep" <?= isset($data['columns']->sales_rep->is_shown)
                                                      ? ($data['columns']->sales_rep->is_shown ? 'checked' : '')
                                                        : 'checked' ?>>
                            Sales Rep
                    </div>
                    <div class="dropdown-item columnSelector">
                        <input type="checkbox" class="columnCheckbox mr-1"
                               name="created_date" <?= isset($data['columns']->created_date->is_shown)
                                                   ? ($data['columns']->created_date->is_shown ? 'checked' : '')
                                                     : 'checked' ?>>
                        Created Date
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="horizontal-scroller sticky-top">
            <div class="scroller-content"></div>
        </div>
        <div class="card-body p-0 inventory-container table-responsive">
            <table class="table table-bordered table-striped table-hover paginated" id="product-table"
                   style="border-collapse: separate;" cellspacing="0">
                <thead class="bg-white border">
                    <tr>
                        <th scope="col" nowrap>&#35;</th>
                        <th scope="col" class="header-barcode<?= isset($data['columns']->barcode->is_shown) ? ($data['columns']->barcode->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Barcode</th>
                        <th scope="col" class="header-client_name<?= isset($data['columns']->client_name->is_shown) ? ($data['columns']->client_name->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Customer</th>
                        <th scope="col" class="header-sales_rep<?= isset($data['columns']->sales_rep->is_shown) ? ($data['columns']->sales_rep->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Sales Rep</th>
                        <th scope="col" class="header-ship_method<?= isset($data['columns']->ship_method->is_shown) ? ($data['columns']->ship_method->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Shipping Method </th>
                        <th scope="col" class="header-ship_date<?= isset($data['columns']->ship_date->is_shown) ? ($data['columns']->ship_date->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Shipping Date </th>
                        <th scope="col" class="header-created_date<?= isset($data['columns']->created_date->is_shown) ? ($data['columns']->created_date->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Created date</th>
                        <th scope="col" class="header-modify<?= isset($data['columns']->modify->is_shown) ? ($data['columns']->modify->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Modify</th>
                    </tr>
                </thead>
                <tbody id="product_table_body">
                    <tr>
                        <td colspan="100%">Search an item to see results here.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

<?php $this->load->view('templates/common/footer'); ?>
<?php $this->load->view('templates/common/spinner'); ?>

<script id="workOrderScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/work_orders/work_orders.js"></script>
<script id="editOrderScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/work_orders/edit_orders.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/dynamicTableColumns.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/paginate.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/work_orders/link_build.js"></script>
<script id="workOrderScript" data-base-url="<?= base_url() ?>" type="text/javascript"
        src="<?= base_url() ?>resources/js/work_orders/print.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/work_orders/retain_fields.js"></script>


