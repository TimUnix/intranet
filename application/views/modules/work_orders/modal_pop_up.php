<!-- delete confirmation -->
<div class="modal fade" id="confirmDelete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0275d8">
                <div >
                <h3 style="color:white">Delete Confirmation</h3>
                </div>
            </div>
            <div class="modal-body">
                <div style="text-align:center;">
                    <i class="fas fa-exclamation-triangle fa-5x" style="color:#df4759"></i>
                </div><br>
                <div style="text-align:center;">
                    <h5>Are you sure you want to delete?</h5><br>
                    <button class="btn btn-primary btn-lg" id="delete_row" data-dismiss="modal"> Yes </button>
                    <button class="btn btn-secondary btn-lg" data-dismiss="modal" id="close_modal"> No </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmRemove" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0275d8">
                <div >
                <h3 style="color:white">Delete Confirmation ts</h3>
                </div>
            </div>
            <div class="modal-body">
                <div style="text-align:center;">
                    <i class="fas fa-exclamation-triangle fa-5x" style="color:#df4759"></i>
                </div><br>
                <div style="text-align:center;">
                    <h5>Are you sure you want to delete?</h5><br>
                    <button class="btn btn-primary btn-lg" id="remove_row"> Yes </button>
                    <button class="btn btn-secondary btn-lg" data-dismiss="modal" id="close_modal"> No </button>
                </div>
            </div>
        </div>
    </div>
</div>