<!-- Customer Data -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-row">
                    <button  style="margin-right: 16px" class="btn btn-primary col-2" id="getcustomerdata">Add Order</button>
                    <button class="btn btn-primary col-2" id="btnPrintOrder">Print Order</button>
                </div><hr>
                <div class="alert alert-danger alert-dismissible" id="error_customerdata" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Please fill all the fields to proceed.</h5>
                </div>
                <div class="alert alert-danger alert-dismissible" id="error_email" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Please input correct email address.</h5>
                </div>
                <div class="alert alert-danger alert-dismissible" id="barcode_error" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Barcode Already Exist.</h5>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-row">
                            <div class="col-12" style="text-align:center">
                                <svg id="barcode" style="width: 100%; margin: 25px 0;"></svg>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <strong><span>Barcode: </span></strong>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm" id="txt_input" placeholder="" maxlength="10">
                            </div>
                        </div>
                    </div>
                    <div class="col-6"><br>
                        <div class="form-row">
                            <label for="created" class="col-4">Created</label>
                            <div class="col-8">
                               <span id="created"><?= $data['created_date'] ?></span>
                            </div>

                        </div><br>
                        <div class="form-row">
                            <label for="sales_rep" class="col-4">Sales Rep</label>
                            <div class="col-8">
                               <span id="sales_rep"><?= $data['user_firstname'] ?> <?= $data['user_lastname'] ?></span> 
                            </div>
                        </div><br>
                        <div class="form-row">
                            <label for="ship_date" class="col-4">Ship Date</label>
                            <div class="col-8">
                            <div class="input-group date" id="ship_date" data-target-input="nearest">
                                <input type="text" id="shipdate_" class="form-control form-control-sm datetimepicker-input" placeholder="Select Date" data-target="#ship_date"/>
                                <div class="input-group-append" data-target="#ship_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div></div><br>
                        <div class="form-row">
                            <label for="ship_method" class="col-4">Ship Method</label>
                            <div class="col-8">
                                <select id="ship_method" class="form-control form-control-sm">
                                    <?php foreach ($data['ship_method'] as $ship_method): ?>
                                    <option value="<?= $ship_method->ship_method ?>">
                                        <?= $ship_method->ship_method; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div><br>
                    </div>
                </div><hr>
                <div class="form-row">
                    <label for="client_name" class="col-4">Details</label>
                    <div class="col-12">
                        <textarea class="form-control form-control-sm" id="details_all" rows="4"></textarea>
                    </div>                  
                </div><br>
                <div class="form-row">
                    <div class="col-12">
                         <table class="table table-bordered table-striped table-hover paginated" id="order-table" style="width: 100%; border-collapse: separate;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" nowrap>&#35;</th>
                                    <th scope="col" class="header-chassis<?= isset($data['columns']->chassis->is_shown) ? ($data['columns']->chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
                                    <th scope="col" class="header-qty_chassis<?= isset($data['columns']->qty_chassis->is_shown) ? ($data['columns']->qty_chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Quantity</th>
                                    <th scope="col" class="header-qty_chassis<?= isset($data['columns']->qty_chassis->is_shown) ? ($data['columns']->qty_chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Modify</th>
                                </tr>
                            </thead>
                            <tbody id="add_order_table_body">
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default"  id="close_mod">Close</button>
                <button type="button" class="btn btn-default" id="submit_all_orders">Submit</button>
            </div>
        </div>
    </div>
</div>

<!--edit customer data -->
<div class="modal fade" id="editorder" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-row">
                    
                </div><hr>
                <div class="alert alert-danger alert-dismissible" id="error_customerdata_edit" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Please fill all the fields to proceed.</h5>
                </div>
                <div class="alert alert-danger alert-dismissible" id="error_email_edit" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Please input correct email address.</h5>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-row">
                            <div class="col-12" style="text-align:center">
                                <svg id="barcode_edit" style="width: 100%; margin: 25px 0;"></svg>
                            </div>
                        </div>
                        <div class="form-row" style="display:none;">
                            <div class="col-6">
                                <input type="text" class="form-control form-control-sm current-item-barcode" id="txt_input_edit" placeholder="" maxlength="10">
                            </div>
                        </div>
                    </div>
                    <div class="col-6"><br>
                        <div class="form-row">
                            <label for="created" class="col-4">Created</label>
                            <div class="col-8">
                               <span id="created_edit"><?= $data['created_date'] ?></span>
                            </div>

                        </div><br>
                        <div class="form-row">
                            <label for="sales_rep" class="col-4">Sales Rep</label>
                            <div class="col-8">
                               <span id="sales_rep_edit"><?= $data['user_firstname'] ?> <?= $data['user_lastname'] ?></span> 
                            </div>
                        </div><br>
                        <div class="form-row">
                            <label for="ship_date_edit" class="col-4">Ship Date</label>
                            <div class="col-8">
                            <div class="input-group date" id="ship_date_edit" data-target-input="nearest">
                                <input type="text" class="form-control form-control-sm datetimepicker-input_edit" placeholder="Select Date" data-target="#ship_date_edit"/>
                                <div class="input-group-append" data-target="#ship_date_edit" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div></div><br>
                        <div class="form-row">
                            <label for="ship_method_edit" class="col-4">Ship Method</label>
                            <div class="col-8">
                                <select id="ship_method_edit" class="form-control form-control-sm">
                                    <?php foreach ($data['ship_method'] as $ship_method): ?>
                                    <option value="<?= $ship_method->ship_method ?>">
                                        <?= $ship_method->ship_method; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div><br>
                    </div>
                </div><hr>
                <div class="form-row">
                    <label for="client_name" class="col-4">Details</label>
                    <div class="col-12">
                        <textarea class="form-control form-control-sm" id="details_all_edit" rows="4"></textarea>
                    </div>                 
                </div><br>
                <div class="form-row">
                    <div class="col-12">
                         <table class="table table-bordered table-striped table-hover paginated" id="order-table" style="width: 100%; border-collapse: separate;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" nowrap>&#35;</th>
                                    <th scope="col" class="header-chassis<?= isset($data['columns']->chassis->is_shown) ? ($data['columns']->chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>SKU</th>
                                    <th scope="col" class="header-qty_chassis<?= isset($data['columns']->qty_chassis->is_shown) ? ($data['columns']->qty_chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Quantity</th>
                                    <th scope="col" class="header-qty_chassis<?= isset($data['columns']->qty_chassis->is_shown) ? ($data['columns']->qty_chassis->is_shown ? '' : ' d-none') 
                                                       : '' ?>"nowrap>Modify</th>
                                </tr>
                            </thead>
                            <tbody id="add_order_table_body_edit">
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_modal_edit">Close</button>
                <button type="button" class="btn btn-primary" id="update_all_orders_edit">Update Customer Details</button>
            </div>
        </div>
    </div>
</div>