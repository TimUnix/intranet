<select name="adddate" class='form-control form-control-sm cpu_select<?php echo $cpu_for_edit;  ?>' id='cpu'>
<?php


foreach($cpu as $data){ ?>
    <?php if($cpu_id == "$data->part $data->stepcode $data->speed $data->codename") :   ?>
        <option value="<?= $data->part ?> <?= $data->stepcode ?> <?= $data->speed ?> <?= $data->codename ?>" selected><?= $data->part ?> <?= $data->stepcode ?> <?= $data->speed ?> <?= $data->codename ?></option>;
    <?php else : ?>
        <option value="<?= $data->part ?> <?= $data->stepcode ?> <?= $data->speed ?> <?= $data->codename ?>"><?= $data->part ?> <?= $data->stepcode ?> <?= $data->speed ?> <?= $data->codename ?></option>;
    <?php endif; ?>
    
<?php    
}


?>
</select>
<script>
$(document).ready(function() {
    $('#cpu').select2({
        width: 315,
        tags: true
    });
    $("#cpu").change(function(){
        var datas = {
                            motherboard_ : $("#motherboard").val(),
                            cpu_ : $("#cpu").val(),
                            memory_ : $("#memory").val(),
                            ajax : '1'   
                        };
                        $.ajax({
                            url : `${baseUrl}index.php/work_orders/get_ob_nic_and_pcie_qty`,
                            type : "post",
                            data : datas,
                            success : function(response_ob_nic_pci_slot){
                                $("#ob_nic").text(response_ob_nic_pci_slot.ob_nic);
                                $("#pcie_slot").text(response_ob_nic_pci_slot.pci_slot);
                                    ///$('#motherboard_chassis').html(motherboard);       
                                }
                            });
                            });
 });
</script>