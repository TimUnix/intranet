<html lang="en"><head>

</head>
<body>
<?php $num; $ob_nic_num; foreach ($orders as $index => $order): ?>
</br>
    <div class="wrapper"><div class="h">
        <div class="left h_blocks"><a class="navbar-brand" href="">
            <img align="left" width="200px" src="<?php echo base_url();  ?>images/unixsurplus.png">
        </a>
    </div>
    <div class="center h_blocks">Call Toll Free Support<br>877-UNIX123<br>877-864-9123</div>
    <div class="right h_blocks">
        <img id="bar" src="https://barcode.tec-it.com/barcode.ashx?data=<?=$barcode_svg?>&amp;code=Code128&amp;multiplebarcodes=false&amp;translate-esc=false&amp;unit=Fit&amp;dpi=96&amp;imagetype=Gif&amp;rotation=0&amp;color=%23000000&amp;bgcolor=%23ffffff&amp;codepage=Default&amp;qunit=Mm&amp;quiet=0">
    </div>
    <br>
</div>
<div class="left-el-wrap float-left">
    <div class="left-top-el">
        <ul>Date: <?= $order->created_date ?></ul>
        <ul>Technician: </ul>
    </div>
    <div class="left-middle-el">
        <ul>IPMI</ul><br>
        <ul>Username:  <?php $num = $num + 1; echo $motherboard_brand[$num - 1]; ?></ul><br>
        <ul>Password:  <?php echo $password[$num - 1]; ?></ul><br>
        <ul>Address:______________</ul><br><ul>Mask:  16 | 24  </ul>
        <br><br></div>
        <div class="left-bottom-el">
            <div>UEFI </div>
            <div> LEGACY</div>
        </div>
        <div class="tagId">
            <div>Tag ID:</div>
            <div> </div>
        </div></div>
        <div class="float-right divTable"  style="height:317px;">
            <div class="divTableBody">
                <?php if($order->qty_chassis != 0) : ?>
                    <div class="divTableRow">
                        <div class="f15 bold divTableCell right-align">Chassis: </div>
                        <div class="divTableCell"><?= ($order->chassis != "null") ? $order->qty_chassis."x" : ""; ?></div>
                        <div class="f15 bold divTableCell"><?= ($order->chassis != "null") ? $order->chassis : ""; ?></div>
                        <div class="divTableCell"></div>
                    </div>
                <?php endif; ?>
                <?php if($order->qty_motherboard != 0) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">MotherBoard: </div>
                        <div class="divTableCell"><?= ($order->motherboard != "null") ? $order->qty_motherboard."x" : ""; ?></div>
                        <div class="divTableCell"><?= ($order->motherboard != "null") ? $order->motherboard : ""; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->qty_front_caddies != 0) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Front Caddies: </div>
                        <div class="divTableCell"><?= ($order->front_caddies != "null") ? $order->qty_front_caddies."x" : ""; ?></div>
                        <div class="divTableCell"><?= ($order->front_caddies != "null") ? $order->front_caddies : ""; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->qty_hard_drive_one != 0 && $order->qty_hard_drive_one != "" && $order->qty_hard_drive_one != "null") : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Drives1: </div>
                        <div class="divTableCell"><?= ($order->item_harddrive_one != "null") ? $order->qty_hard_drive_one."x" : "";  ?></div>
                        <div class="divTableCell"><?= ($order->item_harddrive_one != "null") ? $order->item_harddrive_one  : "";?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->item_harddrive_two != "null" && $order->qty_hard_drive_two != 0 && $order->qty_hard_drive_two) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Drives2: </div>
                        <div class="divTableCell"><?= $order->qty_hard_drive_two."x" ?></div>
                        <div class="divTableCell"><?= $order->item_harddrive_two ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->item_harddrive_three != "null" && $order->qty_hard_drive_three != 0 && $order->qty_hard_drive_three) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Drives3: </div>
                        <div class="divTableCell"><?= $order->qty_hard_drive_three."x" ?></div>
                        <div class="divTableCell"><?= $order->item_harddrive_three ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->qty_cpu != 0) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Cpu: </div>
                        <div class="divTableCell"><?= ($order->qty_cpu != "0") ? $order->qty_cpu."x" : ""; ?></div>
                        <div class="divTableCell"><?= ($order->cpu != "null" && $order->cpu != "") ? $order->cpu : ""; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->qty_memory != 0) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Dimm: </div>
                        <div class="divTableCell"><?= ($order->qty_memory != "0") ? $order->qty_memory."x" : ""; ?></div>
                        <div class="divTableCell"><?= ($order->memory != "null" && $order->memory != "") ? $order->memory : ""; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <!-- use this if the other code doesnt work
                    <?php //$ob_nic_num = $ob_nic_num + 1; if($ob_nic[$ob_nic_num - 1] != "") : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Nic: </div>
                        <div class="divTableCell"></div>
                        <div class="divTableCell"><?php //echo $ob_nic[$ob_nic_num - 1]; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php// endif; ?>-->


                <?php if($order->ob_nic != "") : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Nic: </div>
                        <div class="divTableCell"></div>
                        <div class="divTableCell"><?php echo $order->ob_nic; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>


                <?php if($order->pcie != "") : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">PCIE Count: </div>
                        <div class="divTableCell"></div>
                        <div class="divTableCell"><?php echo $order->pcie; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>

                <?php if($order->qty_cntrlr != 0) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">Card: </div>
                        <div class="divTableCell">
                        <?= ($order->qty_cntrlr != 0) ? $order->qty_cntrlr."x" : ""; ?>
                        </div>
                        <div class="divTableCell"><?= ($order->cntrl != "null" && $order->cntrl != "") ? $order->cntrl : ""; ?></div>
                        <div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
                <?php if($order->controllertwo != "") : ?>
                    <div class="divTableRow">
                    <div class="divTableCell right-align">Card: </div>
                    <div class="divTableCell"><?= $order->qty_controllertwo."x" ?></div>
                    <div class="divTableCell"><?= $order->controllertwo ?></div>
                    <div class="divTableCell">______</div>
                </div>
                    <?php endif; ?>
                    <?php if($order->controllerthree != "") : ?>
                    <div class="divTableRow">
                    <div class="divTableCell right-align">Card: </div>
                    <div class="divTableCell"><?= $order->qty_controllerthree."x" ?></div>
                    <div class="divTableCell"><?= $order->controllerthree ?></div>
                    <div class="divTableCell">______</div>
                </div>
                    <?php endif; ?>
                    <?php if($order->qty_psu != 0) : ?>
                    <div class="divTableRow">
                        <div class="divTableCell right-align">PSU: </div><div class="divTableCell"><?= ($order->qty_psu != "0") ? $order->qty_psu."x" : ""; ?></div>
                        <div class="divTableCell"><?= ($order->psu != "null" && $order->psu != "") ? $order->psu : ""; ?></div><div class="divTableCell">______</div>
                    </div>
                <?php endif; ?>
        </div>
    </div>
    <div class="bottom-text">UNIXSurplus,Inc. - Proudly Assembled and Tested in Santa Clara, CA - <span class="bold">Important: Breaking or removing this label voids the warranty.</span>
</div>
</div>
<style>
divTable{
	display: table;
	width: auto;
        border-style: solid;
}
.divTableRow {
	display: table-row;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
}
.divTableCell, .divTableHead {
/*	border: 1px solid #999999;*/
	display: table-cell;
	padding: 3px 10px;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
	font-weight: bold;
}
.divTableFoot {
	background-color: #EEE;
	display: table-footer-group;
	font-weight: bold;
}
.divTableBody {
	display: table-row-group;
 border: 1px solid black;
 width: 500px;
    height: 313px;

}
.wrapper{
    width: 835px;
    height: 510px;
 border: 1px solid black;
}
.h{
    width: auto;
    height: 70px;
    box-sizing: border-box;
    display: block;
    position: relative;
}
.h_blocks{
    width: auto !important;
    height: 40px !important;
    margin-right: 30px;
    box-sizing: border-box;
    display: inline-block;
    position: relative;


}
ul{
    padding: 6px 10px 0px 10px;
    margin: 0px;
}
.bottom-text{
    width: 600px;
    height: 20px;
    margin: 21px auto 0px auto;
    text-align: center;
}
.inline-block {
    display: inline;
/*        margin-top: 13px;
    padding: 10px;
    border-style: solid;*/
}
.float-right{
   margin: 20px 0px 0px 222px;
    border: 1px solid black;
    width: 600px;
/*    height: 350px;*/
    height: auto;

}
.float-left{
    float: left;
    
}
.clear-both{
   float: clear-both; 
}
.left-middle-el{
    margin: 10px 0px 10px 0px;
 border: 1px solid black;
}
.left-top-el{
    margin: 10px 0px 10px 0px;
 border: 1px solid black;
           padding-bottom: 30px;

}
.left-bottom-el{
    margin: 10px 0px 10px 0px;
 border: 1px solid black;

}

.tagId{
    margin: 10px 0px 10px 0px;
    border: 1px solid black;
    text-align: left !important;
    height: auto;
    padding: 5px 5px 5px 5px;
}

.left-el-wrap{
    padding: 10px;
    text-align: center;

}
.f15{
    font-size: 15pt;
}
.bold{
    font-weight: bold;
}
.left{
        margin: 10px 10px 10px 10px;

}
.center{
    margin: 0px 0px 10px 155px;
    text-align: center;
    font-weight: bold;
}

.right{
    margin: 10px 10px 10px 10px;
    float: right;
}
#bar{
    height: 75px;
}

.right-align{
    text-align: right;
    width: 100px;
}
</style>
<?php endforeach; ?></body></html>