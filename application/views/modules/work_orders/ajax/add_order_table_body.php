<?php if(!empty($items)) { foreach($items as $index => $item): ?>
<tr>
	<td scope="col"><?= $index + 1 ?></td>
    <?php if($item->chassis == null || $item->chassis == ''){ ?>
    <td class="chassis" scope="col" data-value="<?= $item->motherboard ?> <?= $item->cpu ?> <?= $item->memory ?>"<?= isset($columns->motherboard->is_shown) 
                                         ? ($columns->motherboard->is_shown ? '' : ' d-none') 
                                           : '' ?>><?= $item->motherboard ?> <?= $item->cpu ?> <?= $item->memory ?></td>
    <td scope="col" data-value="<?= $item->qty_motherboard ?>"class="<?= isset($columns->qty_motherboard->is_shown) 
                                         ? ($columns->qty_motherboard->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap><?= $item->qty_motherboard ?></td>
    <td nowrap> 
    <button type="button" class="btn btn-link btn-lg edit-mb" data-toggle="modal" data-target="modalMotherboard_edit">
                <i class="fas fa-pen edit-mb" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn btn-link btn-lg removerow" data-toggle="modal" data-target="#confirmRemove">
                <i class="fas fa-trash" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-link btn-lg print_order_row" data-toggle="modal" data-target="">
                <i class="fas fa-print" aria-hidden="true"></i>
            </button></td>

    <?php } else{ ?>                                 
	<td scope="col" data-value="<?= $item->chassis ?>"class="chassis item-chassis<?= isset($columns->chassis->is_shown) 
                                         ? ($columns->chassis->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap><?= $item->chassis ?> </td>
    <td scope="col" data-value="<?= $item->qty_chassis ?>"class="item-qty_chassis<?= isset($columns->qty_chassis->is_shown) 
                                         ? ($columns->qty_chassis->is_shown ? '' : ' d-none') 
                                           : '' ?>" nowrap><?= $item->qty_chassis ?></td>
    <td>
        <button type="button" class="btn btn-link btn-lg edit-item" data-toggle="modal" data-target="modalChassis_edit">
                <i class="fas fa-pen edit-item" aria-hidden="true"></i>
        </button>
        <button type="button" class="btn btn-link btn-lg removerow" data-toggle="modal" data-target="#confirmRemove">
                <i class="fas fa-trash" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-link btn-lg print_order_row" data-toggle="modal" data-target="">
                <i class="fas fa-print" aria-hidden="true"></i>
        </button>
    </td>
    <?php } ?> 

    <!-- do not delete -->
    <td scope="col" data-value="<?= $item->backplane ?> "class="item-backplane<?= isset($columns->backplane->is_shown) ? ($columns->backplane->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->backplane ?></td>
     <td scope="col" data-value="<?= $item->nodes ?> "class="item-nodes<?= isset($columns->nodes->is_shown) ? ($columns->nodes->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->nodes ?></td>
     <td scope="col" data-value="<?= $item->rearbays ?> "class="item-rearbays<?= isset($columns->rearbays->is_shown) ? ($columns->rearbays->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->rearbays ?></td>
    <td scope="col" data-value="<?= $item->drivebays ?> "class="item-drivebays<?= isset($columns->drivebays->is_shown) ? ($columns->drivebays->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->drivebays ?></td>
     <td scope="col" data-value="<?= $item->rff ?> "class="item-rff<?= isset($columns->rff->is_shown) ? ($columns->rff->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->rff ?></td>
    <td scope="col" data-value="<?= $item->ff ?> "class="item-ff<?= isset($columns->ff->is_shown) ? ($columns->ff->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->ff ?></td>
    <td scope="col" data-value="<?= $item->psu_slots ?> "class="item-psu_slots<?= isset($columns->psu_slots->is_shown) ? ($columns->psu_slots->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->psu_slots ?></td>
    <td scope="col" data-value="<?= $item->bin ?> "class="item-bin<?= isset($columns->bin->is_shown) ? ($columns->bin->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->bin ?></td>
    <td scope="col" data-value="<?= $item->front_caddies ?> "class="item-front_caddies<?= isset($columns->front_caddies->is_shown) ? ($columns->front_caddies->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->front_caddies ?></td>
     <td scope="col" data-value="<?= $item->qty_front_caddies ?> "class="item-qty_front_caddies<?= isset($columns->qty_front_caddies->is_shown) ? ($columns->qty_front_caddies->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_front_caddies ?></td>
     <td scope="col" data-value="<?= $item->rear_caddies ?> "class="item-rear_caddies<?= isset($columns->rear_caddies->is_shown) ? ($columns->rear_caddies->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->rear_caddies ?></td>
    <td scope="col" data-value="<?= $item->qty_rear_caddies ?> "class="item-qty_rear_caddies<?= isset($columns->qty_rear_caddies->is_shown) ? ($columns->qty_rear_caddies->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_rear_caddies ?></td>
    <td scope="col" data-value="<?= $item->item_harddrive_one ?> "class="item-item_harddrive_one<?= isset($columns->item_harddrive_one->is_shown) ? ($columns->item_harddrive_one->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->item_harddrive_one ?></td>
    <td scope="col" data-value="<?= $item->item_harddrive_two ?> "class="item-item_harddrive_two<?= isset($columns->item_harddrive_two->is_shown) ? ($columns->item_harddrive_two->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->item_harddrive_two ?></td>
    <td scope="col" data-value="<?= $item->item_harddrive_three ?> "class="item-item_harddrive_three<?= isset($columns->item_harddrive_three->is_shown) ? ($columns->item_harddrive_three->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->item_harddrive_three ?></td>
     <td scope="col" data-value="<?= $item->qty_hard_drive_one ?> "class="item-qty_hard_drive_one<?= isset($columns->qty_hard_drive_one->is_shown) ? ($columns->qty_hard_drive_one->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_hard_drive_one ?></td>
     <td scope="col" data-value="<?= $item->qty_hard_drive_two ?> "class="item-qty_hard_drive_two<?= isset($columns->qty_hard_drive_two->is_shown) ? ($columns->qty_hard_drive_two->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_hard_drive_two ?></td>
    <td scope="col" data-value="<?= $item->qty_hard_drive_three ?> "class="item-qty_hard_drive_three<?= isset($columns->qty_hard_drive_three->is_shown) ? ($columns->qty_hard_drive_three->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_hard_drive_three ?></td>
    <td scope="col" data-value="<?= $item->motherboard ?> "class="item-motherboard<?= isset($columns->motherboard->is_shown) ? ($columns->motherboard->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->motherboard ?></td>
    <td scope="col" data-value="<?= $item->qty_motherboard ?> "class="item-qty_motherboard<?= isset($columns->qty_motherboard->is_shown) ? ($columns->qty_motherboard->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_motherboard ?></td>
    <td scope="col" data-value="<?= $item->cpu ?> "class="item-cpu<?= isset($columns->cpu->is_shown) ? ($columns->cpu->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->cpu ?></td>
     <td scope="col" data-value="<?= $item->qty_cpu ?> "class="item-qty_cpu<?= isset($columns->qty_cpu->is_shown) ? ($columns->qty_cpu->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_cpu ?></td>
     <td scope="col" data-value="<?= $item->memory ?> "class="item-memory<?= isset($columns->memory->is_shown) ? ($columns->memory->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->memory ?></td>
    <td scope="col" data-value="<?= $item->qty_memory ?> "class="item-qty_memory<?= isset($columns->qty_memory->is_shown) ? ($columns->qty_memory->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_memory ?></td>
    <td scope="col" data-value="<?= $item->cntrl ?> "class="item-cntrl<?= isset($columns->cntrl->is_shown) ? ($columns->cntrl->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->cntrl ?></td>
    <td scope="col" data-value="<?= $item->controllertwo ?> "class="item-controllertwo<?= isset($columns->controllertwo->is_shown) ? ($columns->controllertwo->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->controllertwo ?></td>
    <td scope="col" data-value="<?= $item->controllerthree ?> "class="item-controllerthree<?= isset($columns->controllerthree->is_shown) ? ($columns->controllerthree->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->controllerthree ?></td>
     <td scope="col" data-value="<?= $item->qty_cntrlr ?> "class="item-qty_cntrlr<?= isset($columns->qty_cntrlr->is_shown) ? ($columns->qty_cntrlr->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_cntrlr ?></td>
     <td scope="col" data-value="<?= $item->qty_controllertwo ?> "class="item-qty_controllertwo<?= isset($columns->qty_controllertwo->is_shown) ? ($columns->qty_controllertwo->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_controllertwo ?></td>
    <td scope="col" data-value="<?= $item->qty_controllerthree ?> "class="item-qty_controllerthree<?= isset($columns->qty_controllerthree->is_shown) ? ($columns->qty_controllerthree->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_controllerthree ?></td>
    <td scope="col" data-value="<?= $item->psu ?> "class="item-psu<?= isset($columns->psu->is_shown) ? ($columns->psu->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->psu ?></td>
    <td scope="col" data-value="<?= $item->qty_psu ?> "class="item-qty_psu<?= isset($columns->qty_psu->is_shown) ? ($columns->qty_psu->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_psu ?></td>
    <td scope="col" data-value="<?= $item->mounting ?> "class="item-mounting<?= isset($columns->mounting->is_shown) ? ($columns->mounting->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->mounting ?></td>
    <td scope="col" data-value="<?= $item->qty_mounting ?> "class="item-qty_mounting<?= isset($columns->qty_mounting->is_shown) ? ($columns->qty_mounting->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->qty_mounting ?></td>
    <td scope="col" data-value="<?= $item->warranty ?> "class="item-warranty<?= isset($columns->warranty->is_shown) ? ($columns->warranty->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->warranty ?></td>
    <td scope="col" data-value="<?= $item->os ?> "class="item-os<?= isset($columns->os->is_shown) ? ($columns->os->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->os ?></td>
    <td scope="col" data-value="<?= $item->work_order_id ?> "class="item-work_order_id<?= isset($columns->work_order_id->is_shown) ? ($columns->work_order_id->is_shown ? '' : ' d-none') : '' ?>" nowrap style = "display: none;"><?= $item->work_order_id ?></td>


</tr>
<?php endforeach; }?>
