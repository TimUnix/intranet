<html lang="en">
    <?php $this->load->view("modules/work_orders/ajax/print_partials/head"); ?>
    <body>
        <div class="container">
            <?php $this->load->view("modules/work_orders/ajax/print_partials/order_meta_info"); ?>
            <?php if (isset($order->chassis) && trim($order->chassis) !== "null" && trim($order->chassis) !== ""): ?>
                <h2 class="text-center mt-4 border">CHASSIS</h2>
                <div class="row">
                    <span class="col-2"><strong>Chassis:</strong></span>
                    <span class="col-4"><?= $order->chassis ?></span>
                    <span class="col-2"><strong>Qty:</strong></span>
                    <span class="col-4"><?= $order->qty_chassis ?></span>
                </div>
                <div class="row">
                    <span class="col-2"><strong>Backplane:</strong></span>
                    <span class="col-4"><?= $order->backplane ?></span>
                    <span class="col-2"><strong>Nodes:</strong></span>
                    <span class="col-4"><?= $order->nodes ?></span>
                </div>
                <div class="row">
                    <span class="col-2"><strong>Drive Bays:</strong></span>
                    <span class="col-4"><?= $order->drivebays ?></span>
                    <span class="col-2"><strong>PSU Slots:</strong></span>
                    <span class="col-4"><?= $order->psu_slots ?></span>
                </div>
                <div class="row">
                    <span class="col-2"><strong>Rear Bays:</strong></span>
                    <span class="col-4"><?= $order->rearbays ?></span>
                    <span class="col-2"><strong>Bin:</strong></span>
                    <span class="col-4"><?= $order->bin ?></span>
                </div>
                <h2 class="text-center mt-4 border">HARD DRIVES</h2>
                <?php if($order->qty_front_caddies != 0) : ?>
                    <div class="row">
                        <span class="col-2"><strong>Front Caddies:</strong></span>
                        <span class="col-4">
                            <?= isset($order->front_caddies) && trim($order->front_caddies) !== "null" ?
                                $order->front_caddies : "" ?>
                        </span>
                        <span class="col-2"><strong>Qty:</strong></span>
                        <span class="col-4">
                            <?= isset($order->front_caddies) && trim($order->front_caddies) !== "null" ?
                                $order->qty_front_caddies : "" ?>
                        </span>
                    </div>
                <?php endif;?>
                <?php if($order->qty_rear_caddies != 0) : ?>
                <div class="row">
                    <span class="col-2"><strong>Rear Caddies:</strong></span>
                    <span class="col-4">
                        <?= isset($order->rear_caddies) && trim($order->rear_caddies) !== "null" ?
                            $order->rear_caddies : "" ?>
                    </span>
                    <span class="col-2"><strong>Qty:</strong></span>
                    <span class="col-4">
                        <?= isset($order->rear_caddies) && trim($order->rear_caddies) !== "null" ?
                            $order->qty_rear_caddies : "" ?>
                    </span>
                </div>
                <?php endif;?>
                <?php if($order->qty_hard_drive_one != 0) : ?>
                    <div class="row">
                        <span class="col-2"><strong>Drives:</strong></span>
                        <span class="col-4">
                            <?= isset($order->item_harddrive_one) && trim($order->item_harddrive_one) !== "null" ?
                                $order->item_harddrive_one : "" ?>
                        </span>
                        <span class="col-2"><strong>Qty:</strong></span>
                        <span class="col-4">
                            <?= isset($order->item_harddrive_one) && trim($order->item_harddrive_one) !== "null" ?
                                $order->qty_hard_drive_one : "" ?>
                        </span>
                    </div>
                <?php endif;?>
                <?php if (isset($order->item_harddrive_two) && trim($order->item_harddrive_two !== "null")
                          && trim($order->item_harddrive_two) !== ""): ?>
                    <div class="row">
                        <span class="col-2"><strong>Drives:</strong></span>
                        <span class="col-4"><?= $order->item_harddrive_two ?></span>
                        <span class="col-2"><strong>Qty:</strong></span>
                        <span class="col-4"><?= $order->qty_hard_drive_two ?></span>
                    </div>
                <?php endif; ?>
                <?php if (isset($order->item_harddrive_three) && trim($order->item_harddrive_three != "null")
                          && trim($order->item_harddrive_three) != ""): ?>
                    <div class="row">
                        <span class="col-2"><strong>Drives:</strong></span>
                        <span class="col-4"><?= $order->item_harddrive_three ?></span>
                        <span class="col-2"><strong>Qty:</strong></span>
                        <span class="col-4"><?= $order->qty_hard_drive_three ?></span>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <h2 class="text-center mt-4 border">MOTHERBOARD</h2>
            <div class="row">
                <span class="col-2"><strong>Motherboard:</strong></span>
                <span class="col-4">
                    <?= isset($order->motherboard) && trim($order->motherboard) !== "null" ?
                        $order->motherboard : "" ?>
                </span>
                <span class="col-2"><strong>Qty:</strong></span>
                <span class="col-4">
                    <?= isset($order->motherboard) && trim($order->motherboard) !== "null" ?
                        $order->qty_motherboard : "" ?>
                </span>
            </div>
            <?php if($order->qty_cpu != 0) : ?>
                <div class="row">
                    <span class="col-2"><strong>CPU:</strong></span>
                    <span class="col-4">
                        <?= isset($order->cpu) && trim($order->cpu) !== "null" ? $order->cpu : "" ?>
                    </span>
                    <span class="col-2"><strong>Qty:</strong></span>
                    <span class="col-4">
                        <?= isset($order->cpu) && trim($order->cpu) !== "null" ? $order->qty_cpu : "" ?>
                    </span>
                </div>
                <?php endif; ?>
                <?php if($order->qty_memory != 0) : ?>
                    <div class="row">
                        <span class="col-2"><strong>Memory:</strong></span>
                        <span class="col-4">
                            <?= isset($order->memory) && trim($order->memory) !== "null" ? $order->memory : "" ?>
                        </span>
                        <span class="col-2"><strong>Qty:</strong></span>
                        <span class="col-4">
                            <?= isset($order->memory) && trim($order->memory) !== "null" ? $order->qty_memory : "" ?>
                        </span>
                    </div>
            <?php endif; ?>

            <?php if($order->ob_nic != "") : ?>
            <div class="row">
                        <span class="col-2"><strong>OB NIC:</strong></span>
                        <span class="col-4">
                            <?= isset($order->ob_nic) && trim($order->ob_nic) !== "null" ? $order->ob_nic : "" ?>
                        </span>
                        
                    </div>
                    <?php endif; ?>

                    <?php if($order->pcie != 0) : ?>
            <div class="row">
                        <span class="col-2"><strong>PCIE Count:</strong></span>
                        <span class="col-4">
                            <?= isset($order->pcie) && trim($order->pcie) !== "null" ? "x" . $order->pcie : "" ?>
                        </span>
                        
                    </div>
                    <?php endif; ?>

            <?php if($order->qty_cntrlr != 0) : ?>
                <div class="row">
                    <span class="col-2"><strong>Slots:</strong></span>
                    <span class="col-4">
                        <?= isset($order->cntrl) && trim($order->cntrl) !== "null" ? $order->cntrl : "" ?>
                    </span>
                    <span class="col-2"><strong>Qty:</strong></span>
                    <span class="col-4">
                        <?= isset($order->cntrl) && trim($order->cntrl) !== "null" ? $order->qty_cntrlr : "" ?>
                    </span>
                </div>
            <?php endif; ?>
            <?php if (isset($order->controllertwo) && trim($order->controllertwo !== "null")
                      && trim($order->controllertwo) != ""): ?>
                <div class="row">
                    <span class="col-2"></span>
                    <span class="col-4"><?= $order->controllertwo ?></span>
                    <span class="col-2"><strong>Qty:</strong></span>
                    <span class="col-4"><?= $order->qty_controllertwo ?></span>
                </div>
            <?php endif; ?>
            <?php if (isset($order->controllerthree) && trim($order->controllerthree !== "null")
                      && trim($order->controllerthree) != ""): ?>
                <div class="row">
                    <span class="col-2"></span>
                    <span class="col-4"><?= $order->controllerthree ?></span>
                    <span class="col-2"><strong>Qty:</strong></span>
                    <span class="col-4"><?= $order->qty_controllerthree ?></span>
                </div>
            <?php endif; ?>
            <div class="row">
                <span class="col-2"><strong>Mounting:</strong></span>
                <span class="col-4">
                    <?= isset($order->mounting) && trim($order->mounting) !== "null" ? $order->mounting : "" ?>
                </span>
                <span class="col-2"><strong>Qty:</strong></span>
                <span class="col-4">
                    <?= isset($order->mounting) && trim($order->mounting) !== "null" ? $order->qty_mounting : "" ?>
                </span>
            </div>
            <div class="row mt-4">
                <span class="col-2"><strong>Warranty:</strong></span>
                <span class="col-21">
                    <?= isset($order->warranty) && trim($order->warranty) !== "null" ? $order->warranty : "" ?>
                </span>
            </div>
            <div class="row">
                <span class="col-2"><strong>OS/Notes:</strong></span>
                <span class="col-21">
                    <?= isset($order->os) && trim($order->os) !== "null" ? $order->os : "" ?>
                </span>
            </div>
            <div class="row mt-5">
                <span class="col-2"><strong>Processor:</strong></span>
                <span class="col-4">___/___/___</span>
                <span class="col-2"><strong>Inventory:</strong></span>
                <span class="col-4">___/___/___</span>
            </div>
            <div class="row">
                <span class="col-2"><strong>Technician:</strong></span>
                <span class="col-4">___/___/___</span>
                <span class="col-2"><strong>Shipping:</strong></span>
                <span class="col-4">___/___/___</span>
            </div>
        </div>
    </body>
</html>
