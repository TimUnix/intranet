<h1 class="text-center mt-4 border">Work Orders</h1>

<div class="row mt-5">
    
<div class="container">
  <div class="row">
  <div class="col-sm-4">
                <div class="mb-1">
                    <div><strong>Created:</strong></div>
                    <div><?= $created ?></div>
                </div>
                <div class="mb-1">
                    <div><strong>Ship By:</strong></div>
                    <div><?= $ship_date ?></div>
                </div>
                <div class="mb-1">
                    <div><strong>Sales Rep:</strong></div>
                    <div><?= $sales_rep ?></div>
                </div>
                <div>
                    <div><strong>Ship Method:</strong></div>
                    <div><?= $ship_method ?></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div>
                <div><strong>Details</strong></div>
                    <div><?= $client_name ?></div>
                    <div>
                        <?php foreach ($client_address as $address_line): ?>
                            <p class="mb-1"><?= $address_line ?></p>
                        <?php endforeach; ?>
                    </div>
                    <div><?= $client_contact ?></div>
                        <div><?= $client_email ?></div>
                    
                </div>
                
                
            </div>
            <div class="col-sm-4">
                        </br>
                        </br>
                        </br>
                <div class="barcode">
                    <img id="bar" src="https://barcode.tec-it.com/barcode.ashx?data=<?=$barcode_code?>&amp;code=Code128&amp;multiplebarcodes=false&amp;translate-esc=false&amp;unit=Fit&amp;dpi=96&amp;imagetype=Gif&amp;rotation=0&amp;color=%23000000&amp;bgcolor=%23ffffff&amp;codepage=Default&amp;qunit=Mm&amp;quiet=0">
    
                    
                </div>
            </div>
  </div>
</div>
    
</div>
