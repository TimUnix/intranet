<head>
    <title>Work Orders Overview</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/workorders_print.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
