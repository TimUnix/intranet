<!-- Include Chassis -->
<div class="modal fade" id="modalChassis" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="alert alert-danger alert-dismissible" id="error_maxfield" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
            </div>
            <div class="alert alert-danger alert-dismissible" id="caddies_error" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Quantity exceeds number of bays.</h5>
            </div>
            <div id="error_input"></div>
                <div class="form-row justify-content-between">
                    <button class="btn btn-secondary btn-sm col-3" data-toggle="modal" id="remove_chassis">Motherboard Only</button>
                    <div class="col-3"></div>
                    <div class="col-2"><strong><span>Build ID</span></strong></div>
                    <div class="col-4">
                        <select id="build_id" class="form-control form-control-sm">
                            <option value="null">Select Build ID</option>
                            <?php foreach ($data['build_id'] as $build_id): ?>
                                 <option value="<?= $build_id->build_id ?>">
                                <?= $build_id->build_id; ?>
                            </option>
                            <?php endforeach; ?> 
                        </select>
                    </div>
                </div><hr>
                <div><span id="work_order_id_add_ch" style="display:none;"></span></div>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>CHASSIS</strong></div><br>
                <div class="form-row">
                    <label for="chassis" class="col-2">Chassis</label>
                    <div class="col-7">
                        <select id="chasis" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach($chassis as $chassis):?>
                            <option value="<?= $chassis->part_number; ?>"><?= $chassis->part_number; ?></option>
                            <?php endforeach ?>
                         </select>
                    </div>
                    <label for="qty_chassis" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_chassis">
                    </div>

                </div>
                <div>
                <div class="form-row">
                    <strong class="col-3">Backplane:</strong><span class="col-3" id="backplane"></span>
                    <strong class="col-3">Nodes:</strong> <span class="col-3" id="nodes"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Drive Bays:</strong><span class="col-3" id="drivebays"></span>
                    <strong class="col-3">PSU Slots:</strong> <span class="col-3" id="psuchassis"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Rear Bays:</strong> <span class="col-3" id="rearbays"></span>
                    <strong class="col-3">Bin:</strong> <span class="col-3" id="bin"></span>
                </div>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>HARD DRIVES</strong></div><br>
                <div class="form-row">
                    <label for="front_caddies" class="col-2">Front Caddies</label>
                    <div class="col-5">
                        <select id ="front_caddies" name="front_caddies" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach($front_caddies as $fc): ?>
                                <option value="<?= $fc->fc_name;  ?>"><?= $fc->fc_name;  ?></option>
                                <?php endforeach;  ?>
                        </select>
                    </div>
                    <label for="front_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_front_caddies">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong><span class="col-1" id="front_caddies_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="rear_caddies" class="col-2">Rear Caddies</label>
                    <div class="col-5">
                        <select id ="rear_caddies" name="rear_caddies" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach($rear_caddies as $rc): ?>
                                <option value="<?= $rc->rc_name;  ?>"><?= $rc->rc_name;  ?></option>
                                <?php endforeach;  ?>
                        </select>
                    </div>
                    <label for="rear_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_rear_caddies">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="rear_caddies_total"></span></strong>
                </div><br>
                <div class="form-row drives_list">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives">+</button>
                    </div>      
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-5 clonedInput" name="drives_name[]">
                        <select id="item_harddrive_one" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-2 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="qty_hard_drive_one">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="drives1_total"></span></strong>
                </div><br>
                <div class="form-row drives_list_copy" style="display:none;">
                     <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives2">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-5 clonedInput" name="drives_name[]">
                        <select id="item_harddrive_two" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="qty_hard_drive_two">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="drives2_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row drives_list_copy2" style="display:none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives3">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-5 clonedInput" name="drives_name[]">
                        <select id="item_harddrive_three" class="form-control form-control-sm">
                            <option value="null"></option>
                           <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="qty_hard_drive_three">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="drives3_total"></span></strong>
                     <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong></div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-5" id="motherboard_chassis">
                        <select id="motherboard" class="form-control form-control-sm motherboard_select">
                            
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_motherboard">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong><span class="col-1" id="motherboard_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-5" id="motherboard_cpu">
                        <select id="cpu" class="form-control form-control-sm cpu_select">
                            
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_cpu">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="cpu_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-5" id="motherboard_memory">
                       <select id="memory" class="form-control form-control-sm memory_select">

                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_memory">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="memory_total"></span></strong>
                </div><br>


                <div class="form-row">
                   <label for="ob_nic" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic"></span>
                    </div>
                </div>
                <div class="form-row">
                   <label for="pcie_slot" class="col-2">PCIE SLOTS</label>
                    <div class="col-7">
                       <span id="pcie_slot"></span>
                    </div>
                </div>
                <br>

                 <div class="form-row slot_list">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrl">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlr">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="slot1_total"></span></strong>
                </div><br>
                
                <div class="form-row slot_list_copy" style="display:none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller2">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrltwo">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrtwo">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="slot2_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot2"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row slot_list_copy2" style="display: none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller3">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrlthree">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrthree">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="slot3_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot3"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-5">
                        <input type="text" class="form-control form-control-sm" id="psu">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_psu">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="psu_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-5">
                        <select id="mounting" class="form-control form-control-sm">
                            <?php foreach ($data ['mounting'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_mounting">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="mounting_total"></span></strong>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty_ch" class="form-control form-control-sm">
                            <?php foreach ($data ['warranty'] as $warranties): ?>
                            <option value="<?= $warranties->name ?>"
                                <?= $warranties->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranties->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="os" class="col-2">Notes</label>
                    <div class="col-10">
                    <textarea id="os" rows="2" class=" form-control form-control-sm"></textarea></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_modal">Back</button>
                <button type="button" class="btn btn-primary" id="add_order_chassis">Add</button>
            </div>
        </div>
    </div>
</div>

<!--edit include chassis -->
<div class="modal fade" id="modalChassis_edit" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="alert alert-danger alert-dismissible" id="error_maxfield_edit" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
            </div>
            <div class="alert alert-danger alert-dismissible" id="caddies_error_edit" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <h5><i class="icon fas fa-check"></i>Quantity exceeds number of bays.</h5>
            </div>
            <div id="error_input_edit"></div>
                <div class="form-row justify-content-between">
                    <div class="col-6"></div>
                    <div class="col-2"><strong><span>Build ID</span></strong></div>
                    <div class="col-4">
                        <select id="build_id_edit" class="form-control form-control-sm">
                            <option value="null">Select Build ID</option>
                            <?php foreach ($data['build_id'] as $build_id): ?>
                                 <option value="<?= $build_id->build_id ?>">
                                <?= $build_id->build_id; ?>
                            </option>
                            <?php endforeach; ?> 
                        </select>
                    </div>
                    <div><span id="work_order_id" style="display:none;"></span></div>
                </div><hr>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>CHASSIS</strong></div><br>
                <div class="form-row">
                    <label for="chassis-edit" class="col-2">Chassis</label>
                    <div class="col-7">
                        <select id="chassis_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['chassis'] as $chassises): ?>
                                 <option value="<?= $chassises->part_number ?>">
                                <?= $chassises->part_number; ?>
                            </option>
                            <?php endforeach ?>
                         </select>
                    </div>
                    <label for="qty_chassis" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_chassis_edit">
                    </div>

                </div>
                <div>
                <div class="form-row">
                    <strong class="col-3">Backplane:</strong><span class="col-3" id="backplane_edit"></span>
                    <strong class="col-3">Nodes:</strong> <span class="col-3" id="nodes_edit"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Drive Bays:</strong><span class="col-3" id="drivebays_edit"></span>
                    <strong class="col-3">PSU Slots:</strong> <span class="col-3" id="psuchassis_edit"></span>
                </div>
                <div class="form-row">
                    <strong class="col-3">Rear Bays:</strong> <span class="col-3" id="rearbays_edit"></span>
                    <strong class="col-3">Bin:</strong> <span class="col-3" id="bin_edit"></span>
                </div>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>HARD DRIVES</strong></div><br>
                <div class="form-row">
                    <label for="front_caddies" class="col-2">Front Caddies</label>
                    <div class="col-5">
                        <select id ="front_caddies_edit" name="front_caddies_edit" class="form-control form-control-sm">
                            <option class="front_caddies_display"></option>
                        </select>
                    </div>
                    <label for="front_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_front_caddies_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_front_caddies_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="rear_caddies" class="col-2">Rear Caddies</label>
                    <div class="col-5">
                        <select id ="rear_caddies_edit" name="rear_caddies_edit" class="form-control form-control-sm">
                           <option class="rear_caddies_display"></option>
                        </select>
                    </div>
                    <label for="rear_caddies_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_rear_caddies_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_rear_caddies_edit_total"></span></strong>
                </div><br>
                <div class="form-row drives_list_edit">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives_edit">+</button>
                    </div>      
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-5 clonedInput" name="drives_name[]">
                        <select id="item_harddrive_one_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-2 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="qty_hard_drive_one_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_hard_drive_one_edit_total"></span></strong>
                </div><br>
                <div class="form-row drives_list_copy_edit">
                     <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives2_edit">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-5 clonedInput" name="drives_name[]">
                        <select id="item_harddrive_two_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                            <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                         <?php endforeach; ?>   
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="qty_hard_drive_two_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_hard_drive_two_edit_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives2_edit"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row drives_list_copy2_edit">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_drives3_edit">+</button>
                    </div>    
                    <label for="Drives" class="col-1">Drives</label>
                    <div class="col-5 clonedInput" name="drives_name[]">
                        <select id="item_harddrive_three_edit" class="form-control form-control-sm">
                            <option value="null"></option>
                           <?php foreach ($data['drives'] as $drives): ?>
                            <option value="<?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>">
                                <?= $drives->sku ?> <?= $drives->brand ?> <?= $drives->ff ?> <?= $drives->size ?>GB <?= $drives->type ?> <?= $drives->rpm ?> <?= $drives->speed ?> <?= $drives->interface ?> <?= $drives->series ?> <?= $drives->con ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="drives_qty" class="col-1">Qty</label>
                    <div class="col-1 clonedInput2" id="drives_qty[]" name="drives_qty[]">
                        <input type="number" class="form-control form-control-sm" id="qty_hard_drive_three_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_hard_drive_three_edit_total"></span></strong>
                     <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_drives3_edit"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong></div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-5" id="motherboard_chassis_edit">
                        <select id="motherboard_edit" class="form-control form-control-sm motherboard_select">
                           <option class="motherboard_edit"></option>
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_motherboard_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_motherboard_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-5" id="motherboard_cpu_edit">
                        <select id="cpu_edit" class="form-control form-control-sm cpu_select">
                            <option class="cpu_edit"></option>
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_cpu_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cpu_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-5" id="motherboard_memory_edit">
                       <select id="memory_edit" class="form-control form-control-sm memory_select">
                            <option class="memory_edit"></option>
                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_memory_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_memory_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                   <label for="ob_nic_edit" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic_edit"></span>
                    </div>
                </div>
                <div class="form-row">
                   <label for="pcie_slot_edit" class="col-2">PCIE SLOTS</label>
                    <div class="col-7">
                       <span id="pcie_slot_edit"></span>
                    </div>
                </div></br>
                 <div class="form-row slot_list_edit">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_edit">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrl_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlr_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlr_edit_total"></span></strong>
                </div><br>
                
                <div class="form-row slot_list_copy_edit">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller2_edit">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrltwo_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrtwo_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlrtwo_edit_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot2_edit"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row slot_list_copy2_edit">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller3_edit">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrlthree_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrthree_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlrthree_edit_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot3_edit"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-5">
                        <input type="text" class="form-control form-control-sm" id="psu_edit">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_psu_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_psu_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-5">
                        <select id="mounting_edit" class="form-control form-control-sm">
                            <?php foreach ($data ['mounting'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_mounting_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_mounting_edit_total"></span></strong>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty_ch_edit" class="form-control form-control-sm">
                            <?php foreach ($data ['warranty'] as $warranties): ?>
                            <option value="<?= $warranties->name ?>"
                                <?= $warranties->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranties->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="os" class="col-2">Notes</label>
                    <div class="col-10">
                    <textarea id="os_edit" rows="2" class=" form-control form-control-sm"></textarea></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_modal">Back</button>
                <button type="button" class="btn btn-primary" id="edit_order_chassis">Update</button>
            </div>
        </div>
    </div>
</div>