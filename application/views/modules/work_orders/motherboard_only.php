<!-- Motherboard Only -->
<div class="modal fade" id="modalMotherboard" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible" id="error_maxfield_mb" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
                 </div>
                <div id="error_input_mb"></div>
                <div class="form-row justify-content-between">
                    <button class="col-4 btn btn-secondary btn-sm" id="include_chassis">Include Chassis</button>
                    <div class="col-2"></div>
                    <div class="col-2"><strong><span>Build ID</span></strong></div>
                  <div class="col-4">
                        <select id="build_id_mb" class="form-control form-control-sm">
                            <option value="null">Select Build ID</option>
                            <?php foreach ($data['build_id'] as $build_id): ?>
                                 <option value="<?= $build_id->build_id ?>">
                                <?= $build_id->build_id; ?>
                            </option>
                            <?php endforeach; ?> 
                        </select>
                    </div>
                </div><br>
                <div><span id="work_order_id_add_mb" style="display:none;"></span></div>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong></div><br>
                <div class="form-row">
                    <label for="motherboard" class="col-2">Motherboard</label>
                    <div class="col-5" id="motherboard_chassis">
                        <select id="motherboard_mb" class="form-control form-control-sm">
                            <option value="null">Select Motherboard</option>
                            <?php foreach ($motherboard as $motherboard): ?>
                                 <option value="<?= $motherboard->model ?>">
                                <?= $motherboard->model; ?>
                            </option>
                            <?php endforeach; ?> 
                        </select>
                    </div>
                    <label for="motherboard_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_motherboard_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="motherboard_mb_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-5" id="cpu_motherboard2">
                        <select id="cpu_mb" class="form-control form-control-sm cpu_select">
                            
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_cpu_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="cpu_mb_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-5" id="memory_motherboard2">
                       <select id="memory_mb" class="form-control form-control-sm memory_select">

                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_memory_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="memory_mb_total"></span></strong>
                </div><br>


                <div class="form-row">
                   <label for="ob_nic_mb" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic_mb"></span>
                    </div>
                </div>
                <div class="form-row">
                   <label for="pcie_slot_mb" class="col-2">PCIE SLOTS</label>
                    <div class="col-7">
                       <span id="pcie_slot_mb"></span>
                    </div>
                </div>
                            </br>

                 <div class="form-row slot_list_mb">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrl_mb">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlr_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlr_mb_total"></span></strong>
                </div><br>
                
                <div class="form-row slot_list_copy_mb" style="display:none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller2_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrltwo_mb">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrtwo_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlrtwo_mb_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot2_mb"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row slot_list_copy2_mb" style="display: none;">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller3_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrlthree_mb">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrthree_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlrthree_mb_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot3_mb"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-5">
                        <input type="text" class="form-control form-control-sm" id="psu_mb">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_psu_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_psu_mb_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-5">
                        <select id="mounting_mb" class="form-control form-control-sm">
                            <?php foreach ($data['mounting'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_mounting_mb">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_mounting_mb_total"></span></strong>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty_mb" class="form-control form-control-sm">
                            <?php foreach ($data['warranty'] as $warranty): ?>
                            <option value="<?= $warranty->name ?>"
                                <?= $warranty->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranty->name; ?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="os" class="col-2">Notes</label>
                    <div class="col-10">
                    <textarea id="os_mb" rows="2" class=" form-control form-control-sm"></textarea></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_modal">Close</button>
                <button type="button" class="btn btn-primary" id="add_order_motherboard">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Motherboard Only -->
<div class="modal fade" id="modalMotherboard_edit" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible" id="error_maxfield_mb_edit" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i>Maximum of 3 fields are allowed.</h5>
                 </div>
                <div id="error_input_mb_edit"></div>
                <div class="form-row justify-content-between">
                  <div class="col-6"></div>
                    <div class="col-2"><strong><span>Build ID</span></strong></div>
                  <div class="col-4">
                        <select id="build_id_mb_edit" class="form-control form-control-sm">
                            <option value="null">Select Build ID</option>
                            <?php foreach ($data['build_id'] as $build_id): ?>
                                 <option value="<?= $build_id->build_id ?>">
                                <?= $build_id->build_id; ?>
                            </option>
                            <?php endforeach; ?> 
                        </select>
                    </div>
                    <div><span id="work_order_id_mb" style="display:none;"></span></div>
                </div><br>
                <div style="text-align: center; background: white; box-shadow: 0 0 3px #444; height: 30px; background-image: linear-gradient(white, rgba(218, 223, 225, 1));"><strong>MOTHERBOARD</strong></div><br>
                <div class="form-row">
                    <label for="motherboard_mb_edit" class="col-2">Motherboard</label>
                    <div class="col-5">
                        <select id="motherboard_mb_edit" class="form-control form-control-sm">
                            <option value="null">Select Motherboard</option>
                            <?php foreach ($data['motherboard'] as $motherboards): ?>
                                 <option value="<?= $motherboards->model ?>">
                                <?= $motherboards->model; ?>
                            </option>
                            <?php endforeach; ?> 
                        </select>
                    </div>
                    <label for="motherboard_qty_edit" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_motherboard_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_motherboard_mb_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="cpu" class="col-2">CPU</label>
                    <div class="col-5" id="cpu_motherboard_mb_edit">
                        <select id="cpu_mb_edit" class="form-control form-control-sm cpu_select">
                            <option class="cpu_mb_edit"></option>
                        </select>
                    </div>
                    <label for="cpu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_cpu_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cpu_mb_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="memory" class="col-2">Memory</label>
                    <div class="col-5" id="memory_motherboard_mb_edit">
                       <select id="memory_mb_edit" class="form-control form-control-sm memory_select">
                            <option class="memory_mb_edit"></option>
                        </select>
                    </div>
                    <label for="memory_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_memory_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_memory_mb_edit_total"></span></strong>
                </div><br>


                <div class="form-row">
                   <label for="ob_nic_mb_edit" class="col-2">OB NIC</label>
                    <div class="col-7">
                       <span id="ob_nic_mb_edit"></span>
                    </div>
                </div>
                <div class="form-row">
                   <label for="pcie_slot_mb_edit" class="col-2">PCIE SLOTS</label>
                    <div class="col-7">
                       <span id="pcie_slot_mb_edit"></span>
                    </div>
                </div>
                            </br>
                            
                 <div class="form-row slot_list_mb">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrl_mb_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-2">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlr_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlr_mb_edit_total"></span></strong>
                </div><br>
                
                <div class="form-row slot_list_copy_mb">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller2_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrltwo_mb_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrtwo_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlrtwo_mb_edit_two_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot2_mb"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row slot_list_copy2_mb">
                    <div class="col-1">
                        <button type="button" class="btn btn-primary btn-sm" id="add_controller3_mb">+</button>
                    </div>
                    <label for="slot" class="col-1">Slot</label>
                    <div class="col-5">
                        <input type="text" name="slot[]" class="form-control form-control-sm" id="cntrlthree_mb_edit">
                    </div>
                    <label for="slot_qty" class="col-1">Qty</label>
                    <div class="col-1">    
                        <input type="number" name="slot_qty[]" class="form-control form-control-sm" id="qty_cntrlrthree_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_cntrlrthree_mb_edit_total"></span></strong>
                    <div class="col-1">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_slot3_mb"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>-</a>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="psu" class="col-2">PSU</label>
                    <div class="col-5">
                        <input type="text" class="form-control form-control-sm" id="psu_mb_edit">
                    </div>
                    <label for="psu_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_psu_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_psu_mb_edit_total"></span></strong>
                </div><br>
                <div class="form-row">
                    <label for="mounting" class="col-2">Mounting</label>
                    <div class="col-5">
                        <select id="mounting_mb_edit" class="form-control form-control-sm">
                            <?php foreach ($data['mounting'] as $mounting): ?>
                            <option value="<?= $mounting->name ?>"
                                <?= $mounting->name === 'Generic Rail Kit' ? 'selected' : '' ?>>
                                <?= $mounting->name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <label for="mounting_qty" class="col-1">Qty</label>
                    <div class="col-2">
                        <input type="number" class="form-control form-control-sm" id="qty_mounting_mb_edit">
                    </div>
                    <label class="col-1">Total:</label>
                   <strong> <span class="col-1" id="qty_mounting_mb_edit_total"></span></strong>
                </div><hr>
                <div class="form-row">
                    <label for="warranty" class="col-2">Warranty</label>
                    <div class="col-10">
                        <select id="warranty_mb_edit" class="form-control form-control-sm">
                            <?php foreach ($data['warranty'] as $warranty): ?>
                            <option value="<?= $warranty->name ?>"
                                <?= $warranty->name === 'Standard' ? 'selected' : '' ?>>
                                <?= $warranty->name; ?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div><br>
                <div class="form-row">
                    <label for="os" class="col-2">Notes</label>
                    <div class="col-10">
                    <textarea id="os_mb_edit" rows="2" class=" form-control form-control-sm"></textarea></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_modal">Close</button>
                <button type="button" class="btn btn-primary" id="update_order_motherboard">Update</button>
            </div>
        </div>
    </div>
</div>