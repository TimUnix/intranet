<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('templates/common/head'); ?>
    <body>
        <div class="background">
        </div>
	    <div class="container">
            <div class="col-md-6 offset-md-2 ml-auto mr-auto">
                <div class="row">
			        <div class="card col-md-12 login-card">
				        <div class="card-body text-center">
                            <div class="row mb-4">
                                <div class="col">
                                    <img src="<?php echo base_url() ?>resources/img/unixlogo.png"
                                         alt="Unixsurplus logo">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col mx-auto">
					                <h2 class="welcome-msg">Welcome to <strong>Intranet</strong></h2>
                                </div>
                            </div>
							<?php if(ENVIRONMENT == "development") { ?>
								<form method="POST" action="<?= base_url() ?>index.php/login">
									<fieldset>
										<div class="input-group mb-2">
											<span class="input-group-text">
												<i class="fa fa-envelope" aria-hidden="true"></i>
											</span>
											<input class="form-control" placeholder="Email" type="email" name="email"
												required>
										</div>
										
										<div class="input-group">
											<span class="input-group-text">
												<i class="fa fa-lock" aria-hidden="true"></i>
											</span>
											<input class="form-control" placeholder="Password" type="password"
												name="password" required>
										</div>
									</fieldset>
									<button class="btn btn-primary mt-3 col-md-2 offset-md-10" type="submit">
                                        Login
                                    </button>
								</form>
							<?php } ?>
				        </div>
			        </div>
                </div>
                <div class="row" id="load_google_button"></div>
                <div class="row mx-auto mt-3">
                    <?php if ($this->session->flashdata('error')): ?>
				        <div class="alert alert-danger text-center mx-auto mb-auto">
					        <?= $this->session->flashdata('error'); ?>
				        </div>
			        <?php endif; ?>
                </div>
	        </div>
        </div>

        <!--   Core JS Files   -->
        <script src="<?= base_url() ?>webtemplate/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url() ?>webtemplate/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url() ?>/webtemplate/dist/js/adminlte.min.js"></script>
    </body>
</html>
<script>
 const baseUrl = "<?= base_url() ?>";
 $(document).ready(function(){
	 
	 $("#load_google_button").load("<?= base_url() ?>index.php/login/googleLogin");
	 
 });
</script>
