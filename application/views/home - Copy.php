 <!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Home</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/nav_bar.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/loader.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/w3.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="<?php echo base_url(); ?>resources/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>resources/js/jquery-3.5.1.js"></script>
		<script src="<?php echo base_url(); ?>resources/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>resources/js/dataTables.bootstrap4.min.js"></script>
		
		


</style>
	</head>
	<body onload="myFunction()">
	<div id="loader"></div>


	<div class="w3-sidebar w3-bar-block w3-card w3-animate-left" style="display:none;" id="mySidebar">
  <button class="w3-bar-item w3-button w3-large"
  onclick="w3_close()">Close &times;</button>
  <a href="<?php echo base_url(); ?>index.php/home" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-search"></i> Search</a>
  <a href="<?php echo base_url(); ?>index.php/inventory" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-pencil-square-o"></i> Inventory</a>
  <a href="<?php echo base_url(); ?>index.php/price_update" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-money"></i> Price Update</a>
  <a href="<?php echo base_url(); ?>index.php/reports" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-newspaper-o"></i> Reports</a>
  <a href="<?php echo base_url(); ?>index.php/research" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-eye"></i> Research</a>
  <a href="<?php echo base_url(); ?>index.php/test_reports" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-eyedropper"></i> Test Reports</a>
  <a href="<?php echo base_url(); ?>index.php/admin" class="w3-bar-item w3-button" style="text-decoration: none;"><i class="fa fa-user"></i> Admin</a>
</div>


<div id="main">
<div>
  <button id="openNav" class="w3-button" onclick="w3_open()">&#9776;</button>
	<div style="display:none;" id="myDiv" class="animate-bottom">
		<div class="container">
			
			<div class="row">
				<div class="col-md-10">
				<a><img align ='left' width='150px' src="<?php echo base_url(); ?>resources/img/unixlogo.png"></a>
			<h1 class="page-header text-center">Welcome To UnixSurplus</h1>
</div>
				<div class="col-md-2" style="padding-bottom: 90px; margin-top: -30px;">
					<?php
						$user = $this->session->userdata('user');
						extract($user);
					?>
					<!--<h2>Welcome to Unix Surplus </h2>-->
					<!--<h4>User Info:</h4>-->
					<p>Email: <?php echo $email; ?></p>
					<!--<p>Password: <?php echo $password; ?></p>-->
					<a href="<?php echo base_url(); ?>index.php/user/logout" class="btn btn-danger" style="margin-left: 70px;">Logout</a>


				</div>
			</div>
			
			<div id="load_products" class="font-weight-bolder"></div>
			
		</div>
		</div>
	</body>
</html>
<script>
                 $(document).ready(function(){
                     $("#load_products").load("<?php echo base_url();    ?>index.php/search/request/load_items");
                });
                              
				function openNav() {
				document.getElementById("mySidebar").style.width = "190px";
				document.getElementById("main").style.marginLeft = "190px";
				}

				function closeNav() {
				document.getElementById("mySidebar").style.width = "0";
				document.getElementById("main").style.marginLeft= "0";
				}

				function w3_open() {
				document.getElementById("main").style.marginLeft = "25%";
				document.getElementById("mySidebar").style.width = "25%";
				document.getElementById("mySidebar").style.display = "block";
				document.getElementById("openNav").style.display = 'none';
				}
				function w3_close() {
				document.getElementById("main").style.marginLeft = "0%";
				document.getElementById("mySidebar").style.display = "none";
				document.getElementById("openNav").style.display = "inline-block";
				}
</script>


<script>
var myVar;

function myFunction() {
  myVar = setTimeout(showPage, 4000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
</script>
