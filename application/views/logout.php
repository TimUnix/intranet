<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Logout Success | Intranet</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>resources/img/net-icon.png">

        <style>
         html, body {
             height: 100%;
             width: 100%;
             font-family: 'Roboto', sans-serif;
             box-sizing: border-box;
             overflow: hidden;
         }

         .background::before {
             /* background-image: url("<?php echo base_url();?>resources/img/bg_login.jpg"); */
             /* background-size: cover; */
             content: "";
             opacity: 0.4;
             filter: blur(4px);
             width: 100%;
             height: 100%;
             top: 0;
             left: 0;
             position: absolute;
         }

         .background::after {
             background-color: gray;
             content: "";
             opacity: 0.6;
             width: 100%;
             height: 100%;
             top: 0;
             left: 0;
             position: absolute;
         }

         .main {
             display: flex;
             justify-content: center;
             align-items: center;
             width: 100%;
             height: 100%;
             z-index: 1;
             position: relative;
         }

         .info-card {
             width: 600px;
             padding: 20px;
             box-shadow: 2px 2px 2px #333;
             background-color: #eee;
             border-radius: 10px;
             margin: auto;

             display: flex;
             flex-direction: column;
             justify-content: center;
             align-items: center;
         }

         .message {
             font-weight: 300;
         }

         .fa {
             color: #15bb37;
         }

         .btn-return {
             padding: 10px;
             border-radius: 4px;
             border: 2px solid #1277d0;
             text-decoration: none;
             color: #1277d0;
         }

         .btn-return:hover {
             color: #eee;
             background-color: #1277d0;
         }
        </style>
    </head>
    <body>
        <div class="background"></div>
        <main class="main">
            <div class="info-card">
                <i class="fa fa-check-circle-o fa-5x" aria-hidden="true"></i>
                <h1 class="message">You have been successfully logged out.</h1>
                <a href="<?php echo base_url(); ?>index.php/" class="btn-return">
                    Return to Login
                </a>
            </div>
        </main>
    </body>
</html>

<script>
    localStorage.removeItem("accessToken");
    localStorage.removeItem("expires_in");
    localStorage.removeItem("currentUri");
</script>