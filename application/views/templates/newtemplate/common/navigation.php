<aside class="main-sidebar elevation-4 sidebar-light-primary">
	
	<div class="sidebar">
		
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
					<a class="nav-link" href="#">
						<img src="<?php echo $user_picture; ?>""
								class="img-circle elevation-2 nav-icon">
						<p>
							<?php echo $firstname . ' ' . $lastname; ?>
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="#">
								<?php echo $email; ?>
							</a>
						</li>
					</ul>
				</li>
				<?php foreach ($data['nav_menu'] as $item) { ?>
					<li class="nav-item">
						<a class="nav-link"
							href="<?php echo base_url() . "index.php/" . $item['route'] ?>">
							<p>
								<?php echo $item['title']; ?>
								<?php if (count($item['children']) > 1) { ?>
									<i class="right fas fa-angle-left"></i>
								<?php } ?>
							</p>
						</a>
						<?php if (count($item['children']) > 1) { ?>
							<ul class="nav nav-treeview">
								<?php foreach($item['children'] as $child) { ?>
									<li class="nav-item">
										<a class="nav-link"
											href="<?php echo base_url() . "index.php/" . $child['route']; ?>">
											<p>
												<?php echo $child['title']; ?>
											</p>
										</a>
									</li>
								<?php } ?>
							</ul>
						<?php } ?>
					</li>
				<?php } ?>
			</ul>
		</nav>





		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/home/index.php/home">
							<p>
								Search															</p>
						</a>
											</li>
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/inventory">
							<p>
								Inventory															</p>
						</a>
											</li>
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/price_update">
							<p>
								Price Update															</p>
						</a>
											</li>
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/reports">
							<p>
								Reports															</p>
						</a>
											</li>
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/research">
							<p>
								Research															</p>
						</a>
											</li>
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/test_reports">
							<p>
								Test Reports																	<i class="right fas fa-angle-left"></i>
															</p>
						</a>
													<ul class="nav nav-treeview">
																	<li class="nav-item">
										<a class="nav-link" href="<?php echo base_url(); ?>index.php/add_test_report">
											<p>
												Add Test Report											</p>
										</a>
									</li>
																	<li class="nav-item">
										<a class="nav-link" href="<?php echo base_url(); ?>index.php/view_test_reports">
											<p>
												View Test Reports											</p>
										</a>
									</li>
															</ul>
											</li>
									<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>index.php/admin">
							<p>
								Admin																	<i class="right fas fa-angle-left"></i>
															</p>
						</a>
													<ul class="nav nav-treeview">
																	<li class="nav-item">
										<a class="nav-link" href="<?php echo base_url(); ?>index.php/users_access">
											<p>
												Users Access											</p>
										</a>
									</li>
																	<li class="nav-item">
										<a class="nav-link" href="<?php echo base_url(); ?>index.php/menu_items">
											<p>
												Menu Items											</p>
										</a>
									</li>
																	<li class="nav-item">
										<a class="nav-link" href="<?php echo base_url(); ?>index.php/tables">
											<p>
												Tables											</p>
										</a>
									</li>
															</ul>
											</li>
							</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
