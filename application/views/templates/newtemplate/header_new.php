<!doctype html>
<html style="height: auto;" lang="en">

	<?php $this->load->view('templates/newtemplate/common/head'); ?>
	<?php $this->load->view('templates/newtemplate/common/style'); ?>
	
    <body class="sidebar-mini sidebar-closed sidebar-collapse" style="height: auto;">
        <!-- Site wrapper -->
        <!-- class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-dark navbar-primary">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                    
				</ul>

				<ul class="navbar-nav ml-auto">
					<li class="nav-item d-none d-sm-inline-block">
						<a><img align ='right' width='150px' src="<?php echo base_url(); ?>resources/img/unixlogo.png"></a>
					</li>
				</ul>

				
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
						
						<a class="nav-link" data-toggle="dropdown" href="#">
							<i class="fas fa-user" style="font-size: 1.5em;"></i>
						</a>

						<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
							
							<div class="dropdown-divider"></div>
								<a href="#" class="dropdown-item">
									<i class="fas fa-envelope mr-2"></i> 
									
									<?php echo $user_email; ?>
									
									<span class="float-right text-muted text-sm"></span>
								</a>

							<div class="dropdown-divider"></div>
								<a href="<?php echo base_url(); ?>index.php/google_logout/" class="dropdown-item">
									<i class="far fa-bell-slash mr-2"></i> 
									Logout
									<span class="float-right text-muted text-sm"></span>
								</a>
						</div>
					</li>
				</ul>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
           
			<?php $this->load->view('templates/newtemplate/common/navigation'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="min-height: 543px;">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1><?php echo $page_name; ?></h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="<?php echo base_url(); ?>">
                                            <?php echo $home_name; ?>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item active"><?php echo $page_name; ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

	</body>
</html>

			
                    
