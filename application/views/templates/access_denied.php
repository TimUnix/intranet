<!doctype html>

<html lang="en" style="height: auto;">
    <?php $this->load->view('templates/common/head'); ?>
    <body class="vh-100">
        <main class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col col-md-12 text-center">
                    <i class="fa fa-lock fa-5x mb-3" aria-hidden="true"></i>
                    <h1>Access Denied</h1>
                    <h2>You don't have access to this page</h2>
                </div>
            </div>
        </main>
    </body>
</html>
