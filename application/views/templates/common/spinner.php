<script>
 const spinner = '<div class="text-center"><div class="spinner-border ' +
                 'text-info" role="status"><span class="sr-only">' +
                 'Please wait. Loading data...</span></div></div>';
 const spinnerLight = '<div class="text-center"><div class="spinner-border' +
                      ' text-light" role="status"><span class="sr-only">' +
                      'Please wait. Loading data...</span></div></div>';
const error = '<div class="text-center" style = "font-size: 40px">No results </div>';
</script>
