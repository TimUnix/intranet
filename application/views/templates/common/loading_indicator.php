<div class="loading-indicator text-center">
    <div class="spinner-border text-info" role="status">
        <span class="sr-only">
            Please wait. Loading data...
        </span>
    </div>
</div>
