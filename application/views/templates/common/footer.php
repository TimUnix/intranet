  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    Copyright UnixSurplus 2021 reserved
  </footer>

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
<div id="sidebar-overlay"></div></div></div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url() ?>webtemplate/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>webtemplate/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url() ?>webtemplate/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>webtemplate/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>webtemplate/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() ?>webtemplate/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>/webtemplate/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>/webtemplate/dist/js/demo.js"></script>

<!-- Select2 v4.0.13 -->
<script src="<?= base_url() ?>resources/select2/dist/js/select2.full.min.js"></script>

<!-- Chart.js 3.3.2 -->
<script src="<?= base_url() ?>resources/chart.js-3.3.2/package/dist/chart.min.js"></script>

<!-- Date Range Picker -->
<script src="<?= base_url() ?>webtemplate/plugins/daterangepicker/moment.min.js"></script>
<script src="<?= base_url() ?>webtemplate/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Bootstrap 4 Duallistbox -->
<script src="<?= base_url() ?>webtemplate/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url() ?>webtemplate/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>

<script id="sessionCheckScript" type="text/javascript" src="<?= base_url() ?>resources/js/sessionCheck.js"
	      data-base-url="<?= base_url() ?>"></script>
<!-- page script -->
<script type="text/javascript" src="<?= base_url() ?>resources/js/tableScroll.js"></script>
</body></html>
