<?php if (isset($data['is_user_access_elevated']) && !$data['is_user_access_elevated']): ?>
<aside style="display: none;">
<?php else: ?>
    <aside class="main-sidebar elevation-4 sidebar-light-primary">
<?php endif; ?>

	            
	<div class="sidebar">

	<div class="" data-toggle="modal" data-target="#modalinfo">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
				<img src="<?php echo $data['user_picture']; ?>"
						  class="img-circle elevation-2">
            </div>
            <div class="info">
				<a class="d-block" href="#">
					<?php echo $data['user_firstname'] . ' ' . $data['user_lastname']; ?>
				</a>
            </div>
		</div>	
	</div>
		<div class="modal fade" tabindex="-1" role="dialog" id="modalinfo">
		<div class="modal-dialog" role="document">
			<div class="modal-newcontent ">
			<div class="modal-header ">
				<a href="" class="col text-center">
                <img src="<?php echo $data['user_picture']; ?>" class="img-circle elevation-2 " style="height: auto; width: 4.1rem;"> </a>
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="list-group">
				<div class="d-flex justify-content-center"><span class="a">Name:</a>
                    <?php echo $data['user_firstname'] . ' ' . $data['user_lastname']; ?></div>
					<div class="d-flex justify-content-center"><span class="a">Email:</a>
                     <?php echo $data['user_email']; ?></div>
				</div>
			</div>
			</div>
		</div>
		</div>

		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
				<?php foreach ($data['nav_menu'] as $item): ?>
                    <?php
                    $is_active = $item['title'] === $data['page_name'];
                    $has_active_child = count($item['children']) > 0 &&
                                        array_reduce($item['children'], function($result, $child) use ($data) {
                                            return $result || $child['title'] === $data['page_name'];
                                        }, false);
                    ?>
					<li class="nav-item <?= $has_active_child ? 'menu-open' : ''?>">
						<a class="nav-link <?= $is_active || $has_active_child ? 'active' : '' ?>"
						   href="<?= base_url() . "index.php/" . $item['route'] ?>">
							<i class="<?= $item['icon']; ?> nav-icon"></i>
							<p>
								<?= $item['title'] ?>
								<?php if (count($item['children']) > 0): ?>
									<i class="right fas fa-angle-left"></i>
								<?php endif; ?>
							</p>
						</a>
						<?php if (count($item['children']) > 0): ?>
							<ul class="nav nav-treeview">
								<?php foreach($item['children'] as $child): ?>
                                    <?php $is_active = $child['title'] === $data['page_name']; ?>
									<li class="nav-item">
										<a class="nav-link <?= $is_active ? 'active' : '' ?>"
										   href="<?= base_url() . "index.php/" . $child['route'] ?>">
											<i class="<?= $child['icon'] ?>"></i>
											<p>
												<?= $child['title'] ?>
											</p>
										</a>
									</li>
				                <?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
