<style>
 .select2-selection__choice {
     color: #1c1c1c !important;
 }
 .header-logo {
     padding: 8px 12px;
     background-color: #fff;
     border-radius: 20px;
 }
 .loader,
 .loader:after {
     border-radius: 50%;
     width: 10em;
     height: 10em;
 }
 .loader {            
     margin: 60px auto;
     font-size: 10px;
     position: relative;
     text-indent: -9999em;
     border-top: 1.1em solid rgb(0, 0, 0);
     border-right: 1.1em solid rgb(0, 0, 0);
     border-bottom: 1.1em solid rgb(0, 0, 0);
     border-left: 1.1em solid #ffffff;
     -webkit-transform: translateZ(0);
     -ms-transform: translateZ(0);
     transform: translateZ(0);
     -webkit-animation: load8 1.1s infinite linear;
     animation: load8 1.1s infinite linear;
 }
 @-webkit-keyframes load8 {
     0% {
         -webkit-transform: rotate(0deg);
         transform: rotate(0deg);
     }
     100% {
         -webkit-transform: rotate(360deg);
         transform: rotate(360deg);
     }
 }
 @keyframes load8 {
     0% {
         -webkit-transform: rotate(0deg);
         transform: rotate(0deg);
     }
     100% {
         -webkit-transform: rotate(360deg);
         transform: rotate(360deg);
     }
 }
 #loadingDiv {
     margin-top: 240px;
     position:absolute;;
     top:0;
     left:0;
     width:100%;
     height:100%;
     background-color:#f8f9fa;
 }
 .modal-newcontent {
     font-family: sans-serif; font-style: italic; font-weight: normal;
     color: #fff;
     background: linear-gradient(15deg,#646D94,#0E99AF);
     background-clip: padding-box; 
     margin-top: 39vh;
     margin-left: 3vw;
     vertical-align: middle;
     font-size: 17px;
     font-weight:500;
     border: none;
     border-radius: 20px;
     overflow: hidden;
     display: block;
     position: relative;
     letter-spacing: 1px;
 }
 .modal-backdrop {
     z-index: -1;
 }

 .sidebar {
     height: 100vh !important;
     margin-top: 0 !important;
 }
</style>
