<!doctype html>
<html style="height: auto;" lang="en">

	<?php $this->load->view('templates/common/head'); ?>
	<?php $this->load->view('templates/common/style'); ?>

    <?php if (isset($data['is_user_access_elevated']) && !$data['is_user_access_elevated']): ?>
        <body class="layout-top-nav sidebar-collapse" style="height: auto;">
    <?php else: ?>
            <body class="sidebar-mini layout-fixed layout-navbar-fixed" style="height: auto;">
    <?php endif; ?>
    <!-- Site wrapper -->
    <!-- class="wrapper">
         <!-- Navbar -->
    <div class="wrapper">
        <nav class="main-header navbar navbar-dark navbar-primary">
            <!-- Left navbar links -->
            <?php if ((isset($data['is_user_access_elevated']) && $data['is_user_access_elevated']) ||
                      (!isset($data['is_user_access_elevated']))): ?>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button">
                            <i class="fas fa-bars"></i>
                        </a>
                    </li>
            <?php endif; ?>
            
				</ul>

				<ul class="navbar-nav ml-auto">
					<li class="nav-item d-none d-sm-inline-block">
						<a href="<?php echo base_url(); ?>">
                            <img class="header-logo" align ='right'
                                 width='150px'
                                 src="<?php echo base_url(); ?>resources/img/unixlogo.png">
                        </a>
					</li>
				</ul>
				
				<ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a href="" class="nav-link" data-toggle="dropdown">
                            <i class="fas fa-user" style="font-size: 1.5em;"></i>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <div class="dropdown-divider"></div>
                            <a href="#response" class="dropdown-item" data-toggle="modal" data-target="#response">
                                <i class="fas fa-envelope mr-2"></i>
                                
                                <?php echo $data['user_email']; ?>
                                
                                <span class="float-right text-muted text-sm"></span>
                            </a>
                            <div class="dropdown-divider"></div></a>
                        <a href="<?php 
                                 if($data['is_google'] == true){
                                     echo base_url(); ?>index.php/google_logout/" <?php
                                                                                  }else{echo base_url();}?>index.php/logout"  class="dropdown-item">
							<i class="far fa-bell-slash mr-2"></i> 
							Logout 
							<span class="float-right text-muted text-sm"></span>
						</a>
                        </div>
        </nav>
        <div class="modal fade"  id="response">
            <div class="modal-dialog">
                <div class="modal-newcontent">
                    <div class="modal-header">
                        <a href="" class="col text-center">
                            <img src="<?php echo $data['user_picture']; ?>" class="img-circle elevation-2 " style="height: auto; width: 4.1rem;">
                        </a>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="list-group">
                            <div class="d-flex justify-content-center"><span class="a">Name:</a>
                                <?php echo $data['user_firstname'] . ' ' . $data['user_lastname']; ?>
                            </div>
                            <div class="d-flex justify-content-center"><span class="a">Email:</a>
                                <?php echo $data['user_email']; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
                    </li>
                </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        
		<?php $this->load->view('templates/common/navigation'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 543px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
