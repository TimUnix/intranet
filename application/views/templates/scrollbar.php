<style>
 .img-store:hover {
     cursor: pointer;
 }

 .horizontal-scroller {
     width: 100%;
     overflow-x: scroll;
 }

 .scroller-content {
     width: 100%;
     height: 1px;
 }

 .sticky-headers {
     position: sticky;
     top: 68px;
     overflow: auto;
     overflow: -moz-scrollbars-none;
     scrollbar-width: none;
     -ms-overflow-style: none;
	 z-index: 999;
 }

 .sticky-headers::-webkit-scrollbar {
     width: 0 ;
     display: none;
 }
</style>
