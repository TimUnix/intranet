<?php $this->load->view('templates/header_new'); ?>

<div class="container-fluid" style="margin-bottom: 80px;">
    <div class="card">
        <div class="card-header">
            <div class="card-title">Summary</div>
            <div class="card-tools">
                <button type="button" class="btn btn-tool"
                        data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div  class="card-body">
            <div id="summary-container">
                <div id="summary-cards" class="row">
                    <div class="col-md-2">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3 id="total-items"></h3>
                                <p>Items</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-search"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3 id="total-price"></h3>
                                <p>Total Price</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="small-box bg-lightblue">
                            <div class="inner">
                                <h3 id="brand-one-items"></h3>
                                <p id="brand-one-items-label"></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-search"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3 id="brand-two-items"></h3>
                                <p id="brand-two-items-label"></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-search"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="small-box bg-teal">
                            <div class="inner">
                                <h3 id="brand-one-price"></h3>
                                <p id="brand-one-price-label"></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="small-box bg-olive">
                            <div class="inner">
                                <h3 id="brand-two-price"></h3>
                                <p id="brand-two-price-label"></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Items by Brand</h3>
                </div>
                <div class="card-body">
                    <div class="chart-spinner text-center">
                        <div class="spinner-border text-info" role="status">
                            <span class="sr-only">
                                Please wait. Loading data...
                            </span>
                        </div>
                    </div>
                    <div class="chart-container mx-auto">
                        <canvas id="brandItemsChart"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Item Prices by Brand</h3>
                </div>
                <div class="card-body">
                    <div class="chart-spinner text-center">
                        <div class="spinner-border text-info" role="status">
                            <span class="sr-only">
                                Please wait. Loading data...
                            </span>
                        </div>
                    </div>
                    <div class="chart-container mx-auto">
                        <canvas id="brandPricesChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Items by Category</h3>
                </div>
                <div class="card-body">
                    <div class="chart-spinner text-center">
                        <div class="spinner-border text-info" role="status">
                            <span class="sr-only">
                                Please wait. Loading data...
                            </span>
                        </div>
                    </div>
                    <div class="chart-container mx-auto">
                        <canvas id="categoryItems"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Items by Condition</h3>
                </div>
                <div class="card-body">
                    <div class="chart-spinner text-center">
                        <div class="spinner-border text-info" role="status">
                            <span class="sr-only">
                                Please wait. Loading data...
                            </span>
                        </div>
                    </div>
                    <div class="chart-container mx-auto">
                        <canvas id="conditionItems"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Items by Year</h3>
                </div>
                <div class="card-body">
                    <div class="chart-spinner text-center">
                        <div class="spinner-border text-info" role="status">
                            <span class="sr-only">
                                Please wait. Loading data...
                            </span>
                        </div>
                    </div>
                    <div class="chart-container mx-auto">
                        <canvas id="yearItems"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Items by Inventory</h3>
                    <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Area</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#inventory-chart" data-toggle="tab">Donut</a>
                    </li>
                  </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-container mx-auto">
                        <div class="tab-content p-0">
                             <!-- Morris chart - Sales -->
                            <div class="chart tab-pane active" id="revenue-chart">
                                 <canvas id="yearInventory"></canvas>
                            </div>
                            <div class="chart tab-pane" id="inventory-chart">
                                <canvas id="pieInventory"></canvas>
                             </div>
                        </div><!--tab content-->
                     </div> <!--card containter-->
                </div> <!--card body-->
            </div> <!--card-->
        </div>
    </div>
</div>

<?php $this->load->view('templates/common/spinner'); ?>

<script id="homeScript" data-base-url="<?php echo base_url();?>"
        src="<?php echo base_url(); ?>resources/js/home.js"
        type="text/javascript">
</script>


<?php $this->load->view('templates/common/footer'); ?>
