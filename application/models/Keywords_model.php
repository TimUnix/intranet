<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keywords_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function save_keyword($par, $prase, $fullname, $datetime) {
        if (!isset($par) || $par == '' || !isset($prase) || $prase == '' || !isset($fullname) || $fullname == '') return -1;
        
        $data = array(
            'keyword_par' => $par,
            'keyword_prase' => $prase,
            'edited_by' => $fullname,
            'edited_date' => $datetime
        );
        
        $this->db->insert('keywords', $data);

        return $this->db->insert_id();
    }

    function update_keyword($data, $par){
        $this->db->where('keyword_par', $par);
        $this->db->update('keywords', $data);

        return $this->db->insert_id();
    }

    function check_keyword_exists($par, $prase) {
        $this->db->select();
        $this->db->where('keyword_par', $par);

        $query = $this->db->get('keywords', 1, 0);

        return $query->row();
    }
}

?>
