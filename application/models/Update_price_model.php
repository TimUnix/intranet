<?php

class Update_price_model extends CI_Model {
    public function check_if_par_exists($par) {
        $this->db->select('par');
        $this->db->where('par', $par);
        $query = $this->db->get('update_price');

        return sizeof($query->result()) > 0;
    }
    
    public function update_price($par, $new_price, $user_id){
	    $this->db->set('new_pri', $new_price);
        $this->db->set('user_id', $user_id);
        
	    $this->db->where('par', $par);
	    $this->db->update('update_price');
    }

    public function add_updated_price($par, $new_price, $user_id) {
        $data = array(
            'par' => $par,
            'new_pri' => $new_price,
            'user_id' => $user_id
        );
        
        $this->db->insert('update_price', $data);
    }

    public function update_notes($par, $notes, $user_id){
	    $this->db->set('notes', $notes);
        $this->db->set('user_id', $user_id);
        
	    $this->db->where('par', $par);
	    $this->db->update('update_price');
    }

    public function add_updated_notes($par, $notes, $user_id) {
        $data = array(
            'par' => $par,
            'notes' => $notes,
            'user_id' => $user_id
        );
        
        $this->db->insert('update_price', $data);
    }
}

?>
