<?php
class Work_orders_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        parent::__construct();
    }


    /*public function get_sku($sku) {
       $query = $this->db->query("select * from data_import_listing where sku = '$sku'");

       return $query->result();
       }*/


       public function search_for_ob_nic_and_pcie_slot($motherboard_, $cpu_, $memory_){
        $query = $this->db->query("select * from motherboard where model = '$motherboard_' and qty_cpu = '$cpu_' and memory_family = '$memory_'");

        return $query->result();
       }
       
       public function get_mother_board_brand_using_cpu($cpu){
        $this->db->select();
        $this->db->where('part', $cpu);
        $query = $this->db->get('cpu_new');
        return $query->result();
       }


       public function get_mother_board_brand_using_memory($barcode, $memory_family, $brand){
        $this->db->select();
        $this->db->where('barcode', $barcode);
        $this->db->where('memory_family', $memory_family);
        $this->db->where('brand', $brand);
        $query = $this->db->get('memory');
        return $query->result();
       }


       public function get_motherboard_brand($part_cpu, $memory_family, $mother_board){
        $this->db->select();
        $this->db->where('qty_cpu', $part_cpu);
        $this->db->where('memory_family', $memory_family);
        $this->db->where('model', $mother_board);
        $query = $this->db->get('motherboard');
        return $query->result();
       }

    public function get_item(
        $searchItem = null,
        $searchSelected = null,
        $order_by = null,
        $sort_order = null
    ){
        $this->db->select();

        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }
        $this->db->where('archive', '0');
        $this->db->group_by('barcode'); 
        $this->db->order_by('barcode', 'asc');


        $query = $this->db->get('work_orders');
        return $query->result();
    }

    function count(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
       
        $this->db->select('COUNT(*) AS listing_count');
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
            $this->db->select('COUNT(*) AS listing_count');
        }

        $this->db->where('archive', '0');
        $this->db->group_by('barcode');

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('work_orders');
        echo count($query->result());
    }
    public function get_front_caddies(){
        $query = $this->db->get('front_caddies');
        return $query->result();
    }
    public function get_rear_caddies(){
        $query = $this->db->get('rear_caddies');
        return $query->result();
    }

    public function export_csv(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null

    ) {
        $this->load->dbutil();

        $this->db->select();
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('work_orders');

        return $this->dbutil->csv_from_result($query);
    }

    public function get_buildID(){

        $this->db->select('build_id');
        $query = $this->db->get('assembly');
        return $query->result();
    }

    public function get_shipMethod(){
        $this->db->select('ship_method');
        $query = $this->db->get('shipping_method');
        return $query->result();
    }

     public function delete_row($barcode, $key=null){
        $this->db->where('barcode', $barcode);
        $this->db->delete('work_orders');
        return $key;
    }

    public function delete_row_add_order($work_order_id, $key=null){
        $this->db->where('work_order_id', $work_order_id);
        $this->db->delete('orders');
        return $key;
    }

    public function get_last_id(){
        $this->db->select('barcode');
        $this->db->limit(1);
        $this->db->order_by('work_order_id', 'desc');
        $query = $this->db->get('work_orders');
        return $query->result();
    }

    public function get_barcode($barcode){
        $this->db->select();
        $this->db->where('barcode', $barcode);
        $query = $this->db->get('orders');
        return $query->result();
    }
    public function get_work_order_id_from_wo($work_order_id){
        $this->db->select();
        $this->db->where('work_order_id', $work_order_id);
        $query = $this->db->get('work_orders');
        return $query->result();
    }

    public function get_barcode_from_wo($barcode){
        $this->db->select();
        $this->db->where('barcode', $barcode);
        $this->db->where('archive', '0');
        $query = $this->db->get('work_orders');
        return $query->result();
    }

    public function check_barcode_exist($barcode){
        $this->db->select('barcode');
        $this->db->where('barcode', $barcode);
        $query = $this->db->get('work_orders');
        return $query->result();
    }

    public function get_customer_detail_edit($barcode){
        $this->db->select();
        $this->db->limit(1);
        $this->db->order_by('barcode', 'desc');
        $this->db->where('barcode', $barcode);
        $query = $this->db->get('work_orders');
        return $query->result();
    }

    public function get_chassis_detail_edit($chassis, $front_caddies, $rear_caddies, $item_harddrive_one,
        $item_harddrive_two, $item_harddrive_three, $motherboard, $cpu, $memory, $cntrl, $controllertwo,
        $controllerthree, $psu, $mounting, $warranty, $os){
        $this->db->select();
        $this->db->where('chassis', $chassis);
        $this->db->where('front_caddies', $front_caddies);
        $this->db->where('rear_caddies', $rear_caddies);
        $this->db->where('item_harddrive_one', $item_harddrive_one);
        $this->db->where('item_harddrive_two', $item_harddrive_two);
        $this->db->where('item_harddrive_three', $item_harddrive_three);
        $this->db->where('motherboard', $motherboard);
        $this->db->where('cpu', $cpu);
        $this->db->where('memory', $memory);
        $this->db->where('cntrl', $cntrl);
        $this->db->where('controllertwo', $controllertwo);
        $this->db->where('controllerthree', $controllerthree);
        $this->db->where('psu', $psu);
        $this->db->where('mounting', $mounting);
        $this->db->where('warranty', $warranty);
        $this->db->where('os', $os);
        $query = $this->db->get('work_orders');
        return $query->result();
    }

    public function search_chassis($part_number){
        $query = $this->db->query("select * from chassis_new where part_number = '$part_number'");

        return $query->result();
    }

    public function search_motherboard($motherboard){
        $query = $this->db->query("select * from motherboard where model = '$motherboard'");

        return $query->result();
    }


    public function get_cpu_fam($cpu_family){
        $query = $this->db->query("select * from cpu_new where cpu_family = '$cpu_family'");

        return $query->result();
    }

    public function get_cpu_motherboard($cpu){
        $query = $this->db->query("select * from cpu_new where part = '$cpu'");

        return $query->result();
    }

    public function get_memory_motherboard($memory){
        $query = $this->db->query("select * from memory where memory_family = '$memory'");

        return $query->result();
    }

    public function get_motherboard($qty_cpu){
        $query = $this->db->query("select * from motherboard where qty_cpu = '$qty_cpu'");

        return $query->result();
    }

    public function get_mounting(){
        $query = $this->db->query("select * from mounting");

        return $query->result();
    }

    public function get_drives(){
        $query = $this->db->query("select * from drives");

        return $query->result();
    }

    public function get_cpu(){
        $query = $this->db->query("select * from cpu_new");

        return $query->result();
    }

    public function get_memory(){
        $query = $this->db->query("select * from memory");

        return $query->result();
    }

    public function insert_new_order($data){
        
			$this->db->insert("orders",$data);
			return $this->db->insert_id();
		
    }

    public function insert_all_order($barcode){
       
         $this->db->query("insert into work_orders (barcode, created_date, ship_date, ship_method, sales_rep, client_name, contact_no, email, address, address2, chassis, qty_chassis, backplane, nodes, rearbays, drivebays, ff, rff, psu_slots, bin, front_caddies, qty_front_caddies, rear_caddies, qty_rear_caddies, item_harddrive_one, item_harddrive_two, item_harddrive_three, qty_hard_drive_one, qty_hard_drive_two, qty_hard_drive_three, motherboard, qty_motherboard, cpu, qty_cpu, memory, qty_memory, ob_nic, pcie, cntrl, qty_cntrlr, controllertwo, qty_controllertwo, controllerthree, qty_controllerthree, psu, qty_psu, mounting, qty_mounting, warranty, os, archive) select barcode, created_date, ship_date, ship_method, sales_rep, client_name, contact_no, email, address, address2, chassis, qty_chassis, backplane, nodes, rearbays, drivebays, ff, rff, psu_slots, bin, front_caddies, qty_front_caddies, rear_caddies, qty_rear_caddies, item_harddrive_one, item_harddrive_two, item_harddrive_three, qty_hard_drive_one, qty_hard_drive_two, qty_hard_drive_three, motherboard, qty_motherboard, cpu, qty_cpu, memory, qty_memory, ob_nic, pcie, cntrl, qty_cntrlr, controllertwo, qty_controllertwo, controllerthree, qty_controllerthree, psu, qty_psu, mounting, qty_mounting, warranty, os, archive FROM orders where barcode='$barcode'");
        $query = $this->db->query("delete from orders where barcode = '$barcode'");

    }



    public function update_each_order_motherboard_only($motherboard,
    $qty_motherboard,
    $cpu,
    $qty_cpu,
    $memory,
    $qty_memory,
    $ob_nic,
    $pcie,
    $cntrl,
    $controllertwo,
    $controllerthree,
    $qty_cntrlr,
    $qty_cntrlrtwo,
    $qty_cntrlrthree,
    $psu,
    $qty_psu,
    $mounting,
    $qty_mounting,
    $warranty,
    $os,
    $work_order_id,
    $barcode){
        $query = $this->db->query("select * from orders where work_order_id = '$work_order_id'");
        if($query->num_rows() > 0){
            $query = $this->db->query("update orders set work_order_id = '$work_order_id', chassis = '', qty_chassis = '', 
            backplane = '', nodes = '', rearbays = '', 
            drivebays = '', psu_slots = '', bin = '', 
            front_caddies = '', qty_front_caddies = '', rear_caddies = '', 
            qty_rear_caddies = '', item_harddrive_one = '', item_harddrive_two = '', 
            item_harddrive_three = '', qty_hard_drive_one = '', qty_hard_drive_two = '', 
            qty_hard_drive_three = '', motherboard = '$motherboard', qty_motherboard = '$qty_motherboard', 
            cpu = '$cpu', qty_cpu = '$qty_cpu', memory = '$memory', 
            qty_memory = '$qty_memory', ob_nic = '$ob_nic', pcie = '$pcie', cntrl = '$cntrl', qty_cntrlr = '$qty_cntrlr', 
            controllertwo = '$controllertwo', qty_controllertwo = '$qty_cntrlrtwo', controllerthree = '$controllerthree', 
            qty_controllerthree = '$qty_cntrlrthree', psu = '$psu', qty_psu = '$qty_psu', 
            mounting = '$mounting', qty_mounting = '$qty_mounting', warranty = '$warranty', 
            os = '$os' where work_order_id = '$work_order_id'");
            return "orders";
        }else{
            $query = $this->db->query("update work_orders set work_order_id = '$work_order_id', chassis = '', qty_chassis = '', 
                                                          backplane = '', nodes = '', rearbays = '', 
                                                          drivebays = '', psu_slots = '', bin = '', 
                                                          front_caddies = '', qty_front_caddies = '', rear_caddies = '', 
                                                          qty_rear_caddies = '', item_harddrive_one = '', item_harddrive_two = '', 
                                                          item_harddrive_three = '', qty_hard_drive_one = '', qty_hard_drive_two = '', 
                                                          qty_hard_drive_three = '', motherboard = '$motherboard', qty_motherboard = '$qty_motherboard', 
                                                          cpu = '$cpu', qty_cpu = '$qty_cpu', memory = '$memory', 
                                                          qty_memory = '$qty_memory', ob_nic = '$ob_nic', pcie = '$pcie', cntrl = '$cntrl', qty_cntrlr = '$qty_cntrlr', 
                                                          controllertwo = '$controllertwo', qty_controllertwo = '$qty_cntrlrtwo', controllerthree = '$controllerthree', 
                                                          qty_controllerthree = '$qty_cntrlrthree', psu = '$psu', qty_psu = '$qty_psu', 
                                                          mounting = '$mounting', qty_mounting = '$qty_mounting', warranty = '$warranty', 
                                                          os = '$os' where work_order_id = '$work_order_id'");
                                                          return "work_orders";
        }
        
    }



    public function update_each_order($chassis, $qty_chassis, $backplane, $nodes, $rearbays,  $drivebays, $psu_slots, $bin, $front_caddies,  $qty_front_caddies, $rear_caddies, $qty_rear_caddies, $item_harddrive_one,  $item_harddrive_two, $item_harddrive_three, $qty_hard_drive_one, $qty_hard_drive_two, $qty_hard_drive_three, $motherboard, $qty_motherboard, $cpu, $qty_cpu, $memory, $qty_memory, $cntrl, $qty_cntrlr, $controllertwo, $qty_controllertwo, $controllerthree, $qty_controllerthree, $psu, $qty_psu, $mounting, $qty_mounting, $warranty, $os, $work_order_id){
        $query = $this->db->query("update work_orders set work_order_id = '$work_order_id', chassis = '$chassis', qty_chassis = '$qty_chassis', backplane = '$backplane', nodes = '$nodes', rearbays = '$rearbays', drivebays = '$drivebays', psu_slots = '$psu_slots', bin = '$bin', front_caddies = '$front_caddies', qty_front_caddies = '$qty_front_caddies', rear_caddies = '$rear_caddies', qty_rear_caddies = '$qty_rear_caddies', item_harddrive_one = '$item_harddrive_one', item_harddrive_two = '$item_harddrive_two', item_harddrive_three = '$item_harddrive_three', qty_hard_drive_one = '$qty_hard_drive_one', qty_hard_drive_two = '$qty_hard_drive_two', qty_hard_drive_three = '$qty_hard_drive_three', motherboard = '$motherboard', qty_motherboard = '$qty_motherboard', cpu = '$cpu', qty_cpu = '$qty_cpu', memory = '$memory', qty_memory = '$qty_memory', cntrl = '$cntrl', qty_cntrlr = '$qty_cntrlr', controllertwo = '$controllertwo', qty_controllertwo = '$qty_controllertwo', controllerthree = '$controllerthree', qty_controllerthree = '$qty_controllerthree', psu = '$psu', qty_psu = '$qty_psu', mounting = '$mounting', qty_mounting = '$qty_mounting', warranty = '$warranty', os = '$os' where work_order_id = '$work_order_id'");
    }

    public function update_each_order_for_addOrder($chassis,
                                                    $qty_chassis,
                                                    $backplane,
                                                    $drivebays,
                                                    $rearbays,
                                                    $nodes,
                                                    $psuchassis,
                                                    $bin,
                                                    $front_caddies,
                                                    $qty_front_caddies,
                                                    $rear_caddies,
                                                    $qty_rear_caddies,
                                                    $item_harddrive_one,
                                                    $item_harddrive_two,
                                                    $item_harddrive_three,
                                                    $qty_hard_drive_one,
                                                    $qty_hard_drive_two,
                                                    $qty_hard_drive_three,
                                                    $motherboard,
                                                    $qty_motherboard,
                                                    $cpu,
                                                    $qty_cpu,
                                                    $memory,
                                                    $qty_memory,


                                                    $ob_nic,
                                                    $pcie,

                                                    $cntrl,
                                                    $controllertwo,
                                                    $controllerthree,
                                                    $qty_cntrlr,
                                                    $qty_cntrlrtwo,
                                                    $qty_cntrlrthree,
                                                    $psu,
                                                    $qty_psu,
                                                    $mounting,
                                                    $qty_mounting,
                                                    $warranty,
                                                    $os,
                                                    $work_order_id,
                                                    $barcode){
                                                        

                                                        $query = $this->db->query("select * from orders where work_order_id = '$work_order_id'");
                                                        if($query->num_rows() > 0){
                                                            $query = $this->db->query("update orders set work_order_id = '$work_order_id', chassis = '$chassis', qty_chassis = '$qty_chassis', 
                                                            backplane = '$backplane', nodes = '$nodes', rearbays = '$rearbays', 
                                                            drivebays = '$drivebays', psu_slots = '$psuchassis', bin = '$bin', 
                                                            front_caddies = '$front_caddies', qty_front_caddies = '$qty_front_caddies', rear_caddies = '$rear_caddies', 
                                                            qty_rear_caddies = '$qty_rear_caddies', item_harddrive_one = '$item_harddrive_one', item_harddrive_two = '$item_harddrive_two', 
                                                            item_harddrive_three = '$item_harddrive_three', qty_hard_drive_one = '$qty_hard_drive_one', qty_hard_drive_two = '$qty_hard_drive_two', 
                                                            qty_hard_drive_three = '$qty_hard_drive_three', motherboard = '$motherboard', qty_motherboard = '$qty_motherboard', 
                                                            cpu = '$cpu', qty_cpu = '$qty_cpu', memory = '$memory', 
                                                            qty_memory = '$qty_memory', ob_nic = '$ob_nic', pcie = '$pcie', cntrl = '$cntrl', qty_cntrlr = '$qty_cntrlr', 
                                                            controllertwo = '$controllertwo', qty_controllertwo = '$qty_cntrlrtwo', controllerthree = '$controllerthree', 
                                                            qty_controllerthree = '$qty_cntrlrthree', psu = '$psu', qty_psu = '$qty_psu', 
                                                            mounting = '$mounting', qty_mounting = '$qty_mounting', warranty = '$warranty', 
                                                            os = '$os' where work_order_id = '$work_order_id'");
                                                            return "orders";
                                                        }else{
                                                            $query = $this->db->query("update work_orders set work_order_id = '$work_order_id', chassis = '$chassis', qty_chassis = '$qty_chassis', 
                                                            backplane = '$backplane', nodes = '$nodes', rearbays = '$rearbays', 
                                                            drivebays = '$drivebays', psu_slots = '$psuchassis', bin = '$bin', 
                                                            front_caddies = '$front_caddies', qty_front_caddies = '$qty_front_caddies', rear_caddies = '$rear_caddies', 
                                                            qty_rear_caddies = '$qty_rear_caddies', item_harddrive_one = '$item_harddrive_one', item_harddrive_two = '$item_harddrive_two', 
                                                            item_harddrive_three = '$item_harddrive_three', qty_hard_drive_one = '$qty_hard_drive_one', qty_hard_drive_two = '$qty_hard_drive_two', 
                                                            qty_hard_drive_three = '$qty_hard_drive_three', motherboard = '$motherboard', qty_motherboard = '$qty_motherboard', 
                                                            cpu = '$cpu', qty_cpu = '$qty_cpu', memory = '$memory', 
                                                            qty_memory = '$qty_memory', ob_nic = '$ob_nic', pcie = '$pcie', cntrl = '$cntrl', qty_cntrlr = '$qty_cntrlr', 
                                                            controllertwo = '$controllertwo', qty_controllertwo = '$qty_cntrlrtwo', controllerthree = '$controllerthree', 
                                                            qty_controllerthree = '$qty_cntrlrthree', psu = '$psu', qty_psu = '$qty_psu', 
                                                            mounting = '$mounting', qty_mounting = '$qty_mounting', warranty = '$warranty', 
                                                            os = '$os' where work_order_id = '$work_order_id'");
                                                            return "work_orders";
                                                        }
        
    }

    public function update_customer_detail($ship_date, $ship_method, $client_name, $contact_no, 
            $email, $address, $barcode){
        $query = $this->db->query("update work_orders set ship_date = '$ship_date', ship_method='$ship_method', client_name = '$client_name', contact_no = '$contact_no', email = '$email', address = '$address' where barcode = '$barcode'");
    }

    public function soft_delete_all($barcode){
        $query = $this->db->query("update work_orders set archive = '1' where barcode = '$barcode'");
    }

    public function soft_delete_each($work_order_id){
        $query = $this->db->query("update work_orders set archive = '1' where work_order_id = '$work_order_id'");
    }

    public function delete_all_order($barcode, $key=null){
         $this->db->where('barcode', $barcode);
        $this->db->delete('orders');
        return $key;
    }


    public function get_display($barcode){
        $query = $this->db->query("select * from work_orders where barcode = '$barcode'");

        return $query->result();
    }


    public function get_display_orders($barcode){
        $query = $this->db->query("select * from orders where barcode = '$barcode'");

        return $query->result();
    }

    public function view_work_order($part_number, $barcode, $created){
        $query = $this->db->query("select * from work_orders where chassis = '$part_number' and barcode = '$barcode' and created_date = '$created'");

        return $query->result();
    }

    public function get_harddrive($sku){
        $query = $this->db->query("select * from drives where sku = '$sku'");

        return $query->result();
    }

    public function get_motherboard_modal($qty_cpu){
        $query = $this->db->query("select * from motherboard where qty_cpu = '$qty_cpu'");

        return $query->result();
    }

    public function get_all_motherboards(){
        $query = $this->db->query("select * from motherboard");

        return $query->result();
    }

    public function get_cpu_modal($part){
        $query = $this->db->query("select * from cpu_new where part = '$part'");

        return $query->result();
    }

    public function get_memory_modal($sku){
        $query = $this->db->query("select * from memory where sku = '$sku'");

        return $query->result();
    }

    public function update_order($data, $barcode, $chassis, $datecreated){
           
        $this->db->where('barcode',$barcode);
        $this->db->where('chassis',$chassis);
        $this->db->where('created_date',$datecreated);
		$this->db->update("work_orders",$data);
		return $key;
    }

    public function search_barcode($barcode){
        $query = $this->db->query("select * from work_orders where barcode = '$barcode'");

        return $query->result();
    }

    public function get_work_orders(){
        $query = $this->db->query("select * from work_orders order by created_date desc");

        return $query->result();
    }

    public function search_motherboard_for_name($chassis_id, $barcode){
        $query = $this->db->query("select * from work_orders where chassis = '$chassis_id' and work_order_id = '$barcode'");

        return $query->result();
    }

    public function get_name_motherboard($motherboard_name){
        $query = $this->db->query("select * from motherboard where qty_cpu = '$motherboard_name'");

        return $query->result();
    }

    public function get_warranty(){
        $query = $this->db->query("select * from warranty");

        return $query->result();
    }
       function get_field()
        {
            $fields = $this->db->list_fields('chassis_new');
            return $fields;
        }
    public function get_chassis()
    {
        $query = $this->db->query("select * from chassis_new");

        return $query->result();
    }
    public function get_user_id($email)
    {
        $query = $this->db->query("select * from users where email = '$email'");

        return $query->result();
    }
   
    public function update_chassis_search($user_id, $search, $data){
        $this->db->where('user_id', $user_id);
        $this->db->where('search', $search);
        $this->db->update("search_history", $data);
        return $key;
    }
    public function insert_chassis_search($data){
        $this->db->insert("search_history",$data);
			return $this->db->insert_id();
    }
    public function get_chassis_search($partnumber, $sortOrder, $orderBy){
        $this->db->like('part_number', $partnumber);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('chassis_new');

        return $query->result();
    }
    public function generate_csv($partnumber, $sortOrder, $orderBy){
        $this->load->dbutil();
        $this->db->like('part_number', $partnumber);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('chassis_new');

        return $this->dbutil->csv_from_result($query);
    }
    public function get_search_history_for_user($user_id){
        $query = $this->db->query("select * from search_history where user_id = '$user_id'");

        return $query->result();
    }


    public function update_chassiscol($data, $stepcode, $partnumber){
        $this->db->where('part_number', $partnumber);
        //$this->db->where('stepcode', $stepcode);
        $this->db->update("chassis_new", $data);
        return $key;
    }


    public function insert_new_chassis($data){
        $this->db->insert("chassis_new",$data);
        return $this->db->insert_id();
    }
    
    function load_work_order($barcode, $sku, $motherboard, $work_order_id){
        $this->db->select(array(
            "chassis",
            "qty_chassis",
            "backplane",
            "nodes",
            "rearbays",
            "drivebays",
            "psu_slots",
            "bin",
            "front_caddies",
            "qty_front_caddies",
            "rear_caddies",
            "qty_rear_caddies",
            "item_harddrive_one",
            "qty_hard_drive_one",
            "item_harddrive_two",
            "qty_hard_drive_two",
            "item_harddrive_three",
            "qty_hard_drive_three",
            "motherboard",
            "qty_motherboard",
            "cpu",
            "qty_cpu",
            "memory",
            "qty_memory",
            "ob_nic",
            "pcie",
            "cntrl",
            "qty_cntrlr",
            "controllertwo",
            "qty_controllertwo",
            "controllerthree",
            "qty_controllerthree",
            "psu",
            "qty_psu",
            "mounting",
            "qty_mounting",
            "warranty",
            "os",
            "created_date",
            "ship_date",
            "ship_method",
            "sales_rep",
            "client_name",
            "contact_no",
            "email",
            "address",
            "barcode"
        ))->where("barcode", $barcode)
                 ->group_start()
                 ->where("chassis", $sku)
                 ->or_where("motherboard", $motherboard)
                 ->group_end()
                 ->where("work_order_id", $work_order_id);

        $query = $this->db->get("work_orders");

        return $query->row();
    }
    function load_order($barcode, $sku, $motherboard, $work_order_id) {
        $this->db->select(array(
            "chassis",
            "qty_chassis",
            "backplane",
            "nodes",
            "rearbays",
            "drivebays",
            "psu_slots",
            "bin",
            "front_caddies",
            "qty_front_caddies",
            "rear_caddies",
            "qty_rear_caddies",
            "item_harddrive_one",
            "qty_hard_drive_one",
            "item_harddrive_two",
            "qty_hard_drive_two",
            "item_harddrive_three",
            "qty_hard_drive_three",
            "motherboard",
            "qty_motherboard",
            "cpu",
            "qty_cpu",
            "memory",
            "qty_memory",
            "ob_nic",
            "pcie",
            "cntrl",
            "qty_cntrlr",
            "controllertwo",
            "qty_controllertwo",
            "controllerthree",
            "qty_controllerthree",
            "psu",
            "qty_psu",
            "mounting",
            "qty_mounting",
            "warranty",
            "os",
            "created_date",
            "ship_date",
            "ship_method",
            "sales_rep",
            "client_name",
            "contact_no",
            "email",
            "address",
            "barcode"
        ))->where("barcode", $barcode)
                 ->group_start()
                 ->where("chassis", $sku)
                 ->or_where("motherboard", $motherboard)
                 ->group_end()
                 ->where("work_order_id", $work_order_id);

        $query = $this->db->get("orders");

        return $query->row();
    }

    function update_request($data, $listing_id, $key)
    {
        $this->db->where($listing_id, $key);
        $this->db->update("listing_table", $data);
        return $key;
    }

    public function check_chassis_part($partnumber){
        $querypart = $this->db->query("select * from chassis_new where part_number = '$partnumber'");
        ///$querystep = $this->db->query("select * from chassis_new where stepcode = '$stepcode'");

        if($querypart->num_rows() > 1){
            return "PartNumber is Already Exist";
        }else{
            
                return "Not Found";
            
        }
    }

    

    

    

    

    
}
