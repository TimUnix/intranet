<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nebula_model extends CI_Model {
    function __construct() {
        $this->load->database();
        parent::__construct();
    }

    public function create(array $payload) {
        $nebula = $payload;
        $nebula['erase_method'] = 'US DoD 5220.22-M';

        return $this->db->insert('nebula', $nebula) ? $this->db->insert_id() : false;
    }

    public function removeitems($serial_number) {
        if (!empty($serial_number) && is_array($serial_number)) {
            $this->db->where_in('device_serial_number',$serial_number);
            return $this->db->delete('nebula');
        }
    }

    public function load_vendors() {
        $query = $this->db->distinct()->select(array('vendor.vendor_name'))
            ->order_by('vendor.date_added')
            ->join('nebula_purchase_orders AS po', 'ON(po.vendor = vendor.vendor_name)', 'left')
            ->get('nebula_vendor AS vendor');

        return $query->result();
    }

    public function load_vendor($vendor) {
        $this->db->select('name');
        $this->db->where('vendor', $vendor);
        $query = $this->db->get('nebula_purchase_orders');
        
        return $query->result();
    }

    public function load(
        $search_query = null,
        $order_by = 'created',
        $sort_order = 'DESC',
        $search_column = null,
        $directories,
        $offset = 0
    ) {
        $limit = 100;

        $this->db->select(array(
            'id',
            'filename',
            'device_serial_number',
            'device_product_name',
            'created',
            'parent_dir',
            'lot',
            'disposition',
            'cost',
            'sell',
            'erase_method',
            'device_size',
            'result',
            'process_result',
            'status',
            'status_last_edited',
            'status_last_edited_by'
        ));

        if (isset($search_query) && $search_query !=='') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories) && is_array($directories) && count($directories) > 0 && $directories[0] !== '') {
            $this->db->where_in('parent_dir', $directories);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->limit($limit, $offset)
                          ->get('nebula');

        return $query->result();
    }

    function load_for_certificate($search_query, $search_column, $directories, $order_by, $sort_order) {
        $this->db->select(array(
            'process_started_at',
            'device_serial_number',
            'result',
            'created',
            'device_size',
            'process_integrity',
            'device_product_name',
            'geometry_total_sec',
            'results_errors'
        ));

        if (isset($search_query) && $search_query !=='') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories) && is_array($directories) && count($directories) > 0) {
            $this->db->where_in('parent_dir', $directories);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('nebula');

        return $query->result();
    }

    function full_report($serial_number) {
        $query = $this->db->select(array(
            'filename',
            'created',
            'title',
            'parent_dir',
            'lot',
            'disposition',
            'erase_method',
            'process_integrity',
            'fingerprint',
            'device_title',
            'device_serial_number',
            'device_platform_name',
            'device_product_name',
            'device_type',
            'device_revision',
            'device_size',
            'smart_firmware_version',
            'smart_capacity',
            'smart_ata_version',
            'smart_ata_standard',
            'smart_support',
            'smart_offline_data_collection_status',
            'smart_self_test_execution_status',
            'smart_time_offline_data_collection_sec',
            'smart_offline_data_collection_capabilities',
            'smart_capabilities',
            'smart_error_logging_capabilities',
            'smart_short_self_test_time_min',
            'geometry_partitioning',
            'geometry_total_sec',
            'geometry_first_sec',
            'geometry_bps',
            'results_started',
            'results_elapsed',
            'results_errors',
            'result',
            'process_name',
            'process_started_at',
            'process_elapsed',
            'process_result',
            'conclusion'
        ))->where('device_serial_number', $serial_number)
                 ->get('nebula');

        return $query->row();
    }


    public function summary_csv(
        $search_query = null,
        $search_column = null,
        $directories = null
    ) {
        $this->db->select('result');

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories) && is_array($directories) && count($directories) > 0 && $directories[0] !== '') {
            $this->db->where_in('parent_dir', $directories);
        }

        $query = $this->db->get('nebula');

        return $query->result();
    }

    public function reports_csv(
        $search_query = null,
        $order_by = 'created',
        $sort_order = 'DESC',
        $search_column = null,
        $directories = null
    ) {
        $this->load->dbutil();
        $this->db->select(array(
            'process_started_at AS started',
            'device_serial_number AS serial_number',
            'result',
            'created AS completed',
            'device_size AS size',
            'process_integrity AS integrity',
            'device_product_name AS device_model',
            'geometry_total_sec AS sectors',
            'results_errors AS errors'
        ));

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories) && is_array($directories) && count($directories) > 0 && $directories[0] !== '') {
            $this->db->where_in('parent_dir', $directories);
        }

        $this->db->order_by($order_by, $sort_order);
        $query = $this->db->get('nebula');

        return $this->dbutil->csv_from_result($query);
    }

    public function scan(
        $search_query = null,
        $search_column = null,
        $directories,
        $verifier,
        $verify_timestamp
    ) {
        $this->db->group_start()
            ->where('status', null)
            ->or_where('status', '')
            ->group_end();

        if (isset($search_query) && $search_query !=='') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories) && is_array($directories) && count($directories) > 0 && $directories[0] !== '') {
            $this->db->where_in('parent_dir', $directories);
        }

        $this->db->update('nebula', array(
            'status' => 'Scanned',
            'status_last_edited' => $verify_timestamp,
            'status_last_edited_by' => $verifier
        ));

        return $this->db->affected_rows();
    }

    function directory_names($search_query = null, $search_column = null, $directories = null) {
        $this->db->select('parent_dir AS name')
                 ->distinct()
                 ->order_by('name', 'ASC');

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories)) {
            $this->db->where_in('parent_dir', $directories);
        }

        $query = $this->db->get('nebula');

        return $query->result();
    }

    function directory_totals($search_query, $search_column, $directory) {
        $this->db->select('COUNT(*) AS total')
                 ->where('parent_dir', $directory);

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        $query = $this->db->get('nebula');

        return $query->row();
    }

    function directory_current_date_totals($search_query, $search_column, $directory) {
        $this->db->select('COUNT(*) AS total')
                 ->where('parent_dir', $directory)
                 ->where('created >=', '(DATE(NOW()) - INTERVAL 1 DAY)', false);

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        $query = $this->db->get('nebula');

        return $query->row();
    }

    function device_results($search_query = null, $search_column = null, $directories = null) {
        $this->db->select('result')
                 ->distinct();

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories)) {
            $this->db->where_in('parent_dir', $directories);
        }

        $query = $this->db->get('nebula');

        return $query->result();
    }

    function device_sizes($search_query = null, $search_column = null, $directories = null, $device_result = null) {
        $this->db->select(array(
            'COUNT(*) AS device_count',
            'device_size'
        ));

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories)) {
            $this->db->where_in('parent_dir', $directories);
        }

        if (isset($device_result)) {
            $this->db->where('result', $device_result);
        }

        $query = $this->db->group_by('device_size')
                          ->order_by('device_count', 'DESC')
                          ->get('nebula');

        return $query->result();
    }

    function devices_verified($search_query = null, $search_column = null, $directories = null) {
        $this->db->select(array(
            'COUNT(*) AS verified',
            'device_size',
            'result'
        ));

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($directories)) {
            $this->db->where_in('parent_dir', $directories);
        }

        $this->db->where('status', 'Scanned');

        $query = $this->db->group_by('result')
            ->group_by('device_size')
            ->get('nebula');

        return $query->result();
    }

    function update_disposition($serial_numbers, $disposition) {
        $this->db->set('disposition', $disposition)
                 ->where_in('device_serial_number', $serial_numbers)
                 ->update('nebula');

        return $this->db->affected_rows();
    }

    function batch_assign_cost_sell($serial_numbers, $assignment_option, $value) {
        $this->db->where_in('device_serial_number', $serial_numbers)
            ->update('nebula', array( $assignment_option => $value ));

        return $this->db->affected_rows();
    }

    function purchase_orders() {
        $query = $this->db->select(array('name', 'vendor'))
            ->get('nebula_purchase_orders');

        return $query->result();
    }

    function update($serial_number, $data) {
        $this->db->set($data)
                 ->where('device_serial_number', $serial_number)
                 ->update('nebula');

        return $this->db->affected_rows();
    }

    function check_lot_exists($directory, $customer_code) {
        $query = $this->db->get_where('nebula_lot', array(
            'purchase_order' => $directory,
            'customer_code' => $customer_code
        ));

        return $query->num_rows();
    }

    function check_purchase_order_exists($name) {
        $query = $this->db->get_where('nebula_purchase_orders', array(
            'name' => $name
        ));

        return $query->num_rows();
    }

    function find_purchase_order($purchase_order) {
        $query = $this->db->select(array( 'name', 'location' ))
            ->where('name', $purchase_order)
            ->get('nebula_purchase_orders');

        return $query->row();
    }

    public function find_purchase_order_with_vendor($vendor) {
        $query = $this->db->select('name')
            ->where('vendor', $vendor)
            ->get('nebula_purchase_orders');

        return $query->row();
    }

    function create_purchase_order($location, $name, $vendor) {
        $this->db->insert('nebula_purchase_orders', array(
            'location' => $location,
            'name' => $name,
            'vendor' => isset($vendor) && $vendor !== '' ? $vendor : null
        ));
    }

    function rename_purchase_order($old_name, $new_name) {
        $this->db->where('name', $old_name)
            ->update('nebula_purchase_orders', array( 'name' => $new_name ));
    }

    function change_lot_purchase_orders($old_name, $new_name) {
        $this->db->where('purchase_order', $old_name)
            ->update('nebula_lot', array( 'purchase_order' => $new_name ));
    }

    function create_lot($directory, $customer_code) {
        $this->db->insert('nebula_lot', array(
            'purchase_order' => $directory,
            'customer_code' => $customer_code
        ));
    }

    function rename_lot($purchase_order, $old_customer_code, $new_customer_code) {
        $this->db->where('customer_code', $old_customer_code)
            ->where('purchase_order', $purchase_order)
            ->update('nebula_lot', array( 'customer_code' => $new_customer_code ));
    }

    function load_lots($directories = null) {
        $this->db->select(array( 'purchase_order', 'customer_code' ));

        if (isset($directories) && is_array($directories) && count($directories) > 0) {
            $this->db->where_in('purchase_order', $directories);
        }

        $query = $this->db->order_by('purchase_order')
            ->get('nebula_lot');

        return $query->result();
    }

    function assign_lot($lot, $reports) {
        $this->db->where_in('device_serial_number', $reports)
            ->update('nebula', array('lot' => $lot));

        return $this->db->affected_rows();
    }

    public function find_serial_number(string $serial_number) {
        return $this->db->where('device_serial_number', $serial_number)
            ->from('nebula')->count_all_results();
    }
}
?>
