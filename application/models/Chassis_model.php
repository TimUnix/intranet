<?php
class Chassis_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        parent::__construct();
    }


    /*public function get_sku($sku) {
       $query = $this->db->query("select * from data_import_listing where sku = '$sku'");

       return $query->result();
       }*/
       public function search_chassis($part_number){
        $query = $this->db->query("select * from chassis_new where part_number = '$part_number'");

        return $query->result();
       }
       public function search_motherboard($motherboard){
        $query = $this->db->query("select * from motherboard where model = '$motherboard'");

        return $query->result();
       }
       public function get_cpu_motherboard($cpu){
        $query = $this->db->query("select * from cpu_new where part = '$cpu'");

        return $query->result();
       }
       public function get_memory_motherboard($memory){
        $query = $this->db->query("select * from memory where memory_family = '$memory'");

        return $query->result();
       }
       public function get_motherboard($qty_cpu){
        $query = $this->db->query("select * from motherboard where qty_cpu = '$qty_cpu'");

        return $query->result();
       }
       public function get_mounting(){
        $query = $this->db->query("select * from mounting");

        return $query->result();
       }
       public function get_drives(){
        $query = $this->db->query("select * from drives");

        return $query->result();
       }
       public function get_cpu(){
        $query = $this->db->query("select * from cpu_new");

        return $query->result();
       }
       public function get_memory(){
        $query = $this->db->query("select * from memory");

        return $query->result();
       }
       public function insert_new_order($data){
        
			$this->db->insert("work_orders",$data);
			return $this->db->insert_id();
		
       }
       public function get_display($barcode){
        $query = $this->db->query("select * from work_orders where barcode = '$barcode'");

        return $query->result();
       }
       public function delete_order($part_number, $barcode){
        $this->db->query("delete from work_orders where chassis = '$part_number' and barcode = '$barcode'");
        $query = $this->db->query("select * from work_orders where barcode = '$barcode'");

        return $query->result();
       }
       public function view_work_order($part_number, $barcode, $created){
        $query = $this->db->query("select * from work_orders where chassis = '$part_number' and barcode = '$barcode' and created_date = '$created'");

        return $query->result();
       }
       public function get_harddrive($sku){
        $query = $this->db->query("select * from drives where sku = '$sku'");

        return $query->result();
       }
       public function get_motherboard_modal($qty_cpu){
        $query = $this->db->query("select * from motherboard where qty_cpu = '$qty_cpu'");

        return $query->result();
       }
       public function get_all_motherboards(){
        $query = $this->db->query("select * from motherboard");

        return $query->result();
       }
       public function get_cpu_modal($part){
        $query = $this->db->query("select * from cpu_new where part = '$part'");

        return $query->result();
       }
       public function get_memory_modal($sku){
        $query = $this->db->query("select * from memory where sku = '$sku'");

        return $query->result();
       }
       public function update_order($data, $barcode, $chassis){
           
        $this->db->where('barcode',$barcode);
        $this->db->where('chassis',$chassis);
		$this->db->update("work_orders",$data);
		return $key;
       }
       public function search_barcode($barcode){
        $query = $this->db->query("select * from work_orders where barcode = '$barcode'");

        return $query->result();
       }
       public function get_work_orders(){
        $query = $this->db->query("select * from work_orders");

        return $query->result();
       }
       public function get_orderby_work_orders($orderby, $search, $sortOrder){
           if($search == ""){
            $query = $this->db->order_by($orderby, $sortOrder);
            $query = $this->db->get('work_orders');
           }else{
            $this->db->where('barcode', $search);
            $query = $this->db->order_by($orderby, $sortOrder);
            $query = $this->db->get('work_orders');
           }
        
           return $query->result();
       }
       public function export_csv_work_orders( $search,
       $sortOrder,
       $orderby){
        $this->load->dbutil();


        if($search == ""){
            $query = $this->db->order_by($orderby, $sortOrder);
            $query = $this->db->get('work_orders');
           }else{
            $this->db->where('barcode', $search);
            $query = $this->db->order_by($orderby, $sortOrder);
            $query = $this->db->get('work_orders');
           }

        return $this->dbutil->csv_from_result($query);
       }
       function get_field()
        {
            $fields = $this->db->list_fields('chassis_new');
            return $fields;
        }
    public function get_chassis()
    {
        $query = $this->db->query("select * from chassis_new");

        return $query->result();
    }
    public function get_user_id($email)
    {
        $query = $this->db->query("select * from users where email = '$email'");

        return $query->result();
    }
    public function get_search_exist($user_id, $search){
        $query = $this->db->query("select * from search_history where user_id = '$user_id' and search = '$search'");

        return $query->result();
    }
    public function update_chassis_search($user_id, $search, $data){
        $this->db->where('user_id', $user_id);
        $this->db->where('search', $search);
        $this->db->update("search_history", $data);
        /// return this if incase there are error return $key;
    }
    public function insert_chassis_search($data){
        $this->db->insert("search_history",$data);
			return $this->db->insert_id();
    }
    public function get_chassis_search($partnumber, $sortOrder, $orderBy){
        $this->db->like('part_number', $partnumber);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('chassis_new');

        return $query->result();
    }
    public function generate_csv($partnumber, $sortOrder, $orderBy){
        $this->load->dbutil();
        $this->db->like('part_number', $partnumber);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('chassis_new');

        return $this->dbutil->csv_from_result($query);
    }
    public function get_search_history_for_user($user_id){
        $query = $this->db->query("select * from search_history where user_id = '$user_id'");

        return $query->result();
    }


    public function update_chassiscol($data, $stepcode, $partnumber){
        $this->db->where('part_number', $partnumber);
        //$this->db->where('stepcode', $stepcode);
        $this->db->update("chassis_new", $data);
        return $key;
    }


    public function delete_chassiscol($partnumber, $stepcode){
        $this->db->where('part_number', $partnumber);
        ///$this->db->where('stepcode', $stepcode);
        $this->db->delete('chassis_new');
        return 1;
    }


    public function insert_new_chassis($data){
        $this->db->insert("chassis_new",$data);
        return $this->db->insert_id();
    }
    



    function update_request($data, $listing_id, $key)
    {
        $this->db->where($listing_id, $key);
        $this->db->update("listing_table", $data);
        return $key;
    }

    public function check_chassis_part($partnumber){
        $querypart = $this->db->query("select * from chassis_new where part_number = '$partnumber'");
        ///$querystep = $this->db->query("select * from chassis_new where stepcode = '$stepcode'");

        if($querypart->num_rows() > 1){
            return "PartNumber is Already Exist";
        }else{
            
                return "Not Found";
            
        }
    }

    

    

    

    

    
}
