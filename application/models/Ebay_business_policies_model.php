<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ebay_business_policies_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function load_policies_by_store_and_type($store, $type) {
        $this->db->select(array(
            'policy_id',
            'name'
        ))->where('store', $store)
             ->where('policy_type', $type);

        $query = $this->db->get('ebay_business_policies');

        return $query->result();
    }

    function load_default_by_store_and_type($store, $type) {
        $this->db->select(array(
            'policy_id',
            'name'
        ))->where('store', $store)
                 ->where('policy_type', $type)
                 ->where('default', 1)
                 ->limit(1, 0);

        $query = $this->db->get('ebay_business_policies');

        return $query->row();
    }
}

?>
