<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_categories_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function load_all() {
        $query = $this->db->select(array(
            'ebay_id',
            'name'
        ))->order_by('name')
                          ->get('ebay_categories');

        return $query->result();
    }
}

?>
