<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Server_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

	public function get(
		$searchItem = null,
		$searchSelected = null,
		$order_by,
		$sort_order,
		$items_offset = 0
	){
		$this->db->select(array(
		'server_id',
		'sku',
		'con',
		'units',
		'brand',
		'model',
		'nodes',
		'bays',
		'drive_ff',
		'backplane',
		'socket_supported',
		'qty_of_power_supply'
		));

		if(isset($searchItem) && $searchItem != ''){
			$this->db->like($searchSelected, $searchItem);
		}

		if (isset($order_by) && isset($sort_order)) {
			$this->db->order_by($order_by, $sort_order);
		} else {
			$this->db->order_by('server_id');
		}

		$this->db->limit(100, $items_offset);

		$query = $this->db->get('server');
		return $query->result();
	}

	function load_sku($sku) {
        $query = $this->db->select(array(
            'server_id',
			'sku',
			'con',
			'units',
			'brand',
			'model',
			'nodes',
			'bays',
			'drive_ff',
			'backplane',
			'socket_supported',
			'qty_of_power_supply'
        ))->where('sku', $sku)
                          ->get('server');

        return $query->row();
    }

    function insert_listing($data)
    {
        $this->db->insert("server", $data);
        return $this->db->insert_id();
    }

    function show_added_listing(){

        $this->db->select(array(
			'server_id',
			'sku',
			'con',
			'units',
			'brand',
			'model',
			'nodes',
			'bays',
			'drive_ff',
			'backplane',
			'socket_supported',
			'qty_of_power_supply'
        ));
        
        $query = $this->db->get('server');

        return $query->result();  
    }

    function count(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
       
        $this->db->select('COUNT(*) AS listing_count');
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
            $this->db->select('COUNT(*) AS listing_count');
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('server', 1, 0);
        return $query->row();
    }

	function insert_server($data)
    {
        $this->db->insert("server", $data);
        return $this->db->insert_id();
    }

	function show_added_server(){

        $this->db->select(array(
			'server_id',
			'sku',
			'con',
			'units',
			'brand',
			'model',
			'nodes',
			'bays',
			'drive_ff',
			'backplane',
			'socket_supported',
			'qty_of_power_supply'
        ));
        
        $query = $this->db->get('server');

        return $query->result();  
    }

	public function update_servercol(
        $data,  
        $sku,
        $key = null){
        $this->db->where('sku', $sku);
        $this->db->update("server", $data);
        return $key;
    }

	public function export_csv(
		$searchItem = null,
		$searchSelected = null,
		$order_by = null,
		$sort_order = null
	
		) {
			$this->load->dbutil();
	
			$this->db->select(array(
				'server_id',
				'sku',
				'con',
				'units',
				'brand',
				'model',
				'nodes',
				'bays',
				'drive_ff',
				'backplane',
				'socket_supported',
				'qty_of_power_supply'	
			));
			if(isset($searchItem) && $searchItem != ''){
				$this->db->like($searchSelected, $searchItem);
			}
	
			if (isset($order_by) && isset($sort_order)) {
				$this->db->order_by($order_by, $sort_order);
			}
	
			$query = $this->db->get('server');
	
			return $this->dbutil->csv_from_result($query);
		}
}

?>
