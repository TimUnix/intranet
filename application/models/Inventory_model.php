<?php

class Inventory_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_categories() {
        $this->db->select('category')
                 ->distinct()
                 ->where('qty >', 0)
                 ->where('arc', 'False')
                 ->order_by('category', 'ASC');
        $query = $this->db->get('collect');

        return $query->result();
    }

    public function get_conditions() {
        $this->db->select('con')
                 ->distinct()
                 ->where('arc', 'False')
                 ->where('qty >', 0)
                 ->order_by('con', 'ASC');

        $query = $this->db->get('collect');

        return $query->result();
    }

    public function get_filtered_items(
        $search,
        $category_filters,
        $condition_filters,
        $listing_filter = 'all',
        $is_qty_zero = false,
        $sort_order = 'DESC',
        $order_by = 'collect.qty',
        $items_offset = 0
    ) {
        $this->db->select(array(
            'collect.part',
            'collect.barcode_num',
            'collect.des',
            'collect.con',
            'collect.category',
            'collect.price',
            'collect.qty',
            'collect.bin',
            '(collect.qty * collect.price) AS tot',
            'GROUP_CONCAT(images.img SEPARATOR \',\') as images',
            'ebay_items.seller',
            'ebay_items.link',
            'keywords.keyword_prase',
            'keywords.edited_by',
            'keywords.edited_date',
            'inventory_velocity.velocity_difference'

        ));

        $this->db->join(
            'images',
            'ON(collect.part = images.par)',
            'left'
        );

        $this->db->join(
            'ebay_items',
            'ON(collect.part = ebay_items.sku)',
            'left'
        );

        $this->db->join(
            'keywords',
            'ON(collect.part = keywords.keyword_par)',
            'left'
        );

        $this->db->join(
            'inventory_velocity',
            'ON(collect.part = inventory_velocity.sku)',
            'left'
        );


        if (isset($search) && $search !== '') {
            $this->db->group_start()
                 ->like('collect.part', $search)
                 ->or_like('collect.barcode_num', $search)
                 ->or_like('collect.des', $search)
                 ->group_end();
        }

        if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
            $this->db->where_in('collect.category', $category_filters);
        }

        if (isset($condition_filters) && is_array($condition_filters) && count($condition_filters) > 0) {
            $this->db->where_in('collect.con', $condition_filters);
        }

        if ($listing_filter === 'unlisted') {
            $this->db->where('ebay_items.seller', null);
        }

        if ($listing_filter === 'listed') {
            $this->db->where('ebay_items.seller IS NOT NULL', null, false);
            // $this->db->where('ebay_items.seller 
        }

        $this->db->where('collect.arc', 'False');

        if (!$is_qty_zero) {
            $this->db->where('collect.qty >', 0);
        }

        if (isset($order_by) && $order_by !== "" && isset($sort_order) && $sort_order !== "") {
            $this->db->order_by($order_by, $sort_order);
        }

        $this->db->group_by('collect.part')
                 ->limit(100, $items_offset);

        $query = $this->db->get('collect');
   
        return $query->result();
	}

	// FILTER UPDATE PRICE
    public function get_updated_prices($filter) {
		$this->db->select(array(
            'images.img',
            'collect.part',
            'collect.des',
            'collect.con',
            'collect.price',
            'update_price.new_pri',
            'update_price.notes',
            'collect.qty',
            '(collect.price * collect.qty) AS tot'
        ));

		$this->db->join(
            'update_price',
            'ON(update_price.par = collect.part)',
            'left'
        );
        
        $this->db->join(
            'images',
            'ON(images.par = collect.part)',
            'left'
        );
        
		$this->db->where('collect.category', $filter);
        $this->db->where('images.main', 'True');
        $this->db->order_by('tot', 'DESC');
        
		$query = $this->db->get('collect');
        
		return $query->result();
	}

    public function get_summary(
        $search,
        $category_filters,
        $condition_filters,
        $listing_filter = 'all',
        $is_qty_zero = false
    ) {
        $this->db->select(array(
            'part',
           // 'COUNT(*) AS total',
           // 'SUM(FLOOR(collect.qty * collect.price)) AS price',
           'collect.qty as qty',
            'collect.price',
            'ebay_items.seller'
        ));
    
        $this->db->join(
            'ebay_items',
            'ON(collect.part = ebay_items.sku)',
            'left'
        );

        if (isset($search) && $search !== '') {
            $this->db->group_start()
                 ->like('collect.part', $search)
                 ->or_like('collect.barcode_num', $search)
                 ->or_like('collect.des', $search)
                 ->group_end();
        }

        if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
            $this->db->where_in('collect.category', $category_filters);
        }

        if (isset($condition_filters) && is_array($condition_filters) && count($condition_filters) > 0) {
            $this->db->where_in('collect.con', $condition_filters);
        }

        $this->db->where('collect.arc', 'False');

        if ($listing_filter === 'unlisted') {
            $this->db->where('ebay_items.seller', null);
        }

        if ($listing_filter === 'listed') {
            $this->db->where('ebay_items.seller IS NOT NULL', null, false);
        }

        if (!$is_qty_zero) {
            $this->db->where('collect.qty >', 0);
        }
        $this->db->group_by('part');

        $query = $this->db->get('collect');
 
        return $query->result();
    }

    public function get_local_items($part_number) {
        $this->db->select(array(
            'collect.part',
            'collect.des',
            'collect.con',
            'collect.price',
            'collect.qty',
            '(collect.qty * collect.price) AS tot',
            'images.img'
        ));

        $this->db->join(
            'images',
            'ON(collect.part = images.par)',
            'left'
        );

        $this->db->like('collect.part', $part_number);

        $this->db->where('collect.qty >', 0);
        $this->db->where('collect.arc', 'False');
        $this->db->where('images.main', 'True');

        //$this->db->order_by('tot', 'DESC');

        $query = $this->db->get('collect');

        return $query->result();
    }

    public function get_inventory($data){
        $query = $this->db->query("select DISTINCT $data as $data from collect where arc = 'False' and qty > '0' order by $data asc");

        return $query->result();
    }
    
    public function get_item_by_cat($cat_con, $par){
        if($cat_con == "condition_zero_qty"){
            $query = $this->db->query("select * from collect where part like '%$par%' and arc = 'False' and qty = '0'");
        }else{
            $query = $this->db->query("select * from collect where category = '$cat_con' or con = '$cat_con' and part like '%$par%' and arc = 'False' and qty > '0'");
        }

        return $query->result();
    }

    public function get_item_only($data){
        $query = $this->db->query("select * from collect where part like '%$data%'");

        return $query->result();
    }

    public function get_filtered_items_for_inventory_search(
        $search,
        $is_qty_zero
        ///$sort_order,
        ///$order_by
    ) {
        $this->db->select(array(
            'collect.part',
            'collect.barcode_num',
            'collect.des',
            'collect.con',
            'collect.category',
            'collect.price',
            'collect.qty',
            'collect.bin',
            '(collect.qty * collect.price) AS tot',
            'images.img',
            'ebay.ebay_unixpluscom',
            'ebay.ebay_unixsurplusnet'
        ));

        $this->db->join(
            'images',
            'ON(collect.part = images.par)',
            'left'
        );

        $this->db->join(
            'ebay',
            'ON(collect.part = ebay.par)',
            'left'
        );

        if (isset($search) && $search !== '') {
            $this->db->group_start()
                     ->like('collect.part', $search)
                     ->or_like('collect.barcode_num', $search)
                     ->or_like('collect.des', $search)
                     ->group_end();
        }

        if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
            $this->db->where_in('collect.category', $category_filters);
        }

        if (isset($condition_filters) && is_array($condition_filters) && count($condition_filters) > 0) {
            $this->db->where_in('collect.con', $condition_filters);
        }

        $this->db->where('collect.arc', 'False');
        $this->db->where('images.main', 'True');
        

        if (!$is_qty_zero) {
            $this->db->where('collect.qty >', 0);
        }

        ///$this->db->order_by($order_by, $sort_order);

        $query = $this->db->get('collect');

        return $query->result();
	}

    public function get_filtered_items_for_inventory_search_get_zero(
        $search,
        $category_filters,
        $condition_filters,
        $is_qty_zero
        ///$sort_order,
        ///$order_by
    ) {
        $this->db->select(array(
            'collect.part',
            'collect.barcode_num',
            'collect.des',
            'collect.con',
            'collect.category',
            'collect.price',
            'collect.qty',
            'collect.bin',
            '(collect.qty * collect.price) AS tot',
            'images.img',
            'ebay.ebay_unixpluscom',
            'ebay.ebay_unixsurplusnet'
        ));

        $this->db->join(
            'images',
            'ON(collect.part = images.par)',
            'left'
        );

        $this->db->join(
            'ebay',
            'ON(collect.part = ebay.par)',
            'left'
        );

        if (isset($search) && $search !== '') {
            $this->db->group_start()
                     ->like('collect.part', $search)
                     ->or_like('collect.barcode_num', $search)
                     ->or_like('collect.des', $search)
                     ->group_end();
        }

        /*if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
          $this->db->where_in('inventory.cat', $category_filters);
          }

          if (isset($condition_filters) && is_array($condition_filters) && count($condition_filters) > 0) {
          $this->db->where_in('inventory.con', $condition_filters);
          }*/

        $this->db->where('collect.arc', 'False');
        $this->db->where('images.main', 'True');
        
        //$zero_qty = in_array('condition_Zero_QTY', $condition_filters,true);echo "-------------$zero_qty--";
        $num = 0;

        $this->db->group_start();
        $this->db->where_in('collect.category', $condition_filters);
        $this->db->or_where_in('collect.con', $condition_filters);
        $this->db->group_end();


        if (!in_array('condition_Zero_QTY', $condition_filters, true)){
            $num = 1;
            $this->db->where('collect.qty >', 0);
        }
        ///$this->db->order_by($order_by, $sort_order);

        $query = $this->db->get('collect');

        return $query->result();
	}    
    
    public function search_sku_suggestions($search) {
        $this->db->select('part')
             ->like('part', $search)
             ->limit(10, 0);

        $query = $this->db->get('collect');

        return $query->result();
    }

    public function update_inventory_desc($data, $sku){
        $this->db->where('part', $sku);
        $this->db->update('collect', $data);

	 	return $this->db->insert_id();
    }

    public function insert_linn_new_desc($insert_data){
        $this->db->insert('linn_update_desc', $insert_data);
        return $this->db->insert_id();
    }

    public function insert_linn_price($insert_data){
        $this->db->insert('linn_update_price', $insert_data);
        return $this->db->insert_id();
    }
    
    public function drives_title(){
        $par =$this->input->post('par');
        $query = $this->db->select(array(
            'sku',
            'bar',
            'upc',
            'brand',
            'ff',
            'size',
            'type',
            'rpm',
            'speed',
            'interface',
            'series',
            'ext_inf',
            'con'
        ))->where('sku', $par)
                          ->get('drives');

        return $query->row();
    }
    public function get_actions() {
        $this->db->select();

        $query = $this->db->get('actions');

        return $query->result();
    }

    public function add_task($data)
    {
        $this->db->insert("task", $data);
        return $this->db->insert_id();
    }

    public function get_exclude_condition($request){
		$this->db->select('exclude_condition');
        $this->db->where('action_id',$request);
        $query = $this->db->get('actions');
		
        return $query->result();
    }
    public function check_sku($par)
    {
        $this->db->select('sku');
        $this->db->where('sku',$par);
        $query = $this->db->get('drives');
		
        return $query->result();
    }

    public function export_csv(
        $search,
        $category_filters = null,
        $condition_filters = null,
        $listing_filter,
        $is_qty_zero = false,
        $sort_order,
        $order_by
    ){
        $this->load->dbutil();

        $this->db->select(array(
            'collect.part AS Part Number',
            'collect.barcode_num AS OEM PN',
            'collect.des AS Description',
            'collect.con AS Con',
            'collect.category AS Category',
            'collect.price AS Price',
            'collect.qty AS QTY',
            'inventory_velocity.velocity_difference AS Velocity',
            '(collect.qty * collect.price) AS Total',
            'collect.bin AS Bin',
            'ebay_items.seller AS Channel',
            
        ));

        $this->db->join(
            'images',
            'ON(collect.part = images.par)',
            'left'
        );

        $this->db->join(
            'ebay_items',
            'ON(collect.part = ebay_items.sku)',
            'left'
        );

        $this->db->join(
            'keywords',
            'ON(collect.part = keywords.keyword_par)',
            'left'
        );

        $this->db->join(
            'inventory_velocity',
            'ON(collect.part = inventory_velocity.sku)',
            'left'
        );

        if (isset($search) && $search !== '') {
            $this->db->group_start()
                 ->like('collect.part', $search)
                 ->or_like('collect.barcode_num', $search)
                 ->or_like('collect.des', $search)
                 ->group_end();
        }

        if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
            $this->db->where_in('collect.category', $category_filters);
        }

        if (isset($condition_filters) && is_array($condition_filters) && count($condition_filters) > 0) {
            $this->db->where_in('collect.con', $condition_filters);
        }

        if ($listing_filter === 'unlisted') {
            $this->db->where('ebay_items.seller', null);
        }

        if ($listing_filter === 'listed') {
            $this->db->where('ebay_items.seller IS NOT NULL', null, false);
            // $this->db->where('ebay_items.seller 
        }

        $this->db->where('collect.arc', 'False');

        if (!$is_qty_zero) {
            $this->db->where('collect.qty >', 0);
        }

        if (isset($order_by) && $order_by !== "" && isset($sort_order) && $sort_order !== "") {
            $this->db->order_by($order_by, $sort_order);
        }

        $this->db->group_by('collect.part');

        $query = $this->db->get('collect');
 
        return $this->dbutil->csv_from_result($query);
    }

    public function export_csv_gdrive(
        $search,
        $category_filters = null,
        $condition_filters = null,
        $listing_filter,
        $is_qty_zero = false,
        $sort_order,
        $order_by
    ){
        $this->load->dbutil();

        $this->db->select(array(
            'collect.part AS Part Number',
            'collect.barcode_num AS OEM PN',
            'collect.des AS Description',
            'collect.con AS Con',
            'collect.category AS Category',
            'collect.price AS Price',
            'collect.qty AS QTY',
            'inventory_velocity.velocity_difference AS Velocity',
            '(collect.qty * collect.price) AS Total',
            'collect.bin AS Bin',
            'ebay_items.seller AS Channel',
            
        ));

        $this->db->join(
            'images',
            'ON(collect.part = images.par)',
            'left'
        );

        $this->db->join(
            'ebay_items',
            'ON(collect.part = ebay_items.sku)',
            'left'
        );

        $this->db->join(
            'keywords',
            'ON(collect.part = keywords.keyword_par)',
            'left'
        );

        $this->db->join(
            'inventory_velocity',
            'ON(collect.part = inventory_velocity.sku)',
            'left'
        );

        if (isset($search) && $search !== '') {
            $this->db->group_start()
                 ->like('collect.part', $search)
                 ->or_like('collect.barcode_num', $search)
                 ->or_like('collect.des', $search)
                 ->group_end();
        }

        if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
            $this->db->where_in('collect.category', $category_filters);
        }

        if (isset($condition_filters) && is_array($condition_filters) && count($condition_filters) > 0) {
            $this->db->where_in('collect.con', $condition_filters);
        }

        if ($listing_filter === 'unlisted') {
            $this->db->where('ebay_items.seller', null);
        }

        if ($listing_filter === 'listed') {
            $this->db->where('ebay_items.seller IS NOT NULL', null, false);
            // $this->db->where('ebay_items.seller 
        }

        $this->db->where('collect.arc', 'False');

        if (!$is_qty_zero) {
            $this->db->where('collect.qty >', 0);
        }

        if (isset($order_by) && $order_by !== "" && isset($sort_order) && $sort_order !== "") {
            $this->db->order_by($order_by, $sort_order);
        }

        $this->db->group_by('collect.part');

        $query = $this->db->get('collect');
 
        return $query->result();
    }
}
?>
