<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_exclude_condition($action_id){
		$query = $this->db->query("SELECT exclude_condition	 FROM actions WHERE action_id = '$action_id'");
		
        return $query->result();
    }
    
    public function check_sku_and_task($sku, $request){
        $query = $this->db->query("select * from task where sku = '$sku' and request = '$request' and is_archived = 0");

        return $query->result();
    }

    public function check_sku($sku)
    {
        $query = $this->db->query("select * from task where sku = '$sku'");

        return $query->result();
    }

    function insert_listing($data)
    {
        $this->db->insert("task", $data);
        return $this->db->insert_id();
    }

    function update_request($data, $listing_id, $key)
    {
        $this->db->where($listing_id, $key);
        $this->db->update("task", $data);
        return $key;
    }

    function show_added_listing(
        $order_by = 'date_requested',
        $sort_order = 'ASC',
        $actions = null,
        $banner = null
    ) {
        $this->db->select(array(
            'task.listing_id AS listing_id',
            'task.request AS request_id',
            'task.date_requested AS date_requested',
            'task.request_by AS request_by',
            'task.sku AS sku',
            'task.sku_last_edited AS sku_last_edited',
            'task.sku_last_edited_by AS sku_last_edited_by',
            'task.price AS price',
            'task.promotion AS promotion',
            'task.promotion_last_edited AS promotion_last_edited',
            'task.promotion_last_edited_by AS promotion_last_edited_by',
            'task.volume_discount_2x AS volume_discount_2x',
            'task.volume_discount_2x_last_edited AS volume_discount_2x_last_edited',
            'task.volume_discount_2x_last_edited_by AS volume_discount_2x_last_edited_by',
            'task.volume_discount_3x AS volume_discount_3x',
            'task.volume_discount_3x_last_edited AS volume_discount_3x_last_edited',
            'task.volume_discount_3x_last_edited_by AS volume_discount_3x_last_edited_by',
            'task.volume_discount_4x AS volume_discount_4x',
            'task.volume_discount_4x_last_edited AS volume_discount_4x_last_edited',
            'task.volume_discount_4x_last_edited_by AS volume_discount_4x_last_edited_by',
            'task.notes AS notes',
            'task.banner AS banner',
            'task.shipping AS shipping',
            'task.draft_id AS draft_id',
            'task.draft_by AS draft_by',
            'task.draft_date AS draft_date',
            'task.listing_item_id AS listing_item_id',
            'task.listing_by AS listing_by',
            'task.listing_date AS listing_date',
            'task.weight AS weight',
            'task.weight_last_edited_by AS weight_last_edited_by',
            'task.request_last_edited_by AS request_last_edited_by',
            'task.price_last_edited_by AS price_last_edited_by',
            'task.shipping_last_edited_by AS shipping_last_edited_by',
            'task.banner_last_edited_by AS banner_last_edited_by',
            'task.request_last_edited_by AS request_last_edited_by',
            'task.notes_last_edited_by AS notes_last_edited_by',
            'task.request_last_edited AS request_last_edited',
            'task.price_last_edited AS price_last_edited',
            'task.shipping_last_edited AS shipping_last_edited',
            'task.weight_last_edited AS weight_last_edited',
            'task.banner_last_edited AS banner_last_edited',
            'task.notes_last_edited AS notes_last_edited',
            'task.photo AS photo',
            'task.photo_last_edited AS photo_last_edited',
            'task.photo_last_edited_by AS photo_last_edited_by',
            'task.moved AS moved',
            'task.moved_last_edited AS moved_last_edited',
            'task.moved_last_edited_by AS moved_last_edited_by',
            'task.lot AS lot',
            'task.lot_last_edited AS lot_last_edited',
            'task.lot_last_edited_by AS lot_last_edited_by',
            'task.qty_3 AS qty_3',
            'task.qty_3_last_edited AS qty_3_last_edited',
            'task.qty_3_last_edited_by AS qty_3_last_edited_by',
            'actions.action_name AS request',
            'inventory.cat AS category'
        ));

         $this->db->join(
            'inventory',
            'ON task.sku = inventory.par',
            'left'
        );

        $this->db->join(
            'actions',
            "ON task.request = actions.action_id AND task.listing_item_id = 0",
            'inner'
        );

        $this->db->order_by($order_by, $sort_order);

        if (isset($actions) || isset($banner) || isset($photo)) {
            $this->db->group_start();

            if (isset($actions)) {
                $this->db->where_in('request', $actions);
            }

            if (isset($banner)) {
                $this->db->where('banner', $banner);
            }

            if (isset($photo) && $photo !== '') {
                $this->db->group_start();
                $this->db->where('photo', $photo);

                if ($photo == 0) {
                    $this->db->or_where('photo', null);
                }

                $this->db->group_end();
            }

            $this->db->group_end();
        }

        $query = $this->db->get('task');

        return $query->result();
    }

    function update_draft($data, $table_col, $key)
    {
        $this->db->where($table_col, $key);
        $this->db->update("task", $data);
        return $key;
    }

    function update_listing($data, $table_col, $key)
    {
        $this->db->where($table_col, $key);
        $this->db->update("task", $data);
        return $key;
    }

    function get_request()
    {
        $query = $this->db->query("select * from actions");

        return $query->result();
    }

    function get_list()
    {
        $query = $this->db->query("SELECT task.listing_id, actions.action_name
            FROM task
            INNER JOIN actions
            ON task.request = actions.action_id
        ");

        return $query->result();
    }

    function archive($id, $notes, $user_name, $timestamp) {
        $data = array(
            'is_archived' => true,
            'archived_by' => $user_name,
            'date_archived' => $timestamp,
            'notes' => $notes,
            'notes_last_edited' => $timestamp,
            'notes_last_edited_by' => $user_name
        );
        return $this->db->where('listing_id', $id)
                        ->update('task', $data);
    }

    function export_csv(
        $search_query,
        $order_by = 'date_requested',
        $sort_order = 'ASC',
        $actions = null,
        $banner = null,
        $photo = null,
        $users = null,
        $search_column = 'sku',
        $is_done_shown = false,
        $is_archived_shown = false
    ) {
        $this->load->dbutil();

        $this->db->select(array(
            'task.listing_id AS listing_id',
            'task.request AS request_id',
            'actions.action_name AS request',
            'task.request_last_edited AS request_last_edited',
            'task.request_last_edited_by AS request_last_edited_by',
            'task.date_requested AS date_requested',
            'task.request_by AS request_by',
            'task.sku AS sku',
            'task.sku_last_edited AS sku_last_edited',
            'task.sku_last_edited_by AS sku_last_edited_by',
            'task.price AS price',
            'task.price_last_edited AS price_last_edited',
            'task.price_last_edited_by AS price_last_edited_by',
            'task.notes AS notes',
            'task.promotion AS promotion',
            'task.promotion_last_edited AS promotion_last_edited',
            'task.promotion_last_edited_by AS promotion_last_edited_by',
            'task.volume_discount_2x AS volume_discount_2x',
            'task.volume_discount_2x_last_edited AS volume_discount_2x_last_edited',
            'task.volume_discount_2x_last_edited_by AS volume_discount_2x_last_edited_by',
            'task.volume_discount_3x AS volume_discount_3x',
            'task.volume_discount_3x_last_edited AS volume_discount_3x_last_edited',
            'task.volume_discount_3x_last_edited_by AS volume_discount_3x_last_edited_by',
            'task.volume_discount_4x AS volume_discount_4x',
            'task.volume_discount_4x_last_edited AS volume_discount_4x_last_edited',
            'task.volume_discount_4x_last_edited_by AS volume_discount_4x_last_edited_by',
            'task.notes_last_edited AS notes_last_edited',
            'task.notes_last_edited_by AS notes_last_edited_by',
            'task.banner AS banner',
            'task.banner_last_edited AS banner_last_edited',
            'task.banner_last_edited_by AS banner_last_edited_by',
            'task.shipping AS shipping',
            'task.shipping_last_edited AS shipping_last_edited',
            'task.shipping_last_edited_by AS shipping_last_edited_by',
            'task.draft_id AS draft_id',
            'task.draft_by AS draft_by',
            'task.draft_date AS draft_date',
            'task.listing_item_id AS listing_item_id',
            'task.listing_by AS listing_by',
            'task.listing_date AS listing_date',
            'task.weight AS weight',
            'task.weight_last_edited_by AS weight_last_edited_by',
            'task.weight_last_edited AS weight_last_edited',
            'task.photo AS photo',
            'task.photo_last_edited AS photo_last_edited',
            'task.photo_last_edited_by AS photo_last_edited_by',
            'task.moved AS moved',
            'task.moved_last_edited AS moved_last_edited',
            'task.moved_last_edited_by AS moved_last_edited_by',
            'task.lot AS lot',
            'task.lot_last_edited AS lot_last_edited',
            'task.lot_last_edited_by AS lot_last_edited_by',
            'task.qty_3 AS qty_3',
            'task.qty_3_last_edited AS qty_3_last_edited',
            'task.qty_3_last_edited_by AS qty_3_last_edited_by',
            'task.is_archived AS archived',
            'task.archived_by AS archived_by',
            'task.date_archvied AS date_archived',
            'inventory.cat AS category'

        ));

        $this->db->join(
            'inventory',
            'ON task.sku = inventory.par',
            'left'
        );

        $this->db->join(
            'actions',
            'ON task.request = actions.action_id',
            'inner'
        );

        if (isset($search_query) && $search_query != '') {
            $this->db->like($search_column, $search_query);
        }

        if ($is_archived_shown) {
            $this->db->where('task.is_archived', true);
        } else if ($is_done_shown) {
            $this->db->where('task.listing_item_id !=', 0);
        } else {
            $this->db->where('task.listing_item_id', 0);
        }

        $this->db->order_by($order_by, $sort_order);

        if ((isset($actions) && $actions !== '') || (isset($photo) && $photo !== '')
            || (isset($banner) && $banner !== '')) {
            $this->db->group_start();

            if (isset($actions) && $actions !== '') {
                $this->db->where_in('request', $actions);
            }

            foreach ($users as $user) {
                $this->db->or_like('task.lot_last_edited_by', $user)
                         ->or_like('request_last_edited_by', $user)
                         ->or_like('task.price_last_edited_by', $user)
                         ->or_like('task.notes_last_edited_by', $user)
                         ->or_like('task.banner_last_edited_by', $user)
                         ->or_like('task.shipping_last_edited_by', $user)
                         ->or_like('task.weight_last_edited_by', $user)
                         ->or_like('task.photo_last_edited_by', $user)
                         ->or_like('task.moved_last_edited_by', $user)
                         ->or_like('task.lot_last_edited_by', $user)
                         ->or_like('task.draft_by', $user)
                         ->or_like('task.request_by', $user)
                         ->or_like('task.listing_by', $user);
                }

            if (isset($banner) && $banner !== '') {
                $this->db->where('banner', $banner);
            }

            if (isset($photo) && $photo !== '') {
                $this->db->group_start();
                $this->db->where('photo', $photo);

                if ($photo == 0) {
                    $this->db->or_where('photo', null);
                }

                $this->db->group_end();
            }

            $this->db->group_end();
        }

        $query = $this->db->get('task');

        return $this->dbutil->csv_from_result($query);
    }

    function load(
        $search_query,
        $order_by = 'date_requested',
        $sort_order = 'ASC',
        $actions = null,
        $banner = null,
        $photo = null,
        $users = null,
        $categories = null,
        $search_column = 'sku',
        $is_done_shown = false,
        $is_archived_shown = false
    ) {
        $this->db->select(array(
            'task.listing_id AS listing_id',
            'task.request AS request_id',
            'task.request_last_edited AS request_last_edited',
            'task.request_last_edited_by AS request_last_edited_by',
            'task.date_requested AS date_requested',
            'task.request_by AS request_by',
            'task.sku AS sku',
            'task.sku_last_edited AS sku_last_edited',
            'task.sku_last_edited_by AS sku_last_edited_by',
            'task.price AS price',
            'task.price_last_edited AS price_last_edited',
            'task.price_last_edited_by AS price_last_edited_by',
            'task.promotion AS promotion',
            'task.promotion_last_edited AS promotion_last_edited',
            'task.promotion_last_edited_by AS promotion_last_edited_by',
            'task.volume_discount_2x AS volume_discount_2x',
            'task.volume_discount_2x_last_edited AS volume_discount_2x_last_edited',
            'task.volume_discount_2x_last_edited_by AS volume_discount_2x_last_edited_by',
            'task.volume_discount_3x AS volume_discount_3x',
            'task.volume_discount_3x_last_edited AS volume_discount_3x_last_edited',
            'task.volume_discount_3x_last_edited_by AS volume_discount_3x_last_edited_by',
            'task.volume_discount_4x AS volume_discount_4x',
            'task.volume_discount_4x_last_edited AS volume_discount_4x_last_edited',
            'task.volume_discount_4x_last_edited_by AS volume_discount_4x_last_edited_by',
            'task.notes AS notes',
            'task.notes_last_edited AS notes_last_edited',
            'task.notes_last_edited_by AS notes_last_edited_by',
            'task.banner AS banner',
            'task.banner_last_edited AS banner_last_edited',
            'task.banner_last_edited_by AS banner_last_edited_by',
            'task.shipping AS shipping',
            'task.shipping_last_edited AS shipping_last_edited',
            'task.shipping_last_edited_by AS shipping_last_edited_by',
            'task.draft_id AS draft_id',
            'task.draft_by AS draft_by',
            'task.draft_date AS draft_date',
            'task.listing_item_id AS listing_item_id',
            'task.listing_by AS listing_by',
            'task.listing_date AS listing_date',
            'task.weight AS weight',
            'task.weight_last_edited_by AS weight_last_edited_by',
            'task.weight_last_edited AS weight_last_edited',
            'task.photo AS photo',
            'task.photo_last_edited AS photo_last_edited',
            'task.photo_last_edited_by AS photo_last_edited_by',
            'task.moved AS moved',
            'task.moved_last_edited AS moved_last_edited',
            'task.moved_last_edited_by AS moved_last_edited_by',
            'task.lot AS lot',
            'task.lot_last_edited AS lot_last_edited',
            'task.lot_last_edited_by AS lot_last_edited_by',
            'task.qty_3 AS qty_3',
            'task.qty_3_last_edited AS qty_3_last_edited',
            'task.qty_3_last_edited_by AS qty_3_last_edited_by',
            'task.archived_by',
            'task.date_archived',
            'actions.action_name AS request',
            'inventory.cat AS category'
        ));

        $this->db->join(
            'inventory',
            'ON task.sku = inventory.par',
            'left'
        );

        $this->db->join(
            'actions',
            'ON task.request = actions.action_id',
            'inner'
        );

        if (isset($categories) && is_array($categories) && count($categories) > 0) {
          
            $this->db->where_in('inventory.cat', $categories);
        }

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        $this->db->where('is_archived', $is_archived_shown);

        if ($is_done_shown) {
            $this->db->where('task.listing_item_id <>', 0);
        } else {
            $this->db->where('task.listing_item_id', 0);
        }

        $this->db->order_by($order_by, $sort_order);

        if (isset($actions) || isset($users) || (isset($photo) && $photo !== '')
            || (isset($banner) && $banner !== '')) {
            $this->db->group_start();

            if (isset($actions)) {
                $this->db->where_in('request', $actions);
            }

            if (isset($users)) {
                $this->db->group_start();

                foreach ($users as $user) {
                $this->db->or_like('task.lot_last_edited_by', $user)
                         ->or_like('request_last_edited_by', $user)
                         ->or_like('task.price_last_edited_by', $user)
                         ->or_like('task.notes_last_edited_by', $user)
                         ->or_like('task.banner_last_edited_by', $user)
                         ->or_like('task.shipping_last_edited_by', $user)
                         ->or_like('task.weight_last_edited_by', $user)
                         ->or_like('task.photo_last_edited_by', $user)
                         ->or_like('task.moved_last_edited_by', $user)
                         ->or_like('task.lot_last_edited_by', $user)
                         ->or_like('task.draft_by', $user)
                         ->or_like('task.request_by', $user)
                         ->or_like('task.listing_by', $user);
                }
                
                $this->db->group_end();
            }

            if (isset($banner) && $banner !== '') {
                $this->db->where('banner', $banner);
            }

            if (isset($photo) && $photo !== '') {
                $this->db->group_start();
                $this->db->where('photo', $photo);

                if ($photo == 0) {
                    $this->db->or_where('photo', null);
                }

                $this->db->group_end();
            }

            $this->db->group_end();
        }

        $query = $this->db->get('task');

        return $query->result();
    }

    function count(
        $search_query,
        $order_by = 'date_requested',
        $sort_order = 'ASC',
        $actions = null,
        $categories = null,
        $banner = null,
        $photo = null,
        $users = null,
        $search_column = 'sku',
        $is_done_shown = false,
        $is_archived_shown = false
    ) {
        $this->db->select('COUNT(*) AS listing_count');

        if (isset($search_query) && $search_query !== '') {
            $this->db->like($search_column, $search_query);
        }

        if (isset($categories) && is_array($categories) && count($categories) > 0) {
            $this->db->join(
                'inventory',
                'ON task.sku = inventory.par',
                'left'
            )->where_in('inventory.cat', $categories);
        }

        $this->db->where('is_archived', $is_archived_shown);

        if ($is_done_shown) {
            $this->db->where('task.listing_item_id !=', 0);
        } else {
            $this->db->where('task.listing_item_id', 0);
        }

        if (isset($actions) || isset($users) || (isset($photo) && $photo !== '')
            || (isset($banner) && $banner !== '')) {
            $this->db->group_start();

            if (isset($actions)) {
                $this->db->where_in('request', $actions);
            }

            if (isset($users)) {
                $this->db->group_start()->where_in('task.lot_last_edited_by', $users)
                    ->or_where_in('request_last_edited_by', $users)
                    ->or_where_in('task.price_last_edited_by', $users)
                    ->or_where_in('task.notes_last_edited_by', $users)
                    ->or_where_in('task.banner_last_edited_by', $users)
                    ->or_where_in('task.shipping_last_edited_by', $users)
                    ->or_where_in('task.weight_last_edited_by', $users)
                    ->or_where_in('task.photo_last_edited_by', $users)
                    ->or_where_in('task.moved_last_edited_by', $users)
                    ->or_where_in('task.lot_last_edited_by', $users)
                    ->or_where_in('task.draft_by', $users)
                    ->or_where_in('task.request_by', $users)
                    ->or_where_in('task.listing_by', $users)
                    ->group_end();
            }

            if (isset($banner) && $banner !== '') {
                $this->db->where('banner', $banner);
            }

            if (isset($photo) && $photo !== '') {
                $this->db->group_start();
                $this->db->where('photo', $photo);

                if ($photo == 0) {
                    $this->db->or_where('photo', null);
                }

                $this->db->group_end();
            }

            $this->db->group_end();
        }

        $query = $this->db->get('task', 1, 0);

        return $query->row();
    }
}
