<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_profile_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_users_profile(){
        $this->db->select(array(
            'ein',
            'start_date',
            'position',
            'department',
            'manager',
            'location',
            'extension',
            'day_start',
            'day_end',
            'id',
            'email',
            'firstname',
            'lastname',
            'contact_number',
            'birthday',
            'address',
            'picture',
            'schedule_in',
            'schedule_out',
            'lastname',
            'is_manager'
        ));
        $this->db->where('is_active', '1');
        
	 	$query = $this->db->get('users');
	 	return $query->result();
    }

    public function get_user($email =null){
        $this->db->select(array(
            'email',
            'start_date',
            'position',
            'department',
            'manager',
            'location',
            'extension',
            'day_start',
            'day_end',
            'id',
            'firstname',
            'lastname',
            'contact_number',
            'birthday',
            'address',
            'picture',
            'schedule_in',
            'schedule_out',
            'lastname',
            'is_manager'
        ));
        $this->db->where('users.email', $email);
        
	 	$query = $this->db->get('users');
	 	return $query->result();
    }

    public function get_csv(){
        $this->load->dbutil();
        $this->db->select(array(
            'id',
            'email',
            'firstname',
            'lastname',
            'contact_number',
            'birthday',
            'address',
            'schedule_in',
            'schedule_out',
            'start_date',
            'position',
            'department',
            'manager',
            'location',
            'extension',
            'day_start',
            'day_end',
           
        ));
        $this->db->where('is_active', '1');
        
	 	$query = $this->db->get('users');
         return $this->dbutil->csv_from_result($query);
    }

    public function inactive($email){
        $this->db->set('users.is_active',false);
        $this->db->where('users.email', $email);
        $this->db->update('users');
    }

    public function update_userscol($id,$email, array $data){
        $this->db->set('users.contact_number',$data['contact_number']);
        $this->db->set('users.birthday',$data['birthday']);
        $this->db->set('users.address',$data['address']);
        $this->db->set('users.ein',$data['ein']);
        $this->db->set('users.start_date',$data['start_date']);
        $this->db->set('users.position',$data['position']);
        $this->db->set('users.manager',$data['manager']);
        $this->db->set('users.department',$data['department']);
        $this->db->set('users.location',$data['location']);
        $this->db->set('users.extension',$data['extension']);
        $this->db->set('users.day_start',$data['day_start']);
        $this->db->set('users.day_end',$data['day_end']);
        $this->db->where('users.email', $email);
        $this->db->update('users');

        return $this->db->insert_id(); 
    }

    public function get_manager(){
    	$this->db->select();
    	$this->db->where('is_manager', '1');

    	$query = $this->db->get('users');
        return $query->result();
    }

    public function search_it ($searchItem = null){
        $this->db->select(array(
            'ein',
            'start_date',
            'position',
            'department',
            'manager',
            'location',
            'extension',
            'day_start',
            'day_end',
            'id',
            'email',
            'firstname',
            'lastname',
            'contact_number',
            'birthday',
            'address',
            'picture',
            'schedule_in',
            'schedule_out',
            'lastname',
            'is_manager'
        ));
    
        if(isset($searchItem) && $searchItem != ''){
       
        $this->db->where('email',$searchItem);
        $this->db->or_where('firstname',$searchItem);
        $this->db->or_where('lastname',$searchItem);
        $this->db->where('is_active', '1');
        }
       
        $query = $this->db->get('users');
        return $query->result();
}
}

?>

