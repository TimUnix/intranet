<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drives_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get(
    $searchItem = null,
    $searchSelected = null,
    $order_by,
    $sort_order,
    $items_offset = 0
    ){
    	$this->db->select(array(
    	'drive_id',
    	'sku',
    	'bar',
        'upc',
    	'brand',
    	'ff',
    	'size',
    	'type',
    	'rpm',
    	'speed',
    	'interface',
    	'series',
    	'ext_inf',
    	'con'
    	));

    	if(isset($searchItem) && $searchItem != ''){
    		$this->db->like($searchSelected, $searchItem);
    	}

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        } else {
            $this->db->order_by('drive_id');
        }

        $this->db->limit(100, $items_offset);

    	$query = $this->db->get('drives');
        return $query->result();
    }

    function load_sku($sku) {
        $query = $this->db->select(array(
            'drive_id',
            'sku',
            'bar',
            'upc',
            'brand',
            'ff',
            'size',
            'type',
            'rpm',
            'speed',
            'interface',
            'series',
            'ext_inf',
            'con'
        ))->where('sku', $sku)
                          ->get('drives');

        return $query->row();
    }

    function insert_listing($data)
    {
        $this->db->insert("drives", $data);
        return $this->db->insert_id();
    }

    function show_added_listing(){

        $this->db->select(array(
        'drive_id',
        'sku',
        'bar',
        'upc',
        'brand',
        'ff',
        'size',
        'type',
        'rpm',
        'speed',
        'interface',
        'series',
        'ext_inf',
        'con'
        ));
        
        $query = $this->db->get('drives');

        return $query->result();  
    }

    function count(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
       
        $this->db->select('COUNT(*) AS listing_count');
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
            $this->db->select('COUNT(*) AS listing_count');
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('drives', 1, 0);
        return $query->row();
    }

    public function update_drivescol(
        $data,  
        $sku,
        $bar,
        $key = null){
        $this->db->where('sku', $sku);
        $this->db->where('bar', $bar);
        $this->db->update("drives", $data);
        return $key;
    }

    public function export_csv(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null

    ) {
        $this->load->dbutil();

        $this->db->select(array(
        'sku',
        'bar',
        'upc',
        'brand',
        'ff',
        'size',
        'type',
        'rpm',
        'speed',
        'interface',
        'series',
        'ext_inf',
        'con'
        
        ));
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('drives');

        return $this->dbutil->csv_from_result($query);
    }

    public function get_capacity_template(){
        $this->db->select('name');
        $query = $this->db->get('capacity_template');
        return $query->result();
    }

    public function get_ff_template(){
        $this->db->select('name');
        $query = $this->db->get('form_factor_template');
        return $query->result();
    }
    public function get_interface_template(){
        $this->db->select('name');
        $query = $this->db->get('interface_template');
        return $query->result();
    }
    public function get_condition_template(){
        $this->db->select('name');
        $query = $this->db->get('condition_template');
        return $query->result();
    }
    public function get_warranty_template(){
        $this->db->select('name');
        $query = $this->db->get('warranty_template');
        return $query->result();
    }
    public function get_brand_template(){
        $this->db->select('name');
        $query = $this->db->get('brand_template');
        return $query->result();
    }
}

?>
