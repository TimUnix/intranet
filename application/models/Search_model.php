<?php
class Search_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
		parent:: __construct();
	}
    
	public function get_items(){
        $this->db->select(array(
            'inventory.par',
            'inventory.bar',
            'inventory.des',
            'inventory.con',
            'inventory.cat',
            'FLOOR(inventory.pri) AS pri',
            'inventory.qty',
            'inventory.bin',
            'FLOOR(inventory.qty * inventory.pri) AS tot',
            'images.img'
        ));

        $this->db->join(
            'images',
            'ON(inventory.par = images.par)',
            'left'
        );

        $this->db->where('arc', 'False');
        $this->db->where('qty >', 0);

        $this->db->order_by('tot', 'DESC');

        $query = $this->db->get('inventory');

		return $query->result();
	}
	public function total_items_by_items(){
        $this->db->select(array(
            'COUNT(man) AS countman',
            'man'
        ));

        $this->db->group_by('man');
        $this->db->order_by('countman', 'DESC');
        $this->db->limit(10, 0);

        $this->db->where('qty >', 0);
        $this->db->where('arc', 'False');
        
		$data = $this->db->get('inventory');

		return $data->result();
	}
	public function total_items_by_price(){
        $this->db->select(array(
            'COUNT(man) AS countman',
            'SUM(pri) AS pri',
            'man'
        ));

        $this->db->group_by('man');
        $this->db->order_by('pri', 'DESC');
        $this->db->limit(10, 0);
        $this->db->where('qty >', 0);
        $this->db->where('arc', 'False');
        
		$data = $this->db->get('inventory');
		
        return $data->result();
	}

    public function get_count_and_total() {
        $this->db->select(array(
            'COUNT(*) AS amount',
            'SUM(FLOOR(qty * pri)) AS price'
        ));

        $this->db->where('qty >', 0);
        $this->db->where('arc', 'False');
        $query = $this->db->get('inventory');

        return $query->result();
    }
	
	function get_specific_cat_and_con($cat, $con){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('env.cat'=>$cat, 'env.con'=>$con));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}


	function getFields($id){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('env.par'=>$id));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}
	
	
	
	function getField($id, $x, $i){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('l.holiday_type'=>$id, 'l.for_holiday_days_id'=>$x, 'l.daily_type_rate'=>$i));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}
	
	
	
	function getaccount($fname, $lname, $packavailed){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('l.holiday_type'=>$fname, 'l.for_holiday_days_id'=>$lname, 'l.daily_type_rate'=>$packavailed));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}
	
	function getValue($id,$select,$return=''){
		$this->db->select($select);
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('l.holiday_type'=>$id));
		$query = $this->db->get();
		if($query->num_rows()>0){
			$row=$query->row();
			if($return){
				return (!empty($row->{$return}))?$row->{$return}:false;
			}
			return (!empty($row->{$select}))?$row->{$select}:false;
		}
		return false;
	}
	
	function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE , $row = FALSE){
		self::_select();
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		self::group_by($group_by);
		self::orderby($order_by);
		$query = $this->db->get();
		
		if($result){
			return $query->result();
		}
		
		if($count){
			return $query->num_rows();
		}
		
		if($row){
			if($query->num_rows()>0)
				return $query->row();
			return false;	
		}
		
		return $query;
	}
	
	function getList($where,$where_string,$order_by){
		self::_select();
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		self::where_string($where_string);
		// parent::group_by("u.id_user");
		self::orderby($order_by);
		return $query = $this->db->get();
	}
	
	function getListLimit($where,$where_string,$order_by,$page,$number){
		self::_select();
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		self::where_string($where_string);
		// parent::group_by("u.id_user");
		self::orderby($order_by);
		self::pagelimit($page, $number);
		return $query = $this->db->get();
	}
	
	/*
	 * From
	 * @return void
	 */
	private function _from()
	{
		$this->db->from("inventory env");
	}
	
	/*
	 * SELECT
	 * @return void
	 */
	private function _select()
	{
		$this->db->select("
				env.*
				
		");
		// au.user_fname as add_user_fname, au.user_mname as add_user_mname, au.user_lname as add_user_lname, au.user_code as add_user_code,au.user_email as add_user_email, au.user_picture as add_user_picture,
		// uu.user_fname as update_user_fname, uu.user_mname as update_user_mname, uu.user_lname as update_user_lname, uu.user_code as update_user_code,uu.user_email as update_user_email, uu.user_picture as update_user_picture
	}
	
	/*
	 * JOIN
	 * @return void
	 */
	private function _join()
	{
		//$this->db->join('user_types ut', 'ut.id_user_type = u.user_type_id', 'left');
	}
	
	/*
	 * Fix Argument
	 * @return void
	 */
	private function _fix_arg()
	{
		//$this->db->where(array('l.enabled' => 1));
		return 1;
	}
	
	/*
	 * Insert Query
	 * @return id
	 */
	function insert_table($data){
		$this->db->insert("for_holiday_days",$data);
		return $this->db->insert_id();
	}
	
	/*
	 * Batch Insert Query
	 * @return void
	 */
	function insert_batch_table($data){
		$this->db->insert_batch("for_holiday_days", $data); 
	}
	
	/*
	 * Update Query
	 * @return id
	 */
	function update_table($data,$table_col,$key){
		$this->db->where($table_col,$key);
		$this->db->update("for_holiday_days",$data);
		return $key;
	}
	
	/*
	 * Update Where Query
	 * @return void
	 */
	function update_table2($data,$where){
		self::where($where);
		$this->db->update("for_holiday_days",$data);
	}
	
	/*
	 * Batch Update Query
	 * @return void
	 */
	function update_batch_table($data,$table_col){
		$this->db->update_batch("for_holiday_days", $data, $table_col); 
	}
	
	/*
	 * Delete Query
	 * @return void
	 */
	function delete_table($table_col,$key){
		$this->db->delete("for_holiday_days",array($table_col=>$key));
	}
	
	/*
	 * Custom
	 */
	function getFields_arr($where,$select,$return='',$return_value=''){
		$this->db->select($select);
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		$query = $this->db->get();
		$layer='';
		foreach($query->result() as $q){
			if($return){
				if($return_value){
					$layer[$q->{$return}]=$q->{$return_value};
				}else{
					$layer[$q->{$return}]=$q->{$return};
				}
			}else{
				$layer[$q->{$select}]=$q->{$select};
			}	
		}
		
		return $layer;
	}


	
	/*
	 * WHERE
	 * @return void
	 */
	public function where($where)
	{
		if(!empty($where))
		    $this->db->where($where);
	}
	
	/*
	 * WHERE STRING
	 * @return void
	 */
	public function where_string($where)
	{
		if(!empty($where))
		    foreach($where as $val){
			    if($val)
			        $this->db->where($val);
		    }
	}
	
	/*
	 * GROUP BY
	 * @return void
	 */
	public function group_by($group_by)
	{
		if(!empty($group_by))
		    $this->db->group_by($group_by); 
	}
	
	/*
	 * ORDER BY
	 * @return void
	 */
	public function orderby($order_by)
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	/*
	 * LIMIT - OFFSET
	 * @return void
	 */
	public function limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	/*
	 * PAGE LIMIT - OFFSET
	 * @return void
	 */
	public function pagelimit($page, $number)
	{
		$this->db->limit($number,($page-1)*$number);
	}
	public function categories(){
		$data = $this->db->query("select DISTINCT cat from inventory");
        return $data->result();
	}
	public function conditions(){
        $this->db->distinct();
        $this->db->select("IF(con = '', 'Blank', con) AS con");
        $data = $this->db->get('inventory');
		/* $data = $this->db->query("select DISTINCT con from inventory"); */
        return $data->result();
	}
	public function cat($data){
        $this->db->where('cat', $data);
        $this->db->where('qty >', 0);

        $data = $this->db->get('inventory');

        return $data->result();
	}
	public function con($data){
        $this->db->where('con', $data);
        $this->db->where('qty >', 0);

        $data = $this->db->get('inventory');

        return $data->result();
	}
	public function price_add(){
		$data = $this->db->query("SELECT SUM(pri) as pri FROM inventory;");
        return $data->result();
	}

    public function get_category_count() {
        $this->db->select('cat, COUNT(cat) AS cat_count');
        $this->db->where('qty >', 0);
        $this->db->where('arc', 'False');
        $this->db->group_by('cat');
        $this->db->order_by('cat_count', 'DESC');
        $this->db->limit(10, 0);

        $data = $this->db->get('inventory');

        return $data->result();
    }

    public function get_condition_count() {
        $this->db->select('con, COUNT(con) AS con_count');
        $this->db->where('qty >', 0);
        $this->db->where('arc', 'False');
        $this->db->group_by('con');
        $this->db->order_by('con_count', 'DESC');
        $this->db->limit(10, 0);

        $data = $this->db->get('inventory');

        return $data->result();
    }

    public function get_qty_zero_count() {
        $this->db->select('COUNT(*) AS count');
        $this->db->where('qty', 0);
        $this->db->where('arc', 'False');
        
        $data = $this->db->get('inventory');

        return $data->row();
    }
    public function get_year_count() {
        $this->db->select('y, COUNT(y) AS y_count');
        $this->db->where('qty >', 0);
        //$this->db->where('arc', 'False');
        $this->db->group_by('y');
        $this->db->order_by('y_count', 'DESC');
        $this->db->limit(4, 0);

        $data = $this->db->get('inventory_report');

        return $data->result();
    }
     public function get_pieInv_count(){
    	$this->db->select('inv, COUNT(inv) AS inv_count');
    	$this->db->where('qty >', 0);
    	$this->db->group_by('inv');
    	//$this->db->order_by('inv_count', 'DESC');

    	$data = $this->db->get('inventory_report');
    	return $data->result();

    }
    public function inv_count(){
    	$this->db->select('y, COUNT(inv) AS inv_count');
    	$this->db->like('inv', 'inv');
    	$this->db->like('y');
    	$this->db->group_by('y', 'inv');
    	//$this->db->order_by('y');
    	
    	
    	$data = $this->db->get('inventory_report');
    	return $data->result();
     }
     public function uni_count(){
    	$this->db->select('y, COUNT(inv) AS uni_count');
    	$this->db->like('inv', 'uni');
    	$this->db->like('y');
    	$this->db->group_by('y', 'inv');
    	//$this->db->order_by('y');
    	
    	
    	$data = $this->db->get('inventory_report');
    	return $data->result();
    }
    public function itr_count(){
    	$this->db->select('y, COUNT(inv) AS itr_count');
    	$this->db->like('inv', 'itr');
    	$this->db->like('y');
    	$this->db->group_by('y', 'inv');
    	//$this->db->order_by('y');
    	
    	
    	$data = $this->db->get('inventory_report');
    	return $data->result();
    }

}
?>
