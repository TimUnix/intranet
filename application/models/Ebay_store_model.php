<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_store_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get(
        $searchItem = null,
        $seller = null,
        $conditions = null,
        $categories = null,
        $order_by = null,
        $sort_order = null,
        $selling_state = null,
        $current_offset = 0
    ) {
        $items_limit = 100;
        $this->db->select(array(
            'price',
            'link',
            'img',
            'title',
            'category_name',
            'seller',
            'condition_display_name',
            'item_number',
            'shipping_type',
            'ship_to_locations',
            'expedited_shipping',
            'one_day_shipping_available',
            'top_rated_listing',
            'handling_time',
            'selling_state',
            'last_update',
            'history_purchase_link',
            'total_sold'
        ));

        if (isset($searchItem)) {
            $this->db->like('title', $searchItem);
        }

        if (isset($seller)) {
            $this->db->where('seller', $seller);
        }

        if (isset($conditions) && count($conditions) > 0) {
            $this->db->where_in('condition_display_name', $conditions);
        }

        if (isset($categories) && count($categories) > 0) {
            $this->db->where_in('category_name', $categories);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        if (isset($selling_state) && $selling_state !== '') {
            $this->db->where('selling_state', $selling_state);
        }

        $this->db->limit($items_limit, $current_offset);

        $query = $this->db->get('ebay_store');

        return $query->result();
    }

    function summary(
        $searchItem = null,
        $seller = null,
        $conditions = null,
        $categories = null,
        $order_by = null,
        $sort_order = null,
        $selling_state = null,
        $current_offset = 0
    ) {
        $items_limit = 100;
        $this->db->select(array(
            'COUNT(*) AS count',
            'AVG(price) AS average',
            'MIN(price) AS low',
            'MAX(price) AS high'
        ));

        if (isset($searchItem)) {
            $this->db->like('title', $searchItem);
        }

        if (isset($seller)) {
            $this->db->where('seller', $seller);
        }

        if (isset($conditions) && count($conditions) > 0) {
            $this->db->where_in('condition_display_name', $conditions);
        }

        if (isset($categories) && count($categories) > 0) {
            $this->db->where_in('category_name', $categories);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        if (isset($selling_state) && $selling_state != '') {
            $this->db->where('selling_state', $selling_state);
        }

        $query = $this->db->get('ebay_store');
        $result = $query->row();

        $median_query = 'SELECT '
                      . 'AVG(prices_sorted.price) AS median '
                      . 'FROM '
                      . '('
                      . 'SELECT '
                      . 'ebay_store.price,'
                      . '@rownum := @rownum + 1 AS `row_number`, '
                      . '@total_rows := @rownum '
                      . 'FROM '
                      . 'ebay_store,'
                      . '('
                      . 'SELECT '
                      . '@rownum := 0) r '
                      . 'WHERE '
                      . 'ebay_store.price IS NOT NULL';


        if (isset($searchItem)) {
            $median_query .= " AND title LIKE '%$searchItem%'";
        }

        if (isset($seller)) {
            $median_query .= " AND seller = '$seller'";
        }

        if (isset($conditions) && count($conditions) > 0) {
            $median_query .= " AND condition_display_name IN ($conditions)";
        }

        if (isset($categories) && count($categories) > 0) {
            $median_query .= " AND category_name IN ($categories)";
        }

        if (isset($selling_state) && $selling_state !== '') {
            $median_query .= " AND selling_state = '$selling_state'";
        }

        $median_query .= ' ORDER BY '
                      . 'ebay_store.price'
                      . ') AS prices_sorted '
                      . 'WHERE '
                      . 'prices_sorted.row_number IN ( FLOOR((@total_rows + 1)/ 2), FLOOR((@total_rows + 2)/ 2) )';

        $median_result = $this->db->query($median_query, false)->row();
        $result->median = $median_result->median;

        return $result;
    }

    public function conditions() {
        $this->db->select(array(
            'condition_id AS id',
            'condition_display_name AS name'
        ))->distinct()
                 ->order_by('name');

        $query = $this->db->get('ebay_store');

        return $query->result();
    }

    public function categories($stores = null) {
        $this->db->select(array(
            'category_id AS id',
            'category_name AS name'
        ))->distinct()
                 ->order_by('name');

        if (isset($stores) && is_array($stores) && count($stores) > 0) {
            $this->db->where_in('seller', $stores);
        }

        $query = $this->db->get('ebay_store');

        return $query->result();
    }

    public function sellers() {
        $this->db->select('store');

        $query = $this->db->get('ebay_store_name');

        return $query->result();
    }

    public function search_item_number($par, $store) {
        $this->db->select('item_number');
        $this->db->where('seller', $store);
        $this->db->like('title', $par);
        $this->db->limit(1, 0);

        $query = $this->db->get('ebay_store');

        return $query->row();
    }


    public function get_ebay_item_json($get_ebay_item_json){
        $data = $this->db->query("select rwidgets from ebay_store where item_number = '$get_ebay_item_json'");
        return $data->result();
    }
}

?>
