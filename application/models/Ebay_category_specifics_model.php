<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_category_specifics_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function keys($category_id, $is_enabled_filter_on = true) {
        $this->db->select(array('id', 'name', 'cardinality', 'format', 'max_length', 'requirement'))
                 ->where('category_id', $category_id)
                 ->order_by('name', 'ASC');

        if ($is_enabled_filter_on) {
            $this->db->where('is_enabled', 1);
        }

        $query = $this->db->get('ebay_category_specifics_keys');

        return $query->result();
    }

    function values($key_id) {
        $this->db->select(array('id', 'value'))
                 ->where('ebay_category_specifics_keys_id', $key_id)
                 ->order_by('value', 'ASC');

        $query = $this->db->get('ebay_category_specifics_values');

        return $query->result();
    }

    function conditions($category_id) {
        $this->db->select(array(
            'required',
            'ebay_enum',
            'display_name'
        ))->where('category_id', $category_id)
                 ->order_by('condition_id');

        $query = $this->db->get('ebay_conditions');

        return $query->result();
    }
}

?>
