<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assembly_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
    	$this->db->select();

    	if(isset($searchItem) && $searchItem != ''){
    		$this->db->like($searchSelected, $searchItem);
    	}

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

    	$query = $this->db->get('assembly');
        return $query->result();
    }

    public function get_obnic_pcieslot($motherboard = null){
        $this->db->select();
        $this->db->where('model', $motherboard);

        $query = $this->db->get('motherboard');
        return $query->result();

    }

    function insert_listing($data)
    {
        $this->db->insert('assembly', $data);
        return $this->db->insert_id();
    }
    public function get_build($build_id){
        $query = $this->db->query("select * from assembly where build_id = '$build_id'");

        return $query->result();
    }

    function count(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
       
        $this->db->select('COUNT(*) AS listing_count');
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
            $this->db->select('COUNT(*) AS listing_count');
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('assembly', 1, 0);
        return $query->row();
    }

    public function update_row($build_id, $data){
        $this->db->where('build_id', $build_id);
        $this->db->update('assembly', $data);
        return $key;
    }

    public function delete_row($build_id, $key=null){
        $this->db->where('build_id', $build_id);
        $this->db->delete('assembly');
        return $key;
    }

    public function export_csv(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null

    ) {
        $this->load->dbutil();

        $this->db->select(array(
        'build_id',
        'chassis',
        'sales_rep',
        'created_date'
        
        ));
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('assembly');

        return $this->dbutil->csv_from_result($query);
    }

     public function mounting(){

        $this->db->select('name');
        $query = $this->db->get('mounting');

        return $query->result();
    }

    public function warranty(){
        $this->db->select('name');
        $query = $this->db->get('warranty');

        return $query->result();
    }
    public function chassis(){
        $this->db->select('part_number');
 
        $query = $this->db->get('chassis_new');

        return $query->result();
    }

     public function drives(){
        $this->db->select();
        $query = $this->db->get('drives');

        return $query->result();
    }

    public function motherboard(){
        $this->db->select();
        $query = $this->db->get('motherboard');

        return $query->result();
    }

    public function cpu(){
        $this->db->select('part');
        $query = $this->db->get('cpu_new');

        return $query->result();
    }

    public function memory(){
        $this->db->select('sku');
        $query = $this->db->get('memory');

        return $query->result();
    }

    public function chassis_detail($chassis){

        $query = $this->db->query("select * from chassis_new where part_number = '$chassis'");

        return $query->result();
    }

    public function motherboard_for_chassis($motherboard){

        $query = $this->db->query("select * from motherboard where model = '$motherboard'");

        return $query->result();
    }

    public function cpu_for_motherboard($cpu){

        $query = $this->db->query("select * from cpu_new where part = '$cpu'");

        return $query->result();
    }

    public function memory_for_motherboard($memory){
        $query = $this->db->query("select * from memory where memory_family = '$memory'");

        return $query->result();
    }

    public function get_motherboard($qty_cpu){
        $query = $this->db->query("select * from motherboard where qty_cpu = '$qty_cpu'");

        return $query->result();
       }

    public function get_buildID(){
        $this->db->select('build_id');
        //$this->db->where('build_id', $combination);
        $this->db->limit(1);
        $this->db->order_by('build_id', 'desc');
        $query = $this->db->get('assembly');
        return $query->result();
    }

}

?>