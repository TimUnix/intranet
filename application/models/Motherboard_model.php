<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motherboard_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function load_sku($sku) {
        $query = $this->db->select(array(
            'id',
            'sku',
            'barcode',
            'brand',
            'model',
            'rev_version',
            'qty_cpu',
            'memory',
            'memory_family',
            'shield',
            'heatsink'
        ))->where('sku', $sku)
                          ->get('motherboard');

        return $query->row();
    }

    public function get(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
    	$this->db->select(array(
    	'id',
    	'sku',
    	'barcode',
    	'brand',
    	'brand',
    	'model',
    	'rev_version',
    	'qty_cpu',
    	'memory',
    	'shield',
    	'heatsink'
    	));

    	if(isset($searchItem) && $searchItem != ''){
    		$this->db->like($searchSelected, $searchItem);
    	}

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

    	$query = $this->db->get('motherboard');
        return $query->result();
    }

    function insert_listing($data)
    {
        $this->db->insert("motherboard", $data);
        return $this->db->insert_id();
    }

    function show_added_listing(){

        $this->db->select(array(
        'id',
        'sku',
        'barcode',
        'brand',
        'brand',
        'model',
        'rev_version',
        'qty_cpu',
        'memory',
        'shield',
        'heatsink'
        ));
        
        $query = $this->db->get('motherboard');

        return $query->result();  
    }

    function count(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null
    ){
       
        $this->db->select('COUNT(*) AS listing_count');
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
            $this->db->select('COUNT(*) AS listing_count');
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('motherboard', 1, 0);
        return $query->row();
    }

    public function update_motherboardcol(
        $data,  
        $sku,
        $bar,
        $key = null){
        $this->db->where('sku', $sku);
        $this->db->where('barcode', $bar);
        $this->db->update("motherboard", $data);
        return $key;
    }

    public function export_csv(
    $searchItem = null,
    $searchSelected = null,
    $order_by = null,
    $sort_order = null

    ) {
        $this->load->dbutil();

        $this->db->select(array(
        'id',
        'sku',
        'barcode',
        'brand',
        'brand',
        'model',
        'rev_version',
        'qty_cpu',
        'memory',
        'shield',
        'heatsink'
        
        ));
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like($searchSelected, $searchItem);
        }

        if (isset($order_by) && isset($sort_order)) {
            $this->db->order_by($order_by, $sort_order);
        }

        $query = $this->db->get('motherboard');

        return $this->dbutil->csv_from_result($query);
    }

}

?>
