<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class switch_report_model extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_report($last_id = null, $items_offset){
        $done = null;
        $this->db->select();
        $this->db->limit(100, $items_offset);
        
        $query = $this->db->get('switch_report_module');
        return $query->result();
    }

    public function search_it ($searchItem = null, $items_offset){
        $this->db->select();
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like('eqm_des', $searchItem);
            $this->db->or_like(array('serial' => $searchItem, 'notes' => $searchItem));
           
        }

        $this->db->limit(100, $items_offset);

        $query = $this->db->get('switch_report_module');
        return $query->result();
    }
    
    
    function insert_listing($data)
        {
            $this->db->insert("switch_report_module", $data);
            
        }

    public function update_col(
        $data,  
        $serial,
        $id,
        $key = null){
        $this->db->where('serial', $serial);
        $this->db->where('id', $id);
        $this->db->update("switch_report_module", $data);
        return $key; 
        }

    function show_added_listing(){
        $this->db->select(array(
            'modelnum' ,
            'qty' ,
            'lwsku',
            'numports',
            'pspeed',
            'numupports',
            'uplinkspeed',
            'poewatts',
            'psuqty',
            'oucard',
            'noedr', 
            'imgsoft',
            'pricenote',
            'taskc',
            'title'
        ));
        
        $query = $this->db->get('switch_report_module');

        return $query->result();  
    }


}
?>
