<?php
class Ebay_shipping_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
		parent:: __construct();
	}

	public function get_tomorrow_shipping(){

		$data = $this->db->query("SELECT item_number, shipped_date, created_date, COUNT(shipped_date) AS count_tomorrow_shipping FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='0'" );
    	return $data->result();
	}

	public function get_shipping_data(){
		$data = $this->db->query("SELECT item_number, sku, title, shipped_date, counter FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='1' or counter='0'");

    	return $data->result();
	}


	public function display_quantity_shiping(){
		$data = $this->db->query("SELECT item_number, sku, title, qty_sold, shipped_date, counter FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='1' or counter='0'");

    	return $data->result();
	}

	public function display_shipped_tomorrow(){

		$data = $this->db->query("SELECT item_number, sku, title, shipped_date, counter FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='0'");

    	return $data->result();

	}

	public function get_today_shipping(){

		$data = $this->db->query("SELECT item_number, shipped_date, created_date, COUNT(shipped_date) AS count_today_shipping FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='1'" );

    	return $data->result();
	}

	public function display_shipped_today(){

		$data = $this->db->query("SELECT item_number, sku, title, shipped_date, counter FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='1'");

    	return $data->result();

	}

	public function get_today_quantity_sold(){

		$data = $this->db->query("SELECT seller, shipped_date, qty_sold, SUM(qty_sold) as sold_today FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='1'");

    	return $data->result();
	}

	public function display_quantity_today(){

		$data = $this->db->query("SELECT item_number, sku, title, qty_sold, shipped_date, counter FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='1'");

    	return $data->result();
	}

	public function get_tomorrow_quantity_sold(){

		$data = $this->db->query("SELECT seller, shipped_date, qty_sold, SUM(qty_sold) as sold_tomorrow FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='0'");

    	return $data->result();
	}

	public function display_quantity_tomorrow(){

		$data = $this->db->query("SELECT item_number, sku, title, qty_sold, shipped_date, counter FROM ebay_items_shipping_details WHERE DATE(created_date) = DATE(now()) AND order_status = 'Completed' AND counter='0'");
		

		//$data = $this->db->get('ebay_items_shipping_details');
    	return $data->result();
	}
}
?>