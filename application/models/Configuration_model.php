<?php

class Configuration_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
	}

	public function getConfiguration($code) {
        $this->db->select("value");
        $this->db->from('configuration');
		$this->db->where('code', $code)->limit(1);
        $query = $this->db->get();

        return $query->result();
    }
    public function getEbayapistringone() {
        return "https://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsAdvanced&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2&GLOBAL-ID=EBAY-US&descriptionSearch=True&keywords=";
    }
    public function getEbayapistringtwo() {
        return "&outputSelector(0)=SellerInfo";
    }
}
