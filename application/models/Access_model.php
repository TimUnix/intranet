<?php

class Access_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_menu_headers() {
        $this->db->select('menu_title, menu_order');
        $this->db->from('menu');
        $this->db->order_by('menu_order');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_users_access() {
        $query = $this->db->select(array(
            'users.id AS user_id',
            'users.firstname AS user_firstname',
            'users.lastname AS user_lastname',
            'menu.menu_id AS menu_id',
            'menu.menu_title',
            'menu.menu_order',
            'user_menu.menu_access'
        ))->join(
                'user_menu',
                'users.id = user_menu.user_id '
                . 'AND menu.menu_id = user_menu.menu_id',
                'left'
            )->order_by('users.id, menu.menu_order')
            ->get('users, menu');

        return $query->result();
    }

    public function get_user_count() {
        $query = $this->db->select('COUNT(*) count')
            ->get('users');

        return $query->row();
    }

	public function update_menu_access ($access, $menu_id, $user_id){
	    $this->db->set('menu_access', $access);
        $this->db->where('menu_id', $menu_id);
		$this->db->where('user_id', $user_id);
        
	    $this->db->update('user_menu');
    }

	public function insert_modules ($insert_menu){
	    $this->db->trans_start();
		foreach ($insert_menu as $menu){
			$this->db->insert('user_menu', $menu);
		}
		$this->db->trans_complete();
    }

	public function get_users(){
        $this->db->select(array(
			'id',
            'firstname',
            'lastname'
        ));
		
        $this->db->where('is_active', '1');
        
	 	$query = $this->db->get('users');
	 	return $query->result();
    }

	public function get_modules(){
        $this->db->select(array(
			'menu.menu_id',
            'menu.menu_title',
			'user_menu.menu_access'
		));
		$this->db->join(
			'user_menu',
			'ON(user_menu.user_id = menu.menu_id)',
			'left'
		);

		$this->db->where('menu.is_active', '1');
        
	 	$query = $this->db->get('menu');
	 	return $query->result();
    }

	public function search_users($searchItem){
        $this->db->select(array(
			'id',
            'firstname',
            'lastname'
        ));
    
        if(isset($searchItem) && $searchItem != ''){
		$this->db->where('firstname',$searchItem);
		$this->db->or_where('lastname',$searchItem);
        }
       
        $query = $this->db->get('users');
        return $query->result();
	}

	public function search_modules($searchItem){
        $this->db->select(array(
			'menu.menu_id',
            'menu.menu_title',
			'user_menu.menu_access'
        ));
		$this->db->join(
			'user_menu',
			'ON(user_menu.user_id = menu.menu_id)',
			'left'
		);

		$this->db->where('menu.is_active', '1');
    
        if(isset($searchItem) && $searchItem != ''){
		$this->db->where('menu_title',$searchItem);
       
        }
       
        $query = $this->db->get('menu');
        return $query->result();
	}

	public function show_modules($user_id){
		$this->db->select(array(
            'users.id AS user_id',
            'menu.menu_id AS menu_id',
			'menu.menu_title AS menu_title',
			'user_menu.menu_access AS menu_access'
        ));
		$this->db->join(
			'users',
			'ON(user_menu.user_id = users.id)',
			'left'
		);
		$this->db->join(
			'menu',
			'ON (menu.menu_id = user_menu.menu_id)',
			'left'
		);
	
		$this->db->where('user_menu.user_id', $user_id);
		$this->db->where('menu.is_active', '1');

		$query = $this->db->get('user_menu');
        return $query->result();
	}

	public function superadmin($access, $user_id){
		$this->db->set('menu_access', $access);
		$this->db->where('user_id', $user_id);
        
	    $this->db->update('user_menu');	
	}

	public function count_menu(){
		$this->db->select(array(
           'COUNT(*) AS total'
		));

		$this->db->where('is_active', '1');

		$query = $this->db->get('menu');	
		return $query->result();
	}

	public function count_users($user_id){
		$this->db->select(array(
			'COUNT(*) AS total',
			'users.id AS user_id',
			'user_menu.menu_access AS menu_access'

		));
		$this->db->join(
			'users',
			'ON(user_menu.user_id = users.id)',
			'left'
		);
		$this->db->join(
			'menu',
			'ON (menu.menu_id = user_menu.menu_id)',
			'left'
		);

		$this->db->where('user_id',$user_id);
		$this->db->where('menu_access', 1);
		$this->db->where('menu.is_active', '1');

		$query = $this->db->get('user_menu');	
		return $query->result();
	}

	public function get_menus(){
		$this->db->select(
			'menu_id'
		);

		$this->db->where('is_active', '1');

		$query = $this->db->get('menu');	
		return $query->result();
	}
}

?>
