<?php

class Menu_items_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_menu_info() {
        $this->db->select(array(
            'menu_order', 'menu_id', 'menu_title', 'menu_parent', 'menu_file'
        ));
        $this->db->from('menu');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_menu_information() {
        $query = $this->db->query('select aa.menu_order as menu_order, aa.menu_id as menu_id, aa.menu_title as menu_title, aa.menu_file as menu_file, 
        bb.menu_title as parent, aa.icons as icons from menu aa left join menu bb on aa.menu_parent = bb.menu_id order by menu_order');

        return $query->result();
    }

    public function update_menu_item($order, $menu_id, $title, $file,
                                     $parent) {
        $this->db->where('menu_id', $menu_id);
        $this->db->update('menu', array(
            'menu_order' => $order, 'menu_id' => $menu_id,
            'menu_title' => $title, 'menu_file' => $file,
			'menu_parent' => $parent,
			'icons' => $icons,
        ));
    }
    function update_menu_infos($data,$table_col,$key){
        $this->db->where($table_col,$key);
        $this->db->update("menu",$data);
        return $key;
    }
    public function get_menu_id($data) {
        $query = $this->db->query("select * from menu where menu_id = '$data'");

        return $query->result();
    }
    public function get_menu_parent($data){
        $query = $this->db->query("select * from menu where menu_parent = '$data'");

        return $query->result();
    }
    public function get_menu($parent, $id) {
        $query = $this->db->query("select * from menu where menu_id = '$id' and menu_parent <> '$parent'");

        if($query->result()){
            return 1;
        }else{return 0;}
    }
    public function get_menus($parent, $id) {
        $query = $this->db->query("select * from menu where menu_id = '$id' and menu_parent = '$parent'");

        if($query->result()){
            return 1;
        }else{return 0;}
    }
}

?>
