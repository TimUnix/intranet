<?php
class Research_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
		parent:: __construct();
	}
    
	public function get_items(){
		$query = $this->db->get('inventory');
		return $query->result();
	}

    public function get_count_and_total() {
        $this->db->select('COUNT(*) amount, SUM(FLOOR(qty * pri)) price');
        $query = $this->db->get('inventory');
        return $query->result();
    }
	
	function get_user_id($user_id){
		self::_select();	
		self::_from();
		self::_join();
		self::where(array('sh.user_id'=>$user_id));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}


	function getFields($id){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('env.par'=>$id));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}
	
	
	
	function getField($id, $x, $i){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('l.holiday_type'=>$id, 'l.for_holiday_days_id'=>$x, 'l.daily_type_rate'=>$i));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}
	
	
	
	function getaccount($fname, $lname, $packavailed){
		self::_select();	
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('l.holiday_type'=>$fname, 'l.for_holiday_days_id'=>$lname, 'l.daily_type_rate'=>$packavailed));
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return $query->row();
		}
		
		return false;
	}
	
	function getValue($id,$select,$return=''){
		$this->db->select($select);
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where(array('l.holiday_type'=>$id));
		$query = $this->db->get();
		if($query->num_rows()>0){
			$row=$query->row();
			if($return){
				return (!empty($row->{$return}))?$row->{$return}:false;
			}
			return (!empty($row->{$select}))?$row->{$select}:false;
		}
		return false;
	}
	
	function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE , $row = FALSE){
		self::_select();
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		self::group_by($group_by);
		self::orderby($order_by);
		$query = $this->db->get();
		
		if($result){
			return $query->result();
		}
		
		if($count){
			return $query->num_rows();
		}
		
		if($row){
			if($query->num_rows()>0)
				return $query->row();
			return false;	
		}
		
		return $query;
	}
	
	function getList($where,$where_string,$order_by){
		self::_select();
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		self::where_string($where_string);
		// parent::group_by("u.id_user");
		self::orderby($order_by);
		return $query = $this->db->get();
	}
	
	function getListLimit($where,$where_string,$order_by,$page,$number){
		self::_select();
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		self::where_string($where_string);
		// parent::group_by("u.id_user");
		self::orderby($order_by);
		self::pagelimit($page, $number);
		return $query = $this->db->get();
	}
	
	/*
	 * From
	 * @return void
	 */
	private function _from()
	{
		$this->db->from("search_history sh");
	}
	
	/*
	 * SELECT
	 * @return void
	 */
	private function _select()
	{
		$this->db->select("
				sh.*
				
		");
		// au.user_fname as add_user_fname, au.user_mname as add_user_mname, au.user_lname as add_user_lname, au.user_code as add_user_code,au.user_email as add_user_email, au.user_picture as add_user_picture,
		// uu.user_fname as update_user_fname, uu.user_mname as update_user_mname, uu.user_lname as update_user_lname, uu.user_code as update_user_code,uu.user_email as update_user_email, uu.user_picture as update_user_picture
	}
	
	/*
	 * JOIN
	 * @return void
	 */
	private function _join()
	{
		//$this->db->join('user_types ut', 'ut.id_user_type = u.user_type_id', 'left');
	}
	
	/*
	 * Fix Argument
	 * @return void
	 */
	private function _fix_arg()
	{
		//$this->db->where(array('l.enabled' => 1));
		return 1;
	}
	
	/*
	 * Insert Query
	 * @return id
	 */
	function insert_table($data){
		$this->db->insert("ebay_store",$data);
		return $this->db->insert_id();
	}
	
	/*
	 * Batch Insert Query
	 * @return void
	 */
	function insert_batch_table($data){
		$this->db->insert_batch("for_holiday_days", $data); 
	}
	
	/*
	 * Update Query
	 * @return id
	 */
	function update_table($data,$table_col,$key){
		$this->db->where($table_col,$key);
		$this->db->update("for_holiday_days",$data);
		return $key;
	}
	
	/*
	 * Update Where Query
	 * @return void
	 */
	function update_table2($data,$where){
		self::where($where);
		$this->db->update("for_holiday_days",$data);
	}
	
	/*
	 * Batch Update Query
	 * @return void
	 */
	function update_batch_table($data,$table_col){
		$this->db->update_batch("for_holiday_days", $data, $table_col); 
	}
	
	/*
	 * Delete Query
	 * @return void
	 */
	function delete_table($table_col,$key){
		$this->db->delete("for_holiday_days",array($table_col=>$key));
	}
	
	/*
	 * Custom
	 */
	function getFields_arr($where,$select,$return='',$return_value=''){
		$this->db->select($select);
		self::_from();
		self::_join();
		self::_fix_arg();
		self::where($where);
		$query = $this->db->get();
		$layer='';
		foreach($query->result() as $q){
			if($return){
				if($return_value){
					$layer[$q->{$return}]=$q->{$return_value};
				}else{
					$layer[$q->{$return}]=$q->{$return};
				}
			}else{
				$layer[$q->{$select}]=$q->{$select};
			}	
		}
		
		return $layer;
	}


	
	/*
	 * WHERE
	 * @return void
	 */
	public function where($where)
	{
		if(!empty($where))
		    $this->db->where($where);
	}
	
	/*
	 * WHERE STRING
	 * @return void
	 */
	public function where_string($where)
	{
		if(!empty($where))
		    foreach($where as $val){
			    if($val)
			        $this->db->where($val);
		    }
	}
	
	/*
	 * GROUP BY
	 * @return void
	 */
	public function group_by($group_by)
	{
		if(!empty($group_by))
		    $this->db->group_by($group_by); 
	}
	
	/*
	 * ORDER BY
	 * @return void
	 */
	public function orderby($order_by)
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	/*
	 * LIMIT - OFFSET
	 * @return void
	 */
	public function limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	/*
	 * PAGE LIMIT - OFFSET
	 * @return void
	 */
	public function pagelimit($page, $number)
	{
		$this->db->limit($number,($page-1)*$number);
	}
	public function categories(){
		$data = $this->db->query("select DISTINCT cat from inventory");
        return $data->result();
	}
	public function conditions(){
		$data = $this->db->query("select DISTINCT con from inventory");
        return $data->result();
	}
	public function cat($data){
		$data = $this->db->query("select * from inventory where cat = '$data'");
        return $data->result();
	}
	public function con($data){
		$data = $this->db->query("select * from inventory where con = '$data'");
        return $data->result();
	}
	public function price_add(){
		$data = $this->db->query("SELECT SUM(pri) as pri FROM inventory;");
        return $data->result();
	}
    public function load_items(){
        $data = $this->db->query("SELECT cat, sum(ite) as ite, sum(qty) as qty, sum(val) as val FROM `inventory_report` GROUP BY cat");
        return $data->result();
    }
	public function search_db($data){
		$_data = $this->db->query("SELECT * FROM `ebay_store` WHERE item_number like '%$data%'");
		return $_data->result();
	}
    public function search_ebay_item($data, $order_by, $sort_order){
        $this->db->select(array(
            'price',
            'link',
            'img',
            'seller',
            'title',
        ));

        $this->db->like('title', $data);
        $this->db->limit(20, 0);

        $this->db->order_by($order_by, $sort_order);
        
        $query = $this->db->get('ebay_store');
        
		return $query->result();
	}
	public function check_item($itemid, $title){
		$_data = $this->db->query("SELECT * FROM `ebay_store` WHERE item_number = '$itemid' and title = '$title'");
		return $_data->result();
	}
	public function get_check_item_available($item_number, $title){
		$_data = $this->db->query("SELECT * FROM `ebay_store` WHERE item_number = '$item_number'");
		return $_data->result();
	}
	public function search_item_ebay($data){
		$_data = $this->db->query("SELECT * FROM `ebay_store` WHERE 'title' LIKE '$data'");
		return $_data->result();
	}
	public function get_item_from_ebay_store($data, $order_by, $sort_order){
		$_data = $this->db->query("SELECT * FROM `ebay_store` WHERE item_number = '$data' order by $order_by $sort_order limit 20");
		return $_data->result();
	}
    public function get_store_names() {
        $this->db->select('store');

        $query = $this->db->get('ebay_store_name');

        return $query->result();
    }
    
}
?>
