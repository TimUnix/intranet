<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_items_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function archive_item($item_number) {
        $query = $this->db->select()
                          ->where('item_number', $item_number)
                          ->get('ebay_items');

        $item = $query->row();

        $this->db->insert('ebay_items_archive', $item);
        $this->db->delete('ebay_items', array('item_number' => $item_number));
    }

    function save_item($data) {
        $this->db->insert('ebay_items', $data);
    }

    public function get_part_number($data){
        $query = $this->db->query("select * from ebay_store where item_number = '$data'");
        return $query->result();
    }

    public function export_csv(
        $searchItem = null,
        $columns = null,
        $searchSelected = null,
        $categories = null,
        $channels = null,
        $is_filtered_none_sold
    ) {
        $this->load->dbutil();

        if (isset($columns) && is_array($columns) && count($columns) > 0) {
            $this->db->select($columns);
        } else {
            $this->db->select(array(
                'id',
                'item_number',
                'seller',
                'sku',
                'img',
                'link',
                'quantity',
                'quantity_sold',
                'total_sold_7_days',
                "CONCAT('$', current_price)",
                'hit_counter',
                'hit_count',
                'total_hits_7_days',
                'title',
                'shipping_cost',
                'date',
                'timestamp',
                'start_time',
                'end_time',
                'registration_date',
                'listing_status',
                'inventory_qty',
                'price_research_market_low',
                'price_research_market_high'
            ));
        }

        if (isset($searchItem) && $searchItem != '') {
            if($searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all') {
                $this->db->group_start();

                if ($searchSelected == 'all') {
                    foreach ($searchItem as $item) {
                        $this->db->or_like('LOWER(sku)', strtolower($item))
                                 ->or_like('LOWER(title)', strtolower($item))
                                 ->or_like('LOWER(item_number)', strtolower($item));
                    }
                } else {
                    foreach ($searchItem as $item) {
                        $this->db->or_like("LOWER($searchSelected)", strtolower($item));
                    }
                }

                $this->db->group_end();
            } else {
                $this->db->like("LOWER($searchSelected)", strtolower($searchItem));
            }
        }

        if (isset($categories) && is_array($categories) && count($categories) > 0) {
            $this->db->where_in('inventory_cat', $categories);
        }

        if (isset($channels) && is_array($channels) && count($channels) > 0) {
            $this->db->where_in('LOWER(seller)', $channels);
        }

        if ($is_filtered_none_sold == 'past_week') {
            $this->db->where('start_time >', 'NOW() - INTERVAL 7 DAY', false)
                        ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
                        ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '3_weeks'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 21 DAY', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '1_month'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 1 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '3_months'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 3 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '6_months'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 6 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == 'past_year'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 1 YEAR', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        }
        
        $this->db->where("date > DATE(NOW() - INTERVAL 1 DAY)")
                 ->order_by('id');

        $query = $this->db->get('ebay_items');

        return $this->dbutil->csv_from_result($query);
    }


    public function get_keyword_prase($sku){
        $query = $this->db->query("select * from keywords where keyword_par = '$sku'");

        return $query->result();
    }

    public function get_summary(
        $searchItem = null,
        $searchSelected = null,
        $categories = null,
        $channels = null,
        $order_by,
        $sort_order,
        $is_filtered_none_sold
    ){
        $this->db->select(array(
            'ebay_items.id AS id',
            'ebay_items.item_number AS item_number',
            'ebay_items.seller AS seller',
            'ebay_items.sku AS sku',
            'ebay_items.img AS img',
            'ebay_items.link AS link',
            'ebay_items.quantity AS quantity',
            'ebay_items.quantity_sold AS quantity_sold',
            'ebay_items.total_sold_7_days AS total_sold_7_days',
            'ebay_items.current_price AS current_price',
            'ebay_items.net_sold_7_days AS net_sold_7_days',
            'ebay_items.net_sold_total AS net_sold_total',
            'ebay_items.hit_counter AS hit_counter',
            'ebay_items.hit_count AS hit_count',
            'ebay_items.total_hits_7_days AS total_hits_7_days',
            'ebay_items.title AS title',
            'ebay_items.shipping_cost AS shipping_cost',
            'ebay_items.date AS date',
            'ebay_items.timestamp AS timestamp',
            'ebay_items.start_time AS start_time',
            'ebay_items.end_time AS end_time',
            'ebay_items.registration_date AS registration_date',
            'ebay_items.listing_status AS listing_status',
            'ebay_items.inventory_qty AS inventory_qty',
            'ebay_items.price_research_market_low AS price_research_market_low',
            'ebay_items.price_research_market_high AS price_research_market_high'
        ));

		if (isset($searchItem) && $searchItem != '') {
            if($searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all') {
                $this->db->group_start();

                if ($searchSelected == 'all') {
                    foreach ($searchItem as $item) {
                        $this->db->or_like('LOWER(ebay_items.sku)', strtolower($item))
                                 ->or_like('LOWER(ebay_items.title)', strtolower($item))
                                 ->or_like('LOWER(ebay_items.item_number)', strtolower($item));
                    }
                } else {
                    foreach ($searchItem as $item) {
                        $this->db->or_like("LOWER($searchSelected)", strtolower($item));
                    }
                }

                $this->db->group_end();
            } else {
                $this->db->like($searchSelected, $searchItem);
            }
        }

        if (isset($categories) && is_array($categories) && count($categories) > 0) {
            $this->db->where_in('inventory_cat', $categories);
        }

        if (isset($channels) && is_array($channels) && count($channels) > 0) {
            $this->db->where_in('seller', $channels);
        }

        if ($is_filtered_none_sold == 'past_week') {
            $this->db->where('start_time >', 'NOW() - INTERVAL 7 DAY', false)
                        ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
                        ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '3_weeks'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 21 DAY', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '1_month'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 1 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '3_months'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 3 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '6_months'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 6 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == 'past_year'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 1 YEAR', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        }

        if (isset($order_by) && isset($sort_order) && $order_by !== '' && $sort_order !== '') {
            $this->db->order_by($order_by, $sort_order);
        } else {
            $this->db->order_by('id');
        }

		$this->db->where("date > DATE(NOW() - INTERVAL 1 DAY)");

        $query = $this->db->get('ebay_items');
 
        return $query->result();
    }

    public function get(
        $searchItem = null,
        $searchSelected = null,
        $categories = null,
        $channels = null,
        $order_by,
        $sort_order,
        $is_filtered_none_sold,
        $items_offset = 0
    ) {
        $this->db->select(array(
            'ebay_items.id AS id',
            'ebay_items.item_number AS item_number',
            'ebay_items.seller AS seller',
            'ebay_items.sku AS sku',
            'ebay_items.img AS img',
            'ebay_items.link AS link',
            'ebay_items.quantity AS quantity',
            'ebay_items.quantity_sold AS quantity_sold',
            'ebay_items.total_sold_7_days AS total_sold_7_days',
            'ebay_items.current_price AS current_price',
            'ebay_items.net_sold_7_days AS net_sold_7_days',
            'ebay_items.net_sold_total AS net_sold_total',
            'ebay_items.hit_counter AS hit_counter',
            'ebay_items.hit_count AS hit_count',
            'ebay_items.total_hits_7_days AS total_hits_7_days',
            'ebay_items.title AS title',
            'ebay_items.shipping_cost AS shipping_cost',
            'ebay_items.date AS date',
            'ebay_items.timestamp AS timestamp',
            'ebay_items.start_time AS start_time',
            'ebay_items.end_time AS end_time',
            'ebay_items.registration_date AS registration_date',
            'ebay_items.listing_status AS listing_status',
            'ebay_items.inventory_qty AS inventory_qty',
            'ebay_items.price_research_market_low AS price_research_market_low',
            'ebay_items.price_research_market_high AS price_research_market_high'
        ));

		if (isset($searchItem) && $searchItem != '') {
            if($searchSelected == 'sku' || $searchSelected == 'title' || $searchSelected == 'all') {
                $this->db->group_start();

                if ($searchSelected == 'all') {
                    foreach ($searchItem as $item) {
                        $this->db->or_like('LOWER(ebay_items.sku)', strtolower($item))
                                 ->or_like('LOWER(ebay_items.title)', strtolower($item))
                                 ->or_like('LOWER(ebay_items.item_number)', strtolower($item));
                    }
                } else {
                    foreach ($searchItem as $item) {
                        $this->db->or_like("LOWER($searchSelected)", strtolower($item));
                    }
                }

                $this->db->group_end();
            } else {
                $this->db->like($searchSelected, $searchItem);
            }
        }

        if (isset($categories) && is_array($categories) && count($categories) > 0) {
            $this->db->where_in('inventory_cat', $categories);
        }

        if (isset($channels) && is_array($channels) && count($channels) > 0) {
            $this->db->where_in('seller', $channels);
        }

        if ($is_filtered_none_sold == 'past_week') {
            $this->db->where('start_time >', 'NOW() - INTERVAL 7 DAY', false)
                        ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
                        ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '3_weeks'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 21 DAY', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '1_month'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 1 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '3_months'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 3 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == '6_months'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 6 MONTH', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        } else if ($is_filtered_none_sold == 'past_year'){
            $this->db->where('start_time >', 'NOW() - INTERVAL 1 YEAR', false)
            ->where('start_time <', 'NOW() - INTERVAL 1 DAY', false)
            ->where('quantity_sold', 0);
        }

        if (isset($order_by) && isset($sort_order) && $order_by !== '' && $sort_order !== '') {
            $this->db->order_by($order_by, $sort_order);
        } else {
            $this->db->order_by('id');
        }

		$this->db->where("date > DATE(NOW() - INTERVAL 1 DAY)")
                 ->limit(100, $items_offset);

        $query = $this->db->get('ebay_items');
        //echo $query;
        return $query->result();

    } 
    
    //for graph
    public function item_trend($item_number) {
        $this->db->select(array(
            'date',
            'hit_count',
            'quantity_sold',
            'current_price',
            'total_sold_7_days',
            'total_hits_7_days'
        ))->where('item_number', $item_number)
                 ->from('ebay_items');

        $ebay_items_query = $this->db->get_compiled_select();

        $this->db->select(array(
            'date',
            'hit_count',
            'quantity_sold',
            'current_price',
            'total_sold_7_days',
            'total_hits_7_days'
        ))->where('item_number', $item_number)
                 ->where('date >', 'DATE(NOW() - INTERVAL 8 DAY)', false)
                 ->order_by('date', 'ASC')
                 ->from('ebay_items_archive');

        $ebay_items_archive_query = $this->db->get_compiled_select();

        $query = $this->db->query("$ebay_items_query UNION $ebay_items_archive_query");

        return $query->result();
    }

    public function sellers() {

        $this->db->select('name');

        $query = $this->db->get('ebay_items_store');

        return $query->result();
    }
    public function search_item_number($par, $store) {
        $this->db->select('item_number');
        // $this->db->where('seller', $store);
        $this->db->like('sku', $par);
        $this->db->limit(1, 0);

        $query = $this->db->get('ebay_items');

        return $query->row();
    }

    public function update_description($id, $description, $key=null){

        $this->db->where('id', $id);
        $this->db->update("ebay_items", array('title' => $description));
        return $key;
    }

    public function update_price($id, $price) {
        $this->db->where('id', $id);
        $this->db->update('ebay_items', array('current_price' => $price));
   }

    public function update_quantity($id, $quantity) {
        $this->db->where('id', $id);
        $this->db->update('ebay_items', array('quantity' => $quantity));
   }

    public function get_data_from_ebay_items($store, $item_number){
        $query = $this->db->query("select * from ebay_items where seller = '$store' and item_number = '$item_number'");

        return $query->result();
    }

    public function update_ebay_items($data_update, $store, $item_number){
        $this->db->where('seller', $store);
        $this->db->where('item_number', $item_number);
        $this->db->update("ebay_items", $data_update);
        return $key;
    }

    function load_categories() {
        $query = $this->db->select('category')
                          ->distinct()
                          ->order_by('category')
                          ->get('ebay_items');

        return $query->result();
    }

    public function total_sold_ebay_items($sku = null){
        $this->db->select();
        $this->db->select_max('quantity_sold');
        $this->db->like('sku', $sku);

        $query = $this->db->get('ebay_items');
        return $query->result();

    }

    public function total_sold_ebay_archive($sku = null){
        $this->db->select();
        $this->db->select_max('quantity_sold');
        $this->db->where('sku', $sku);

        $query = $this->db->get('ebay_items_archive');
        return $query->result();

    }

    function load_channel(){
        $query = $this->db->select('seller')
                          ->distinct()
                          ->order_by('seller')
                          ->get('ebay_items');

        return $query->result();
    }
}

?>