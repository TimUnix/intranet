<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photo_request_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_request(){
        $this->db->select();
        $this->db->where( 'is_archived',false);
        $this->db->where( 'done',false);
        
	 	$query = $this->db->get('photo_request');
	 	return $query->result();
         $this->db->select();
    }

    public function get_done($done = null){
        $this->db->select();
        $this->db->where( 'done',$done);
        $this->db->where( 'is_archived',false);
        
	 	$query = $this->db->get('photo_request');
	 	return $query->result();
         $this->db->select();
    }

    function insert_request($data){
            $this->db->insert("photo_request", $data);
            }
    
    public function search_it ($searchItem = null){
            $this->db->select();
            if(isset($searchItem) && $searchItem != ''){
            $this->db->where( 'sku',$searchItem);
            $this->db->where( 'is_archived',false);
			$this->db->where( 'done',false);
            }
            $this->db->where( 'is_archived',false);
			$this->db->where( 'done',false);
            $query = $this->db->get('photo_request');
                return $query->result();
    }
        
    public function search_archive($searchItem = null){
        $this->db->select();
        $this->db->where( 'sku',$searchItem);
        $this->db->where( 'is_archived',true);
        
        $query = $this->db->get('photo_request');
            return $query->result();
        }

	public function search_done($searchItem = null){
		$this->db->select();
		$this->db->where( 'sku',$searchItem);
		$this->db->where( 'done',true);
		
		$query = $this->db->get('photo_request');
			return $query->result();
		}

    public function update_col(
    $data,  
    $sku,
    $name){
    $this->db->where('sku', $sku);
    $this->db->where('name', $name);
    $this->db->update("photo_request", $data);
    return $key;
    }

    public function update_col_addtolinn(
        $data,  
        $sku,
        $name,$datatoupdate,$user){
            $this->db->where('sku', $sku);
            $this->db->where('name', $name);
            $this->db->set(array(
            'date_added' => date('Y-m-d'),
            'added_by' => $user,
            'added_to_linnworks' => $datatoupdate
        ));
       
        $this->db->update("photo_request");
        return $key;
        }
        
    function mark_done($id, $user) {
        return $this->db->set(array(
            'done' => true,
            'done_date' => date('Y-m-d'),
            'marked_done_by' => $user
        ))
                        ->where('id', $id)
                        ->update('photo_request');
    }

    function mark_undone($id, $user) {
        return $this->db->set(array(
            'done' => false,
            'done_date' => date('Y-m-d'),
            'marked_done_by' => $user
        ))
                        ->where('id', $id)
                        ->update('photo_request');
    }

    function archive($id, $user, $notes) {
        return $this->db->set(array(
            'is_archived' => true,
            'archived_date' => date('Y-m-d'),
            'archived_by' => $user,
            'comments' => $notes
        ))
                        ->where('id', $id)
                        ->update('photo_request');
    }

    public function get_archive($is_archive = null){
        $this->db->select();
        $this->db->where( 'is_archived',$is_archive);
        
	 	$query = $this->db->get('photo_request');
	 	return $query->result();
         $this->db->select();
    }
}
?>
