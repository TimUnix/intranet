<?php
class Memory_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        parent::__construct();
    }


    public function load_sku($sku) {
        $query = $this->db->select(array(
            'id',
            'sku',
            'barcode',
            'brand',
            'con',
            'memory_family',
            'memory_type',
            'memory_size',
            'speed',
            'registry',
            'cycles',
            'rank',
            'voltage',
            'notes'
        ))->where('sku', $sku)
                          ->get('memory');

       return $query->row();
       }

    public function get_memory()
    {
        $query = $this->db->query("select * from memory");

        return $query->result();
    }
       function get_field()
        {
            $fields = $this->db->list_fields('memory');
            return $fields;
        }
        public function update_memorycol($data, $barcode, $sku){
            $this->db->where('sku', $sku);
            $this->db->where('barcode', $barcode);
            $this->db->update("memory", $data);
            return $key;
        }
    
    public function get_user_id($email)
    {
        $query = $this->db->query("select * from users where email = '$email'");

        return $query->result();
    }
    public function get_search_exist($user_id, $search){
        $query = $this->db->query("select * from search_history where user_id = '$user_id' and search = '$search'");

        return $query->result();
    }
    public function update_memory_search($user_id, $search, $data){
        $this->db->where('user_id', $user_id);
        $this->db->where('search', $search);
        $this->db->update("search_history", $data);
        return $key;
    }
    public function insert_memory_search($data){
        $this->db->insert("search_history",$data);
			return $this->db->insert_id();
    }
    public function get_memory_search($sku, $sortOrder, $orderBy){
        $this->db->like('sku', $sku);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('memory');

        return $query->result();
    }
    public function generate_csv($sku, $sortOrder, $orderBy){
        $this->load->dbutil();
        $this->db->like('sku', $sku);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('memory');

        return $this->dbutil->csv_from_result($query);
    }
    public function get_search_history_for_user($user_id){
        $query = $this->db->query("select * from search_history where user_id = '$user_id'");

        return $query->result();
    }


    public function update_cpucol($data, $stepcode, $partnumber){
        $this->db->where('part', $partnumber);
        $this->db->where('stepcode', $stepcode);
        $this->db->update("cpu_new", $data);
        return $key;
    }


    public function delete_memorycol($sku, $barcode){
        $this->db->where('sku', $sku);
        $this->db->where('barcode', $barcode);
        $this->db->delete('memory');
        return 1;
    }



    function update_request($data, $listing_id, $key)
    {
        $this->db->where($listing_id, $key);
        $this->db->update("listing_table", $data);
        return $key;
    }


    public function check_memory($sku, $barcode){
        $querysku = $this->db->query("select * from memory where sku = '$sku'");
        $querybar = $this->db->query("select * from memory where barcode = '$barcode'");

        if($querysku->num_rows() > 1){
            return "SKU is Already Exist";
        }else{
            if($querybar->num_rows() > 1){
                return "BarCode is Already Exist";
            }else{
                return "Not Found";
            }
        }
    }


    public function insert_new_mem($data){
        $this->db->insert("memory",$data);
        return $this->db->insert_id();
    }

    
    
}
