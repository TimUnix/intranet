<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function load_listing(
        $search_query,
        $category_filters = null,
        $channel_filters = null,
        $last_id = null,
        $users = null,
        $done_filter = null
        
    ) {
        $limit = 100;

        $this->db->select(array(
            'listing.id',
            'listing.sku',
            'listing.description',
            'listing.category',
            'listing.quantity',
            'listing.price',
            'listing.price_research_market_low',
            'listing.price_research_market_high',
            'listing.note',
            'listing.link',
            'listing.link_last_edited',
            'listing.link_last_edited_by',
            'listing.channel',
            'listing.is_done',
            'listing.done_date',
            'listing.marked_done_by',
            'listing.created_by',
            'listing.price_low_last_edited_by',
            'listing.price_low_last_edited',
            'listing.price_high_last_edited_by',
            'listing.price_high_last_edited',
            'listing.note_last_edited_by',
            'listing.note_last_edited',
            'listing.channel_last_edited_by',
            'listing.channel_last_edited',
            'listing.comp_link',
            'listing.comp_link_last_edited_by',
            'listing.comp_link_last_edited'
        ))->limit($limit)
                 ->order_by('listing.id', 'DESC');

        if (isset($category_filters) && is_array($category_filters) && count($category_filters) > 0) {
            $this->db->where_in('category', $category_filters);
        }

        if (isset($channel_filters) && is_array($channel_filters) && count($channel_filters) > 0) {
            $this->db->where_in('LOWER(channel)', $channel_filters, false);
        }

        if (isset($search_query)) {
            $this->db->like('LOWER(sku)', strtolower($search_query), false);
        }

        if (isset($last_id) && $last_id !== '') {
            $this->db->where('listing.id <', $last_id);
        }

        if (isset($done_filter)) {
            $this->db->where('is_done',($done_filter));
        }
        
        

        if (isset($users) && is_array($users)) {
            $this->db->group_start();

            foreach ($users as $user) {
                $this->db->or_like('listing.price_low_last_edited_by', $user)
                            ->or_like('listing.price_high_last_edited_by', $user)
                            ->or_like('listing.note_last_edited_by', $user)
                            ->or_like('listing.link_last_edited_by', $user)
                            ->or_like('listing.comp_link_last_edited_by', $user)
                            ->or_like('listing.channel_last_edited_by', $user)
                            ->or_like('listing.marked_done_by', $user);
                }

            $this->db->group_end();
        }

        $query = $this->db->get('listing');
        
        return $query->result();
    }

    function create_listing($data) {
        $is_success = $this->db->insert('listing', $data);

        if (!$is_success) {
            return null;
        }

        $id = $this->db->insert_id();

        $query = $this->db->select(array(
            'listing.id',
            'listing.sku',
            'listing.description',
            'listing.category',
            'listing.quantity',
            'listing.price',
            'listing.price_research_market_low',
            'listing.price_research_market_high',
            'listing.note',
            'listing.link',
            'listing.link_last_edited',
            'listing.link_last_edited_by',
            'listing.channel',
            'listing.is_done',
            'listing.created_by'
        ))->where('listing.id', $id)
                          ->limit(1)
                          ->get('listing');

        return $query->result();
    }

    function mark_done($id, $user) {
        return $this->db->set(array(
            'is_done' => true,
            'done_date' => date('Y-m-d'),
            'marked_done_by' => $user
        ))
                        ->where('id', $id)
                        ->update('listing');
    }

    function mark_undone($id, $user) {
        return $this->db->set(array(
            'is_done' => false,
            'done_date' => date('Y-m-d'),
            'marked_done_by' => $user
        ))
                        ->where('id', $id)
                        ->update('listing');
    }

    function check_sku_exists($sku) {
        return $this->db->where('sku', $sku)
                        ->get('listing')
                        ->num_rows() > 0;
    }

    function load_categories() {
        $query = $this->db->select('category AS name')
                          ->distinct()
                          ->order_by('category')
                          ->get('listing');

        return $query->result();
    }

    function save_column($id, $user, $value, $column){
        $columns = array ($column => $value);

        if($column == 'link'){
                $columns['link_last_edited'] = date('Y-m-d');
                $columns['link_last_edited_by'] = $user;
        }else if($column == 'price_research_market_low'){
            $columns['price_low_last_edited'] = date('Y-m-d');
            $columns['price_low_last_edited_by'] = $user;
        }else if($column == 'price_research_market_high'){
            $columns['price_high_last_edited'] = date('Y-m-d');
            $columns['price_high_last_edited_by'] = $user;
        }else if($column == 'note'){
            $columns['note_last_edited'] = date('Y-m-d');
            $columns['note_last_edited_by'] = $user;
        }else if($column == 'channel'){
            $columns['channel_last_edited'] = date('Y-m-d');
            $columns['channel_last_edited_by'] = $user;
        }else if($column == 'comp_link'){
            $columns['comp_link_last_edited'] = date('Y-m-d');
            $columns['comp_link_last_edited_by'] = $user;
        }
        
        
        $result = $this->db->set($columns)
                        ->where('id', $id)
                        ->update('listing');
        return $result;
    }

    function getprice($id){
        $this->db->select();
        $this->db->where('id', $id);
        $query = $this->db->get('listing');

        return $query->result();
    }

    public function export_csv($filter_sku = null) {
        $this->load->dbutil();

        $this->db->select();
        if(isset($filter_sku) && trim($filter_sku) !== ''){
	 		$this->db->where('sku', $filter_sku);
	 	}
	 	$this->db->order_by('description', 'DESC');

	 	$query = $this->db->get('listing');

        return $this->dbutil->csv_from_result($query);
    }

    function get_pick_users(){
        $this->db->select(array('id', 'firstname', 'lastname', 'email'));
        $query = $this->db->get('users');

        return $query->result();

    }

    function get_done($id, $done_filters) {
        return $this->db->where('is_done', $done_filter)
                        ->get('listing')
                        ->num_rows() > 0;
    }
}


