<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_items_logs_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data){
        $this->db->insert('ebay_items_logs', $data);
        return $this->db->insert_id();
    }

    public function get_item_logs($item_number){
        $this->db->select(array(
            'ebay_items_logs.item_number AS item_number',
            'ebay_items_logs.old_value AS old_value',
            'ebay_items_logs.new_value AS new_value',
            'ebay_items_logs.edited_by AS edited_by',
            'ebay_items_logs.date_edited AS date_edited',
            'action_type_ads.description AS action'
        ))->join(
            'action_type_ads',
            'ebay_items_logs.action_type_ads = action_type_ads.id',
            'left'
        )->where('item_number', $item_number)
                 ->order_by('date_edited', 'DESC');
        
        $query = $this->db->get('ebay_items_logs');
        
        return $query->result();
    }
}

?>
