<?php

class Navigation_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_navigation_menu($userid) {
        /*
           select
           menu_id, menu_access, menu_title, menu_parent
           from user_menu left join menu using(menu_id) where user_id = 107 and menu_id = menu_parent and menu_access = 1 order by menu_order
         */
        $this->db->select(array(
            'user_menu.menu_id', 'menu_access', 'menu_title', 'menu_parent', 'menu_intranet', 'menu_file', 'icons'
        ));

        $this->db->from('user_menu');

        $this->db->join(
            'menu',
            'user_menu.menu_id = menu.menu_id',
            'left'
        );

        $this->db->where(array('user_id =' => $userid, 'menu_access =' => '1'));

        $this->db->order_by('menu_order');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_first_available_menu_item($userid) {
        $this->db->select(array(
            'menu.menu_title'
        ));

        $this->db->join(
            'menu',
            'ON(menu.menu_id = user_menu.menu_id)',
            'left'
        );

        $this->db->where('user_menu.user_id', $userid);
        $this->db->where('user_menu.menu_access', 1);
        $this->db->where('menu.menu_file !=', '');
        $this->db->order_by('menu.menu_order', 'ASC');
        $this->db->limit(1, 0);

        $query = $this->db->get('user_menu');

        return $query->row();
    }

    public function get_user_menu_access($user_id, $menu_intranet) {
        $this->db->select('um.menu_access, m.menu_title');
        $this->db->from(array( 'user_menu um', 'menu m' ));
        $this->db->where('um.menu_id = m.menu_id');
        $this->db->where(array(
            'm.menu_intranet' => $menu_intranet,
            'um.user_id' => $user_id,
        ));
        $query = $this->db->get();

        return $query->row();
    }
}

?>
