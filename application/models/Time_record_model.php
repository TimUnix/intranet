<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time_record_model extends CI_Model{
	function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert_time_record($data){
    	$this->db->where('status', 'Time In');
        $this->db->insert('time_record', $data);
        return $this->db->insert_id();
    }
    public function insert_approved_request_to_time_record($data){
        $this->db->insert('time_record', $data);
        return $this->db->insert_id();
    }
//
	public function insert_time_record_requests($data){
    	$this->db->where('status', 'Time In');
        $this->db->insert('time_record_requests', $data);
        return $this->db->insert_id();
    }
//
	 public function insert_many_leave($leave_requests){
	 	$this->db->trans_start();
	 	foreach($leave_requests as $leave_request){
	 		$this->db->insert('time_record', $leave_request);
	 	}
	 	$this->db->trans_complete();
	 }

	 public function insert_many_leave_records($leave_requests){
		$this->db->trans_start();
		foreach($leave_requests as $leave_request){
			$this->db->insert('time_record_requests', $leave_request);
		}
		$this->db->trans_complete();
	}

	 public function insert_eod($data, $email){
	 	$this->db->where('email', $email);
	 	$this->db->where('time_out', null);
	 	$this->db->group_start()
	 			->where('status', 'Time In')
	 			->or_where('status', 'Half Day Leave')
	 			->group_end();
	 	$this->db->order_by('date_in', 'DESC');
	 	$this->db->limit(1);
	 	$this->db->update('time_record', $data);
	 	return $this->db->insert_id();

    }

	 public function update_half_day_leave_time_in($data, $email, $date){
	 	$this->db->where('email', $email);
	 	$this->db->where('date_in', $date);
	 	$this->db->update('time_record', $data);

	 	return $this->db->insert_id();
	 }

	 public function insert_schedule($data){
	 	$this->db->insert('time_schedule', $data);
        return $this->db->insert_id();
    }

    public function get_item($email){
	 	$this->db->select();
	 	
	 	$this->db->where('email', $email);
	 	$this->db->where('date_in !=', null);
	 	$this->db->group_start()
	 			 ->where('time_request', 'Approved')
	 			 ->or_where('time_request', null)
	 			 ->group_end();
	 	$this->db->order_by('date_in', 'DESC');

	 	$query = $this->db->get('time_record');
        return $query->result();
    }

    public function load_schedule($filter_employee){
	 	$this->db->select();
	 	$this->db->where('is_active', '1');

	 	if(isset($filter_employee) && ($filter_employee != null || $filter_employee != '')){
	 		$this->db->where('email', $filter_employee);
	 	}
	 	$query = $this->db->get('users');
	 	return $query->result();
    }

	 public function get_eod($items_offset){
		$this->db->select(array(
			'time_record.employee AS employee',
			'time_record.finished_task AS finished_task',
			'time_record.challenges AS challenges',
			'time_record.tom_goal AS tom_goal',
			'time_record.date_in AS date_in'));

			$this->db->where('status !=', 'Leave');	
			$this->db->order_by('date_in', 'DESC');
			$this->db->limit(100, $items_offset);
		$query = $this->db->get('time_record');
        return $query->result();
	
	}

	 public function filter_date_own($email, $min_date = null, $max_date=null){
	 	$this->db->select();
	 	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('date_in >=', $min_date);
	 		$this->db->where('date_in <=', $max_date);
	 	}
	 	$this->db->where('email', $email);
	 	$this->db->order_by('date_in', 'DESC');

	 	$query = $this->db->get('time_record');
        return $query->result();
    }

    public function count_number_days($email, $min_date, $max_date){
    	$this->db->select(array('COUNT(*) as total'));
	 	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('date_in >=', $min_date);
	 		$this->db->where('date_in <=', $max_date);
	 	}
	 	$this->db->where('email', $email);
	 	$this->db->order_by('date_in', 'DESC');

	 	$query = $this->db->get('time_record');
        return $query->result();
    }

    public function export_csv($filter_employee = null) {
        $this->load->dbutil();

        $this->db->select(array(
	 		'time_record.id AS id',
	 		'time_record.employee AS employee',
	 		'time_record.email AS email', 
	 		'time_record.status AS status', 
	 		'time_record.time_in AS time_in', 
	 		'time_record.time_out AS time_out', 
	 		'time_record.date_in AS date_in',
	 		'time_record.date_out AS date_out',
	 		'time_record.schedule_in as schedule_in',
	 		'time_record.schedule_out AS schedule_out',
	 		'time_record.no_hours AS no_hours',
	 		'time_record.finished_task AS finished_task',
	 		'time_record.challenges AS challenges',
	 		'time_record.tom_goal AS tom_goal',
	 		'time_record.time_request AS time_request',
	 		'time_record.request_date AS request_date',
	 		'time_record.manager AS manager',
	 		'time_record.manager_note AS manager_note',
	 		'time_record.leave_day_type AS leave_day_type',
	 		'time_record.start_leave_date AS start_leave_date',
	 		'time_record.end_leave_date AS end_leave_date',
	 		'time_record.time_of_day AS time_of_day',
	 		'time_record.leave_type AS leave_type',
	 		'time_record.leave_reason AS leave_reason',
	 		'users.is_active'
	 		
	 	));

	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left'
        );

        if(isset($filter_employee) && trim($filter_employee) !== ''){
	 		$this->db->where('time_record.email', $filter_employee);
	 	}
	 	
	 	$this->db->where('users.is_active', '1');
	 	$this->db->where('time_record.date_in !=', null);
	 	$this->db->group_start()
	 			 ->where('time_record.time_request', 'Approved')
                 ->or_where('time_record.time_request', null)
                 ->group_end();
        $this->db->order_by('time_record.date_in', 'DESC');

	 	$query = $this->db->get('time_record');

        return $this->dbutil->csv_from_result($query);
    }

	public function export_csv_eod($filter_employee = null, $min_date = null, $max_date = null) {
        $this->load->dbutil();

        $this->db->select(array(
	 		'time_record.id AS id',
	 		'time_record.employee AS employee',
			'time_record.email AS email',
			'time_record.status AS status',
			'time_record.time_in AS time_in',
			'time_record.time_out AS time_out',
			'time_record.date_in AS date_in',
	 		'time_record.date_out AS date_out',
			'time_record.schedule_in AS schedule_in',
	 		'time_record.schedule_out AS schedule_out',
	 		'time_record.finished_task AS finished_task',
	 		'time_record.challenges AS challenges',
	 		'time_record.tom_goal AS tom_goal'
	 		
	 	));

	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left'
        );

        if(isset($filter_employee) && trim($filter_employee) !== ''){
	 		$this->db->where('time_record.email', $filter_employee);
	 	}
		 
		if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
			$this->db->where('date_in >=', $min_date);
			$this->db->where('date_out <=', $max_date);
		}
	 	
	 	$this->db->where('users.is_active', '1');
	 	$this->db->where('time_record.date_in !=', null);
	 	$this->db->group_start()
	 			 ->where('time_record.time_request', 'Approved')
                 ->or_where('time_record.time_request', null)
                 ->group_end();
        $this->db->order_by('time_record.date_in', 'DESC');

	 	$query = $this->db->get('time_record');

        return $this->dbutil->csv_from_result($query);
    }

    public function own_generate_pdf($min_date = null, $max_date = null, $email){
    	$this->db->select(array(
    		'employee',
    		'email',
        	'status',
        	'time_in',
        	'time_out',
        	'date_in',
        	'schedule_in',
        	'schedule_out',
        	'no_hours'

        ));

        if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('date_in >=', $min_date);
	 		$this->db->where('date_in <=', $max_date);
	 	}

	 	$this->db->where('email', $email);
	 	$this->db->where('time_record.date_in !=', null);
	 	$this->db->group_start()
	 			 ->where('time_record.time_request', 'Approved')
                 ->or_where('time_record.time_request', null)
                 ->group_end();
        $this->db->order_by('time_record.date_in', 'DESC');

	 	$query = $this->db->get('time_record');
	 	return $query->result();

    }

    public function total_no_hours($min_date = null, $max_date = null, $email){
    	$this->db->select_sum('no_hours');

    	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('date_in >=', $min_date);
	 		$this->db->where('date_in <=', $max_date);
	 	}

	 	$this->db->where('email', $email);
	 	$this->db->where('time_record.date_in !=', null);
	 	$this->db->group_start()
	 			 ->where('time_record.time_request', 'Approved')
                 ->or_where('time_record.time_request', null)
                 ->group_end();
        $this->db->order_by('time_record.date_in', 'DESC');

	 	$query = $this->db->get('time_record');
	 	return $query->result();
    }


    public function get_employees($filter_employee, $min_date, $max_date){
	 	$this->db->select(array(
	 		'time_record.id AS id',
	 		'time_record.employee AS employee',
	 		'time_record.email AS email', 
	 		'time_record.status AS status', 
	 		'time_record.time_in AS time_in', 
	 		'time_record.date_in AS date_in',
	 		'time_record.time_out AS time_out',
	 		'time_record.no_hours AS no_hours',
	 		'time_record.date_out AS date_out',
	 		'time_record.finished_task AS finished_task',
	 		'time_record.challenges AS challenges',
	 		'time_record.tom_goal AS tom_goal',
	 		'time_record.schedule_in as schedule_in',
	 		'time_record.schedule_out AS schedule_out',
	 		'users.is_active',
	 		'time_record.time_request'
	 	));
	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left'
        );
                 

        if(isset($filter_employee) && trim($filter_employee) !== ''){
	 		$this->db->where('time_record.email', $filter_employee);
	 	}
	 	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('time_record.date_in >=', $min_date);
	 		$this->db->where('time_record.date_in <=', $max_date);
	 	}

	 	$this->db->where('users.is_active', '1');
	 	$this->db->where('date_in !=', null);
	 	$this->db->group_start()
	 			->where('time_record.time_request', 'Approved')
                 ->or_where('time_record.time_request', null)
                ->group_end();
	 	$this->db->order_by('time_record.date_in', 'DESC');

	 	$query = $this->db->get('time_record');

       return $query->result();
	 }

    public function filter_employee($filter_employee = null, $min_date = null, $max_date = null){
	 	$this->db->select(array(
	 		'time_record.id AS id',
	 		'time_record.employee AS employee',
	 		'time_record.email AS email', 
	 		'time_record.status AS status', 
	 		'time_record.time_in AS time_in', 
	 		'time_record.date_in AS date_in',
	 		'time_record.time_out AS time_out',
	 		'time_record.no_hours AS no_hours',
	 		'time_record.date_out AS date_out',
	 		'time_record.finished_task AS finished_task',
	 		'time_record.challenges AS challenges',
	 		'time_record.tom_goal AS tom_goal',
	 		'time_record.schedule_in as schedule_in',
	 		'time_record.schedule_out AS schedule_out',
	 		'users.is_active',
	 		'time_record.time_request'
	 	));
	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left'
        );

	 	if(isset($filter_employee) && trim($filter_employee) !== ''){
	 		$this->db->where('time_record.email', $filter_employee);
	 	}
	 	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('date_in >=', $min_date);
	 		$this->db->where('date_in <=', $max_date);
	 	}

	 	$this->db->where('users.is_active', '1');
	 	$this->db->where('date_in !=', null);
	 	$this->db->group_start()
	 			->where('time_record.time_request', 'Approved')
                 ->or_where('time_record.time_request', null)
                ->group_end();
	 	$this->db->order_by('date_in', 'DESC');


	 	$query = $this->db->get('time_record');
      
        return $query->result();
    }

	 public function filter_employee_eod($filter_employee = null, $min_date = null, $max_date = null, $items_offset){
		$this->db->select(array(
			'time_record.employee AS employee',
			'time_record.finished_task AS finished_task',
			'time_record.challenges AS challenges',
			'time_record.tom_goal AS tom_goal',
			'time_record.date_in AS date_in', 
			'time_record.date_out AS date_out'));

			if(isset($filter_employee) && trim($filter_employee) !== ''){
				$this->db->where('email', $filter_employee);
			}
			if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
				$this->db->where('date_in >=', $min_date);
				$this->db->where('date_out <=', $max_date);
			}

			
		$this->db->order_by('date_in', 'DESC');

		$this->db->limit(100, $items_offset);
		$query = $this->db->get('time_record');
	 
	   return $query->result();
	}

	public function filter_date_eod($min_date = null, $max_date=null){
		$this->db->select(array(
			'time_record.employee AS employee',
			'time_record.finished_task AS finished_task',
			'time_record.challenges AS challenges',
			'time_record.tom_goal AS tom_goal',
			'time_record.date_out AS date_out'));
			
		if(isset($filter_employee) && trim($filter_employee) !== ''){
			$this->db->where('email', $filter_employee);
		}
		if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
			
			$this->db->where('date_out >=', $min_date);
			$this->db->where('date_out <=', $max_date);
		}
		$this->db->order_by('date_in', 'DESC');


		$query = $this->db->get('time_record');
   
	   return $query->result();
   }

   //
	 public function filter_employee_approval_request($filter_employee = null, $min_date = null, $max_date = null,
	 	$approval_status = null, $email){
	 	$this->db->select();

	 	if(isset($filter_employee) && trim($filter_employee) !== ''){
	 		$this->db->where('email', $filter_employee);
	 	}
	 	if(isset($approval_status)){
	 		$this->db->where('time_request', $approval_status);
	 	}
	 	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('start_leave_date >=', $min_date);
	 		$this->db->where('end_leave_date <=', $max_date);
	 	}
	
	 	$this->db->where('manager', $email)
                 ->where('status !=', 'Manual');
	 	$this->db->order_by('date_in', 'DESC');


	 	$query = $this->db->get('time_record_requests');
        return $query->result();
    }

    public function get_employees_filter_date($min_date = null, $max_date=null){
	 	$this->db->select();

	 	if(isset($min_date) && trim($min_date) !=='' && isset($max_date) && trim($max_date) !==''){
	 		$this->db->where('date_in >=', $min_date);
	 		$this->db->where('date_in <=', $max_date);
	 	}
	 	$this->db->order_by('date_in', 'DESC');


	 	$query = $this->db->get('time_record');
	
        return $query->result();
    }

    public function get_all_employees(){
	 	$this->db->select(array('firstname', 'lastname', 'email'));
	 	$this->db->where('is_active', '1');
	 	$query = $this->db->get('users');
        return $query->result();
    }

    public function limit_time_in($email){
	 	$this->db->select(array(
	 		'time_record.date_in', 
	 		'time_record.email', 
	 		'time_record.status AS status',
	 		'time_record.leave_day_type AS leave_day_type',
	 		'time_record.time_request AS time_request',
	 		'time_record.time_in',
	 		'users.schedule_in AS schedule_in',
	 		'users.schedule_out AS schedule_out',
			'users.is_flexible_unaware as flexible_unaware',
	 		'time_record.schedule_in AS schedule_in_half_day',
	 		'time_record.schedule_out AS schedule_out_half_day',
	 		'time_record.no_hours AS no_hours'
	 	));

	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left');

	 	$this->db->where('time_record.email', $email);
	 	$this->db->where('time_record.time_out', null);
	 	$this->db->order_by('time_record.date_in', 'DESC');
	 	$this->db->limit(1);
	 	$query = $this->db->get('time_record');
        
        return $query->result();
    }

    public function limit_time_out($email){
	 	$this->db->select(array(
	 		'time_record.date_out', 
	 		'time_record.email',
	 		'users.schedule_in AS schedule_in',
	 		'users.schedule_out AS schedule_out'
		));

	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left');

	 	$this->db->where('time_record.email', $email);
	 	$this->db->order_by('time_record.date_out', 'DESC');
	 	$this->db->limit(1);
	 	$query = $this->db->get('time_record');
        
        return $query->result();
    }

    public function date_modify_logs($email, $date_in_logs){
	 	$this->db->select(array(
	 		'time_record.date_in',
	 		'time_record.email', 
	 		'time_record.time_in',
	 		'users.schedule_in AS schedule_in',
	 		'users.schedule_out AS schedule_out'
	 	));
	 	$this->db->join(
	 		'users', 
	 		'ON time_record.email = users.email', 
	 		'left');

	 	$this->db->where('time_record.email', $email);
	 	$this->db->where('time_record.date_in', $date_in_logs);
	 	$query = $this->db->get('time_record');
        
        return $query->result();
    }

    public function insert_time_out_logs($data, $email, $date_in_logs){
    	$this->db->where('status', 'Time In');
	 	$this->db->where('email', $email);
	 	$this->db->where('date_in', $date_in_logs);
	 	$this->db->update('time_record', $data);
	 	return $this->db->insert_id();
    }

	public function insert_time_out_logs_requests($data, $email, $date_in_logs){
    	$this->db->where('status', 'Time In');
	 	$this->db->where('email', $email);
	 	$this->db->where('date_in', $date_in_logs);
	 	$this->db->update('time_record_requests', $data);
	 	return $this->db->insert_id();
    }

	 public function data_not_yet_exist($email, $date){
	 	$this->db->select();
	 	$this->db->where('email', $email);
	 	$this->db->where('date_in', $date);
	 	$this->db->where('leave_day_type !=', 'Half Day');
	 	$this->db->where('time_request !=', 'Approved');
	 	$this->db->where('time_in !=', null);

	 	$query = $this->db->get('time_record_requests');

        return $query->result();
	 }

	 public function get_leave_details($email, $date){
	 	$this->db->select();
	 	$this->db->where('status !=', 'Manual');
	 	$this->db->where('email', $email);
	 	$this->db->where('date_in', $date);

		$query = $this->db->get('time_record');

        return $query->result();	 	
	 }

	 public function validate_request_date($email, $date){
	 	$this->db->select('request_date');
	 	$this->db->where('email', $email);
	 	$this->db->where('request_date', $date);

	 	$query = $this->db->get('time_record_requests');
        
        return $query->result();
    }

    public function validate_exist_timeIn($email, $date){
    	$this->db->select();
    	$this->db->where('email', $email);
    	$this->db->where('date_in', $date);
    	$this->db->group_start()
    			 ->where('status', 'Time In')
    			 ->or_where('status', 'Time Out')
    			 	->or_group_start()
	    			 	->where('status', 'TITO')
	    			 	->where('time_request', 'Approved')
    			 	->group_end()
    			 	->or_group_start()
    			 		->where('status', 'Leave')
    			 		->where('time_request', 'Approved')
    			 		->where('leave_day_type', 'Whole Day')
    			 	->group_end()
    			 ->group_end();

    	$query = $this->db->get('time_record');
        
        return $query->result();

    }

	public function validate_timeOut_exist($email, $date_in_logs){
		/*$this->db->select('date_in');
		$this->db->where('email', $email);
		$this->db->where('date_out', date('Y-m-d'));
		$this->db->where('status', 'Time Out');
		$this->db->group_start()
				  ->where('date_in', date('Y-m-d'))
				  ->or_where('date_in <', date('Y-m-d'))
				  ->group_end();*/
				  $this->db->select('date_out');
				  $this->db->where('email', $email);
				  $this->db->where('date_in', $date_in_logs);
				  $this->db->where('date_out !=', null);
		$query = $this->db->get('time_record');
		//echo $query;

	   return $query->result();
   }

       public function validate_exist_timeOut($email, $date_in_logs){
		 	$this->db->select('date_out');
		 	$this->db->where('email', $email);
		 	$this->db->where('date_in', $date_in_logs);
		 	$this->db->where('date_out', NULL);
		 	$this->db->group_start()
		 			 ->where('status', 'Time In')
		 			 ->or_where('status', 'Half Day Leave')
		 			  ->group_end();
		 	$query = $this->db->get('time_record');
	        
	        return $query->result();
	    }

    public function validate_sched_timeIn($email){
	 	$this->db->select();
	 	$this->db->where('email', $email);

	 	$query = $this->db->get('users');
	 	return $query->result();
    }
//edit schedule
    public function update_schedule($data, $email){
        $this->db->where('email', $email);
        $this->db->update("users", $data);
        
        return $this->db->insert_id();
    }
	public function get_update_schedule_record($id){
    	$this->db->select();
    	$this->db->where('id', $id);

    	$query = $this->db->get('time_record');
	 	return $query->result();

    }

    public function check_approved_denied_exist($email, $start_date, $end_date){
    	$this->db->select();
    	$this->db->where('email', $email);
    	$this->db->group_start()
    			 ->where('start_leave_date', $start_date)
    			 ->or_where('end_leave_date', $end_date)
    			 ->group_end();

    	$query = $this->db->get('time_record_requests');
	 	return $query->result();
    }

	public function update_schedule_record($data, $id){
    	$this->db->where('id', $id);
        $this->db->update("time_record", $data);
        
        return $this->db->insert_id();
    }
//get record to be updated time recors requests
    public function get_update_schedule_record_requests($id){
    	$this->db->select();
    	$this->db->where('id', $id);

    	$query = $this->db->get('time_record_requests');
	 	return $query->result();

    }
//update the record selected to 
    public function update_schedule_record_requests($data, $id){
    	$this->db->where('id', $id);
        $this->db->update("time_record_requests", $data);
        
        return $this->db->insert_id();
    }
//get record to be updated from time records
	public function get_tr_details($email, $date_in){
		$this->db->select();
		$this->db->where('email', $email);
		$this->db->where('date_in', $date_in);

	   $query = $this->db->get('time_record');

	   return $query->result();	 	
	}
//get record from trr
	public function get_trequests_details($email, $date_in){
		$this->db->select();
		$this->db->where('email', $email);
		$this->db->where('date_in', $date_in);

	   $query = $this->db->get('time_record');

	   return $query->result();	 	
	}
	
//to update time records
	public function update_time_record($dataup,$email,$date_in,$idnum){
		$this->db->where('id', $idnum);
		$this->db->where('email', $email);
		$this->db->where('date_in', $date_in);
		$this->db->update("time_record", $dataup);
        
        return $this->db->insert_id();
    }




    public function get_manager(){
    	$this->db->select();
    	$this->db->where('is_manager', '1');

    	$query = $this->db->get('users');
        return $query->result();
    }

    public function load_request($email){
    	$this->db->select(array(
    		'time_record_requests.id AS id',
    		'time_record_requests.employee AS employee',
    		'time_record_requests.manager_note as manager_note',
    		'time_record_requests.request_date AS request_date',
    		'time_record_requests.start_leave_date as start_leave_date',
    		'time_record_requests.end_leave_date AS end_leave_date',
    		'time_record_requests.leave_day_type AS leave_day_type',
    		'time_record_requests.time_of_day AS time_of_day',
    		'time_record_requests.leave_type AS leave_type',
    		'time_record_requests.leave_reason AS leave_reason',
    		'time_record_requests.status AS status',
    		'time_record_requests.email AS email',
    		'time_record_requests.date_in AS date_in',
    		'time_record_requests.time_request AS time_request',
    		'users.firstname AS firstname',
    		'users.lastname AS lastname'
    	));

    	$this->db->join(
            'users',
            'ON time_record_requests.manager = users.email',
            'left'
        );
        
    	$this->db->where('time_record_requests.email', $email);
    	$this->db->where('time_record_requests.time_request !=', 'Replaced with Leave');
    	$this->db->order_by('time_record_requests.request_date', 'DESC');

    	$query = $this->db->get('time_record_requests');
        
        return $query->result();
    }

    public function get_email_manager($firstname, $lastname){
    	$this->db->select(array(
    		'firstname',
    		'lastname',
    		'email'
    	));

    	$this->db->where('firstname', $firstname);
    	$this->db->where('lastname', $lastname);

    	$query = $this->db->get('users');
        return $query->result();
    }

    public function validate_request_exist($email, $date, $type){
    	$this->db->select();

    	$this->db->where('email', $email);

    	$this->db->where('time_request !=', 'Denied');
		
    	if($type == 'TITO'){
    		$this->db->where('request_date', $date);
    	}else if($type == 'Leave'){
    		$this->db->group_start()
    				 ->where('request_date', $date)
    				 ->or_where('start_leave_date', $date)
    				 ->or_where('end_leave_date', $date)
    				 ->group_end();
    	}

    	

    	$query = $this->db->get('time_record_requests');
  
        return $query->result();

    }


	public function validate_request_approved_exist_update($email, $date){
    	$this->db->select();

    	$this->db->where('email', $email);

    	$this->db->where('time_request', 'Approved');
    	$this->db->where('status', $type);

    	if($type == 'TITO'){
    		$this->db->where('request_date', $date);
    	}else if($type == 'Leave'){
    		$this->db->group_start()
    				 ->where('request_date', $date)
    				 ->or_where('start_leave_date', $date)
    				 ->or_where('end_leave_date', $date)
    				 ->group_end();
    	}

    	

    	$query = $this->db->get('time_record_requests');
  
        return $query->result();

    }
	//
	public function data_not_yet_exist_update($email, $date){
		$this->db->select();

    	$this->db->where('email', $email);

    	$this->db->where('date_in', $date);
		$this->db->where('date_out', $date);

    	$query = $this->db->get('time_record');
  
        return $query->result();;
	}

	public function update_time_record_requests($data){
    	$this->db->where('status', 'Time In');
        $this->db->update('time_record_requests', $data);
        return $this->db->insert_id();
    }
	//
    public function load_approver($email){

    	$this->db->select()
                 ->where('manager', $email)
                 ->where('status !=', 'Manual')
                 ->where('time_request', 'Pending')
                 ->order_by('start_leave_date', 'DESC');

    	$query = $this->db->get('time_record_requests');

        return $query->result();
    }

    function check_open_manual_requests($date, $email) {
        $this->db->where('date_in', $date)
                 ->where('date_out', null)
                 ->where('email', $email)
                 ->where('status', 'Manual')
                 ->where('time_request', 'Pending')
                 ->from('time_record');
            
        return $this->db->count_all_results();
    }

    function load_latest_open_manual_request($date, $email) {
        $query = $this->db->select(array( 'id', 'date_in', 'time_in' ))
                          ->where('date_in', $date)
                          ->where('date_out', null)
                          ->where('email', $email)
                          ->where('status', 'Manual')
                          ->where('time_request', 'Pending')
                          ->limit(1)
                          ->get('time_record');
                          
            
        $result = $query->row();

        return $result ?? null;
    }

    function update_manual_request($id, $data) {
        $this->db->where('id', $id)
                 ->update('time_record', $data);
    }

    function manual_requests($manager, $employee, $date) {
        if (isset($employee) && $employee !== '') {
            $this->db->where('email', $employee);
        }

        if (isset($date) && $date !== '') {
            $this->db->group_start()
                     ->where('date_in', $date)
                     ->where('date_out', $date)
                     ->group_end();
        }

        $query = $this->db->select(array(
            'date_in',
            'date_out',
            'employee',
            'email',
            'time_in',
            'time_out',
            'no_hours'
        ))->where('manager', $manager)
                          ->where('status', 'Manual')
                          ->where('time_request', 'Pending')
                          ->where('date_in <>', null)
                          ->where('date_out <>', null)
                          ->get('time_record');
        
        return $query->result();
    }

    function record_manual_approval($filters, $data) {
        $this->db->where($filters)
                 ->update('time_record', $data);
    }

    function check_in_out_exist_whole_day_leave($start_leave_date, $email){
    	$this->db->select();
    	$this->db->where('date_in', $start_leave_date);
    	$this->db->where('email', $email);
    	$this->db->group_start()
    			 ->where('status', 'Time In')
    			 ->or_where('status', 'Time Out')
    			 ->group_end();

		$query = $this->db->get('time_record');

        return $query->result();    	    
    }

    function update_in_out_copy_whole_day_leave($datas, $start_leave_date, $email){
    	$this->db->where('date_in', $start_leave_date);
    	$this->db->where('email', $email);
    	$this->db->group_start()
    			 ->where('status', 'Time In')
    			 ->or_where('status', 'Time Out')
    			 ->group_end();
    	$this->db->update('time_record', $datas);
	 	return $this->db->insert_id();
    }

    function check_check($check_leave_date, $email){
    	$this->db->select();
    	$this->db->where_in('date_in', $check_leave_date);
    	$this->db->where('email', $email);
    	$this->db->group_start()
    			 ->where('status', 'Time In')
    			 ->or_where('status', 'Time Out')
    			 ->group_end();

		$query = $this->db->get('time_record');

        return $query->result();    	    
    }

    function check_check_update($datas, $check_leave_date, $email){
    	$this->db->where_in('date_in', $check_leave_date);
    	$this->db->where('email', $email);
    	$this->db->group_start()
    			 ->where('status', 'Time In')
    			 ->or_where('status', 'Time Out')
    			 ->group_end();
    	$this->db->update('time_record', $datas);
	 	return $this->db->insert_id();
    }
	public function get_name($email){
    	$this->db->select(array(
    		'firstname',
    		'lastname'
    	));

    	$this->db->where('email', $email);

    	$query = $this->db->get('users');
        return $query->result();
    }
}

?>
