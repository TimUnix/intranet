<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_ad_draft_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function load_for_user($user_id) {
        $query = $this->db->select(array(
            'draft.id AS draft_id',
            'draft.date_added AS date_added',
            'fields.field AS field',
            'fields.value AS value',
        ))
            ->where(array(
                'draft.draft_by' => $user_id,
                'draft.archive_date' => null
            ))
            ->join(
                'ebay_ad_draft_fields fields',
                "draft.id = fields.ebay_ad_draft_id AND (fields.field = 'sku' OR fields.field = 'img-0')",
                'left'
            )
            ->get('ebay_ad_draft AS draft');

        return $query->result();
    }

    public function save_draft($user_id, $fields) {
        $this->db->insert('ebay_ad_draft', array(
            'draft_by' => $user_id
        ));

        $draft_id = $this->db->insert_id();

        $field_data = array();

        foreach ($fields as $key => $value) {
            $field_data[] = array(
                'ebay_ad_draft_id' => $draft_id,
                'field' => $key,
                'html_id' => $value['id'],
                'value' => $value['value']
            );
        }

        $this->db->insert_batch('ebay_ad_draft_fields', $field_data);
    }

    public function archive_draft($id) {
        $this->db->where('id', $id)
            ->update('ebay_ad_draft', array('archive_date' => date("Y-m-d H:i:s")));
    }

    public function draft_fields($id) {
        $query = $this->db->select(array( 'html_id', 'value' ))
            ->where('ebay_ad_draft_id', $id)
            ->get('ebay_ad_draft_fields');

        return $query->result();
    }
}
