<?php
class Cpu_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        parent::__construct();
    }


    public function load_sku($sku) {
       // $query = $this->db->query("select * from data_import_listing where sku = '$sku'");
        $query = $this->db->select(array(
            'part',
            'speed',
            'number_of_cores',
            'codename',
            'thermal_design_power',
            'cpu_cache',
            'sockets_supported',
            'grade'
        ))->where('part', $sku)
                          ->get('cpu_new');

       return $query->row();
    }

    function get_field()
    {
        $fields = $this->db->list_fields('cpu_new');
        return $fields;
    }
    public function get_cpu()
    {
        $query = $this->db->query("select * from cpu_new");

        return $query->result();
    }
    public function get_user_id($email)
    {
        $query = $this->db->query("select * from users where email = '$email'");

        return $query->result();
    }
    public function get_search_exist($user_id, $search){
        $query = $this->db->query("select * from search_history where user_id = '$user_id' and search = '$search'");

        return $query->result();
    }
    public function update_cpu_search($user_id, $search, $data){
        $this->db->where('user_id', $user_id);
        $this->db->where('search', $search);
        $this->db->update("search_history", $data);
       /// return this if incase there are error return $key;
    }
    public function insert_cpu_search($data){
        $this->db->insert("search_history",$data);
			return $this->db->insert_id();
    }
    public function get_cpu_search($partnumber, $sortOrder, $orderBy){
        $this->db->like('part', $partnumber);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('cpu_new');

        return $query->result();
    }
    public function generate_csv($partnumber, $sortOrder, $orderBy){
        $this->load->dbutil();
        $this->db->like('part', $partnumber);
        $query = $this->db->order_by($orderBy, $sortOrder);
        $query = $this->db->get('cpu_new');

        return $this->dbutil->csv_from_result($query);
    }
    public function get_search_history_for_user($user_id){
        $query = $this->db->query("select * from search_history where user_id = '$user_id'");

        return $query->result();
    }


    public function update_cpucol($data, $stepcode, $partnumber){
        $this->db->where('part', $partnumber);
        $this->db->where('stepcode', $stepcode);
        $this->db->update("cpu_new", $data);
        return $key;
    }


    public function delete_cpucol($partnumber, $stepcode){
        $this->db->where('part', $partnumber);
        $this->db->where('stepcode', $stepcode);
        $this->db->delete('cpu_new');
        return 1;
    }


    public function insert_new_cpu($data){
        $this->db->insert("cpu_new",$data);
        return $this->db->insert_id();
    }
    



    function update_request($data, $listing_id, $key)
    {
        $this->db->where($listing_id, $key);
        $this->db->update("listing_table", $data);
        return $key;
    }

    public function check_cpu_part($partnumber, $stepcode){
        $querypart = $this->db->query("select * from cpu_new where part = '$partnumber'");
        $querystep = $this->db->query("select * from cpu_new where stepcode = '$stepcode'");

        if($querypart->num_rows() > 1){
            return "PartNumber is Already Exist";
        }else{
            if($querystep->num_rows() > 1){
                return "StepNumber is Already Exist";
            }else{
                return "Not Found";
            }
        }
    }

    

    

    

    

    
}
