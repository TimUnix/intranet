<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nebula_vendor_model extends CI_Model {
    public function add_vendor($vendor_name, $logo_path) {
        $this->db->insert('nebula_vendor', array(
            'vendor_name' => $vendor_name,
            'logo_path' => $logo_path
        ));
    }

    public function check_vendor_exists($vendor_name) {
        $this->db->where('LOWER(vendor_name)', strtolower($vendor_name))
            ->from('nebula_vendor');

        return $this->db->count_all_results() > 0;
    }

    public function load_vendors() {
        $query = $this->db->select(array('po.name AS purchase_order', 'vendor.vendor_name', 'vendor.logo_path'))
            ->order_by('vendor.date_added')
            ->join('nebula_purchase_orders AS po', 'ON(po.vendor = vendor.vendor_name)', 'left')
            ->get('nebula_vendor AS vendor');

        return $query->result();
    }

    public function load_vendor($vendor) {
        $query = $this->db->where('vendor_name', $vendor)
            ->get('nebula_vendor');

        return $query->row();
    }

    public function change_name($old_name, $new_name) {
        $this->db->where('vendor_name', $old_name)
            ->update('nebula_vendor', array( 'vendor_name' => $new_name ));

        return $this->db->affected_rows() > 0;
    }

    public function load_logo_path($vendor) {
        $query = $this->db->select('logo_path')
            ->where('vendor_name', $vendor)
            ->get('nebula_vendor');

        return $query->row();
    }

    public function update_logo($vendor, $path) {
        $this->db->where('vendor_name', $vendor)
            ->update('nebula_vendor', array ( 'logo_path' => $path ));
    }
}
