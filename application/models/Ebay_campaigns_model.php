<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebay_campaigns_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function load_by_store($store) {
        $query = $this->db->select(array(
            'ebay_id',
            'name'
        ))->where('store', $store)
                          ->get('ebay_campaigns');

        return $query->result();
    }
}

?>
