<?php
	class Users_model extends CI_Model {
		function __construct(){
			parent::__construct();
			$this->load->database();
		}
 
		public function login($email, $password){
			$query = $this->db->get_where('users', array('email'=>$email));
			return $query->row_array();
		}
		public function get_user_id($email){
			$data = $this->db->query("select * from users where email = '$email'");
			return $data->result();
		}
		public function insert_users($data){
			$this->db->insert("users",$data);
			return $this->db->insert_id();
		}
		public function get_user_max(){
			$data = $this->db->query("SELECT MAX(id) AS id FROM users;");
			return $data->result();
		}
		public function get_user_max_menu(){
			$data = $this->db->query("SELECT MAX(menu_id) AS id FROM menu;");
			return $data->result();
		}
		public function insert_users_menu($data){
			$this->db->insert("user_menu",$data);
			return $this->db->insert_id();
		}
		public function get_max_user(){
			$data = $this->db->query("SELECT MAX(user_menu_id) AS user_menu_id FROM user_menu;");
			return $data->result();
		}

        public function get_table_columns($page, $userid) {
            $this->db->select("{$page}_columns");
            $this->db->where('id', $userid);

            $query = $this->db->get('users');

            return $query->row();
        }

        public function save_table_columns($page, $userid, $json_string) {
            $this->db->set("{$page}_columns", $json_string);
            $this->db->where('id', $userid);
            $this->db->update('users');
		}

        public function get_users_with_access_to_page($page) {
            $this->db->select(array(
                'users.firstname',
                'users.lastname'
            ))->join(
                'users',
                'user_menu.user_id = users.id',
                'inner'
            )->join(
                'menu',
                'user_menu.menu_id = menu.menu_id',
                'inner'
            )->where('menu.menu_title', $page)
                 ->where('user_menu.menu_access', 1)
            ->order_by('users.firstname', 'ASC');

            $query = $this->db->get('user_menu');

            return $query->result();
        }

		public function insert_menu_items($user_id)
		{
			$this->db->query("INSERT INTO user_menu(user_id, menu_id) SELECT $user_id, menu_id FROM menu");
		}

		public function update_menu_access($user_id)
		{
			$this->db->query("UPDATE user_menu SET menu_access = 1 WHERE user_id = $user_id AND menu_id IN (7, 3, 14, 15)");
		}
		
		public function setdefaultmenu($user_id){
			$sql = "UPDATE user_menu 
			SET menu_access = 1 WHERE user_id = $user_id AND menu_id = (
				SELECT menu_id
				FROM menu
				WHERE user_menu.menu_id = menu.menu_id AND menu.is_default = 1 
			)";
			$this->db->query($sql);   
		}
	

        function load_users() {
            $query = $this->db->select(array(
                'id',
                'firstname',
                'lastname'
            ))->order_by('firstname')->get('users');

            return $query->result();
        }

        function load_schedule($email) {
            $query = $this->db->select(array( 'schedule_in', 'schedule_out' ))
                              ->where('email', $email)
                              ->get('users');

            return $query->row();
        }
	}
?>
