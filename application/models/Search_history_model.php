<?php
	class Search_history_model extends CI_Model {
		function __construct(){
			parent::__construct();
			$this->load->database();
		}
 
		
		public function check_users_history($id, $search){
			$data = $this->db->query("select * from search_history where user_id = '$id' and search = '$search'");
			return $data->result();
		}

		public function update_search_history_query($id, $search, $time){
			$data = $this->db->query("update search_history set updated = '$time' where user_id = '$id' and search = '$search'");
			return $data->result();
		}
		function insert_table($data){
			$this->db->insert("search_history",$data);
			return $this->db->insert_id();
		}
		function update_search_history($data,$user_id,$id,$search,$item){
			$this->db->where($user_id,$id);
			$this->db->where($search,$item);
			$this->db->update("search_history",$data);
			return $id;
		}
		public function get_all_rows_num(){
			$data = $this->db->query("SELECT MAX(sh_id) AS sh_id FROM search_history;");
			return $data->result();
		}

        public function get_search_history_for_user($user_id) {
            $this->db->select('search');
            $this->db->where('user_id', $user_id);
            $this->db->order_by('updated', 'DESC');
            $this->db->limit(10, 0);

            $query = $this->db->get('search_history');

            return $query->result();
        }
	}
?>
