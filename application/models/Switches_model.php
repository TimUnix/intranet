<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class switches_model extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_galactus($last_id = null, $items_offset){
        $done = null;

        if (isset($done)) {
            $this->db->where('taskc',($done));
        }

        $this->db->limit(100, $items_offset);
        
        $query = $this->db->get('switches');
        return $query->result();
    }

    public function search_it ($searchItem = null, $items_offset){
        $this->db->select();
        if(isset($searchItem) && $searchItem != ''){
            $this->db->like('sku', $searchItem);
        }

        $this->db->limit(100, $items_offset);

        $query = $this->db->get('switches');
        return $query->result();
    }
    

    function mark_done($id, $user) {
        return $this->db->set(array(
            'taskc' => true,
            'done_date' => date('Y-m-d'),
            'marked_done_by' => $user
        ))
                        ->where('switchid', $id)
                        ->update('switches');
    }

    function mark_undone($id, $user) {
        return $this->db->set(array(
            'taskc' => false,
            'done_date' => date('Y-m-d'),
            'marked_done_by' => $user
        ))
                        ->where('switchid', $id)
                        ->update('switches');
    }

    
    function insert_listing($data)
        {
            $this->db->insert("switches", $data);
            
        }

    public function update_col(
        $data,  
        $sku,
        $switchmodel){
        $this->db->where('sku', $sku);
        $this->db->where('switchmodel', $switchmodel);
        $this->db->update("switches", $data);
        return $key;
        }

    function show_added_listing(){
        $this->db->select(array(
            'modelnum' ,
            'qty' ,
            'lwsku',
            'numports',
            'pspeed',
            'numupports',
            'uplinkspeed',
            'poewatts',
            'psuqty',
            'oucard',
            'noedr', 
            'imgsoft',
            'pricenote',
            'taskc',
            'title'
        ));
        
        $query = $this->db->get('switches');

        return $query->result();  
    }

    function get_done($done = null) {
       $limit = 100;
       $this->db->select();
       $this->db->where('taskc', $done);

       $query = $this->db->get('switches');
       return $query->result();
       $this->db->select();
    }

}
?>
