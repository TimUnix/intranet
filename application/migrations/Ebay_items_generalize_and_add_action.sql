ALTER TABLE `ebay_items_logs`
      CHANGE `previous_description` `old_value` VARCHAR(255),
      CHANGE `current_description` `new_value` VARCHAR(255),
      CHANGE `previous_edited_by` `edited_by` VARCHAR(255),
      CHANGE `previous_date_edited` `date_edited` DATETIME,
      ADD COLUMN `action_type_ads` INT AFTER `id`;
