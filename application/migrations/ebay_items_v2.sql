ALTER TABLE `ebay_items`
      DROP COLUMN `Timestamp`,
      CHANGE `HitCounter` `hit_count` INT(11) NULL DEFAULT NULL,
      CHANGE `SKU` `sku` VARCHAR(255) NULL DEFAULT NULL,
      CHANGE `Quantity` `quantity` INT(11) NULL DEFAULT NULL,
      CHANGE `QuantitySold` `quantity_sold` INT(11) NULL DEFAULT NULL,
      ADD `current_price` FLOAT(7,2) AFTER `quantity_sold`,
      ADD `hit_counter` VARCHAR(255) AFTER `current_price`,
      ADD `title` VARCHAR(255) AFTER `hit_counter`,
      ADD `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;
