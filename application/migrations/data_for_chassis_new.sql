-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2021 at 11:49 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linn`
--

-- --------------------------------------------------------

--
-- Table structure for table `chassis_new`
--

CREATE TABLE `chassis_new` (
  `chassis_id` int(11) NOT NULL,
  `part_number` varchar(255) NOT NULL,
  `mfr` varchar(255) NOT NULL,
  `u` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `ff` varchar(255) NOT NULL,
  `psu` int(11) NOT NULL,
  `motherboard` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `bin` varchar(255) NOT NULL,
  `node` int(11) NOT NULL,
  `backplane` varchar(255) NOT NULL,
  `drivebays` varchar(255) NOT NULL,
  `rearbays` varchar(255) NOT NULL,
  `con` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chassis_new`
--

INSERT INTO `chassis_new` (`chassis_id`, `part_number`, `mfr`, `u`, `model`, `ff`, `psu`, `motherboard`, `controller`, `bin`, `node`, `backplane`, `drivebays`, `rearbays`, `con`) VALUES
(54, '1U-R620-8BSF-H710-NEW', '', '1U', 'R620', '2.5', 2, 'R620', 'Onboard', 'TD1B', 1, 'SAS2', '8 2.5', ' ', 'NEW'),
(55, '1U-R620-8BSS-H710-NEW', '', '1U', '', '2.5', 2, 'R620', 'Onboard', 'TD1B/listing', 1, 'SAS2', '8 2.5', ' ', 'NEW'),
(56, '1U-R630-10BS-2xE5-2630V3-H730', 'Dell', '1U', 'R630', '2.5', 2, 'R620', 'Onboard', 'TD4 Floor', 1, 'SAS2', '8 2.5', ' ', 'REF'),
(57, '1U-R630-10BS-H730-2PSU', 'Dell', '1U', 'R630', '2.5', 2, 'R630', 'Onboard', 'TD4D/TD5A/TD5B/TD5D', 1, 'SAS', ' 2.5', ' ', 'REF'),
(58, '1U-R630-8BS-2xE5-2620V3-H730-1x 495W-2x900GB-32GB', 'DELL', '1U', 'R630', '2.5', 1, 'R630', 'N/A', 'Tech', 1, 'SAS3', ' 2.5', 'N/A N/A', 'REF'),
(59, '1U-R630-8BS-2xE5-2630V3-H730', 'Dell', '1U', 'R630', '2.5', 2, 'R630', 'N/A', 'TD5D', 1, 'SAS3', ' 2.5', 'N/A N/A', 'REF'),
(60, '1U-R630-8BS-2xE5-2630V3-H730-2x1TB HDD', 'Dell', '1U', 'R630', '2.5', 2, 'R630', 'N/A', 'TD5D', 1, 'SAS3', ' 2.5', 'N/A N/A', 'REF'),
(61, '1U-R630-8BS-2xE5-2680V3-H730', 'Dell', '1U', 'R630', '2.5', 2, 'R630', 'N/A', '', 1, 'SAS3', ' 2.5', 'N/A N/A', 'REF'),
(62, '1U-R630-8BS-H730', 'Dell', '1U', 'R630', '2.5', 2, 'R630', 'Onboard', 'TD2B/TD4D/TD6A', 1, 'SAS3', ' 2.5', '0 N/A', 'REF'),
(63, '1U-R640-10BS-H730-2x1100', '', '1U', '', '2.5', 2, 'R630', 'Onboard', 'TD2C', 1, 'SAS3', ' 2.5', '0 N/A', 'REF'),
(64, '1U-R640-10BS-H730-2x1100-New', 'Dell', '1U', '', '2.5', 2, 'No Motherboard', 'N/A', 'B5-0', 1, 'SAS3', ' 2.5', ' ', 'REF'),
(65, '1U-R6515-10BS-NVMe Only', 'DELL', '1U', 'R6515', '2.5', 2, 'r6515', 'N/A', 'Listing', 1, '', ' 2.5', ' ', ''),
(66, '1U-R6515-10BS-NVMe-24c7402P', 'DELL', '1U', 'R6515', '2.5', 2, 'r6515', 'N/A', 'Listing', 1, '', ' 2.5', ' ', ''),
(67, '1U-R6515-10BS-NVMe-32c7452', 'DELL', '1U', 'R6515', '2.5', 2, 'r6515', 'N/A', 'Listing', 1, '', ' 2.5', ' ', ''),
(68, '1U-R6515-8BS-H330-8c7262', 'DELL', '1U', 'R6515', '2.5', 2, 'r6515', 'N/A', 'Listing', 1, '', ' 2.5', ' ', ''),
(69, '1U-R6515-8BS-NVMe-8c7262', 'DELL', '1U', '', '2.5', 2, 'r6515', 'N/A', 'Listing', 1, '', ' 2.5', ' ', ''),
(70, '1U-RS700-E7', '', '1U', '', '2.5', 2, 'r6515', 'N/A', '', 1, '', ' 2.5', ' ', ''),
(71, '1U-S1600JP-4BL-TQ-2PS', '', '1U', 'Intel S1600JP', '2.5', 2, 'r6515', 'N/A', 'TC4C', 1, '', ' 2.5', ' ', 'REF'),
(72, '1U-S7076-GM2NR-4BL', '', '1U', 'S7076-GM2NR', '3.5', 1, 'S7076-GM2NR', 'N/A', 'SP9 Floor (Rear)', 1, 'TQ', '4 3.5', ' ', 'REF'),
(73, '1U-S7081-4BS-SATA-3xGPUx16-2P-NEW', 'Supermicro', '1U', 'Tyan S7081 3x GPU x16', '2.5', 2, 'S7081', 'N/A', 'Receiving', 1, 'SATA', '4 2.5', ' ', 'REF'),
(74, '1U-S7113-12BL-S3-2P-NEW', 'Supermicro', '1U', 'Tyan S7113  Xeon Scalable', '2.5', 2, 'S7081', 'N/A', 'Receiving', 1, 'SATA', '4 2.5', ' ', 'REF'),
(75, '1U-UCS-C220-M4-4BL-2PS-MRAID12G', 'SUPERMICRO', '1U', 'UCS C220 M4', '3.5', 2, 'UCS C220 M4', 'N/A', 'TCT4 (Floor)', 1, '', '4 3.5', ' ', 'REF'),
(76, '1U-UCS-C220-M4-8BS-2PS-MRAID12G', 'SUPERMICRO', '1U', 'UCS C220 M4', '2.5', 2, 'No Motherboard', 'N/A', 'SP20C', 1, '', '8 2.5', ' ', 'REF'),
(77, '1U-UCS-HX220c-M4S-8BS-2PS-SAS12GHBA', 'SUPERMICRO', '1U', 'Cisco Hyperflex HX220C M4S', '2.5', 2, 'No Motherboard', 'N/A', 'TCT4(Floor)', 1, 'SAS', '8 2.5', ' ', 'REF'),
(78, '1U-UIO-Chassis-8BS-2PS', 'SUPERMICRO', '1U', 'CSE-113TQ-R650UB UIO', '2.5', 2, 'No Motherboard', 'N/A', 'TA3A/TC3C', 1, 'SAS', '8 2.5', ' ', 'REF'),
(79, '1U-X10DDW-I-4BLTQ', 'Supermicro', '1U', 'X10DDW-I', '3.5', 2, 'X10DDW-i', 'N/A', 'TC5C', 1, 'TQ', '4 3.5', 'N/A N/A', 'REF'),
(80, '1U-X10DGQ-2BSTQ-4GPU', '', '1U', 'X10DGQ SYS-1028GQ-TR', '3.5', 2, 'X10DDW-i', 'N/A', 'TB1B', 1, 'TQ', '4 3.5', 'N/A N/A', 'REF'),
(81, '1U-X10DRD-iNT-4BL-TQ-2xNVMe-2x10GBaseT-2P', 'Supermicro', '1U', 'X10DRD-iNT 815TQC-R504CB W NVm', '3.5', 2, 'X10DRD-iNT', 'Onboard', 'TB5C', 1, 'TQ', '4 3.5', 'N/A N/A', 'REF'),
(82, '1U-X10DRD-LT-2BL-TQ-2x10GbaseT-2PS', 'SUPERMICRO', '1U', 'X10DRD-LT CSE-813LT-R500CB', '3.5', 2, 'X10DRD-LT', 'N/A', 'TB3D', 1, 'TQ', '2 3.5', ' ', 'REF'),
(83, '1U-X10DRD-LTP-4BL-TQ-2x10GSFP-2PS', 'SUPERMICRO', '1U', 'X10DRD-LTP SYS-6018R-TDTPR', '3.5', 2, 'X10DRD-LTP', 'N/A', 'TC6B', 1, 'TQ', '4 3.5', 'N/A N/A', 'REF'),
(84, '1U-X10DRH-CLN4-4BLTQ', '', '1U', 'X10DRH-CLN4 CSE-815TQC-R706CB', '3.5', 2, 'X10DRH-CLN4', 'N/A', 'TB3B', 1, 'SAS3', '4 3.5', 'N/A N/A', 'REF'),
(501, 'QT41-24S-4Node-16M', 'Quanta', '2U', 'SYS-S2SZZZ0030,1S2S090082, 1S2', '2.5', 2, 'T41S-2U', 'Onboard', 'SP4-6/Floor TE5C/SEA Area', 4, 'SAS', '24 2.5', '0 N/A', 'USED'),
(502, 'QT42D-24S-4Node-16M', 'Quanta', '2U', 'SYS- 1S5DZZZ0STH, 1S5DZZZ0STV', '2.5', 2, 'T42D-2U', 'N/A', 'SP4-6', 4, 'SAS', '16 2.5', '0 N/A', 'NEW'),
(503, 'QT42S-24S-4Node-16M', 'Quanta', '2U', 'SYS-1S5SZZZ0STJ, 1S5SZZZ0STL,1', '2.5', 2, 'T42S-2U', 'N/A', 'SP4-6', 4, 'SAS', '24 2.5', '0 N/A', 'NEW'),
(504, 'R1208WT2GS', 'INTEL', '1U', '1u Intel Server', '2.5', 2, 'T42S-2U', 'N/A', 'tc2b', 4, 'SAS', '24 2.5', '0 N/A', 'REF'),
(505, 'r1208wt2gsr', 'INTEL', '1U', '1U intel server', '2.5', 2, 'No Motherboard', 'N/A', 'TC2B', 1, 'SAS3', '8 2.5', 'N/A N/A', 'REF'),
(506, 'R2208WT2YS', 'INTEL', '2U', '', 'N/A', 2, 'No Motherboard', 'N/A', 'TC2B', 4, 'SATA', '8 N/A', 'N/A N/A', 'REF'),
(507, 'R2308WTTYSR', 'INTEL', '2U', '2U Intel Server', 'N/A', 2, 'No Motherboard', 'N/A', 'TC2B', 4, 'SATA', '8 N/A', 'N/A N/A', ''),
(508, 'R2312WTTYSR', 'INTEL', '2U', '', '2.5', 2, 'No Motherboard', 'N/A', 'TC2B', 1, 'SAS3', '12 2.5', 'N/A N/A', 'REF'),
(509, 'R610', '', '2U', 'R610', '2.5', 2, 'No Motherboard', 'N/A', '', 1, 'SAS3', '12 2.5', 'N/A N/A', 'REF'),
(510, 'R720-D8L-H710-750', 'Dell', '2U', 'R720', '2.5', 2, 'No Motherboard', 'N/A', 'TD3D', 1, 'SAS3', '12 2.5', 'N/A N/A', 'REF'),
(511, 'RMC3E2-XP', '', '3U', '', '2.5', 2, 'No Motherboard', 'N/A', 'C3-2', 1, 'SAS3', '12 2.5', 'N/A N/A', 'REF'),
(512, 'SA5112M4-D4L-None-800', '', '3U', '', '2.5', 2, 'No Motherboard', 'N/A', 'A3-2', 1, 'SAS3', '12 2.5', 'N/A N/A', 'REF'),
(513, 'SCL04822N-02', 'LANTRONIX', '3U', '', '2.5', 2, 'No Motherboard', 'N/A', 'A1-2', 1, 'SAS3', '12 2.5', 'N/A N/A', 'REF'),
(514, 'SSG-2028R-DE2CR24L', 'Supermicro', '2U', 'X10DRS-2U', '2.5', 2, 'No Motherboard', 'N/A', 'A5-0', 1, 'SAS3', '24 2.5', 'N/A N/A', 'REF'),
(515, 'Sun-Ultra-24', '', '2U', '371-3990 Video, 390-0389 DVD, ', '2.5', 2, 'No Motherboard', 'N/A', 'B7-1', 1, 'SAS3', '24 2.5', 'N/A N/A', 'REF'),
(516, 'Supermicro 1U-X10SLH-N6-ST031 4Bay 3.5in TQ', '', '1U', '', '2.5', 2, 'No Motherboard', 'N/A', '', 1, 'SAS3', '24 2.5', 'N/A N/A', ''),
(517, 'SV300G2-D10S-None-650', 'WIWYNN', '1U', '', '2.5', 2, 'No Motherboard', 'N/A', '', 1, 'SAS3', '24 2.5', 'N/A N/A', 'REF'),
(518, 'SYS-1027R-72RFTP', 'Supermicro', '1U', 'X9DRW-7TPF', '2.5', 2, 'X9DRW-7TPF', 'N/A', 'A3-3', 1, 'SAS', '8 2.5', '0 N/A', 'REF'),
(519, 'sys-5018d-fn4t-32G-120SSD', '', '1U', 'x10SDV-8C-TLN4F', '3.5', 1, 'x10SDV-8C-TLN4F', 'N/A', 'A2-2', 1, '', '2 3.5', ' ', 'REF'),
(520, 'SYS-5018D-MTRF', 'Supermicro', '1U', 'X10SLM-F 4', '3.5', 2, 'X10SLM-F', 'N/A', '', 1, 'SAS2', '4 3.5', '0 N/A', 'REF'),
(521, 'SYS-5037MC-H12TRF', '', '1U', '', '3.5', 2, 'X10SLM-F', 'N/A', '', 1, 'SAS2', '4 3.5', '0 N/A', ''),
(522, 'SYS-6036ST-6LR', 'Supermicro', '1U', 'X8DTS-F', '3.5', 2, 'X8DTS-F', 'N/A', 'A6-0', 1, 'SAS2', '16 3.5', '0 N/A', 'REF'),
(523, 'T21P-4U', '', '4U', 'T21P-4U', '3.5', 2, 'X8DTS-F', 'N/A', 'SP18A', 1, 'SAS2', '16 3.5', '0 N/A', ''),
(524, 'T5-859U-RP+', '', '859U', 'TS-859U-RP+', '3.5', 2, 'X8DTS-F', 'N/A', 'C3-2', 1, 'SAS2', '16 3.5', '0 N/A', 'REF'),
(525, 'TS-1679U-RP', '', '1679U', '', '3.5', 2, 'X8DTS-F', 'N/A', 'A2-3', 1, 'SAS2', '16 3.5', '0 N/A', 'Ref'),
(526, 'TS-EC879U-RP', '', '879U', '', '3.5', 2, 'X8DTS-F', 'N/A', 'A3-1', 1, 'SAS2', '16 3.5', '0 N/A', 'Ref'),
(527, 'UCS-5108-B200-M3-BLADE', '', '879U', '', '3.5', 2, 'X8DTS-F', 'N/A', '', 1, 'SAS2', '16 3.5', '0 N/A', 'ref'),
(528, 'UCS-C2200-M5-128G-1x400G-2x1TB-1x750w', '', '879U', '', '3.5', 2, 'X8DTS-F', 'N/A', ' Build Requiered First / ', 1, 'SAS2', '16 3.5', '0 N/A', 'NEW'),
(529, 'UCSB-B200-M4-2620v3', '', '879U', 'w/ UCSB-MLOM-40G add 2x E5-262', '2.5', 2, 'UCS B200 M4', 'N/A', 'B4-1', 1, 'SAS', ' 2.5', 'N/A N/A', 'REF'),
(530, 'UP-R520-8BLFF-750W-E5-2440-H710-64DDR3-600GBSAS15K-R', 'Dell', '879U', '', '2.5', 2, 'UCS B200 M4', 'N/A', '', 1, 'SAS', ' 2.5', 'N/A N/A', 'REF'),
(531, 'UP-SM-1U-CSE-512-260B chassis', 'Supermicro', '1U', 'UP-SM-1U-CSE-512-260B chassis', '2.5', 2, 'UCS B200 M4', 'N/A', '', 1, 'SAS', ' 2.5', 'N/A N/A', 'REF'),
(532, 'VTrak-E5600f', 'promise', '3U', '', '2.5', 2, 'UCS B200 M4', 'N/A', 'B4-0', 1, 'SAS', ' 2.5', 'N/A N/A', 'ref'),
(533, 'VTrak-E830f', 'promise', '3U', '', '2.5', 2, 'UCS B200 M4', 'N/A', 'B4-0', 1, 'SAS', ' 2.5', 'N/A N/A', 'ref'),
(534, 'VTrak-j5800s', 'promise', '3U', '', '2.5', 2, 'UCS B200 M4', 'N/A', 'B4-0', 1, 'SAS', ' 2.5', 'N/A N/A', 'ref'),
(535, 'X3550 M5 4x LFF', 'Lenovo', '1U', '1U M5 4x 3.5 no rail', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', 'TC2B', 1, 'SATA', '4 3.5', 'N/A N/A', 'REF'),
(536, 'X3550 M5 4x LFF SAS', 'Lenovo', '1U', '1U M5 4x LFF No Rail', '3.5', 2, 'X3550', 'N/A', 'TC2B', 1, 'SAS3', '4 3.5', ' ', 'REF'),
(537, 'X3550 M5 4x LFF SATA', 'Lenovo', '1U', '1U SATA 4X3.5', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', 'TC2B', 1, 'SATA', '4 3.5', 'N/A N/A', 'REF'),
(538, 'X3550 M5 4x SFF', 'Lenovo', '1U', '1U M5 4x SFF w/ Rail', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', 'TC2B', 1, 'SATA', '4 3.5', 'N/A N/A', 'REF'),
(539, 'X3650 M5 SATA', 'Lenovo', '2U', 'x3650 M5 12x LFF', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', 'TC2B', 1, 'SATA', '4 3.5', 'N/A N/A', 'REF'),
(540, 'X7SLM-L', '', '1U', 'X7SLM-L', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', 'A6-1', 1, 'SATA', '4 3.5', 'N/A N/A', ''),
(541, 'X8DTS-F-NODE', '', '1U', 'X8DTS-F', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', 'MB4-6', 1, 'SATA', '4 3.5', 'N/A N/A', 'REF'),
(542, 'Z800', 'HP', '1U', '', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', '', 1, 'SATA', '4 3.5', 'N/A N/A', ''),
(543, 'Z800 E5620 2.40GHz 24GB', '', '1U', 'Z800 E5620 2.40GHz 24GB', '3.5', 2, 'Lenovo 1U DDR4', 'N/A', '', 1, 'SATA', '4 3.5', 'N/A N/A', 'REF');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chassis_new`
--
ALTER TABLE `chassis_new`
  ADD PRIMARY KEY (`chassis_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chassis_new`
--
ALTER TABLE `chassis_new`
  MODIFY `chassis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=544;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
