ALTER TABLE nebula
      ADD COLUMN cost VARCHAR(100) AFTER disposition,
      ADD COLUMN sell VARCHAR(100) AFTER cost;
