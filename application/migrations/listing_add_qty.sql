ALTER TABLE `listing_table`
      ADD COLUMN `qty_3` VARCHAR(255) NULL DEFAULT NULL AFTER `banner_last_edited`,
      ADD COLUMN `qty_3_last_edited` VARCHAR(255) NULL DEFAULT NULL AFTER `qty_3`,
      ADD COLUMN `qty_3_last_edited_by` VARCHAR(255) NULL DEFAULT NULL AFTER `qty_3_last_edited`;
