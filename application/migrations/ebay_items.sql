CREATE TABLE IF NOT EXISTS `linn`.`ebay_items` (
       `id` INT(11) NOT NULL AUTO_INCREMENT,
       `item_number` VARCHAR(255) NOT NULL,
       `seller` VARCHAR(255) NOT NULL,
       `SKU` VARCHAR(255),
       `HitCounter` INT(11),
       `Quantity` INT(11),
       `QuantitySold` INT(11),
       `Timestamp` VARCHAR(255),
       PRIMARY KEY (`id`)
);
