ALTER TABLE `ebay_items`
ADD COLUMN `inventory_qty` INT AFTER `quantity`;

ALTER TABLE `ebay_items_archive`
ADD COLUMN `inventory_qty` INT AFTER `quantity`;
