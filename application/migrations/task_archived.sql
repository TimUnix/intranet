ALTER TABLE `task`
       ADD COLUMN `is_archived` BOOLEAN NOT NULL DEFAULT FALSE AFTER `listing_by`,
       ADD COLUMN `archived_by` VARCHAR(255) AFTER `is_archived`,
       ADD COLUMN `date_archived` TIMESTAMP AFTER `archived_by`;
