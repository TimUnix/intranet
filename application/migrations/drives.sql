CREATE TABLE IF NOT EXISTS `linn`.`drives` (
  `drive_id` int(11) NOT NULL,
  `sku` varchar(256) NOT NULL,
  `bar` varchar(256) DEFAULT NULL,
  `brand` varchar(32) DEFAULT NULL,
  `ff` enum('1.8in','2.5in','3.5in','U.2','M.2','PCIe') DEFAULT NULL,
  `size` int(12) DEFAULT NULL,
  `type` enum('SAS','SATA','SSD') DEFAULT NULL,
  `rpm` varchar(8) DEFAULT NULL,
  `speed` varchar(16) DEFAULT NULL,
  `interface` enum('SSD') DEFAULT NULL,
  `series` varchar(16) DEFAULT NULL,
  `ext_inf` varchar(64) DEFAULT NULL,
  `con` enum('NEW','NP3','NP6','GB') DEFAULT NULL,
   PRIMARY KEY (`drive_id`)
);