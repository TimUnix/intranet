ALTER TABLE `linn`.`listing_table`
      ADD `photo` BOOL DEFAULT 0 AFTER `sku`,
      ADD `photo_last_edited` DATETIME DEFAULT NULL AFTER `photo`,
      ADD `photo_last_edited_by` VARCHAR(100) DEFAULT NULL AFTER `photo_last_edited`,
      ADD `moved` VARCHAR(255) DEFAULT NULL AFTER `channel_last_edited`,
      ADD `moved_last_edited` DATETIME DEFAULT NULL AFTER `moved`,
      ADD `moved_last_edited_by` VARCHAR(100) DEFAULT NULL AFTER `moved_last_edited`,
      ADD `lot` VARCHAR(50) DEFAULT '1' AFTER `price`,
      ADD `lot_last_edited` DATETIME DEFAULT NULL AFTER `lot`,
      ADD `lot_last_edited_by` VARCHAR(100) DEFAULT NULL AFTER `lot_last_edited`,
      MODIFY `weight` FLOAT(11,2) DEFAULT 0;
