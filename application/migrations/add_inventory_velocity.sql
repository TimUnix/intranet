CREATE TABLE inventory_velocity (
	   id INTEGER NOT NULL AUTO_INCREMENT,
	   sku VARCHAR(100),
	   velocity_today INT,
	   velocity_yesterday INT,
	   velocity_difference INT,
	   date_added TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	   PRIMARY KEY (id)
);
