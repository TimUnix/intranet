ALTER TABLE menu
ADD COLUMN icons VARCHAR (100) NULL AFTER menu_order;

CREATE TABLE `linn`.`configuration` (
  `code` VARCHAR (100),
  `value` VARCHAR (100)
);