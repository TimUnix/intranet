ALTER TABLE `ebay_category_specifics_keys`
DROP COLUMN `help_text`,
DROP COLUMN `min_values`,
DROP COLUMN `max_values`,
ADD COLUMN `cardinality` TINYINT DEFAULT 1 AFTER `requirement`,
ADD COLUMN `max_length` INT AFTER `cardinality`,
ADD COLUMN `format` VARCHAR(50) AFTER `max_length`;
