-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2021 at 05:26 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linn`
--

-- --------------------------------------------------------

--
-- Table structure for table `listing_table`
--

CREATE TABLE `listing_table` (
  `listing_id` int(11) NOT NULL,
  `request` varchar(255) NOT NULL,
  `date_requested` datetime NOT NULL,
  `request_by` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `shipping` float NOT NULL,
  `notes` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `draft_id` varchar(255) NOT NULL,
  `draft_by` varchar(255) NOT NULL,
  `draft_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `listing_item_id` varchar(255) NOT NULL,
  `listing_by` varchar(255) NOT NULL,
  `listing_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listing_table`
--

INSERT INTO `listing_table` (`listing_id`, `request`, `date_requested`, `request_by`, `sku`, `channel`, `price`, `shipping`, `notes`, `banner`, `draft_id`, `draft_by`, `draft_date`, `listing_item_id`, `listing_by`, `listing_date`) VALUES
(3, '1', '2021-07-10 04:17:40', 'Cliford Nazareno', 'a', '1', 100, 100, 'text', 'yes', '111', 'Cliford Nazareno', '2021-07-15 04:34:14', '123', 'Cliford Nazareno', '2021-07-15 01:49:08'),
(4, '2', '2021-07-10 05:13:05', 'Cliford Nazareno', 'b', '2', 1, 100, 'text', 'no', '1231', 'Cliford Nazareno', '2021-07-15 04:34:30', '123', 'Cliford Nazareno', '2021-07-13 02:45:22'),
(5, '3', '2021-07-12 21:11:42', 'Cliford Nazareno', 'c', '4', 100, 100, 'text', 'yes', '11111', 'Cliford Nazareno', '2021-07-14 23:37:44', '123', 'Cliford Nazareno', '2021-07-12 21:14:06'),
(6, '1', '2021-07-12 23:03:34', 'Cliford Nazareno', 'd', '1', 10, 100, 'texts', 'no', '0', 'Cliford Nazareno', '2021-07-15 04:43:41', '1', 'Cliford Nazareno', '2021-07-15 04:43:44'),
(7, '1', '2021-07-15 01:39:21', 'Cliford Nazareno', 'e', '1', 10, 0, '', '', '0', 'Cliford Nazareno', '2021-07-15 01:40:40', '0', 'Cliford Nazareno', '2021-07-15 01:40:56'),
(8, '1', '2021-07-15 01:43:40', 'Cliford Nazareno', 'f', '4', 10, 0, '', '', '1', 'Cliford Nazareno', '2021-07-15 02:08:56', '0', 'Cliford Nazareno', '2021-07-15 02:08:47'),
(75, '1', '2021-07-15 05:20:39', 'Cliford Nazareno', 'PDB-PT825-36824 Rev. 1.0', '1', 0, 0, 'sdsdsad', 'YES', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00'),
(76, '1', '2021-07-15 05:20:56', 'Cliford Nazareno', 'PDB-PT825-36824 Rev. 1.01', '1', 0, 0, 'AxxZxzXZX', 'YES', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00'),
(77, '1', '2021-07-15 05:21:51', 'Cliford Nazareno', 'PDB-PT825-36824 Rev. 1.0111', '1', 0, 0, '', 'NO', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00'),
(78, '1', '2021-07-15 05:22:51', 'Cliford Nazareno', 'QWERT', '1', 0, 0, '', 'NO', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `listing_table`
--
ALTER TABLE `listing_table`
  ADD PRIMARY KEY (`listing_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `listing_table`
--
ALTER TABLE `listing_table`
  MODIFY `listing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
=7;
NNECTION=@OLD_COLLATION_CONNECTION */;
