CREATE TABLE IF NOT EXISTS `listing` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `sku` VARCHAR(100),
       `description` TEXT,
       `category` VARCHAR(50),
       `quantity` INT,
       `price` DECIMAL,
       `price_research_market` VARCHAR(100),
       `note` VARCHAR(255),
       `link` VARCHAR(255),
       `assignee` VARCHAR(100),
       `is_done` BOOLEAN NOT NULL DEFAULT false,
       `date_created` TIMESTAMP,
       PRIMARY KEY (`id`)
);

INSERT INTO `menu` (`menu_title`, `menu_file`, `menu_intranet`, `menu_parent`, `menu_order`, `icons`)
VALUES ('Listing', 'listing.php', 'listing', LAST_INSERT_ID(), LAST_INSERT_ID(), 'fa fa-list');

ALTER TABLE `listing`
      ADD COLUMN `done_date` TIMESTAMP NULL DEFAULT NULL AFTER `is_done`;

ALTER TABLE `listing`
      CHANGE COLUMN `assignee` `channel` VARCHAR(100),
      CHANGE COLUMN `price_research_market` `price_research_market_low` VARCHAR(100),
      ADD COLUMN `price_research_market_high` VARCHAR(100) AFTER `price_research_market_low`,
      ADD COLUMN `marked_done_by` VARCHAR(100) AFTER `done_date`,
      ADD COLUMN `link_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `link`,
      ADD COLUMN `link_last_edited_by` VARCHAR(100) AFTER `link_last_edited`,
      ADD COLUMN `created_by` VARCHAR(100) AFTER `marked_done_by`;

