ALTER TABLE `listing_table`
      CHANGE COLUMN `price_volume` `volume_discount_2x` VARCHAR(100),
      CHANGE COLUMN `price_volume_last_edited` `volume_discount_2x_last_edited` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      CHANGE COLUMN `price_volume_last_edited_by` `volume_discount_2x_last_edited_by` VARCHAR(100),
      ADD COLUMN `volume_discount_3x` VARCHAR(100) AFTER `volume_discount_2x_last_edited_by`,
      ADD COLUMN `volume_discount_3x_last_edited` TIMESTAMP DEFAULT CURRENT_TIMESTAMP AFTER `volume_discount_3x`,
      ADD COLUMN `volume_discount_3x_last_edited_by` VARCHAR(100) AFTER `volume_discount_3x_last_edited`,
      ADD COLUMN `volume_discount_4x` VARCHAR(100) AFTER `volume_discount_3x_last_edited_by`,
      ADD COLUMN `volume_discount_4x_last_edited` TIMESTAMP DEFAULT CURRENT_TIMESTAMP AFTER `volume_discount_4x`,
      ADD COLUMN `volume_discount_4x_last_edited_by` VARCHAR(100) AFTER `volume_discount_4x_last_edited`;
