ALTER TABLE `linn`.`ebay_items`
      ADD `link` VARCHAR(255) NULL DEFAULT NULL AFTER `title`,
      ADD `img` VARCHAR(255) NULL DEFAULT NULL AFTER `link`;
