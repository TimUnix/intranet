CREATE TABLE `ebay_conditions` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `category_id` INT NOT NULL,
       `enabled` VARCHAR(50) NOT NULL,
       `condition_id` INT,
       `display_name` VARCHAR(100),
       PRIMARY KEY(id)
);

ALTER TABLE `ebay_conditions`
CHANGE COLUMN `enabled` `required` TINYINT NOT NULL DEFAULT 1;

ALTER TABLE `ebay_conditions`
ADD COLUMN `ebay_enum` VARCHAR(50) AFTER `condition_id`;
