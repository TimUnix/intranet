CREATE TABLE IF NOT EXISTS `ebay_categories` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `ebay_id` INT,
       `name` VARCHAR(100),
       PRIMARY KEY(`id`)
);

INSERT INTO `ebay_categories` (`name`)
SELECT DISTINCT `cat`
FROM `inventory`;

DELETE FROM `ebay_categories`
WHERE `name` = 'cat';

UPDATE `ebay_categories`
SET `ebay_id` = 175701
WHERE `name` = 'Terminal Server';

UPDATE `ebay_categories`
SET `ebay_id` = 26261
WHERE `name` = 'Industrial';

UPDATE `ebay_categories`
SET `ebay_id` = 175669
WHERE `name` = 'SSD';

UPDATE `ebay_categories`
SET `ebay_id` = 42017
WHERE `name` = 'Power Supply';

UPDATE `ebay_categories`
SET `ebay_id` = 1244
WHERE `name` = 'Motherboard';

UPDATE `ebay_categories`
SET `ebay_id` = 183548
WHERE `name` = 'Rails';

UPDATE `ebay_categories`
SET `ebay_id` = 170083
WHERE `name` = 'Memory';

UPDATE `ebay_categories`
SET `ebay_id` = 27386
WHERE `name` = 'GPU';

UPDATE `ebay_categories`
SET `ebay_id` = 11211
WHERE `name` = 'Server';

UPDATE `ebay_categories`
SET `ebay_id` = 74941
WHERE `name` = 'Drive Cable';

UPDATE `ebay_categories`
SET `ebay_id` = 51268
WHERE `name` = 'Network Switch';

UPDATE `ebay_categories`
SET `ebay_id` = 164
WHERE `name` = 'CPU';

UPDATE `ebay_categories`
SET `ebay_id` = 182097
WHERE `name` = 'Power Cable';

UPDATE `ebay_categories`
SET `ebay_id` = 131486
WHERE `name` = 'Fans';

UPDATE `ebay_categories`
SET `ebay_id` = 80053
WHERE `name` = 'Monitors';

UPDATE `ebay_categories`
SET `ebay_id` = 48479
WHERE `name` = 'UPS';

UPDATE `ebay_categories`
SET `ebay_id` = 67779
WHERE `name` = 'Power Strip';
