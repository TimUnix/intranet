ALTER TABLE `linn`.`ebay_store`
  ADD COLUMN `category_id` INT NULL AFTER `seller`,
  ADD COLUMN `category_name` VARCHAR (255) NULL AFTER `category_id`;