-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2021 at 08:16 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linn`
--

-- --------------------------------------------------------

--
-- Table structure for table `work_orders`
--

CREATE TABLE `work_orders` (
  `chassis` varchar(255) DEFAULT NULL,
  `qty_chassis` int(11) DEFAULT NULL,
  `front_caddies` varchar(255) NOT NULL,
  `qty_front_caddies` int(11) DEFAULT NULL,
  `rear_caddies` varchar(255) DEFAULT NULL,
  `qty_rear_caddies` int(11) DEFAULT NULL,
  `item_harddrive_one` varchar(255) DEFAULT NULL,
  `item_harddrive_two` varchar(255) DEFAULT NULL,
  `item_harddrive_three` varchar(255) DEFAULT NULL,
  `qty_hard_drive_one` int(11) DEFAULT NULL,
  `qty_hard_drive_two` int(11) DEFAULT NULL,
  `qty_hard_drive_three` int(11) DEFAULT NULL,
  `motherboard` varchar(255) DEFAULT NULL,
  `qty_motherboard` int(11) DEFAULT NULL,
  `cpu` varchar(255) DEFAULT NULL,
  `qty_cpu` int(11) DEFAULT NULL,
  `memory` varchar(255) DEFAULT NULL,
  `qty_memory` int(11) DEFAULT NULL,
  `cntrl` varchar(255) DEFAULT NULL,
  `qty_cntrlr` int(11) DEFAULT NULL,
  `psu` varchar(255) DEFAULT NULL,
  `qty_psu` int(255) DEFAULT NULL,
  `mounting` varchar(255) DEFAULT NULL,
  `qty_mounting` int(11) DEFAULT NULL,
  `warranty` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `work_order_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `ship_date` date DEFAULT NULL,
  `ship_method` varchar(255) DEFAULT NULL,
  `sales_rep` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `contact_no` bigint(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_orders`
--

INSERT INTO `work_orders` (`chassis`, `qty_chassis`, `front_caddies`, `qty_front_caddies`, `rear_caddies`, `qty_rear_caddies`, `item_harddrive_one`, `item_harddrive_two`, `item_harddrive_three`, `qty_hard_drive_one`, `qty_hard_drive_two`, `qty_hard_drive_three`, `motherboard`, `qty_motherboard`, `cpu`, `qty_cpu`, `memory`, `qty_memory`, `cntrl`, `qty_cntrlr`, `psu`, `qty_psu`, `mounting`, `qty_mounting`, `warranty`, `os`, `work_order_id`, `created_date`, `ship_date`, `ship_method`, `sales_rep`, `client_name`, `contact_no`, `email`, `address`, `barcode`) VALUES
('1U-R620-8BSF-H710-NEW', 0, 'Select Front Caddies', 1, 'Select Rear Caddies', 1, '0KV02', '01KWKJ', '0KV02', 1, 1, 1, '1.01-E5-2620', 1, '1.01-E5-2620', 1, '00YG732211', 1, 'cntrolr', 1, 'psu', 1, '1U Rail Kit', 1, 'warranty', 'os', 28, '2021-09-30 13:57:05', '2021-09-01', 'ship method', 'sales rep', 'client name', 0, 'email', 'address', '1'),
('1U-R620-8BSF-H710-NEW', 2, '2.5 Filler', 1, '3.5 Filler', 1, '00Y8879', '00Y8879', '0b26022', 1, 1, 1, '1.01-E5-2620', 1, '1.01-E5-2620', 1, '00YG732211', 1, 'cntrolr', 1, 'psu', 1, '1U Rail Kit', 1, 'warranty', 'os', 29, '2021-09-30 13:59:24', '2021-09-01', 'ship method', 'sales rep', 'client name', 0, 'email', 'address', '2'),
('1U-R630-10BS-2xE5-2630V3-H730', 3, '2.5 Caddy', 1, 'Select Rear Caddies', 1, '01KWKJ', '01KWKJ', '0KV02', 1, 1, 1, '1.01-E5-2620', 1, '1.01-E5-2620', 1, '00YG732211', 1, 'cntrolr', 1, 'psu', 1, 'Dell Rails A6, A7 or A16', 1, 'warranty', 'os', 30, '2021-09-30 14:02:37', '2021-09-01', 'ship method', 'sales rep', 'client name', 0, 'email', 'address', '3'),
('1U-R620-8BSF-H710-NEW', 1, '2.5 Filler', 1, '3.5 Filler', 1, '0', '0', NULL, 0, 0, NULL, '1.01-E5-2620', 0, '1.01-E5-2620', 0, '00YG732211', 0, '', 0, '', 0, '1U Rail Kit', 0, '', '', 31, '2021-09-30 14:21:46', '2021-09-01', 'ship method', 'sales rep', 'client name', 0, 'email', 'address', '4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `work_orders`
--
ALTER TABLE `work_orders`
  ADD PRIMARY KEY (`work_order_id`,`front_caddies`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `work_orders`
--
ALTER TABLE `work_orders`
  MODIFY `work_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
