-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2021 at 08:35 PM
-- Server version: 5.7.35
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intranet`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `work_order_id` int(11) NOT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `ship_date` date DEFAULT NULL,
  `ship_method` varchar(255) DEFAULT NULL,
  `sales_rep` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `contact_no` bigint(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `chassis` varchar(255) DEFAULT NULL,
  `qty_chassis` int(11) DEFAULT NULL,
  `backplane` varchar(255) DEFAULT NULL,
  `nodes` varchar(255) DEFAULT NULL,
  `rearbays` varchar(255) DEFAULT NULL,
  `drivebays` varchar(255) DEFAULT NULL,
  `psu_slots` varchar(255) DEFAULT NULL,
  `bin` varchar(255) DEFAULT NULL,
  `front_caddies` varchar(255) DEFAULT NULL,
  `qty_front_caddies` int(11) DEFAULT NULL,
  `rear_caddies` varchar(255) DEFAULT NULL,
  `qty_rear_caddies` int(11) DEFAULT NULL,
  `item_harddrive_one` varchar(255) DEFAULT NULL,
  `item_harddrive_two` varchar(255) DEFAULT NULL,
  `item_harddrive_three` varchar(255) DEFAULT NULL,
  `qty_hard_drive_one` int(11) DEFAULT NULL,
  `qty_hard_drive_two` int(11) DEFAULT NULL,
  `qty_hard_drive_three` int(11) DEFAULT NULL,
  `motherboard` varchar(255) DEFAULT NULL,
  `qty_motherboard` int(11) DEFAULT NULL,
  `cpu` varchar(255) DEFAULT NULL,
  `qty_cpu` int(11) DEFAULT NULL,
  `memory` varchar(255) DEFAULT NULL,
  `qty_memory` int(11) DEFAULT NULL,
  `cntrl` varchar(255) DEFAULT NULL,
  `qty_cntrlr` int(11) DEFAULT NULL,
  `controllertwo` varchar(255) DEFAULT NULL,
  `controllerthree` varchar(255) DEFAULT NULL,
  `qty_controllertwo` int(11) DEFAULT NULL,
  `qty_controllerthree` int(11) DEFAULT NULL,
  `psu` varchar(255) DEFAULT NULL,
  `qty_psu` int(255) DEFAULT NULL,
  `mounting` varchar(255) DEFAULT NULL,
  `qty_mounting` int(11) DEFAULT NULL,
  `warranty` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`work_order_id`, `barcode`, `created_date`, `ship_date`, `ship_method`, `sales_rep`, `client_name`, `contact_no`, `email`, `address`, `chassis`, `qty_chassis`, `backplane`, `nodes`, `rearbays`, `drivebays`, `psu_slots`, `bin`, `front_caddies`, `qty_front_caddies`, `rear_caddies`, `qty_rear_caddies`, `item_harddrive_one`, `item_harddrive_two`, `item_harddrive_three`, `qty_hard_drive_one`, `qty_hard_drive_two`, `qty_hard_drive_three`, `motherboard`, `qty_motherboard`, `cpu`, `qty_cpu`, `memory`, `qty_memory`, `cntrl`, `qty_cntrlr`, `controllertwo`, `controllerthree`, `qty_controllertwo`, `qty_controllerthree`, `psu`, `qty_psu`, `mounting`, `qty_mounting`, `warranty`, `os`) VALUES
(8, 'dfdgdh', '2021-10-08 15:17:33', '2021-10-29', 'Standard Shipping', 'Eliecer Enerio', 'Taylor Swift', 944, 'jhonnettemaes@gmail.com', '267 Tubod', '1U-R630-8BS-2xE5-2680V3-H730', 1, 'SAS3', '1', 'N/A N/A', ' 2.5', '2', '', 'null', 0, 'null', 0, 'null', 'null', 'null', 0, 0, 0, 'R630', 1, '', 0, '15-11767-01', 1, '', 0, '', '', 0, 0, '', 0, 'Generic Rail Kit', 1, 'Standard', ''),
(9, 'fgfg', '2021-10-08 15:18:22', '2021-10-25', 'Standard Shipping', 'Eliecer Enerio', 'Taylor Swift', 944, 'jhonnettemaes@gmail.com', '267 Tubod', '1U-R630-8BS-2xE5-2630V3-H730-2x1TB HDD', 1, 'SAS3', '1', 'N/A N/A', ' 2.5', '2', 'TD5D', 'null', 0, 'null', 0, 'null', 'null', 'null', 0, 0, 0, 'R630', 1, '', 0, '15-11767-01', 1, '', 0, '', '', 0, 0, '', 0, 'Generic Rail Kit', 1, 'Standard', ''),
(18, 'dfdgdh', '2021-10-08 15:50:33', '2021-10-19', 'Standard Shipping', 'Eliecer Enerio', 'Taylor Swift', 944, 'jhonnettemaes@gmail.com', '267 Tubod', '1U-R630-10BS-H730-2PSU', 1, 'SAS', '1', ' ', ' 2.5', '2', 'TD4D/TD5A/TD5B/TD5D', 'null', 0, 'null', 0, 'null', 'null', 'null', 0, 0, 0, 'R630', 1, '', 0, '15-11767-01', 1, '', 0, '', '', 0, 0, '', 0, 'Generic Rail Kit', 1, 'Standard', ''),
(19, 'dfdgdh', '2021-10-08 15:54:45', '2021-10-20', 'Standard Shipping', 'Eliecer Enerio', 'Taylor Swift', 944, 'jhonnettemaes@gmail.com', '267 Tubod', '1U-R640-10BS-H730-2x1100', 1, 'SAS3', '1', '0 N/A', ' 2.5', '2', 'TD2C', 'null', 0, 'null', 0, 'null', 'null', 'null', 0, 0, 0, 'R630', 1, '', 0, '15-11767-01', 1, '', 0, '', '', 0, 0, '', 0, 'Generic Rail Kit', 1, 'Standard', ''),
(20, 'dfdgdh', '2021-10-08 15:55:45', '2021-10-27', 'Standard Shipping', 'Eliecer Enerio', 'Taylor Swift', 4545, 'jhonnettemaes@gmail.com', '267 Tubod', '1U-R630-8BS-2xE5-2620V3-H730-1x 495W-2x900GB-32GB', 1, 'SAS3', '1', 'N/A N/A', ' 2.5', '1', 'Tech', 'null', 0, 'null', 0, 'null', 'null', 'null', 0, 0, 0, 'R630', 1, '', 0, '15-11767-01', 1, '', 0, '', '', 0, 0, '', 0, 'Generic Rail Kit', 1, 'Standard', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`work_order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `work_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
