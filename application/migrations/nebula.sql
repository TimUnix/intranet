CREATE TABLE IF NOT EXISTS nebula (
       id INT NOT NULL AUTO_INCREMENT,
       filename VARCHAR(100),
       created TIMESTAMP NULL DEFAULT NULL,
       title VARCHAR(100),
       erase_method VARCHAR(50),
       process_integrity VARCHAR(100),
       fingerprint VARCHAR(100),
       device_title VARCHAR(50),
       device_serial_number VARCHAR(100),
       device_platform_name VARCHAR(50),
       device_product_name VARCHAR(50),
       device_type VARCHAR(50),
       device_revision VARCHAR(50),
       device_size VARCHAR(50),
       smart_firmware_version VARCHAR(50),
       smart_capacity VARCHAR(50),
       smart_ata_version INT,
       smart_ata_standard VARCHAR(100),
       smart_support INT,
       smart_offline_data_collection_status INT,
       smart_self_test_execution_status INT,
       smart_time_offline_data_collection_sec INT,
       smart_offline_data_collection_capabilities INT,
       smart_capabilities INT,
       smart_error_logging_capabilities INT,
       smart_short_self_test_time_min INT,
       smart_extended_self_test_time_min INT,
       geometry_partitioning VARCHAR(50),
       geometry_total_sec BIGINT,
       geometry_first_sec INT,
       geometry_bps INT,
       results_started TIMESTAMP NULL DEFAULT NULL,
       results_elapsed VARCHAR(50),
       results_errors VARCHAR(255),
       result VARCHAR(50),
       process_name VARCHAR(50),
       process_started_at TIMESTAMP NULL DEFAULT NULL,
       process_elapsed VARCHAR(50),
       process_result VARCHAR(50),
       conclusion VARCHAR(100),
       PRIMARY KEY(id)
);
