ALTER TABLE `ebay_items`
      ADD COLUMN `total_sold_7_days` INT AFTER `quantity_sold`,
      ADD COLUMN `total_hits_7_days` INT AFTER `hit_count`;
            
ALTER TABLE `ebay_items_archive`
      ADD COLUMN `total_sold_7_days` INT AFTER `quantity_sold`,
      ADD COLUMN `total_hits_7_days` INT AFTER `hit_count`;
