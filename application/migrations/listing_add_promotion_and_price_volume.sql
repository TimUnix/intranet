ALTER TABLE `listing_table`
      ADD COLUMN `promotion` VARCHAR(255) AFTER `price`,
      ADD COLUMN `promotion_last_edited` TIMESTAMP AFTER `promotion`,
      ADD COLUMN `promotion_last_edited_by` VARCHAR(100) AFTER `promotion_last_edited`,
      ADD COLUMN `price_volume` VARCHAR(255) AFTER `promotion_last_edited_by`,
      ADD COLUMN `price_volume_last_edited` TIMESTAMP AFTER `price_volume`,
      ADD COLUMN `price_volume_last_edited_by` VARCHAR(100) AFTER `price_volume_last_edited`;
      
