CREATE TABLE `linn`.`ebay_store` (
  `id` INT (11) NOT NULL AUTO_INCREMENT,
  `item_number` VARCHAR (255) NOT NULL,
  `price` FLOAT (7, 2) NOT NULL,
  `link` LONGTEXT NOT NULL,
  `img` LONGTEXT,
  `seller` LONGTEXT,
  PRIMARY KEY (`id`)
);