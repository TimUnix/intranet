CREATE TABLE ebay_ad_draft_fields (
	   id INT NOT NULL AUTO_INCREMENT,
	   ebay_ad_draft_id INT NOT NULL,
	   field VARCHAR(100),
	   html_id VARCHAR(100),
	   value VARCHAR(100),
	   PRIMARY KEY (id),
	   FOREIGN KEY (ebay_ad_draft_id) REFERENCES ebay_ad_draft (id)
);
