-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2021 at 06:31 PM
-- Server version: 5.7.35
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intranet`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ebay_competitors_columns` text COLLATE utf8mb4_unicode_ci,
  `ebay_items_columns` text COLLATE utf8mb4_unicode_ci,
  `listing_columns` text COLLATE utf8mb4_unicode_ci,
  `drives_columns` text COLLATE utf8mb4_unicode_ci,
  `nebula_columns` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `picture`, `password`, `access_token`, `google_id`, `last_url`, `ebay_competitors_columns`, `ebay_items_columns`, `listing_columns`, `drives_columns`, `nebula_columns`, `created_at`, `updated_at`) VALUES
(107, 'Tim', 'Huynh', 'thuynh@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GjWMkp1oKHu0YBaDA0Ab-130DkqcEHhyiVjJITT=s96-c', NULL, 'ya29.A0AfH6SMCx7v_cyuR-6kSIknhuFCn2CbDeY3ktab0XZ8jUF7B9-JPRETEibDUAcaC_oQ3T2XKR7cj-tx7LoJ31XSXZ7AFW_CXF9C0WuEKMQgwUklCnqk3BGEqJnaFQn0EGIlVuiH3JK1U5f7B2zc7xPensnO5k-uYVmQBTM1PWrfhW', '118225130982864770641', '/access.php', NULL, NULL, NULL, NULL, NULL, '2020-10-31 10:47:18', '2021-05-18 07:17:07'),
(108, 'Hector', 'Ramirez', 'hramirez@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GjD9asI59WzRoB_b_GINGGOvI32_nHZGmi9MIF_=s96-c', NULL, 'ya29.a0AfH6SMBSRtWMX_814QJn1PRYwjKsYtjvS-7PmOokmNqt4FlCx9pWpKNKVmvQHZm1dXxUHmHp9uMpuxOy-JL6yzFwYSbjA1CFrVSpnwv8Tl29jEycq8mczT-DYWiy5ltgyeYsccWYmS_LJxlUBj8YjZ2TKnvdS1iaz6cGo0Me-pI', '115001643891396659387', '/inventory.php', NULL, NULL, NULL, NULL, NULL, '2021-01-21 11:40:01', '2021-02-24 14:21:54'),
(109, 'Basil', 'Ryen', 'bryen@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14Ghs_XScnqF5pYHO9e2C-bGdBUy06obaKMFiU6aTMA=s96-c', NULL, 'ya29.a0AfH6SMCfHY3jPvBmDKIZo6BdeRSp6LjJy7MBcFgpcsMykjLpmd7Iuc2v7mFvs_lvyHruuxQYiPyhrdbgaUAVSfMtycfWJR4xEapr3xbNgNeh2kV6ZR1Jr8sCLUvFiGHkyt6uN9B1PgArtypaMQRaWjgwWqZsjZaCbTAb-4JyKXY', '108290099285936153337', '/index.php', NULL, NULL, NULL, NULL, NULL, '2021-01-27 14:21:53', '2021-01-27 14:21:53'),
(110, 'Justin', 'Truong', 'jtruong@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GiXVYe0u_Kp7K6g8zKDMAOen9K1m_AgyL1VZKrKVw=s96-c', NULL, 'ya29.A0AfH6SMDxQTmay4NJkV3nj-MkKtTxoAKU7jpJhGdS4Jw-wmKUmUUDa4b2MPKx5x2wbWZp4tM_V4Hjlo2cWAm0JeCN18SKwxNQx9PGkiscwoYQtLtuh_qllEHzCOQ9H5vhsRM99nbp0yzyb3lH7EDc6k7L1hxb', '107939031352767165586', '/price_update.php', NULL, NULL, NULL, NULL, NULL, '2021-01-27 14:22:16', '2021-03-02 12:22:48'),
(111, 'Nick', 'Gray', 'ngray@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14Gj6LJX54vtO8YHHijAnLfJs1mCr256xf4tL048Qhw=s96-c', NULL, 'ya29.a0AfH6SMBJg37Pj8KmNH2q6eNleLtDnApS4GyyDonGvwUSN6HxRICVauzf6rA-8CIuw0EjE6YuH3zqfvZhZ7iVE8_wPqzz5JdVM67GU7JRh8BXdQhz3QZGn32gJOHZBlebtbkO1g6rrXsIuw8BbFraIRMQsT5nvPsvCuWRhV9QKfY', '107300733604222068442', '/inventory.php', NULL, NULL, NULL, NULL, NULL, '2021-01-27 14:26:37', '2021-02-10 14:27:53'),
(112, 'Tiffany', 'Ramirez', 'tramirez@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GiXuSuOjD1-qmTM9dk-w3y7kPCG4LdvswrbRzH_2A=s96-c', NULL, 'ya29.a0AfH6SMAelsKsOHywVdlz7HIFLDLXZ8octaltuwGO1Q9PTilhYKPYYyR-YAAFHnpND_1WUR9uItkk2fst8iWYva84xL4eRJBZn1MYSScD_mNGExlp2sbO7a-wcM7tMA5SLnjrq-pn9jxFs1JrIo62d3Rylrsqw9QIMoh1iG2URRc', '110649986710837180455', '/price_update.php', NULL, NULL, NULL, NULL, NULL, '2021-01-27 14:28:48', '2021-02-02 15:31:07'),
(113, 'Jason', 'Nguyen', 'jnguyen@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GhRM5HFFHTibkfL9FTZYTI1494T_fdnxV3ny6Up=s96-c', NULL, 'ya29.a0AfH6SMBOaBpv802Ko-L_JZPxvA8dXSENYTKNwy8N89S3UmtEd5hm-ZacZYYJAIyJyIPG4xv6zLl5z9_ngorX4e7Oskp4Y-HydgJJ6YAhbvF4SGC3_5xhweKf6kn9Pqo7Uq0JCbx3XQD351rFm8_-uQXKiTMIBa3Jybg62GV6Was', '113630310736679655109', '/research.php?search=9211-4i', NULL, NULL, NULL, NULL, NULL, '2021-01-27 14:34:57', '2021-04-16 12:01:32'),
(114, 'Susan', 'Gonzales', 'drives@unixsurplus.com', 'https://lh4.googleusercontent.com/-xIwwr_wK-j0/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucmKWJWz5JfVaWUf86A-J0sPfGR1kg/s96-c/photo.jpg', NULL, 'ya29.a0AfH6SMBtNgyH_TaqNQVQsyj_Niby56gd-uNAENVTTmxDf7pIySBiqmnELrYPDmk3EVtseC9s9CrwPhnd68a14MW0rn-7RzGl9dyr1-sgWViL3pVstAo_gB_v387nAJszJfq-ZKEsiMOd9x-dgU2l0mfj5GWDZQUZWnsg13URBzI', '114691699621517656691', '/inventory.php', NULL, NULL, NULL, NULL, NULL, '2021-02-09 14:56:46', '2021-04-11 07:53:31'),
(115, 'Tay', 'Luong', 'tluong@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GihDf-7fibPVQiV4iaF1hbtqmPpM6T9BYJIOsXs=s96-c', NULL, 'ya29.A0AfH6SMDmIl0sW-2wkC5VbYObSBtnqj-QbpG7Xpz9hXB58C3DUNKadK_C6P04wHkG4m3UVWKdFex90RJWQCfMrcNtfDP-w7W7mBYdwJca7x_2nE50LIYjN1-zl8IqU0R_vPxnRYE78wtvqoBouoblIkseQwKz', '109254797482635646653', '/price_update.php', NULL, NULL, NULL, NULL, NULL, '2021-02-10 14:26:38', '2021-02-10 14:26:55'),
(116, 'Jasmine', 'Escamilla', 'jescamilla@unixsurplus.com', 'https://lh3.googleusercontent.com/-SXHdx_dz32g/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucmYfNmodx14Tx8k7hyhHaxtm6l_Tw/s96-c/photo.jpg', NULL, 'ya29.A0AfH6SMAxidj-PyiML0LiKbtyF9JWtyywVo7wU666x1upJZ3L-nOi6y1RpmP6_9VzS5obbxe_nSjXG-6Qnk83FlA2oln32s9uqljbfaRXNFdMT638BXeFSTvKu5jnIahOb5XoeFO-OiRkdX9gyC51asfqdhCd', '106174641577369789912', '/inventory.php', NULL, NULL, NULL, NULL, NULL, '2021-02-10 15:14:30', '2021-02-25 10:54:04'),
(117, 'John', 'Bodo', 'jbodo@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14Gjuv8Bz_7dXyJTrTpASddfTiOFifMnD8oYnbH5W=s96-c', NULL, 'ya29.a0AfH6SMCkQYhqDVCb9griEFToKMMONgf-nHyg4-7G6R6adXdTe4jGm6XtBHZs9fIwNRxjY4Ogn_ZSw7X7cMEN6ZDWXJwvn6iBPTJev4xD5-yH-Rfs34-oBO6No5e8Gd_bnc-RpCS7G6F5tJpUlX1-NOAB_tp1cu1d3lWhdSNBVdw', '117891546749555377966', '/reports.php', NULL, NULL, NULL, NULL, NULL, '2021-02-10 15:29:37', '2021-04-07 14:57:04'),
(118, 'Katherine', 'Silorio', 'ksilorio@unixsurplus.com', 'https://lh3.googleusercontent.com/-pj1W21TxFqQ/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucldKh5q18jQ7w1hok9wqyhAmMloDg/s96-c/photo.jpg', NULL, 'ya29.a0AfH6SMAQW7-FSw7Ro4G-ZNa9OH0dBBFHYKb8L2PMKbFsvoWSjZuJouF0JB8kBOtjXWYXQ3dVwaPgacfw11BScNrPVPTfW3k1qNl1NOGuRaCMJXwU5XtE_76ZUhiuBz_tWgyuQha8JrwoxlFG4iPX497ipU3Q', '107590062690306031587', '/research.php?search=9266-8i+High+Profile', NULL, NULL, NULL, NULL, NULL, '2021-04-09 17:58:50', '2021-05-11 09:30:20'),
(120, 'test', 'test', 'test@gmail.com', 'NA', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 'Kent', 'Delante', 'kdelante@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GgVv60r4cIxWlhV9ibPZvW_YEYpKWOkKvIu6XiL=s96-c', NULL, 'ya29.a0ARrdaM8nZiX2qvGnZlYnh6uw7689PpwUetRhducVSYAvgacacM5u4yEEFYfsej3B0_IiMXWnYTYLU6v4wqU-3YtKRDErIfdslGrAPiC-wqaNApvsPDv2Nsn4c8CwJSVgrxmbmtvByQ0QgMETJtaKDyk_Gu2FBg', '108411223805621070804', 'NULL', '{{\"item_number\":{\"is_shown\":true},\"image\":{\"is_shown\":true},\"title\":{\"is_shown\":true},\"seller\":{\"is_shown\":true},\"price\":{\"is_shown\":true},\"category\":{\"is_shown\":true},\"condition\":{\"is_shown\":false},\"shipping_type\":{\"is_shown\":false},\"ship_to_locations\":{\"is_shown\":false},\"expedited\":{\"is_shown\":false},\"one_day\":{\"is_shown\":false},\"top\":{\"is_shown\":false},\"handling_time\":{\"is_shown\":false},\"selling_state\":{\"is_shown\":false}}', NULL, NULL, NULL, NULL, '2021-06-29 21:21:59', '2021-06-29 21:21:59'),
(122, 'Cliford', 'Nazareno', 'cnazareno@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GjrfHHOgJLNy2Bg-S6ZBDoK5MlxyEirSsW2SUNI=s96-c', NULL, 'ya29.a0ARrdaM_tgq6ggUCxW4BgpJb8j9hWnZ5WPBqfWjTibHngH1TRCT-LvGuTlmkFgD5bg5FDrSg4FidffslcWK0uIar-Q62GGLpXC82Qh-WvJI76stp20buQkIJxuZR4yZ2-uDWrjFXJqbLTjice07Uz0IiIOdFYPA', '105530021653545483658', 'NULL', NULL, NULL, NULL, NULL, NULL, '2021-06-29 23:18:06', '2021-06-29 23:18:06'),
(123, 'Jhonnette Mae Sabenicio', 'Sabenicio', 'jsabenicio@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GjB5UGFUX7WWZyEe6SVMPTECP-MT_fWdtD6Lk2x=s96-c', NULL, 'ya29.a0ARrdaM-dHXUybMyJSwLgVCPmD9ys5A46vXoCtxDLmOFepAOSv8_4cTErYFOZCr0E3tdPQOlCi0tNeHTjBuIQH0WW7rp08TqsqZm-mGrwVVN4qF5YxrzmFxs8HBcwqRY2k3d3mUFIiYsi6rAUvhFxdX1joQIo', '113081459854265903830', 'NULL', NULL, NULL, NULL, NULL, NULL, '2021-07-08 17:31:02', '2021-07-08 17:31:02'),
(129, 'Eliecer', 'Enerio', 'eenerio@unixsurplus.com', 'https://lh3.googleusercontent.com/a-/AOh14GgVSJOn1iFTwlTU2_K7-tybaZxoN5X2Nl9sXyE0=s96-c', NULL, 'ya29.a0ARrdaM9wEs9DCLupdoRNh8sk-rbM-60PQm999IztnM_PorKNgouNp8dbvnGiSEMc_xe87Ijw3a7_-QQZSp6kZo4SRe9EPHyLZRJFO9MLd4f00AlO7nkWu6isVi4q44M2IjsCc8r4OveVVG6btv1sajGi3L2ygc8', '113393794589649675135', 'NULL', '{\"item_number\":{\"is_shown\":true},\"image\":{\"is_shown\":true},\"title\":{\"is_shown\":false}}', '{\"item_number\":{\"is_shown\":true},\"item_count\":{\"is_shown\":true},\"image\":{\"is_shown\":true},\"title\":{\"is_shown\":true},\"seller\":{\"is_shown\":true},\"price\":{\"is_shown\":true},\"sku\":{\"is_shown\":true},\"quantity\":{\"is_shown\":true},\"sold\":{\"is_shown\":true},\"hits\":{\"is_shown\":true},\"date\":{\"is_shown\":true},\"date_count\":{\"is_shown\":true}}', '{\"moved\":{\"is_shown\":true}}', '{\"sku\":{\"is_shown\":true},\"bar\":{\"is_shown\":true},\"brand\":{\"is_shown\":true},\"ff\":{\"is_shown\":true},\"size\":{\"is_shown\":true},\"type\":{\"is_shown\":true},\"rpm\":{\"is_shown\":true},\"speed\":{\"is_shown\":true},\"interface\":{\"is_shown\":true},\"series\":{\"is_shown\":true},\"con\":{\"is_shown\":true}}', NULL, '2021-07-29 20:14:11', '2021-07-29 20:14:11');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
