ALTER TABLE `linn`.`listing_table`
      ADD COLUMN `weight` INT(11) DEFAULT 0 AFTER `shipping`,
      ADD COLUMN `weight_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `weight`,
      ADD COLUMN `request_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `request`,
      ADD COLUMN `sku_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `sku`,
      ADD COLUMN `channel_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `channel`,
      ADD COLUMN `price_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `price`,
      ADD COLUMN `shipping_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `shipping`,
      ADD COLUMN `banner_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `banner`,
      ADD COLUMN `draft_id_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `draft_id`,
      ADD COLUMN `listing_item_id_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `listing_item_id`,
      ADD COLUMN `notes_last_edited_by` VARCHAR(255) DEFAULT NULL AFTER `notes`;
