ALTER TABLE `linn`.`ebay_store_name`
  ADD `username` VARCHAR (255) NULL AFTER `store`,
  ADD `products` VARCHAR (255) NULL AFTER `username`,
  ADD `url` VARCHAR(255) NULL AFTER `products`;
