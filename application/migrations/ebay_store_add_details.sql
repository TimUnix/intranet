ALTER TABLE `linn`.`ebay_store`
  ADD COLUMN `shipping_type` VARCHAR (255) NULL AFTER `title`,
  ADD COLUMN `ship_to_locations` VARCHAR (255) NULL AFTER `shipping_type`,
  ADD COLUMN `expedited_shipping` BOOLEAN NULL AFTER `ship_to_locations`,
  ADD COLUMN `one_day_shipping_available` BOOLEAN NULL AFTER `expedited_shipping`,
  ADD COLUMN `top_rated_listing` BOOLEAN NULL AFTER `one_day_shipping_available`,
  ADD COLUMN `handling_time` INT (11) NULL AFTER `top_rated_listing`,
  ADD COLUMN `selling_state` VARCHAR (255) NULL AFTER `handling_time`,
  ADD COLUMN `condition_id` INT (11) NULL AFTER `selling_state`,
  ADD COLUMN `condition_display_name` VARCHAR (255) NULL AFTER `condition_id`,
  ADD COLUMN `qty` INT (11) NULL AFTER `condition_display_name`;
