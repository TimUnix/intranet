ALTER TABLE `linn`.`listing_table`
      ADD COLUMN `request_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `request_last_edited_by`,
      ADD COLUMN `channel_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `channel_last_edited_by`,
      ADD COLUMN `price_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `price_last_edited_by`,
      ADD COLUMN `shipping_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `shipping_last_edited_by`,
      ADD COLUMN `weight_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `weight_last_edited_by`,
      ADD COLUMN `banner_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `banner_last_edited_by`,
      ADD COLUMN `notes_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `notes_last_edited_by`;
      
