CREATE TABLE IF NOT EXISTS `ebay_business_policies` (
       `id` INTEGER NOT NULL AUTO_INCREMENT,
       `policy_id` VARCHAR(50),
       `name` VARCHAR(100),
       `description` VARCHAR(255),
       `store` VARCHAR(20),
       `policy_type` VARCHAR(20),
       PRIMARY KEY(`id`)
);
