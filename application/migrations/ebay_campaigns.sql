CREATE TABLE `ebay_campaigns` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `ebay_id` VARCHAR(255),
       `name` VARCHAR(80),
       `status` VARCHAR(50),
       `start_date` VARCHAR(255),
       `end_date` VARCHAR(255),
       `bid_percentage` VARCHAR(10),
       `store` VARCHAR(25),
       PRIMARY KEY(`id`)
);
