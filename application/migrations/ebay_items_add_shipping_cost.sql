ALTER TABLE `ebay_items`
ADD COLUMN `shipping_cost` DECIMAL AFTER `img`;

ALTER TABLE `ebay_items_archive`
ADD COLUMN `shipping_cost` DECIMAL AFTER `img`;
