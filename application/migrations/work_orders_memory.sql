CREATE TABLE IF NOT EXISTS `linn`.`memory`(
       `id` INT NOT NULL AUTO_INCREMENT,
       `sku` VARCHAR(100) NOT NULL,
       `barcode` VARCHAR(100) NOT NULL,
       `brand` VARCHAR(20) NULL DEFAULT NULL,
       `memory_size` INT NULL DEFAULT NULL,
       `memory_family` VARCHAR(10) NULL DEFAULT NULL,
       `memory_type` VARCHAR(10) NULL DEFAULT NULL,
       `speed` INT NULL DEFAULT NULL,
       `registry` VARCHAR(10) NULL DEFAULT NULL,
       `cycles` INT NULL DEFAULT NULL,
       `rank` VARCHAR(10) NULL DEFAULT NULL,
       `voltage` FLOAT NULL DEFAULT NULL,
       `notes` TEXT NULL DEFAULT NULL,
       PRIMARY KEY (id)
);
