CREATE TABLE `ebay_category_specifics_keys` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `category_id` INT NOT NULL,
       `name` VARCHAR(100) NOT NULL,
       `help_text` LONGTEXT,
       `min_values` INT DEFAULT 0,
       `max_values` INT DEFAULT 1,
       `requirement` VARCHAR(50),
       PRIMARY KEY (id)
);

CREATE TABLE  `ebay_category_specifics_values` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `ebay_category_specifics_keys_id` INT NOT NULL,
       `value` VARCHAR(100) NOT NULL,
       PRIMARY KEY (id),
       FOREIGN KEY (ebay_category_specifics_keys_id) REFERENCES ebay_category_specifics_keys(id)
);
