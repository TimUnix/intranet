CREATE TABLE `nebula_vendor` (
	   id INT NOT NULL AUTO_INCREMENT,
	   vendor_name VARCHAR(100),
	   logo_path VARCHAR(255),
	   date_added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	   PRIMARY KEY (id)
);
