ALTER TABLE `nebula`
	  ADD COLUMN `status_last_edited` TIMESTAMP NULL DEFAULT NULL AFTER `status`,
	  ADD COLUMN `status_last_edited_by` VARCHAR(50) AFTER `status_last_edited`;
