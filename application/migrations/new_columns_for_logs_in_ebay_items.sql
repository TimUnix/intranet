ALTER TABLE `ebay_items` ADD `previous_description` VARCHAR(255) NOT NULL AFTER `registration_date`;
ALTER TABLE `ebay_items` ADD `previous_edited_by` VARCHAR(255) NOT NULL AFTER `previous_description`;
ALTER TABLE `ebay_items` ADD `previous_date_edited` VARCHAR(255) NOT NULL AFTER `previous_edited_by`;
ALTER TABLE `ebay_items` ADD `current_date_added` VARCHAR(255) NOT NULL AFTER `previous_date_edited`;
ALTER TABLE `ebay_items` ADD `current_added_by` VARCHAR(255) NOT NULL AFTER `current_date_added`;