ALTER TABLE `linn`.`ebay_items`
      ADD COLUMN `timestamp` VARCHAR(255) NULL DEFAULT NULL AFTER `date`,
      ADD COLUMN `start_time` VARCHAR(255) NULL DEFAULT NULL AFTER `timestamp`,
      ADD COLUMN `end_time` VARCHAR(255) NULL DEFAULT NULL AFTER `start_time`,
      ADD COLUMN `registration_date` VARCHAR(255) NULL DEFAULT NULL AFTER `end_time`;
