<?php

include_once APPPATH . "libraries/vendor/autoload.php";
// Alias Google_Model to prevent naming conflict with the project's Google_model
use Google\Google_Model as GModel;
use phpseclib\Net\SFTP;


class NebulaSessionHandler {
    private $home_dir;
    private $session;

    public function __construct(string $host = '', int $port = 22, string $home = '') {
        $this->session = new SFTP($host, $port);
        $this->home_dir = $home;
    }

    public function login(string $username, string $password) {
        return $this->session->login($username, $password);
    }

    public function create_lot($lot, $purchase_order) {
        $lot_dir = "{$this->home_dir}/certificates/$purchase_order/$lot";

        if (substr($this->home_dir, -1) === '/') {
            $certificate_dir = "{$this->home_dir}certificates/$purchase_order/$lot";
        }

        $this->session->mkdir($lot_dir);
    }

    public function rename_lot($purchase_order, $old_lot, $new_lot) {
        $certificate_dir = "{$this->home_dir}/certificates";

        if (substr($this->home_dir, -1) === '/') {
            $certificate_dir = "{$this->home_dir}certificates";
        }

        $old_path = "$certificate_dir/$purchase_order/$old_lot";
        $new_path = "$certificate_dir/$purchase_order/$new_lot";

        return $this->session->rename($old_path, $new_path);
    }

    public function create_purchase_order($purchase_order) {
        $certificate_dir = "{$this->home_dir}/certificates/$purchase_order";

        if (substr($this->home_dir, -1) === '/') {
            $certificate_dir = "{$this->home_dir}certificates/$purchase_order";
        }

        $this->session->mkdir($certificate_dir);
    }

    public function rename_purchase_order($old_name, $new_name) {
        $certificate_dir = "{$this->home_dir}/certificates";

        if (substr($this->home_dir, -1) === '/') {
            $certificate_dir = "{$this->home_dir}certificates";
        }

        $old_path = "$certificate_dir/$old_name";
        $new_path = "$certificate_dir/$new_name";

        return $this->session->rename($old_path, $new_path);
    }

    public function disconnect() {
        $this->session->disconnect();
        unset($this->session);
    }
}
