<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Google API Configuration
| -------------------------------------------------------------------
| 
| To get API details you have to create a Google Project
| at Google API Console (https://console.developers.google.com)
| 
|  client_id         string   Your Google API Client ID.
|  client_secret     string   Your Google API Client secret.
|  redirect_uri      string   URL to redirect back to after login.
|  application_name  string   Your Google application name.
|  api_key           string   Developer key.
|  scopes            string   Specify scopes
*/
$config['google']['client_id']        = '859200342572-vbmjs42oj0mnlkjfi7dplo3kk1uvq4u7.apps.googleusercontent.com';
$config['google']['client_secret']    = '_gC_mFoEgWLYgU3YCUHKbEb2';
$config['google']['redirect_uri']     = base_url() .'index.php/home/';
$config['google']['application_name'] = 'Web client 4';
$config['google']['api_key']          = '';
$config['google']['scopes']           = array();