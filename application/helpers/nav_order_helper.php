<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nav_Item {
    private $id;
    private $title;
    private $children;
}

function organize_nav_order($nav) {
    $ordered_nav = array();
	
    $menus = array_filter($nav, function($nav_item) {
        return $nav_item->menu_id === $nav_item->menu_parent;
    });
	
    $submenus = array_filter($nav, function($nav_item) {;
        return $nav_item->menu_id !== $nav_item->menu_parent;
    });
	
    foreach ($menus as $menu) {
		//$route = str_replace(' ', '_', strtolower($menu->menu_intranet));
		$route = $menu->menu_intranet;

        $filtered_children = array_filter($submenus, function($child) use($menu) {
            return $menu->menu_id === $child->menu_parent;
        });

        $children = array_map(function($child) {
			//$child_route = str_replace(' ', '_', strtolower($child->menu_intranet));
			$child_route = $child->menu_intranet;
            return array(
                'id' => $child->menu_id,
                'title' => $child->menu_title,
                'route' => $child_route,
                'icon' => $child->icons,
            );
        }, $filtered_children);
        
        $ordered_nav[$route] = array(
            'id' => $menu->menu_id,
            'title' => $menu->menu_title,
            'route' => $route,
            'icon' => $menu->icons,
            'children' => isset($children) && count($children) > 0 ? $children : array()
        );
	}

    return $ordered_nav;
}

?>
