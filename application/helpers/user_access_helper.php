<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_login_redirect_route($menu) {

    if ($menu) {
        $route = str_replace(' ', '_', strtolower($menu->menu_title));
        
        // If the queried row does exist, return the title of that row as the route
        return $route === 'search' ? 'home' : $route;
    }

    // If the queried row returned null, route to access_denied
    return 'inventory';
}

function check_user_access_permissions($access) {
    return ((int) $access) === 1;
}

?>
