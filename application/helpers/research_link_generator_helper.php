<?php

function generate_research_link($query) {
    $encoded = urlencode($query);
    $url = base_url() . "index.php/research?search={$encoded}";
    return "<a href=\"{$url}\">" .
           '<i class="fa fa-search" aria-hidden="true"></i>' .
           '</a>';
}

?>
