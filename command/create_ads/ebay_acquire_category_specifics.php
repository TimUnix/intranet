<?php
ini_set("memory_limit", "-1");
set_time_limit(0);

require_once __DIR__.'/../ebay_auth.php';
require_once __DIR__.'/../database.php';

// This script is expected to be run manually and rarely. It's only meant to update category specifics, for use with
// creating eBay Ads in the ebay items module, whenever ebay makes changes to them, which doesn't happen all the time.

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: $mysqli->connect_error");
}

echo "Attempting to clear ebay category specifics data...\n";

$ebay_category_specifics_values_delete_query = "DELETE FROM `ebay_category_specifics_values`;";
$ebay_category_specifics_values_delete_stmt = $mysqli->prepare($ebay_category_specifics_values_delete_query);

$ebay_category_specifics_keys_delete_query = "DELETE FROM `ebay_category_specifics_keys`;";
$ebay_category_specifics_keys_delete_stmt = $mysqli->prepare($ebay_category_specifics_keys_delete_query);

if (($ebay_category_specifics_values_delete_stmt && $ebay_category_specifics_values_delete_stmt->execute())
    && $ebay_category_specifics_keys_delete_stmt && $ebay_category_specifics_keys_delete_stmt->execute()) {
    echo "Successfully deleted category specifics data\n";
} else {
    echo "A problem was encountered while deleting ebay category specifics data...\n";
    die("Error: {$mysqli->error}");
}

echo "Acquiring category id's required for API call...\n";

$ebay_categories_query = "SELECT `ebay_id`, `name` FROM `ebay_categories`";

$ebay_categories_stmt = $mysqli->prepare($ebay_categories_query);
$ebay_categories_success_state = $ebay_categories_stmt->execute();
$ebay_categories_result = $ebay_categories_stmt->get_result();

if ($ebay_categories_success_state && $ebay_categories_result->num_rows > 0) {
    echo "Acquired {$ebay_categories_result->num_rows} categories...\n\n";
} else {
    die("An error occured while acquiring categories...\n");
}

$ebay_api_token = generate_application_access_token();

while ($category = $ebay_categories_result->fetch_object()) {
    if ($category->ebay_id === '') {
        continue;
    }

    $category_exists_check_query = "SELECT `category_id` FROM `ebay_category_specifics_keys` "
                                 . "WHERE `category_id` = {$category->ebay_id}";
    $category_exists_check_stmt = $mysqli->prepare($category_exists_check_query);

    if ($category_exists_check_stmt && $category_exists_check_stmt->execute()) {
        $category_exists_check_result = $category_exists_check_stmt->get_result();

        if ($category_exists_check_result->num_rows > 0) {
            continue;
        }
    }    

    echo "Acquiring specifics for {$category->name}...\n";

    $ebay_taxonomy_url = "https://api.ebay.com/commerce/taxonomy/v1/category_tree/0/get_item_aspects_for_category"
                       . "?category_id={$category->ebay_id}";
    $curl = curl_init($ebay_taxonomy_url);
    $headers = array(
        "Authorization: Bearer $ebay_api_token"
    );

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response_body = curl_exec($curl);
    $response = json_decode($response_body);

    if (!isset($response->aspects) || count($response->aspects) === 0) {
        echo "No category specifics found for {$category->name}\n";
        continue;
    }

    foreach ($response->aspects as $aspect) {
        $name = $mysqli->real_escape_string($aspect->localizedAspectName);
        $requirement = $aspect->aspectConstraint->aspectRequired ? 'REQUIRED'
                     : $mysqli->real_escape_string($aspect->aspectConstraint->aspectUsage);
        $cardinality = $aspect->aspectConstraint->itemToAspectCardinality === 'SINGLE' ? 1 : 0;
        $max_length = isset($aspect->aspectConstraint->aspectMaxLength)
                    ? $aspect->aspectConstraint->aspectMaxLength : NULL;
        $format = isset($aspect->aspectConstraint->aspectFormat) ? $aspect->aspectConstraint->aspectFormat : NULL;

        $insert_category_specifics_key_query = "INSERT INTO `ebay_category_specifics_keys` ("
                                             . "`name`,"
                                             . "`cardinality`,"
                                             . "`requirement`,"
                                             . "`max_length`,"
                                             . "`format`,"
                                             . "`category_id`"
                                             . ") VALUES (?, ?, ?, ?, ?, ?);";
        $insert_category_specifics_key_stmt = $mysqli->prepare($insert_category_specifics_key_query);
        $insert_category_specifics_key_stmt->bind_param("sisisi", $name, $cardinality, $requirement, $max_length,
                                                        $format, $category->ebay_id);
        if ($insert_category_specifics_key_stmt && $insert_category_specifics_key_stmt->execute()) {
            echo "Successfully added $name\n";

            $values = array();
            $key_id = $mysqli->insert_id;

            if (!isset($aspect->aspectValues) || count($aspect->aspectValues) == 0) {
                echo "No default values for $name.\n";
                continue;
            }

            foreach ($aspect->aspectValues as $value) {
                $value = $mysqli->real_escape_string($value->localizedValue);
                $values[] = "$key_id, '$value'";
            }

            $insert_category_specifics_value_query = "INSERT INTO `ebay_category_specifics_values` ("
                                                   . "`ebay_category_specifics_keys_id`,"
                                                   . "`value`"
                                                   . ") VALUES ("
                                                   . implode("), (", $values)
                                                   . ");";


            $insert_category_specifics_value_stmt = $mysqli->prepare($insert_category_specifics_value_query);

            if ($insert_category_specifics_value_stmt && $insert_category_specifics_value_stmt->execute()) {
                echo "Successfully added values for $name\n";
            } else {
                echo "A problem was encountered while adding values for $name\n";
                print_r($insert_category_specifics_value_stmt->error_list);
            }
        }
    }

    curl_close($curl);
}

$mysqli->close();
