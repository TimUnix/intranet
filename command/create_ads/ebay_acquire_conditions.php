<?php
ini_set("memory_limit", "-1");
set_time_limit(0);

require_once __DIR__.'/../ebay_auth.php';
require_once __DIR__.'/../database.php';

$ebay_condition_values = array(
    '1000' => 'NEW',
    '1500' => 'NEW_OTHER',
    '1750' => 'NEW_WITH_DEFECTS',
    '2000' => 'CERTIFIED_REFURBISHED',
    '2500' => 'SELLER_REFURBISHED',
    '2750' => 'LIKE_NEW',
    '3000' => 'USED_EXCELLENT',
    '4000' => 'USED_VERY_GOOD',
    '5000' => 'USED_GOOD',
    '6000' => 'USED_ACCEPTABLE',
    '7000' => 'FOR_PARTS_OR_NOT_WORKING'
);

// This script is expected to be run manually and rarely. It's meant to update conditions applicable for each
// category, and whether they're required or not. eBay may update the data occasionally, which is when this script may
// be run. The data gathered by this script is expected to be used in creating eBay Ads in the ebay items module.

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: $mysqli->connect_error");
}

echo "Attempting to clear ebay item conditions data...\n";

$ebay_conditions_delete_query = "DELETE FROM `ebay_conditions`";
$ebay_conditions_delete_stmt = $mysqli->prepare($ebay_conditions_delete_query);

if ($ebay_conditions_delete_stmt !== false & $ebay_conditions_delete_stmt->execute()) {
    echo "Successfully deleted ebay conditions data\n";
} else {
    echo "A problem was encountered while deleting ebay conditions data...\n";
    print_r("Error: ${$mysqli->error}");

    $mysqli->close();
    return;
}

echo "Acquiring category id's required for API call...\n";

$ebay_categories_query = "SELECT DISTINCT `ebay_id` FROM `ebay_categories` WHERE `ebay_id` IS NOT NULL";
$ebay_categories_stmt = $mysqli->prepare($ebay_categories_query);
$ebay_categories_success_state = $ebay_categories_stmt->execute();
$ebay_categories_result = $ebay_categories_stmt->get_result();

$ebay_categories = array();

while ($ebay_category = $ebay_categories_result->fetch_object()) {
    $ebay_categories[] = $ebay_category->ebay_id;
}

$ebay_marketplace_id = 'EBAY_US';
$ebay_condition_policy_filters = urlencode('categoryIds:{'.implode('|', $ebay_categories).'}');
$ebay_metadata_url = "https://api.ebay.com/sell/metadata/v1/marketplace/$ebay_marketplace_id/"
                   . "get_item_condition_policies?filter=$ebay_condition_policy_filters";

$ebay_oauth_token = generate_application_access_token();

$headers = array(
    "Authorization: Bearer $ebay_oauth_token"
);

$curl = curl_init($ebay_metadata_url);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$conditions_response = json_decode(curl_exec($curl));

curl_close($curl);

$conditions_insert = array();

foreach ($conditions_response->itemConditionPolicies as $policy) {
    $category_id = $policy->categoryId;
    $is_required = $policy->itemConditionRequired ? 1 : 0;

    foreach ($policy->itemConditions as $condition) {
        $condition_id = $condition->conditionId;
        $description = $condition->conditionDescription;
        $ebay_enum = $ebay_condition_values["$condition_id"];
        
        $conditions_insert[] = "($category_id, $is_required, $condition_id, '$description', '$ebay_enum')";
    }
}

if (isset($conditions_response->warnings)) {
    foreach($conditions_response->warnings as $warning) {
        $error_id = $warning->errorId;
        $long_message = $warning->longMessage;
        echo "An error with id $errorId occurred:\n"
            . "$long_message\n";
    }
}

$conditions_insert_query = 'INSERT INTO `ebay_conditions` ('
    . '`category_id`, `required`, `condition_id`, `display_name`, `ebay_enum`'
    . ')'
    . 'VALUES '
    . implode(',', $conditions_insert);
$conditions_insert_stmt = $mysqli->prepare($conditions_insert_query);

if (!$conditions_insert_stmt || !$conditions_insert_stmt->execute()) {
    print_r($mysqli->error);
    $mysqli->close();
    die("Something went wrong while attempting to save eBay category conditions data\n");
}

if ((int) $conditions_insert_stmt->affected_rows > 0) {
    echo "Successfully inserted {$conditions_insert_stmt->affected_rows} rows";
}

$mysqli->close();
