<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

require_once __DIR__.'/../ebay_auth.php';
require_once __DIR__.'/../database.php';

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: {$mysqli->connect_error}");
}

echo "Clearing campaigns...\n";

$clear_campaigns_query = 'DELETE FROM `ebay_campaigns`';
$clear_campaigns_stmt = $mysqli->prepare($clear_campaigns_query);

if ($clear_campaigns_stmt && !$clear_campaigns_stmt->execute()) {
    echo "An unexpected error ocurred while clearing saved campaigns\n";
}

echo "Succesfully cleared campaigns...\n";

$stores_query = 'SELECT `name` FROM `ebay_items_store`';
$stores_stmt = $mysqli->prepare($stores_query);

if ($stores_stmt && !$stores_stmt->execute()) {
    echo "Failed to acquire store names\n";
    $mysqli->close();
    return;
}

$stores_stmt->bind_result($store);
while($stores_stmt->fetch()) {
    $stores[] = $store;
}

$ebay_marketing_url = 'https://api.ebay.com/sell/marketing/v1/ad_campaign?limit=500';
$ebay_scope = urlencode('https://api.ebay.com/oauth/api_scope/sell.marketing.readonly');

$curl = curl_init($ebay_marketing_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

foreach ($stores as $store) {
    $refresh_token_code = '';

    switch(strtolower($store)) {
    case 'unixsurpluscom':
        $refresh_token_code = 'ebay_refresh_token_com';
        break;
    case 'unixsurplusnet':
        $refresh_token_code = 'ebay_refresh_token_net';
        break;
    case 'unixpluscom':
        $refresh_token_code = 'ebay_refresh_token_plus';
        break;
    case 'itrecyclenow':
        $refresh_token_code = 'ebay_refresh_token_itr';
        break;
    default:
        $refresh_token_code = '';
    }

    if ($refresh_token_code === '') {
        echo "No token code found for $store.\n";
        continue;
    }

    $refresh_token_query = "SELECT `value` FROM `configuration` WHERE `code` = '$refresh_token_code'";
    $refresh_token_stmt = $mysqli->prepare($refresh_token_query);

    if ($refresh_token_stmt && !$refresh_token_stmt->execute()) {
        echo "Unable to acquire refresh token for $store.\n";
        continue;
    }

    $refresh_token_stmt->bind_result($refresh_token);
    if (!$refresh_token_stmt->fetch()) {
        $refresh_token_stmt->close();
        echo "Unable to acquire refresh token for $store.\n";
        continue;
    }

    $refresh_token_stmt->close();

    $token_response = json_decode(refresh_ebay_oauth_token($refresh_token, $ebay_scope));
    $oauth_token = $token_response->access_token;

    $headers = array(
        "Authorization: Bearer $oauth_token",
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $response = json_decode(curl_exec($curl));

    $campaign_insert = array();

    foreach ($response->campaigns as $campaign) {
        $campaign_id = $mysqli->real_escape_string($campaign->campaignId);
        $campaign_name = $mysqli->real_escape_string($campaign->campaignName);
        $campaign_status = $mysqli->real_escape_string($campaign->campaignStatus);
        $campaign_start_date = $mysqli->real_escape_string($campaign->startDate);
        $campaign_end_date = $mysqli->real_escape_string($campaign->endDate);
        $campaign_bid_percentage = $mysqli->real_escape_string($campaign->fundingStrategy->bidPercentage);

        $campaign_insert[] = "('$campaign_id', '$campaign_name', '$campaign_status', '$campaign_start_date', "
                           . "'$campaign_end_date', '$campaign_bid_percentage', '$store')";
    }

    $campaign_add_query = 'INSERT INTO `ebay_campaigns` (`ebay_id`, `name`, `status`, `start_date`, `end_date`, '
                        . '`bid_percentage`, `store`) VALUES '
                        . implode(', ', $campaign_insert);

    $campaign_add_stmt = $mysqli->prepare($campaign_add_query);

    if ($campaign_add_stmt && !$campaign_add_stmt->execute()) {
        echo "Failed to save campaign data for $store\n";
    }

    if ($campaign_add_stmt->affected_rows > 0) {
        echo "Succesfully added {$campaign_add_stmt->affected_rows} rows for $store\n";
    }

    $campaign_add_stmt->close();
}

curl_close($curl);
$mysqli->close();
