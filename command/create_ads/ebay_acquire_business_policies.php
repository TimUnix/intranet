<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

require_once __DIR__.'/../ebay_auth.php';
require_once __DIR__.'/../database.php';

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: {$mysqli->connect_error}");
}

$delete_query = 'DELETE FROM ebay_business_policies';
$delete_stmt = $mysqli->prepare($delete_query);

if ($delete_stmt !== false && $delete_stmt->execute()) {
    echo "Successfully cleared business policies\n";
} else {
    echo "Unable to clear business policies\n";
    echo "Unable to proceed\n";
    print_r($delete_stmt->error);
    
    $mysqli->close();

    return;
}

$stores_query = 'SELECT name FROM ebay_items_store';
$stores_stmt = $mysqli->prepare($stores_query);

if (!$stores_stmt->execute()) {
    echo "Failed to load stores.\n";
    print_r($stores_stmt->error);
    $mysqli->close();

    return;
}

if (!$stores_stmt->num_rows === 0) {
    echo "No stores found.\n";
    print_r($stores_stmt->error);
    $mysqli->close();

    return;
}

$stores_stmt->bind_result($store);
$stores = array();

while ($stores_stmt->fetch()) {
    $stores[] = $store;
}

$stores_stmt->close();

$marketplace_id = 'EBAY_US';
$ebay_account_api_base_url = 'https://api.ebay.com/sell/account/v1/';

$fulfillment_policy_url = "{$ebay_account_api_base_url}fulfillment_policy?marketplace_id=$marketplace_id";
$payment_policy_url = "{$ebay_account_api_base_url}payment_policy?marketplace_id=$marketplace_id";
$return_policy_url = "{$ebay_account_api_base_url}return_policy?marketplace_id=$marketplace_id";

$curl_fulfillment_policy = curl_init($fulfillment_policy_url);
$curl_payment_policy = curl_init($payment_policy_url);
$curl_return_policy = curl_init($return_policy_url);

foreach ($stores as $store) {
    echo "Loading refresh token for {$store}...\n";

    switch(strtolower($store)) {
    case 'unixsurplusnet':
        $configuration_code = 'ebay_refresh_token_net';
        break;
    case 'unixpluscom':
        $configuration_code = 'ebay_refresh_token_plus';
        break;
    case 'itrecyclenow':
        $configuration_code = 'ebay_refresh_token_itr';
        break;
    default:
        $configuration_code = 'ebay_refresh_token_com';
        break;
    }

    $refresh_token_query = "SELECT value FROM configuration WHERE code = ?";
    $refresh_token_stmt = $mysqli->prepare($refresh_token_query);
    $refresh_token_stmt->bind_result($refresh_token);
    $refresh_token_stmt->bind_param('s', $configuration_code);


    if (!$refresh_token_stmt->execute() && !$refresh_token_stmt->fetch()) {
        echo "An error occured while loading refresh token for {$store}\n";

        continue;
    }

    if ($refresh_token_stmt->fetch()) {
        echo "Acquiring oauth token for {$store}...\n";

        $oauth_token_response = json_decode(refresh_ebay_oauth_token($refresh_token));
        $oauth_token = $oauth_token_response->access_token;
    }

    $refresh_token_stmt->close();

    if (!isset($oauth_token)) {
        echo "Failed to acquire oauth token\n";

        continue;
    }

    $headers = array(
        "Authorization: Bearer $oauth_token",
        'Accept: application/json',
        'Content-Type: application/json'
    );
    
    curl_setopt($curl_fulfillment_policy, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl_fulfillment_policy, CURLOPT_RETURNTRANSFER, true);

    $fulfillment_policy_response = curl_exec($curl_fulfillment_policy);
    $fulfillment_policies = json_decode($fulfillment_policy_response);

    $fulfillment_policies_insert = array();

    foreach ($fulfillment_policies->fulfillmentPolicies as $policy) {
        $id = $mysqli->real_escape_string($policy->fulfillmentPolicyId);
        $name = $mysqli->real_escape_string($policy->name);
        $description = isset($policy->description) ? $mysqli->real_escape_string($policy->description) : '';
        
        $fulfillment_policies_insert[] = "'$id', '$name', '$description', '$store', 'shipping'";
    }

    $fulfillment_policies_insert_query = "INSERT INTO "
                                       . "ebay_business_policies (policy_id, name, description, store, policy_type) "
                                       . "VALUES ("
                                       . implode('), (', $fulfillment_policies_insert)
                                       . ")";

    echo "Saving shipping policies for {$store}...\n";

    $fulfillment_policies_insert_stmt = $mysqli->prepare($fulfillment_policies_insert_query);

    if (!$fulfillment_policies_insert_stmt->execute()) {
        echo "An error occurred while trying to save fulfillment policies for {$store}.\n";
        print_r($fulfillment_policies_insert_stmt->error);
    }

    if ($fulfillment_policies_insert_stmt->affected_rows > 0) {
        echo "Successfully saved fulfillment policies for {$store}.\n";
    }

    $fulfillment_policies_insert_stmt->close();

    curl_setopt($curl_payment_policy, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl_payment_policy, CURLOPT_RETURNTRANSFER, true);

    $payment_policy_response = curl_exec($curl_payment_policy);
    $payment_policies = json_decode($payment_policy_response);

    $payment_policies_insert = array();

    foreach ($payment_policies->paymentPolicies as $policy) {
        $id = $mysqli->real_escape_string($policy->paymentPolicyId);
        $name = $mysqli->real_escape_string($policy->name);
        $description = isset($policy->description) ? $mysqli->real_escape_string($policy->description) : '';
        
        $payment_policies_insert[] = "'$id', '$name', '$description', '$store', 'payment'";
    }

    $payment_policies_insert_query = "INSERT INTO "
                                   . "ebay_business_policies (policy_id, name, description, store, policy_type) "
                                   . "VALUES ("
                                   . implode('), (', $payment_policies_insert)
                                   . ")";

    echo "Saving payment policies for {$store}...\n";

    $payment_policies_insert_stmt = $mysqli->prepare($payment_policies_insert_query);

    if (!$payment_policies_insert_stmt->execute()) {
        echo "An error occurred while trying to save payment policies for {$store}.\n";
        print_r($payment_policies_insert_stmt->error);
    }

    if ($payment_policies_insert_stmt->affected_rows > 0) {
        echo "Successfully saved payment policies for {$store}.\n";
    }

    $payment_policies_insert_stmt->close();

    curl_setopt($curl_return_policy, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl_return_policy, CURLOPT_RETURNTRANSFER, true);

    $return_policy_response = curl_exec($curl_return_policy);
    $return_policies = json_decode($return_policy_response);

    $return_policies_insert = array();

    foreach ($return_policies->returnPolicies as $policy) {
        $id = $mysqli->real_escape_string($policy->returnPolicyId);
        $name = $mysqli->real_escape_string($policy->name);
        $description = isset($policy->description) ? $mysqli->real_escape_string($policy->description) : '';
        
        $return_policies_insert[] = "'$id', '$name', '$description', '$store', 'return'";
    }

    $return_policies_insert_query = "INSERT INTO "
                                  . "ebay_business_policies (policy_id, name, description, store, policy_type) "
                                  . "VALUES ("
                                  . implode('), (', $return_policies_insert)
                                  . ")";

    echo "Saving return policies for {$store}...\n";

    $return_policies_insert_stmt = $mysqli->prepare($return_policies_insert_query);

    if (!$return_policies_insert_stmt->execute()) {
        echo "An error occurred while trying to save return policies for {$store}.\n";
        print_r($return_policies_insert_stmt->error);
    }
        
    if ($return_policies_insert_stmt->affected_rows > 0) {
        echo "Successfully saved return policies for {$store}.\n";
    }

    $return_policies_insert_stmt->close();        
}

curl_close($curl_fulfillment_policy);
curl_close($curl_payment_policy);
curl_close($curl_return_policy);

$mysqli->close();
