<?php
require_once __DIR__.'/../database.php';

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: {$mysqli->connect_error}");
}

$store_token_owner = readline(
    "Choose a store to which the generated token will belong: [0-4]\n"
    . "(0) Cancel\n"
    . "(1) UNIXSurplusCom\n"
    . "(2) UNIXSurplusNet\n"
    . "(3) UNIXPlusCom\n"
    . "(4) ITRecycleNow\n"
);

switch($store_token_owner) {
case 1:
    $store = 'ebay_refresh_token_com';
    break;
case 2:
    $store = 'ebay_refresh_token_net';
    break;
case 3:
    $store = 'ebay_refresh_token_plus';
    break;
case 4:
    $store = 'ebay_refresh_token_itr';
    break;
default:
    echo "Auth generation cancelled.\n";
    return;
}

$consent_url = grant_user_consent_url_builder();
echo "Please visit the following link and grant consent: $consent_url\n";

$auth_code = readline("Paste the authorization code here to generate and save your token: ");
$oauth_token_response = generate_ebay_oauth_token($auth_code);

$oauth_token = json_decode($oauth_token_response);

$token_expiry_date = new DateTime();
$token_expiry_date->modify("+ {$oauth_token->refresh_token_expires_in} second");
$formatted_token_expiry_date = $token_expiry_date->format('Y-m-d');

$refresh_token_save_query = "UPDATE `configuration` SET `value` = '{$oauth_token->refresh_token}' "
                          . "WHERE `code` = '$store'";
$refresh_token_save_stmt = $mysqli->prepare($refresh_token_save_query);

$refresh_token_expiry_save_query = "UPDATE `configuration` SET `value` = '{$formatted_token_expiry_date}' "
                                 . "WHERE `code` = '{$store}_expiry'";
$refresh_token_expiry_save_stmt = $mysqli->prepare($refresh_token_expiry_save_query);

if (!$refresh_token_save_stmt->execute()) {
    echo "Failed to save refresh token";
    return;
}

if (!$refresh_token_expiry_save_stmt->execute()) {
    echo "Failed to save refresh token";
    return;
}

echo "Successfully saved refresh token\n";

$mysqli->close();

function grant_user_consent_url_builder() {
    $client_id = 'UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2';
    $redirect_uri = 'UNIXSurplus__In-UNIXSurp-itrecy-lereszia';
    $response_type = 'code';
    $state = 'intranet_consent_grant';

    $scopes = array(
        'https://api.ebay.com/oauth/api_scope',
        'https://api.ebay.com/oauth/api_scope/sell.marketing.readonly',
        'https://api.ebay.com/oauth/api_scope/sell.marketing',
        'https://api.ebay.com/oauth/api_scope/sell.inventory.readonly',
        'https://api.ebay.com/oauth/api_scope/sell.inventory',
        'https://api.ebay.com/oauth/api_scope/sell.account.readonly',
        'https://api.ebay.com/oauth/api_scope/sell.account',
        'https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly',
        'https://api.ebay.com/oauth/api_scope/sell.fulfillment',
        'https://api.ebay.com/oauth/api_scope/sell.analytics.readonly',
        'https://api.ebay.com/oauth/api_scope/sell.finances',
        'https://api.ebay.com/oauth/api_scope/sell.payment.dispute',
        'https://api.ebay.com/oauth/api_scope/commerce.identity.readonly',
        'https://api.ebay.com/oauth/api_scope/commerce.notification.subscription',
        'https://api.ebay.com/oauth/api_scope/commerce.notification.subscription.readonly'
    );

    $scope = urlencode(implode(' ', $scopes));

    return "https://auth.ebay.com/oauth2/authorize?"
        . "client_id=$client_id"
        . "&redirect_uri=$redirect_uri"
        . "&response_type=$response_type"
        . "&state=$state"
        . "&scope=$scope"
        . "&prompt=login";
}

function generate_ebay_oauth_token($auth_code) {
    $url = 'https://api.ebay.com/identity/v1/oauth2/token';

    $client_id = 'UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2';
    $client_secret = 'PRD-81f16f178339-a79c-482c-9342-4c37';
    $redirect_uri = 'UNIXSurplus__In-UNIXSurp-itrecy-lereszia';

    $content_type = 'application/x-www-form-urlencoded';
    $oauth_credentials = base64_encode("$client_id:$client_secret");
    $grant_type = 'authorization_code';

    $headers = array(
        "Content-Type: $content_type",
        "Authorization: Basic $oauth_credentials"
    );

    $request_body = "grant_type=$grant_type&code=$auth_code&redirect_uri=$redirect_uri";

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);

    $response = curl_exec($curl);

    curl_close($curl);

    return $response;
}
