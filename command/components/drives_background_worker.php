<?php
require_once __DIR__.'/../database.php';

$db = new db();
$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->autocommit(false);

$category_name_query = "SELECT * FROM `inventory` WHERE `cat` in ('SAS', 'SATA', 'SSD') and `arc` = 'false'";
$category_name_result = $conn->query($category_name_query);

if ($category_name_result && $category_name_result->num_rows > 0){
    while ($category_searcher = $category_name_result->fetch_object()) {
       
    $form_factor = null;
    $size=null;
    $rpm = null;
    $speed = null;
    $condition= NULL;
    $series = null;
    $extra= null;
    $upc = null;

    $brand = $category_searcher->man;
    strtoupper($brand);

    $brand = strtoupper($category_searcher->man);

    $category = $category_searcher->cat;

    if(preg_match('/[Nn][Ee][WW]/', $category_searcher->con) === 1 || preg_match('/[Nn][Pp][6]/', $category_searcher->con) === 1 || preg_match('/[Nn][Pp][3]/', $category_searcher->con) === 1 || preg_match('/[Gg][Bb]/', $category_searcher->con) === 1){
        $condition = $category_searcher->con;
    }
    else if ($category_searcher->con === 'REF'){
        $condition = null;
    }
    else{
         $condition = null;
    }

        /*if(preg_match('/[Ww][Dd]/', $category_searcher->man) === 1 && preg_match('/[Ww][Dd]/', $category_searcher->bar) === 1 ){
                $sku = $category_searcher->bar;
                $barcode = null;
                    
            //diri ang sayop CURRENT CODE WALA NAKUHA ANG NAAY NP3

                //$sku = str_ireplace(array("NEW", "NP6", "NP3", "GB", "0 HOURS"), " ", $category_searcher->par);
            
        }
        else if(preg_match('/[Ww][Dd]/', $category_searcher->man) === 1 && preg_match('/[Ww][Dd]/', $category_searcher->bar) === 0 ){
            $sku = $category_searcher->par;
            $barcode = null;
        }*/

        if(preg_match('/[Ww][Dd]/', $category_searcher->man) === 1){
            if(preg_match('/[Ww][Dd]/', $category_searcher->bar) === 1){
                $sku = $category_searcher->bar;
                $barcode = null;
            }else{
                $sku = $category_searcher->par;
                $barcode = null;
            }
        }


        else if(preg_match('/[Hh][Gg][Ss][Tt]/', $category_searcher->man) === 1 && preg_match('/^[Hh][Uu]/', $category_searcher->bar) === 1 ){

                $remove_con = explode(' ', $category_searcher->bar);
                $sku = $remove_con[0];
                $barcode = null;
            

        }else if(preg_match('/[Ss][Ee][Aa][Gg][Aa][Tt][Ee]/', $category_searcher->man) === 1 && preg_match('/^[Ss][Tt]/', $category_searcher->bar) === 1 ){
                $sku = $category_searcher->bar;
                $barcode = null;
            
        }
        else{
            $sku = $category_searcher->par;
            $barcode = $category_searcher->bar;
        }
    
    $separate_des = explode(' ', $category_searcher->des);

    foreach($separate_des as $item){
       /* if (strpos(strtolower($item), '1.8') !== false || strpos(strtolower($item), '2.5') !== false || strpos(strtolower($item), '3.5') !== false || strpos(strtolower($item), 'u.2') !== false || strpos(strtolower($item), 'm.2') !== false || strpos(strtolower($item), 'pcie') !== false || strpos(strtolower($item), 'pci-e') !== false) {

                $form_factor = $item;
                $item = preg_replace('/["]/', 'in', $item);
                var_dump($form_factor);
            }
        else{
            $form_factor = ' ';

        }*/

        if(strpos(strtolower($item), '1.8in') !== false || strpos(strtolower($item), '2.5in') !== false || strpos(strtolower($item), '3.5in') !== false){
            $form_factor = $item;

        }
        else if(strpos(strtolower($item), '1.8"') !== false || strpos(strtolower($item), '2.5"') !== false || strpos(strtolower($item), '3.5"') !== false){
            $form_factor = str_ireplace('"', 'in', $item);

        }
        else if(strpos(strtolower($item), '1.8') !== false || strpos(strtolower($item), '2.5') !== false || strpos(strtolower($item), '3.5') !== false){
            if(preg_match('/^[1.][8][Tt][Bb]/', $item) === 0){
                $end = 'in';
                $form_factor = $item.$end;

            }
            
        }
        else if(strpos(strtolower($item), 'u.2') !== false || strpos(strtolower($item), 'm.2') !== false){
            $form_factor = $item;
   
        }
        else if(preg_match('/[Pp][Cc][Ii][Ee]/', $item) === 1 || preg_match('/[Pp][Cc][Ii][-][Ee]/', $item) === 1){
            $form_factor = 'PCIe';
        }
        else{
            $form_factor = null;

        }
        


         if (strpos(strtolower($item), 'gb') !== false){
             $size = $item;
         }else if (strpos(strtolower($item), 'tb') !== false ) {
               $item = intval($item)*1000;
               $size = $item;
            }
         if (preg_match('/^[0-9]{1,}[Kk]$/', $item) === 1 || preg_match('/^[0-9.]+[0-9][Kk]$/', $item) === 1) {
                $rpm = $item;
            }
            else{
                $rpm = null;
            }
         if (strpos(strtolower($item), 'gbps') !== false || strpos(strtolower($item), 'gb/s') !== false ) {
                $speed = $item;
                trim($item);
            }
         if ($item == 'SSD') {
                $interface = $item;
            }
            else{
                $interface = null;

            }
         if(preg_match('/^[a-zA-z][0-9]{3,}$/', $item) === 1 || preg_match('/^[a-zA-z][a-zA-z][0-9]{3,}$/', $item) === 1 || preg_match('/^[a-zA-z][0-9]{3,}[a-zA-z][a-zA-z]$/', $item) === 1 ){
            $series = $item;
        }

    }

        $checker = $conn->prepare("SELECT sku FROM `drives` WHERE sku = ?");
        $checker->bind_param('s', $sku);
        $checker->execute();
        $checker_result = $checker->get_result();

        if($checker_result && $checker_result->num_rows === 0){
                $stmt = $conn->prepare("INSERT INTO drives (sku, bar, upc, brand, ff, size, type, rpm, speed, 
                                                        interface, series, con)
                                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                $stmt->bind_param('sssssdssssss', $sku, $barcode, $sku, $brand, $form_factor, $size, $category, $rpm,
                $speed, $interface, $series, $condition);

                if ($stmt->execute()) {
                                          echo "Successful\n";
                                      } else {
                                          echo "Error: {$stmt->error}\n";
                                      }  
        }
         
    }
    $conn->commit();

}

$conn->close();
?>
