<?php
require_once __DIR__.'/../database.php';

$db = new db();

$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

$mysqli->autocommit(false);


$memories_from_inventory_query = "SELECT `par`, `bar`, `des`, `con`, `man` FROM `inventory` WHERE `cat` = 'Memory' 
                                  AND `arc` = 'False'";
$memories_from_inventory = $mysqli->query($memories_from_inventory_query);

$manufacturers_from_inventory_query = "SELECT DISTINCT `man` FROM `inventory`";
$manufacturers_from_inventory = $mysqli->query($manufacturers_from_inventory_query);
$manufacturers_reference = array();

if ($manufacturers_from_inventory && $manufacturers_from_inventory->num_rows > 0) {
    while($manufacturer = $manufacturers_from_inventory->fetch_object()) {
        $manufacturers_reference[] = $manufacturer->man;
    }
}

$manufacturers_reference = array_filter($manufacturers_reference, function($manufacturer) {
    return trim($manufacturer) !== '';
});

$manufacturers_reference = array_map(function($manufacturer) {
    return strtolower(trim($manufacturer));
}, $manufacturers_reference);

if ($memories_from_inventory && $memories_from_inventory->num_rows > 0) {
    while ($memory_from_inventory = $memories_from_inventory->fetch_object()) {
        $brand = null;

        $sku = $memory_from_inventory->par;
        $barcode = $memory_from_inventory->bar;

        $description_separated = explode(' ', $memory_from_inventory->des);
        
        $memory_size = null;
        $memory_family = null;
        $memory_type = null;
        $speed = null;
        $registry = null;
        $cycles = null;
        $rank = null;
        $voltage = null;
        $condition = null;

        if (isset($memory_from_inventory->con) && trim($memory_from_inventory->con) !== '') {
            $condition = $memory_from_inventory->con;
        }

        if (isset($memory_from_inventory->man) && trim($memory_from_inventory->man) !== '') {
            if (strpos($memory_from_inventory->man, '/') !== false) {
                $oem_and_manufacturer = explode('/', $memory_from_inventory->man);
                // By convention the OEM is on the left side of the forward slash (/) so that will be used
                $brand = strtoupper($oem_and_manufacturer[0]);
            } else {
                $brand = strtoupper($memory_from_inventory->man);
            }
        }
        
        foreach($description_separated as $description_item) {
            if (!isset($brand) && trim($description_item) !== ''
                && in_array(strtolower($description_item), $manufacturers_reference)) {
                if (strpos($description_item, '/') !== false) {
                    $oem_and_manufacturer = explode('/', $description_item);
                    // By convention the OEM is on the left side of the forward slash (/) so that will be used
                    $brand = strtoupper($oem_and_manufacturer[0]);
                } else {
                    $brand = strtoupper($description_item);
                }
            }

            if (!isset($memory_size) && strpos(strtolower($description_item), 'gb') !== false) {
                $memory_size = $description_item;
            }

            if (strpos(strtolower($description_item), 'ddr') !== false
                || strpos(strtolower($description_item), 'pc') !== false) {

                if (!isset($memory_family) && !isset($memory_type)) {
                    if (strpos($description_item, '-') !== false && strpos($description_item, '-') > 0) {
                        $family_and_speed = explode('-', $description_item);
                        $family = $family_and_speed[0];
                        $speed = $family_and_speed[1];
                        $memory_family = preg_replace('/^[Pp][Cc]/', 'DDR', $family);
                        $memory_type = preg_replace('/^[Dd][Dd][Rr]/', 'PC', $family);
                    } else {
                        $memory_family = preg_replace('/^[Pp][Cc]/', 'DDR', $description_item);
                        $memory_type = preg_replace('/^[Dd][Dd][Rr]/', 'PC', $description_item);
                    }

                    if (isset($speed) && !isset($registry)) {
                        // Replace numbers with blank and retain letters
                        $registry = isset($speed) ? preg_replace('/^[0-9]+/', '', $speed) : null;
                        // Replace letters with blank and retain numbers
                        $speed = isset($speed) ? preg_replace('/[A-Za-z]+/', '', $speed) : null;

                        $cycles = determine_cycle((int) $speed, $memory_family);
                    }
                }
            }
            
            if (strpos(strtolower($description_item), 'mhz') !== false) {
                $cycles = $description_item;
            }

            if (!isset($rank) && preg_match('/^[a-zA-Z0-9]+[Rr][Xx][0-9]+$/', $description_item) === 1) {
                $rank = $description_item;
            }

            if (!isset($voltage) && preg_match('/^[0-9]+.[0-9][Vv]$/', $description_item) === 1) {
                $voltage = $description_item;
            }
        }

        $memory_check_stmt = $mysqli->prepare("SELECT id FROM memory WHERE sku = ?");
        $memory_check_stmt->bind_param('s', $sku);
        $memory_check_stmt->execute();
        $memory_check = $memory_check_stmt->get_result();

        if ($memory_check && $memory_check->num_rows > 0) {
            $stmt = $mysqli->prepare("UPDATE memory
                                      SET
                                          barcode = ?,
                                          brand = ?,
                                          con = ?,
                                          memory_size = ?,
                                          memory_family = ?,
                                          memory_type = ?,
                                          speed = ?,
                                          registry = ?,
                                          cycles = ?,
                                          rank = ?,
                                          voltage = ?
                                      WHERE
                                          sku = ?"
            );

            $stmt->bind_param('sssissisisds', $barcode, $brand, $condition, $memory_size, $memory_family,
                                $memory_type, $speed, $registry, $cycles, $rank, $voltage, $sku);
        } else {
            $stmt = $mysqli->prepare("INSERT INTO memory
                                     (sku, barcode, brand, con, memory_size, memory_family, memory_type, speed, 
                                      registry, cycles, rank, voltage)
                                  VALUES
                                     (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $stmt->bind_param('ssssissisisd', $sku, $barcode, $brand, $condition, $memory_size, $memory_family,
                              $memory_type, $speed, $registry, $cycles, $rank, $voltage);
        }

        if ($stmt->execute()) {
            echo $memory_check && $memory_check->num_rows > 0 ? "Row updated\n" : "Insert success\n";
        } else {
            echo "Insert failure: {$stmt->error}\n";
        }
    }

    $mysqli->commit();
}

$mysqli->close();

function determine_cycle($speed, $family) {
    $cycles = null;

    switch($family) {
        case 'DDR4':
            if ($speed >= 25600) {
                $cycles = 3200;
            } else if ($speed >= 23400) {
                $cycles = 2933;
            } else if ($speed >= 21300) {
                $cycles = 2666;
            } else if ($speed >= 19200) {
                $cycles = 2400;
            } else if ($speed >= 17000) {
                $cycles = 2133;
            } else if ($speed >= 14900) {
                $cycles = 1866;
            } else {
                $cycles = 1600;
            }
            
            break;
        case 'DDR3':
        case 'DDR3L':
            if ($speed >= 17000) {
                $cycles = 2133;
            } else if ($speed >= 14900) {
                $cycles = 1866;
            } else if ($speed >= 12800) {
                $cycles = 1600;
            } else if ($speed >= 10600) {
                $cycles = 1333;
            } else if ($speed >= 8500) {
                $cycles = 1066;
            } else {
                $cycles = 800;
            }
            
            break;
        case 'DDR2':
            if ($speed >= 8500) {
                $cycles = 1066;
            } else if ($speed > 6400) {
                $cycles = 800;
            } else if ($speed >= 5300) {
                $cycles = 666;
            } else if ($speed >= 4200) {
                $cycles = 533;
            } else {
                $cycles = 400;
            }
            
            break;
        case 'DDR':
            if ($speed >= 3200) {
                $cycles = 400;
            } else if ($speed >= 2000) {
                $cycles = 333;
            } else if ($speed >= 2100) {
                $cycles = 266;
            } else {
                $cycles = 200;
            }
            
            break;
    }

    return $cycles;
}

?>
