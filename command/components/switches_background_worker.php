<?php
require_once __DIR__.'/../database.php';

$db = new db();
$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->autocommit(false);
echo "Acquiring items from inventory...\n";
$category_name_query = "SELECT * FROM `inventory` WHERE `cat` = 'Network Switch'";
$category_name_result = $conn->query($category_name_query);

if ($category_name_result && $category_name_result->num_rows > 0){
    while ($category_searcher = $category_name_result->fetch_object()) {
        
        $sku = null;
        $qty = null;
        $con = null;
        $oem = null;
        $switchmodel = null;
        $port = null;
        $license = null;
        $features = null;
        $dualsingle = null;
        $airflow = null;
        $noedr = null;

        $port1 = null;
        $port2 = null;
        $dualsingle1 = null;
        $dualsingle2 = null;
        $sw1 =null;
        $sw2 =null;

        $qty = ($category_searcher->qty);
        $con = ($category_searcher->con);
        $switchmodel= ($category_searcher->par);
        $oem = ($category_searcher->man);

        $separate_des = explode(' ', $category_searcher->des);
        $countdes = count($separate_des);
        
    
                for ($x = 0; $x < $countdes; $x++) {
                    if (preg_match('/2|4|5|8|10|16|20|24|28|48|52/',$separate_des[$x])&&(preg_match('/port|Ports|Port|P/',$separate_des[$x])||preg_match('/x/',$separate_des[$x]))) {
                        $port1 = ($separate_des[$x]);
                    }
                
                    else if (preg_match('/1-9|4|5|8|10|16|20|24|28|48|52|100|1000/',$separate_des[$x]) && (preg_match_all('/Gb|gb|GbE|Gbps|G|GBE/',$separate_des[$x]) || preg_match_all('/Mbps|Mb/',$separate_des[$x]))) {
                        $port2 = ($separate_des[$x]);
                    }
                
                    else if (preg_match('/Dual|DUAL/',$separate_des[$x])|| preg_match('/Single|SINGLE/',$separate_des[$x])){
                        $dualsingle1 = ($separate_des[$x]);
                    }
                
                    else if (preg_match('/100|200|750|501|1000|1200/',$separate_des[$x]) && preg_match('/W|w/',$separate_des[$x])){
                        $dualsingle2 = ($separate_des[$x]);
                    }
                    else if (preg_match('/Rear|Front|rear|front/',$separate_des[$x]) || preg_match('/R-F|F-R/',$separate_des[$x]) || preg_match('/R-to-F|F-to-R|F-To-R|R-To-F/',$separate_des[$x])) {
                        $xair = $x;
                        $xair1 = $xair + 1;
                        if ($xair1 < $countdes){
                        $air1 = $separate_des[$xair1];}
                        $xair2 = $xair1 + 1;
                        if ($xair2 < $countdes){
                        $air2 = $separate_des[$xair2];}
                        $airflow = $separate_des[$x].' '.$air1.' '.$air2;
                    }
                    else if (preg_match("/Switch/",$separate_des[$x])){
                        $xsw = $x;
                        if ($xsw <= $countdes){
                        $xsw1 = $xsw - 1;
                        $sw1 = $separate_des[$xsw1];}
                        $xsw2 = $xsw1 - 1;
                        if ($xsw2 >= 0){ 
                        $sw2 = $separate_des[$xsw2];}
                        $features = $sw1.' '.$sw2.' '.($separate_des[$x]);
                    }
                    else if (preg_match('/no|NO|No/',$separate_des[$x]) && (preg_match('/rack|Rack|ears|Ears/',$separate_des[$x]) || preg_match('/rails|Rails/',$separate_des[$x]))){
                        $noedr = ($separate_des[$x]);
                    }
                
                    $port = $port1.' '.$port2;
                    $dualsingle = $dualsingle1.$dualsingle2;
                    if (($category_searcher->con) == 'NEW' || ($category_searcher->con) == 'New' || ($category_searcher->con) == 'new'){
                        $sku = ($category_searcher->par).' '.$noedr.' '.($category_searcher->con);
                    }else{
                    $sku = ($category_searcher->par).' '.$noedr;
                    }
                }
    
            if(!isset($condition)){
                $skucheck = $conn->prepare("SELECT switchid FROM switches WHERE sku = ?");
                $skucheck->bind_param('s', $sku);
            }       
            if ($skucheck === false || !$skucheck->execute()) {
                echo "An unexpected error occurred while checking if sku $sku exists.\n";
                echo "{$mysqli->error}\n";
                continue;
            }
                $resultcheck = $skucheck->get_result();


            if($resultcheck && $resultcheck->num_rows > 0){
                echo "Updating items from inventory...\n";
                    $stmt = $conn->prepare("UPDATE switches SET sku=?, qty=?, con=?, oemman=?, switchmodel=?, port=?, license=?, features=?, dualsingle=?, airflow=?, noedr=? WHERE switchmodel = ?");

                    $stmt->bind_param('sissssssssss', $sku, $qty, $con, $oem, $switchmodel, $port, $license, $features, $dualsingle, $airflow, $noedr, $switchmodel);
                
            }else{
                echo "Inserting new items from inventory...\n";
                $stmt = $conn->prepare("INSERT INTO switches (sku, qty, con, oemman, switchmodel ,port, license, features, dualsingle, airflow, noedr)
                                        VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            
                $stmt->bind_param('sisssssssss', $sku, $qty, $con, $oem, $switchmodel, $port, $license, $features, $dualsingle, $airflow, $noedr);
                }

    
        if ($stmt->execute()) {
                                "Successful\n";
                            } else {
                                echo "Error: {$stmt->error}\n";
                            }
    }
    $conn->commit();  
    
}
$conn->close();

?>
