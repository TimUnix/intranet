<?php
require_once __DIR__.'/../database.php';

$db = new db();
$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->autocommit(false);
$category_name_query = "SELECT * FROM `inventory` WHERE `cat`='motherboard' and `arc` = 'false'";
$category_name_result = $conn->query($category_name_query);

if ($category_name_result && $category_name_result->num_rows > 0){
	while ($category_searcher = $category_name_result->fetch_object()) {

		$model = null;
		$brand = null;
		$rev_version = null;
		$qty_cpu = null;
		$memory = null;
		$shield = null;
		$heatsink = null;
		$ob_nic = null;
		$pci_count = null;

		$separate_model = explode(' ', $category_searcher->des);
		$brand = strtoupper($category_searcher->man);

		if(strtolower($separate_model[0]) == strtolower($brand)){
			$model = $separate_model[1];
			$model = str_replace(',', '', $model);
		}
		else if(strtolower($separate_model[1]) == strtolower($brand)){
			$model = $separate_model[2];
			$model = str_replace(',', '', $model);
		}else{
			$model = $separate_model[0];
			$model = str_replace(',', '', $model);
		}

		$separate_des = explode(' ', $category_searcher->des);

		foreach($separate_des as $item){

         	if(preg_match('/([Rr][Ee][Vv][0-9.]+[0-9]{1,2})/', $item) === 1 ||preg_match('/[Vv][1-9.]+[1-9]{1,2}/', $item) === 1 || preg_match('/[Vv][1-9.]+[1-9]{1,2}[a-zA-z]/', $item) === 1 || preg_match('/^[(][Vv][Ee][Rr]\s[0-9]+[.0-9]{1,2}[)]$/', $item) === 1 || preg_match('/^[0-9.]+[0-9]{2}[Rr][Ee][Vv]$/', $item) === 1){
         		$rev_version = $item;
         	}
         	if(preg_match('/^[Rr][Ee][Vv]$/', $item) === 1 || preg_match('/^[Rr][Ee][Vv][.:]$/', $item) === 1){
         		$rev = $item;
         		$separate_rev = explode(' ', $category_searcher->des);
         		foreach($separate_rev as $sep_rev){
         			if(preg_match('/^[0-9.]+[0-9]{1,2}$/', $sep_rev) === 1 || preg_match('/^[0-9.]+[0-9]{1,2}[a-zA-z]$/', $sep_rev) === 1){
         			$second = $sep_rev;

         			if($rev == null && !isset($rev)){
         				$rev_version = null;
         			}
         			else if(isset($rev) && $rev !==null &&isset($second)){
         				$rev_version = $rev . ' ' . $second;
         			}
         		} 
         		}
         	}
         	if(preg_match('/^[(][Vv][Ee][Rr]$/', $item) ===1){
         		$ver = $item;
         		$ver = str_replace('(', '', $ver);
         		$separate_ver = explode(' ', $category_searcher->des);
         		foreach($separate_ver as $sep_ver){
         			if(preg_match('/^[0-9.]+[0-9]{1,2}/', $sep_ver) === 1){
         				$sec = $sep_ver;
         				$sec = str_replace(')', '', $sec);
         				if($ver == null && !isset($ver)){
         					$rev_version = null;
         				}
         				else if(isset($ver) && $ver !== null && isset($sec)){
         					$rev_version = $ver . ' ' . $sec;
         				}
         			}
         		}
         	}


			if(preg_match('/[Ee][1-9][-]/', $item) === 1 || preg_match('/[Ee][1-9][-][0-9]{4}[Vv][1-9]/', $item) === 1 || preg_match('/[Ee][0-9]{4}/', $item) === 1 || preg_match('/[Ii][3]/', $item) == 1 || preg_match('/[Ii][5]/', $item) === 1 || preg_match('/[Ii][7]/', $item) === 1 || preg_match('/[Ii][9]/', $item) === 1 || preg_match('/[Gg][6]/', $item) === 1 || preg_match('/[Gg][9]/', $item) === 1 || preg_match('/^[Cc][0-9]{3}$/', $item) === 1 || preg_match('/^[Pp][4]$/', $item) === 1 || preg_match('/^[Rr][7]$/', $item) === 1 || preg_match('/^[H][0-9]{2}$/', $item) === 1 || preg_match('/[QqXxWw][0-9]{4}/', $item) === 1 || preg_match('/[Ww][0-9]{4}[Vv][0-9]/', $item) === 1){
				$qty_cpu = trim($item);
				$qty_cpu = str_replace(',', '', $qty_cpu);
			}

         	if (preg_match('/[0-9]{1,3}[Gg][Bb]$/', $item) === 1 || preg_match('/[0-9]{1,3}[Gg][Bb][,]$/', $item) === 1){
            	$memory = $item;
         	}

         	if(preg_match('/[Dd][Dd][Rr]/', $item) === 1){
         		$memory_family = $item;
         		$memory_family = trim($item, ',');
         	}

         	if(strpos(strtolower($category_searcher->des), 'no i/o') !== false){
         		$shield = null;
         	}
         	else if(preg_match('/^[Ii][\/][Oo]$/', $item) === 1 || preg_match('/^[Ii][Oo]$/', $item) === 1 || preg_match('/^[Ww\/][Ii][Oo]$/', $item) === 1){
         		$shield = 'I/O Shield';
         	}

         	if(preg_match('/[0-9][Uu][Hh][Ss]/', $item) === 1){
         		$heatsink = $item;
         	}
		}

		if(preg_match('/^[Xx][9][Dd][Rr][Ii][-][Ll][Nn][4][Ff]/', $model) === 1){

			if($rev_version !==null && isset($rev_version) && $qty_cpu !== null && isset($qty_cpu) && $memory !== null && isset($memory) && $heatsink !== null && isset($heatsink)){
				$sku =  $model . '-' . $rev_version . '-' . $qty_cpu . '-' . $memory . '-' . $heatsink;
				$barcode = $sku;
			
			}
			else if($rev_version == null && !isset($rev_version) && $qty_cpu == null && !isset($qty_cpu) && $memory == null && !isset($memory) && $heatsink == null && !isset($heatsink)){
				$sku =  $model;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version) && $qty_cpu == null && !isset($qty_cpu) && $memory == null && !isset($memory)){
				$sku =  $model . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version) && $qty_cpu == null && !isset($qty_cpu) && $heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $memory;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version) && $memory == null && !isset($memory) && $heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $qty_cpu;
				$barcode = $sku;
			}
			else if($qty_cpu == null && !isset($qty_cpu) && $memory == null && !isset($memory) && $heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $rev_version;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version) && $qty_cpu == null && !isset($qty_cpu)){
				$sku =  $model . '-' . '-' . $memory . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version) && $memory == null && !isset($memory)){
				$sku =  $model . '-' . '-' . $qty_cpu . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version) && $heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $qty_cpu . '-' . $memory;
				$barcode = $sku;
			}
			else if($qty_cpu == null && !isset($qty_cpu) && $memory == null && !isset($memory)){
				$sku =  $model . '-' . $rev_version . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($qty_cpu == null && !isset($qty_cpu) && $heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $rev_version . '-' . $memory;
				$barcode = $sku;
			}
			else if($memory == '0' && !isset($memory) && $heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $rev_version . '-' . $qty_cpu;
				$barcode = $sku;
			}
			else if($rev_version == null && !isset($rev_version)){
				$sku =  $model . '-' . $qty_cpu . '-' . $memory . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($qty_cpu == null && !isset($qty_cpu)){
				$sku =  $model . '-' . $rev_version . '-' . $memory . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($memory == null && !isset($memory)){
				$sku =  $model . '-' . $rev_version . '-' . $qty_cpu . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($heatsink == null && !isset($heatsink)){
				$sku =  $model . '-' . $rev_version . '-' . $qty_cpu . '-' . $memory;
				$barcode = $sku;
			}
			
		}else{
			// this should be in order
			if($qty_cpu !== null && isset($qty_cpu) && $memory !== null && isset($memory) && $heatsink !== null && isset($heatsink)){
			$sku = $model . '-' . $qty_cpu . '-' . $memory . '-' . $heatsink;
			$barcode = $sku;
			
			}
			else if($qty_cpu == null && !isset($qty_cpu) && $memory == null && !isset($memory) && $heatsink == null && !isset($heatsink)){
				$sku = $model;
				$barcode = $sku;
			}

			else if($qty_cpu == null && !isset($qty_cpu) && $memory == null && !isset($memory)){
				$sku = $model . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($qty_cpu == null && !isset($qty_cpu) && $heatsink == null && !isset($heatsink)){
				$sku = $model . '-' . $memory;
				$barcode = $sku;
			}
			else if($memory == null && !isset($memory) && $heatsink == null && !isset($heatsink)){
				$sku = $model . '-' . $qty_cpu;
				$barcode = $sku;
			}

			else if($qty_cpu == null && !isset($qty_cpu)){
				$sku = $model . '-' . '-' . $memory . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($memory == null && !isset($memory)){
				$sku = $model . '-' . $qty_cpu . '-' . $heatsink;
				$barcode = $sku;
			}
			else if($heatsink == null && !isset($heatsink)){
				$sku = $model . '-' . $qty_cpu . '-' . $memory;
				$barcode = $sku;
			}	
		}

		$motherboard_check_stmt = $conn->prepare("SELECT id from motherboard where sku=?");
		$motherboard_check_stmt->bind_param('s', $sku);
		$motherboard_check_stmt->execute();
		$motherboard_check = $motherboard_check_stmt->get_result();

		if($motherboard_check && $motherboard_check->num_rows > 0){
			$stmt = $conn->prepare("UPDATE motherboard SET barcode = ?, brand = ?, model = ?, rev_version = ?, qty_cpu = ?, memory = ?, memory_family = ?, shield = ?, heatsink = ?, ob_nic = ?, pci_count = ? WHERE sku = ?");

			$stmt->bind_param('sssssdssssss', $barcode, $brand, $model, $rev_version, $qty_cpu, $memory,
                          $memory_family, $shield, $heatsink, $ob_nic, $pci_count, $sku);
		}
		else{
			$stmt = $conn->prepare("INSERT INTO motherboard (sku, barcode, brand, model, rev_version, qty_cpu, memory, 
                                      memory_family, shield, heatsink, ob_nic, pci_count)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    
       		$stmt->bind_param('ssssssdsssss', $sku, $barcode, $brand, $model, $rev_version, $qty_cpu, $memory,
                          $memory_family, $shield, $heatsink, $ob_nic, $pci_count);
		}
		

       if ($stmt->execute()) {
                            echo "Successful\n";
                        } else {
                            echo "Error: {$stmt->error}\n";
                        }
	}

$conn->commit();

}

$conn->close();
?>
