<?php
require_once __DIR__.'/../database.php';

$db = new db();
// Create connection
$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$cpu_sql = "SELECT * from inventory where cat = 'cpu' and arc = 'False'";
$cpu_result = $conn->query($cpu_sql);

if ($cpu_result->num_rows > 0) {
  
  // output data of each row
  while($cpu_row = $cpu_result->fetch_assoc()) { /// get cpu cat
    $par = $cpu_row["par"];
    $bar_code = $cpu_row['bar'];

    $part_number = $par;
    $step_code = $bar_code;


    $des = $cpu_row['des'];


    $condition = $cpu_row['con'];

    $ia_sql = "SELECT * from intel_ark where proc_name = '$par'";
    $ia_result = $conn->query($ia_sql);
    
        if ($ia_result->num_rows > 0) {   /// get intel ark proc name = inventory.par
            while($ia_row = $ia_result->fetch_assoc()) {
                $part = $par;
                $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                $stepcode = $step_code;
                $speed = $ia_row['speed_base'];
                $cores = $ia_row['num_cores'];
                $code_name = $ia_row['code_name'];
                $tdp = $ia_row['tdp'];
                $cache = $ia_row['cache'];
                $socket_support = $ia_row['socket'];
                $grade = $condition;
    
                //echo 1;
                insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                //insert
            }
            

        }else if($ia_result->num_rows == 0){
            $ia_step_code_sql = "SELECT * from intel_ark where step_code = '$step_code'";
            $ia_step_code_result = $conn->query($ia_step_code_sql);

                if ($ia_step_code_result->num_rows > 0) {
                    while($ia_step_code_row = $ia_step_code_result->fetch_assoc()) {  /// get intel ark step code = inventory.bar
                        $part = $par;
                        $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                        $stepcode = $step_code;
                        $speed = $ia_step_code_row['speed_base'];
                        $cores = $ia_step_code_row['num_cores'];
                        $code_name = $ia_step_code_row['code_name'];
                        $tdp = $ia_step_code_row['tdp'];
                        $cache = $ia_step_code_row['cache'];
                        $socket_support = $ia_step_code_row['socket'];
                        $grade = $condition;
        
                        //echo 2;
                        insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                    //insert
                    }
        
                }else{
                    $ia_step_code_like_sql = "SELECT * from intel_ark where step_code like '%$step_code%'";
                    $ia_step_code_like_result = $conn->query($ia_step_code_like_sql);


                        if ($ia_step_code_like_result->num_rows > 0) {
                            while($ia_step_code_like_row = $ia_step_code_like_result->fetch_assoc()) {   /// get intel ark step code like inentory.bar
                                $part = $par;
                                $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                                $stepcode = $step_code;
                                $speed = $ia_step_code_like_row['speed_base'];
                                $cores = $ia_step_code_like_row['num_cores'];
                                $code_name = $ia_step_code_like_row['code_name'];
                                $tdp = $ia_step_code_like_row['tdp'];
                                $cache = $ia_step_code_like_row['cache'];
                                $socket_support =$ia_step_code_like_row['socket'];
                                $grade = $condition;
                    
                                //echo 3;
                                insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                                //insert
                            }
                            
                
                        }else{
                            $bar_number_give = $bar_code;
                            $split_bar = str_split($bar_number_give);

                            $first = $split_bar[0];
                            $second = $split_bar[1];
                            $third = $split_bar[2];


                            $combine_number = "$first$second$third";


                            $ia_step_code_like_wise_sql = "SELECT * from intel_ark where step_code like '%$combine_number%'";
                            $ia_step_code_like_wise_result = $conn->query($ia_step_code_like_wise_sql);


                            if ($ia_step_code_like_wise_result->num_rows > 0) {
                                while($ia_step_code_like_wise_row = $ia_step_code_like_wise_result->fetch_assoc()) {   /// get intel ark like inventory.bar
                                    $part = $par;
                                    $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                                    $stepcode = $step_code;
                                    $speed = $ia_step_code_like_wise_row['speed_base'];
                                    $cores = $ia_step_code_like_wise_row['num_cores'];
                                    $code_name = $ia_step_code_like_wise_row['code_name'];
                                    $tdp = $ia_step_code_like_wise_row['tdp'];
                                    $cache = $ia_step_code_like_wise_row['cache'];
                                    $socket_support =$ia_step_code_like_wise_row['socket'];
                                    $grade = $condition;
                        
                        //echo 4;
                        insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                                    //insert
                                }
                                
                    
                            }else{
                                $par_number_give = $par;
                                $split_par = str_split($par_number_give);

                                $first = $split_par[0];
                                $second = $split_par[1];
                                $third = $split_par[2];
                                $fourth = $split_par[3];


                                $combine_number = "$first$second$third$fourth";


                                $ia_step_code_like_wise_sql = "SELECT * from intel_ark where proc_name like '%$combine_number%'";
                                $ia_step_code_like_wise_result = $conn->query($ia_step_code_like_wise_sql);


                                if ($ia_step_code_like_wise_result->num_rows > 0) {
                                    while($ia_step_code_like_wise_row = $ia_step_code_like_wise_result->fetch_assoc()) {   /// get intel ark proc name like inventory.par
                                        $part = $par;
                                        $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                                        $stepcode = $step_code;
                                        $speed = $ia_step_code_like_wise_row['speed_base'];
                                        $cores = $ia_step_code_like_wise_row['num_cores'];
                                        $code_name = $ia_step_code_like_wise_row['code_name'];
                                        $tdp = $ia_step_code_like_wise_row['tdp'];
                                        $cache = $ia_step_code_like_wise_row['cache'];
                                        $socket_support =$ia_step_code_like_wise_row['socket'];
                                        $grade = $condition;
                            
                                        //echo 5;
                                        insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                                        //insert
                                    }
                                
                    
                            }else{




                                $des_number_give = $des;
                                $split_des = str_split($des_number_give);

                                $first = $split_des[0];
                                $second = $split_des[1];
                                $third = $split_des[2];
                                $fourth = $split_des[3];


                                $combine_number_des = "$first$second$third$fourth";


                                $ia_step_code_like_wise_sql = "SELECT * from intel_ark where proc_name like '%$combine_number_des%'";
                                $ia_step_code_like_wise_result = $conn->query($ia_step_code_like_wise_sql);


                                if ($ia_step_code_like_wise_result->num_rows > 0) {
                                    while($ia_step_code_like_wise_row = $ia_step_code_like_wise_result->fetch_assoc()) {   /// get intel ark proc name like inventory.par
                                        $part = $par;
                                        $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                                        $stepcode = $step_code;
                                        $speed = $ia_step_code_like_wise_row['speed_base'];
                                        $cores = $ia_step_code_like_wise_row['num_cores'];
                                        $code_name = $ia_step_code_like_wise_row['code_name'];
                                        $tdp = $ia_step_code_like_wise_row['tdp'];
                                        $cache = $ia_step_code_like_wise_row['cache'];
                                        $socket_support =$ia_step_code_like_wise_row['socket'];
                                        $grade = $condition;
                            
                                        //echo 6;
                                        insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                                        //insert
                                    }
                                
                    
                            }else{
                                $par_sql = "SELECT * from intel_ark where proc_name = '$par'";
                                $par_result = $conn->query($par_sql);


                                if ($par_result->num_rows > 0) {
                                    while($par_row = $par_result->fetch_assoc()) {   /// get intel ark proc name like inventory.par
                                        $part = $par;
                                        $title = $par." ".$bar_code." ". $ia_row['speed_base'] ." ".$ia_row['num_cores'];
                                        $stepcode = $step_code;
                                        $speed = $par_row['speed_base'];
                                        $cores = $par_row['num_cores'];
                                        $code_name = $par_row['code_name'];
                                        $tdp = $par_row['tdp'];
                                        $cache = $par_row['cache'];
                                        $socket_support =$par_row['socket'];
                                        $grade = $condition;
                            
                                        //echo 7;
                                        insertdata($part, $stepcode, $speed, $cores, $code_name, $tdp, $cache, $socket_support, $grade);
                                        //insert
                                    }
                                
                    
                            }else{
                                //
                                
                            }
                            }
                                ///
                            }
                                ///
                            }

                        }
                }

        }

    

  }
  
} else {
  echo "0 results";
}


function insertdata($part, $stepcode, $speed, $number_of_cores, $codename, $thermal_design_power, $cpu_cache, $sockets_supported, $grade){
    $servername = "localhost";
    $username = "root";
    $password = "3060Unix!";
    $dbname = "linn";


    try {
        // Create connection
    $insert_conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($insert_conn->connect_error) {
    die("Connection failed: " . $insert_conn->connect_error);
    }



    $check_sql = "SELECT * from cpu_new where part = '$part' and stepcode = '$stepcode'";
    $check_result = $insert_conn->query($check_sql);


    if ($check_result->num_rows < 1) {
    $sql = "INSERT INTO cpu_new (part, stepcode, speed, number_of_cores, codename, thermal_design_power, cpu_cache, sockets_supported, grade)
            VALUES ('$part', '$stepcode', '$speed', '$number_of_cores', '$codename', '$thermal_design_power', '$cpu_cache', '$sockets_supported', '$grade')";

    if ($insert_conn->query($sql) === TRUE) {
    ///successfully inserted
    } else {
    ///error in inserting
    }
    $insert_conn->close();             
                    
                            }else if($check_result->num_rows > 0){
                                $sqludate = "UPDATE cpu_new SET stepcode = '$stepcode', speed = '$speed', number_of_cores = '$number_of_cores', codename = '$codename', thermal_design_power = '$thermal_design_power', cpu_cache = '$cpu_cache', sockets_supported = '$sockets_supported', grade = '$grade' WHERE part='$part'";
                                if ($insert_conn->query($sqludate) === TRUE) {
                                    ///successfully updated
                                } else {
                                    ///error in updating
                                }
                            }
      } catch(Exception $e) {
        echo $e;
      }
   
}
$conn->close();
?>
