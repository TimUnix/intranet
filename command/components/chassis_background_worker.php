<?php
require_once __DIR__.'/../database.php';

$db = new db();
$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->autocommit(false);

$category_name_query = "SELECT * FROM `inventory` WHERE `cat` = 'server' and `arc` = 'false'";
$category_name_result = $conn->query($category_name_query);

$chassis_name_query = "SELECT * FROM `chassis_work_orders`";
$chassis_name_result = $conn->query($chassis_name_query);

if ($category_name_result && $category_name_result->num_rows > 0){
  while ($category_searcher = $category_name_result->fetch_object()) {
  
    $manufacturer = null;
    $u = null;
    $model = null;
    $dff = null;
    $rff = null;
    $psu_slots = null;
    $motherboard = null;
    $bin = null;
    $nodes = null;
    $backplane = null;
    $drivebays = null;
    $rearbays = null;
    $controller = null;
    $mb_ff = null;

    $part_number = $category_searcher->par;

    $manufacturer = strtoupper($category_searcher->man);
    $bin = $category_searcher->bin;
    $model = $category_searcher->bar;
    var_dump($part_number);
    $separate_des = explode(' ', $category_searcher->des);

    $separate_par = str_split($category_searcher->par);
    if($separate_par[0] == '1' || $separate_par[0] == '2' || $separate_par[0] == '3' || $separate_par[0] == '4'){
      if($separate_par[1] == 'U' || $separate_par[1] == 'u'){
        $u = $separate_par[0].''.$separate_par[1];
      }
    }

    $separate_par_preg = explode(' ', $category_searcher->par);
    foreach($separate_par_preg as $find_u){
      if(preg_match('/[1][Uu]/', $find_u) === 1){
        $u = '1U';
        var_dump($u);
      }else if(preg_match('/[2][Uu]/', $find_u) === 1){
        $u = '2U';
        var_dump($u);
      }
      else if(preg_match('/[3][Uu]/', $find_u) === 1){
        $u = '3U';
        var_dump($u);
      }else if(preg_match('/[4][Uu]/', $find_u) === 1){
        $u = '4U';
        var_dump($u);
      }
    }
    

    if(strpos(strtoupper($category_searcher->man), 'ASUS') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'ASUS') !== false && $category_searcher->bar == ''){
            $motherboard = $category_searcher->par;          
    }
    else if(strpos(strtoupper($category_searcher->man), 'DELL') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'DELL') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'EMC') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'EMC') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'GIGABYTE') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'GIGABYTE') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'HP') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'HP') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'IBM') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'IBM') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'INSPUR') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'INSPUR') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'INTEL') !== false && $category_searcher->bar != '' && strpos(strtolower($category_searcher->bar), 'server') !== false){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'INTEL') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'INTEL') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'LANTRONIX') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'LANTRONIX') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'NETAPP') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'NETAPP') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }else if(strpos(strtoupper($category_searcher->man), 'LENOVO') !== false && $category_searcher->bar != '' && strpos(strtolower($category_searcher->bar), '1u') !== false){
              $explode_lenovo_par = explode(' ', $category_searcher->par);
              $motherboard = $explode_lenovo_par[0];
    }else if(strpos(strtoupper($category_searcher->man), 'LENOVO') !== false && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar;
    }else if(strpos(strtoupper($category_searcher->man), 'LENOVO') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par;
    }
    else if(strpos(strtoupper($category_searcher->man), 'OEM') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'OEM') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'OPEN GEAR') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'OPEN GEAR') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'ORACLE') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'ORACLE') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'PROMISE') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'PROMISE') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'QUANTA') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'QUANTA') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'SUPERMICRO') !== false && $category_searcher->bar != '' && strpos(strtolower($category_searcher->bar), 'ucs') !== false){
              $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'SUPERMICRO') !== false && $category_searcher->bar != '' && strpos(strtolower($category_searcher->bar), 'tyan') !== false){
              $explode_supermicro_bar_tyan = explode(' ', $category_searcher->bar);
              $motherboard = $explode_supermicro_bar_tyan[1];
    }
    else if(strpos(strtoupper($category_searcher->man), 'SUPERMICRO') !== false && $category_searcher->bar != ''){
              $explode_supermicro_bar = explode(' ', $category_searcher->bar);
              $motherboard = $explode_supermicro_bar[0];
    }else if(strpos(strtoupper($category_searcher->man), 'SUPERMICRO') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'TYAN') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'TYAN') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if(strpos(strtoupper($category_searcher->man), 'WIWYNN') !== false && $category_searcher->bar != ''){
          $motherboard = $category_searcher->bar;
    }
    else if(strpos(strtoupper($category_searcher->man), 'WIWYNN') !== false && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par; 
    }
    else if($category_searcher->man == '' && $category_searcher->bar != ''){
              $motherboard = $category_searcher->bar;
    }
    else if($category_searcher->man == '' && $category_searcher->bar == ''){
              $motherboard = $category_searcher->par;
    }else{
              $motherboard = $category_searcher->bar;
    }

    if(strpos(strtoupper($category_searcher->par), '2U-CSE-827HQ+-R2K04B CHASSIS') !== false || strpos(strtoupper($category_searcher->par), '2U-2N-X10DRT-P-12BLS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-H-4N-12BL-ADPX9-6SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-HF+-4N-12BL-ADP-4SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-H-2N-12BL-ADPX9-6SATA') !== false || strpos(strtoupper($category_searcher->par), 'QT41-24S-4NODE-16M') !== false || strpos(strtoupper($category_searcher->par), 'SYS-5018D-MTRF') !== false || strpos(strtoupper($category_searcher->par), '3U-X10SRD-F-8N-16BL-SATA3-1X10GSFP-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-24BS-SAS2-H6IR-2PS') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U4N-24BS-SATA-V3-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U4N-12LSATA-3647-2PS NEW') !== false){
      $mb_ff = 'MULTI-NODE';
    }else if(strpos(strtoupper($category_searcher->par), '1U-X10SLM-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRH-CT-12BLS3') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRH-7F-12BLS2E') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-SM-CSE-512-260-LFF') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRD-LF-2BL-TQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRD-INT-4BL-TQ-2XNVME-2x10GBASET-2P') !== false || strpos(strtoupper($category_searcher->par), '4U-NOMOTHERBOARD-36BL-SAS3-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRD-LT-2BL-TQ-2x10GBASET-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRD-IF-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-S7076-GM2NR-4BL') !== false){
      $mb_ff = 'EATX';
    }else if(strpos(strtoupper($category_searcher->par), '2U-X9DRI-LN4F+1.2-12BLIP') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DR3-LN4F+-8BLTQ') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-2N-12BL-SAS2-L6I') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-12BL-SAS2-L6I') !== false || strpos(strtoupper($category_searcher->par), '3U-X9DR3-LN4F+-16BLIP') !== false || strpos(strtoupper($category_searcher->par), '3U-X9DRI-LN4F+-16BLTQ-1.1') !== false || strpos(strtoupper($category_searcher->par), '4U-X10DRI-T4+-36BLS3') !== false || strpos(strtoupper($category_searcher->par), 'CSE-414-O48L-NONE-1620') !== false || strpos(strtoupper($category_searcher->par), 'NEW-1U-512L-200B-CHASSIS-2BL') !== false || strpos(strtoupper($category_searcher->par), 'E-SUPERCHASSIS CSE-732D4-865B') !== false || strpos(strtoupper($category_searcher->par), '4U-X9DRI-LN4F+1.2-36BLSAS2') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.2-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.1-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRI-F-12BL-S2-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRI-LN4F+-24BS-IP-2PS') !== false){
      $mb_ff = 'EEATX';
    }else if(strpos(strtoupper($category_searcher->par), '1U-X10DRL-IT-12BL-TQ-2X10BASET-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRL-12BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCI-LN4F-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCL-F-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCM-F-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '03-07-2021-A') !== false){
      $mb_ff = 'ATX';
    }else if(strpos(strtoupper($category_searcher->par), '1U-X10SLM-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), 'X10SLH-N6-ST031') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLH-N6-ST031-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLM-F-E3-1240V3') !== false){
      $mb_ff = 'MICRO ATX';
    }else if(strpos(strtoupper($category_searcher->par), '1U-X9DRW-7TPF-4BLTQ') !== false){
      $mb_ff = 'SMC WIO';
    }else{
      $mb_ff = 'PROPRIETARY';
    }

    if(strpos(strtoupper($category_searcher->par), '4U-X10DRFR-8N-48BS-SATA3-4PS') !== false || strpos(strtoupper($category_searcher->par), '4U-X9DRFR-4N-32BL-SATA-4PS') !== false){
        $psu_slots = '4';
    }else if(strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLM-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRFF-7TG+-12BL') !== false || strpos(strtoupper($category_searcher->par), 'K-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-1XE3-1240V2-H200') !== false || strpos(strtoupper($category_searcher->par), 'R930-E7-8890V3-24SFF') !== false || strpos(strtoupper($category_searcher->par), '1U-R310-4BLS-PERC6I') !== false || strpos(strtoupper($category_searcher->par), '2U-R720XD-24BSS-H710') !== false || strpos(strtoupper($category_searcher->par), 'QS2PH-D16-1ULH USED') !== false || strpos(strtoupper($category_searcher->par), 'QS12-10S-12M') !== false || strpos(strtoupper($category_searcher->par), 'QS2B-H-D12L-24-1200W') !== false || strpos(strtoupper($category_searcher->par), 'QS4L-A-4U12S-96M') !== false || strpos(strtoupper($category_searcher->par), 'QS2P-D-14L-S21J') !== false || strpos(strtoupper($category_searcher->par), 'QS5B-C-D12S-24M') !== false || strpos(strtoupper($category_searcher->par), 'NEW-1U-512L-200B-CHASSIS-2BL') !== false || strpos(strtoupper($category_searcher->par), '70QM0012UX') !== false || strpos(strtoupper($category_searcher->par), 'CSE-833T-653B-NEW') !== false || strpos(strtoupper($category_searcher->par), '1U-R220-H310') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCI-LN4F-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), 'QS2P-C-D4L-16M') !== false || strpos(strtoupper($category_searcher->par), 'QS5B-B-D8SL-24M') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.2-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.1-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), 'X10SLH-N6-ST031') !== false || strpos(strtoupper($category_searcher->par), '1U-R430-4BL-H730-1X550W-1XCPU-IDRA') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLH-N6-ST031-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R410-4BL-SATA-1PSU-NR') !== false || strpos(strtoupper($category_searcher->par), 'K-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCL-F-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCM-F-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRD-IF-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-S7076-GM2NR-4BL') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRT-F-2N-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-8BSS-H310') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SDV-TLN4F-CSE502-2X10GT') !== false || strpos(strtoupper($category_searcher->par), 'NEW-1UH-X11SDV-4C-TP8F-2BS-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R230-4BL-H730-1PS') !== false){
        $psu_slots = '1';
    }else{
      $psu_slots = '2';
    }

    if(strpos(strtoupper($category_searcher->par), '4U-JBOD-45BLS2E') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRFF-7TG+-12BL') !== false || strpos(strtoupper($category_searcher->par), '2U-PTJBOD-CB2-12BLS2') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-2N-12BL-SAS2-L6I') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-12BL-SAS2-L6I') !== false || strpos(strtoupper($category_searcher->par), '4U-NOBOARD-36BLSAS2') !== false || strpos(strtoupper($category_searcher->par), '4U-NOMOTHERBOARD-36BLSAS2') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-HEF+-4N-24BS-SAS2-H6IRP') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-1XE3-1240V2-H200') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-C4130-4X') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-C4130') !== false || strpos(strtoupper($category_searcher->par), 'POWEREDGE_T630') !== false || strpos(strtoupper($category_searcher->par), '4U-QUANTA-M4600H-JBOD-60BL-S2-2P') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRH-7F-12BLS2E') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-SM-CSE-512-260-LFF') !== false || strpos(strtoupper($category_searcher->par), 'SYS-5018D-MTRF') !== false || strpos(strtoupper($category_searcher->par), '4U-X9DRI-LN4F+1.2-36BLSAS2') !== false || strpos(strtoupper($category_searcher->par), '4U-DDN-SS8460-JBOD-84BL-SAS2-2PS') !== false || strpos(strtoupper($category_searcher->par), '100-400-028-01') !== false || strpos(strtoupper($category_searcher->par), '1U-R620-8BSF-H710-NEW') !== false || strpos(strtoupper($category_searcher->par), '2U-R520-8BL-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-7TPF-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '2U-UCS-C240-M3-24BS-2P') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRI-F-12BL-S2-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-24BS-SAS2-H6IR-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-8BSS-H310') !== false){
        $backplane = 'SAS2';
    }else if(strpos(strtoupper($category_searcher->par), '2U-CSE-827HQ+-R2K04B CHASSIS') !== false || strpos(strtoupper($category_searcher->par), '2U-2N-X10DRT-P-12BLS') !== false || strpos(strtoupper($category_searcher->par), '4U-X10DRFR-8N-48BS-SATA3-4PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X9SRD-F-8N-16BL-SATA3-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X10SRD-F-8N-16BL-SATA3-2X10GSFP-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X10SRD-F-8N-16BL-SATA3-1X10GSFP-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCL-F-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCM-F-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRD-IF-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS-ADP-6SATA3P-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), 'NEW-1UH-X11SDV-4C-TP8F-2BS-1PS') !== false){
        $backplane = 'SATA3';
    }else if(strpos(strtoupper($category_searcher->par), '1U-X9DRW-7TPF-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-IF-4BLTQ-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-3LN4F+-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLM-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRW-I-10BSTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X8DTU-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-3LN4F+-8BSTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SPU-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DDW-I-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRL-IT-12BL-TQ-2X10BASET-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLM-F-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), '2U-H8QG7-LN4F-6BLTQ-4XAMD6212') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DR3-LN4F+-8BLTQ') !== false || strpos(strtoupper($category_searcher->par), '3U-X9DRI-LN4F+-16BLTQ-1.1') !== false || strpos(strtoupper($category_searcher->par), '1U-X8DTU-F-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRW-IT-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), 'N-2U-X10DRU-I+-12BL-IP-3XPCIEX16') !== false || strpos(strtoupper($category_searcher->par), '1U-X11DDW-L-4BL-TQ-2P') !== false || strpos(strtoupper($category_searcher->par), '8205-E6D') !== false || strpos(strtoupper($category_searcher->par), 'NEW-1U-512L-200B-CHASSIS-2BL') !== false || strpos(strtoupper($category_searcher->par), 'E-SUPERCHASSIS CSE-732D4-865B') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-3TF+-8BS-TQ-2PS') !== false || strpos(strtoupper($category_searcher->par), 'A1SRM-LN5F-2358') !== false || strpos(strtoupper($category_searcher->par), 'CSE-833T-653B-NEW') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRD-LF-2BL-TQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCI-LN4F-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRD-INT-4BL-TQ-2XNVME-2X10GBASET-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-IF-8BS-TQ-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.2-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.1-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), 'X10SLH-N6-ST031') !== false || strpos(strtoupper($category_searcher->par), '1U-X11SSW-F-4BL-TQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRD-LT-2BL-TQ-2X10GBASET-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLH-N6-ST031-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLH-N6-ST031-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-S7076-GM2NR-4BL') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRT-F-2N-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-C220-M3-4BL-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R6515-8BS-H330-8C7262') !== false || strpos(strtoupper($category_searcher->par), '1U-R230-4BL-H730-1PS') !== false || strpos(strtoupper($category_searcher->par), '03-07-2021-A') !== false){
      $backplane = 'TQ';
    }else if(strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), '1U-R630-8BS-H730') !== false || strpos(strtoupper($category_searcher->par), '2U-R720XD-12BL-H710') !== false || strpos(strtoupper($category_searcher->par), '2U-R730XD-24BSS-H730') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRH-CT-12BLS3') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DSC+-24BLS3') !== false || strpos(strtoupper($category_searcher->par), '4U-X10DRI-T4+-36BLS3') !== false || strpos(strtoupper($category_searcher->par), '4U-X10QBI-24BLS3-4XE7-4850V2') !== false || strpos(strtoupper($category_searcher->par), 'D-2U-X10DRU-I+-24BSS3-AOC-S3008L-L8E') !== false || strpos(strtoupper($category_searcher->par), '1U-R430-4B-NO IDRAC') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS-S3108L-H6IRP') !== false || strpos(strtoupper($category_searcher->par), '2U-X11DPU-12BL-A3N4-S3008L-L8E-2X10GBT') !== false || strpos(strtoupper($category_searcher->par), 'R930-E7-8890V3-24SFF') !== false || strpos(strtoupper($category_searcher->par), '1U-R310-4BLS-PERC6I') !== false || strpos(strtoupper($category_searcher->par), '2U-R720XD-24BSS-H710') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-R720-6B-SFF') !== false || strpos(strtoupper($category_searcher->par), '1S2BZZZ000N NEW') !== false || strpos(strtoupper($category_searcher->par), '1U-M5A78L-FX8320') !== false || strpos(strtoupper($category_searcher->par), 'G-2U-X10DRU-I+-12BLS3-3XPCIEGPUX16') !== false || strpos(strtoupper($category_searcher->par), 'CSE-414-O48L-NONE-1620') !== false || strpos(strtoupper($category_searcher->par), '1U-X11DPU-10BS-S3A-2XNVME-2X10GSFP-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRH-CLN4-4BLTQ') !== false || strpos(strtoupper($category_searcher->par), 'R2312WTTYSR') !== false || strpos(strtoupper($category_searcher->par), '2U-X10D-CHASSIS-2N-12BL-NVME-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10D-CHASSIS-4N-24BS-2PS') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-1U-PROJECT') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-R720-12B-SFF') !== false || strpos(strtoupper($category_searcher->par), 'R1208WT2GSR') !== false || strpos(strtoupper($category_searcher->par), '2U-R730-8BLF-H730') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRU-XLL-10BS-S3-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-8B-SFF-H310') !== false || strpos(strtoupper($category_searcher->par), '4U-NOMOTHERBOARD-36BL-SAS3-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X11DDW-L-8BS-S3A-S3108M-H8-2P-NEW') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-8BS-H710') !== false || strpos(strtoupper($category_searcher->par), '2U-R720-8BL-H710') !== false || strpos(strtoupper($category_searcher->par), 'N-2U-X10DRU-I+-12BL-IP-3XPCIEX16') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), 'UXP-1U-10BSS-V3-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U-24BSS-V4-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U-12BLS-V4-2PS NEW') !== false){
      $backplane = 'SAS3';
    }else if(strpos(strtoupper($category_searcher->par), '2U-X9DRI-LN4F+1.2-12BLIP') !== false || strpos(strtoupper($category_searcher->par), '3U-X9DR3-LN4F+-16BLIP') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS-S3008L-L6IP') !== false || strpos(strtoupper($category_searcher->par), '4U-CHASSIS-X9Q-24BL-IP-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRW-ITPF+-16BS-IP-2X10GSFP-ASR71605') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRI-LN4F+-24BS-IP-2PS') !== false){
        $backplane = 'iPATH';
    }else if(strpos(strtoupper($category_searcher->par), '2U-X10DRT-H-4N-12BL-ADPX9-6SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF-4N-12BL-SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF-4N-24BS-SATA') !== false || strpos(strtoupper($category_searcher->par), 'C-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), 'K-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-HF+-4N-12BL-ADP-4SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-H-2N-12BL-ADPX9-6SATA') !== false || strpos(strtoupper($category_searcher->par), '3U-X9SCD-F-8N-16BL-SATA-1270V1') !== false || strpos(strtoupper($category_searcher->par), 'QS4L-A-4U12S-96M') !== false || strpos(strtoupper($category_searcher->par), 'QS2B-B-D10S-24-750') !== false || strpos(strtoupper($category_searcher->par), 'P9D-C/4L') !== false || strpos(strtoupper($category_searcher->par), '2U-X11DPT-PS-4N-24BS-6SATA3P-8XG5118-2P') !== false || strpos(strtoupper($category_searcher->par), 'R2208WT2YS') !== false || strpos(strtoupper($category_searcher->par), '1U-MD90-FS0-8SFF-SATA') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-1U-4XESATA') !== false || strpos(strtoupper($category_searcher->par), 'X3550 M5 4X LFF') !== false || strpos(strtoupper($category_searcher->par), 'X3550 M5 4X LFF SATA') !== false || strpos(strtoupper($category_searcher->par), '70QM0012UX') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRG-HF-4BS-SATA-3XGPUX16-2P') !== false || strpos(strtoupper($category_searcher->par), 'QS3A-A-S4L-4M-400') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRL-12BLTQ') !== false || strpos(strtoupper($category_searcher->par), '1U-R220-H310') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-F-4N-12BL-SATA-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X9SCE-F-12N-24BL-SATA-1240V2-16GB') !== false || strpos(strtoupper($category_searcher->par), '3U-S910-X31E-9N-18BL-SATA-2PSU-NR') !== false || strpos(strtoupper($category_searcher->par), '1U-R410-4BL-SATA-1PSU-NR') !== false || strpos(strtoupper($category_searcher->par), '4U-S7100-10BS-SATA-2P-NEW') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-24BS-ADPX9-6SATA-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), '1U-R240-2BL-1XE2226G-SATA-1X250W') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SDV-TLN4F-CSE502-2X10GT') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U4N-24BS-SATA-V3-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U4N-12LSATA-3647-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U-24BS-SATA-V3-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U-12BL-SATA-V3-2PS NEW') !== false){
      $backplane = 'SATA';
    }else if(strpos(strtoupper($category_searcher->par), '1U-R620-10BS-H710') !== false || strpos(strtoupper($category_searcher->par), '100-562-779') !== false || strpos(strtoupper($category_searcher->par), '1U-D51PH-1ULH-12BL') !== false || strpos(strtoupper($category_searcher->par), 'QT41-24S-4Node-16M') !== false || strpos(strtoupper($category_searcher->par), 'QS2PH-D16-1ULH USED') !== false || strpos(strtoupper($category_searcher->par), 'QS12-10S-12M') !== false || strpos(strtoupper($category_searcher->par), 'QS2B-H-D12L-24-1200W') !== false || strpos(strtoupper($category_searcher->par), 'QS2P-D-14L-S21J') !== false || strpos(strtoupper($category_searcher->par), 'DL360G9-B8S-E5-2660V3-64G-8X600G') !== false || strpos(strtoupper($category_searcher->par), 'DL360G9-B4F-E5-2603V4-2X540') !== false || strpos(strtoupper($category_searcher->par), 'DL360G9-B8S-E5-2660V3-2X800W') !== false || strpos(strtoupper($category_searcher->par), 'QS2PH-D16-1ULH') !== false || strpos(strtoupper($category_searcher->par), '2U-UCS-C240-M4-16BS-2XE5-2690V3-2X400GBSSD') !== false || strpos(strtoupper($category_searcher->par), 'QS2S-A-D4L-24M') !== false || strpos(strtoupper($category_searcher->par), 'UCSB-B200-M4-2620V3') !== false || strpos(strtoupper($category_searcher->par), 'QS5B-C-D12S-24M') !== false || strpos(strtoupper($category_searcher->par), '2U-X8QB6-F-6BL-TQ-2P') !== false || strpos(strtoupper($category_searcher->par), '2U-R720-16BSS-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-R630-10BS-H730-2PSU') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-4BL-H310') !== false || strpos(strtoupper($category_searcher->par), 'QS2P-C-D4L-16M') !== false || strpos(strtoupper($category_searcher->par), 'QS5B-B-D8SL-24M') !== false || strpos(strtoupper($category_searcher->par), '1U-R430-4BL-H730-1X550W-1XCPU-IDRAC') !== false || strpos(strtoupper($category_searcher->par), '2U-R820-8BSS-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-R610-6BS-SAS-2PSU') !== false || strpos(strtoupper($category_searcher->par), 'K-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRW-IF-16BS-IP-2PS') !== false || strpos(strtoupper($category_searcher->par), '4U-X9DRFR-4N-32BL-SATA-4PS') !== false || strpos(strtoupper($category_searcher->par), '1U-UCS-HX220C-M4S-8BS-2PS-SAS12GHBA') !== false || strpos(strtoupper($category_searcher->par), '1U-R340-4BL-SAS-2PS') !== false){
      $backplane = 'SAS';
    }

    foreach ($separate_des as $item){
      if(strpos(strtolower($item), '1.8in') !== false || strpos(strtolower($item), '2.5in') !== false || strpos(strtolower($item), '3.5in') !== false){
            $dff = str_ireplace('in', '', $item);

        }
        else if(strpos(strtolower($item), '1.8"') !== false || strpos(strtolower($item), '2.5"') !== false || strpos(strtolower($item), '3.5"') !== false){
            $dff = str_ireplace('"', '', $item);

        }
      
      if(preg_match('/^[Dd][Uu][Aa][Ll]/', $item) === 1){
          $psu_slots = '2';
      }

      if(preg_match('/^[Ss][Ii][Nn][Gg][Ll][Ee]/', $item) === 1){
          $psu_slots = '1';
      }

      if(preg_match('/^[Ss][Aa][Ss]$/', $item) === 1 || preg_match('/^[Ss][Aa][Ss][0-9]{1}$/', $item) === 1 || preg_match('/^[Ss][Aa][Ss][Ss]/', $item) === 1 || preg_match('/^[Ss][Aa][Tt][Aa]$/', $item) === 1 || preg_match('/^[Ss][Aa][Tt][Aa][0-9]{1}$/', $item) === 1 || preg_match('/^[Tt][Qq]$/', $item) === 1 || preg_match('/^[Ii][Pp][Aa][Tt][Hh]$/', $item) === 1){

        $backplane = $item;
      }
      
      if(preg_match('/^[0-9]{1,2}[Bb][Aa][Yy]$/', $item) === 1){
        $explode_bay_stick = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$item);
        $drivebays = $explode_bay_stick[0]." ".$explode_bay_stick[1];
      }
      else if(preg_match('/^[Bb][Aa][Yy]$/', $item) === 1){
        $position_bay = strpos(strtolower($category_searcher->des), 'bay');
        $get_position = substr($category_searcher->des, $position_bay-4, 7);
        if(preg_match('/^\d/', $get_position) === 1){
          $drivebays = $get_position;
        }else if(preg_match('/^[A-Za-z]/', $get_position) === 1){
          $explode_get_position = explode(' ', $get_position);
          $drivebays = $explode_get_position[1]." ".'Bay';
          
        }else if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $get_position) === 1){
          $replace_special = preg_replace('/[^A-Za-z0-9]/', '', $get_position);
          $drivebays = $replace_special;
        }
      }
    

      if(preg_match('/^[0-9]{1,2}[Nn][Oo][Dd][Ee]$/', $item) === 1){
        $explode_node_stick = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$item);
        $nodes = $explode_node_stick[0];
      }
      else if(preg_match('/^[Nn][Oo][Dd][Ee]$/', $item) === 1){
        $position_node = strpos(strtolower($category_searcher->des), 'node');
        $get_position_node = substr($category_searcher->des, $position_node-4, 7);
          if(preg_match('/^\d/', $get_position_node) === 1){
            $get_number_node = explode(' ', $get_position_node);
            $nodes = $get_number_node;
          }
      }else if(preg_match('/[-][Nn][Oo][Dd][Ee]$/', $item) === 1){
        $explode_node_item = explode('', $item);
        $nodes = $explode_node_item[0];
      }

      if(strpos(strtoupper($item), 'ATX') !== false || strpos(strtoupper($item), 'EATX') !== false || strpos(strtoupper($item), 'EEATX') !== false || strpos(strtoupper($item), 'MICRO ATX') !== false || strpos(strtoupper($item), 'MULTI-NODE') !== false || strpos(strtoupper($item), 'PROPRIETARY') !== false || strpos(strtoupper($item), 'MICROATX') !== false || strpos(strtoupper($item), 'SMC WIO') !== false){
          $mb_ff = $item;
      }

      if(strpos(strtoupper($item), 'ONBOARD') !== false){
        $controller = $item;
      }
    }

    if(strpos(strtoupper($category_searcher->par), '4U-JBOD-45BLS2E') !== false){
      $rearbays = '21 Bay';
    }else if(strpos(strtoupper($category_searcher->par), 'CHASSIS-C4130-4X') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-C4130') !== false){
      $rearbays = '2 Bay';
    }else if(strpos(strtoupper($category_searcher->par), '1U-D51PH-1ULH-12BL') !== false || strpos(strtoupper($category_searcher->par), '4U-NOMOTHERBOARD-36BL-SAS3-2PS') !== false){
      $rearbays = '12 Bay';
    }

    if(strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), '1U-R630-8BS-H730') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-IF-4BLTQ-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRW-3LN4F+-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRI-LN4F+1.2-12BLIP') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRFF-7TG+-12BL') !== false || strpos(strtoupper($category_searcher->par), 'C-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), 'K-1U-X10DRU-I+-4BLSATA') !== false || strpos(strtoupper($category_searcher->par), '1U-X8DTU-F-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-R720XD-24BSS-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-D51PH-1ULH-12BL') !== false || strpos(strtoupper($category_searcher->par), 'QT41-24S-4NODE-16M') !== false || strpos(strtoupper($category_searcher->par), 'QS2PH-D16-1ULH USED') !== false || strpos(strtoupper($category_searcher->par), 'QS2PH-D16-1ULH') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRG-HF-4BS-SATA-3XGPUX16-2P') !== false || strpos(strtoupper($category_searcher->par), '2U-R720-16BSS-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRD-LF-2BL-TQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R630-10BS-H730-2PSU') !== false || strpos(strtoupper($category_searcher->par), '1U-R220-H310') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-4BL-H310') !== false || strpos(strtoupper($category_searcher->par), '1U-X9SCI-LN4F-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10DRD-INT-4BL-TQ-2XNVME-2X10GBASET-2P') !== false || strpos(strtoupper($category_searcher->par), 'QS2P-C-D4L-16M') !== false || strpos(strtoupper($category_searcher->par), 'QS5B-B-D8SL-24M') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.2-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRI-LN4F+1.1-4BLTQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-X10SLH-N6-ST031-4BLTQ-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-S910-X31E-9N-18BL-SATA-2PSU-NR') !== false || strpos(strtoupper($category_searcher->par), '2U-R820-8BSS-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-R410-4BL-SATA-1PSU-NR') !== false || strpos(strtoupper($category_searcher->par), '1U-R610-6BS-SAS-2PSU') !== false || strpos(strtoupper($category_searcher->par), '1U-R620-8BSF-H710-NEW') !== false || strpos(strtoupper($category_searcher->par), '1U-R420-8BS-H710') !== false || strpos(strtoupper($category_searcher->par), '2U-R720-8BL-H710') !== false || strpos(strtoupper($category_searcher->par), '2U-R520-8BL-H710') !== false){
        $controller = 'ONBOARD';
    }

    if(strpos(strtoupper($category_searcher->par), '2U-CSE-827HQ+-R2K04B CHASSIS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-H-4N-12BL-ADPX9-6SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DSC+-24BLS3') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-12BL-SAS2-L6I') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF-4N-12BL-SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF-4N-24BS-SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-HF+-4N-12BL-ADP-4SATA') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS-S3008L-L6IP') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS-S3108L-H6IRP') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-HEF+-4N-24BS-SAS2-H6IRP') !== false || strpos(strtoupper($category_searcher->par), 'QT41-24S-4NODE-16M') !== false || strpos(strtoupper($category_searcher->par), 'H2224XXLR3') !== false || strpos(strtoupper($category_searcher->par), '2U-X11DPT-PS-4N-24BS-6SATA3P-8XG5118-2P') !== false || strpos(strtoupper($category_searcher->par), 'R2208WT2YS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10D-CHASSIS-4N-24BS-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X8DTT-F-4N-12BL-SATA-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-P-4N-24BS-ADP-6SATA3P-2P') !== false || strpos(strtoupper($category_searcher->par), '4U-X9DRFR-4N-32BL-SATA-4PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-24BS-ADPX9-6SATA-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-4N-24BS-SAS2-H6IR-2PS') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U4N-24BS-SATA-V3-2PS NEW') !== false || strpos(strtoupper($category_searcher->par), 'UXP-2U4N-12LSATA-3647-2PS NEW') !== false ){
        $nodes = '4';
    }else if(strpos(strtoupper($category_searcher->par), '2U-X9DRT-HF+-2N-12BL-SAS2-L6I') !== false || strpos(strtoupper($category_searcher->par), '2U-X10DRT-H-2N-12BL-ADPX9-6SATA') !== false || strpos(strtoupper($category_searcher->par), 'QS2B-G-D12L-16M') !== false || strpos(strtoupper($category_searcher->par), '2U-X10D-CHASSIS-2N-12BL-NVME-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-R820-8BSS-H710') !== false || strpos(strtoupper($category_searcher->par), '1U-X9DRT-F-2N-4BL-TQ-1PS') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false){
        $nodes = '2';
    }else if(strpos(strtoupper($category_searcher->par), '3U-X9SCD-F-8N-16BL-SATA-1270V1') !== false || strpos(strtoupper($category_searcher->par), '4U-X10DRFR-8N-48BS-SATA3-4PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X9SRD-F-8N-16BL-SATA3-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X10SRD-F-8N-16BL-SATA3-2X10GSFP-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X10SRD-F-8N-16BL-SATA3-1X10GSFP-2PS') !== false){
        $nodes = '8';
    }else if(strpos(strtoupper($category_searcher->par), '3U-X9SCE-F-12N-24BL-SATA-1240V2-16GB') !== false){
        $nodes = '12';
    }else if(strpos(strtoupper($category_searcher->par), '3U-S910-X31E-9N-18BL-SATA-2PSU-NR') !== false){
        $nodes = '9';
    }else{
        $nodes = '1';
    }

    if(strpos(strtoupper($category_searcher->par), '4U-JBOD-45BLS2E') !== false || strpos(strtoupper($category_searcher->par), '2U-PTJBOD-CB2-12BLS2') !== false || strpos(strtoupper($category_searcher->par), '1U-D51PH-1ULH-12BL') !== false || strpos(strtoupper($category_searcher->par), '4U-NOMOTHERBOARD-36BL-SAS3-2PS') !== false || strpos(strtoupper($category_searcher->par), '3U-X10SRD-F-8N-16BL-SATA3-1X10GSFP-2PS') !== false || strpos(strtoupper($category_searcher->par), '2U-X9DRI-F-12BL-S2-2PS') !== false || strpos(strtoupper($category_searcher->par), '1U-C220-M3-4BL-2PS') !== false){
      $rff = '3.5';
    }else if(strpos(strtoupper($category_searcher->par), 'CHASSIS-C4130-4X') !== false || strpos(strtoupper($category_searcher->par), 'CHASSIS-C4130') !== false){
      $rff = '1.8';
    }else if(strpos(strtoupper($category_searcher->par), 'G-2U-X10DRU-I+-12BLS3-3xPCIEGPUX16') !== false || strpos(strtoupper($category_searcher->par), '2U-UCS-C240-M4-24BS-MRAID12G-2X10GSFP+-2P') !== false || strpos(strtoupper($category_searcher->par), '1U-R210-2BL-H200') !== false){
      $rff = '2.5';
    }




    $chassis_check_stmt = $conn->prepare("SELECT chassis_id FROM chassis_new WHERE part_number = ?");
    $chassis_check_stmt->bind_param('s', $part_number);
    $chassis_check_stmt->execute();
    $chassis_check = $chassis_check_stmt->get_result();

    if ($chassis_check && $chassis_check->num_rows > 0) {
            $stmt = $conn->prepare("UPDATE chassis_new
                                      SET
                                          mfr = ?,
                                          u = ?,
                                          model = ?,
                                          ff = ?,
                                          rff = ?,
                                          psu = ?,
                                          motherboard = ?,
                                          mb_ff = ?,
                                          bin = ?,
                                          node = ?,
                                          backplane = ?,
                                          drivebays = ?,
                                          rearbays = ?,
                                          controller = ?
                                      WHERE
                                          part_number = ?"
            );

            $stmt->bind_param('sssssssssdsssss', $manufacturer, $u, $model, $dff, $rff, $psu_slots, $motherboard, $mb_ff, $bin, $nodes, $backplane, $drivebays, $rearbays, $controller, $part_number);
    }else{

    $stmt = $conn->prepare("INSERT INTO chassis_new (part_number, mfr, u, model, ff, rff, psu, motherboard, mb_ff, bin, node, backplane, drivebays, rearbays, controller) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");

    $stmt->bind_param('ssssssssssdssss', $part_number, $manufacturer, $u, $model, $dff, $rff, $psu_slots, $motherboard, $mb_ff, $bin, $nodes, $backplane, $drivebays, $rearbays, $controller);

    }
    if ($stmt->execute()) {
                            echo "Successful\n";
                        } else {
                            echo "Error: {$stmt->error}\n";
                        }
  }
  
  $conn->commit();
}

$conn->close();

