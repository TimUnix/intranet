<?php
require_once __DIR__.'/../database.php';

$db_cpu_new = new db();
$conn_dbcpu_new = new mysqli($db_cpu_new->servername(), $db_cpu_new->username(), $db_cpu_new->password(), $db_cpu_new->dbname());

if ($conn_dbcpu_new->connect_error) {
    die("Connection failed: " . $conn_dbcpu_new->connect_error);
}

$get_work_orders_cpu = "SELECT * FROM `cpu`";
$get_work_orders_cpu_result = $conn_dbcpu_new->query($get_work_orders_cpu);

$get_cpu = "select * from cpu_new";
$get_cpu_result = $conn_dbcpu_new->query($get_cpu);

if ($get_work_orders_cpu_result->num_rows > 0){
  while ($search_par = $get_work_orders_cpu_result->fetch_object()) {
  
    $par = $search_par->par;
    $cpu_family = $search_par->CPU_Family;

    /// for cpu_new
    if ($get_cpu_result->num_rows > 0){
      while ($search_part = $get_cpu_result->fetch_object()) {
      
        $part = $search_part->part;
        
        if($par == $part){
          
          $update_cpu = "UPDATE cpu_new SET cpu_family = '$cpu_family' WHERE part='$part'";

          if ($conn_dbcpu_new->query($update_cpu) === TRUE) {
            echo "Record updated successfully";
          } else {
            echo "Error updating record: " . $conn_dbcpu_new->error;
          }

          /// end update
          break;
        }
        
      }
    }
    /// end cpu_new
  }
}

$conn_dbcpu_new->close();
