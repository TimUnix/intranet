<?php
require_once __DIR__.'/database.php';
require_once __DIR__.'/ebay_auth.php';
require_once __DIR__.'/ebay_shopping.php';

$db = create_mysqli_connection();

$stores_query = 'SELECT name FROM ebay_items_store';
$stores_result = $db->query($stores_query);

if (!$stores_result) {
    echo "Failed to acquire stores. Exiting...\n";
    $db->close();
    return;
}

$stores = array();

while ($store = $stores_result->fetch_object()) {
    $stores[] = $store->name;
}

$store_item_numbers = load_store_item_numbers($db, $stores);


if (!$store_item_numbers) {
    echo "Unable to acquire eBay store item id references\n";
    $db->close();
    return;
}

$ebay_items_item_numbers = load_ebay_item_numbers($db, $store);

if (!$ebay_items_item_numbers) {
    echo "Unable to acquire eBay items id references\n";
    $db->close();
    return;
}

$ebay_items_new_items = array();

$ebay_base_url = 'https://svcs.ebay.com/services/search/FindingService/v1?';
$ebay_operation_name = 'findItemsIneBayStores';
$ebay_service_version = '1.13.0';
$ebay_app_id = 'UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2';
$ebay_global_id = 'EBAY-US';
$output_selector = 'StoreInfo';
$pagination_input = 'True';
$entries_per_page = 100;

foreach ($stores as $store) {
    $update_queries = array();
    $insert_queries = array();
    $total_pages = 1;
    $page_number_acquired = false;

    for ($current_page = 1; $current_page <= $total_pages; $current_page++) {
        $store_name = urlencode($store);
        $api_url = $ebay_base_url
                 . "OPERATION-NAME=$ebay_operation_name"
                 . "&SECURITY-APPNAME=$ebay_app_id"
                 . "&SERVICE-VERSION=$ebay_service_version"
                 . "&GLOBAL-ID=$ebay_global_id"
                 . "&storeName=$store_name"
                 . "&outputSelector=$output_selector"
                 . "&paginationInput=True"
                 . "&paginationInput.entriesPerPage=$entries_per_page"
                 . "&paginationInput.pageNumber=$current_page";

        try {
            $api_result = new SimpleXMLElement($api_url, 0, true);
        } catch (Exception $e) {
            continue;
        }

        if (((string) $api_result->ack) !== 'Success') {
            continue;
        }

        if (!$page_number_acquired) {
            $total_pages = $api_result->paginationOutput->totalPages;
            $page_number_acquired = true;
        }

        echo "Acquiring data on page $current_page of $total_pages for $store.\n";

        if (!isset($api_result->searchResult->item)) {
            continue;
        }

        foreach ($api_result->searchResult->item as $item) {
            $view_item_url = $db->real_escape_string((string) $item->viewItemURL);
            $item_number = (string) $item->itemId;
            $gallery_url = $db->real_escape_string((string) $item->galleryURL);
            $current_price = number_format((float) $item->sellingStatus->currentPrice, 2);
            $seller = $db->real_escape_string((string) $item->storeInfo->storeName);
            $category_id = $db->real_escape_string((string) $item->primaryCategory->categoryId);
            $category_name = $db->real_escape_string((string) $item->primaryCategory->categoryName);
            $patterns = array('/\xE2\x80\x8BCR/', '/\xE2\x80\x8E/');
            $title = trim(preg_replace($patterns, '', (string) $db->real_escape_string($item->title)));
            $shipping_type = $db->real_escape_string((string) $item->shippingInfo->shippingType);
            $ship_to_locations = $db->real_escape_string((string) $item->shippingInfo->shipToLocations);
            $expedited_shipping = ((string) $item->shippingInfo->expeditedShipping) === 'true' ? 1 : 0 ;
            $one_day_shipping_available = ((string) $item->shippingInfo->oneDayShippingAvailable) === 'true' ?
                                        1 : 0;
            $handling_time = ((string) $item->shippingInfo->handlingTime) === '' ? '' : -1;
            $top_rated_listing = ((string) $item->topRatedListing) === 'true' ? 1 : 0;
            $selling_state = $db->real_escape_string((string) $item->sellingStatus->sellingState);
            $condition_id =  ((string) $item->condition->conditionId) === '' ?
                          -1 : $db->real_escape_string((string) $item->condition->conditionId);
            $condition_display_name = ((string) $item->condition->conditionDisplayName) ?
                                    $db->real_escape_string($item->condition->conditionDisplayName) : '';

            if (!in_array($item_number, $ebay_items_item_numbers)) {
                $ebay_items_new_items[] = $item_number;
            }

            if (in_array($item_number, $store_item_numbers)) {
                $update_queries[] = "UPDATE ebay_store
                            SET title = '$title',
                            price = '$current_price',
                            link = '$view_item_url',
                            img = '$gallery_url',
                            seller = '$seller',
                            category_id = '$category_id',
                            category_name = '$category_name',
                            shipping_type = '$shipping_type',
                            ship_to_locations = '$ship_to_locations',
                            expedited_shipping = '$expedited_shipping',
                            one_day_shipping_available = '$one_day_shipping_available',
                            top_rated_listing = '$top_rated_listing',
                            handling_time = '$handling_time',
                            selling_state = '$selling_state',
                            condition_id = '$condition_id',
                            condition_display_name = '$condition_display_name'
                            WHERE item_number = '$item_number';";
            } else {
                $item = array(
                    $title,
                    $item_number,
                    $current_price,
                    $view_item_url,
                    $gallery_url,
                    $seller,
                    $category_id,
                    $category_name,
                    $shipping_type,
                    $ship_to_locations,
                    $expedited_shipping,
                    $one_day_shipping_available,
                    $top_rated_listing,
                    $handling_time,
                    $selling_state,
                    $condition_id,
                    $condition_display_name
                );

                $insert_queries[] = "'". implode("', '", $item) . "'";
            }
        }
    }

    $update_queries_count = count($update_queries);
    $insert_queries_count = count($insert_queries);

    if ($update_queries_count > 0) {
        echo "Updating $update_queries_count records...\n";

        $page_size = 50;
        $total_records_updated = 0;
        $total_records_already_updated = 0;

        for ($offset = 0; $offset <= $update_queries_count; $offset += $page_size) {
            $paginated_data = array_slice($update_queries, $offset, $page_size);
            $update = implode(' ', $paginated_data);

            $db->multi_query($update);
            do {
                if ($db->affected_rows > 0) {
                    $total_records_updated += $db->affected_rows;
                    echo "Updated $total_records_updated out of $update_queries_count records.\n";
                } else {
                    $total_records_already_updated++;
                    echo "$total_records_already_updated out of $update_queries_count records already up to date.\n";
                }
            } while ($db->next_result());
        }
    }

    if ($insert_queries_count > 0) {
        echo "Adding $insert_queries_count records...\n";
        $page_size = 50;
        $total_records_added = 0;

        for ($offset = 0; $offset <= $insert_queries_count; $offset += $page_size) {
            $paginated_data = array_slice($insert_queries, $offset, $page_size);

            $insert_query = 'INSERT INTO ebay_store ( '
                          . 'title, '
                          . 'item_number, '
                          . 'price, '
                          . 'link, '
                          . 'img, '
                          . 'seller, '
                          . 'category_id, '
                          . 'category_name, '
                          . 'shipping_type, '
                          . 'ship_to_locations, '
                          . 'expedited_shipping, '
                          . 'one_day_shipping_available, '
                          . 'top_rated_listing, '
                          . 'handling_time, '
                          . 'selling_state, '
                          . 'condition_id, '
                          . 'condition_display_name'
                          . ') VALUES ('
                          . implode('), (', $paginated_data)
                          . ')';

            $insert_result = $db->query($insert_query);

            if ($insert_result === false) {
                echo "Unable to add $page_size records.\n";
                print_r($db->error);
                echo "\n";
            }

            if ($db->affected_rows > 0) {
                echo "Successfully added {$db->affected_rows} records.\n";
            }
        }
    }
}

$ebay_items_insert = array();

if (count($ebay_items_new_items) > 0) {
    $token = generate_application_access_token();
    $ebay_shopping_base_url = "https://open.api.ebay.com/shopping";
    $ebay_site_id = 0;
    $ebay_call_name = "GetMultipleItems";
    $ebay_api_version = 1119;
    $ebay_request_encoding = "xml";

    $new_items_count = count($ebay_items_new_items);
    $page_size = 20;

    for ($offset = 0; $offset <= $new_items_count; $offset += $page_size) {
        $paginated_items = array_slice($ebay_items_new_items, $offset, $page_size);

        $request_body = ebay_shopping_request_builder($paginated_items);

        $headers = array(
            "X-EBAY-API-IAF-TOKEN: BEARER $token",
            "X-EBAY-API-SITEID: $ebay_site_id",
            "X-EBAY-API-VERSION: $ebay_api_version",
            "X-EBAY-API-CALL-NAME: $ebay_call_name",
            "X-EBAY-API-REQUEST-ENCODING: $ebay_request_encoding"
        );

        $curl = curl_init($ebay_shopping_base_url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response_body = curl_exec($curl);
        curl_close($curl);
        
        $response = new SimpleXMLElement($response_body);

        if (((string) $response->Ack) !== 'Success') {
            echo "An error ocurred while acquiring the data\n";
            echo "{$response->Errors->ShortMessage}\n";
            continue;
        }

        foreach ($response->Item as $item) {
            $item_number = $db->real_escape_string((string) $item->ItemID);
            $sku = $db->real_escape_string((string) $item->SKU);
            $hit_count = ((string) $item->HitCount) === '' ? 0 : $db->real_escape_string((string) $item->HitCount);
            $quantity_sold = $db->real_escape_string((string) $item->QuantitySold);
            $quantity = $db->real_escape_string((string) $item->Quantity);
            $current_price = $db->real_escape_string((string) $item->CurrentPrice);
            $patterns = array('/\xE2\x80\x8BCR/', '/\xE2\x80\x8E/');
            $title = trim(preg_replace($patterns, '', $db->real_escape_string((string) $item->Title)));
            $store_name = $db->real_escape_string((string) $item->Seller->UserID);
            $link = $db->real_escape_string((string) $item->ViewItemURLForNaturalSearch);
            $img = $db->real_escape_string((string) $item->PictureURL);
            $timestamp = $db->real_escape_string((string) $response->Timestamp);
            $start_time = $db->real_escape_string((string) $item->StartTime);
            $end_time = $db->real_escape_string((string) $item->EndTime);
            $category = $db->real_escape_string((string) $item->PrimaryCategoryName);
            $listing_status = $db->real_escape_string((string) $item->ListingStatus);
            $shipping_cost_response = (string) $item->ShippingCostSummary->ShippingServiceCost;
            $shipping_cost = $shipping_cost_response === '' ? -1 : $shipping_cost_response;

            $inventory_qty_stmt = $db->prepare('SELECT qty, cat FROM inventory WHERE par = ?');
            $inventory_qty_stmt->bind_result($inventory_qty_result, $inventory_cat);
            $inventory_qty_stmt->bind_param('s', $sku);
            $inventory_qty_stmt->execute();
            $inventory_qty_stmt->fetch();
            $inventory_qty_stmt->close();

            $inventory_qty = $inventory_qty_result === '' ? -1 : $inventory_qty_result;

            $net_sold_total = floatval($current_price * $quantity_sold);

            // Figure out how to get hit_counter and registration_date because both fields are no longer
            // available through Shopping API. Set them as empty strings for now.
            $ebay_items = array(
                $item_number,
                $store_name,
                $sku,
                $quantity,
                $quantity_sold,
                $hit_count,
                $current_price,
                $net_sold_total,
                $title,
                $link,
                $img,
                $timestamp,
                $start_time,
                $end_time,
                $category,
                '',
                '',
                $listing_status,
                $shipping_cost,
                $inventory_qty,
                $inventory_cat
            );

            $ebay_items_insert[] = '"'.implode('","', $ebay_items).'"';
        }
    }
}

if (count($ebay_items_insert) > 0) {
    echo "Adding records to eBay Items...\n";
    
    $ebay_items_insert_count = count($ebay_items_insert);
    $page_size = 50;

    for ($offset = 0; $offset <= $ebay_items_insert_count; $offset += $page_size) {
        $paginated_items = array_slice($ebay_items_insert, $offset, $page_size);

        $insert_query = "INSERT INTO ebay_items ("
                      . "`item_number`, "
                      . "`seller`, "
                      . "`sku`, "
                      . "`quantity`, "
                      . "`quantity_sold`, "
                      . "`hit_count`, "
                      . "`current_price`, "
                      . "`net_sold_total`,"
                      . "`title`, "
                      . "`link`, "
                      . "`img`, "
                      . "`timestamp`, "
                      . "`start_time`, "
                      . "`end_time`, "
                      . "`category`, "
                      . "`registration_date`, "
                      . "`hit_counter`, "
                      . "`listing_status`, "
                      . "`shipping_cost`, "
                      . "`inventory_qty`,"
                      . "`inventory_cat`"
                      . ") VALUES ("
                      . implode('),(', $paginated_items)
                      .")";

        $insert_stmt = $db->prepare($insert_query);

        if ($insert_stmt && $insert_stmt->execute()) {
            echo "Successfully added {$insert_stmt->affected_rows} records to eBay Items\n";
        } else {
            echo "An error occured while saving records for eBay Items\n";
        }
    }
}

$db->close();

function load_store_item_numbers($db, $stores) {
    if (is_array($stores)) {
        $stores = "('" . implode("', '", $stores) . "')";
    }

    $item_numbers_query = "SELECT item_number FROM ebay_store WHERE seller IN $stores";
    $item_numbers_result = $db->query($item_numbers_query);

    if (!$item_numbers_result) {
        return $item_numbers_result;
    }

    $item_numbers_values = $item_numbers_result->fetch_all();
    $item_numbers = array_map(function($item_number) { return $item_number[0]; }, $item_numbers_values);

    return $item_numbers;
}

function load_ebay_item_numbers($db) {
    $item_numbers_query = "SELECT item_number FROM ebay_items";
    $item_numbers_result = $db->query($item_numbers_query);

    if (!$item_numbers_result) {
        return $item_numbers_result;
    }

    $item_numbers_values = $item_numbers_result->fetch_all();
    $item_numbers = array_map(function($item_number) { return $item_number[0]; }, $item_numbers_values);

    return $item_numbers;
}
