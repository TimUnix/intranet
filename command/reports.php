<?php
require_once __DIR__.'/database.php';

$conn = create_mysqli_connection();

if (count($argv) < 3) {
    exit("Missing arguments. Usage: php reports.php directory purchase_order");
}

$directory = $argv[1];

if (!is_dir($directory)) {
    exit("{$argv[1]} is not a directory.\n");
}

$po = $argv[2];

$directory_contents = scandir($directory);
$files = array_filter($directory_contents, function($file) { return !in_array($file, array('.', '..')); });

$error_count = 0;
$success_count = 0;
$total = count($files);

$valid_mime_types = array(
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.text'
);

// Open a known directory, and proceed to read its contents
foreach($files as $file) {
    $directory_trailing_slash = substr($directory, -1) === '/';
    $filepath = $directory_trailing_slash ? "{$directory}{$file}" : "$directory/$file";

    $file_info = pathinfo($file);
    $filename = $file_info['filename'];
    $file_split = explode("_", $filename);
    $serial = $file_split[0];
    $part = $file_split[1];

    $mime_type = mime_content_type($filepath);

    if ($mime_type === false) {
        $error_count++;
        continue;
    }
    if (!in_array($mime_type, $valid_mime_types)) {
        $error_count++;
        continue;
    }

    // echo "Serial Number ".$serial;
    // echo "Part Number ".$part;

    $report = extracttext($filepath, $mime_type);

    if (!$report) {
        $error_count++;
        continue;
    }

    $reportesc = $conn->real_escape_string($report);
    $tech = 'System';
    $qry = "INSERT INTO switch_reports(part_number, serial_number, purchase_order, tech, report) "
        . "VALUES ('$part', '$serial', '$po', '$tech', '$reportesc')";
    $qry_stmt = $conn->prepare($qry);

    if ($qry_stmt && $qry_stmt->execute()) {
        $success_count++;
        echo "Successfully added {$serial}_{$part} to switch reports...\n";
    } else {
        $error_count++;
        echo $qry;
        echo "An error occured while saving the records for $file...\n";
        print_r($conn->error_list);
    }
}

echo "\n$error_count errors out of $total files\n";
echo "$success_count files added out of $total\n";

function get_word_text($element) {
    $result = '';
    if ($element instanceof \PhpOffice\PhpWord\Element\AbstractContainer) {
        foreach ($element->getElements() as $element) {
            $result .= get_word_text($element);
        }
    } elseif ($element instanceof \PhpOffice\PhpWord\Element\Text) {
        $result .= $element->getText();
    }

    return $result;
}

/**Function to extract text*/
function extracttext($filename, $mime_type) {
    include_once "../vendor/autoload.php";

    global $valid_mime_types;

    $php_word = null;

    switch ($mime_type) {
        case $valid_mime_types[0]:
            $php_word = \PhpOffice\PhpWord\IOFactory::load($filename, 'MsDoc');
            break;
        case $valid_mime_types[1]:
            $php_word = \PhpOffice\PhpWord\IOFactory::load($filename);
            break;
        default:
            $php_word = \PhpOffice\PhpWord\IOFactory::load($filename, 'ODText');
    }

    if ($php_word === null) {
        echo "Invalid file.\n";
        return;
    }

    $document_text = '';

    foreach ($php_word->getSections() as $section) {
        foreach ($section->getElements() as $element) {
            $document_text .= get_word_text($element) . "\n";
        }
    }

    return $document_text;
}

?>
