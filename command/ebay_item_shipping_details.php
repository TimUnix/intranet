<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

include_once __DIR__.'/database.php';

$db = new db();

$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$stores_query = 'SELECT name FROM ebay_items_store';
    $stores_stmt = $conn->prepare($stores_query);

    if (!$stores_stmt->execute()) {
        echo "Failed to load stores.\n";
        print_r($stores_stmt->error);
        $mysqli->close();
        return;
    }

    if (!$stores_stmt->num_rows === 0) {
        echo "No stores found.\n";
        print_r($stores_stmt->error);
        $mysqli->close();
        return;
    }

    $stores_stmt->bind_result($store);
    $stores = array();

    while ($stores_stmt->fetch()) {
        $stores[] = $store;
    }

    $stores_stmt->close();
foreach($stores as $store){
         echo "Loading refresh token for {$store}...\n";

        switch(strtolower($store)) {
            case 'unixsurplusnet':
                $configuration_code = 'ebay_token_net';
                break;
            case 'unixpluscom':
                $configuration_code = 'ebay_token_plus';
                break;
            case 'itrecyclenow':
                $configuration_code = 'ebay_token_itr';
                break;
            case 'unixsurpluscom':
                $configuration_code = 'ebay_token';
                break;
        }

        $refresh_token_query = "SELECT value FROM configuration WHERE code = ?";
        $refresh_token_stmt = $conn->prepare($refresh_token_query);
        $refresh_token_stmt->bind_result($refresh_token);
        $refresh_token_stmt->bind_param('s', $configuration_code);

        if (!$refresh_token_stmt->execute() && !$refresh_token_stmt->fetch()) {
            echo "An error occured while loading refresh token for {$store}\n";

            continue;
        }

        if (!$refresh_token_stmt->fetch()) {
                continue;
                echo "Acquiring token for {$store}...\n";    
        }
        $request_body = api_query_builder($refresh_token); 
        $refresh_token_stmt->close();

        echo "Acquiring new data...\n\n";

$ebay_base_url = "https://api.ebay.com/ws/api.dll";
$ebay_site_id = 0;
$ebay_api_version = 967;
$ebay_call_name = "GetOrders";

$headers = array(

    "X-EBAY-API-SITEID: $ebay_site_id",
    "X-EBAY-API-COMPATIBILITY-LEVEL: $ebay_api_version",
    "X-EBAY-API-CALL-NAME: $ebay_call_name"
);



$curl = curl_init($ebay_base_url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response_body = curl_exec($curl);
$response = new SimpleXMLElement($response_body);

$ebay_items_insert = [];
$ebay_shipped_update = array();
$getShippedItems = order_id($conn);

if ($response->Ack == "Success"){
    $date = date("Y-m-d H:i:s");
    foreach ($response->OrderArray->Order as $item){
        $order_id = $conn->real_escape_string((string) $item->OrderID);       
        $seller = $conn->real_escape_string((string) $item->SellerUserID);
        $cancel_status = $conn->real_escape_string((string) $item->CancelStatus);
        $order_status = $conn->real_escape_string((string) $item->OrderStatus);

        foreach($item->TransactionArray->Transaction as $transaction){
            $item_number = $conn->real_escape_string((string) $transaction->Item->ItemID);
            $title = $conn->real_escape_string((string) $transaction->Item->Title);
            $sku = $conn->real_escape_string((string) $transaction->Item->SKU);
            $qty_sold = $conn->real_escape_string((string) $transaction->QuantityPurchased);
            $shipped_date = $conn->real_escape_string((string) $transaction->ShippedTime);
            $create_date = $conn->real_escape_string((string) $transaction->CreatedDate);
            if($shipped_date == ''){
                $counter = 0;
            }else{
                $counter = 1;
            }
            if (array_key_exists($item_number, $getShippedItems)){
                $ebay_shipped_update[] = "UPDATE ebay_items_shipping_details
                            SET item_number='$item_number',
                            title = '$title',
                            sku = '$sku',
                            order_id = '$order_id',
                            qty_sold = '$qty_sold',
                            seller = '$seller',
                            cancel_status = '$cancel_status',
                            order_status = '$order_status',
                            shipped_date = '$shipped_date',
                            created_date = '$create_date',
                            counter = '$counter'
                            WHERE order_id =   '$getShippedItems[$item_number]'
                            AND item_number = '$item_number'";
            }else{
                $ebay_items = array(
                $item_number,
                $title,
                $sku,
                $order_id,
                $qty_sold,
                $seller,
                $cancel_status,
                $order_status,
                $shipped_date === '' ? '0000-00-00T00:00:00.000Z' : $shipped_date,
                $create_date,
                $date,
                $counter
                );
                
                $ebay_items_insert[] = '"'.implode('","', $ebay_items).'"';
            }   
        }      
    }
    
}else{
    echo "An error ocurred while acquiring the data\n";
    echo "{$response->Errors->ShortMessage}\n";
}

if (count($ebay_shipped_update) > 0) {

        $offset = 0;
        $records_to_insert = 20;
        $total_records = count($ebay_shipped_update);

        while ($offset <= $total_records) {
            $paginated_data = array_slice($ebay_shipped_update, $offset, $records_to_insert);
            $update = implode("; ", $paginated_data);

            if (mysqli_multi_query($conn, $update)) {
                do {
                    
                    if (mysqli_affected_rows($conn) > 0) {
                        echo "Updated " . mysqli_affected_rows($conn) . " records\n";
                    }
                } while (mysqli_next_result($conn));
            }

            $offset += $records_to_insert;
        }
    }

if (count($ebay_items_insert) > 0){
    $offset = 0;
    $records_to_insert = 20;
    $total_records = count($ebay_items_insert);

    while ($offset <= $total_records){
        $paginated_data = array_slice($ebay_items_insert, $offset, $records_to_insert);

        $insert = "INSERT INTO ebay_items_shipping_details ("
                    . "`item_number`, "
                    . "`title`, "
                    . "`sku`, "
                    . "`order_id`, "
                    . "`qty_sold`, "
                    . "`seller`, "
                    . "`cancel_status`, "
                    . "`order_status`, "
                    . "`shipped_date`, "
                    . "`created_date`, "
                    . "`date`, "
                    . "`counter`"
                    . ") VALUES ("
                    . implode('),(', $paginated_data )
                    .")";

            $insert_stmt = $conn->prepare($insert);

            if ($insert_stmt && $insert_stmt->execute()) {
                echo "Successfully added {$insert_stmt->affected_rows} records...\n\n";
            } else {
                echo "An error occured while saving the records for...\n\n";
                print_r($insert);
                print_r($conn->error_list);
            }

            $offset += $records_to_insert;
    }
}
    }

$conn->close();

function order_id($conn){
    $sql = "SELECT item_number, order_id from ebay_items_shipping_details";
    $data =  $conn->query($sql);
    $storage_array = array();
    if ($data && $data->num_rows > 0){
    while ($data_each = $data->fetch_object()) {
        $storage_array[$data_each->item_number] = $data_each->order_id;
        
        }
    }
    return $storage_array;
}

function api_query_builder($refresh_token) {


    $beginOfDay = gmdate("Y-m-d\T00:00:00.000\Z");
    $endOfDay = gmdate("Y-m-d\T23:59:59.000\Z");

    $xml = 
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>
            <GetOrdersRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">
                <RequesterCredentials>
                    <eBayAuthToken>$refresh_token</eBayAuthToken>
                </RequesterCredentials>
                <CreateTimeFrom>$beginOfDay</CreateTimeFrom>    
                <CreateTimeTo>$endOfDay</CreateTimeTo>
            </GetOrdersRequest>";
       

    $xml_element =  new SimpleXMLElement($xml);
    return $xml_element->asXML();
   
}

?>
