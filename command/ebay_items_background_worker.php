<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

require_once __DIR__.'/database.php';
require_once __DIR__.'/ebay_auth.php';
require_once __DIR__.'/ebay_shopping.php';

$db = new db();

$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if(isset($argv[1])){
    $archive = $argv[1];
} else {
    $archive = "not hourly";
}

if($archive === "not hourly"){
    echo "Archiving current data...\n";
    
    $send_to_archive_query = "INSERT INTO `ebay_items_archive` SELECT * FROM `ebay_items`";
    $send_to_archive_stmt = $conn->prepare($send_to_archive_query);

    if ($send_to_archive_stmt && $send_to_archive_stmt->execute()) {
        echo "Successfully archived current ebay items...\n";
        echo $send_to_archive_stmt->affected_rows . " records archived...\n\n";
    } else {
        echo "An error occured while archiving current ebay items...\n\n";
        print_r($conn->error_list);
        exit();
    }

}

echo "Acquiring new data...\n\n";

$ebay_application_token = generate_application_access_token();
$ebay_shopping_base_url = "https://open.api.ebay.com/shopping";
$ebay_site_id = 0;
$ebay_call_name = "GetMultipleItems";
$ebay_api_version = 863;
$ebay_request_encoding = "xml";

$query_stores = "SELECT `name` FROM `ebay_items_store`";    
$stores = $conn->query($query_stores);

$ebay_items_insert = array();

if ($stores && $stores->num_rows > 0) {
    while ($store = $stores->fetch_object()) {
        $query_items = "SELECT `item_number` FROM `ebay_store` WHERE `seller` = '{$store->name}' "
                     . "AND selling_state = 'Active';";
        $reference_items_result = $conn->query($query_items);

        if ($reference_items_result && $reference_items_result->num_rows > 0) {

            echo "Current store: {$store->name}\n";
            
            $reference_items = $reference_items_result->fetch_all();
            $reference_items_offset = 0;
            $reference_items_to_check = 20;
            $total_reference_items = count($reference_items);

            while($reference_items_offset <= $total_reference_items) {
                $paginated_reference_items = array_slice(
                    $reference_items, $reference_items_offset, $reference_items_to_check
                );

                $paginated_reference_items_count = count($paginated_reference_items);
                echo "Acquiring data for $paginated_reference_items_count items...\n";

                $paginated_items = array_map(function($item) { return $item[0]; }, $paginated_reference_items);
                $request_body = ebay_shopping_request_builder($paginated_items);

                $headers = array(
                    "X-EBAY-API-IAF-TOKEN: BEARER $ebay_application_token",
                    "X-EBAY-API-SITEID: $ebay_site_id",
                    "X-EBAY-API-VERSION: $ebay_api_version",
                    "X-EBAY-API-CALL-NAME: $ebay_call_name",
                    "X-EBAY-API-REQUEST-ENCODING: $ebay_request_encoding"
                );

                $curl = curl_init($ebay_shopping_base_url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $response_body = curl_exec($curl);
                curl_close($curl);
                $response = new SimpleXMLElement($response_body);

                if ($response->Ack == "Success") {
                    foreach ($response->Item as $item) {
                        $item_number = $conn->real_escape_string((string) $item->ItemID);
                        $sku = $conn->real_escape_string((string) $item->SKU);
                        $hit_count = $conn->real_escape_string((string) $item->HitCount);
                        $hit_count = $hit_count == '' ? 0 : $hit_count;
                        $quantity_sold = $conn->real_escape_string((string) $item->QuantitySold);
                        $quantity = $conn->real_escape_string((string) $item->Quantity);
                        $current_price = $conn->real_escape_string((string) $item->CurrentPrice);
                        $patterns = array('/\xE2\x80\x8BCR/', '/\xE2\x80\x8E/');
                        $title = trim(preg_replace($patterns, '', $conn->real_escape_string((string) $item->Title)));
                        $store_name = $store->name;
                        $link = $conn->real_escape_string((string) $item->ViewItemURLForNaturalSearch);
                        $img = $conn->real_escape_string((string) $item->PictureURL);
                        $timestamp = $conn->real_escape_string((string) $response->Timestamp);
                        $start_time = $conn->real_escape_string((string) $item->StartTime);
                        $end_time = $conn->real_escape_string((string) $item->EndTime);
                        $category = $conn->real_escape_string((string) $item->PrimaryCategoryName);
                        $listing_status = $conn->real_escape_string((string) $item->ListingStatus);
                        $shipping_cost_response = (float) $item->ShippingCostSummary->ShippingServiceCost;
                        $shipping_cost = $shipping_cost_response === '' ? -1 : $shipping_cost_response;

                        $inventory_qty_stmt = $conn->prepare('SELECT qty, cat FROM inventory WHERE par = ?');
                        $inventory_qty_stmt->bind_result($inventory_qty_result, $inventory_cat);
                        $inventory_qty_stmt->bind_param('s', $sku);
                        $inventory_qty_stmt->execute();
                        $inventory_qty_stmt->fetch();
                        $inventory_qty_stmt->close();

                        $inventory_qty = $inventory_qty_result === '' ? -1 : $inventory_qty_result;

                        $net_sold_total = floatval($current_price * $quantity_sold);

                        // Figure out how to get hit_counter and registration_date because both fields are no longer
                        // available through Shopping API. Set them as empty strings for now.
                        $ebay_items = array(
                            $item_number,
                            $store_name,
                            $sku,
                            $quantity,
                            $quantity_sold,
                            $hit_count,
                            $current_price,
                            $net_sold_total,
                            $title,
                            $link,
                            $img,
                            $timestamp,
                            $start_time,
                            $end_time,
                            $category,
                            '',
                            '',
                            $listing_status,
                            $shipping_cost,
                            $inventory_qty,
                            $inventory_cat
                        );

                        $ebay_items_insert[] = '"'.implode('","', $ebay_items).'"';
                    }
                } else {
                    echo "An error ocurred while acquiring the data\n";
                    echo "{$response->Errors->ShortMessage}\n";
                }

                $reference_items_offset += $reference_items_to_check;
            }
        }
    }

    if (count($ebay_items_insert) > 0) {

        echo "Removing transferred data...\n";

        $remove_archived_data_query = "DELETE FROM `ebay_items`;";
        $remove_archived_data_statement = $conn->prepare($remove_archived_data_query);

        if ($remove_archived_data_statement->execute()) {
            echo "Removed transferred data...\n";
            echo $remove_archived_data_statement->affected_rows . " records removed...\n\n";
        } else {
            echo "An error occured while removing the transferred data...\n\n";
            exit();
        }

        $offset = 0;
        $records_to_insert = 20;
        $total_records = count($ebay_items_insert);

        while ($offset <= $total_records) {
            $paginated_data = array_slice($ebay_items_insert, $offset, $records_to_insert);

            $insert = "INSERT INTO ebay_items ("
                    . "`item_number`, "
                    . "`seller`, "
                    . "`sku`, "
					. "`quantity`, "
					. "`quantity_sold`, "
					. "`hit_count`, "
					. "`current_price`, "
                    . "`net_sold_total`,"
					. "`title`, "
					. "`link`, "
					. "`img`, "
					. "`timestamp`, "
					. "`start_time`, "
					. "`end_time`, "
					. "`category`, "
                    . "`registration_date`, "
                    . "`hit_counter`, "
					. "`listing_status`, "
                    . "`shipping_cost`, "
                    . "`inventory_qty`,"
                    . "`inventory_cat`"
                    . ") VALUES ("
                    . implode('),(', $paginated_data )
                    .")";

            $insert_stmt = $conn->prepare($insert);

            if ($insert_stmt && $insert_stmt->execute()) {
                echo "Successfully added {$insert_stmt->affected_rows} records...\n\n";
            } else {
                echo "An error occured while saving the records for...\n\n";
                print_r($insert);
                print_r($conn->error_list);
            }

            $offset += $records_to_insert;
        }
    }
}

$conn->close();
?>
