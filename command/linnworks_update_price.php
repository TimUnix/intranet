<?php
    require_once __DIR__.'/database.php';

    $db = new db();
    $mysqli = create_mysqli_connection();

    $project_root_result = $mysqli->query("SELECT value FROM configuration WHERE code = 'project_root'");

    if (!$project_root_result) {
        print_r($mysqli->error);
        $mysqli->close();
        exit("Failed to acquire project root. Exiting...\n");
    }

    $project_root = $project_root_result->fetch_object()->value;
    $folderpath = "$project_root/backups/database";
    if (!is_dir($folderpath)) {
        mkdir($folderpath, 0755, true);
    }

    $linn_price_update_query = "SELECT `sku`, `price`, `is_done` FROM `linn_update_price` WHERE `is_done` = '0'";
    $linn_price_update_result = $mysqli->query($linn_price_update_query);
    
    if($linn_price_update_result && $linn_price_update_result->num_rows > 0){
        $delimiter = ",";
         $filepath = "$folderpath/linnUpdatePrice.csv";
               
        touch($filepath);
        $f = fopen($filepath, 'w');
        
        $fields = array('sku', 'price');
        fputcsv($f, $fields, $delimiter);

        while($row = $linn_price_update_result->fetch_assoc()){
            $lineData = array($row['sku'], $row['price']);
            fputcsv($f, $lineData, $delimiter);

            $sku = $row['sku'];
            $is_done = 1;
            $stmt = $mysqli->prepare("UPDATE linn_update_price SET is_done = ? WHERE sku = ?");
           
			$stmt->bind_param('ds', $is_done, $sku);

            if ($stmt->execute()) {
                echo "Successful\n";
            } else {
                echo "Error: {$stmt->error}\n";
            }
        }
    }
?>