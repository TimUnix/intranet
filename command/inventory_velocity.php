<?php

require_once __DIR__ . '/database.php';

$db = create_mysqli_connection();

$backups_query = 'SELECT sku, qty, date FROM inventory_backup '
    . 'WHERE date > DATE(NOW() - INTERVAL 3 DAY) ORDER BY sku, date DESC';
$backups_result = $db->query($backups_query);
$backups = $backups_result->fetch_all(MYSQLI_ASSOC);

$devices = array();

foreach ($backups as $backup) {
    $devices[$backup['sku']][] = array('qty' => (int) $backup['qty'], 'date' => $backup['date']);
}

$velocities = array();
$get_sku_id = sku_id($db);
$velocity_insert = [];
$velocity_update = array();

foreach ($devices as $sku => $device_values) {
    // velocity = current date - previous date
    // The data is in descending order so the data with the latest date will be at array 0 and then go down to earlier
    // dates as array the index increases. In that case, the only necessary indices needed are 0, 1 and 2 to calculate
    // the velocity for the current date and the previous date.
    // In short, the data in array index 0 is the data for the current date, index 1 for the previous date
    // and index 2 for the date before the previous date.

    if (count($device_values) < 3) {
        echo "Insufficient data for velocity calculation for $sku\n";
        continue;
    }

    $today_velocity = $device_values[0]['qty'] - $device_values[1]['qty'];
    $yesterday_velocity = $device_values[1]['qty'] - $device_values[2]['qty'];
    $velocity_difference = $today_velocity - $yesterday_velocity;
    $sku = $db->real_escape_string($sku);

    if (in_array($sku, $get_sku_id)){
       
        $velocity_update[] = "UPDATE inventory_velocity
							SET velocity_today = '$today_velocity',
							velocity_yesterday = '$yesterday_velocity',
							velocity_difference = '$velocity_difference'
							WHERE sku = '$sku'";

    }else{
        $velocity = array(
            $sku,
            $today_velocity,
            $yesterday_velocity,
            $velocity_difference
        );
        //var_dump($velocity);
        $velocity_insert[] = '"'.implode('","', $velocity).'"';
        
    }

    //$velocities[] = "('$sku', $today_velocity, $yesterday_velocity, $velocity_difference)";
}

if(count($velocity_update) > 0){
    
    $offset = 0;
    $records_to_insert = 20;
    $total_records = count($velocity_update);
    
    while ($offset <= $total_records) {
        $paginated_data = array_slice($velocity_update, $offset, $records_to_insert);
        $update = implode("; ", $paginated_data);
        
        if (mysqli_multi_query($db, $update)) {
            do {
                
                if (mysqli_affected_rows($db) > 0) {
                    echo "Updated " . mysqli_affected_rows($db) . " records\n";
                }
            } while (mysqli_next_result($db));
        }

        $offset += $records_to_insert;
    }
}

if (count($velocity_insert) > 0){

    $offset = 0;
    $records_to_insert = 20;
    $total_records = count($velocity_insert);

    while ($offset <= $total_records){
        $paginated_data = array_slice($velocity_insert, $offset, $records_to_insert);

        $insert = "INSERT INTO inventory_velocity ("
                    . "`sku`, "
                    . "`velocity_today`, "
                    . "`velocity_yesterday`, "
                    . "`velocity_difference` "
                    . ") VALUES ("
                    . implode('),(', $paginated_data )
                    .")";

            $insert_stmt = $db->prepare($insert);

            if ($insert_stmt && $insert_stmt->execute()) {
                echo "Successfully added {$insert_stmt->affected_rows} records...\n\n";
            } else {
                echo "An error occured while saving the records for...\n\n";
                print_r($insert);
                print_r($db->error_list);
            }

            $offset += $records_to_insert;
    }
}


$db->close();

function sku_id($db){
    $sql = "SELECT sku, id from inventory_velocity";
    $data =  $db->query($sql);
    $storage_array = array();
    if ($data && $data->num_rows > 0){
    while ($data_each = $data->fetch_object()) {
        $storage_array[] = $db->real_escape_string($data_each->sku);
            
        }
    }
    return $storage_array;
}


