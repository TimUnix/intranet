<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

require_once __DIR__ . "/ebay_auth.php";
require_once __DIR__ . "/database.php";

$conn = create_mysqli_connection();

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$stores_query = 'SELECT name FROM ebay_items_store';
$stores_stmt = $conn->prepare($stores_query);

if (!$stores_stmt->execute()) {
    echo "Failed to load stores.\n";
    print_r($stores_stmt->error);
    $mysqli->close();
    return;
}

if (!$stores_stmt->num_rows === 0) {
    echo "No stores found.\n";
    print_r($stores_stmt->error);
    $mysqli->close();
    return;
}

$stores_stmt->bind_result($store);
$stores = array();

while ($stores_stmt->fetch()) {
    $stores[] = $store;
}

$stores_stmt->close();
foreach($stores as $store){

    switch(strtolower($store)) {
        case 'unixsurplusnet':
            $configuration_code = 'ebay_refresh_token_net';
            break;
        case 'unixpluscom':
            $configuration_code = 'ebay_refresh_token_plus';
            break;
        case 'itrecyclenow':
            $configuration_code = 'ebay_refresh_token_itr';
            break;
        case 'unixsurpluscom':
            $configuration_code = 'ebay_refresh_token_com';
            break;
    }

    $refresh_token_query = "SELECT value FROM configuration WHERE code = ?";
    $refresh_token_stmt = $conn->prepare($refresh_token_query);
    $refresh_token_stmt->bind_result($refresh_token);
    $refresh_token_stmt->bind_param('s', $configuration_code);

    if (!$refresh_token_stmt->execute() && !$refresh_token_stmt->fetch()) {
        echo "An error occured while loading refresh token for {$store}\n";

        continue;
    }

    if (!$refresh_token_stmt->fetch()) {
        continue;
        echo "Acquiring token for {$store}...\n";    
    }
    $refresh_token_stmt->close();
    $scope = urlencode('https://api.ebay.com/oauth/api_scope');

	$oauth = refresh_ebay_oauth_token($refresh_token, $scope);

	echo "Acquiring new data...\n\n";
	$total_page = total_pages();

	
	for($per_page = 1; $per_page<$total_page; $per_page++){
		var_dump($per_page);
   		$request_body = api_query_builder($per_page);
   	
		$ebay_base_url = "https://api.ebay.com/ws/api.dll";
		$ebay_site_id = 0;
		$ebay_api_version = 967;
		$ebay_call_name = "GetFeedback";


		$headers = array(

			"X-EBAY-API-IAF-TOKEN: $oauth",
			"X-EBAY-API-SITEID: $ebay_site_id",
			"X-EBAY-API-COMPATIBILITY-LEVEL: $ebay_api_version",
			"X-EBAY-API-CALL-NAME: $ebay_call_name"
		);

		$curl = curl_init($ebay_base_url);
	    	curl_setopt($curl, CURLOPT_POST, true);
	    	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	    	curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);
	    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$response_body = curl_exec($curl);
		$response = new SimpleXMLElement($response_body);
		$product_review_insert = [];
		$ebay_product_review =array();
		$get_feedback_id = feedback_id($conn);

		if ($response->Ack == "Success"){
			if(empty($response->FeedbackDetailArray->FeedbackDetail)){
				echo "No data available";
			}else{		
				foreach($response->FeedbackDetailArray->FeedbackDetail as $item){
					$customer = $conn->real_escape_string((string) $item->CommentingUser);
					$description = $conn->real_escape_string((string) $item->ItemTitle);
					$review_content = $conn->real_escape_string((string) $item->CommentText);
					$review_type = $conn->real_escape_string((string) $item->CommentType);
					$review_date_gmt = $conn->real_escape_string((string) $item->CommentTime);
					$feedback_id = $conn->real_escape_string((string) $item->FeedbackID);
					$item_id = $conn->real_escape_string((string) $item->ItemID);

				//	var_dump($feedback_id);
					$review_date = date(DATE_ISO8601, strtotime($review_date_gmt));

					$sku_query = "SELECT sku FROM ebay_items WHERE item_number = $item_id";
					$sku_result = $conn->query($sku_query);

					if ($sku_result && $sku_result->num_rows > 0){
						while($sku_searcher = $sku_result->fetch_object()){
					
							$sku = $sku_searcher->sku;
		
						}
					}else{
						$sku = 'Not Available';
					}
						

						if (array_key_exists($feedback_id, $get_feedback_id)){
							$ebay_product_review[] = "UPDATE ebay_product_review
							SET item_number = '$item_id',
							sku = '$sku',
							description = '$description',
							customer = '$customer',
							review_content = '$review_content',
							review_type = '$review_type',
							review_date = '$review_date',
							feedback_id = '$feedback_id'
							WHERE item_id = '$get_feedback_id[$feedback_id]'
							and feedback_id = $feedback_id";
					
						}else{
							$ebay_items = array(
								$item_id,
								$sku,
								$description,
								$customer,
								$review_content,
								$review_type,
								$review_date,
								$feedback_id

							);

							$product_review_insert[] = '"'.implode('","', $ebay_items).'"';
						}
					//}
				}
			}
				
		}else{
			echo "An error ocurred while acquiring the data\n";
    		echo "{$response->Errors->ShortMessage}\n";
		}

		if (count($ebay_product_review) > 0) {

	        $offset = 0;
	        $records_to_insert = 20;
	        $total_records = count($ebay_product_review);

	        while ($offset <= $total_records) {
	            $paginated_data = array_slice($ebay_product_review, $offset, $records_to_insert);
	            $update = implode("; ", $paginated_data);

	            if (mysqli_multi_query($conn, $update)) {
	                do {
	                    
	                    if (mysqli_affected_rows($conn) > 0) {
	                        echo "Updated " . mysqli_affected_rows($conn) . " records\n";
	                    }
	                } while (mysqli_next_result($conn));
	            }

	            $offset += $records_to_insert;
	        }
	    }

	    if (count($product_review_insert) > 0){
		    $offset = 0;
		    $records_to_insert = 20;
		    $total_records = count($product_review_insert);

		    while ($offset <= $total_records){
		        $paginated_data = array_slice($product_review_insert, $offset, $records_to_insert);

		        $insert = "INSERT INTO ebay_product_review ("
		                    . "`item_number`, "
		                    . "`sku`, "
		                    . "`description`, "
		                    . "`customer`, "
		                    . "`review_content`, "
		                    . "`review_type`, "
		                    . "`review_date`, "
		                    . "`feedback_id` "
		                    . ") VALUES ("
		                    . implode('),(', $paginated_data )
		                    .")";

		            $insert_stmt = $conn->prepare($insert);
		           
		            if ($insert_stmt && $insert_stmt->execute()) {
		                echo "Successfully added {$insert_stmt->affected_rows} records...\n\n";
		            } else {
		                echo "An error occured while saving the records for...\n\n";
		                print_r($insert);
		                print_r($conn->error_list);
		            }

		            $offset += $records_to_insert;
		    }
		}
		//ubos gi add na close }
	}

}
$conn->close();

function feedback_id($conn){
    $sql = "SELECT item_number, feedback_id from ebay_product_review";
    $data =  $conn->query($sql);
    $storage_array = array();
    if ($data && $data->num_rows > 0){
    while ($data_each = $data->fetch_object()) {
        $storage_array[$data_each->feedback_id] = $data_each->item_number;
        
        }
    }
    return $storage_array;
}

function api_query_builder($per_page){
		
		$xml = 
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>
			 <GetFeedbackRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">
	  		 <FeedbackType>FeedbackReceivedAsSeller</FeedbackType>
	  		 <DetailLevel>ReturnAll</DetailLevel>
	  		 <Pagination>
			    <EntriesPerPage>200</EntriesPerPage>
			    <PageNumber>$per_page</PageNumber>
			</Pagination>
			 </GetFeedbackRequest>";

		$xml_element =  new SimpleXMLElement($xml);
    	return $xml_element->asXML();

}

function api_query_builder_first_page(){
		
		$xml = 
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>
			 <GetFeedbackRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">
	  		 <FeedbackType>FeedbackReceivedAsSeller</FeedbackType>
	  		 <DetailLevel>ReturnAll</DetailLevel>
	  		 <Pagination>
			    <EntriesPerPage>200</EntriesPerPage>
			</Pagination>
			 </GetFeedbackRequest>";

		$xml_element =  new SimpleXMLElement($xml);
    	return $xml_element->asXML();

}

function total_pages(){
	$conn = create_mysqli_connection();

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	$stores_query = 'SELECT name FROM ebay_items_store';
	$stores_stmt = $conn->prepare($stores_query);

	if (!$stores_stmt->execute()) {
	    echo "Failed to load stores.\n";
	    print_r($stores_stmt->error);
	    $mysqli->close();
	    return;
	}

	if (!$stores_stmt->num_rows === 0) {
	    echo "No stores found.\n";
	    print_r($stores_stmt->error);
	    $mysqli->close();
	    return;
	}

	$stores_stmt->bind_result($store);
	$stores = array();

	while ($stores_stmt->fetch()) {
	    $stores[] = $store;
	}

	$stores_stmt->close();
	foreach($stores as $store){

	    switch(strtolower($store)) {
	        case 'unixsurplusnet':
	            $configuration_code = 'ebay_refresh_token_net';
	            break;
	        case 'unixpluscom':
	            $configuration_code = 'ebay_refresh_token_plus';
	            break;
	        case 'itrecyclenow':
	            $configuration_code = 'ebay_refresh_token_itr';
	            break;
	        case 'unixsurpluscom':
	            $configuration_code = 'ebay_refresh_token_com';
	            break;
	    }

	    $refresh_token_query = "SELECT value FROM configuration WHERE code = ?";
	    $refresh_token_stmt = $conn->prepare($refresh_token_query);
	    $refresh_token_stmt->bind_result($refresh_token);
	    $refresh_token_stmt->bind_param('s', $configuration_code);

	    if (!$refresh_token_stmt->execute() && !$refresh_token_stmt->fetch()) {
	        echo "An error occured while loading refresh token for {$store}\n";

	        continue;
	    }

	    if (!$refresh_token_stmt->fetch()) {
	        continue;
	        echo "Acquiring token for {$store}...\n";    
	    }
	    $refresh_token_stmt->close();
		$scope = urlencode('https://api.ebay.com/oauth/api_scope');

		$oauth = refresh_ebay_oauth_token($refresh_token, $scope);

		$ebay_base_url = "https://api.ebay.com/ws/api.dll";
		$ebay_site_id = 0;
		$ebay_api_version = 967;
		$ebay_call_name = "GetFeedback";

		$request_body = api_query_builder_first_page();
		$headers = array(

			"X-EBAY-API-IAF-TOKEN: $oauth",
			"X-EBAY-API-SITEID: $ebay_site_id",
			"X-EBAY-API-COMPATIBILITY-LEVEL: $ebay_api_version",
			"X-EBAY-API-CALL-NAME: $ebay_call_name"
		);

		$curl = curl_init($ebay_base_url);
		    curl_setopt($curl, CURLOPT_POST, true);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$response_body = curl_exec($curl);
		$response = new SimpleXMLElement($response_body);
		$product_review_insert = [];
		$ebay_product_review =array();
		$get_feedback_id = feedback_id($conn);

		if ($response->Ack == "Success"){
			if(empty($response->FeedbackDetailArray->FeedbackDetail)){
				echo "No data available";
			}else{
				foreach ($response->PaginationResult as $total_pages){
					$total_page = $conn->real_escape_string((string) $total_pages->TotalNumberOfPages);
				}
			}
		}
	}
	return $total_page;

}

?>