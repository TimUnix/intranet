<?php
require_once __DIR__.'/database.php';

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

$get_price_low_high_query = "SELECT * FROM listing WHERE price_research_market_low IS NOT NULL OR price_research_market_high IS NOT NULL";
$get_price_low_high_result = $mysqli->query($get_price_low_high_query);

if ($get_price_low_high_result && $get_price_low_high_result->num_rows > 0){
  while ($category_searcher = $get_price_low_high_result->fetch_object()) {
    $sku = $category_searcher->sku;
    $price_research_market_low = $category_searcher->price_research_market_low;
    $price_research_market_high = $category_searcher->price_research_market_high;

    $stmt = $mysqli->prepare("UPDATE ebay_items
                                      SET
                                        price_research_market_low = ?,
                                        price_research_market_high = ?
                                      WHERE
                                          sku = ?"

            );

            $stmt->bind_param('dds', $price_research_market_low, $price_research_market_high, $sku);
            if ($stmt->execute()) {
                            echo "Successful\n";
                        } else {
                            echo "Error: {$stmt->error}\n";
                        
    
    }
  }

  $mysqli->commit();
}

$mysqli->close();