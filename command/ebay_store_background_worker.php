<?php
ini_set("memory_limit", "-1");
set_time_limit(0);

require_once(__DIR__."/ultimate-web-scraper/support/web_browser.php");
require_once(__DIR__."/ultimate-web-scraper/support/tag_filter.php");
require_once(__DIR__."/database.php");

$db = new db();

// Create connection
$conn = mysqli_connect($db->servername(), $db->username(), $db->password(), $db->dbname());
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$ebay_base_url = "https://svcs.ebay.com/services/search/FindingService/v1?";
$ebay_operation_name = "findItemsIneBayStores";
$ebay_service_version = "1.13.0";
$ebay_app_id = "UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2";
$ebay_global_id = "EBAY-US";
$max_count = 16;


if(isset($argv[1])){
    $table = $argv[1];
} else {
    $table = "ebay_store_name";
}
if(isset($argv[2])){
    $column = $argv[2];
} else {
    $column = 'store';
}

if($table === "ebay_store_name"){
    $update_selling_state_query = "update ebay_store set selling_state = 'Inactive'";
    mysqli_query($conn, $update_selling_state_query);
}

$getAllEbayItems = getEbayStore($conn);


$store_name_query = "SELECT $column FROM $table";
$store_name_result = mysqli_query($conn, $store_name_query);

if ($store_name_result->num_rows > 0) {

	$ebay_store_update = array();
	$ebay_store_insert = array();

    while($store = $store_name_result->fetch_assoc()) {
        // output data of each row
        $inventory_to_ebayresrow = 0;

        while($inventory_to_ebayresrow <= $max_count) {
            $inventory_to_ebayresrow++;
            // connect to ebay

            $urlencoded_storename = urlencode($store[$column]);

            $api_url = $ebay_base_url
                     . "OPERATION-NAME=$ebay_operation_name"
                     . "&SECURITY-APPNAME=$ebay_app_id"
                     . "&SERVICE-VERSION=$ebay_service_version"
                     . "&GLOBAL-ID=$ebay_global_id"
                     . "&storeName=$urlencoded_storename"
                     . "&outputSelector=StoreInfo"
                     . "&paginationInput=True"
                     . "&paginationInput.entriesPerPage=100"
                     . "&paginationInput.pageNumber=".$inventory_to_ebayresrow;

            $ebaylinkapi = new SimpleXMLElement($api_url, 0, true);

            if ($ebaylinkapi->ack == "Success") {
                echo "Gathering data on page $inventory_to_ebayresrow for $urlencoded_storename\n";
                
                // If the response was loaded, parse it and build links
                // print_r($resp->searchResult);
                foreach($ebaylinkapi->searchResult->item as $item){
                    $viewitemurl = str_replace(' ', '', $conn->real_escape_string((string) $item->viewItemURL));
                    $itemnumber = $conn->real_escape_string($item->itemId);
                    $galleryurl = str_replace(' ', '', $conn->real_escape_string((string) $item->galleryURL));
                    // Format the price to 2 decimal places if the decimals are greater than .0 and remove them
                    // otherwise
                    $currentprice = floatval(number_format((float) $item->sellingStatus->currentPrice, 2));
                    $sellerusername = $conn->real_escape_string((string) $item->storeInfo->storeName);
                    $categoryId = $conn->real_escape_string($item->primaryCategory->categoryId);
                    $categoryId_nonblank = $categoryId === '' ? -1 : $categoryId;
                    $categoryName = $conn->real_escape_string($item->primaryCategory->categoryName);
                    $patterns = array('/\xE2\x80\x8BCR/', '/\xE2\x80\x8E/');
                    $title = trim(preg_replace($patterns, '', $conn->real_escape_string((string) $item->title)));
                    $shipping_type = $conn->real_escape_string($item->shippingInfo->shippingType);
                    $ship_to_locations = $conn->real_escape_string($item->shippingInfo->shipToLocations);
                    $expedited_shipping = $item->shippingInfo->expeditedShipping == 'true' ? 1 : 0 ;
                    $one_day_shipping_available = $item->shippingInfo->oneDayShippingAvailable == 'true' ? 1 : 0;
                    $handling_time = $conn->real_escape_string($item->shippingInfo->handlingTime);
                    $handling_time_non_blank = $handling_time === '' ? -1 : $handling_time;
                    $top_rated_listing = $item->topRatedListing == 'true' ? 1 : 0;
                    $selling_state = $conn->real_escape_string($item->sellingStatus->sellingState);
                    $condition_id = $conn->real_escape_string($item->condition->conditionId);
                    $condition_id_nonblank = $condition_id === '' ? -1 : $condition_id;
                    $condition_display_name = $conn->real_escape_string($item->condition->conditionDisplayName);

					if (array_key_exists($itemnumber, $getAllEbayItems)) 
					{
                        $ebay_store_update[] = "UPDATE ebay_store
                            SET item_number='$itemnumber',
                            title = '$title',
                            price = '$currentprice',
                            link = '$viewitemurl',
                            img = '$galleryurl',
                            seller = '$sellerusername',
                            category_id = '$categoryId_nonblank',
                            category_name = '$categoryName',
                            shipping_type = '$shipping_type',
                            ship_to_locations = '$ship_to_locations',
                            expedited_shipping = '$expedited_shipping',
                            one_day_shipping_available = '$one_day_shipping_available',
                            top_rated_listing = '$top_rated_listing',
                            handling_time = '$handling_time_non_blank',
                            selling_state = '$selling_state',
                            condition_id = '$condition_id_nonblank',
                            condition_display_name = '$condition_display_name'
                            WHERE id = " . $getAllEbayItems[$itemnumber];
                    }else{
                        $ebay_store = array(
                            $title,
                            $itemnumber,
                            $currentprice,
                            $viewitemurl,
                            $galleryurl,
                            $sellerusername,
                            $categoryId_nonblank,
                            $categoryName,
                            $shipping_type,
                            $ship_to_locations,
                            $expedited_shipping,
                            $one_day_shipping_available,
                            $top_rated_listing,
                            $handling_time_non_blank,
                            $selling_state,
                            $condition_id_nonblank,
                            $condition_display_name
						);

						$ebay_store_insert[] = '"'.implode('","',$ebay_store).'"';
                    }
                }
            }
        }
	}
	
	if (count($ebay_store_update) > 0) {
        $offset = 0;
        $records_to_insert = 20;
        $total_records = count($ebay_store_update);

        while ($offset <= $total_records) {
            $paginated_data = array_slice($ebay_store_update, $offset, $records_to_insert);
            $update = implode("; ", $paginated_data);

            if (mysqli_multi_query($conn, $update)) {
                do {
                    if (mysqli_affected_rows($conn) > 0) {
                        echo "Updated " . mysqli_affected_rows($conn) . " records\n";
                    }
                } while (mysqli_next_result($conn));
            }

            $offset += $records_to_insert;
        }
	}

	if(count($ebay_store_insert) > 0){
        $offset = 0;
        $records_to_insert = 20;
        $total_records = count($ebay_store_insert);

        while ($offset <= $total_records) {
            $paginated_data = array_slice($ebay_store_insert, $offset, $records_to_insert);
            $insert = "INSERT INTO ebay_store (
			title,
			item_number,
			price,
			link,
			img,
			seller,
			category_id,
			category_name,
			shipping_type,
			ship_to_locations,
			expedited_shipping,
			one_day_shipping_available,
			top_rated_listing,
			handling_time,
			selling_state,
			condition_id,
			condition_display_name
			) VALUES (".implode('),(', $paginated_data ).")";

            $stmt = mysqli_prepare($conn, $insert);

            if (mysqli_stmt_execute($stmt)) {
                echo "Successfully inserted {$stmt->affected_rows} records\n";
            } else {
                echo "Error inserting set.\n";
                print_r(mysqli_error_list($conn));
            }

            $offset += $records_to_insert;
        }
	}
}

mysqli_close($conn);

function getEbayStore($conn){
	$sql = "SELECT id, item_number from ebay_store";
	$data = mysqli_query($conn, $sql);
	
	$storage_array = array();
	if(!empty($data)){
		foreach($data as $key => $value){
			$storage_array[$value['item_number']] = $value['id'];
		}
	}

    return $storage_array;
}

function get_rwidgets_from_ebay($url) {
    // Retrieve the standard HTML parsing array for later use.
    $htmloptions = TagFilter::GetHTMLOptions();

    // Retrieve a URL (emulating Firefox by default).
    $web = new WebBrowser();
    $result = $web->Process($url);

    // Check for connectivity and response errors.
    if (!$result["success"]) {
	    echo "Error retrieving URL.  " . $result["error"] . "\n";
	    exit();
    }

    if ($result["response"]["code"] != 200) {
	    echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
	    exit();
    }

    // Get the final URL after redirects.
    $baseurl = $result["url"];

    $html = TagFilter::Explode($result["body"], $htmloptions);

    // Retrieve a pointer object to the root node.
    $root = $html->Get();

    $scripts = $root->Find("script");

    $found_script = "";

    foreach ($scripts as $script) {
        if (strpos($script, "rwidgets")) {
            $found_script = $script;
        }
    }

    if ($found_script === "") {
        echo "Couldn't find \$rwidgets";
        return "";
    }

    $rwidgets = "";

    foreach (preg_split("/((\r?\n)|(\r\n?))/", $found_script) as $line){
        if (strpos($line, "rwidgets")) {
            $rwidgets = $line;
        }
    }

    if ($rwidgets === "") {
        echo "Couldn't find \$rwidgets";
    }

    $rwidgets = str_replace("\$rwidgets(", "", $rwidgets);
    $rwidgets = preg_replace("/\);new .*$/", "", $rwidgets);

    return $rwidgets;
}

?>
