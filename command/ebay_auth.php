<?php

function generate_application_access_token($scope = null) {
    $url = "https://api.ebay.com/identity/v1/oauth2/token";

    $client_id = "UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2";
    $client_secret = "PRD-81f16f178339-a79c-482c-9342-4c37";

    $encoded_credentials = base64_encode("$client_id:$client_secret");
    $content_type = "application/x-www-form-urlencoded";

    $grant_type = "client_credentials";

    if (!isset($scope)) {
        $scope = urlencode("https://api.ebay.com/oauth/api_scope");
    }

    $curl = curl_init($url);

    $headers = array(
        "Content-Type: $content_type",
        "Authorization: Basic $encoded_credentials"
    );

    $request_body = "grant_type=$grant_type&scope=$scope";

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response_body = curl_exec($curl);
    $response = json_decode($response_body);

    curl_close($curl);

    return $response->access_token;
}

function refresh_ebay_oauth_token($refresh_token, $scope = null) {
    $url = 'https://api.ebay.com/identity/v1/oauth2/token';

    $client_id = 'UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2';
    $client_secret = 'PRD-81f16f178339-a79c-482c-9342-4c37';
    $redirect_uri = 'UNIXSurplus__In-UNIXSurp-itrecy-lereszia';

    $content_type = 'application/x-www-form-urlencoded';
    $oauth_credentials = base64_encode("$client_id:$client_secret");
    $grant_type = 'refresh_token';

    if (!isset($scope)) {
        $scope = urlencode('https://api.ebay.com/oauth/api_scope/sell.account.readonly');
    }

    $headers = array(
        "Content-Type: $content_type",
        "Authorization: Basic $oauth_credentials"
    );

    $request_body = "grant_type=$grant_type&refresh_token=$refresh_token&scope=$scope";

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $request_body);

    $response = curl_exec($curl);

    curl_close($curl);

    return $response;
}
