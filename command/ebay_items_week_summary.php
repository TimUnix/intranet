<?php
require_once __DIR__.'/database.php';

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if (!$mysqli) {
    die("Connection failed: " . $mysqli->connect_error());
}

echo "Acquiring item IDs...\n";

$acquire_item_ids_query = "SELECT `item_number` FROM `ebay_items`";
$acquire_item_ids_stmt = $mysqli->prepare($acquire_item_ids_query);

if (!$acquire_item_ids_stmt || !$acquire_item_ids_stmt->execute()) {
    echo "Failed to acquire item IDs: ";
    print_r($mysqli->error);
    $mysqli->close();
    die();
}

$item_ids_result = $acquire_item_ids_stmt->get_result();

if (!$item_ids_result) {
    echo "Failed to acquire item IDs: ";
    print_r($mysqli->error);
    $mysqli->close();
    die();
}

if ($item_ids_result->num_rows === 0) {
    echo "No items found\n";
    $mysqli->close();
    die();
}

$total_items = $item_ids_result->num_rows;
$offset = 0;
$page_size = 15;
$items = $item_ids_result->fetch_all(MYSQLI_ASSOC);

$ebay_items_update = array();

while ($offset <= $total_items) {
    $paginated_data = array_slice($items, $offset, $page_size);
    $item_count = count($paginated_data);

    $query_items = array_map(function($item) {
        return $item['item_number'];
    }, $paginated_data);

    echo "Acquiring data from the last 7 days for {$item_count} items...\n";

    foreach($query_items as $item_number) {
        $acquire_archive_data_query = "SELECT `quantity_sold`, `hit_count`, `current_price` "
                                    . "FROM `ebay_items_archive` "
                                    . "WHERE `date` > DATE(NOW() - INTERVAL 7 DAY) "
                                    . "AND `item_number` = '$item_number' "
                                    . "ORDER BY `date`";
        $acquire_archive_data_stmt = $mysqli->prepare($acquire_archive_data_query);

        if (!$acquire_archive_data_stmt || !$acquire_archive_data_stmt->execute()) {
            echo "Failed to acquire data from the last 7 days: {$mysqli->error}\n";
            $mysqli->close();
            die();
        }

        $archive_data = $acquire_archive_data_stmt->get_result()->fetch_all(MYSQLI_ASSOC);

        $acquire_data_query  = "SELECT `quantity_sold`, `hit_count`, `current_price` "
                             . "FROM `ebay_items` "
                             . "WHERE `item_number` = '$item_number' "
                             . "ORDER BY `date`";
        $acquire_data_stmt = $mysqli->prepare($acquire_data_query);

        if (!$acquire_data_stmt || !$acquire_data_stmt->execute()) {
            echo "Failed to acquire data from the last 7 days: {$mysqli->error}\n";
            $mysqli->close();
            die();
        }

        $main_data = $acquire_data_stmt->get_result()->fetch_all(MYSQLI_ASSOC);
       
        $hits = array();
        $sold = array();

        $hits = array_map(function($data) {
            return $data['hit_count'];
        }, $archive_data, $main_data);

        $previous_hit = $hits[0];

        $calculated_hits = array_map(function($hit) use (&$previous_hit) {
            
            $calculated_hit = $hit - $previous_hit;
            $previous_hit = $hit;

            return $calculated_hit;
        }, $hits);

        $total_hits = array_reduce($calculated_hits, function($accumulated_hits, $hit) {
            return $accumulated_hits + $hit;
        });


        $quantities_sold = array_map(function($data) {
            return $data['quantity_sold'];
        }, $archive_data, $main_data);

        $previous_quantity_sold = $quantities_sold[0];

        $calculated_quantities_sold = array_map(function($sold) use (&$previous_quantity_sold) {
            $calculated_quantity_sold = $sold - $previous_quantity_sold;
            $previous_quantity_sold = $sold;

            return $calculated_quantity_sold;
        }, $quantities_sold);

        $total_sold = array_reduce($calculated_quantities_sold, function($accumulated_sold, $sold) {
            return $accumulated_sold + $sold;
        });

        $current_prices = array_map(function($data){
            return $data['current_price'];
        }, $main_data);

        $current_price = $current_prices[0];
        $current_price = floatval($current_price);

        $net_sold = $current_price*$total_sold;
        $net_sold = floatval($net_sold);

        $ebay_items_update[] = "UPDATE `ebay_items` "
                             . "SET `total_sold_7_days` = '$total_sold', "
                             . "`total_hits_7_days` = '$total_hits', "
                             . "`net_sold_7_days` = '$net_sold'"
                             . "WHERE item_number = '$item_number';";
    }
    
    $offset += $page_size;
}

$total_items = count($ebay_items_update);

if ($total_items === 0) {
    echo "No records to add...\n";
    $mysqli->close();
    die();
}

$page_size = 50;
$offset = 0;

$records_updated = 0;

while ($offset <= $total_items) {
    $paginated_update = array_slice($ebay_items_update, $offset, $page_size);
    $update_query = implode(" ", $paginated_update);

    if ($mysqli->multi_query($update_query)) {
        do {
            if ($mysqli->affected_rows > 0) {
                echo "Updated {$mysqli->affected_rows} record(s)\n";
                $records_updated += $mysqli->affected_rows;
            } else {
                echo "No records to update\n";
            }
        } while($mysqli->next_result());
    }

    $offset += $page_size;
}

echo "Total item count: $total_items\n";
echo "Total recourds updated: $records_updated";

$mysqli->close();
?>
