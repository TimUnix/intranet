<?php
require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../database.php';
require_once __DIR__.'/utils.php';


$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

$project_root_result = $mysqli->query("SELECT `value` FROM `configuration` WHERE `code` = 'project_root' LIMIT 0, 1");
$project_root = $project_root_result->fetch_object()->value;

if (!$project_root_result && $project_root_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire project root.\n");
}

$xml_dirs = array();

$xmls_parent_dir = "$project_root/nebula/certificates/xml";
$xml_dir_contents = scandir($xmls_parent_dir);
$xml_purchase_orders = array_filter($xml_dir_contents, function($file) use ($xmls_parent_dir) {
    $file_path = "$xmls_parent_dir/$file";
    return is_dir($file_path) && !in_array($file, array('.', '..'));
});

foreach ($xml_purchase_orders as $purchase_order_dir) {
    $dir_contents = scandir("$xmls_parent_dir/$purchase_order_dir");

    $lots = array_filter($dir_contents, function($file) use ($xmls_parent_dir, $purchase_order_dir) {
        return is_dir("$xmls_parent_dir/$purchase_order_dir/$file") && !in_array($file, array('.', '..'));
    });

    $xml_dirs[] = $purchase_order_dir;

    foreach ($lots as $lot) {
        $xml_dirs[] = "$purchase_order_dir/$lot";
    }
}

foreach ($xml_dirs as $dir_name) {
    $xml_dir = "$xmls_parent_dir/$dir_name";

    $xmls = scandir($xml_dir);
    $xmls = array_filter($xmls, function($file) use ($xml_dir) {
        return !is_dir("$xml_dir/$file") && !in_array($file, array('.', '..'));
    });

    if (count($xmls) === 0) {
        echo "No xml reports found in $dir_name. Continuing...\n";
        continue;
    }

    $mysqli->begin_transaction();

    foreach ($xmls as $file) {
        $filename = pathinfo($file);

        if (!isset($filename['extension']) || (isset($filename['extension']) && $filename['extension'] !== 'xml'))
            continue;

        try {
            $file_contents = simplexml_load_file("$xml_dir/$file");
            $report = create_report($file_contents, $file, $mysqli);

            $dir_name_split = explode('/', $dir_name);
            $purchase_order = $dir_name_split[0];
            $lot = $dir_name_split[1] ?? null;
            save_report($mysqli, $report, $purchase_order, isset($lot) ? "$purchase_order $lot" : null);
        } catch (Exception $e) {
            continue;
        }
    }

    $mysqli->commit();
}

echo "Searching pdfs without xml...\n";

$serial_numbers_reference_result = $mysqli->query("SELECT device_serial_number FROM nebula");

if (!$serial_numbers_reference_result) {
    $serial_numbers_reference_result->free();
    $mysqli->close();
    exit("Unable to acquire serial numbers from Nebula\n");
}

$serial_numbers_reference = $serial_numbers_reference_result->fetch_all(MYSQLI_ASSOC);
$serial_numbers_reference = array_map(function($reference) {
    return $reference['device_serial_number'];
}, $serial_numbers_reference);
$serial_numbers_reference_result->free();

$pdf_dir = "$project_root/nebula/certificates/pdf";
$pdf_dirs = scandir($pdf_dir);
$pdf_dirs = array_filter($pdf_dirs, function($dir) use ($pdf_dir) {
    return is_dir("$pdf_dir/$dir") && $dir !== '.' && $dir !== '..';
});

if (!$pdf_dirs) {
    $mysqli->close();
    exit("No pdf certificate directories found. Exiting...\n");
}

foreach ($pdf_dirs as $dir) {
    echo "Currently checking $dir\n";

    $pdfs = scandir("$pdf_dir/$dir");
    $pdfs = array_filter($pdfs, function($file) use ($pdf_dir, $dir) {
        return !is_dir("$pdf_dir/$dir/$file");
    });

    $pdfs_without_xml = array_filter($pdfs, function($pdf) use ($serial_numbers_reference) {
        $is_xml_available = false;

        foreach($serial_numbers_reference as $serial_number) {
            $is_xml_available = $is_xml_available || strpos($pdf, $serial_number) !== false;
        }

        return $is_xml_available;
    });

    $mysqli->begin_transaction();

    foreach ($pdfs_without_xml as $pdf) {
        try {
            $report = extract_report_info($pdf);
        } catch (Exception $e) {
            continue;
        }

        $filename = str_replace('.pdf', '.xml', str_replace('Report', 'Certificate', $pdf));
        $title = 'Disk Erase';

        if (in_array($report['serial'], $serial_numbers_reference)) {
            continue;
        }

        $stmt = $mysqli->prepare('INSERT INTO `nebula` ('
                             . '`filename`, '
                             . '`created`, '
                             . '`parent_dir`, '
                             . '`device_serial_number`, '
                             . '`title`, '
                             . '`result`, '
                             . '`process_result`) VALUES (?,?,?,?,?,?,?)');

        $stmt->bind_param('sssssss', $filename, $report['date'], $dir, $report['serial'], $title, $report['result'],
                          $report['result']);

        if ($stmt->execute()) {
            echo "Successfully added report for {$report['serial']}.\n";
        } else {
            echo "Failed to add report for {$report['serial']}.\n";
        }
    }

    $mysqli->commit();

    foreach ($pdfs as $pdf) {
        rename("$pdf_dir/$dir/$pdf", "$pdf_dir/$pdf");
    }
}

$mysqli->close();

function extract_report_info($filename) {
    $pdf = pathinfo($filename);
    $filename_split = explode('-', $pdf['filename']);

    $max_index = count($filename_split) - 1;
    $date_found = false;
    $report = array();
    $serial_last_index = -1;
    $date_start_index = -1;

    foreach ($filename_split as $index => $component) {
        $previous_index = $index - 1;
        $next_index = $index + 1;

        if ($previous_index < 0 || $next_index > $max_index) {
            continue;
        }

        if (in_array($filename_split[$previous_index], array( 'Report', 'Certificate' ))) {
            if (in_array($filename_split[$next_index], array('Errors', 'Success', 'Terminated'))) {
                $report['serial'] = $component;
                $serial_last_index = $index;
            } else {
                $report['serial'] = "$component-{$filename_split[$next_index]}";
                $serial_last_index = $next_index;
            }
        }

        if (isset($report['serial']) && $serial_last_index >= 0 && $index > $serial_last_index &&
            !isset($report['result'])) {
            $report['result'] = ($component === 'Success' ? 'Erased' :
                ($component === 'Errors' ? 'Erased with errors' :
                 $component));
            $date_start_index = $index + 1;
        }
    }

    $date_values = implode('-', array_slice($filename_split, $date_start_index));
    $date = date_create_from_format('Y-m-d-H-i-s', $date_values);

    if (!$date) throw new Exception('Invalid date format');

    $report['date'] = $date->format('Y-m-d H:i:s');

    return $report;
}

function create_report($file, $filename, $mysqli) {
    $report = new stdClass();

    $report->filename = $mysqli->real_escape_string($filename);
    $report->created = log_date_to_timestamp((string) $file->attributes()[0]);
    $report->title = $mysqli->real_escape_string((string) $file->title);
    $report->erase_method = $mysqli->real_escape_string((string) $file->erase->attributes()[0]);
    $report->process_integrity = $mysqli->real_escape_string(
        (string) $file->{'kill-disk'}->{'process-integrity'}
    );
    $report->fingerprint = $mysqli->real_escape_string((string) $file->{'kill-disk'}->fingerprint);
    $report->conclusion = $mysqli->real_escape_string((string) $file->conclusion);

    $report->device_title = $mysqli->real_escape_string((string) $file->device->title);
    $device_serial_number_unchecked = (string) $file->device->{'serial-number'};
    $report->device_serial_number = mb_check_encoding($device_serial_number_unchecked, 'utf-8')
                                  ? $mysqli->real_escape_string($device_serial_number_unchecked) : null;
    $report->device_platform_name = $mysqli->real_escape_string((string) $file->device->platformname);
    $report->device_product_name = $mysqli->real_escape_string((string) $file->device->product);
    $report->device_type = $mysqli->real_escape_string((string) $file->device->type);
    $report->device_revision = $mysqli->real_escape_string((string) $file->device->revision);
    $report->device_size = $mysqli->real_escape_string((string) $file->device->size);

    if ((int) $file->device->{'smart-parameters'}->count() > 0) {
        $report->smart_firmware_version = $mysqli->real_escape_string(
            (string) $file->device->{'smart-parameters'}->param[2]
        );
        $report->smart_capacity = $mysqli->real_escape_string((string) $file->device->{'smart-parameters'}->param[3]);
        $report->smart_ata_version = (int) $file->device->{'smart-parameters'}->param[4];
        $report->smart_ata_standard = $mysqli->real_escape_string(
            (string) $file->device->{'smart-parameters'}->param[5]
        );
        $report->smart_support = $mysqli->real_escape_string((int) $file->device->{'smart-parameters'}->param[6]);
        $report->smart_offline_data_collection_status = (int) $file->device->{'smart-parameters'}->param[7];
        $report->smart_self_test_execution_status = (int) $file->device->{'smart-parameters'}->param[8];
        $report->smart_time_offline_data_collection_sec = (int) $file->device->{'smart-parameters'}->param[9];
        $report->smart_offline_data_collection_capabilities = (int) $file->device->{'smart-parameters'}->param[10];
        $report->smart_capabilities = (int) $file->device->{'smart-parameters'}->param[11];
        $report->smart_error_logging_capabilities = (int) $file->device->{'smart-parameters'}->param[12];
        $report->smart_short_self_test_time_min = (int) $file->device->{'smart-parameters'}->param[13];
        $report->smart_extended_self_test_time_min = (int) $file->device->{'smart-parameters'}->param[14];
    } else {
        $report->smart_firmware_version = null;
        $report->smart_capacity = null;
        $report->smart_ata_version = null;
        $report->smart_ata_standard = null;
        $report->smart_support = null;
        $report->smart_offline_data_collection_status = null;
        $report->smart_self_test_execution_status = null;
        $report->smart_time_offline_data_collection_sec = null;
        $report->smart_offline_data_collection_capabilities = null;
        $report->smart_capabilities = null;
        $report->smart_error_logging_capabilities = null;
        $report->smart_short_self_test_time_min = null;
        $report->smart_extended_self_test_time_min = null;
    }

    $report->geometry_partitioning = $mysqli->real_escape_string((string) $file->device->geometry->partitioning);
    $report->geometry_total_sec = (int) str_replace(',', '', $file->device->geometry->{'total-sec'});
    $report->geometry_first_sec = (int) str_replace(',', '', $file->device->geometry->{'first-sec'});
    $report->geometry_bps = (int) str_replace(',', '', $file->device->geometry->bps);

    $report->results_started = log_date_to_timestamp((string) $file->results->started);
    $report->results_elapsed = $mysqli->real_escape_string((string) $file->results->elapsed);
    $report->results_errors = $mysqli->real_escape_string((string) $file->results->process->errors);
    $report->result = $mysqli->real_escape_string((string) $file->results->result);

    $report->process_name = $mysqli->real_escape_string((string) $file->results->process->name);
    $report->process_started_at = log_date_to_timestamp((string) $file->results->process->started);
    $report->process_elapsed = $mysqli->real_escape_string((string) $file->results->process->elapsed);
    $report->process_result = $mysqli->real_escape_string((string) $file->results->process->result);

    return $report;
}

function save_report($mysqli, $report, $parent_dir, $lot) {
    $file_check_query = "SELECT `id`, `created`, parent_dir FROM `nebula` WHERE `device_serial_number` = ? LIMIT 1";
    $file_check_stmt = $mysqli->prepare($file_check_query);
    $file_check_stmt->bind_param('s', $report->device_serial_number);
    $file_check_stmt->execute();
    $file_check_result = $file_check_stmt->get_result();

    if ($file_check_result && $file_check_result->num_rows > 0) {
        $report_found = $file_check_result->fetch_object();

        $date_reference = strtotime($report_found->created);
        $date_comparator = strtotime($report->created);

        $is_file_outdated = $report_found->parent_dir !== 'PO79049X' &&
            ($parent_dir !== $report_found->parent_dir || $date_reference < $date_comparator);

        if (!$is_file_outdated) {
            echo "{$report_found->parent_dir}/{$report->device_serial_number} already up to date.\n";
            return;
        }

        $id = $report_found->id;
        $stmt = $mysqli->prepare("
                    UPDATE `nebula`
                    SET
                         `created` = ?,
                         `parent_dir` = ?,
                         `lot` = ?,
                         `title` = ?,
                         `erase_method` = ?,
                         `process_integrity` = ?,
                         `fingerprint` = ?,
                         `device_title` = ?,
                         `device_serial_number` = ?,
                         `device_platform_name` = ?,
                         `device_product_name` = ?,
                         `device_type` = ?,
                         `device_revision` = ?,
                         `device_size` = ?,
                         `smart_firmware_version` = ?,
                         `smart_capacity` = ?,
                         `smart_ata_version` = ?,
                         `smart_ata_standard` = ?,
                         `smart_support` = ?,
                         `smart_offline_data_collection_status` = ?,
                         `smart_self_test_execution_status` = ?,
                         `smart_time_offline_data_collection_sec` = ?,
                         `smart_offline_data_collection_capabilities` = ?,
                         `smart_capabilities` = ?,
                         `smart_error_logging_capabilities` = ?,
                         `smart_short_self_test_time_min` = ?,
                         `smart_extended_self_test_time_min` = ?,
                         `geometry_partitioning` = ?,
                         `geometry_total_sec` = ?,
                         `geometry_first_sec` = ?,
                         `geometry_bps` = ?,
                         `results_started` = ?,
                         `results_elapsed` = ?,
                         `results_errors` = ?,
                         `result` = ?,
                         `process_name` = ?,
                         `process_started_at` = ?,
                         `process_elapsed` = ?,
                         `process_result` = ?,
                         `conclusion` = ?
                     WHERE
                         `id` = ?;
                    ");

        $stmt->bind_param('ssssssssssssssssisiiiiiiiiisiiisssssssssi',
                          $report->created,
                          $parent_dir,
                          $lot,
                          $report->title,
                          $report->erase_method,
                          $report->process_integrity,
                          $report->fingerprint,
                          $report->device_title,
                          $report->device_serial_number,
                          $report->device_platform_name,
                          $report->device_product_name,
                          $report->device_type,
                          $report->device_revision,
                          $report->device_size,
                          $report->smart_firmware_version,
                          $report->smart_capacity,
                          $report->smart_ata_version,
                          $report->smart_ata_standard,
                          $report->smart_support,
                          $report->smart_offline_data_collection_status,
                          $report->smart_self_test_execution_status,
                          $report->smart_time_offline_data_collection_sec,
                          $report->smart_offline_data_collection_capabilities,
                          $report->smart_capabilities,
                          $report->smart_error_logging_capabilities,
                          $report->smart_short_self_test_time_min,
                          $report->smart_extended_self_test_time_min,
                          $report->geometry_partitioning,
                          $report->geometry_total_sec,
                          $report->geometry_first_sec,
                          $report->geometry_bps,
                          $report->results_started,
                          $report->results_elapsed,
                          $report->results_errors,
                          $report->result,
                          $report->process_name,
                          $report->process_started_at,
                          $report->process_elapsed,
                          $report->process_result,
                          $report->conclusion,
                          $id
        );

        if ($stmt->execute()) {
            echo "Successfully updated {$report->device_serial_number}\n";
        } else {
            echo "Failed to update {$report->device_serial_number}\n";
        }
    } else {
        $stmt = $mysqli->prepare("
                 INSERT INTO `nebula`(
                         `filename`,
                         `created`,
                         `parent_dir`,
                         `lot`,
                         `title`,
                         `erase_method`,
                         `process_integrity`,
                         `fingerprint`,
                         `device_title`,
                         `device_serial_number`,
                         `device_platform_name`,
                         `device_product_name`,
                         `device_type`,
                         `device_revision`,
                         `device_size`,
                         `smart_firmware_version`,
                         `smart_capacity`,
                         `smart_ata_version`,
                         `smart_ata_standard`,
                         `smart_support`,
                         `smart_offline_data_collection_status`,
                         `smart_self_test_execution_status`,
                         `smart_time_offline_data_collection_sec`,
                         `smart_offline_data_collection_capabilities`,
                         `smart_capabilities`,
                         `smart_error_logging_capabilities`,
                         `smart_short_self_test_time_min`,
                         `smart_extended_self_test_time_min`,
                         `geometry_partitioning`,
                         `geometry_total_sec`,
                         `geometry_first_sec`,
                         `geometry_bps`,
                         `results_started`,
                         `results_elapsed`,
                         `results_errors`,
                         `result`,
                         `process_name`,
                         `process_started_at`,
                         `process_elapsed`,
                         `process_result`,
                         `conclusion`
                     ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        $stmt->bind_param('sssssssssssssssssisiiiiiiiiisiiisssssssss',
                          $report->filename,
                          $report->created,
                          $parent_dir,
                          $lot,
                          $report->title,
                          $report->erase_method,
                          $report->process_integrity,
                          $report->fingerprint,
                          $report->device_title,
                          $report->device_serial_number,
                          $report->device_platform_name,
                          $report->device_product_name,
                          $report->device_type,
                          $report->device_revision,
                          $report->device_size,
                          $report->smart_firmware_version,
                          $report->smart_capacity,
                          $report->smart_ata_version,
                          $report->smart_ata_standard,
                          $report->smart_support,
                          $report->smart_offline_data_collection_status,
                          $report->smart_self_test_execution_status,
                          $report->smart_time_offline_data_collection_sec,
                          $report->smart_offline_data_collection_capabilities,
                          $report->smart_capabilities,
                          $report->smart_error_logging_capabilities,
                          $report->smart_short_self_test_time_min,
                          $report->smart_extended_self_test_time_min,
                          $report->geometry_partitioning,
                          $report->geometry_total_sec,
                          $report->geometry_first_sec,
                          $report->geometry_bps,
                          $report->results_started,
                          $report->results_elapsed,
                          $report->results_errors,
                          $report->result,
                          $report->process_name,
                          $report->process_started_at,
                          $report->process_elapsed,
                          $report->process_result,
                          $report->conclusion
        );

        if ($stmt->execute()) {
            echo "Successfully saved {$report->device_serial_number}.\n";
        } else {
            echo "Failed to save {$report->device_serial_number}\n";
            echo "{$mysqli->error}\n";
        }
    }
}
