<?php

require_once __DIR__ . "/../../vendor/autoload.php";
require_once __DIR__.'/../database.php';

use phpseclib3\Net\SFTP;

$db = create_mysqli_connection();

if (!$db) {
    die('Connection failed: ' . mysqli_connect_error());
}

$project_root_result = $db->query("SELECT value FROM configuration WHERE code = 'project_root'");

if (!$project_root_result) {
    $db->close();
    die("Failed to acquire project root. Exiting...\n");
}

$project_root = $project_root_result->fetch_object()->value;

$serial_numbers_csv = file_get_contents('nebula_not_in_db.csv');
$serial_numbers = explode("\n", $serial_numbers_csv);
array_shift($serial_numbers);
$serial_numbers = array_map(function($serial_number) { return trim($serial_number); }, $serial_numbers);

$ip_result = $db->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_ip' LIMIT 0, 1");

if (!$ip_result && $ip_result->num_rows === 0) {
    echo "Couldn't acquire nebula's IP.\n";
    $db->close();
    return;
}

$port_result = $db->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_port' LIMIT 0, 1");

if (!$port_result && $port_result->num_rows === 0) {
    echo "Couldn't acquire nebula's IP.\n";
    $db->close();
    return;
}

$username_result = $db->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_username' LIMIT 0, 1");

if (!$username_result && $username_result->num_rows === 0) {
    echo "Couldn't acquire nebula's username.\n";
    $db->close();
    return;
}

$password_result = $db->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_password' LIMIT 0, 1");

if (!$password_result && $password_result->num_rows === 0) {
    echo "Couldn't acquire nebula's password.\n";
    $db->close();
    return;
}

$nebula_ip = $ip_result->fetch_object()->value;
$nebula_port = $port_result->fetch_object()->value;
$nebula_username = $username_result->fetch_object()->value;
$nebula_password = $password_result->fetch_object()->value;

$sftp = new SFTP($nebula_ip, $nebula_port);

if (!$sftp->login($nebula_username, $nebula_password)) {
    echo "Failed to authenticate to the remote server\n";
    $db->close();
    return;
}

echo "Successfully logged in.\n";

$sftp->chdir("/home/nebula/certificates");
$certificates_dirs = $sftp->nlist();

$blacklist = array("killdisk", "Nebula", ".", "..");

$filtered_dirs = array_filter($certificates_dirs, function($dir) use ($blacklist) {
    return !in_array($dir, $blacklist);
});

$dirs = array_values($filtered_dirs);

foreach ($dirs as $dir) {
    $not_in_db_dir = "$project_root/nebula/certificates/not_in_db";

    if (!is_dir($not_in_db_dir)) {
        mkdir($not_in_db_dir, 0777, true);
    }

    $sftp->chdir("/home/nebula/certificates/$dir");
    $remote_files = $sftp->nlist();

    echo "Finding files in $dir...\n";

    foreach($remote_files as $file) {
        $filename = pathinfo($file);
        
        if (!in_array($file, array('.', '..')) && $filename['extension'] === 'pdf') {
            $filename_split = explode('-', $file);
            $serial_number = $filename_split[1];

            if (in_array($serial_number, $serial_numbers)) {
                echo "Found $serial_number. Downloading...";

                if ($sftp->get($file, "$not_in_db_dir/$file")) {
                    echo "Success!\n";
                } else {
                    echo "Failed\n";
                }
            }
        }
    }
}

$db->close();
