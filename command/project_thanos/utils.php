<?php

function log_date_to_timestamp($date) {
    $date_created = date_create_from_format('d/m/Y H:i:s', $date);

    if (!$date_created) return null;
    
    return date_format(
        $date_created,
        'Y-m-d H:i:s'
    );
}
