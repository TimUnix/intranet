<?php

/*
 * A remote ssh server
 */
class Remote {
    public $ip;
    public $port;
    public $username;
    public $password;
    public $location;

    function __construct($ip, $port, $username, $password, $location) {
        $this->ip = $ip;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->location = $location;
    }
}
