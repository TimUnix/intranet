<?php
require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../database.php';
require_once __DIR__.'/nebula/remote.php';

use phpseclib3\Net\SFTP;

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

$ip_result = $mysqli->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_ip' LIMIT 0, 1");

if (!$ip_result && $ip_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire nebula's IP.\n");
}

$merced_ip_result = $mysqli->query(
    "SELECT `value` FROM `configuration` WHERE `code` = 'nebula_merced_ip' LIMIT 0, 1"
);

if (!$merced_ip_result && $merced_ip_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire Merced nebula's IP.\n");
}

$port_result = $mysqli->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_port' LIMIT 0, 1");

if (!$port_result && $port_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire nebula's port.\n");
}

$merced_port_result = $mysqli->query(
    "SELECT `value` FROM `configuration` WHERE `code` = 'nebula_merced_port' LIMIT 0, 1"
);

if (!$merced_port_result && $merced_port_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire Merced nebula's port.\n");
}

$username_result = $mysqli->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_username' LIMIT 0, 1");

if (!$username_result && $username_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire nebula's username.\n");
}

$password_result = $mysqli->query("SELECT `value` FROM `configuration` WHERE `code` = 'nebula_password' LIMIT 0, 1");

if (!$password_result && $password_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire nebula's password.\n");
}

$project_root_result = $mysqli->query("SELECT `value` FROM `configuration` WHERE `code` = 'project_root' LIMIT 0, 1");

if (!$project_root_result && $project_root_result->num_rows === 0) {
    $mysqli->close();
    exit("Couldn't acquire project root.\n");
}

$nebula_ip = $ip_result->fetch_object()->value;
$nebula_port = $port_result->fetch_object()->value;
$nebula_merced_ip = $merced_ip_result->fetch_object()->value;
$nebula_merced_port = $merced_port_result->fetch_object()->value;
$nebula_username = $username_result->fetch_object()->value;
$nebula_password = $password_result->fetch_object()->value;
$project_root = $project_root_result->fetch_object()->value;

$remotes = array(
    new Remote($nebula_ip, $nebula_port, $nebula_username, $nebula_password, 'staclara'),
    new Remote($nebula_merced_ip, $nebula_merced_port, $nebula_username, $nebula_password, 'merced')
);

foreach ($remotes as $remote) {
    $sftp = new SFTP($remote->ip, $remote->port);

    try {
        if (!$sftp->login($remote->username, $remote->password)) {
            echo "Failed to authenticate to the remote server\n";
            continue;
        }
    } catch (Exception $e) {
        echo "Failed to authenticate to the remote server\n";
        continue;
    }

    echo "Succesfully logged in.\n";

    $dirs = acquire_remote_dir_names($sftp);
    // save_purchase_order_info($mysqli, $dirs, $remote->location);

    foreach ($dirs as $dir) {
        download_reports($project_root, $dir, $sftp);
    }
}

$mysqli->close();

function download_reports($project_root, $dir, $sftp) {
    $destination_dir = preg_replace('/[^A-Za-z0-9\-\/]/', '', $dir);
    $pdf_dir = "$project_root/nebula/certificates/pdf/$destination_dir";
    $xml_dir = "$project_root/nebula/certificates/xml/$destination_dir";

    if (!is_dir($pdf_dir)) {
        mkdir($pdf_dir, 0755, true);
    }

    if (!is_dir($xml_dir)) {
        mkdir($xml_dir, 0755, true);
    }

    $stored_xmls = scandir($xml_dir);
    $stored_xmls = array_filter($stored_xmls, function($file) {
        return !in_array($file, array('.', '..'));
    });

    $stored_pdfs = scandir($pdf_dir);
    $stored_pdfs = array_filter($stored_pdfs, function($file) {
        return !in_array($file, array('.', '..'));
    });

    $remote_dir = "/home/nebula/certificates/$dir";
    $remote_files = $sftp->nlist($remote_dir);

    echo "Downloading reports from $dir...\n";

    foreach ($remote_files as $file) {
        if (in_array($file, $stored_xmls) || in_array($file, $stored_pdfs)) {
            echo "$file already downloaded\n";
            continue;
        }

        $filename = pathinfo($file);
        $finfo = new finfo(FILEINFO_MIME_TYPE);

        try {
            $file_contents = $sftp->get("$remote_dir/$file");
        } catch (Exception $e) {
            echo "Error determining mimetype: {$e->getMessage()}";
            continue;
        }

        $mimetype = $finfo->buffer($file_contents);

        if (isset($filename['extension']) && $filename['extension'] === 'xml'
            && $mimetype && ($mimetype === 'application/xml' || $mimetype === 'text/xml')) {
            echo "Downloading $dir/$file...";

            try {
                if ($sftp->get("$remote_dir/$file", "$xml_dir/$file")) {
                    echo " Success!\n";
                } else {
                    echo " Failed\n";
                }
            } catch (Exception $e) {
                echo " Unexpected Error\n{$e->getMessage()}";
                continue;
            }
        }

        if (isset($filename['extension']) && $filename['extension'] === 'pdf'
            && $mimetype && $mimetype === 'application/pdf') {
            echo "Downloading $dir/$file...";

            try {
                if ($sftp->get("$remote_dir/$file", "$pdf_dir/$file")) {
                    echo " Success!\n";
                } else {
                    echo " Failed\n";
                }
            } catch (Exception $e) {
                echo " Unexpected Error\n{$e->getMessage()}";
                continue;
            }
        }
    }
}

function acquire_remote_dir_names($sftp) {
    $certificates_dir = "/home/nebula/certificates";

    $sftp->chdir($certificates_dir);
    $certificates_dir_contents = $sftp->nlist();

    $blacklist = array("killdisk", "Nebula", ".", "..", "System Volume Information", "New Folder");

    $purchase_order_dirs = array_filter($certificates_dir_contents, function($dir) use ($blacklist) {
        return !in_array($dir, $blacklist);
    });

    $dirs = array();

    foreach($purchase_order_dirs as $purchase_order_dir) {
        $purchase_order_dir_absolute = "$certificates_dir/$purchase_order_dir";
        $files = $sftp->nlist($purchase_order_dir_absolute, true);

        $lot_dirs = array_filter($files, function($file) use ($blacklist) {
            $result = strpos($file, '/..');

            return $result !== false && $result>= 0;
        });

        $lot_dirs = array_map(function($file) {
            return str_replace('/.', '', str_replace('/..', '', $file));
        }, $lot_dirs);

        $dirs[] = $purchase_order_dir;

        foreach($lot_dirs as $lot_dir) {
            $dirs[] = "$purchase_order_dir/$lot_dir";
        }
    }

    return array_values($dirs);
}

function save_purchase_order_info($mysqli, $dirs, $location) {
    // If a value in $dirs contains a '/', it contains a LOT.
    $purchase_orders_result = $mysqli->query("SELECT name FROM nebula_purchase_orders");
    $purchase_orders_reference = $purchase_orders_result->fetch_all(MYSQLI_ASSOC);
    $purchase_orders_reference = array_map(function($order) {
        return $order['name'];
    }, $purchase_orders_reference);
    $purchase_orders_result->close();

    $lots_result = $mysqli->query("SELECT purchase_order, customer_code FROM nebula_lot");
    $lots_reference = $lots_result->fetch_all(MYSQLI_ASSOC);
    $lots_reference = array_map(function($lot) {
        return $lot['purchase_order'] . ' ' . $lot['customer_code'];
    }, $lots_reference);

    $purchase_orders = array();
    $lots = array();
    $blacklist = array('.', '..', 'New Folder', 'System Volume Information');

    foreach ($dirs as $dir) {
        if (strpos($dir, '/') !== false) {
            $dir_split = explode('/', $dir);
            $purchase_order = str_replace('#', '', $dir_split[0]);
            $lot = explode(' ', $dir_split[1]);

            if (in_array(implode(' ', $lot), $blacklist)) {
                continue;
            }

            if (!in_array($purchase_order, $purchase_orders_reference)
                && !in_array("('$purchase_order', '$location')", $purchase_orders)) {
                $purchase_orders[] = "('$purchase_order', '$location')";
            }

            if (count($lot) > 1) {
                if (!in_array(implode(' ', $lot), $lots_reference)) {
                    $lots[] = "('{$lot[0]}', '{$lot[1]}')";
                }
            } else {
                if (!in_array("$purchase_order {$lot[0]}", $lots_reference)) {
                    var_dump("$purchase_order {$lot[0]}");
                    $lots[] = "('$purchase_order', '{$lot[0]}')";
                }
            }
        } else {
            $purchase_order = str_replace('#', '', $dir);

            if (!in_array($purchase_order, $purchase_orders_reference)
                && !in_array("('$purchase_order', '$location')", $purchase_orders)) {
                $purchase_orders[] = "('$purchase_order', '$location')";
            }
        }
    }

    if (count($purchase_orders) > 0) {
        $purchase_orders_insert = "INSERT INTO nebula_purchase_orders (name, location) VALUES "
            . implode(',', $purchase_orders);

        $mysqli->query($purchase_orders_insert);
    }

    if (count($lots) > 0) {
        $lots_insert = "INSERT INTO nebula_lot (purchase_order, customer_code) VALUES "
            . implode(',', $lots);

        $mysqli->query($lots_insert);
    }
}
