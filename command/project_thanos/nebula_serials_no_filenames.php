<?php

require_once __DIR__.'/../database.php';

$db = create_mysqli_connection();

if (!$db) {
    die('Connection failed: ' . mysqli_connect_error());
}

$project_root_result = $db->query("SELECT value FROM configuration WHERE code = 'project_root'");

if (!$project_root_result) {
    $db->close();
    die("Unable to acquire project root.\n");
}

$project_root = $project_root_result->fetch_object()->value;

$no_filenames_query = "SELECT device_serial_number FROM nebula WHERE filename IS NULL OR filename = '' "
    . " AND parent_dir = 'PO78564NJ'";
$no_filenames_result = $db->query($no_filenames_query);

if (!$no_filenames_result) {
    print_r($db->error);
    $db->close();
    die("Unable to acquire serial numbers without file names.\n");
}

$serial_numbers = $no_filenames_result->fetch_all(MYSQLI_ASSOC);
$no_filenames_result->free();
$serial_numbers = array_map(function($report) { return $report['device_serial_number']; }, $serial_numbers);

$pdf_dir = "$project_root/nebula/certificates/pdf";
$pdfs = scandir($pdf_dir);
$pdfs = array_filter($pdfs, function($file) use ($pdf_dir) {
    $path = "$pdf_dir/$file";
    return !in_array($file, array('.', '..')) && !is_dir($path);
});

$pdfs_found = array_filter($pdfs, function($file) use ($serial_numbers, $pdf_dir) {
    $file_split = explode('-', $file);
    $file_serial = $file_split[1];
    return in_array($file_serial, $serial_numbers);
});

$no_filenames_dir = "$project_root/nebula/certificates/pdf/no_filenames_PO78564NJ";

if (!is_dir($no_filenames_dir)) {
    mkdir($no_filenames_dir);
}

$db->begin_transaction();

foreach ($pdfs_found as $pdf) {
    $filename_split = explode('-', $pdf);
    $serial_number = $filename_split = $filename_split[1];
    $filename = str_replace('.pdf', '.xml', str_replace('Certificate', 'Report', $pdf));

    $update_query = "UPDATE nebula "
                  . "SET filename = ? "
                  . "WHERE device_serial_number = ? "
                  . "AND parent_dir = 'PO78564NJ'";

    $update_stmt = $db->prepare($update_query);

    if (!$update_stmt) {
        echo "Unable to update filename for $serial_number\n";
        print_r($db->error);
    } else {
        $update_stmt->bind_param('ss', $filename, $serial_number);

        if (!$update_stmt->execute()) {
            echo "Unable to update filename for $serial_number\n";
            print_r($update_stmt->error);
        } else {
            echo "Succesfully updated filename for $serial_number\n";
        }
    }

    $source_dir = "$pdf_dir/$pdf";
    $destination_dir = "$no_filenames_dir/$pdf";
    copy($source_dir, $destination_dir);
}

$db->commit();

$db->close();
