<?php

function ebay_shopping_request_builder($items) {
    $item_ids = "";

    foreach($items as $item) {
        $item_ids .= "<ItemID>" . $item . "</ItemID>\n";
    }
    
    $xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<GetMultipleItemsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
$item_ids<IncludeSelector>Details</IncludeSelector>
</GetMultipleItemsRequest>
XML;

    $xml_element =  new SimpleXMLElement($xml);
    return $xml_element->asXML();
}
