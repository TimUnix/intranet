<?php
ini_set("memory_limit", "-1");
set_time_limit(0);

require_once __DIR__.'/database.php';
require_once __DIR__.'/ultimate-web-scraper/support/web_browser.php';
require_once __DIR__.'/ultimate-web-scraper/support/tag_filter.php';

$db = new db();


$rwidgets_dd = "";

// Create connection
$conn = mysqli_connect($db->servername(), $db->username(), $db->password(), $db->dbname());
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$count = count_records($conn);

$total_records_updated = 0;

if ($count > 0) {
    $offset = 0;
    $records_to_update = 20;
    $total_records = $count;

    while ($offset < $total_records) {
        //////////////
        $getAllEbayItems = getEbayStore($conn, $offset, $records_to_update);

        $ebay_store_update = [];

        if (!empty($getAllEbayItems)) {
				
            foreach($getAllEbayItems as $item){
                $viewitemurl = $item['link'];
                $itemnumber = $item['item_number']; 
					
					
                $result = get_rwidgets_from_ebay($viewitemurl);
					

                echo "item number: " . $itemnumber . "\n";
                $rwidgets = trim(preg_replace('/\xE2\x96\xA0/', '', $result['rwidgets']));
                $rwidgets_date = $result['rwidgets_date'];
                $rwidgets_link = $result['rwidgets_link'];
                $rwidgets_total_sold = $result['total_sold'];
					
                $rwidgets_date = preg_replace('/(\d+)' . ":" . '(\d+)' . ":" . '(\d+)/', '', $rwidgets_date);

                $rwidgets = str_replace(array('\'', '"'), '', $rwidgets); 
					
                $ebay_store_update[] = "UPDATE ebay_store
							SET rwidgets = '$rwidgets',
							last_update = '$rwidgets_date',
							history_purchase_link = '$rwidgets_link',
							total_sold = '$rwidgets_total_sold'
							WHERE item_number = '$itemnumber'";
            }
				
        }
        /////////////////
        //$paginated_data = array_slice($ebay_store_update, $offset, $records_to_update);
			
        if(count($ebay_store_update) > 0) {
            $update = implode("; ", $ebay_store_update);
            $total_paginated_records_updated = 0;
            $total_paginated_records = count($ebay_store_update);

            echo "Updating $total_paginated_records records\n";
            if (mysqli_multi_query($conn, $update)) {
                do {
                    if (mysqli_affected_rows($conn) > 0) {
                        $total_paginated_records_updated += mysqli_affected_rows($conn);
                        echo "Updated $total_paginated_records_updated out of $total_paginated_records records\n";
                    }
                } while (mysqli_next_result($conn));
            } else {
                echo nl2br("Error updating record: " . mysqli_error($conn) ."<br>");
            }
        }

        $offset += $records_to_update;
        $progress =  $offset / $count * 100;
        $progress = number_format($progress, 2);

        echo "Progress: $progress%\n";
    }
}

mysqli_close($conn);

function count_records($conn){
    $sql = "SELECT COUNT(*) AS records from ebay_store where selling_state = 'Active'";
    $data = mysqli_query($conn, $sql);
    if ($data->num_rows > 0) {
        // output data of each row
        while($row = $data->fetch_assoc()) {
            return $row['records'];
        }
    } 
}

function getEbayStore($conn, $offset, $total_records){
    $sql = "SELECT item_number, link from ebay_store where selling_state = 'Active' Limit $offset , $total_records";
    $data = mysqli_query($conn, $sql);
		
    $storage_array = array();
    if(!empty($data)){
        foreach($data as $key => $value){
            $storage_array[] = [
                'item_number' => $value['item_number'],
                'link' => $value['link']
            ];
        }
    }

    return $storage_array;
}

function get_rwidgets_from_ebay($url) {
    // Retrieve the standard HTML parsing array for later use.
    $htmloptions = TagFilter::GetHTMLOptions();

    // Retrieve a URL (emulating Firefox by default).
    $web = new WebBrowser();
    $result = $web->Process($url);

    // Check for connectivity and response errors.
    if (!$result["success"]) {
        echo "Error retrieving URL.  " . $result["error"] . "\n";
        return;
    }

    if ($result["response"]["code"] != 200) {
        echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
        return;
    }

    // Get the final URL after redirects.
    $baseurl = $result["url"];

    $html = TagFilter::Explode($result["body"], $htmloptions);

    // Retrieve a pointer object to the root node.
    $root = $html->Get();

    $scripts = $root->Find("script");


    /// find html


    $htmls_sold = $root->Find("html");
    $for_htmls_sold = serialize($htmls_sold);

    if(preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $for_htmls_sold, $matches_link_sold)){
        foreach($matches_link_sold[0] as $matches_link_data_sold){
			
            $pos_sold = strpos($matches_link_data_sold,"bin/purchaseHistory");
            if($pos_sold == true){
                $rwidgets_link_sold = $matches_link_data_sold;
					
                break;
            }else{
                $rwidgets_link_sold = "";
					
                //break;
            }
			
		}
    }else{
        $rwidgets_link_sold = "";
    }
		
    $htmls = $root->Find("html");
    $for_htmls = serialize($htmls);
    if(preg_match_all('/(\d+) sold/', $for_htmls, $sold_matches)){
        if($rwidgets_link_sold == ""){
            $total_sold = 0;
        }else{
            $total_sold = $sold_matches[1][0];
        }
			
    }else{
        $total_sold = 0;
    }
    /// end html

    $found_script = "";

    foreach ($scripts as $script) {
        if (strpos($script, "rwidgets")) {
            $found_script = $script;
        }
    }

    /// for pdt
    $for_pdt = serialize($scripts);

    $string = str_replace(' ', '-', $for_pdt);

    $seacrh_pdt = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

    $pdt = $seacrh_pdt;
    $pattern_pdt = "/PDT/i";
    preg_match($pattern_pdt, $pdt, $matches_pdt, PREG_OFFSET_CAPTURE);
    /// end pdt

    /// for dd
    $for_dd = serialize($scripts);

    $string_dd = str_replace(' ', '-', $for_dd);

    $seacrh_dd = preg_replace('/[^A-Za-z0-9\-]/', '', $string_dd);

    $dd = $seacrh_dd;
    $pattern_dd = "/\d{2}\-\d{2}-\d{4}/";

    $last_updated_tags = $root->Find('span.vi-desc-revHistory-lbl');
    $last_updated_tag_found = $last_updated_tags->count() > 0 && $last_updated_tags->current() !== false;
                                
    if ($last_updated_tag_found) {
        $last_updated_text = $last_updated_tags->current()->parent()->GetPlainText();
        $last_updated_split = explode('&nbsp;', $last_updated_text);
        $date_rwidget = $last_updated_split[1];

        $patterns = array ('/ PST/', '/ PDT/');
        $date_rwidget = preg_replace($patterns, '', $date_rwidget);
    } else {
        $date_rwidget = '';
    }

    /// end dd

    /// for link
    if(preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $for_htmls, $matches_link)){
        foreach($matches_link[0] as $matches_link_data){
			
            $pos = strpos($matches_link_data,"/bin/purchaseHistory");
            if($pos == true){
                $rwidgets_link = $matches_link_data;
					
                break;
            }else{
                $rwidgets_link = "";
					
                //break;
            }
        }
    } else {
        $rwidgets_link = "";
    }
    /// end link
		
    if ($found_script === "") {
        echo "Couldn't find \$rwidgets";
        return "";
    }

    $rwidgets = "";

    foreach (preg_split("/((\r?\n)|(\r\n?))/", $found_script) as $line){
        if (strpos($line, "rwidgets")) {
            $rwidgets = $line;
        }
    }

    if ($rwidgets === "") {
        echo "Couldn't find \$rwidgets";
    }

    $rwidgets = str_replace("\$rwidgets(", "", $rwidgets);
    $rwidgets = preg_replace("/\);new .*$/", "", $rwidgets);
		
    return array('rwidgets'=> $rwidgets, 'rwidgets_date'=> $date_rwidget, 'rwidgets_link' => $rwidgets_link, 'total_sold' => $total_sold);
}

?>
