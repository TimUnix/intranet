<?php

include_once(__DIR__.'/../database.php');

$db = new db();
$mysqli = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

echo "Acquiring unlisted items from inventory...\n";

$unlisted_items_query = "SELECT `par`, `cat`, `des`, `qty`, `pri` FROM `inventory` WHERE `arc` = 'False' AND `qty` > 0";
$unlisted_items_stmt = $mysqli->prepare($unlisted_items_query);

if (!$unlisted_items_stmt) {
    echo "An unexpected error occurred.\n";
    echo "{$mysqli->error}\n";

    return;
}

if (!$unlisted_items_stmt->execute()) {
    echo "An unexpected error occurred.\n";
    echo "{$mysqli->error}\n";

    return;
}

$unlisted_items_result = $unlisted_items_stmt->get_result();

if ($unlisted_items_result === false) {
    echo "An unexpected error occurred while acquiring unlisted items.\n";
    echo "{$unlisted_items_stmt->error}";
    return;
}

$unlisted_items_count = (int) $unlisted_items_result->num_rows;
if ($unlisted_items_count === 0) {
    echo "Unable to find unlisted items. Exiting.\n";
    return;
}

$update_listing_values = [];
$add_to_listing_values = [];

while ($item = $unlisted_items_result->fetch_object()) {
    $sku = $mysqli->real_escape_string($item->par);
    $category = $mysqli->real_escape_string($item->cat);
    $description = $mysqli->real_escape_string($item->des);
    $quantity = $item->qty;
    $price = $item->pri;


    $check_ssku_query = "SELECT `sku` FROM listing";
    $check_ssku_stmt = $mysqli->prepare($check_ssku_query);
    
    if ($check_ssku_stmt === false || !$check_ssku_stmt->execute()) {
        echo "An unexpected error occurred while checking if sku $sku exists.\n";
        echo "{$mysqli->error}\n";
        continue;
    }
    $check_ssku_result = $check_ssku_stmt->get_result();
    if ($check_ssku_result === false) {
        echo "An unexpected error occurred while acquiring listing items.\n";
        echo "{$check_ssku_stmt->error}";
        return;
    }
    
    $check_ssku_result_count = (int) $check_ssku_result->num_rows;
    if ($check_ssku_result_count === 0) {
        echo "Unable to find listing items.Adding new ones...\n";
       
    }
    while ($items = $check_ssku_result->fetch_object()){
    $ssku = $items->sku;
    $listingsku = $mysqli->real_escape_string($ssku); 
    }
    if($listingsku = $sku){

    $check_sku_query = "SELECT `id` FROM listing WHERE `sku` = ?";
    $check_sku_stmt = $mysqli->prepare($check_sku_query);
    $check_sku_stmt->bind_param('s', $ssku);
    }

    if ($check_sku_stmt === false || !$check_sku_stmt->execute()) {
        echo "An unexpected error occurred while checking if sku $sku exists.\n";
        echo "{$mysqli->error}\n";
        continue;
    }

    $check_sku_result = $check_sku_stmt->get_result();
    $check_sku_count = $check_sku_result->num_rows;

    if ($check_sku_count > 0) {
        $update_listing_values[] = "UPDATE `listing` SET `category` = '$category', `description` = '$description', `quantity` = $quantity, `price` = $price WHERE `sku` = '$sku'";
    } else{
        $add_to_listing_values[] = "('$sku', '$category', '$description', $quantity, $price)";     
        }
}


echo "$unlisted_items_count unlisted found. Adding to listing...\n";

$paginated_item_count = 20;

$update_listing_values_count = count($update_listing_values);
if ($update_listing_values_count > 0) {

    $total_records_updated = 0;

    for ($items_updated = 0; $items_updated < $update_listing_values_count; $items_updated += $paginated_item_count) {
    $mysqli->begin_transaction();

    $paginated_items = array_slice($update_listing_values, $items_updated, $paginated_item_count);

    foreach($paginated_items as $query) {
        $update_listing_stmt = $mysqli->prepare($query);

        if ($update_listing_stmt !== false && $update_listing_stmt->execute()) {
            $total_records_updated++;
        }
    }

    $mysqli->commit();
    echo "Updated $total_records_updated out of $update_listing_values_count.\n";
    }
}

$add_to_listing_values_count = count($add_to_listing_values);
if ($add_to_listing_values_count > 0) {
    $total_records_added = 0;
    for ($items_added = 0; $items_added < $add_to_listing_values_count; $items_added += $paginated_item_count) {
        $paginated_items = array_slice($add_to_listing_values, $items_added, $paginated_item_count);
        $add_to_listing_query = 'INSERT INTO listing (`sku`, `category`, `description`, `quantity`, `price`) VALUES '
                              . implode(', ', $paginated_items);
        $add_to_listing_stmt = $mysqli->prepare($add_to_listing_query);
    
        if ($add_to_listing_stmt !== false && $add_to_listing_stmt->execute()) {
            $total_records_added += $paginated_item_count;
            echo "Added $total_records_added out of $add_to_listing_values_count items.\n";
        } else {
            echo "An unexpected error occurred.\n";
            echo "{$mysqli->error}\n";
            echo "$add_to_listing_query\n";
        }
    }
}

$mysqli->close();
