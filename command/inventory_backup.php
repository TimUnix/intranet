<?php
ini_set('memory_limit', '-1');
set_time_limit(0);


require_once __DIR__.'/database.php';
$project_root_result = '/home/ironhyde/backup/';

$db = new db();
$conn = new mysqli($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$values = scandir($project_root_result);

foreach ($values as $key) {

    $explode_filename = explode('_', $key);

    $explode_filename_for_date = explode('.', $explode_filename[1]);

    $date = $explode_filename_for_date[0];
  
    $date_now = date('Y-m-d');

    if($date == $date_now){
        $filename = $project_root_result.$key;
      
        $file = fopen($filename,"r");
        
        fgetcsv($file, 1000, ",");

        while ($data = fgetcsv($file)) {
      
            if($data[4] == '' || $data[4] == null){
                $price = null;
            }else{
                $price = $data[4];
            }

            if($data[5] == '' || $data[5] == null){
                $qty = null;
            }else{
                $qty = $data[5];
            }


            $insert = "INSERT INTO inventory_backup(sku, price, qty, date)
                VALUES('".$data[0]."', '".$price."', '".$qty."', '".$date."')";
            $insert_stmt = $conn->prepare($insert);
                
            if ($insert_stmt && $insert_stmt->execute()) {
                echo "Successfully added {$insert_stmt->affected_rows} records...\n\n";
            } else {
                echo "An error occured while saving the records for ".$data[0]."...\n\n";
                echo "An error occured while saving the records for ".$filename."...\n\n";
                print_r($conn->error_list);
            }
          
            
        }
        echo 'Done';
        fclose($file);
    }
}
if($date !== $date_now){
    echo "No Script for ".$date_now."";
}
 
?>