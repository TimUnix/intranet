<?php
ini_set('memory_limit', '-1');
set_time_limit(0);

require_once __DIR__ . "/database.php";

$db = new db();
$conn = mysqli_connect($db->servername(), $db->username(), $db->password(), $db->dbname());

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$stmt = $conn->prepare("DELETE FROM `keywords_top_store`");
    if ($stmt->execute()) {
        echo "Successful\n";
    } else {
        echo "Error: {$stmt->error}\n";
    }  

$ebay_base_url ="https://svcs.ebay.com/services/search/FindingService/v1?";
$ebay_operation_name = "findItemsByKeywords";
$ebay_service_version = "1.13.0";
$ebay_app_id = "UNIXSurp-itrecycl-PRD-481f16f17-8dcc11f2";
$ebay_global_id = "EBAY-US";
$max_count = 20;

$keywords_top_store_update = array();
$keywords_top_store_insert = [];

$keyword_query = "SELECT keyword_prase FROM `keywords`";
$keyword_result = $conn->query($keyword_query);

if ($keyword_result && $keyword_result->num_rows > 0){
  while ($keyword_searcher = $keyword_result->fetch_object()){
    $keyword_prase = explode(',', $keyword_searcher->keyword_prase);
    $unique_keywords = array_unique($keyword_prase);

    foreach($unique_keywords as $unique_keyword) {
      $unique_keywords = $unique_keyword;

    $api_url = $ebay_base_url
                     . "OPERATION-NAME=$ebay_operation_name"
                     . "&SECURITY-APPNAME=$ebay_app_id"
                     . "&SERVICE-VERSION=$ebay_service_version"
                     . "&GLOBAL-ID=$ebay_global_id"
                     . "&outputSelector=StoreInfo"
                     . "&paginationInput=True"
                     . "&paginationInput.entriesPerPage=20"
                     . "&keywords=$unique_keywords";

    $ebaylinkapi = new SimpleXMLElement($api_url, 0, true);
    $keyword;
   
    if ($ebaylinkapi->ack == "Success") {
      foreach($ebaylinkapi->searchResult->item as $item){
        $store_name = $conn->real_escape_string((string) $item->storeInfo->storeName);
        $category = $conn->real_escape_string($item->primaryCategory->categoryName);
        $title = $conn->real_escape_string((string) $item->title);
        $store_location = $conn->real_escape_string((string) $item->location);
        $price = $conn->real_escape_string((string) $item->sellingStatus->currentPrice);
        $current_price = floatval($price);
        $item_condition = $conn->real_escape_string($item->condition->conditionDisplayName);
        $view_item_url = str_replace(' ', '', $conn->real_escape_string((string) $item->viewItemURL));

          $keywords_top_store = array(
                $unique_keywords,
                $store_name,
                $category,
                $title,
                $store_location,
                $current_price,
                $item_condition,
                $view_item_url
                );

          $keywords_top_store_insert[] = '"'.implode('","', $keywords_top_store).'"';
        }
      }
    }
  }

  if (count($keywords_top_store_insert) > 0){
  $offset = 0;
  $records_to_insert = 20;
  $total_records = count($keywords_top_store_insert);

    while ($offset <= $total_records){
      $paginated_data = array_slice($keywords_top_store_insert, $offset, $records_to_insert);

      $insert = "INSERT INTO keywords_top_store ("
                  . "`keyword`, "
                  . "`store_name`, "
                  . "`category`, "
                  . "`title`, "
                  . "`store_location`, "
                  . "`current_price`, "
                  . "`item_condition`, "
                  . "`view_item_url` "
                  . ") VALUES ("
                  . implode('),(', $paginated_data )
                  .")";

          $insert_stmt = $conn->prepare($insert);

          if ($insert_stmt && $insert_stmt->execute()) {
              echo "Successfully added {$insert_stmt->affected_rows} records...\n\n";
          } else {
              echo "An error occured while saving the records for...\n\n";
              print_r($insert);
              print_r($conn->error_list);
          }

          $offset += $records_to_insert;
    }
  }
}

?>