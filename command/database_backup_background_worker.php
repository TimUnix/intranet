<?php

require_once __DIR__.'/database.php';

$db = new db();
$mysqli = create_mysqli_connection();

$project_root_result = $mysqli->query("SELECT value FROM configuration WHERE code = 'project_root'");

if (!$project_root_result) {
    print_r($mysqli->error);
    $mysqli->close();
    exit("Failed to acquire project root. Exiting...\n");
}

$project_root = $project_root_result->fetch_object()->value;
$backup_folder = "$project_root/backups/database";

if (!is_dir($backup_folder)) {
    mkdir($backup_folder, 0755, true);
}

echo "Backing up database....\n";

$date = date_create('now', new DateTimeZone('America/Los_Angeles'))->format('Y-m-d-H-i-s');
$filepath = "$backup_folder/$date.sql";
touch($filepath);

echo exec("mysqldump -u {$db->username()} -p{$db->password()} --all-databases --result-file=$filepath");

$mysqli->close();

$backups = scandir($backup_folder);
$backups = array_filter($backups, function($file) { return !in_array($file, array('.', '..')); });

$backup_days_archive_count = 40;
$backup_count = count($backups);

if ($backup_count > $backup_days_archive_count) {
    $backups_to_remove = $backup_count - $backup_days_archive_count;
    echo "Backups to remove: $backups_to_remove\n";

    sort($backups);

    $outdated_backups = array_slice($backups, 0, $backups_to_remove);

    foreach ($outdated_backups as $backup) {
        unlink("$backup_folder/$backup");
    }
}
