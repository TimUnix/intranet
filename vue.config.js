const { defineConfig } = require('@vue/cli-service');
const path = require('path');
module.exports = defineConfig({
    transpileDependencies: true,
    outputDir: 'resources',
	configureWebpack: {
		resolve: {
			alias: {
				'@': path.join(__dirname, 'resources', 'vue')
			}
		},
		entry: {
			app: path.join(__dirname, 'resources', 'vue', 'main.js')
		}
	}
});

